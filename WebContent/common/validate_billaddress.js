// Used to validate Billing Information

function FrontPage_Form1_Validator(theForm)
{
  if (theForm.PAYMENT_FIRSTNAME.value == "" && theForm.PAYMENT_NUMBER.value == "")
  {
   alert("Please enter a value for the \"First Name \" option.");
   theForm.PAYMENT_FIRSTNAME.focus();
   return (false);
  }
   
  if (theForm.PAYMENT_LASTNAME.value == "" && theForm.PAYMENT_NUMBER.value == "")
  {
   alert("Please enter a value for the \"Last Name \" option.");
   theForm.PAYMENT_LASTNAME.focus();
   return (false);
  } 

  if (theForm.PAYMENT_STREET.value == "" && theForm.PAYMENT_NUMBER.value == "")
  {
   alert("Please enter a value for the \"Street\" field.");
   theForm.PAYMENT_STREET.focus();
   return (false);
  }

  if (theForm.PAYMENT_CITY.value == "" && theForm.PAYMENT_NUMBER.value == "")
  {
   alert("Please enter a value for the \"City\" field.");
   theForm.PAYMENT_CITY.focus();
   return (false);
  }

  if (theForm.PAYMENT_STATE.selectedIndex < 0 && theForm.PAYMENT_NUMBER.value == "")
  {
   alert("Please select one of the \"State\" options.");
   theForm.PAYMENT_STATE.focus();
   return (false);
  }

  if (theForm.PAYMENT_STATE.selectedIndex == 0 && theForm.PAYMENT_NUMBER.value == "")
  {
   alert("The first \"State\" option is not a valid selection.  Please choose one of the other options.");
   theForm.PAYMENT_STATE.focus();
   return (false);
  }

  with (theForm.PAYMENT_ZIP)
    {
 	var newValue = (value);
 	var newLength = (value.length);
 	var newFlag = 0;

	if (value.length != 5)
	{ 
 	 newFlag = 1; 	
	} 
	
	for(var i = 0; i != newLength; i++) 
 	{ 	
  	 aChar = newValue.substring(i,i+1);
  	 if(aChar < "0" || aChar > "9") 
  	 {
  	   newFlag = 1;  	 
         }
       } 
    
      if (newFlag == 1 && theForm.PAYMENT_NUMBER.value == "") {
        alert("Please enter a value for the \"Billing zip code\" option.");
        theForm.PAYMENT_ZIP.focus();	  
        return (false);
        }
    }

 
// check for all three phone fields, make sure they aren't blank
 
 if (((theForm.PAYMENT_PHONE1.value == "") || (theForm.PAYMENT_PHONE2.value == "") || 
 	  (theForm.PAYMENT_PHONE3.value == "")) && theForm.PAYMENT_NUMBER.value == "")
    {
      alert("Please enter a value for the \"Telephone\" option.");
      theForm.PAYMENT_PHONE1.focus();
      return (false);
    }  	  
 
//check for correct length of entry and no characters on all three fields

  with (theForm.PAYMENT_PHONE1)
        {
 	    var newValue = (value);
 	    var newLength = (value.length);
 	    var newFlag = 0;
	
	    for(var i = 0; i != newLength; i++) 
 	    { 	
  	     aChar = newValue.substring(i,i+1);
  	     if((aChar < "0" || aChar > "9" || newLength != 3) && theForm.PAYMENT_NUMBER.value == "")
  	       {
  	        alert("Please enter a value for the \"Telephone\"  option.");
		theForm.PAYMENT_PHONE1.focus();	  
                return (false); 
               }
            }         	 
        }
       
  with (theForm.PAYMENT_PHONE2)
        {
 	    var newValue = (value);
 	    var newLength = (value.length);
 	    var newFlag = 0;
	
	    for(var i = 0; i != newLength; i++) 
 	    { 	
  	     aChar = newValue.substring(i,i+1);
  	     if((aChar < "0" || aChar > "9" || newLength != 3) && theForm.PAYMENT_NUMBER.value == "") 
  	       {
  	        alert("Please enter a value for the \"Telephone\"  option.");
		theForm.PAYMENT_PHONE2.focus();	  
                return (false); 
               }
            }         	 
        }

  with (theForm.PAYMENT_PHONE3)
        {
 	    var newValue = (value);
 	    var newLength = (value.length);
 	    var newFlag = 0;
	
	    for(var i = 0; i != newLength; i++) 
 	    { 	
  	     aChar = newValue.substring(i,i+1);
  	     if((aChar < "0" || aChar > "9" || newLength != 4) && theForm.PAYMENT_NUMBER.value == "") 
  	       {
  	        alert("Please enter a value for the \"Telephone\"  option.");
		theForm.PAYMENT_PHONE3.focus();	  
                return (false); 
               }
            }         	 
        }
  if (theForm.CCInfoEZPay.checked)
  {
      if (theForm.PAYMENT_NUMBER.value == "")
      {
	    alert("Please enter a value for the \"credit card number\" option.");
	    theForm.PAYMENT_NUMBER.focus();
	    return (false);
	  }
	  if (theForm.PAYMENT_MONTH.value == "Month")
	  {
	    alert("Please enter a value for the \"Credit card expiration month\" option.");
	    theForm.PAYMENT_MONTH.focus();
	    return (false);
	  }
	  if (theForm.PAYMENT_YEAR.value == "Year")
	  {
	    alert("Please enter a value for the \"Credit card expiration year\" option.");
	    theForm.PAYMENT_YEAR.focus();
	    return (false);
	  } 
  }
}
