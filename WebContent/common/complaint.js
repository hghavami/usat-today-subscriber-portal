<!-- 

   var requiresSection = false;
   var sectionSelected = '';
	
   function validateForm(theForm) {
   
      if (checkRadios()==false) {
         alert('Please select the most appropriate Delivery Issue.');
         return false;
      }

      if (requiresSection) {
         if (sectionSelected.length < 2) {
	       alert('Please select the section that was missing.');
	       theForm.sectionOptions.focus();
               return false;
         }

         theForm.section.value = sectionSelected;
      }

       return true;
   }

   function checkRadios() {
      var el = document.forms[0].elements;

      for(var i = 0 ; i < el.length ; ++i) {
         if(el[i].type == "radio") {
            var radiogroup = el[el[i].name]; // get the whole set of radio buttons.
            var itemchecked = false;
            for(var j = 0 ; j < radiogroup.length ; ++j) {
               if(radiogroup[j].checked) {
	          itemchecked = true;
	          break;
	       }
            }
            if(!itemchecked) { 
	       return false;
            }
         }
     }
     return true;
  } 


   function check(selectedText) {
      if (selectedText == '9') {
	requiresSection = true;
      }
      else {
             requiresSection = false;
      }
   }   

   function checkOptionStatus(theForm) {

      if (theForm.sectionOptions.selectedIndex > 0) {
         theForm.sectionOptions.disabled=false;
      }
      else {
         theForm.sectionOptions.disabled=true;
      }
   }
	
   function setSection(sectionOps) {
      sectionSelected = sectionOps[sectionOps.selectedIndex].text;
   }

   function disableSections(theForm) {
	theForm.sectionOptions.disabled=true;
	theForm.sectionOptions.selectedIndex=0;
   }

function validateDates(dateForm) {

   // check that at least one date is selected
   if ( (dateForm.ISSUE_DATE1.value == "") &&
        (dateForm.ISSUE_DATE2.value == "") &&
        (dateForm.ISSUE_DATE3.value == "") &&
        (dateForm.ISSUE_DATE4.value == "") &&
        (dateForm.ISSUE_DATE5.value == "")  )    {
      alert("Please enter the date(s) that the issue occurred.");
      dateForm.ISSUE_DATE1.focus();
      return (false);
   } 

   if (dateForm.ISSUE_DATE1.value.length > 0) {
      if (checkDate(dateForm.ISSUE_DATE1) == false) {
         return (false);
      }
   }

   if (dateForm.ISSUE_DATE2.value.length > 0) {
      if (checkDate(dateForm.ISSUE_DATE2) == false) {
         return (false);
      }
   }

   if (dateForm.ISSUE_DATE3.value.length > 0) {
      if (checkDate(dateForm.ISSUE_DATE3) == false) {
         return (false);
      }
   }

   if (dateForm.ISSUE_DATE4.value.length > 0) {
      if (checkDate(dateForm.ISSUE_DATE4) == false) {
         return (false);
      }
   }

   if (dateForm.ISSUE_DATE5.value.length > 0) {
      if (checkDate(dateForm.ISSUE_DATE5) == false) {
         return (false);
      }
   }

   return (true);
}

function checkDate(date) {

   var year = '';
   var month = '';
   var day = '';

   with (date) {
      p1pos=value.indexOf("/");
      p2pos=value.lastIndexOf("/");     
      endpos=value.length;

      if ((p1pos != 2) || (p2pos != 5) || (endpos != 10))  {
         alert("The date must be in mm/dd/yyyy format.  Please try again.");
         focus(); 
         select();
         return (false);
      }

      var newValue = (value);
      var newLength = (value.length);
      var newFlag = 0;

      year = newValue.substr(6,4);
      month = newValue.substr(0,2);
      day = newValue.substr(3,2);

      for(var i = 0; i != newLength; i++)  { 	
         aChar = newValue.substring(i,i+1);
          
         if (aChar != "/")  {
            if ((aChar < "0") || (aChar > "9")) {
               alert("Please enter a valid date for the highlighted date.");
               focus();
               select();
               return (false); 
            }
         } 
      } // end for
   } // end with date

   if ( (parseInt(month,10) > 12) || (parseInt(month,10) < 1) || (parseInt(day,10) < 1) || (parseInt(day,10) > 31)) {
      alert('Invalid Issue Date. Please enter a valid date.');
      date.focus();
      date.select();
      return (false);
   }

   var today = new Date();
   var tempDate = new Date( year, month-1, day);

   if (tempDate.getFullYear() > today.getFullYear()) {
      alert('Issue Date cannot be greater than today\'s date: ' + today.toString());
      date.focus();
      date.select();
      return (false);
   }
   else {
      if (tempDate.getFullYear() == today.getFullYear()) { // years are equal so check month
         if( tempDate.getMonth() > today.getMonth()) {
            alert('Issue Date cannot be greater than today\'s date: ' + today.toString());
            date.focus();
            date.select();
            return (false); 
         }
         else { // check the day if necessary
             if(tempDate.getMonth() == today.getMonth()) {
                if(tempDate.getDate() > today.getDate()) {
                  alert('Issue Date cannot be greater than today\'s date: ' + today.toString());
                  date.focus();
                  date.select();
                  return (false); 
                }
             }
         }
      } // end else
   } // end if year is not greater than today's year

   return (true);
}    // end checkDate()	
   // -->

///////////////////////////////////////////////////////////////////////
//////////// NEW STUFF Delete Above once we purge old stuff
//////////////////////////////////////////////////////////////////////
function cp_dateChanged(textField) {
	if (cp_isValidDateFormat(textField)) {
		cp_doPubDateCheck(textField);
	}
	else {
		alert("Invalid Date Format. Date must be in MM/DD/YYYY format.");
		textField.focus();
		textField.select();
	}
}

function cp_isValidDateFormat(formField)
{
	var result = true;
	var formValue = formField.value;
	
 	if (formValue.length>0)
 	{
 		var elems = formValue.split("/");
 		
 		result = (elems.length == 3); // should be three components
 		
 		if (result)
 		{
 			result = !(isNaN(elems[0])) && !(isNaN(elems[1])) && !(isNaN(elems[2]));
 			
 			if (result) {
 				var month = parseInt(elems[0],10);
	 			var day = parseInt(elems[1],10);
 			
				result = (elems[0].length == 2) && (month > 0) && (month < 13) &&
						 (elems[1].length == 2) && (day > 0) && (day < 32) &&
						 (elems[2].length == 4);
			}
 		} 		
	} 
	
	return result;
}

function cp_initRequest(url) {
       if (window.XMLHttpRequest) {
           return new XMLHttpRequest();
       } else if (window.ActiveXObject) {
           isIE = true;
           return new ActiveXObject("Microsoft.XMLHTTP");
       }
}

   function cp_doPubDateCheck(textField) {
	   var completeField = textField;
       if (completeField.value == "") {
           return;
       } else {  
    	   var url = null;
    	   url = "/validatePubDateForCurrentPub.do?selectedDate=" + escape(completeField.value);	
           var req = cp_initRequest(url);
           req.onreadystatechange = function() {
               if (req.readyState == 4) {
                   if (req.status == 200) {
                	   cp_parseMessages(req.responseXML,textField);
                   } else if (req.status == 204){
                       return;
                   }
               }
           };
           req.open("GET", url, true);
           req.send(null);
       }
   }

   function cp_parseMessages(responseXML,textField) {
           var response = responseXML.getElementsByTagName(
                   "datevalid")[0];
      // alert('response: ' + response.childNodes[0].nodeValue);
       if (response.childNodes[0].nodeValue == 'true') {
       		// date valid, do nothing
       		;
       } else {
       		//alert('USA TODAY is not published on Saturdays or Sundays. Please pick a weekday.');
       		var eMsg = responseXML.getElementsByTagName("message")[0].childNodes[0].nodeValue;
       		alert(eMsg);
       		try{
     			textField.value = "";
     			textFeild.focus();
       		}
       			catch(e){}
       		}    
   }
