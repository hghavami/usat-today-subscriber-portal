// For Delivery Address Validation

function FrontPage_Form1_Validator(theForm)
{

  if (theForm.DELIVERY_COMPANYNAME.value == "")
  {
    if (theForm.DELIVERY_LASTNAME.value == "")
    {		
    alert("Please enter a value for the \"Last Name or Company Name\" option.");
    theForm.DELIVERY_LASTNAME.focus();
    return (false);
    }
  } 

  if (theForm.DELIVERY_LASTNAME.value != "")
  {
    if (theForm.DELIVERY_FIRSTNAME.value == "")
    {		
    alert("Please enter a value for the \"First Name \" option.");
    theForm.DELIVERY_FIRSTNAME.focus();
    return (false);
    }
  } 

  
  if (theForm.DELIVERY_STREET1.value == "")
  {
    alert("Please enter a value for the \"Street\" field.");
    theForm.DELIVERY_STREET1.focus();
    return (false);
  }

  
  if (theForm.DELIVERY_CITY.value == "")
  {
    alert("Please enter a value for the \"city\" field.");
    theForm.DELIVERY_CITY.focus();
    return (false);
  }

  if (theForm.DELIVERY_STATE.selectedIndex < 0)
  {
    alert("Please select one of the \"state\" options.");
    theForm.DELIVERY_STATE.focus();
    return (false);
  }

  if (theForm.DELIVERY_STATE.selectedIndex == 0)
  {
    alert("The first \"state\" option is not a valid selection.  Please choose one of the other options.");
    theForm.DELIVERY_STATE.focus();
    return (false);
  }

  with (theForm.DELIVERY_ZIP)
  {
 	var newValue = (value);
 	var newLength = (value.length);
 	var newFlag = 0;

	if (value.length != 5)
	{ 
 	 newFlag = 1; 	
	} 
	
	for(var i = 0; i != newLength; i++) 
 	{ 	
  	 aChar = newValue.substring(i,i+1);
  	 if(aChar < "0" || aChar > "9") 
  	 {
  	   newFlag = 1;  	 
         }
       } 
    
    if (newFlag == 1) {
      alert("Please enter a value for the \"Delivery zip code\" option.");
      theForm.DELIVERY_ZIP.focus();	  
      return (false);
      }
  }

  if ((theForm.DELIVERY_HOMEPHONE1.value == "") || (theForm.DELIVERY_HOMEPHONE2.value == "") || (theForm.DELIVERY_HOMEPHONE3.value == ""))
    {
     if ((theForm.DELIVERY_WORKPHONE1.value == "") || (theForm.DELIVERY_WORKPHONE2.value == "") || (theForm.DELIVERY_WORKPHONE3.value == ""))
     {
      alert("Please enter a value for the \"Home or Work telephone\" options.");
      theForm.DELIVERY_HOMEPHONE1.focus();
      return (false);
     }  	  
    }

    if ((theForm.DELIVERY_HOMEPHONE1.value != "") || (theForm.DELIVERY_HOMEPHONE2.value != "") || (theForm.DELIVERY_HOMEPHONE3.value != ""))
    {
     if ((theForm.DELIVERY_HOMEPHONE1.value == "") || (theForm.DELIVERY_HOMEPHONE2.value == "") || (theForm.DELIVERY_HOMEPHONE3.value == ""))
     {
      alert("Please enter a value for the \"Home telephone\" options.");
      theForm.DELIVERY_HOMEPHONE1.focus();
      return (false);
     }  	  
    }

    if ((theForm.DELIVERY_WORKPHONE1.value != "") || (theForm.DELIVERY_WORKPHONE2.value != "") || (theForm.DELIVERY_WORKPHONE3.value != ""))
    {
     if ((theForm.DELIVERY_WORKPHONE1.value == "") || (theForm.DELIVERY_WORKPHONE2.value == "") || (theForm.DELIVERY_WORKPHONE3.value == ""))
     {
      alert("Please enter a value for the \"Work telephone\" options.");
      theForm.DELIVERY_WORKPHONE1.focus();
      return (false);
     }  	  
    }

	    
        with (theForm.DELIVERY_WORKPHONE1)
              {
 	      var newValue = (value);
 	      var newLength = (value.length);
 	      var newFlag = 0;
	
	      for(var i = 0; i != newLength; i++) 
 	      { 	
  	       aChar = newValue.substring(i,i+1);
  	       if(aChar < "0" || aChar > "9" || newLength != 3) 
  	         {
  	         alert("Please enter a value for the \"Home or Work telephone\"  option.");  theForm.DELIVERY_WORKPHONE1.focus();	  
                 return (false); 
                 }
              }         	 
        }
        
        with (theForm.DELIVERY_WORKPHONE2)
        {
 	    var newValue = (value);
 	    var newLength = (value.length);
 	    var newFlag = 0;
	
	    for(var i = 0; i != newLength; i++) 
 	    { 	
  	     aChar = newValue.substring(i,i+1);
  	     if(aChar < "0" || aChar > "9" || newLength != 3) 
  	       {
  	        alert("Please enter a value for the \"Home or Work telephone\"  option.");  theForm.DELIVERY_WORKPHONE2.focus();	  
                return (false); 
               }
            }         	 
        }

        with (theForm.DELIVERY_WORKPHONE3)
        {
 	    var newValue = (value);
 	    var newLength = (value.length);
 	    var newFlag = 0;
	
	    for(var i = 0; i != newLength; i++) 
 	    { 	
  	     aChar = newValue.substring(i,i+1);
  	     if(aChar < "0" || aChar > "9" || newLength != 4) 
  	       {
  	        alert("Please enter a value for the \"Home or Work telephone\"  option.");  theForm.DELIVERY_WORKPHONE3.focus();	  
                return (false); 
               }
            }         	 
        }
     	  
  
      
        with (theForm.DELIVERY_HOMEPHONE1)
        {
 	    var newValue = (value);
 	    var newLength = (value.length);
 	    var newFlag = 0;
	
	    for(var i = 0; i != newLength; i++) 
 	    { 	
  	     aChar = newValue.substring(i,i+1);
  	     if(aChar < "0" || aChar > "9" || newLength != 3) 
  	       {
  	        alert("Please enter a value for the \"Home or Work telephone\"  option.");  theForm.DELIVERY_HOMEPHONE1.focus();	  
                return (false); 
               }
            }         	 
        }
       
        with (theForm.DELIVERY_HOMEPHONE2)
        {
 	    var newValue = (value);
 	    var newLength = (value.length);
 	    var newFlag = 0;
	
	    for(var i = 0; i != newLength; i++) 
 	    { 	
  	     aChar = newValue.substring(i,i+1);
  	     if(aChar < "0" || aChar > "9" || newLength != 3) 
  	       {
  	        alert("Please enter a value for the \"Home or Work telephone\"  option.");  theForm.DELIVERY_HOMEPHONE2.focus();	  
                return (false); 
               }
            }         	 
        }

        with (theForm.DELIVERY_HOMEPHONE3)
        {
 	    var newValue = (value);
 	    var newLength = (value.length);
 	    var newFlag = 0;
	
	    for(var i = 0; i != newLength; i++) 
 	    { 	
  	     aChar = newValue.substring(i,i+1);
  	     if(aChar < "0" || aChar > "9" || newLength != 4) 
  	       {
  	        alert("Please enter a value for the \"Home or Work telephone\"  option.");  theForm.DELIVERY_HOMEPHONE3.focus();	  
                return (false); 
               }
            }         	 
        }	   

 
}