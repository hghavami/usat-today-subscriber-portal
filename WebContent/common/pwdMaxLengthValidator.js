// pwdMaxLengthValidator.js
// To use these functions:
// Add the following lines to between the <HEAD></HEAD> tags
//  <SCRIPT LANGUAGE="JavaScript" SRC="/common/pwdMaxLengthValidator.js"></SCRIPT>  
//
//  Sample useage:  <input type="password" maxlength="30" name="DELIVERY_PASSWORD" size="30" onkeyup="maxPwdChars(this, 30)" >
//

var pwdLength = 0;
var confirmLength = 0;
var pwdShown = false;
var confirmShown = false;

function resetFlag() {
	pwdShown = false;
	confirmShown = false;
}

function maxPwdChars(field, maximum) {
	if ((pwdLength == field.value.length) && field.value.length == maximum  ) {
		if (pwdShown == false) {
			pwdShown = true;
			alert('Please enter a password between 5 and 30 alphanumeric with no spaces or special characters.');		
			field.focus();
			setTimeout("resetFlag()", 2500);
		}
	} 
	else {
		pwdLength = field.value.length;
	}
}
function maxConfirmPwdChars(field, maximum) {
	if ((confirmLength == field.value.length) && field.value.length == maximum  ) {
		if (confirmShown == false) {
			confirmShown = true;
			alert('Please enter a confirmation password between 5 and 30 alphanumeric with no spaces or special characters.s.');
			field.focus();
			setTimeout("resetFlag()", 2500);
		}
	} 
	else {
		confirmLength = field.value.length;
	}
}
// -->