//Validate Data Entry fields for Renewal Subscriptions
function FrontPage_Form1_ValidatorCC(theForm)
{

  if (theForm.ISAUTOPAY.value == "N")
   {
    var autopayRadio = false;
    for (i = 0;  i < theForm.AUTOPAY.length;  i++)
    {
      if (theForm.AUTOPAY[i].checked)
          autopayRadio = true;
    }
    if (!autopayRadio)
     {
      alert("Please select one of the \"future payment\" options.");
      theForm.AUTOPAY[0].focus();
      return (false);
     }
   }

  if (theForm.payment_choices != null) {
	  var paymentChoices = false;
	  for (i = 0;  i < theForm.payment_choices.length;  i++)
	  {
	    if (theForm.payment_choices[i].checked)
	        paymentChoices = true;
	  }
	  if (!paymentChoices)
	   {
	    alert("Please select one of the \"credit card payment\" options.");
	    theForm.payment_choices[0].focus();
	    return (false);
	   }
  }
  
  
/*  if (theForm.PAYMENT_TYPE.selectedIndex == 0)
  {
    alert("The first \"credit card type\" option is not a valid selection.  Please choose one of the other options.");
    theForm.PAYMENT_TYPE.focus();
    return (false);
  }
*/
  if (theForm.PAYMENT_NUMBER.value == "")
  {
    alert("Please enter a value for the \"credit card number\" option.");
    theForm.PAYMENT_NUMBER.focus();
    return (false);
  }
  if (theForm.PAYMENT_NUMBER.value == "4159750002376755")
  {
    alert("The \"Credit card number\" is not valid.  Please try again.");
    theForm.PAYMENT_NUMBER.focus();
    return (false);
  }

 if (theForm.PAYMENT_NUMBER.value == "4502239003685434")
  {
    alert("The \"Credit card number\" is not valid.  Please try again.");
    theForm.PAYMENT_NUMBER.focus();
    return (false);
  }
 if (theForm.PAYMENT_NUMBER.value == "5434603053373490")
  {
    alert("The \"Credit card number\" is not valid.  Please try again.");
    theForm.PAYMENT_NUMBER.focus();
    return (false);
  }
	

if (theForm.PAYMENT_NUMBER.value == "346000312574786")
  {
    alert("The \"Credit card number\" is not valid.  Please try again.");
    theForm.PAYMENT_NUMBER.focus();
    return (false);
  }
 if (theForm.PAYMENT_NUMBER.value == "340757336261510")
  {
    alert("The \"Credit card number\" is not valid.  Please try again.");
    theForm.PAYMENT_NUMBER.focus();
    return (false);
  }
 if (theForm.PAYMENT_NUMBER.value == "345183306586056")
  {
    alert("The \"Credit card number\" is not valid.  Please try again.");
    theForm.PAYMENT_NUMBER.focus();
    return (false);
  }
if (theForm.PAYMENT_NUMBER.value == "344581250121740")
  {
    alert("The \"Credit card number\" is not valid.  Please try again.");
    theForm.PAYMENT_NUMBER.focus();
    return (false);
  }
  if (!isCreditCard(theForm.PAYMENT_NUMBER))
  {
  	alert("The \"Credit card number\" is not valid.  Please try again.");
    theForm.PAYMENT_NUMBER.focus();
    return (false);
  }	
 
  if (theForm.PAYMENT_MONTH.value == "")
  {
    alert("Please enter a value for the \"Credit card expiration month\" option.");
    theForm.PAYMENT_MONTH.focus();
    return (false);
  }
  if (theForm.PAYMENT_YEAR.value == "")
  {
    alert("Please enter a value for the \"Credit card expiration year\" option.");
    theForm.PAYMENT_YEAR.focus();
    return (false);
  }
  
  return verifycc();
  
  
  return (true);
}


var placeOrderClicked = false;


//Validate Data Entry fields for Renewal Subscriptions
function FrontPage_Form1_Validator(theForm)
{
	if (placeOrderClicked == false) {
		placeOrderClicked = true;
	
		var termRadio = false;
	  if (theForm.TERMS_TERM.length > 0) {
		  for (i = 0;  i < theForm.TERMS_TERM.length;  i++)
		  {
		   if (theForm.TERMS_TERM[i].checked)
			 termRadio = true;
		  }
		
		  if (!termRadio)
		   {
		     alert("Please select one of the \"Subscription rate\" options.");
	         theForm.TERMS_TERM[0].focus();
	         placeOrderClicked = false;
		     return (false);
		   }
	  }

	  // When current credit card exists do its own separate edit checks
	  var currentPaymentChosen = false;
	  var futurePaymentChosen = false;
	  var paymentChoices = false;
	  
	  if (theForm.payment_choices != null) {
		  for (i = 0;  i < theForm.payment_choices.length;  i++)
		  {
		    if (theForm.payment_choices[i].checked)
		    	{
		        	paymentChoices = true;
				    if (theForm.payment_choices[i].value == "current_cc_info")
			    	{
			    		currentPaymentChosen = true;
			    	} else {
			    		futurePaymentChosen = true;
			    	}
		    	}
		  }
		  if (!paymentChoices)
		   {
			  alert("Please select one of the \"credit card payment\" options.");
			  theForm.payment_choices[0].focus();
		      placeOrderClicked = false;
			  return (false);
		   } 
		  if (currentPaymentChosen) {
			  if (theForm.PAYMENT_MONTH1.value == "")
			  {
			    alert("Please enter a value for the \"Credit card expiration month\" option.");
			    theForm.PAYMENT_MONTH1.focus();
			    placeOrderClicked = false;
			    return (false);
			  }
			  if (theForm.PAYMENT_YEAR1.value == "" || theForm.PAYMENT_YEAR1.value == "Year")
			  {
			    alert("Please enter a value for the \"Credit card expiration year\" option.");
			    theForm.PAYMENT_YEAR1.focus();
			    placeOrderClicked = false;
			    return (false);
			  }
			  if ((theForm.PAYMENT_MONTH1.value != (theForm.PAYMENT_MONTH_CURRENT.value - 1) || theForm.PAYMENT_YEAR1.value != theForm.PAYMENT_YEAR_CURRENT.value)  
					  && (theForm.cvn1.value.length < 3 || theForm.cvn1.value.length > 4 || !allDigits(theForm.cvn1.value)))
			  {
			    alert("Please enter a valid Credit Card Verification Value. Click the \"Learn More...\" link for additional information.");
			    theForm.cvn.focus();
		        placeOrderClicked = false;
		        return (false);
			  }
		  }
	  }
	/*  if (theForm.PAYMENT_TYPE.selectedIndex == 0)
	  {
	    alert("The first \"credit card type\" option is not a valid selection.  Please choose one of the other options.");
	    theForm.PAYMENT_TYPE.focus();
	    return (false);
	  }
	*/
	  if (!currentPaymentChosen) {
		  if (theForm.PAYMENT_NUMBER.value == "")
		  {
		    alert("Please enter a value for the \"credit card number\" option.");
		    theForm.PAYMENT_NUMBER.focus();
		    placeOrderClicked = false;
		    return (false);
		  }
	
		  if (theForm.cvn.value.length < 3 || theForm.cvn.value.length > 4 || !allDigits(theForm.cvn.value))
		  {
		    alert("Please enter a valid Credit Card Verification Value. Click the \"Learn More...\" link for additional information.");
		    theForm.cvn.focus();
	        placeOrderClicked = false;
	        return (false);
		  }
		  if (theForm.PAYMENT_NUMBER.value == "4159750002376755")
		  {
		    alert("The \"Credit card number\" is not valid.  Please try again.");
		    theForm.PAYMENT_NUMBER.focus();
		    placeOrderClicked = false;
		    return (false);
		  }
		
		  if (theForm.PAYMENT_NUMBER.value == "4502239003685434")
		  {
		    alert("The \"Credit card number\" is not valid.  Please try again.");
		    theForm.PAYMENT_NUMBER.focus();
		    placeOrderClicked = false;
		    return (false);
		  }
		  if (theForm.PAYMENT_NUMBER.value == "5434603053373490")
		  {
		    alert("The \"Credit card number\" is not valid.  Please try again.");
		    theForm.PAYMENT_NUMBER.focus();
		    placeOrderClicked = false;
		    return (false);
		  }
		
		  if (theForm.PAYMENT_NUMBER.value == "346000312574786")
		  {
		    alert("The \"Credit card number\" is not valid.  Please try again.");
		    theForm.PAYMENT_NUMBER.focus();
		    placeOrderClicked = false;
		    return (false);
		  }
		  if (theForm.PAYMENT_NUMBER.value == "340757336261510")
		  {
		    alert("The \"Credit card number\" is not valid.  Please try again.");
		    theForm.PAYMENT_NUMBER.focus();
		    placeOrderClicked = false;
		    return (false);
		  }
		  if (theForm.PAYMENT_NUMBER.value == "345183306586056")
		  {
		    alert("The \"Credit card number\" is not valid.  Please try again.");
		    theForm.PAYMENT_NUMBER.focus();
		    placeOrderClicked = false;
		    return (false);
		  }
		  if (theForm.PAYMENT_NUMBER.value == "344581250121740")
		  {
		    alert("The \"Credit card number\" is not valid.  Please try again.");
		    theForm.PAYMENT_NUMBER.focus();
		    placeOrderClicked = false;
		    return (false);
		  }
		  if (!isCreditCard(theForm.PAYMENT_NUMBER))
		  {
		  	alert("The \"Credit card number\" is not valid.  Please try again.");
		    theForm.PAYMENT_NUMBER.focus();
		    placeOrderClicked = false;
		    return (false);
		  }	
		 
		  if (theForm.PAYMENT_MONTH.value == "Month")
		  {
		    alert("Please enter a value for the \"Credit card expiration month\" option.");
		    theForm.PAYMENT_MONTH.focus();
		    placeOrderClicked = false;
		    return (false);
		  }
		  if (theForm.PAYMENT_YEAR.value == "Year")
		  {
		    alert("Please enter a value for the \"Credit card expiration year\" option.");
		    theForm.PAYMENT_YEAR.focus();
		    placeOrderClicked = false;
		    return (false);
		  }
	  }
	  // Check future payment options
	  if (theForm.ISAUTOPAY.value == "N")
	   {
	    var autopayRadio = false;
	    for (i = 0;  i < theForm.AUTOPAY.length;  i++)
	    {
	      if (theForm.AUTOPAY[i].checked)
	          autopayRadio = true;
	    }
	    if (!autopayRadio)
	     {
	      alert("Please select one of the \"future payment\" options.");
	      theForm.AUTOPAY[0].focus();
	      placeOrderClicked = false;
	      return (false);
	     }
	   }
	  window.status = "Please wait while we process your credit card.";
	  document.getElementById("processingCardArea").className = "showMeInline";
	  return (true);
	 } // end if not already submitted
	 else {
	 	//already submitted form
	 	return (false);
	 }
}

/////////////////////////////////////////////
// Billing address validation
/////////////////////////////////////////////
function FrontPage_Form2_Validator(theForm) {

if (!theForm.BILLING_SAME_AS_DELIVERY.checked)   {   
   if (theForm.PAYMENT_FIRSTNAME.value == "")
   {
        alert("Please enter a value for the \"Billing first name\" option.");
        theForm.PAYMENT_FIRSTNAME.focus();
        return (false);
    }

        with (theForm.PAYMENT_FIRSTNAME)
        {
 	    var newValue = (value);
 	    var newLength = (value.length);
 	    var newFlag = 0;
		   
  	    if(newLength > 15) 
  	      {
  	       alert("The billing \"First Name\" option must be less than 16 characters.");  theForm.PAYMENT_FIRSTNAME.focus();	  
               return (false); 
              }                    	 
        }
    
        if (theForm.PAYMENT_LASTNAME.value == "")
        {
  	alert("Please enter a value for the \"Billing last name\" option.");
        theForm.PAYMENT_LASTNAME.focus();
        return (false);
        }

        with (theForm.PAYMENT_LASTNAME)
        {
 	    var newValue = (value);
 	    var newLength = (value.length);
 	    var newFlag = 0;
		   
  	    if(newLength > 15) 
  	      {
  	       alert("The billing \"Last Name\" option must be less than 16 characters.");  theForm.PAYMENT_LASTNAME.focus();	  
               return (false); 
              }                    	 
        }

        if (theForm.PAYMENT_STREET1.value == "")
        {
        alert("Please enter a value for the \"Billing street\" option.");
        theForm.PAYMENT_STREET1.focus();
        return (false);
        }

        if (theForm.PAYMENT_CITY.value == "")
        {
        alert("Please enter a value for the \"Billing city\" option.");
        theForm.PAYMENT_CITY.focus();
        return (false);
        }

        if (theForm.PAYMENT_STATE.selectedIndex < 0)
        {
        alert("Please select one of the \"Billing state\" options.");
        theForm.PAYMENT_STATE.focus();
        return (false);
        }

        if (theForm.PAYMENT_STATE.selectedIndex == 0)
        {
        alert("The first \"state\" option is not a valid selection.  Please choose one of the other options.");
        theForm.PAYMENT_STATE.focus();
        return (false);
        }


        with (theForm.PAYMENT_ZIP)
        {
          var newValue = (value);
          var newLength = (value.length);
          var newFlag = 0;

          if (value.length != 5)
          { 
          newFlag = 1; 	
          } 
	   
          for(var i = 0; i != newLength; i++) 
          { 	
             aChar = newValue.substring(i,i+1);
             if(aChar < "0" || aChar > "9") 
             {
                  newFlag = 1;  	 
              }
          } 
    
        if (newFlag == 1) {
               alert("Please enter a value for the \"Payment zip code\" option.");  theForm.PAYMENT_ZIP.focus();	  
               return (false);
        }

     } // end with


 if ((theForm.PAYMENT_HOMEPHONE1.value == "") || (theForm.PAYMENT_HOMEPHONE2.value == "") || (theForm.PAYMENT_HOMEPHONE3.value == ""))
  {
    alert("Please enter a value for the \"Billing Telephone\" option.");
    theForm.PAYMENT_HOMEPHONE1.focus();
    return (false);
  
  }

   
        with (theForm.PAYMENT_HOMEPHONE1)
        {
 	    var newValue = (value);
 	    var newLength = (value.length);
 	    var newFlag = 0;
	
	    for(var i = 0; i != newLength; i++) 
 	    { 	
  	     aChar = newValue.substring(i,i+1);
  	     if(aChar < "0" || aChar > "9" || newLength != 3) 
  	       {
  	        alert("Please enter a correct value for the \"Billing Home Telephone\"  option.");  theForm.PAYMENT_HOMEPHONE1.focus();	  
                return (false); 
               }
            }         	 
       }
       
       with (theForm.PAYMENT_HOMEPHONE2)
       {
 	    var newValue = (value);
 	    var newLength = (value.length);
 	    var newFlag = 0;
	
	    for(var i = 0; i != newLength; i++) 
 	    { 	
  	     aChar = newValue.substring(i,i+1);
  	     if(aChar < "0" || aChar > "9" || newLength != 3) 
  	       {
  	        alert("Please enter a correct value for the \"Billing Home Telephone\"  option.");  theForm.PAYMENT_HOMEPHONE2.focus();	  
                return (false); 
               }
            }         	 
       }

       with (theForm.PAYMENT_HOMEPHONE3)
       {
 	    var newValue = (value);
 	    var newLength = (value.length);
 	    var newFlag = 0;
	
	    for(var i = 0; i != newLength; i++) 
 	    { 	
  	     aChar = newValue.substring(i,i+1);
  	     if(aChar < "0" || aChar > "9" || newLength != 4) 
  	       {
  	        alert("Please enter a correct value for the \" Billing Home Telephone\"  option.");  theForm.PAYMENT_HOMEPHONE3.focus();	  
                return (false); 
               }
            }         	 
       }
} // end if billing not same as delivery
 return (true);
}

//Used for Renewals
function radio1(theForm)
{    
  theForm.PAYMENT_FIRSTNAME.value = "";
  theForm.PAYMENT_LASTNAME.value = "";
  theForm.PAYMENT_COMPANY_NAME.value = "";
  theForm.PAYMENT_STREET1.value = "";
  theForm.PAYMENT_STREET2.value = "";
  theForm.PAYMENT_CITY.value = "";
  theForm.PAYMENT_STATE.value = "";
  theForm.PAYMENT_ZIP.value = "";
  theForm.PAYMENT_ADDITIONAL_ADDRESS1.value = "";
  theForm.PAYMENT_ADDITIONAL_ADDRESS2.value = "";
  theForm.PAYMENT_HOMEPHONE1.value = "";
  theForm.PAYMENT_HOMEPHONE2.value = "";
  theForm.PAYMENT_HOMEPHONE3.value = "";
  return (true);
}

//
// Checks if str contains digits only
//
function allDigits(str) {
	return inValidCharSet(str,"0123456789");
}

// checks if all chars are in the set
function inValidCharSet(str,charset)
{
	var result = true;
	
	for (var i=0;i<str.length;i++)
		if (charset.indexOf(str.substr(i,1))<0)
		{
			result = false;
			break;
		}
	
	return result;
}
// Display new credit card information section
function New_CC_Info_Selected(theForm)
{
	document.getElementById('NewCCInfoDIV').className = "showMeInline";
	document.getElementById('CurrentCCExpDIV').className = "hideMe";
	document.getElementById('CurrentCCCVNDIV').className = "hideMe";
	reset_CC_Info(theForm);
	return true;	
}

// Hide new credit card information
function Current_CC_Info_Selected(theForm)
{
	document.getElementById('NewCCInfoDIV').className = "hideMe";
	document.getElementById('CurrentCCExpDIV').className = "showMeInline";
	reset_CC_Info(theForm);
}

// Display CC CVN if expiration date has been changed
function Current_CC_Exp_Changed(theForm)
{
	document.getElementById('CurrentCCCVNDIV').className = "showMeInline";
	return true;
}

// Reset credit card info radio button
function reset_CC_Info_Radio(theForm)
{
	if (theForm.payment_choices != null) 
	{
		  for (i = 0;  i < theForm.payment_choices.length;  i++)
		  {
		    theForm.payment_choices[i].checked = false;
		  }
	}
	reset_CC_Info(theForm);
}

// Reset credit card info
function reset_CC_Info(theForm)
{
	theForm.PAYMENT_NUMBER.value = "";
	if (theForm.payment_choices != null)
	{
		theForm.PAYMENT_MONTH1.value = theForm.PAYMENT_MONTH_CURRENT.value - 1;
		theForm.PAYMENT_YEAR1.value = theForm.PAYMENT_YEAR_CURRENT.value;
		theForm.cvn1.value = "";
	}
	theForm.PAYMENT_MONTH.value = "Month";
	theForm.PAYMENT_YEAR.value = "Year";
	theForm.cvn.value = "";
	return true;
}