// keepalive.js

//apporximately 30 minutes for each keep alive 3= 1.5 hours
var MAX_KEEP_ALIVES = 4;  
var keepAliveCount = 1;

function startKeepAlive() {
	keepAlive();
}


// newFunction
// following method is used to keep the session alive.
function keepAlive() {

	var url = "/include/keepAlive.jsp";
	var ajaxReq = initRequest(url);
	ajaxReq.onreadystatechange = function() {
		
		if (ajaxReq.readyState == 4) {
			if (ajaxReq.status == 200) {
			
				// reset timer for 25 minutes 1000ms * 60 * 25 = 1500000
				if (keepAliveCount < MAX_KEEP_ALIVES) {
					setTimeout('keepAlive()', 1500000);
					keepAliveCount++;
				}
				else {
					// send to welcome page if expiring
					document.location = '/welcome.jsp';
				}
			}
		}
	};

    ajaxReq.open("GET", url, true);
	ajaxReq.send(null);
}


function initRequest(url) {
   if (window.XMLHttpRequest) {
       return new XMLHttpRequest();
   } else if (window.ActiveXObject) {
       return new ActiveXObject("Microsoft.XMLHTTP");
   }
}