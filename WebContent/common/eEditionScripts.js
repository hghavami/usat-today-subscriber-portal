

function initAjaxRequest(url) {
       if (window.XMLHttpRequest) {
           return new XMLHttpRequest();
       } else if (window.ActiveXObject) {
           isIE = true;
           return new ActiveXObject("Microsoft.XMLHTTP");
       }
}

function trackOpenEReader() {
	   var url = "/recordEReaderOpen.do";
	   
       var req = initAjaxRequest(url);
       req.onreadystatechange = function() {
           if (req.readyState == 4) {
               if (req.status == 200) {
                   return;
               } 
           }
       };
       
       // change to "GET" to only send once per cache, "POST" for every click
       req.open("POST", url, true);
       req.send(null);
}

function openReaderWindow() {
	  
	 trackOpenEReader();

	 var width = screen.width - 30;
	 var height = screen.height - 100;
	 	 
	 window.moveTo(5,5);
	 window.resizeTo(width, height);
	 	 
	 return true;
}

