// This script requires inclusiong of the Prototype Library and Scriptaculous libriaries
// The additional scripts should be included in the page using these methods.
// 
// <script src="/common/prototype/scriptaculous/scriptaculous.js?load=effects,dragdrop" type="text/javascript"></script>
// <script language="JavaScript" src="/common/prototype/prototype.js"></script>
//
// Add the following 
//  Add the following javascript methods to the page if they do not exist on the page, otherwise, just add the lines of code if it does exist.
//
//  function initPage() {
//	  try {
//		var showOverlayField = $('formOrderEntryForm:showOverlayPopUp');
//	
//		if (showOverlayField.value == "true") {
//			keyCodeOverlayEnabled = true;
//		}
//	  }
//	    catch (err) {
//	  } 
//  }
//
//  function onbeforeunloadPage(thisObj, thisEvent) {
//	     if( popOffer_programEnabled) {
//	     	return popUpOnBeforeUnload(thisObj, thisEvent);
//           }
//		
//  }
//
//  function unloadPage(thisObj, thisEvent) {
// 	   popOffer_unloadingPage = true;
//	   closeSpecialOffer(thisObj, thisEvent);
//  }

	
// to manually turn off pop up overlays for all offers change the programEnambled flag to false
var popOffer_programEnabled = true;
var showPopOffer = true;
var popOffer_unloadingPage = false;
// next attribute used to enable/disable at key code level. Updated on JSF page.
var keyCodeOverlayEnabled = false;
var usat_productName = "USA TODAY";

// Extend the Element class methods

// Set left such that object is centered left to right on the page
Element.CenterLeftRight = function(element, parent) {

    var w, h, pw, ph;
    var d = Element.getDimensions(element);
    w = d.width;

    if (!parent) {
          pw = document.viewport.getWidth();
            
    } else {
            pw = parent.offsetWidth;
    }
    
   var left = ((pw/2) - (w/2));
         
   $(element).style.left = left + 'px';           
};

// Set left and top such that object is centered on page.
// Set left and top such that object is centered on page.
Element.CenterOnPage = function(element, parent) {

    var w, h, pw, ph;
    var d = Element.getDimensions(element);
    w = d.width;
    h = d.height;

    if (!parent) {
          pw = document.viewport.getWidth();
          ph = document.viewport.getHeight();
            
    } else {
            pw = parent.offsetWidth;
            ph = parent.offsetHeight;
    }
    
   var scrollVariance = document.viewport.getScrollOffsets();
	
   var top = ((ph/2) - (h/2)) + scrollVariance[1];
   var left = ((pw/2) - (w/2)) + scrollVariance[0];
   
   $(element).style.top = top + 'px';      
   $(element).style.left = left + 'px';           
};

Element.CenterOnPageV2 = function(element) {
     if($(element) != null) {
          if(typeof window.innerHeight != 'undefined') {
               $(element).style.top =  Math.round(document.viewport.getScrollOffsets().top + 
                    ((window.innerHeight - $(element).getHeight()))/2)+'px';
               $(element).style.left =   Math.round(document.viewport.getScrollOffsets().left +
                     ((window.innerWidth - $(element).getWidth()))/2)+'px';
          } else {
               $(element).style.top =  Math.round(document.body.scrollTop + 
                    (($$('body')[0].clientHeight - $(element).getHeight()))/2)+'px';
               $(element).style.left =  Math.round(document.body.scrollLeft + (($$('body')[0].clientWidth - $(element).getWidth()))/2)+'px';
          }
     }
};
     
// Depends on Prototype being loaded too.
//
Event.observe(window, 'load', function() {

	popOffer_unloadingPage = false;

	if (popOffer_programEnabled) {	
		Event.observe(document.body, 'click', function userClicked(event) {
				showPopOffer = false;
		});
	
		Event.observe(document.body, 'mousemove', userMovedMouse);

		Event.observe(window, 'scroll', redisplayPopOffer);
		Event.observe(window, 'resize', redisplayPopOffer);
	}
	
});

function userMovedMouse(event) {
	showPopOffer = true;
}

function redisplayPopOffer(event) {
	var popUnderDiv = $('popunder');
	var popAreaDiv = $('leavingofferdiv');
	
	try {
		if (popOffer_programEnabled && popAreaDiv.visible()) {
			popUnderDiv.show();
	
			popAreaDiv.hide();
	
			Element.CenterOnPage(popAreaDiv);
		
			Effect.Appear(popAreaDiv, {duration:0.1});		
		}
	}
	catch (err) {
		// ignore
	}
}

function showSpecialOffer() {

	if (popOffer_programEnabled && !popOffer_unloadingPage ) {
	    var popUnderDiv = $('popunder');
	    var popAreaDiv = $('leavingofferdiv');
	     
		popUnderDiv.show();
	

	        Element.CenterOnPage(popAreaDiv);
	
	    Effect.Appear(popAreaDiv, {duration:0.3});

	}
}
 
function closeSpecialOffer(thisObj, thisEvent) {
	try {
		$('leavingofferdiv').hide();
		$('popunder').hide();
	}
	catch (err) {
		 // ignore
	}
	return false;
}

function popUpOnBeforeUnload(thisObj, thisEvent) {
    
	if (showPopOffer == true ) { 
		
		var messageText = "Choose to stay on this page to see a " + usat_productName + " special offer.";
		
		thisEvent.returnValue = messageText;
		
		setTimeout("showSpecialOffer()", 300);
		return messageText;
	}
	else {
		return "";
	}
}

///////////////////////////////////////////
// For non JSF pages
//////////////////////////////////////////
function initOverlayPop() {
	try {
		keyCodeOverlayEnabled = true;
		
	}
	catch (err) {
	} 
}

function onbeforeunloadPageOverlayPop(thisObj, thisEvent) {
	if( popOffer_programEnabled && keyCodeOverlayEnabled) {
		return popUpOnBeforeUnload(thisObj, thisEvent);
	}
}

function unloadOverlayPop(thisObj, thisEvent) {
	if (popOffer_programEnabled && keyCodeOverlayEnabled) {
		popOffer_unloadingPage = true;
		closeSpecialOffer(thisObj, thisEvent);
	}
}