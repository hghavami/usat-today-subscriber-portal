// usatCommon.js

Event.observe(window, 'load', function(event) {

	try {
		_initPage(null, event);
	} catch (e) {
		// todo: handle exception
	}

	Event.observe(window, 'unload', function(event) {
		_unloadPage(null, event);
	});

	Event.observe(window, 'beforeunload', function(event) {
		_onBeforeUnloadPage(null, event);
	});

});

// Page loading/unloading functions
var chatWindowDivElement;
var chatWindowGuestDivElement;

// Page loading/unloading functions
function _initPage(thisObj, thisEvent) {
	// insert site-wide code here

	try {
		chatWindowDivElement = $('templateChatWindowDiv');

		new Draggable(chatWindowDivElement);

		setTimeout('templateClickToChatMouseOver()', 180000);
	} catch (e) {
		; // ignore
	}

	try {
		chatWindowGuestDivElement = $('templateChatWindowGuestDiv');

		new Draggable(chatWindowDivGuestElement);

		setTimeout('templateClickToChatMouseOverGuest()', 180000);
	} catch (e) {
		; // ignore
	}

	// you typically would run this after all site-wide
	// execution has processed, but it's really up to
	// the design of your site to when you may want to run this
	if (typeof window.initPage == "function") {
		window.initPage(thisObj, thisEvent);
	}

}

function _unloadPage(thisObj, thisEvent) {
	// insert site-wide code here

	// you typically would run this after all site-wide
	// execution has processed, but it's really up to
	// the design of your site to when you may want to run this
	if (typeof window.unloadPage == "function") {
		window.unloadPage(thisObj, thisEvent);
	}

}

function _onBeforeUnloadPage(thisObj, thisEvent) {
	// insert site-wide code here

	// you typically would run this after all site-wide
	// execution has processed, but it's really up to
	// the design of your site to when you may want to run this
	if (typeof window.onBeforeUnloadPage == "function") {
		return window.onBeforeUnloadPage(thisObj, thisEvent);
	}
	return "";
}

function _subscribeToPub() {
	if (typeof window.subscribeToPub == "function") {
		window.subscribeToPub();
	} else {
		document.location = "/subscriptions/order/checkout.faces";
	}
}

function _subscribeToBW() {
	document.location = "/subscriptions/order/checkout.faces?pub=BW";
}

function _giveAGift() {
	document.location = "/subscriptions/order/checkout.faces";
}

function _subscribeByMail() {
	if (typeof window.subscribeByMail == "function") {
		window.subscribeByMail();
	} else {
		// usatoday redirect
		document.location = "/subscriptions/subscribebymail.html";
	}
}

function _subscribeToElectronicEdition() {

	if (typeof window.subscribeToElectronicEdition == "function") {
		window.subscribeToElectronicEdition();
	} else {
		document.location = "/subscriptions/electronic/orderEntry.faces";
	}
}

function _subscribeToElectronicEditionOrderPage() {

	if (typeof window.subscribeToElectronicEditionOrderPage == "function") {
		window.subscribeToElectronicEditionOrderPage();
	} else {
		document.location = "/subscriptions/order/checkout.faces?pub=EE";
	}
}

function _subscribeToInternationalEdition() {
	if (typeof window.subscribeToInternationalEdition == "function") {
		window.subscribeToInternationalEdition();
	} else {
		document.location = "/international/welcomeint.jsp";
	}
}

function _shopUSAT() {
	if (typeof window.shopUSAT == "function") {
		window.shopUSAT();
	} else {
		document.location = "http://onlinestore.usatoday.com";
	}
}

function _loadFAQ() {
	if (typeof window.loadFAQ == "function") {
		window.loadFAQ();
	} else {
		window
				.open(
						'/faq/index.jsp',
						'FAQ',
						'width=825,height=800,menubar=no,titlebar=yes,scrollbars=yes,status=no,location=no,directories=no');
	}
}

function func_Logout(thisObj, thisEvent) {
	document.location = '/index.jsp?logsessobj=rem';
}

function respondToFlashClickEvent(whereToGo) {
	if (typeof whereToGo == 'string' && whereToGo.length > 0) {
		document.location = whereToGo;
	}
}

function templateLogoClick() {
	document.location = "/welcome.jsp";
}

function mobileNavChanged(thisObj, thisEvent) {

	if (thisObj.value != "noop") {
		document.location = thisObj.value;
	}
}
function mobileNavChangedV2(thisObj, thisEvent, url) {

	if (url != "noop") {
		document.location = url;
	}
}


var templateShowChatOnMouseOver = true;
var checkoutGuaranteeOnMouseOver = true;
var usat_lastTimeStamp = 0;
// call so following require scriptaculous

function templateClickToChatClicked(thisObj, thisEvent) {

	try {

		if (chatWindowDivElement.visible()
				|| chatWindowGuestDivElement.visible()) {
			// hide them if already visible.
			if ((chatWindowDivElement.visible())) {
				setTimeout('templateClickToChatHide()', 1000);
			}
			if ((chatWindowGuestDivElement.visible())) {
				setTimeout('templateClickToChatHideGuest()', 1000);
			}
		} else {
			// otherwise shot it
			Effect.Grow('templateChatWindowDiv', {
				duration : 0.75
			});
			chatWindowDivElement.setOpacity(1.0);

		}

	} catch (e) {
	}
	return false;
}

function checkoutGuarantee(thisObj, thisEvent) {

	if ((thisEvent.timeStamp - usat_lastTimeStamp) < 1000) {
		return false;
	}

	usat_lastTimeStamp = thisEvent.timeStamp;

	try {
		if (!(checkoutGuaranteeDivElement.visible())) {
			Effect.Grow('checkoutGuaranteeDiv', {
				duration : 0.75
			});
			checkoutGuaranteeDivElement.setOpacity(1.0);

		} else {
			if ((checkoutGuaranteeDivElement.visible())) {
				setTimeout('checkoutGuarantee()', 1000);
			}
		}
	} catch (e) {
	}

	return false;
}

function templateClickToChatClickedGuest(thisObj, thisEvent) {

	try {
		if (chatWindowDivElement.visible()
				|| chatWindowGuestDivElement.visible()) {
			// hide them if already visible.
			if ((chatWindowDivElement.visible())) {
				setTimeout('templateClickToChatHide()', 1000);
			}
			if ((chatWindowGuestDivElement.visible())) {
				setTimeout('templateClickToChatHideGuest()', 1000);
			}
		} else {
			// otherwise shot it
			Effect.Grow('templateChatWindowGuestDiv', {
				duration : 0.75
			});
			chatWindowGuestDivElement.setOpacity(1.0);
		}

	} catch (e) {
	}

	return false;
}

function templateClickToChatMouseOver(thisObj, thisEvent) {

	try {
		if (!(chatWindowDivElement.visible()) && templateShowChatOnMouseOver) {
			usat_lastTimeStamp = thisEvent.timeStamp;
			Element.CenterOnPage(chatWindowDivElement);
			Effect.Grow('templateChatWindowDiv', {
				duration : 0.75
			});
			chatWindowDivElement.setOpacity(1.0);
		}
		return false;
	} catch (e) {
	}
	return false;
}

function templateClickToChatMouseOverGuest(thisObj, thisEvent) {

	try {
		if (!(chatWindowDivElement.visible()) && templateShowChatOnMouseOver) {
			usat_lastTimeStamp = thisEvent.timeStamp;
			Element.CenterOnPage(chatWindowDivElement);
			Effect.Grow('templateChatWindowGuestDiv', {
				duration : 0.75
			});
			chatWindowGuestDivElement.setOpacity(1.0);
		}
		return false;
	} catch (e) {
	}
	return false;
}
function checkoutGuaranteeHide() {
	try {
		checkoutGuaranteeOnMouseOver = false;
		Effect.Shrink('checkoutGuaranteeDiv');
		return false;
	} catch (e) {
		return false;
	}
}
function templateClickToChatHideGuest() {
	try {
		templateShowChatOnMouseOver = false;
		Effect.Shrink('templateChatWindowGuestDiv');
		return false;
	} catch (e) {
		return false;
	}
}

function templateClickToChatHide() {
	try {
		templateShowChatOnMouseOver = false;
		Effect.Shrink('templateChatWindowDiv');
		return false;
	} catch (e) {
		return false;
	}
}

Element.CenterOnPage = function(element, parent) {

	var w, h, pw, ph;
	var d = Element.getDimensions(element);
	w = d.width;
	h = d.height;

	if (!parent) {
		pw = document.viewport.getWidth();
		ph = document.viewport.getHeight();

	} else {
		pw = parent.offsetWidth;
		ph = parent.offsetHeight;
	}

	var scrollVariance = document.viewport.getScrollOffsets();

	var top = ((ph / 2) - (h / 2)) + scrollVariance[1];
	// var left = ((pw/2) - (w/2)) + scrollVariance[0];
	var left = ((pw / 2) - (w / 2));

	$(element).style.top = top + 'px';
	$(element).style.left = left + 'px';
};

// ////////////////////////////////////////////
// Order Entry/Renewal page methods
var usatOE_showRenewalOptions = true;
var usatOE_selectedTermRequiresEZPay = false;

function usatOE_updateDeliveryInfoPanelClass() {

	try {
		var checkbox = $('formOrderEntryForm:checkboxIsBillDifferentFromDelSelector');
		// gift information
		var panelEmailGiftInformation = $('formOrderEntryForm:gridBillingAddress');
		var panelOneTimeBillGift = $('formOrderEntryForm:gridEZPAYOptionsGridGift');
		var checkBoxIsGiftPayer = $('formOrderEntryForm:giftSubscriptionCheckbox');
		var panelRequiresEZPay = $('formOrderEntryForm:gridEZPAYRequiredGrid');
		var panelOneTimeBill = $('formOrderEntryForm:gridEZPAYOptionsGrid');
		panelEmailGiftInformation.hide();
		panelOneTimeBillGift.hide();
		panelOneTimeBill.hide();

		if (checkbox.checked) {
			panelEmailGiftInformation.show();
			if (usatOE_showRenewalOptions) {
				if (usatOE_selectedTermRequiresEZPay) {
					panelOneTimeBillGift.hide();
					panelOneTimeBill.hide();
					panelRequiresEZPay.show();
				} else {
					if (checkBoxIsGiftPayer.checked) {
						panelOneTimeBillGift.show();
						panelOneTimeBill.hide();
					} else {
						panelOneTimeBillGift.hide();
						panelOneTimeBill.show();
					}
					panelRequiresEZPay.hide();
				}
			}
		} else {
			panelEmailGiftInformation.hide();
			if (usatOE_showRenewalOptions) {
				if (usatOE_selectedTermRequiresEZPay) {
					panelOneTimeBillGift.hide();
					panelOneTimeBill.hide();
					panelRequiresEZPay.show();
				} else {
					panelOneTimeBillGift.hide();
					panelOneTimeBill.show();
					panelRequiresEZPay.hide();
				}
			}
		}
	} catch (e) {
	}
}

function usatOE_updatePaymentPanelClass(billMeValue) {

	try {
		var panelCreditCardInformation = $('formOrderEntryForm:gridPaymentInformationGrid');
		var panelOneTimeBillGift = $('formOrderEntryForm:gridEZPAYOptionsGridGift');
		var panelOneTimeBill = $('formOrderEntryForm:gridEZPAYOptionsGrid');

		// update ezpay selection and disclaimer
		var panelRequiresEZPay = $('formOrderEntryForm:gridEZPAYRequiredGrid');

		if (billMeValue == "B") {
			panelCreditCardInformation.hide();
			panelOneTimeBillGift.hide();
			panelOneTimeBill.hide();
			panelRequiresEZPay.hide();
		} else {
			try {
				panelCreditCardInformation.show();
			} catch (e) {
			}

			if (usatOE_selectedTermRequiresEZPay) {
				panelRequiresEZPay.show();
				panelOneTimeBill.hide();
				panelOneTimeBillGift.hide();
			} else {
				var checkbox = $('formOrderEntryForm:checkboxIsBillDifferentFromDelSelector');
				if (checkbox) {
					if (checkbox.checked) {
						if (checkBoxIsGiftPayer.checked) {
							panelOneTimeBillGift.show();
							panelOneTimeBill.hide();
						} else {
							panelOneTimeBill.show();
							panelOneTimeBillGift.hide();
						}
					}
				} else {
					panelOneTimeBill.show();
				}
			}
		}
	} catch (e) {
	}
}

function usatOE_isForceEZPayTermSelectedFunc() {
	var isEZPAY = false;

	try {
		// try to determine if a term is selcted and if so, if it requires ezpay
		var count = 0;

		var allTermsRequireEZPay = true;
		var found = false;
		while (!found) {
			var elementID = 'formOrderEntryForm:radioTermsSelection:' + count;
			var termRadioB = $(elementID);
			count = count + 1;
			if (termRadioB && (termRadioB != undefined)) {
				if (termRadioB.checked) {
					found = true;
					if (termRadioB.value.indexOf("_true") > 0) {
						isEZPAY = true;
					}
					usatOE_updateDisclaimer(termRadioB.value);
				}
				if (termRadioB.value.indexOf("_false") > 0) {
					allTermsRequireEZPay = false;
				}
			} else {
				// must be a mpf with no terms
				allTermsRequireEZPay = false;
				found = true;
			}
		} // end while
	} catch (e11111) {
		isEZPAY = false;
		allTermsRequireEZPay = false;
	}

	var returnVal = false;
	if (isEZPAY || allTermsRequireEZPay) {
		returnVal = true;
	}
	return returnVal;
}

function usatOE_updateAnimation() {
	try {
		var actionImage = document.getElementById('formSubmissionImage');
		actionImage.src = actionImage.src;
	} catch (e) {
	}
}

function usatOE_updateDisclaimer(selectedTermString) {

	var isForceBillMe = "false";
	try {
		isForceBillMe = $('formOrderEntryForm:isForceBillMe').value;
	} catch (e) {
		// no force bill me indicator so stay false
	}

	if (isForceBillMe != "true") {
		var numFreeWeeks = 0;
		
		try{ numFreeWeeks = $('formOrderEntryForm:numberEZPayFreeWeeks').value;} catch (e) {}
		var selectedTermPlusFree = $('selectedTermPlusFree');
		var selectedTermSpan = $('selectedTermSpan');
		var selectedTermSpan_2 = $('selectedTermSpan_2');
		var selectedTermPlusFree_2 = $('selectedTermPlusFree_2');
		var selectedTermPlusFree_3 = $('selectedTermPlusFree_3');
		var selectedTermPlusFree_4 = $('selectedTermPlusFree_4');

		var renewalTerm = $('renewalTerm');
		var renewalTerm_2 = $('renewalTerm_2');

		var selectedTermAmount = $('selectedTermAmount');
		var selectedTermAmount_2 = $('selectedTermAmount_2');

		try {
			var ezPayFinePrintArea = $('formOrderEntryForm:formatEZPAYFinePrint');
			ezPayFinePrintArea.show();
		} catch (e) {
		}

		try {
			// terms are in format
			// pub_keycode_ratecode_duration_amount_ezpayflag_renewalduration
			var tokens = selectedTermString.split("_");

			var term = tokens[3];

			var termAsInt = parseInt(term, 10);
			var freeWeeksAsInt = parseInt(numFreeWeeks, 10);

			if (selectedTermSpan) {
				try {
					selectedTermSpan.innerHTML = termAsInt;
				} catch (e) {
				}
			}

			if (selectedTermSpan_2) {
				try {
					selectedTermSpan_2.innerHTML = termAsInt;
				} catch (e) {
				}
			}
			
			if (selectedTermAmount) {
				try {
					selectedTermAmount.innerHTML = tokens[4];
				} catch (e) {
				}
			}

			if (selectedTermAmount_2) {
				try {
					selectedTermAmount_2.innerHTML = tokens[4];
				} catch (e) {
				}
			}

			termAsInt = termAsInt + freeWeeksAsInt; // add free weeks of EZPay

			var rTerm = tokens[6];
			var renewalTermAsInt = parseInt(rTerm, 10);

			if (renewalTerm) {
				try {
					renewalTerm.innerHTML = renewalTermAsInt;
				} catch (e) {
				}
			}

			if (renewalTerm_2) {
				try {
					renewalTerm_2.innerHTML = renewalTermAsInt;
				} catch (e) {
				}
			}

			if (selectedTermPlusFree) {
				try {
					selectedTermPlusFree.innerHTML = termAsInt;
				} catch (e) {
				}
			}

			if (selectedTermPlusFree_2) {
				try {
					selectedTermPlusFree_2.innerHTML = termAsInt;
				} catch (e) {
				}
			}

			if (selectedTermPlusFree_3) {
				try {
					selectedTermPlusFree_3.innerHTML = termAsInt;
				} catch (e) {
				}
			}

			if (selectedTermPlusFree_4) {
				try {
					selectedTermPlusFree_4.innerHTML = termAsInt;
				} catch (e) {
				}
			}

		} catch (e) {
			;//
		}

	} // end if not force bill me
}

function usatOE_selectedTermChanged(thisObj, thisEvent) {
	// use 'thisObj' to refer directly to this component instead of keyword
	// 'this'
	// use 'thisEvent' to refer to the event generated instead of keyword
	// 'event'

	// select EZ-PAY option based on term selected, attempt to update both gift
	// options and regular
	var renewalOptionRadioButton = $('formOrderEntryForm:radioRenewalOptions:0');
	var renewalOptionRadioButtonGift = $('formOrderEntryForm:radioRenewalOptionsGift:0');

	usatOE_selectedTermRequiresEZPay = false;

	try {
		// first determine if selected term requires ezpay.
		if (thisObj.value.indexOf("_true") > 0) {
			usatOE_selectedTermRequiresEZPay = true;
		}

		usatOE_updateDeliveryInfoPanelClass();
	} catch (e) {
		// if can't determine ezpay option do nothing.
		return;
	}

	try {
		// regular options
		// if term is an ezpay term
		if (usatOE_selectedTermRequiresEZPay) {
			// term requires ezpay
			renewalOptionRadioButton.checked = true;
		}
	} catch (e) {
		// ignore
	}

	try {
		// gift options
		if (usatOE_selectedTermRequiresEZPay) {
			// term requires ezpay
			renewalOptionRadioButtonGift.checked = true;
		}
	} catch (e) {
		// ignore
	}

	// update disclaimer
	try {
		usatOE_updateDisclaimer(thisObj.value);
	} catch (e) {
		// ignore
	}
}

// ////////////////////////////////////////////////////////////////////////
// original
// newFunction
var selectedRow;
function usat_highlight(item) {
	if (item != selectedRow) {
		var pItem = $(item);
		if (pItem.hasClassName('termsNormal')) {
			pItem.removeClassName('termsNormal');
		}
		pItem.addClassName('termsHighlight');
	}
}

function usat_cancelHighlight(item) {
	if (item != selectedRow) {
		item.className = "termsNormal";
	}
}

function usat_selectRow(theid) {
	var row = document.getElementById(theid);

	if (selectedRow) {
		selectedRow.className = "termsNormal";
		var columns = selectedRow.getElementsByTagName("td");
		for ( var i = 0; i < columns.length; i++) {
			columns[i].className = "usatTermsFontNormal";
		}
	}
	row.className = "termsSelected";
	var columns2 = row.getElementsByTagName("td");
	for (i = 0; i < columns2.length; i++) {
		columns2[i].className = "usatTermsFontSelected";
	}
	selectedRow = row;
}

function usat_selectRadioAndHighlight(row) {

	var radio = row.getElementsByTagName('input');
	if (radio[0]) {
		radio[0].checked = true;
		usat_selectRow(row.id);
	}
}

function usat_selectRadio(row) {

	var radio = row.getElementsByTagName('input');
	if (radio[0]) {
		radio[0].checked = true;
	}
}

function usat_selectInitialRow(table) {
	var rows = table.getElementsByTagName('tr');
	for ( var i = 0; i < rows.length; i++) {
		var radio = rows[i].getElementsByTagName('input');
		if (radio && radio[0] && radio[0].checked) {
			usat_selectRadioAndHighlight(rows[i]);
			break;
		}
	}

}

function getCurrentYear() {
	today = new Date();
	y0 = today.getFullYear();
	return y0;
}

// newFunction
// following method is used to verify domain.
function usat_gup(name) {
	name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
	var regexS = "[\\?&]" + name + "=([^&#]*)";
	var regex = new RegExp(regexS);
	var results = regex.exec(window.location.href);
	if (results == null)
		return "";
	else
		return results[1];
}

var usat_verifyDomains = true;

function usat_verifyDomain() {

	try {
		if (usat_verifyDomains) {
			var host = window.location.hostname;

			if (host != "service.usatoday.com") {
				if (host.startsWith("usat-") || host == "localhost"  || host.startsWith("10.") || host.startsWith("ent-")) {
					;
				} else {
					usat_forceRedirect();
				}
			}
		}
	} catch (e) {
		// 
	}
}

function usat_forceRedirect() {
	try {
		// kill any on exit pop ups
		popOffer_programEnabled = false;
	} catch (e) {
	}
	var pubCode = usat_gup('pub');
	var keycode = usat_gup('keycode');

	var newLoc = 'http' + 's://'
			+ 'service.usatoday.com/subscriptions/order/checkout.faces';

	if (pubCode.length == 2) {

		newLoc = newLoc + '?pub=' + pubCode;
		if (keycode.length == 5) {
			newLoc = newLoc + '&keycode=' + keycode;
		}
	}

	document.location = newLoc;
}