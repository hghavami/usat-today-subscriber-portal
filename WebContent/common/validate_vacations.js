
////////////////////////////////////////////////////////////////////////////////////
// date validations
///////////////////////////////////////////////////////////////////////////////////

function vaca_dateChanged(textField,tranType) {
	if (vaca_isValidDateFormat(textField)) {
		vaca_doPubDateCheck(textField,tranType);
	}
	else {
		alert("Invalid Date Format. Date must be in MM/DD/YYYY format.");
		textField.focus();
		textField.select();
	}
}

function vaca_isValidDateFormat(formField)
{
	var result = true;
	var formValue = formField.value;
	
 	if (formValue.length>0)
 	{
 		var elems = formValue.split("/");
 		
 		result = (elems.length == 3); // should be three components
 		
 		if (result)
 		{
 			result = !(isNaN(elems[0])) && !(isNaN(elems[1])) && !(isNaN(elems[2]));
 			
 			if (result) {
 				var month = parseInt(elems[0],10);
	 			var day = parseInt(elems[1],10);
 			
				result = (elems[0].length == 2) && (month > 0) && (month < 13) &&
						 (elems[1].length == 2) && (day > 0) && (day < 32) &&
						 (elems[2].length == 4);
			}
 		} 		
	} 
	
	return result;
}

function vaca_initRequest(url) {
       if (window.XMLHttpRequest) {
           return new XMLHttpRequest();
       } else if (window.ActiveXObject) {
           isIE = true;
           return new ActiveXObject("Microsoft.XMLHTTP");
       }
}

   function vaca_doPubDateCheck(textField, tranType) {
	   var completeField = textField;
       if (completeField.value == "") {
           return;
       } else {  
    	   var url = null;
    	   url = "/validatePubDate.do?selectedDate=" + escape(completeField.value) + "&tranType=" + tranType;	
           var req = vaca_initRequest(url);
           req.onreadystatechange = function() {
               if (req.readyState == 4) {
                   if (req.status == 200) {
                	   vaca_parseMessages(req.responseXML,textField);
                   } else if (req.status == 204){
                       return;
                   }
               }
           };
           req.open("GET", url, true);
           req.send(null);
       }
   }

   function vaca_parseMessages(responseXML,textField) {
           var response = responseXML.getElementsByTagName(
                   "datevalid")[0];
      // alert('response: ' + response.childNodes[0].nodeValue);
       if (response.childNodes[0].nodeValue == 'true') {
       		// date valid, do nothing
       		;
       } else {
       		//alert('USA TODAY is not published on Saturdays or Sundays. Please pick a weekday.');
       		var eMsg = responseXML.getElementsByTagName("message")[0].childNodes[0].nodeValue;
       		alert(eMsg);
       		
       		var earliestDate = responseXML.getElementsByTagName("nextValidDate")[0].childNodes[0].nodeValue;
     		textField.value = earliestDate;
       }    
   }