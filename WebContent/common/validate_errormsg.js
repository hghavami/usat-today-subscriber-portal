//Used to display Servlet error messages via JavaScript

function errormsg(theForm)
{
  var num = theForm.elements.length;
  var validFlag = true;
  for (var i=0; i<num; i++) 
  {
	if (theForm.elements[i].value == "ERROR")
	{
	  if (theForm.elements[i].name == "PAYMENT_ERROR") {
	    theForm.elements[i].value = "";
	    theForm.PAYMENT_NUMBER.focus();
	    alert("The Credit card type used is not accepted.  Please enter your Credit Card information again using one of the accepted card types."); 
	    return (false);
            break;
	  } 
	  else if (theForm.elements[i].name == "DELIVERY_PASSWORD") {
		theForm.elements[i].value = "";
		theForm.elements[i + 1].value = "";
	    theForm.elements[i].focus();         
		alert("The E-Mail account exists, but password does not match.  Please try again."); 
 		return (false);
                break;
  	  } 
	  else {

	       theForm.elements[i].value = "";
	       theForm.elements[i].focus();         
 	       alert("The \""+ theForm.elements[i].name + "\" is not valid.  Please try again."); 
	       return (false);
               break;
          }	
	    
        }

	// Check email address for correctness

	if (theForm.elements[i].value == "EMAILERROR")
	{
	  if (theForm.elements[i].name == "DELIVERY_EMAIL") {
	    theForm.elements[i].value = "";
	    theForm.elements[i + 1].value = "";
	    theForm.elements[i].focus();         
	    alert("The email address format is invalid.  Please enter a new email address."); 
	    return (false);
            break;
	  }
	}

	if (theForm.elements[i].value == "DELSTDATEERROR")
	{
	  if (theForm.elements[i].name == "DELIVERY_STARTDATE") {
	    theForm.elements[i].value = "";
	    theForm.elements[i].focus();         
	    alert("The Delivery Start Date is Invalid.  Please Enter a New Date."); 
	    return (false);
            break;
	  }
	}

	if (theForm.elements[i].value == "DELADDERROR")
	{
	  if (theForm.elements[i].name == "DELIVERY_STREET1") {
	    theForm.elements[i].value = "";
	    theForm.elements[i].focus();         
	    alert("The Delivery Address is Invalid.  Please Enter a New Delivery Address."); 
	    return (false);
            break;
	   }
	}

	if (theForm.elements[i].value == "BILLADDERROR")
	{
	  if (theForm.elements[i].name == "PAYMENT_STREET") {
	    theForm.elements[i].value = "";
	    theForm.elements[i].focus();         
	    alert("The Billing Address is Invalid.  Please Enter a New Billing Address."); 
	    return (false);
            break;
	   }
	} 

	if (theForm.elements[i].value == "BILLADDERROR")
	{
	  if (theForm.elements[i].name == "PAYMENT_STREET2") {
	    theForm.elements[i].value = "";
	    theForm.elements[i].focus();         
	    alert("The Billing Address is Invalid.  Please Enter a New Billing Address."); 
	    return (false);
            break;
	   }
	} 

	if (theForm.elements[i].value == "CCBILLADDERROR")
	{
	  if (theForm.elements[i].name == "PAYMENT_STREET1") {
	    theForm.elements[i].value = "";
	    theForm.elements[i].focus();         
	    alert("Credit Card Billing Address is Invalid.  Please Enter a New Billing Address."); 
	    return (false);
            break;
	   }
	} 

	if (theForm.elements[i].value == "DCARDEXPIRED")
	{
	    alert("The Credit Card entered has expired.  Please adjust the expiration date or enter a new credit card."); 
	    return (false);
            break;
	} 

	if (theForm.elements[i].value == "DCARDREFUSED")
	{
	  if (theForm.elements[i].name == "PAYMENT_NUMBER") {
	    theForm.elements[i].value = "";
	    theForm.elements[i].focus();         
	    alert("The Credit Card entered has been declined.  Please enter a new credit card."); 
	    return (false);
            break;
	   }
	}  

	if (theForm.elements[i].value == "DCARDREFUSED")
	{
	  if (theForm.elements[i].name == "PAYMENT_NUMBER") {
	    theForm.elements[i].value = "";
	    theForm.elements[i].focus();         
	    alert("The Credit Card entered has been declined.  Please enter a new credit card."); 
	    return (false);
            break;
	   }
	}  

	if (theForm.elements[i].value == "DAVSNO")
	{
	  if (theForm.elements[i].name == "PAYMENT_NUMBER") {
	    theForm.elements[i].value = "";
	    theForm.elements[i].focus();         
	    alert("Your Credit Card cannot be processed, please call our National Customer Service at 1-800-USA-0001."); 
	    return (false);
            break;
	   }
	}    	

	if (theForm.elements[i].value == "DCALL")
	{
	  if (theForm.elements[i].name == "PAYMENT_NUMBER") {
	    theForm.elements[i].value = "";
	    theForm.elements[i].focus();         
	    alert("Your Credit Card cannot be processed, please call our National Customer Service at 1-800-USA-0001."); 
	    return (false);
            break;
	   }
	}   
	// This is used for change billing cc validation
	if (theForm.elements[i].value == "DINVALIDCARD")
	{
	  if (theForm.elements[i].name == "PAYMENT_NUMBER") {
//	    document.getElementsByName('CCInfoEZPay')[0].checked = true;
	    theForm.elements[i-2].checked = true;
	    theForm.elements[i].value = "";
	    theForm.elements[i].focus();         
	    alert(theForm.elements[i+1].value + ".  Please retry or call our National Customer Service number below.  Thank you.");
	    return (false);
            break;
	   }
	} 


	if (theForm.elements[i].value == "DINVALIDDATA")
	{
	  if (theForm.elements[i].name == "PAYMENT_NUMBER") {
	    theForm.elements[i].value = "";
	    try {
	    	theForm.elements[i].focus();
	    }
		catch (err) {}
	    
	    alert(theForm.elements[i+1].value + ".  Please retry or call our National Customer Service number below.  Thank you.");
	    theForm.elements[i+1].value = "";
	    return (false);
            break;
	   }
	}    

	if (theForm.elements[i].value == "DINVALIDDATA")
	{
	  if (theForm.elements[i].name == "cvn") {
	    theForm.elements[i].value = "";
	    theForm.elements[i].focus();         
	    alert("Please specify your credit card verification number. Click the \"Learn More...\" link for additional information on locating your verification number.");
	    theForm.elements[i+1].value = "";
	    return (false);
            break;
	   }
	}    


	if (theForm.elements[i].value == "DMISSINGFIELD")
	{
	  if (theForm.elements[i].name == "PAYMENT_NUMBER") {
	    theForm.elements[i].value = "";
	    theForm.elements[i].focus();         
	    alert("Your Credit Card cannot be processed, please call our National Customer Service at 1-800-USA-0001."); 
	    return (false);
            break;
	   }
	}  

	if (theForm.elements[i].value == "DNOAUTH")
	{
	  if (theForm.elements[i].name == "PAYMENT_NUMBER") {
	    theForm.elements[i].value = "";
	    theForm.elements[i].focus();         
	    alert(theForm.elements[i+1].value);
	    theForm.elements[i+1].value = "";
	    return (false);
            break;
	   }
	}  
  }
  return (true);
}

