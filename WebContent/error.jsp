<%@ page isErrorPage="true" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%	

	String defaultErrorMessage = "An Unexpected Error Occurred. Your web application session has probably expired or been reset, or a malformed web form was submitted. Please <a href=\"https://service.usatoday.com\">click here</a> to continue.<br><br>We apologize for any inconvenience.";
	String errorMessage = null;

	try {

		errorMessage = com.usatoday.esub.common.UTCommon.displayError(request, response, this);
		if (errorMessage == null) {
			errorMessage = defaultErrorMessage;
		}

		System.out.println(errorMessage);
//		bug.setErrorCode(session);
//		bug.setErrorPage(true);

	}
	catch( Exception e) {
		if (errorMessage == null) {
			errorMessage = defaultErrorMessage;
		}
	}
%>
<!DOCTYPE HTML>
<%-- tpl:insert page="/theme/subscriptionPortalRoot.htpl" --%><HTML>
<HEAD>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8">
<%-- tpl:put name="headerareaTop" --%>
<title>USA Today Online Subscriptions - Error</title>
<script language="JavaScript" src="/common/prototype/prototype.js"></script>

<%-- /tpl:put --%>
<LINK REL="SHORTCUT ICON" HREF="/favicon.ico">
<LINK REL="icon" href="/favicon.ico" TYPE="image/ico" />
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META http-equiv="Content-Style-Type" content="text/css">
<meta name="viewport" content="initial-scale=1.0">
<LINK href="/theme/themeV3/css/usatodayBase.css" rel="stylesheet"
	type="text/css">
<LINK href="/theme/themeV3/css/usatodayAddOn.css" rel="stylesheet"
	type="text/css">
<script src="/common/usatCommon.js"></script>
<%-- tpl:put name="headareaBottom" --%>		
<%-- /tpl:put --%>
<script type="text/javascript">
	/*   var _gaq = _gaq || [];
	 _gaq.push(['_setAccount', 'UA-23158809-1']);
	 _gaq.push(['_trackPageview']);

	 (function() {
	 var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	 ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	 var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	 })();
	 */
</script>
</HEAD>
<body>
	<DIV id="wrapper">
		<DIV id="header">
			<div id="headerLeft" onclick="templateLogoClick();"></div>
			<div id="headerRight">
				<%-- tpl:put name="headerbodyarea" --%><%-- /tpl:put --%>
			</div>
		</DIV>
		<!-- end header -->
		<DIV id="content">
			<DIV id="whitebox">
				<%-- tpl:put name="bodyAreaMainWhiteBox" --%>
					<script
						src="/eedition/scripts/usatOliveScript.js"></script>
					<!-- SiteCatalyst code version: H.10.
Copyright 1997-2007 Omniture, Inc. More info available at
http://www.omniture.com -->
<script language="JavaScript" src="/include/s_code.js"></script>
<script language="JavaScript"><!--
/* You may give each page an identifying name, server, and channel on
the next lines. */
s.pageName="Root Error Page";
s.server="";
s.channel="";
s.pageType="";
s.prop1="";
s.prop2="";
s.prop3="";
s.prop4="";
s.prop5="";
/* Conversion Variables */
s.campaign="";
s.state="";
s.zip="";
s.events="";
s.products="";
s.purchaseID="";
s.eVar1="";
s.eVar2="";
s.eVar3="";
s.eVar4="";
s.eVar5="";
/************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/
var s_code=s.t();if(s_code)document.write(s_code);//--></script>
<script language="JavaScript"><!--
if(navigator.appVersion.indexOf('MSIE')>=0)document.write(unescape('%3C')+'\!-'+'-')
//--></script><noscript><a href="http://www.omniture.com" title="Web Analytics"><img src="https://102.112.2O7.net/b/ss/ganusatodaycirc/1/H.10--NS/0" height="1" width="1" border="0" alt=""></a></noscript><!--/DO NOT REMOVE/-->
<!-- End SiteCatalyst code version: H.10. -->

<!--  body area -->
<TABLE border="0" cellspacing="5" cellpadding="5" width="600">
	<TBODY>
		<TR>
			<TD colspan="2"></TD></TR>
		<TR>
			<TD colspan="2"><B><span style='font-size: 13.5pt;line-height: normal'>The
												USA TODAY and Sports Weekly Subscription Service site is
												temporarily unavailable at this time.</span></B><br></TD></TR>
		
		<TR>
			<TD colspan="2"><B>We apologize for any inconvenience. <span
										style="font-size: 11.0pt;line-height: normal" >
											Please try again in 30 minutes or call our National Customer
											Service Center at (800) 872-0001. &nbsp;<span style='color: red;font-size: 11.0pt;line-height: normal'><br><br>Our
												automated telephone system is available to assist you 24
												hours a day, seven days a week!&nbsp; To speak with a
												customer service representative, please call Monday  - Friday
												from 8am to 7pm (EST).</span>
									</span><BR><br>
<!--
Please enjoy our									
<script type="text/javascript">
//var olive = new oliveReaderURL();
//olive.usat_oliveuserid = 'usatSiteDown';  
//var BookURL = olive.getOliveReaderURL();
//document.write ("<a href="+BookURL+" target='_blank'>USA TODAY e-Newspaper</a>");
</script>
-->
							<BR><br>
							Thank you.</B></TD></TR>
		<TR>
			<TD height="30"></TD>
			<TD></TD>
		</TR>
		<TR>
			<TD align="center" colspan="2"><A href="http://www.usatoday.com">Click here to go to USATODAY.com</A></TD>
		</TR>

	</TBODY>
</TABLE>

<p>&nbsp;</p>
<%-- /tpl:put --%>
				<DIV id="clear"></DIV>
				<DIV id="bluebar">
					<a href="/feedback/feedback.jsp"
						style="color: white; font-weight: bold; text-decoration: none; font-size: 14px"
						title="Contact Us">Contact Us</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;1-800-872-0001&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a
						href="/faq/utfaq.jsp"
						style="color: white; font-weight: bold; text-decoration: none; font-size: 14px"
						title="Frequently Asked Questions">FAQ</a>
				</DIV>
				<%-- tpl:put name="footerMainArea" --%><%-- /tpl:put --%>
				<DIV id=graybar>
					Copyright
					<script>
						document.write(getCurrentYear());
					</script>
					USA TODAY, a division of <a href="http://www.gannett.com">Gannett
						Co. Inc.</a> <a href="/privacy/privacy.htm">Privacy
						Notice/California Privacy Notice</a>. <a
						href="/privacy/privacy.htm#adchoice">Ad Choices</a>.<br> By
					using this service, you accept our <a href="/service/service.jsp">Terms
						of Service</a>.
				</DIV>
				<DIV id=copyright>
					<P>Offers available to new subscribers only. Home delivery not
						available in some areas. Not valid with any other offer.
						Prepayment required. After the promotional period ends, the
						subscription will continue and be billed monthly at the then
						regular rate, less any applicable credits, unless USA TODAY is
						otherwise notified. Applicable taxes may not be included in the
						rate. If at any time you decide to cancel your subscription, you
						may contact customer service at 1-800-872-0001 and the full amount
						of any balance over $2.00 will be returned.</P>
				</DIV>
				<!--END OF COPYWRITE-->
				<DIV id=footer></DIV>
			</DIV>
			<!--END OF WHITEBOX-->
		</DIV>
		<!--END OF CONTENT-->
	</DIV>
</BODY>
</HTML><%-- /tpl:insert --%>