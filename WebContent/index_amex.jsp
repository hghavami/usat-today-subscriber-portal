<html>

<head>
<title>USA Today Online Subscriptions - Portal</title>
<script language="JavaScript">
</script>
</head>

<body>
<%@ page import = "com.usatoday.esub.common.*" %>
<%
	com.usatoday.business.interfaces.products.OfferIntf offer = UTCommon.getCurrentOffer(request);
	//
	// save the promo code
	//
	
	try {
		String promo = request.getParameter(UTCommon.KEYCODE_QUERYSTRING);
	
		if (promo.length() > 0) {
			// promo code found:  assume from a web link and redirect
			// to the new subscription page
	
			// redirect to the new subscription  page
			UTCommon.showUrl(response, UTCommon.NEWSUBSCRIPTION_URL + "/index.jsp");
		} else {
			// no promo code: assume not from a web link so use default
			// code and redirect to the main page
			UTCommon.showUrl(response, UTCommon.BASE_URL + "/welcome.jsp");
		}
	} catch (NullPointerException e) {
		// no promo code: assume not from a web link so use default
		// code and redirect to the main page
		UTCommon.showUrl(response, UTCommon.BASE_URL + "/welcome.jsp");
	}
%>

<p>This page should never be displayed.</p>

</body>

</html>
