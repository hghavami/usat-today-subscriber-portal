<%

// Redirect to the proper page based on pub code

try {
	com.usatoday.business.interfaces.products.OfferIntf offer = com.usatoday.esub.common.UTCommon.getCurrentOffer(request);
	String pubCode = com.usatoday.util.constants.UsaTodayConstants.UT_PUBCODE;
	if (offer != null) {
		pubCode = offer.getPubCode();
	}
	if (pubCode.equalsIgnoreCase(com.usatoday.util.constants.UsaTodayConstants.SW_PUBCODE)) {
		com.usatoday.esub.common.UTCommon.showUrl(response, com.usatoday.esub.common.UTCommon.BASE_URL + "/testimonials_sw.jsp");	
	} else {
		com.usatoday.esub.common.UTCommon.showUrl(response, com.usatoday.esub.common.UTCommon.BASE_URL + "/testimonials_ut.jsp");		
	}
} catch (Exception e) {

}	
%>
<html>
<head>
<title>USA Today Online Subscriptions - Portal</title>
</head>
<body>
<p>This page should never be displayed.</p>
</body>
</html>