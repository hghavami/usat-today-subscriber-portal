<%@ page import = "com.usatoday.esub.common.*" %>
<%
	String accountPath = "";
	//
	// save the customer service option
	//
	
	try {
		String custopt = request.getParameter(UTCommon.CUSTOPT_QUERYSTRING);
		if (custopt.length() > 0) {
			// Customer Service option found:  Redirect based on it value
	
			if (custopt.equals("RENEW")) {
		        String omnitureEmailTrackingCode = request.getParameter("ccode");
		        if (omnitureEmailTrackingCode == null) {
		        	omnitureEmailTrackingCode = "";
		        }
		        else {
		        	omnitureEmailTrackingCode = "?ccode=" + java.net.URLEncoder.encode(omnitureEmailTrackingCode, "UTF-8");
		        }
		        
				String paytype = request.getParameter(UTCommon.PAYTYPE_QUERYSTRING);
				session.setAttribute(UTCommon.SESSION_PAYTYPE, paytype);
				accountPath = UTCommon.RENEWALS_URL + "/renew.faces";
				session.setAttribute(UTCommon.SESSION_ACCOUNT_PATH, accountPath);
				UTCommon.showUrl(response, UTCommon.RENEWALS_URL + "/renew.faces" + omnitureEmailTrackingCode);
			}
			if (custopt.equals("BILL")) {
				String paytype = request.getParameter(UTCommon.PAYTYPE_QUERYSTRING);
				session.setAttribute(UTCommon.SESSION_PAYTYPE, paytype);	
				accountPath = UTCommon.RENEWALS_URL + "/renew.faces";
				session.setAttribute(UTCommon.SESSION_ACCOUNT_PATH, accountPath);
				UTCommon.showUrl(response, UTCommon.RENEWALS_URL + "/renew.faces");
			}
			if (custopt.equals("DELA")) {
				accountPath =  "/account/changeaddress/changeaddress.faces";
				session.setAttribute(UTCommon.SESSION_ACCOUNT_PATH, accountPath);
				UTCommon.showUrl(response, "/account/changeaddress/changeaddress.faces");
			}
			if (custopt.equals("BILA")) {
				accountPath = "/account/changebilling/changebilling.faces";
				session.setAttribute(UTCommon.SESSION_ACCOUNT_PATH, accountPath);
				UTCommon.showUrl(response, "/account/changebilling/changebilling.faces");
			}
			if (custopt.equals("VACA")) {
				accountPath = "/account/vacationholds/holds.faces";
				session.setAttribute(UTCommon.SESSION_ACCOUNT_PATH, accountPath);
				UTCommon.showUrl(response, "/account/vacationholds/holds.faces");
			}
			if (custopt.equals("ACTI")) {
				accountPath = "/account/accounthistory/accountSummary.faces";
				session.setAttribute(UTCommon.SESSION_ACCOUNT_PATH, accountPath);
				UTCommon.showUrl(response, "/account/accounthistory/accountSummary.faces");
			}
			
			if (custopt.equals("IDPW")) {
				accountPath = UTCommon.ID_PASSWORD_URL + "/index.jsp";
				session.setAttribute(UTCommon.SESSION_ACCOUNT_PATH, accountPath);			
				UTCommon.showUrl(response, UTCommon.ID_PASSWORD_URL + "/index.jsp");
			}

			if (custopt.equals("CP")) {
				UTCommon.showUrl(response, "/account/cp/delissue.faces");
			}			

			if (custopt.equals("MAIL")) {
				UTCommon.showUrl(response, UTCommon.SUBSCRIPTION_URL + "/subscribebymail.jsp");
			}

			if (custopt.equals("CNCL")) {
				accountPath = "/account/cancels/stopSubscription.faces";
				session.setAttribute(UTCommon.SESSION_ACCOUNT_PATH, accountPath);
				UTCommon.showUrl(response, "/account/cancels/cancelSubscription.faces");
			}

		} 
	} catch (NullPointerException e) {
		// no customer service option
		// redirect to the Customer Service page
		UTCommon.showUrl(response, "/account/accounthistory/accountSummary.faces");
	}

%>
<html>

<head>
<title>USA Today Online Subscriptions - Customer Service Portal</title>
</head>
<body>
<p>This page should never be displayed.</p>
</body>
</html>
