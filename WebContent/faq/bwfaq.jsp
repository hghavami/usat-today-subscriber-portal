<%-- tpl:insert page="/theme/subscriptionPortalJSPNoNavigation.jtpl" --%><!DOCTYPE HTML><%@page import="com.usatoday.business.interfaces.products.ProductIntf"%>
<%@page import="com.usatoday.businessObjects.products.ProductBO"%>
<%@page import="com.usatoday.util.MobileDeviceDetectionUtil"%><%-- tpl:insert page="/theme/subscriptionPortalRoot.htpl" --%><HTML>
<HEAD>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8">
<%-- tpl:put name="headerareaTop" --%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%
	String docProtocol = "https:";  
	if (!request.isSecure()) {
		docProtocol = "http:";
	}
	String initFunction = "";  // override this value if need be on page load
	
	com.usatoday.esub.common.WebBug trackingBug = com.usatoday.esub.common.UTCommon.getWebAnalyticsBug();
	String pubCode = com.usatoday.util.constants.UsaTodayConstants.UT_PUBCODE;
	String productName = "USA TODAY";
	com.usatoday.business.interfaces.products.OfferIntf offer = null;
	try {
		
		offer = com.usatoday.esub.common.UTCommon.getCurrentOffer(request);
		
		if (offer != null) {
			pubCode = offer.getPubCode();
			try {
				String brandingPubCode = offer.getProduct().getBrandingPubCode();
				ProductIntf bp = ProductBO.fetchProduct(brandingPubCode);
				productName = bp.getName();
			}
			catch (Exception teee) {
			}				
		}
		// figure out tracking
		trackingBug.deriveValuesFromRequest(request);
	}
	catch (Exception e) {
		; // ignore
	}

	com.usatoday.businessObjects.products.promotions.PromotionSet defaultPromotionSet = null;
	//com.usatoday.businessObjects.products.promotions.PromotionSet currentOfferPromotionSet = null;
	
	com.usatoday.business.interfaces.products.promotions.ImagePromotionIntf imagePromo = null;
	
	String mobileCSS = "";
	if (MobileDeviceDetectionUtil.isSmallScreenDevice(request)) {
		mobileCSS = "<LINK href=\"/theme/themeV3/css/smallScreen.css\" rel=\"stylesheet\" type=\"text/css\">";
	}	
%>
<script language="JavaScript" src="/common/prototype/prototype.js"></script>
<script src="/common/prototype/scriptaculous/scriptaculous.js" type="text/javascript"></script>
<%-- tpl:put name="headarea_Top" --%>
<TITLE>USA Today - Sports Weekly Online Subscriptions - FAQ</TITLE>
<%-- /tpl:put --%>
<%-- /tpl:put --%>
<LINK REL="SHORTCUT ICON" HREF="/favicon.ico">
<LINK REL="icon" href="/favicon.ico" TYPE="image/ico" />
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META http-equiv="Content-Style-Type" content="text/css">
<meta name="viewport" content="initial-scale=1.0">
<LINK href="/theme/themeV3/css/usatodayBase.css" rel="stylesheet"
	type="text/css">
<LINK href="/theme/themeV3/css/usatodayAddOn.css" rel="stylesheet"
	type="text/css">
<script src="/common/usatCommon.js"></script>
<%-- tpl:put name="headareaBottom" --%>
<%=mobileCSS %>
<%-- tpl:put name="headarea_Bottom" --%>		
<%-- /tpl:put --%>
<script>			
	function initPage() {

		// for product messageing in popUpOverlay.js
		usat_productName = "<%=productName %>";
				
		<%=initFunction %>
	}	
</script>		
	<%-- /tpl:put --%>
<script type="text/javascript">
	/*   var _gaq = _gaq || [];
	 _gaq.push(['_setAccount', 'UA-23158809-1']);
	 _gaq.push(['_trackPageview']);

	 (function() {
	 var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	 ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	 var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	 })();
	 */
</script>
</HEAD>
<body>
	<DIV id="wrapper">
		<DIV id="header">
			<div id="headerLeft" onclick="templateLogoClick();"></div>
			<div id="headerRight">
				<%-- tpl:put name="headerbodyarea" --%>
					<%-- tpl:insert page="/login/loginform.jspf" --%><div style="float: none">
	<%
		String uri = request.getRequestURI();
		String pageName = uri.substring(uri.lastIndexOf("/") + 1);
		String jScript = "";
		if (uri.indexOf("subscriptions") > 0 && uri.indexOf("thankyou.jsp") > 0) {
			try {
				com.usatoday.esub.handlers.ShoppingCartHandler shoppingCartHandler = (com.usatoday.esub.handlers.ShoppingCartHandler) session
						.getAttribute("shoppingCartHandler");
				if (shoppingCartHandler != null) {
					jScript = shoppingCartHandler.getOnePageThankYouJavaScriptTealiumText();
				}
			} catch (Exception E) {

			}
		} else {
			com.usatoday.businessObjects.products.promotions.PromotionSet currentOfferPromotionSetTealium = null;
			com.usatoday.business.interfaces.products.OfferIntf offerTealium = com.usatoday.esub.common.UTCommon
					.getCurrentOffer(request);
			if (offerTealium != null && offerTealium instanceof com.usatoday.business.interfaces.products.SubscriptionOfferIntf) {
				currentOfferPromotionSetTealium = ((com.usatoday.business.interfaces.products.SubscriptionOfferIntf) offerTealium)
						.getPromotionSet();
				jScript = currentOfferPromotionSetTealium.getPromoOnePageJavaScriptTealiumText();
				jScript = org.apache.commons.lang3.StringUtils.replace(jScript, "page_name:\"\"", "page_name:\"" + uri + "\"");
			}
		}
	%>
	<%=jScript%>
</div>
<%-- 			<f:verbatim>
				<h:outputText styleClass="outputText" id="textThankYouJS13"	escape="false" value="#{shoppingCartHandler.tealiumOnePageThankYouJavaScriptText}"></h:outputText>
			</f:verbatim>
 --%>
<%
	String isLoginDisplay = "true";
	com.usatoday.business.interfaces.customer.CustomerIntf customer2 = null;
	String loggedinDivClass = "hideMe";
	String notLoggedinDivClass = "showMeInline";
	String firstName = "";
	String lastName = "";
	String firmInfo = "";
	String accountNum = "";
	String loginErrorMessage2 = "";
	String logOut = "";
	String publication = "";
	String changeAccountLink = "";
	String earlyAlertDisplay = "display:none";

	String jScriptNoOp = "javascript:;";

	try {
		publication = (String) session.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_PUB_NAME);
		if (publication == null) {
			publication = request.getParameter("pub");
			if (publication == null) {
				publication = com.usatoday.util.constants.UsaTodayConstants.UT_PUBCODE;
			}
		}
		if (publication.equals(com.usatoday.esub.common.UTCommon.PUB_NAMES[0])) { // USATODAY publication
			logOut = com.usatoday.esub.common.UTCommon.BASE_URL + "/index.jsp?pub=UT&logsessobj=rem";
		} else {
			logOut = com.usatoday.esub.common.UTCommon.BASE_URL + "/index.jsp?pub=BW&logsessobj=rem";
		}

		// If customer is logged in remove login page, otherwise display it
		customer2 = (com.usatoday.business.interfaces.customer.CustomerIntf) session
				.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_CUSTOMER_INFO);
		if (customer2 == null) {
			// showMeInling and hideMe are defined in style sheets
			loggedinDivClass = "showMeInline";
			notLoggedinDivClass = "hideMe";
		} else {

			if (customer2.getCurrentAccount() != null) {
				firstName = customer2.getCurrentAccount().getDeliveryContact().getFirstName();
				if (firstName != null && !firstName.trim().equals("")) {
					firstName = com.usatoday.esub.common.UTCommon.formatNameCapitalization(firstName); // Format it
				}
				lastName = customer2.getCurrentAccount().getDeliveryContact().getLastName();
				if (lastName != null && !lastName.trim().equals("")) {
					lastName = com.usatoday.esub.common.UTCommon.formatNameCapitalization(lastName); // Format it
				}
				firmInfo = customer2.getCurrentAccount().getDeliveryContact().getFirmName();
				if (customer2.getNumberOfAccounts() > 1) {
					changeAccountLink = "[&nbsp;<a href=\"/account/select_accnt.jsp\">Change</a>&nbsp;]";
				}
				try {
					accountNum = customer2.getCurrentAccount().getAccountNumber();
					//				if (accountNum == null || accountNum.length() < 4) {
					if (accountNum == null) {
						accountNum = "";
					}
					//				else {
					/*
					if (accountNum.equalsIgnoreCase("340000000")) {
						accountNum = "Pending Setup";
					}
					 */
					//				}
				} catch (Exception eeee) {
					eeee.printStackTrace();
				}

				//EarlyAlert - Check to see if their paper delivery has been affected by a delay
				// HD-CONS109 SEW
				/*
				if (customer2.getCurrentAccount().isShowDeliveryAlert()) {
				earlyAlertDisplay = "display:inline";
				}
				 */

			} else {
				if (customer2.getCurrentEmailRecord() != null
						&& customer2.getCurrentEmailRecord().getEmailAddress() != null) {
					firstName = customer2.getCurrentEmailRecord().getEmailAddress();
					accountNum = "Pending Setup";
				} else {
					firstName = "";
				}
			}
		}
		loginErrorMessage2 = (String) session.getAttribute("LOGIN_FAILED_MESSAGE");
		if (loginErrorMessage2 == null || loginErrorMessage2.trim().equals("")) {
			loginErrorMessage2 = "";
		} else {
			session.removeAttribute("LOGIN_FAILED_MESSAGE");
		}
	} catch (Exception e) {
		e.printStackTrace();
		if (firstName == null) {
			firstName = "";
		}
		if (lastName == null) {
			lastName = "";
		}
	}
	String nameInfo = firstName + " " + lastName;
	String loginInfo = null;
	if (!nameInfo.trim().equals("")) {
		loginInfo = nameInfo;
	} else {
		if (firmInfo != null && !firmInfo.trim().equals("")) {
			loginInfo = firmInfo;
		}
	}
	// if no login info, then hide data
	if (loginInfo == null) {
		loggedinDivClass = "showMeInline";
		notLoggedinDivClass = "hideMe";
		loginInfo = "";
	}
%>

<%@page import="com.usatoday.util.constants.UsaTodayConstants"%><div
	id="loginDisplayDiv" class="<%=loggedinDivClass%>">
	<table border="0" width="100%" height="60px">
		<tr>
			<td>
				<TABLE class="loginFormTable">
					<TBODY>
						<TR>
							<TD class="loginFormColumn"><br /> <a
								href="/login/auth.faces">Login to Manage Account</a></TD>
						</TR>
						<tr>
							<td class="loginFormColumn" align="right"><img
								style="cursor: pointer; margin-top: 10px;"
								onclick="javascript: templateClickToChatClickedGuest(this, event);"
								alt="Click for Assistance" title="Click for Assistance"
								src="${pageContext.request.contextPath}/theme/themeV3/themeImages/chatIcon136.png"
								hspace="0" vspace="5"></td>
						</tr>
					</TBODY>
				</TABLE></td>
		</tr>
	</table>
</div>
<div id="loginHideDiv" class="<%=notLoggedinDivClass%>">
	<table width="100%" border="0" class="loginFormTable">
		<tr>
			<td nowrap align="right">
				<TABLE class="loginFormTable">
					<TBODY>
						<TR>
							<TD class="loginFormColumn" nowrap><span
								class="loginNormalPrint">Hello, <%=loginInfo%>&nbsp;&nbsp;[&nbsp;<a
									href="<%=jScriptNoOp%>"
									onclick="javascript:document.location='<%=logOut%>';_unloadPage(this, event);">Logout</a>&nbsp;]</span>
							</TD>
						</TR>
					</TBODY>
				</TABLE></TD>
		</tr>
		<tr>
			<td class="loginFormColumn" align="right"><span
				class="loginfineprint">Account #:&nbsp;<%=accountNum%>&nbsp;<%=changeAccountLink%></span>
			</td>
		</tr>
		<tr>
			<td class="loginFormColumn" align="right"><div
					id="chatIconRequestDiv">
					<img onclick="javascript: templateClickToChatClicked(this, event);"
						style="cursor: pointer; margin-top: 10px;"
						alt="Click for Assistance" title="Click for Assistance"
						src="${pageContext.request.contextPath}/theme/themeV3/themeImages/chatIcon136.png"
						hspace="0" vspace="5">
				</div>
			</td>
		</tr>
	</table>
</div>
<div id="templateChatWindowDiv" style="display: none;">
	<div id="templateChatTopDiv">
		<span onclick="templateClickToChatHide();" style="cursor: pointer;">No
			Thanks<img src="/theme/themeV3/themeImages/close_14x14.png"
			width="14" height="14" border="0"
			style="margin-left: 5px; margin-right: 5px" /> </span>
	</div>
<!-- 	<div id="tempateChatImageDiv">
		<a
			onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
			href=""><img border="0"
			src="https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY" />
		</a>
	</div>
 -->	<div id="templateChatAccountLinksDiv">
		<ul>
			<li>To report a problem with delivery, click <a
				href="/account/cp/delissue.faces"> here</a>.</li>
			<li>To place your delivery on a temporary hold, click <a
				href="/account/vacationholds/holds.faces"> here</a>.</li>
			<li>Forgot your password or login? Click <a
				href="/idpassword/forgotpassword.jsp"> here</a>.
			<li>To contact us <a href="/feedback/feedback.jsp"> click here</a>.</li>				
<!-- 			<li>To cancel your subscription, please call 1-800-872-0001 and -->
<!-- 				select option 6.</li> -->
		</ul>
	</div>

	<div id="templateChatLinksDiv">
		<ul>

			<li><a
				onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
				href="">Click to Chat</a>
			</li>
		</ul>
	</div>
	<div id="templateChatBottomDiv">
		<span class="outputText">Or, call us at:<br> <b>Call
				1-877-713-6241</b><br>(M-F 8:00 a.m. to 7 p.m. E.T.)</span>
	</div>
</div>
<div id="templateChatWindowGuestDiv" style="display: none;">
	<div id="templateChatTopDiv">
		<span onclick="templateClickToChatHideGuest();"
			style="cursor: pointer;">No Thanks<img
			src="/theme/themeV3/themeImages/close_14x14.png" width="14"
			height="14" border="0" style="margin-left: 5px; margin-right: 5px" />
		</span>
	</div>
<!-- 	<div id="tempateChatImageDiv">
		<a
			onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
			href=""><img border="0"
			src="https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY" />
		</a>
	</div>
 -->	<div id="templateChatAccountLinksDiv">
		<ul>
			<li>To manage your account online - including reporting a
				problem with delivery or placing a temporary hold on your account -
				log in by entering your e-mail address and password. Click <a
				href="/login/auth.faces"> here</a> to access the login page now.</li>
			<li>Forgot your password or login? <a
				href="/idpassword/forgotpassword.jsp"> click here</a>.</li>
			<li>To contact us <a href="/feedback/feedback.jsp"> click here</a>.</li>
			<!-- 			<li>To cancel your subscription, please call 1-800-872-0001 and -->
			<!-- 				select option 6.</li> -->
		</ul>

	</div>

	<div id="templateChatLinksDiv">
		<ul>
			<li><a
				onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
				href="">Click to Chat</a>
			</li>
		</ul>
	</div>
	<div id="templateChatBottomDiv">
		<span class="outputText">Or, call us at:<br> <b>Call
				1-877-713-6241</b><br>(M-F 8:00 a.m. to 7 p.m. E.T.)</span>
	</div>
</div>
<%-- /tpl:insert --%>
		
	<%-- /tpl:put --%>
			</div>
		</DIV>
		<!-- end header -->
		<DIV id="content">
			<DIV id="whitebox">
				<%-- tpl:put name="bodyAreaMainWhiteBox" --%>
		<DIV id="pageContentFullWidth">
			<%-- tpl:put name="bodyarea" --%>
<h1>Sports Weekly FAQ</h1>
				<TABLE border="0" width="758" cellspacing="0" cellpadding="0">
					<TBODY>
						<TR>
							<TD width="30" bgcolor="white">&nbsp;</TD>
							<TD width="0"></TD>
							<TD valign="top" align="left" width="728">
							<P><BR>
							<FONT face="Arial" size="2" color="black">Welcome to the
							Frequently Asked Questions (FAQ) page. If you need answers to
							questions not addressed here, please call our National Customer
							Service Department at 1-800-872-1415 (M-F 8:00AM - 7:00PM ET).</FONT><BR>
							<br>
							<FONT size="2" face="Arial"> <B>For questions about USA TODAY <a href="/faq/utfaq.jsp">click here</a></b></font></P>
							<p><font size="2" face="Arial"><b><a
								href="/faq/utfaq.jsp"></a><br>
							<font size="2" face="Arial"> <b>For questions about
							USA TODAY e-Newspaper <a href="/faq/eeFAQ.pdf">click here</a><br>
							<br>
							<b><font size="2" face="Arial">Features of </font><font
								size="2" face="Arial" color="red"><i>Sports Weekly<br>
							</i></font><a href="#section1"><font face="Arial" size="2">Why
							should I subscribe to </font><font size="2" face="Arial" color="red"><i>Sports
							Weekly</i></font><font size="2" face="Arial"><i>?</i></font></a><br>
							</b> <br>
							<font size="2" face="Arial" color="black"><b>Delivery
							of the Magazine</b></font><br>
							<a href="#section2"> <font size="2" face="Arial"> How
							will </font><font size="2" face="Arial" color="red"><i><b>Sports
							Weekly</b></i></font><font size="2" face="Arial"><b> be delivered?</b></font></a><font
								face="Arial"><a href="#section3"><font size="2"
								face="Arial"><br>
							What should I do if my magazine does not arrive?<br>
							</font></a></font><font color="blue" face="Arial"><b><i></i></b></font><font
								color="blue"><b><font face="Arial"><i> <br>
							</i></font></b></font><font color="blue" face="Arial"><font face="Arial"><font
								size="2" face="Arial" color="black"><b>Billing and
							Payment<br>
							</b></font><a href="#section4"><font size="2" face="Arial">How
							can I pay for my subscription?<br>
							</font></a><font face="Arial" color="blue"><a href="#section5"><font
								face="Arial"><font face="Arial"><font size="2"
								face="Arial">What is EZ-PAY?</font></font></font></a></font><a href="#section6"><font
								size="2" face="Arial"><br>
							How do I make a change to EZ-PAY? </font></a><font size="2" face="Arial"
								color="black"><b> </b></font></font></font><font face="Arial"><br>
							<br>
							<b><font size="2" color="black" face="Arial">Managing
							your Subscription Online</font><font size="2" face="Arial" color="red"><i><br>
							</i></font></b><a href="#section7"><font size="2" face="Arial">How do
							I access my account online</font></a><font size="2" face="Arial" color="red"><b><i></i></b></font><font
								size="2" face="Arial" color="blue"><b></b>?</font><br>
							<a href="#section8"><font size="2" face="Arial">Where
							can I find my account number?</font></a><br>
							<a href="#section9"> <font size="2" face="Arial">How
							do I make a change to my account information</font></a><font size="2"
								face="Arial" color="red"></font><font size="2" face="Arial"
								color="blue">?</font><br>
							<a href="#section10"><font size="2" face="Arial">Why
							can't I see the changes I made to my account immediately?</font></a><br>
							<a href="#section11"> <font size="2" face="Arial">How
							do I temporarily stop my subscription if I go on vacation</font></a><font
								size="2" face="Arial" color="red"></font><font size="2"
								face="Arial" color="blue">?</font><br>
							<b><font size="2" face="Arial" color="black"><br>
							Other Services and Questions</font></b></font><br>
							<a href="#section29"><font size="2" face="Arial">How
							do I order multiple copies of the newspaper?</font></a><br>
							<font face="Arial" color="blue"><font face="Arial"><a
								href="#section13"><font size="2" face="Arial">If I am
							purchasing a subscription as a gift, how do I request a gift
							announcement card?<br>
							</font></a></font><a href="#section14"><font size="2" face="Arial"> Why
							does my address get reformatted when I submit it?<br>
							</font></a></font><a href="#section15"><font size="2" face="Arial"> What
							is a back issue?<br>
							</font></a> <br>
							<font face="Arial"><font size="2" face="Arial"
								color="black"><b>Features of </b></font><b><font size="2"
								face="Arial" color="red">Sports Weekly</font></b></font><br>
							<br>
							<font face="Arial"><b><a name="section1"><font
								size="2" color="#5b748a">Why should I subscribe to </font></a></b></font><font
								size="2" face="Arial" color="red"><b><i>Sports
							Weekly</i></b><font size="2" face="Arial"><b>?<br>
							</b></font><font size="2" face="Arial" color="red"><b><i>Sports
							Weekly</i></b></font><font size="2" face="Arial" color="blue"><b> </b></font><font
								size="2" face="Arial" color="black">has the best
							behind-the-scenes Baseball and Professional Football coverage
							anywhere. When you subscribe to</font><font size="2" face="Arial"
								color="blue"> </font><font size="2" face="Arial" color="red"><i><b>Sports
							Weekly</b></i></font></font><font face="Arial" color="black"><font face="Arial"
								color="black"><font size="2" face="Arial" color="black">,
							you'll get all of the in-depth stats and analysis that true
							sports fans love. You'll also get compelling interview and
							profiles of your favorite players as well as award-winning
							fantasy advice and information. By subscribing, you'll receive a
							great savings off of the newsstand price, plus you'll enjoy the
							convenience of having it delivered right to your home. </font></font><br>
							<br>
							<font face="Arial" color="black"><font size="2"
								face="Arial" color="black">Some of the features that you
							can look for include:</font></font></font></b></font></b></font></p>
							<UL style="margin-bottom: 5px;">
								<LI><FONT face="Arial" color="blue"><FONT face="Arial"
									color="blue"><FONT
									face="Arial" color="black"><FONT face="Arial" color="black"><FONT
									size="2" face="Arial" color="black">The best Fantasy Baseball
								and Football Reports that feature award-winning stats, analysis,
								tips and strategies to win your league. A must read for any
								fantasy player.</FONT></FONT></FONT></FONT></FONT></LI>
								<LI><FONT face="Arial" color="black"><FONT face="Arial"
									color="black"><FONT face="Arial" color="black"><FONT size="2"
									face="Arial" color="black">An exciting cover and compelling
								feature stories gives you a unique insider's view of Baseball
								and Professional Football.</FONT></FONT></FONT></FONT></LI>
								<LI><FONT face="Arial" color="black"><FONT face="Arial"
									color="black"><FONT face="Arial" color="black"><FONT size="2"
									face="Arial" color="black">Comprehensive Major League team
								notes keeps you up-to-date on what's happening on the field, in
								the clubhouse and in the front office.</FONT></FONT></FONT></FONT></LI>
								<LI><FONT face="Arial" color="black"><FONT face="Arial"
									color="black"><FONT face="Arial" color="black"><FONT size="2"
									face="Arial" color="black">Award-winning columnist Jim Corbett
								breaks down the hottest Professional Football stories of the
								week in his popular column. You'll get inside scoops and
								analysis from one of the best writers in the business.</FONT></FONT></FONT></FONT></LI>
								<LI><FONT face="Arial" color="black"><FONT face="Arial"
									color="black"><FONT face="Arial" color="black"><FONT size="2"
									face="Arial" color="black">An in-depth profile of every game
								played in the NFL each week. Our experts keep you in the game
								with complete match-up reports that include injury updates, keys
								to the game, current line-ups and much more. </FONT></FONT></FONT></FONT></LI>
								<LI><FONT face="Arial" color="black"><FONT face="Arial"
									color="black"><FONT size="2" face="Arial" color="black">&quot;Inside
								Scoop&quot; offers our offbeat view and behind-the-scenes
								stories from around the world of Baseball and Professional Football.</FONT></FONT></FONT></LI>
								<LI><FONT face="Arial" color="black"><FONT face="Arial"
									color="black"><FONT face="Arial" color="black"><FONT size="2"
									face="Arial" color="black">Our unique tracking of the top Minor
								League prospects, broken out by Major League Affiliation, tells
								you who's &quot;nearly ready,&quot; &quot;up and coming,&quot;
								or a &quot;sleeper.&quot;</FONT></FONT></FONT></FONT></LI>
								<LI><FONT face="Arial" color="black"><FONT face="Arial"
									color="black"><FONT size="2" face="Arial" color="black">Our
								one-on-one Q&amp;A's get up-close and personal with Baseball
								and Professional Football newsmakers.</FONT></FONT></FONT></LI>
								<LI><FONT face="Arial" color="black"><FONT face="Arial"
									color="black"><FONT size="2" face="Arial" color="black">We also
								keep our readers on top of the college scene with our top 25
								College Baseball rankings and special College Baseball reports.</FONT></FONT></FONT></LI>


							</UL>
							<FONT size="2" face="Arial" color="blue"><A href="#section99">Back
							to top</A></FONT><FONT face="Arial"><BR>
							<BR>
							</FONT><FONT size="2" face="Arial" color="black"><B>Delivery of
							the Magazine</B></FONT><FONT size="2" face="Arial" color="red"><B></B></FONT><FONT
								face="Arial"><BR>
							<BR>
							</FONT><A name="section2"><FONT size="2" face="Arial"
								color="#5b748a"><B>How will<I> </I></B></FONT><I><B><FONT
								size="2" face="Arial" color="red">Sports Weekly</FONT></B></I><FONT
								face="Arial"></FONT> <FONT face="Arial" size="2" color="#5b748a"><B>be
							delivered?</B></FONT></A><B></B><BR>
							<FONT face="Arial" color="black"><SPAN
								style="font-size:10.0pt;font-family:Arial">Your subscription to
							<B style="mso-bidi-font-weight:normal"><I
								style="mso-bidi-font-style:
normal"><SPAN style="color:red">Sports
							Weekly</SPAN></I></B> will be delivered via the US Postal Service
							with your regular mail.&nbsp; Most subscribers will receive their
							<B style="mso-bidi-font-weight:normal"><I
								style="mso-bidi-font-style:
normal"><SPAN style="color:red">Sports
							Weekly </SPAN></I></B>every Wednesday, the issue day of the
							magazine (in rare cases, your subscription could be
							hand-delivered by a carrier).<SPAN style="mso-spacerun:yes">&nbsp;
							</SPAN>Some rural areas may be delivered by the US Postal Service
							on a delay.<SPAN style="mso-spacerun:yes">&nbsp; </SPAN>If you
							have questions about how your subscription will be delivered,
							please call us at 1-800-872-1415 (M-F 8:00AM - 7:00PM ET).</SPAN></FONT><FONT
								face="Arial" color="blue"><FONT face="Arial" color="blue"></FONT>
							</FONT><BR>
							<FONT color="blue" face="Arial"><SPAN
								style="font-size:10.0pt;font-family:Arial"><FONT color="black"
								face="Arial"></FONT><FONT face="Arial" color="blue"><FONT
								face="Arial"><A href="#section99"><FONT size="2" face="Arial"
								color="blue">Back to top</FONT></A></FONT></FONT></SPAN><BR>
							<BR>


							</FONT><FONT face="Arial"><SPAN
								style="font-size:10.0pt;font-family:Arial;color:#5b748a"><B
								style="mso-bidi-font-weight:normal"><A name="section3">What
							should I do if my magazine does not arrive?</A></B></SPAN><BR>
							</FONT><FONT face="Arial" color="black"><SPAN
								style='font-size:10.0pt;font-family:Arial;mso-fareast-font-family:"Times New Roman";
mso-ansi-language:EN-US;mso-fareast-language:EN-US;mso-bidi-language:AR-SA'>If
							your magazine does not arrive or arrives in poor condition, you
							can do one of the following</SPAN> :</FONT><BR>

							<FONT face="Arial" color="black"><SPAN
								style='font-size:10.0pt;font-family:Arial;mso-fareast-font-family:"Times New Roman";
mso-ansi-language:EN-US;mso-fareast-language:EN-US;mso-bidi-language:AR-SA'><SPAN
								style='font-size:10.0pt;font-family:Arial;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
EN-US;mso-bidi-language:AR-SA'>1.
							Visit <SPAN style="color:black"><B><A
								href="http://www.mysportsweekly.com/">www.mysportsweekly.com</A></B>,
							click on the <A
								href="https://service.usatoday.com/account/cp/delissue.faces"><B>Report
							A Delivery Problem</B></A> link and select the appropriate issue.</SPAN></SPAN></SPAN></FONT>
							<FONT face="Arial" color="black"><SPAN
								style='font-size:10.0pt;font-family:Arial;mso-fareast-font-family:"Times New Roman";
mso-ansi-language:EN-US;mso-fareast-language:EN-US;mso-bidi-language:AR-SA'></SPAN><SPAN
								style='font-size:10.0pt;font-family:Arial;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
EN-US;mso-bidi-language:AR-SA'><BR>
							2. Call 1-800-872-1415 (M-F 8:00AM - 7:00PM ET) <SPAN
								style="color:black">and talk to a Customer Service Professional</SPAN></SPAN>.</FONT>

							<FONT color="blue" face="Arial"><SPAN
								style="font-size:10.0pt;font-family:Arial"><FONT color="black"
								face="Arial"></FONT><FONT face="Arial"><A href="#section99"><FONT
								size="2" face="Arial" color="blue"><BR>
							Back to top</FONT></A></FONT><FONT face="Arial" color="black"></FONT></SPAN><FONT
								color="blue" face="Arial"> <BR>
							</FONT><BR>


							</FONT><FONT face="Arial"><SPAN
								style="font-size:10.0pt;font-family:Arial;
color:black"><B>Billing
							and Payment</B></SPAN><BR>
							<BR>
							<B><SPAN
								style='font-size:10.0pt;
font-family:Arial;mso-fareast-font-family:"Times New Roman";color:#5b748a;
mso-ansi-language:EN-US;mso-fareast-language:EN-US;mso-bidi-language:AR-SA'><SPAN
								style="mso-bookmark:section8"><A name="section4">How can I pay
							for my subscription?</A></SPAN></SPAN><SPAN
								style="mso-bookmark:section8"><SPAN
								style='font-size:12.0pt;font-family:Arial;
mso-fareast-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US;
mso-fareast-language:EN-US;mso-bidi-language:AR-SA'>&nbsp;</SPAN></SPAN></B>
							</FONT><FONT color="blue" face="Arial"><BR>
							</FONT><FONT face="Arial"><SPAN
								style="font-size:10.0pt;font-family:Arial;color:black">Online
							subscription orders must be paid for with a credit card.&nbsp; </SPAN><B
								style="mso-bidi-font-weight:normal"><I
								style="mso-bidi-font-style:normal"><SPAN
								style="font-size:10.0pt;font-family:Arial;color:red">Sports
							Weekly</SPAN></I></B><SPAN
								style="font-size:10.0pt;font-family:Arial;color:black"> accepts
							American Express, VISA, MasterCard, Discover, and Diners Club.<BR>
							</SPAN> </FONT>
							<P style="margin:0in;margin-bottom:.0001pt"><FONT face="Arial"><SPAN
								style="font-size:10.0pt;
font-family:Arial;color:black">If you
							do not have a credit card and wish to pay by check or money
							order, please send subscription information and payment to:</SPAN>
							</FONT></P>
							<P
								style="margin-top:0in;margin-right:0in;margin-bottom:0in;margin-left:.5in;
margin-bottom:.0001pt"><FONT
								face="Arial"><SPAN
								style="font-size:10.0pt;font-family:Arial;
color:black">Sports
							Weekly</SPAN></FONT></P>
							<P
								style="margin-top:0in;margin-right:0in;margin-bottom:0in;margin-left:.5in;
margin-bottom:.0001pt"><FONT
								face="Arial"><SPAN
								style="font-size:10.0pt;font-family:Arial;
color:black">Attn:
							Subscription Processing Center</SPAN></FONT></P>
							<P
								style="margin-top:0in;margin-right:0in;margin-bottom:0in;margin-left:.5in;
margin-bottom:.0001pt"><FONT
								face="Arial"><SPAN
								style="font-size:10.0pt;font-family:Arial;
color:black">National
							Customer Service<BR>
							7950 Jones Branch Drive<BR>
							McLean, Virginia 22108</SPAN><BR>
							</FONT></P>
							<FONT face="Arial" color="blue"> <BR>
							<SPAN style="font-size:10.0pt;
font-family:Arial;color:black"><FONT
								face="Arial">For other payment options, please call us at
							1-800-872-1415 (M-F 8:00AM - 7:00PM ET)</FONT></SPAN>. <BR>
							<A href="#section99"><FONT size="2" face="Arial" color="blue">Back
							to top</FONT></A><BR>
							<BR>
							</FONT><FONT face="Arial"><A name="EZPAY"></A><A name="section5"><B><SPAN
								style="font-size:10.0pt;
font-family:Arial;color:#5b748a">What
							is EZ-PAY?</SPAN></B></A></FONT><SPAN
								style="font-family:Arial;color:blue"> </SPAN><BR>
							<SPAN
								style='font-size:10.0pt;font-family:Arial;mso-fareast-font-family:"Times New Roman";
color:black;mso-ansi-language:EN-US;mso-fareast-language:EN-US;mso-bidi-language:
AR-SA'>EZ-PAY
							is a convenient payment option for subscribers. <SPAN
								style="mso-spacerun:yes">&nbsp;</SPAN>At the end of your term,
							your card will be charged automatically to continue the subscription
							at the current published rate unless you notify us otherwise. <SPAN
								style="mso-spacerun:yes">&nbsp;</SPAN>With EZ-PAY we remember so
							you don't have to!</SPAN> <BR>
							<FONT face="Arial" color="blue"><A href="#section99"><FONT
								size="2" face="Arial" color="blue">Back to top</FONT></A> <FONT
								face="Arial"></FONT><BR>
							</FONT><FONT face="Arial"><BR>
							<B style="mso-bidi-font-weight:normal"><SPAN
								style="font-size:10.0pt;font-family:Arial;color:#5b748a"><A
								name="section6">How do I make a change to EZ-PAY?</A><SPAN
								style="mso-spacerun:yes">&nbsp; </SPAN></SPAN></B> </FONT>
							<P style="margin:0in;margin-bottom:.0001pt"><FONT face="Arial"><SPAN
								style="font-size:10.0pt;
font-family:Arial;color:black">If you
							decide that you want to change your card number or do not want to
							be on EZ-PAY just let us know. <SPAN style="mso-spacerun:yes">&nbsp;</SPAN>You
							can send us an e-mail through </SPAN><SPAN
								style="font-size:10.0pt;font-family:Arial;color:blue"><A
								href="https://service.usatoday.com/feedback/feedback.jsp"><B>Feedback</B></A></SPAN><SPAN
								style="font-size:10.0pt;font-family:Arial;color:black">, call us
							at 1-800-872-1415&nbsp;(M-F 8:00AM - 7:00PM ET) or send us your
							EZ-PAY service cancellation request in writing to: </SPAN></FONT></P>
							<P class="MsoNormal"
								style="margin-top:2.5pt;margin-right:0in;margin-bottom:2.5pt;
margin-left:.5in;text-indent:.5in"><FONT
								face="Arial"><SPAN
								style="font-size:10.0pt;font-family:
Arial;color:black">Sports
							Weekly</SPAN></FONT></P>
							<P class="MsoNormal"
								style="margin-top:2.5pt;margin-right:0in;margin-bottom:2.5pt;
margin-left:.5in;text-indent:.5in"><FONT
								face="Arial"><SPAN
								style="font-size:10.0pt;font-family:
Arial;color:black">Attn:
							EZ-PAY Coordinator</SPAN><SPAN style="color:black"></SPAN></FONT></P>
							<P class="MsoNormal"
								style="margin-top:2.5pt;margin-right:0in;margin-bottom:2.5pt;
margin-left:.5in;text-indent:.5in"><FONT
								face="Arial"><SPAN
								style="font-size:10.0pt;font-family:
Arial;color:black">National
							Customer Service</SPAN></FONT></P>
							<P class="MsoNormal"
								style="margin-top:2.5pt;margin-right:0in;margin-bottom:2.5pt;
margin-left:.5in;text-indent:.5in"><FONT
								face="Arial"><SPAN
								style="font-size:10.0pt;font-family:
Arial;color:black">7950
							Jones Branch Drive</SPAN></FONT></P>
							<P class="MsoNormal"
								style="margin-top:2.5pt;margin-right:0in;margin-bottom:2.5pt;
margin-left:.5in;text-indent:.5in"><FONT
								face="Arial"><SPAN
								style="font-size:10.0pt;font-family:
Arial;color:black">McLean,
							VA 22108</SPAN><SPAN style="color:black"></SPAN></FONT></P>
							<P class="MsoNormal"
								style="margin-top:2.5pt;margin-right:0in;margin-bottom:2.5pt;
margin-left:.5in;text-indent:.5in"></P>
							<P style="margin:0in;margin-bottom:.0001pt"><FONT face="Arial"><SPAN
								style="font-size:10.0pt;
font-family:Arial;color:black">Please
							include your account number, name, and address to ensure we
							accurately process your request.<SPAN style="mso-spacerun:yes">&nbsp;
							</SPAN>Once your EZ-PAY service has been cancelled, you will
							receive renewal notices via mail to continue your subscription.<BR>
							<A href="#section99"><FONT size="2" face="Arial" color="blue">Back
							to top</FONT></A> <BR>
							</SPAN></FONT><FONT face="Arial"><B><SPAN
								style="font-size:10.0pt;font-family:Arial;
color:black"><BR>
							Managing your Subscription Online <BR>
							<BR>
							</SPAN><SPAN
								style="font-size:10.0pt;font-family:Arial;
color:#5b748a"><A
								name="section7">How do I access my account online?</A></SPAN></B><BR>
							<SPAN style="mso-bookmark:section9"><SPAN
								style="font-size:10.0pt;font-family:Arial;color:black">Visit </SPAN><A
								href="http://www.usatodayservice.com/"><B><SPAN
								style="font-size:10.0pt;font-family:Arial">www.mysportsweekly.com</SPAN></B></A><SPAN
								style="font-size:10.0pt;font-family:Arial;color:black">, click
							on the </SPAN><SPAN
								style="font-size:10.0pt;font-family:Arial;color:#5B748A"><B
								style="mso-bidi-font-weight:normal">Online Account Setup</B></SPAN><SPAN
								style="font-size:10.0pt;font-family:Arial;
color:black"> link
							and complete the form.<BR>
							</SPAN><SPAN
								style="font-size:10.0pt;font-family:Arial;color:blue;mso-bidi-font-weight:bold;
mso-bidi-font-style:italic"><A
								href="#section99"><FONT size="2" face="Arial" color="blue">Back
							to top</FONT></A></SPAN><SPAN
								style="font-size:10.0pt;font-family:Arial;color:#5b748a"><B><BR>
							<BR>
							<A name="section28"></A><A name="section8">Where can I find my
							account number?</A></B></SPAN></SPAN><SPAN
								style="font-size:10.0pt;font-family:Arial;color:black"><B> </B></SPAN><BR>
							</FONT></P>

							<P style="margin:0in;margin-bottom:.0001pt"><FONT face="Arial"><SPAN
								style="font-size:10.0pt;
font-family:Arial;color:black">This
							can be found on the label of your magazine or on your bill. <SPAN
								style="mso-spacerun:yes">&nbsp;</SPAN>If your magazine does not
							have a label, then you will need to get your account number by
							calling our National Customer Service center at 1-800-USA-1415
							(M-F 8:00AM - 7:00PM ET). <SPAN style="mso-spacerun:yes">&nbsp;</SPAN>If
							you are unable to call us during this time, please send us your
							request through <A
								href="https://service.usatoday.com/feedback/feedback.jsp"><B>Feedback</B></A>
							and we will e-mail your account number to you.</SPAN><BR>
							<A href="#section99"><FONT size="2" face="Arial" color="blue">Back
							to top</FONT></A> <BR>
							<BR>
							<B><A name="section9"><SPAN
								style="font-size:10.0pt;font-family:Arial;color:#5b748a">How do
							I make a change to my account information?</SPAN></A></B><SPAN
								style="mso-bookmark:section11"></SPAN><BR>
							<SPAN style="font-size:10.0pt;
font-family:Arial;color:black">You
							can make changes to your account, address, or phone number by
							clicking on</SPAN><SPAN style="font-family:Arial;color:black"> </SPAN><SPAN
								style="font-size:10.0pt;font-family:Arial;color:black"><A
								href="https://service.usatoday.com/account/accounthistory/accountSummary.faces"><B>Customer
							Service</B></A> or by calling our National Customer Service at
							1-800-872-1415 (M-F 8:00AM - 7:00PM ET).</SPAN><BR>
							<A href="#section99"><FONT size="2" face="Arial" color="blue">Back
							to top</FONT></A><BR>
							<SPAN
								style="font-size:10.0pt;font-family:Arial;
color:blue;mso-bidi-font-weight:bold;mso-bidi-font-style:italic"></SPAN><BR>
							<A name="section10"><B><SPAN
								style="font-size:10.0pt;font-family:Arial;color:#5b748a">Why
							can't I see the changes I made to my account immediately?<BR>
							</SPAN></B></A><SPAN style="mso-bookmark:section12"></SPAN><SPAN
								style="font-size:10.0pt;font-family:Arial;color:black">When you
							make a change to your account information online, it may take up
							to 48 hours for your change to take effect.<BR>
							</SPAN><A href="#section99"><FONT size="2" face="Arial"
								color="blue">Back to top</FONT></A> <BR>




							<A name="section11"><B><SPAN
								style="font-size:10.0pt;font-family:Arial;color:#5b748a"><BR>
							How do I temporarily stop my subscription if I go on vacation?<BR>
							</SPAN></B></A><SPAN style="mso-bookmark:section13"></SPAN><SPAN
								style="font-size:10.0pt;font-family:Arial;color:black">Simply go
							to the <A
								href="https://service.usatoday.com/account/vacationholds/holds.faces"><B>Suspend/Resume
							Delivery</B></A> link to temporarily stop your magazine.
							&nbsp;You can submit multiple vacation hold requests in advance
							if you are going to be traveling in intervals.&nbsp; Please let
							us know at least seven business days in advance when placing
							these requests online.</SPAN><BR>
							<SPAN
								style="font-size:10.0pt;font-family:Arial;
color:blue;mso-bidi-font-weight:bold;mso-bidi-font-style:italic"><SPAN
								style="font-size:10.0pt;font-family:Arial;
color:blue;mso-bidi-font-weight:bold;mso-bidi-font-style:italic"><A
								href="#section99"><FONT size="2" face="Arial" color="blue">Back
							to top</FONT></A><BR>
							<BR>
							<SPAN style="font-size:10.0pt;font-family:Arial;
color:black"><B>Other
							Services and Questions</B></SPAN><BR>
							<FONT size="2" face="Arial"><B> <BR>
							<A name="section29"><FONT size="2" face="Arial" color="#5b748a">How
							do I order multiple copies of the magazine?</FONT></A><BR>
							<FONT color="black" size="2" face="Arial"></FONT></B><FONT
								color="black" size="2" face="Arial"><SPAN
								style="FONT-SIZE: 10pt; FONT-FAMILY: Arial">You can have
							multiple copies delivered to the same address by selecting the
							number of copies option on the same page you select your
							subscription term. &nbsp;If you are ordering more than 10 copies,
							you may be eligible for a group discount.&nbsp; To learn more,
							please contact our customer service department at 1-800-USA-0001.</SPAN></FONT><BR>
							</FONT><A href="#section99"><FONT size="2" face="Arial"
								color="blue">Back to top</FONT><BR>
							</A><BR>
							<SPAN style="font-size:10.0pt;font-family:Arial;
color:#5b748a"><B><A
								name="section13">If I am purchasing a subscription as a gift,
							how do I request a gift announcement card?</A></B></SPAN><SPAN
								style="font-family:Arial;color:black"><BR>
							</SPAN><B style="mso-bidi-font-weight:normal"><I
								style="mso-bidi-font-style:
normal"><SPAN
								style="font-size:10.0pt;font-family:Arial;color:red">Sports
							Weekly</SPAN></I></B><SPAN
								style="font-size:10.0pt;font-family:Arial;color:black"> provides
							FREE gift announcement cards. <SPAN style="mso-spacerun:yes">&nbsp;</SPAN>To send a gift card via email, visit  <a href="/giftcard/sendcard.faces"><u>GIFT CARD</u></SPAN><SPAN
								style="font-family:Arial;color:black"> <BR>
							</SPAN><A href="#section99"><FONT size="2" face="Arial"
								color="blue">Back to top</FONT></A><SPAN
								style="mso-bookmark:section15"> <BR>
							<BR>
							</SPAN><B><SPAN style="mso-bookmark:section17"><SPAN
								style="font-size:10.0pt;font-family:
Arial;color:#5b748a"><A
								name="section14">Why does my address get reformatted when I
							submit it?</A></SPAN><BR>
							</SPAN></B><B style="mso-bidi-font-weight:
normal"><I
								style="mso-bidi-font-style:normal"><SPAN
								style="font-size:10.0pt;
font-family:Arial;color:red">Sports
							Weekly</SPAN></I></B><SPAN
								style="font-size:10.0pt;font-family:Arial;color:black"> runs the
							submitted address information through an address hygiene software
							program.&nbsp; It is recommended that you choose the returned
							address to ensure the best delivery of your magazine either by
							mail or carrier delivery.&nbsp;<BR>
							</SPAN><A href="#section99"><FONT size="2" face="Arial"
								color="blue">Back to top</FONT></A><BR>
							<BR>
							<SPAN style="font-size:10.0pt;
font-family:Arial;color:#5b748a"><B><A
								name="section15">What is a back issue?</A></B></SPAN></SPAN></SPAN><BR>
							</FONT> <SPAN style="font-size:10.0pt;color:red"><I
								style="mso-bidi-font-style:normal"><B>Sports Weekly</B></I></SPAN><SPAN
								style="font-size:10.0pt;color:black"> keeps an inventory of
							previously published print editions of the magazine.<SPAN
								style="mso-spacerun:yes">&nbsp; </SPAN>You may purchase one of
							these editions for a nominal cost, plus shipping &amp; handling.<SPAN
								style="mso-spacerun:yes">&nbsp; </SPAN>To request a back issue,
							call our National Customer Service center at 1-800-USA-1415 (M-F
							8:00AM - 7:00PM ET).</SPAN><BR>
							<A href="#section99"><FONT size="2" face="Arial" color="blue">Back
							to top</FONT></A><BR>
							&nbsp;</P>
							</TD>
							<%=com.usatoday.esub.common.UTCommon.displayPubGraphics(request)%>
						</TR>
						<TR>
							<TD valign="top"></TD>
						</TR>
					</TBODY>
				</TABLE>
			<%-- /tpl:put --%>
		</DIV>
	<%-- /tpl:put --%>
				<DIV id="clear"></DIV>
				<DIV id="bluebar">
					<a href="/feedback/feedback.jsp"
						style="color: white; font-weight: bold; text-decoration: none; font-size: 14px"
						title="Contact Us">Contact Us</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;1-800-872-0001&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a
						href="/faq/utfaq.jsp"
						style="color: white; font-weight: bold; text-decoration: none; font-size: 14px"
						title="Frequently Asked Questions">FAQ</a>
				</DIV>
				<%-- tpl:put name="footerMainArea" --%>
	<%-- tpl:put name="footerarea1" --%><%-- /tpl:put --%>
		<%=trackingBug.getOmnitureHTML() %>
	<%-- /tpl:put --%>
				<DIV id=graybar>
					Copyright
					<script>
						document.write(getCurrentYear());
					</script>
					USA TODAY, a division of <a href="http://www.gannett.com">Gannett
						Co. Inc.</a> <a href="/privacy/privacy.htm">Privacy
						Notice/California Privacy Notice</a>. <a
						href="/privacy/privacy.htm#adchoice">Ad Choices</a>.<br> By
					using this service, you accept our <a href="/service/service.jsp">Terms
						of Service</a>.
				</DIV>
				<DIV id=copyright>
					<P>Offers available to new subscribers only. Home delivery not
						available in some areas. Not valid with any other offer.
						Prepayment required. After the promotional period ends, the
						subscription will continue and be billed monthly at the then
						regular rate, less any applicable credits, unless USA TODAY is
						otherwise notified. Applicable taxes may not be included in the
						rate. If at any time you decide to cancel your subscription, you
						may contact customer service at 1-800-872-0001 and the full amount
						of any balance over $2.00 will be returned.</P>
				</DIV>
				<!--END OF COPYWRITE-->
				<DIV id=footer></DIV>
			</DIV>
			<!--END OF WHITEBOX-->
		</DIV>
		<!--END OF CONTENT-->
	</DIV>
</BODY>
</HTML><%-- /tpl:insert --%><%-- /tpl:insert --%>