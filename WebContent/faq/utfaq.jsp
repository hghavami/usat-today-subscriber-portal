<%-- tpl:insert page="/theme/subscriptionPortalJSPNoNavigation.jtpl" --%><!DOCTYPE HTML><%@page import="com.usatoday.business.interfaces.products.ProductIntf"%>
<%@page import="com.usatoday.businessObjects.products.ProductBO"%>
<%@page import="com.usatoday.util.MobileDeviceDetectionUtil"%><%-- tpl:insert page="/theme/subscriptionPortalRoot.htpl" --%><HTML>
<HEAD>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8">
<%-- tpl:put name="headerareaTop" --%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%
	String docProtocol = "https:";  
	if (!request.isSecure()) {
		docProtocol = "http:";
	}
	String initFunction = "";  // override this value if need be on page load
	
	com.usatoday.esub.common.WebBug trackingBug = com.usatoday.esub.common.UTCommon.getWebAnalyticsBug();
	String pubCode = com.usatoday.util.constants.UsaTodayConstants.UT_PUBCODE;
	String productName = "USA TODAY";
	com.usatoday.business.interfaces.products.OfferIntf offer = null;
	try {
		
		offer = com.usatoday.esub.common.UTCommon.getCurrentOffer(request);
		
		if (offer != null) {
			pubCode = offer.getPubCode();
			try {
				String brandingPubCode = offer.getProduct().getBrandingPubCode();
				ProductIntf bp = ProductBO.fetchProduct(brandingPubCode);
				productName = bp.getName();
			}
			catch (Exception teee) {
			}				
		}
		// figure out tracking
		trackingBug.deriveValuesFromRequest(request);
	}
	catch (Exception e) {
		; // ignore
	}

	com.usatoday.businessObjects.products.promotions.PromotionSet defaultPromotionSet = null;
	//com.usatoday.businessObjects.products.promotions.PromotionSet currentOfferPromotionSet = null;
	
	com.usatoday.business.interfaces.products.promotions.ImagePromotionIntf imagePromo = null;
	
	String mobileCSS = "";
	if (MobileDeviceDetectionUtil.isSmallScreenDevice(request)) {
		mobileCSS = "<LINK href=\"/theme/themeV3/css/smallScreen.css\" rel=\"stylesheet\" type=\"text/css\">";
	}	
%>
<script language="JavaScript" src="/common/prototype/prototype.js"></script>
<script src="/common/prototype/scriptaculous/scriptaculous.js" type="text/javascript"></script>
<%-- tpl:put name="headarea_Top" --%>
		<A name="section99"> </A>
		<TITLE>USA Today Online Subscriptions - FAQ</TITLE>
	<%-- /tpl:put --%>
<%-- /tpl:put --%>
<LINK REL="SHORTCUT ICON" HREF="/favicon.ico">
<LINK REL="icon" href="/favicon.ico" TYPE="image/ico" />
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META http-equiv="Content-Style-Type" content="text/css">
<meta name="viewport" content="initial-scale=1.0">
<LINK href="/theme/themeV3/css/usatodayBase.css" rel="stylesheet"
	type="text/css">
<LINK href="/theme/themeV3/css/usatodayAddOn.css" rel="stylesheet"
	type="text/css">
<script src="/common/usatCommon.js"></script>
<%-- tpl:put name="headareaBottom" --%>
<%=mobileCSS %>
<%-- tpl:put name="headarea_Bottom" --%>
	<%-- /tpl:put --%>
<script>			
	function initPage() {

		// for product messageing in popUpOverlay.js
		usat_productName = "<%=productName %>";
				
		<%=initFunction %>
	}	
</script>		
	<%-- /tpl:put --%>
<script type="text/javascript">
	/*   var _gaq = _gaq || [];
	 _gaq.push(['_setAccount', 'UA-23158809-1']);
	 _gaq.push(['_trackPageview']);

	 (function() {
	 var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	 ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	 var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	 })();
	 */
</script>
</HEAD>
<body>
	<DIV id="wrapper">
		<DIV id="header">
			<div id="headerLeft" onclick="templateLogoClick();"></div>
			<div id="headerRight">
				<%-- tpl:put name="headerbodyarea" --%>
					<%-- tpl:insert page="/login/loginform.jspf" --%><div style="float: none">
	<%
		String uri = request.getRequestURI();
		String pageName = uri.substring(uri.lastIndexOf("/") + 1);
		String jScript = "";
		if (uri.indexOf("subscriptions") > 0 && uri.indexOf("thankyou.jsp") > 0) {
			try {
				com.usatoday.esub.handlers.ShoppingCartHandler shoppingCartHandler = (com.usatoday.esub.handlers.ShoppingCartHandler) session
						.getAttribute("shoppingCartHandler");
				if (shoppingCartHandler != null) {
					jScript = shoppingCartHandler.getOnePageThankYouJavaScriptTealiumText();
				}
			} catch (Exception E) {

			}
		} else {
			com.usatoday.businessObjects.products.promotions.PromotionSet currentOfferPromotionSetTealium = null;
			com.usatoday.business.interfaces.products.OfferIntf offerTealium = com.usatoday.esub.common.UTCommon
					.getCurrentOffer(request);
			if (offerTealium != null && offerTealium instanceof com.usatoday.business.interfaces.products.SubscriptionOfferIntf) {
				currentOfferPromotionSetTealium = ((com.usatoday.business.interfaces.products.SubscriptionOfferIntf) offerTealium)
						.getPromotionSet();
				jScript = currentOfferPromotionSetTealium.getPromoOnePageJavaScriptTealiumText();
				jScript = org.apache.commons.lang3.StringUtils.replace(jScript, "page_name:\"\"", "page_name:\"" + uri + "\"");
			}
		}
	%>
	<%=jScript%>
</div>
<%-- 			<f:verbatim>
				<h:outputText styleClass="outputText" id="textThankYouJS13"	escape="false" value="#{shoppingCartHandler.tealiumOnePageThankYouJavaScriptText}"></h:outputText>
			</f:verbatim>
 --%>
<%
	String isLoginDisplay = "true";
	com.usatoday.business.interfaces.customer.CustomerIntf customer2 = null;
	String loggedinDivClass = "hideMe";
	String notLoggedinDivClass = "showMeInline";
	String firstName = "";
	String lastName = "";
	String firmInfo = "";
	String accountNum = "";
	String loginErrorMessage2 = "";
	String logOut = "";
	String publication = "";
	String changeAccountLink = "";
	String earlyAlertDisplay = "display:none";

	String jScriptNoOp = "javascript:;";

	try {
		publication = (String) session.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_PUB_NAME);
		if (publication == null) {
			publication = request.getParameter("pub");
			if (publication == null) {
				publication = com.usatoday.util.constants.UsaTodayConstants.UT_PUBCODE;
			}
		}
		if (publication.equals(com.usatoday.esub.common.UTCommon.PUB_NAMES[0])) { // USATODAY publication
			logOut = com.usatoday.esub.common.UTCommon.BASE_URL + "/index.jsp?pub=UT&logsessobj=rem";
		} else {
			logOut = com.usatoday.esub.common.UTCommon.BASE_URL + "/index.jsp?pub=BW&logsessobj=rem";
		}

		// If customer is logged in remove login page, otherwise display it
		customer2 = (com.usatoday.business.interfaces.customer.CustomerIntf) session
				.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_CUSTOMER_INFO);
		if (customer2 == null) {
			// showMeInling and hideMe are defined in style sheets
			loggedinDivClass = "showMeInline";
			notLoggedinDivClass = "hideMe";
		} else {

			if (customer2.getCurrentAccount() != null) {
				firstName = customer2.getCurrentAccount().getDeliveryContact().getFirstName();
				if (firstName != null && !firstName.trim().equals("")) {
					firstName = com.usatoday.esub.common.UTCommon.formatNameCapitalization(firstName); // Format it
				}
				lastName = customer2.getCurrentAccount().getDeliveryContact().getLastName();
				if (lastName != null && !lastName.trim().equals("")) {
					lastName = com.usatoday.esub.common.UTCommon.formatNameCapitalization(lastName); // Format it
				}
				firmInfo = customer2.getCurrentAccount().getDeliveryContact().getFirmName();
				if (customer2.getNumberOfAccounts() > 1) {
					changeAccountLink = "[&nbsp;<a href=\"/account/select_accnt.jsp\">Change</a>&nbsp;]";
				}
				try {
					accountNum = customer2.getCurrentAccount().getAccountNumber();
					//				if (accountNum == null || accountNum.length() < 4) {
					if (accountNum == null) {
						accountNum = "";
					}
					//				else {
					/*
					if (accountNum.equalsIgnoreCase("340000000")) {
						accountNum = "Pending Setup";
					}
					 */
					//				}
				} catch (Exception eeee) {
					eeee.printStackTrace();
				}

				//EarlyAlert - Check to see if their paper delivery has been affected by a delay
				// HD-CONS109 SEW
				/*
				if (customer2.getCurrentAccount().isShowDeliveryAlert()) {
				earlyAlertDisplay = "display:inline";
				}
				 */

			} else {
				if (customer2.getCurrentEmailRecord() != null
						&& customer2.getCurrentEmailRecord().getEmailAddress() != null) {
					firstName = customer2.getCurrentEmailRecord().getEmailAddress();
					accountNum = "Pending Setup";
				} else {
					firstName = "";
				}
			}
		}
		loginErrorMessage2 = (String) session.getAttribute("LOGIN_FAILED_MESSAGE");
		if (loginErrorMessage2 == null || loginErrorMessage2.trim().equals("")) {
			loginErrorMessage2 = "";
		} else {
			session.removeAttribute("LOGIN_FAILED_MESSAGE");
		}
	} catch (Exception e) {
		e.printStackTrace();
		if (firstName == null) {
			firstName = "";
		}
		if (lastName == null) {
			lastName = "";
		}
	}
	String nameInfo = firstName + " " + lastName;
	String loginInfo = null;
	if (!nameInfo.trim().equals("")) {
		loginInfo = nameInfo;
	} else {
		if (firmInfo != null && !firmInfo.trim().equals("")) {
			loginInfo = firmInfo;
		}
	}
	// if no login info, then hide data
	if (loginInfo == null) {
		loggedinDivClass = "showMeInline";
		notLoggedinDivClass = "hideMe";
		loginInfo = "";
	}
%>

<%@page import="com.usatoday.util.constants.UsaTodayConstants"%><div
	id="loginDisplayDiv" class="<%=loggedinDivClass%>">
	<table border="0" width="100%" height="60px">
		<tr>
			<td>
				<TABLE class="loginFormTable">
					<TBODY>
						<TR>
							<TD class="loginFormColumn"><br /> <a
								href="/login/auth.faces">Login to Manage Account</a></TD>
						</TR>
						<tr>
							<td class="loginFormColumn" align="right"><img
								style="cursor: pointer; margin-top: 10px;"
								onclick="javascript: templateClickToChatClickedGuest(this, event);"
								alt="Click for Assistance" title="Click for Assistance"
								src="${pageContext.request.contextPath}/theme/themeV3/themeImages/chatIcon136.png"
								hspace="0" vspace="5"></td>
						</tr>
					</TBODY>
				</TABLE></td>
		</tr>
	</table>
</div>
<div id="loginHideDiv" class="<%=notLoggedinDivClass%>">
	<table width="100%" border="0" class="loginFormTable">
		<tr>
			<td nowrap align="right">
				<TABLE class="loginFormTable">
					<TBODY>
						<TR>
							<TD class="loginFormColumn" nowrap><span
								class="loginNormalPrint">Hello, <%=loginInfo%>&nbsp;&nbsp;[&nbsp;<a
									href="<%=jScriptNoOp%>"
									onclick="javascript:document.location='<%=logOut%>';_unloadPage(this, event);">Logout</a>&nbsp;]</span>
							</TD>
						</TR>
					</TBODY>
				</TABLE></TD>
		</tr>
		<tr>
			<td class="loginFormColumn" align="right"><span
				class="loginfineprint">Account #:&nbsp;<%=accountNum%>&nbsp;<%=changeAccountLink%></span>
			</td>
		</tr>
		<tr>
			<td class="loginFormColumn" align="right"><div
					id="chatIconRequestDiv">
					<img onclick="javascript: templateClickToChatClicked(this, event);"
						style="cursor: pointer; margin-top: 10px;"
						alt="Click for Assistance" title="Click for Assistance"
						src="${pageContext.request.contextPath}/theme/themeV3/themeImages/chatIcon136.png"
						hspace="0" vspace="5">
				</div>
			</td>
		</tr>
	</table>
</div>
<div id="templateChatWindowDiv" style="display: none;">
	<div id="templateChatTopDiv">
		<span onclick="templateClickToChatHide();" style="cursor: pointer;">No
			Thanks<img src="/theme/themeV3/themeImages/close_14x14.png"
			width="14" height="14" border="0"
			style="margin-left: 5px; margin-right: 5px" /> </span>
	</div>
<!-- 	<div id="tempateChatImageDiv">
		<a
			onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
			href=""><img border="0"
			src="https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY" />
		</a>
	</div>
 -->	<div id="templateChatAccountLinksDiv">
		<ul>
			<li>To report a problem with delivery, click <a
				href="/account/cp/delissue.faces"> here</a>.</li>
			<li>To place your delivery on a temporary hold, click <a
				href="/account/vacationholds/holds.faces"> here</a>.</li>
			<li>Forgot your password or login? Click <a
				href="/idpassword/forgotpassword.jsp"> here</a>.
			<li>To contact us <a href="/feedback/feedback.jsp"> click here</a>.</li>				
<!-- 			<li>To cancel your subscription, please call 1-800-872-0001 and -->
<!-- 				select option 6.</li> -->
		</ul>
	</div>

	<div id="templateChatLinksDiv">
		<ul>

			<li><a
				onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
				href="">Click to Chat</a>
			</li>
		</ul>
	</div>
	<div id="templateChatBottomDiv">
		<span class="outputText">Or, call us at:<br> <b>Call
				1-877-713-6241</b><br>(M-F 8:00 a.m. to 7 p.m. E.T.)</span>
	</div>
</div>
<div id="templateChatWindowGuestDiv" style="display: none;">
	<div id="templateChatTopDiv">
		<span onclick="templateClickToChatHideGuest();"
			style="cursor: pointer;">No Thanks<img
			src="/theme/themeV3/themeImages/close_14x14.png" width="14"
			height="14" border="0" style="margin-left: 5px; margin-right: 5px" />
		</span>
	</div>
<!-- 	<div id="tempateChatImageDiv">
		<a
			onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
			href=""><img border="0"
			src="https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY" />
		</a>
	</div>
 -->	<div id="templateChatAccountLinksDiv">
		<ul>
			<li>To manage your account online - including reporting a
				problem with delivery or placing a temporary hold on your account -
				log in by entering your e-mail address and password. Click <a
				href="/login/auth.faces"> here</a> to access the login page now.</li>
			<li>Forgot your password or login? <a
				href="/idpassword/forgotpassword.jsp"> click here</a>.</li>
			<li>To contact us <a href="/feedback/feedback.jsp"> click here</a>.</li>
			<!-- 			<li>To cancel your subscription, please call 1-800-872-0001 and -->
			<!-- 				select option 6.</li> -->
		</ul>

	</div>

	<div id="templateChatLinksDiv">
		<ul>
			<li><a
				onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
				href="">Click to Chat</a>
			</li>
		</ul>
	</div>
	<div id="templateChatBottomDiv">
		<span class="outputText">Or, call us at:<br> <b>Call
				1-877-713-6241</b><br>(M-F 8:00 a.m. to 7 p.m. E.T.)</span>
	</div>
</div>
<%-- /tpl:insert --%>
		
	<%-- /tpl:put --%>
			</div>
		</DIV>
		<!-- end header -->
		<DIV id="content">
			<DIV id="whitebox">
				<%-- tpl:put name="bodyAreaMainWhiteBox" --%>
		<DIV id="pageContentFullWidth">
			<%-- tpl:put name="bodyarea" --%>
							<H1>USA TODAY FAQ</H1>
							<TABLE border="0" width="774" cellspacing="0" cellpadding="0">
								<TBODY>
									<TR>
										<TD width="5"></TD>
										<TD valign="top" width="25"></TD>
										<TD valign="top" width="744">

											<P>
												<SPAN><FONT face="Arial" size="2"><BR>
														Welcome to the Frequently Asked Questions (FAQ) page.<SPAN><FONT
															face="Arial" size="2">&nbsp;If you need answers to
																questions not addressed here, please call our National
																Customer Service Department at 1-800-872-0001 (M-F
																8:00AM - 7:00PM ET).</FONT> </SPAN> </FONT> </SPAN>
											</P> <FONT size="2" face="Arial"> <B>For questions
													about USA TODAY e-Newspaper <a href="/faq/eeFAQ.pdf">click
														here<br> </a> </b><br> <FONT size="2" face="Arial">
													<B>For questions about <FONT size="2" face="Arial"
														color="red"><I>Sports Weekly</I> </FONT> <a
														href="/faq/bwfaq.jsp">click here</a><br> <br>
														Features of </B> </FONT><FONT size="2" face="Arial" color="black"><B>USA
														TODAY</B><BR> <A href="#section81"><FONT size="2"
														face="Arial">Why should I subscribe to <B>USA
																TODAY</B>?<BR> </FONT> </A> </FONT><BR> <FONT size="2" face="Arial">
													<B>Delivery of the Newspaper<I> </I> </B> </FONT> <BR> <A
												href="#section1"><FONT size="2" face="Arial">How
														often is USA TODAY published?</FONT> </A><BR> <A
												href="#section3"><FONT size="2" face="Arial">How
														will USA TODAY be delivered</FONT> </A> <BR> <A href="#section4"><FONT
													size="2" face="Arial">At what time should I expect
														my newspaper to be delivered?</FONT> </A><BR> <A
												href="#section5"><FONT size="2" face="Arial">What
														should I do if my newspaper does not arrive?</FONT> </A><BR> <BR>
												<B><FONT size="2" face="Arial" color="black">The
														e-Newspaper of USA TODAY</FONT> </B><BR> <A href="#whatEEdition">
													<FONT size="2" face="Arial">What is the e-Newspaper
														of USA TODAY?</FONT> </A><br> <A href="#whatTimeEE"> <FONT
													size="2" face="Arial">What time of day is the
														e-Newspaper available?</FONT> </A><br> <A href="#addlSubEE">
													<FONT size="2" face="Arial">Does the e-Newspaper
														require an additional subscription?</FONT> </A><br> <A
												href="#whyEE"> <FONT size="2" face="Arial">Why
														should I subscribe to the e-Newspaper of USA TODAY?</FONT> </A><br>
												<A href="#internetEE"> <FONT size="2" face="Arial">Do
														I need to have a connection to the internet when reading
														the e-Newspaper?</FONT> </A><br> <A href="#moreEE"> <FONT
													size="2" face="Arial">Where can I find more
														information about the e-Newspaper of USA TODAY?<br> </FONT>
											</A><BR> <B><FONT size="2" face="Arial" color="black">Billing
														and Payment</FONT> </B><BR> <A href="#section8"> <FONT
													size="2" face="Arial">How can I pay for my
														subscription?</FONT> </A><BR> <A href="#section6"><FONT
													size="2" face="Arial">What is EZ-PAY?</FONT> </A><BR> <A
												href="#section7"><FONT size="2" face="Arial">How
														do I make a change to EZ-PAY?</font> </a><br> <A
												href="#sectionEE1"><FONT size="2" face="Arial">How
														do I give an e-Newspaper gift subscription? </FONT> </A><BR> <BR>
												<B><FONT size="2" face="Arial" color="black">Managing
														your Subscription Online</FONT> </B> <BR> <A href="#section82"><FONT
													size="2" face="Arial">How do I access my account
														online?</FONT> </A><BR> <A href="#section9"><FONT size="2"
													face="Arial">Where can I find my account number?</FONT> </A><BR>
												<A href="#section10"><FONT size="2" face="Arial">How
														do I make a change to my account information?</FONT> </A><BR> <A
												href="#section11"><FONT size="2" face="Arial">Why
														can't I see the changes I made to my account immediately?</FONT>
											</A><BR> <A href="#section12"><FONT size="2"
													face="Arial">How do I temporarily stop my
														subscription if I go on vacation?</FONT> </A><BR> <A
												href="#section13"><FONT size="2" face="Arial">How
														can I help schools when I temporarily stop my
														subscription?</FONT> </A><BR> <BR> <B><FONT size="2"
													face="Arial" color="black">Other Services and
														Questions</FONT> </B> <BR> <A href="#section29"><FONT
													size="2" face="Arial">How do I order multiple copies
														of the newspaper?</FONT> </A><BR> <A href="#section14"><FONT
													size="2" face="Arial">If I am purchasing a
														subscription as a gift, how do I request a gift
														announcement card?</FONT> </A><BR> <A href="#section16"><FONT
													size="2" face="Arial">How do I subscribe to an
														International Edition of USA TODAY?</FONT> </A><BR> <A
												href="#section17"><FONT size="2" face="Arial">Why
														does my address get reformatted when I submit it?</FONT> </A><BR>
												<BR> <B><FONT size="2" face="Arial" color="black">Content
														Licensing, Reprints, Permissions and Back Issues</FONT> </B> <BR>
												<A href="#section21"><FONT size="2" face="Arial">How
														do I order hard copy and digital reprints or license
														permission to republish USA TODAY content?</FONT> </A><BR> <A
												href="#section22"><FONT size="2" face="Arial">What
														is a backissue?</FONT> </A><BR> <A href="#section23"><FONT
													size="2" face="Arial">What are hard copy reprints?</FONT> </A><BR>
												<A href="#section24"><FONT size="2" face="Arial">What
														are digital reprints?</FONT> </A><BR> <A href="#section25"><FONT
													size="2" face="Arial">What are permissions?</FONT> </A><BR>
												<A href="#section26"><FONT size="2" face="Arial">How
														do I obtain copies of the USA TODAY articles I have
														received permission to re-use?</FONT> </A><BR> <BR> <FONT
												size="2" face="Arial" color="black"><B>Features
														of </B> </FONT><FONT size="2" face="Arial" color="black"><B>USA
														TODAY</B><BR> <BR> <b><A name="section81"> <FONT
															color="#5b748a" face="">Why should I subscribe to
																USA TODAY?</FONT> </A> </b><FONT size="2"><BR> <B>USA
															TODAY</B> is the most widely read newspaper in the United
														States. Every day more than 5 million people read <B>USA
															TODAY</B> to get their news. People love the fair, balanced
														and complete coverage that <B>USA TODAY</B> provides, in a
														concise and quick to read format. <B>USA TODAY</B> also
														has exclusive stories and the latest headlines. By
														subscribing, you will enjoy the convenience of home or
														office delivery.<BR> <BR> </FONT><FONT size="2"
													face="Arial" color="green"><BR> </FONT><FONT size="2"
													face="Arial"><B>Delivery of the Newspaper</B> </FONT>
													<P>
														<FONT size="2" face="Arial"><B><A
																name="section1"><FONT color="#5b748a" face="">How
																		often is<B> USA TODAY</B> published?</FONT> </A> </B> </FONT><BR> <FONT
															face="Arial" size="2"><B>USA TODAY</B> is
															published Monday through Friday with the exception of the
															following six holidays:</FONT>
													</P>
													<UL style="margin-top: 5px; margin-bottom: 5px;">
														<b> <FONT size="2" face="Arial">New Years Day</FONT><br>
															<FONT size="2" face="Arial">Memorial Day</FONT><br>
															<FONT size="2" face="Arial">Independence Day</FONT><br>
															<FONT size="2" face="Arial">Labor Day</FONT><br> <FONT
															size="2" face="Arial">Thanksgiving Day</FONT><br> <FONT
															size="2" face="Arial">Christmas Day</FONT><br> </b>
													</UL> <FONT size="2" face="Arial" color="green"></FONT><FONT
													size="2" face="Arial" color="blue"></FONT><FONT size="2"
													face="Arial" color="green"><FONT color="blue"
														face="Arial"><SPAN
															style="font-size: 10.0pt; font-family: Arial"><SPAN
																style="font-size: 10.0pt; font-family: Arial"><FONT
																	face="Arial" color="blue"><FONT face="Arial"><A
																			href="#section99"><FONT size="2" face="Arial"
																				color="009BBF">Back to top</FONT> </A> </FONT> </FONT> </SPAN> </SPAN> </FONT><BR> </FONT><BR>
													<FONT size="2" face="Arial"><B><A
															name="section3"><FONT color="#5b748a">How will
																	USA TODAY be delivered?</FONT> </A> </B> </FONT> <BR> <FONT face="Arial"
													size="2">Depending on where you live, you will
														receive <B>USA TODAY</B> by either carrier delivery (to
														your home or office in the morning) or via the US Postal
														Service.&nbsp; Although the majority of <B>USA TODAY</B>
														subscriptions are delivered either by carrier or same-day
														mail, some areas may be delivered by the US Postal Service
														on a delay.&nbsp; If you have questions about how your
														subscription will be delivered, please call us at
														1-800-872-0001 (M-F 8:00AM - 7:00PM ET).</FONT><BR> <FONT
													size="2" face="Arial" color="green"></FONT><FONT size="2"
													face="Arial" color="blue"></FONT><FONT size="2"
													face="Arial" color="green"><FONT color="blue"
														face="Arial"><SPAN
															style="font-size: 10.0pt; font-family: Arial"><SPAN
																style="font-size: 10.0pt; font-family: Arial"><FONT
																	face="Arial" color="blue"><FONT face="Arial"><A
																			href="#section99"><FONT size="2" face="Arial"
																				color="009BBF">Back to top</FONT> </A> </FONT> </FONT> </SPAN> </SPAN> </FONT> </FONT>
													<P>
														<BR> <FONT face="Arial" size="2"><B><A
																name="section4"><FONT color="#5b748a">What
																		time should I expect my newspaper to be delivered?</FONT> </A><BR>
														</B>Guaranteed delivery times can vary. For carrier delivered
															orders, you can generally expect the newspaper delivered
															to your door by 7:00 am each morning.</FONT>
													</P>
													<P>
														<FONT size="2" face="Arial">For mail delivered
															orders, the newspaper will arrive with your daily mail.
															Customers receiving their newspaper by US Mail will
															experience a delay in delivery on Postal Holidays.<BR>
															<FONT size="2" face="Arial" color="green"></FONT><FONT
															size="2" face="Arial" color="blue"></FONT><FONT size="2"
															face="Arial" color="green"><FONT color="blue"
																face="Arial"><SPAN
																	style="font-size: 10.0pt; font-family: Arial"><SPAN
																		style="font-size: 10.0pt; font-family: Arial"><FONT
																			face="Arial" color="blue"><FONT face="Arial"><A
																					href="#section99"><FONT size="2" face="Arial"
																						color="009BBF">Back to top</FONT> </A> </FONT> </FONT> </SPAN> </SPAN> </FONT><BR> </FONT>
														</FONT>
													</P> <FONT size="2" face="Arial"><B><A
															name="section5"><FONT color="#5b748a">What
																	should I do if my newspaper does not arrive?</FONT> </A> </B> </FONT><BR>
													<FONT face="Arial" size="2">If your newspaper does
														not arrive or arrives in poor condition, you can do one of
														the following:<BR> 1. Visit <A
														href="http://usatodayservice.com">www.usatodayservice.com</A>,
														click on the <A href="/account/cp/delissue.faces">Report
															A Delivery Problem</A> link and select the appropriate issue.<BR>
														2. Call 1-800-USA-0001 (M-F 8:00AM - 7:00PM ET) and talk
														to a Customer Service Professional.&nbsp; If the call is
														received early enough, same-day redelivery may be a
														possibility for our carrier delivered subscriptions.</FONT> <BR>
													<FONT size="2" face="Arial"><FONT size="2"
														face="Arial" color="green"></FONT><FONT size="2"
														face="Arial" color="blue"></FONT><FONT size="2"
														face="Arial" color="green"><FONT color="blue"
															face="Arial"><SPAN
																style="font-size: 10.0pt; font-family: Arial"><SPAN
																	style="font-size: 10.0pt; font-family: Arial"><FONT
																		face="Arial" color="blue"><FONT face="Arial"><A
																				href="#section99"><FONT size="2" face="Arial"
																					color="009BBF">Back to top</FONT> </A> </FONT> </FONT> </SPAN> </SPAN> </FONT> </FONT> </FONT> <BR>
													<br> <FONT color="black" size="2"><B>The
															e-Newspaper of USA TODAY</B> </FONT> <BR> <B><A
														name="whatEEdition"><FONT color="#5b748a">What
																is the e-Newspaper of USA TODAY?</FONT> </A> </B> </FONT><BR> <FONT
												face="Arial" size="2">The e-Newspaper of USA TODAY is
													a page-by-page exact digital replica of the printed version
													of USA TODAY. The e-Newspaper allows for on-the-go
													accessibility via your computer and combines the speed of
													the Internet with the organization of a newspaper. The
													familiar look of USA TODAY is presented in a flexible and
													dynamic digital portal powered by Olive Software.</FONT><BR>
												<FONT size="2" face="Arial"><FONT size="2"
													face="Arial" color="green"></FONT><FONT size="2"
													face="Arial" color="blue"></FONT><FONT size="2"
													face="Arial" color="green"><FONT color="blue"
														face="Arial"><SPAN
															style="font-size: 10.0pt; font-family: Arial"><SPAN
																style="font-size: 10.0pt; font-family: Arial"><FONT
																	face="Arial" color="blue"><FONT face="Arial"><A
																			href="#section99"><FONT size="2" face="Arial"
																				color="009BBF"> Back to top</FONT> </A> </FONT> </FONT> </SPAN> </SPAN> </FONT> </FONT> </FONT> <BR>
												<br> <font size="2" face="Arial"> <b><a
														name="whatTimeEE"><font color="#5b748a">What
																time of day is the e-Newspaper available?</font> </a> </b> </font><br> <font
												face="Arial" size="2">The e-Newspaper is available by
													5:30 a.m. ET, Monday thru Saturday.</font><br> <font size="2"
												face="Arial"><font size="2" face="Arial"
													color="green"></font><font size="2" face="Arial"
													color="blue"></font><font size="2" face="Arial"
													color="green"><font color="blue" face="Arial"><span
															style="font-size: 10.0pt; font-family: Arial"><span
																style="font-size: 10.0pt; font-family: Arial"><font
																	face="Arial" color="blue"><a href="#section99"><font
																			size="2" face="Arial" color="009BBF"> Back to
																				top</font> </a> </font>
													</font> </font> </font> <br> <br> <b><a name="addlSubEE"><font
														color="#5b748a">Does the e-Newspaper require an
															additional subscription?</font> </a> </b><br> <font face="Arial"
												size="2">The e-Newspaper of USA TODAY is provided as
													a companion to all subscribers of the printed version of
													our newspaper - at no additional charge. A subscription to
													just the e-Newspaper can also be purchased.</font><br> <font
												size="2" face="Arial"><font size="2" face="Arial"
													color="green"></font><font size="2" face="Arial"
													color="blue"></font><font size="2" face="Arial"
													color="green"><font color="blue" face="Arial"><span
															style="font-size: 10.0pt; font-family: Arial"><span
																style="font-size: 10.0pt; font-family: Arial"><font
																	face="Arial" color="blue"><font face="Arial"><a
																			href="#section99"><font size="2" face="Arial"
																				color="009BBF"> Back to top</font> </a> </font> </font> </span> </span> </font> </font> </font> <br>
												<br> <FONT color="black" size="2"><B></B> </FONT><b><a
													name="whyEE"><font color="#5b748a">Why should I
															subscribe to the e-Newspaper of USA TODAY?</font> </a> </b><br> <b>Features
													of the e-Newspaper include:</b>

												<DIV
													style="margin-top: 5px; margin-bottom: 5px; margin-left: .5in;">
													<FONT size="2" face="Arial"><b>Daily
															&quot;Editor's Picks&quot;</b> - Highlighting the best of the
														day's content across USA TODAY'S sections.</FONT><br> <FONT
														size="2" face="Arial"><b>Table of Contents</b> - A
														section by section table of contents, providing direct
														access to articles inside the newspaper.</FONT><br> <FONT
														size="2" face="Arial"><b>Videos</b> - Online videos
														embedded directly into the accompanying newspaper content.</FONT><br>
													<FONT size="2" face="Arial"><b>Puzzles and Games</b>
														- USA TODAY's interactive puzzles and games including
														Crossword, Don't Quote Me, Sudoku, Mini Sudoku, Word Round
														Up, and Up &amp; Down Words.</FONT><br> <FONT size="2"
														face="Arial"><b>Snapshots&reg;</b> - USA TODAY's
														popular infographic, with an interactive bonus allowing
														readers to actively participate in quick polling results.</FONT><br>
													<font size="2" face="Arial"><b>Featured Pages
															and Thumbnails </b> - Quick scan option to review USA TODAY
														featured pages like weather, markets, and TV listings. A
														thumbnail section for each page in the newspaper.</font><br>
													<font size="2" face="Arial"><b>Search</b> - The
														ability to text search content and advertisement in the
														day's issue and through back issues.</font><br> <font
														size="2" face="Arial"><b>Back Issues</b> - Easy
														access to all digital back issues available from product
														launch.</font><br> <font size="2" face="Arial"><b>Text
															to Speech</b> - One-button text to speech capability,
														allowing subscribers to have the news provided in an audio
														format.</font><br> <FONT size="2" face="Arial"><b>Multifunction
															display - </b> View content in the newspaper format, or
														simply as text. Other options allow subscribers to zoom,
														fit-to-page, print and email.</FONT><br> <font size="2"
														face="Arial"><b>Connect to Internet </b> - All
														links throughout content and advertisements will be
														clickable, allowing readers to access Web sites outside
														the digital reader environment.</font><br>
												</DIV> <br> <b><a name="internetEE"><font
														color="#5b748a">Do I need to have a connection to
															the internet when reading the e-Newspaper?</font> </a> </b><br> <font
												face="Arial" size="2">The e-Newspaper can be read
													online or downloaded for later use - even when a connection
													to the internet is not available.</font><br> <br> <font
												face="Arial" size="2"><b><a name="moreEE"><font
															color="#5b748a">Where can I find more information
																about the e-Newspaper of USA TODAY?</font> </a> </b><br> <font
													face="Arial" size="2"><b>For additional
															information about USA TODAY e-Newspaper <a
															href="/eeFAQ/index.html"><u>click here</u><br> </a>
													</b> </font> <BR> <FONT color="black" size="2"><B>Billing
															and Payment</B> </FONT> <BR> <BR> <FONT face="Arial"
													size="2"><B><A name="section8"><FONT
																color="#5b748a">How can I pay for my
																	subscription?&nbsp;</FONT> </A> </B><BR> Online subscription
														orders must be paid for with a credit card.&nbsp; </FONT><FONT
													face="Arial" size="2" color="black"><B>USA TODAY</B>
												</FONT><FONT face="Arial" size="2"> accepts American
														Express, VISA, MasterCard, and Discover.&nbsp; For other
														payment methods, please call us at 1-800-872-0001 (M-F
														8:00AM - 7:00PM ET). </FONT><FONT face="Arial" size="2"
													color="black">For your convenience, we also offer an
														EZ Pay option!</FONT> <BR> <FONT size="2" face="Arial"><FONT
														size="2" face="Arial" color="green"><FONT
															color="blue" face="Arial"><FONT color="blue"
																face="Arial"><SPAN
																	style="font-size: 10.0pt; font-family: Arial"><SPAN
																		style="font-size: 10.0pt; font-family: Arial"><FONT
																			face="Arial" color="blue"><FONT face="Arial"><A
																					href="#section99"><FONT size="2" face="Arial"
																						color="009BBF">Back to top</FONT> </A> </FONT> </FONT> </SPAN> </SPAN> </FONT> </FONT> </FONT> </FONT><BR>
													<br>
													<P>
														<FONT size="2" face="Arial"><A name="section6"></A><A
															name="EZPAY"></A> </FONT><FONT size="2" face="Arial"
															color="#5b748a"><B><FONT color="#5b748a">What
																	is EZ-PAY</FONT> </B> </FONT><B><FONT color="#5b748a" size="2"
															face="Arial">?</FONT><FONT color="blue"> </FONT> </B><FONT
															color="#5b748a"><BR> </FONT><FONT size="2"
															face="Arial" color="black"> EZ-PAY, as the name
															implies, is an easy way to automatically pay your bill.
															You can put the charge for your <B>USA TODAY</B> on a
															credit card and never have to be troubled with receiving
															or mailing a bill again! At the end of your term, your
															card will be charged automatically to continue the
															subscription at the published rate unless you notify us
															otherwise. It's easy and convenient. </FONT><BR> <FONT
															size="2" face="Arial"><FONT size="2" face="Arial"
															color="green"><FONT color="blue" face="Arial"><FONT
																	color="blue" face="Arial"><SPAN
																		style="font-size: 10.0pt; font-family: Arial"><SPAN
																			style="font-size: 10.0pt; font-family: Arial"><FONT
																				face="Arial" color="blue"><FONT face="Arial"><A
																						href="#section99"><FONT size="2" face="Arial"
																							color="009BBF">Back to top</FONT> </A> </FONT> </FONT> </SPAN> </SPAN> </FONT> </FONT> </FONT> </FONT> <BR>
														<BR> <FONT color="#5b748a"><b></b> </font><A
															name="section7"><FONT color="#5b748a"><B>How
																	do I make a change to EZ-PAY? </B> </FONT> </A><BR> <FONT
															face="Arial" size="2">If you need to change your
															credit card number or wish to be removed from the EZ-PAY
															program, just let us know. Either send us an e-mail
															through <SPAN style="color: blue"><A
																href="/feedback/feedback.jsp">Feedback</A> </SPAN>, call us at
															1-800-872-0001&nbsp; (M-F 8:00AM - 7:00PM ET) or send us
															your cancellation notice in writing to:<SPAN
															style="mso-spacerun: yes">&nbsp;</SPAN> </FONT>
													</P>
													<P class="MsoNormal"
														style="text-indent: .5in; line-height: 100%; margin-left: .5in; margin-top: 3; margin-bottom: 3">
														<FONT size="2" face="Arial">USA TODAY</FONT>
													</P>
													<P class="MsoNormal"
														style="text-indent: .5in; line-height: 100%; margin-left: .5in; margin-top: 3; margin-bottom: 3">
														<FONT size="2" face="Arial">Attn:EZ-PAY Coordinator</FONT>
													</P>
													<P class="MsoNormal"
														style="text-indent: .5in; line-height: 100%; margin-left: .5in; margin-top: 3; margin-bottom: 3">
														<FONT size="2" face="Arial">National Customer
															Service</FONT>
													</P>
													<P class="MsoNormal"
														style="text-indent: .5in; line-height: 100%; margin-left: .5in; margin-top: 3; margin-bottom: 3">
														<FONT size="2" face="Arial">7950 Jones Branch Drive</FONT>
													</P>
													<P class="MsoNormal"
														style="text-indent: .5in; line-height: 100%; margin-left: .5in; margin-top: 3; margin-bottom: 3">
														<FONT size="2" face="Arial">McLean, VA<SPAN
															style="mso-spacerun: yes">&nbsp; </SPAN>22108</FONT>
													</P> <FONT face="Arial" size="2">Please include your
														account number, name and address to ensure we accurately
														process your request.<SPAN style="mso-spacerun: yes"></SPAN>
												</FONT><BR> <FONT face="Arial" color="blue"><FONT
														face="Arial"><A href="#section99"><FONT
																size="2" face="Arial" color="009BBF">Back to top</FONT>
														</A> </FONT> </FONT> <BR> <br> <A name="sectionEE1"><FONT
														color="#5b748a"><B>How do I give an e-Newspaper
																gift subscription? </B> </FONT> </A><BR> <FONT face="Arial"
													size="2">Your gift subscription will start on the
														date you select. On that day, your gift recipient will
														receive a Welcome email to their email address you
														provided. This Welcome email will let your gift recipient
														know who gave them the gift and it will provide them with
														their log in information. Gift cancellations must be
														received seven days prior to the start date, in order to
														stop this Welcome email from being sent to the gift
														recipient.</FONT> <BR> <FONT face="Arial" color="blue"><FONT
														face="Arial"><A href="#section99"><FONT
																size="2" face="Arial" color="009BBF">Back to top</FONT>
														</A> </FONT> </FONT> <BR> <B> <FONT size="2" face="Arial"
														color="black"> <BR> Managing your Subscription
															Online <BR> <BR> </FONT><FONT size="2"></FONT><FONT
														face="Arial" size="2"><A name="section82"><FONT
																color="#5b748a">How do I access my account
																	online?</FONT> </A> </FONT> </B> <BR> <SPAN
													style="font-size: 10.0pt; font-family: Arial; color: black">Visit
														<B><A href="http://usatodayservice.com">www.usatodayservice.com</A>
													</B>, click on the </SPAN><SPAN
													style="font-size: 10.0pt; font-family: Arial; color: #5B748A"><B
														style="mso-bidi-font-weight: normal">Online Account
															Setup</B> </SPAN><SPAN
													style="font-size: 10.0pt; font-family: Arial; color: black">
														link and complete the form.<BR> </SPAN> <A href="#section99"><FONT
														size="2" face="Arial" color="009BBF">Back to top</FONT> </A> <B><FONT
														size="2" face="Arial"><BR> <BR> </FONT><FONT
														size="2" face="Arial" color="#5b748a"><A
															name="section28"></A><A name="section9">Where can I
																find my account number?</A> </FONT> </B><BR> <SPAN
													style="font-size: 10.0pt; font-family: Arial; color: black">This
														can be found on the label on your newspaper or on your
														bill. <SPAN style="mso-spacerun: yes">&nbsp;</SPAN>If your
														newspaper does not have a label, you will need to obtain
														your account number by calling our National Customer
														Service center at 1-800-</SPAN><SPAN
													style="font-size: 10.0pt; font-family: Arial">872<SPAN
														style="color: black">-0001 (M-F 8:00AM - 7:00PM
															ET). If you are not available to call us during those
															times, please send us your request through <A
															href="https://service.usatoday.com/feedback/feedback.jsp"><B>Feedback</B>
														</A> and we will e-mail your account number to you.</SPAN> </SPAN><BR>
													<A href="#section99"><FONT size="2" face="Arial"
														color="009BBF">Back to top</FONT> </A><BR> <BR> <FONT
													color="#5b748a"><B> <FONT size="2" face="Arial">
																<A name="section10">How do I make a change to my
																	account information?</A> </FONT> </B> </FONT> <FONT face="Arial" size="2"><BR>
														You can make changes to your account, address, or phone
														number by clicking on <SPAN
														style='font-size: 10.0pt; font-family: Arial; mso-fareast-font-family: "Times New Roman"; color: black; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA'><A
															href="https://service.usatoday.com/account/accounthistory/accountSummary.faces"><B>Customer
																	Service</B> </A> </SPAN>. &nbsp; Certain </FONT><FONT face="Arial" size="2"
													color="black"><B>USA TODAY </B> </FONT><FONT face="Arial"
													size="2">subscriptions are delivered by other
														newspapers who maintain all the billing and account
														information on their systems.&nbsp; If this is the case,
														you will not be able to interact with your account on this
														site.&nbsp; You will be able to obtain more information
														about your account by calling <B>USA TODAY</B> National
														Customer Service at 1-800-872-0001 (M-F 8:00AM - 7:00PM
														ET).</FONT><BR> <SPAN
													style="font-size: 10.0pt; font-family: Arial; color: blue; mso-bidi-font-weight: bold; mso-bidi-font-style: italic"><FONT
														size="2" face="Arial"><FONT size="2" face="Arial"
															color="green"></FONT><FONT size="2" face="Arial"
															color="blue"></FONT><FONT size="2" face="Arial"
															color="green"><FONT color="blue" face="Arial"><SPAN
																	style="font-size: 10.0pt; font-family: Arial"><SPAN
																		style="font-size: 10.0pt; font-family: Arial"><FONT
																			face="Arial" color="blue"><FONT face="Arial"><A
																					href="#section99"><FONT size="2" face="Arial"
																						color="009BBF">Back to top</FONT> </A> </FONT> </FONT> </SPAN> </SPAN> </FONT> </FONT> </FONT> </SPAN>
													<P>
														<FONT face="Arial" size="2"><B><br> <A
																name="section11"><FONT color="#5b748a">Why
																		can't I see the changes I made to my account
																		immediately?</FONT><FONT color="blue"><BR> </FONT> </A> </B>When
															you make a change to your account information online, it
															may take up to 48 hours for your change to take effect.</FONT><BR>
														<SPAN
															style="font-size: 10.0pt; font-family: Arial; color: blue; mso-bidi-font-weight: bold; mso-bidi-font-style: italic"><FONT
															size="2" face="Arial"><FONT size="2" face="Arial"
																color="green"></FONT><FONT size="2" face="Arial"
																color="blue"></FONT><FONT size="2" face="Arial"
																color="green"><FONT color="blue" face="Arial"><SPAN
																		style="font-size: 10.0pt; font-family: Arial"><SPAN
																			style="font-size: 10.0pt; font-family: Arial"><FONT
																				face="Arial" color="blue"><FONT face="Arial"><A
																						href="#section99"><FONT size="2" face="Arial"
																							color="009BBF">Back to top</FONT> </A> </FONT> </FONT> </SPAN> </SPAN> </FONT> </FONT> </FONT> </SPAN>
													</P>

													<P>
														<FONT face="Arial" size="2"><B><FONT
																color="#5b748a"><A name="section12">How do I
																		temporarily stop my subscription if I go on vacation?<BR>
																</A> </FONT> </B>Just go to the <A
															href="https://service.usatoday.com/account/vacationholds/holds.faces"><B>Suspend/Resume
																	Delivery</B> </A> link to temporarily stop your newspaper.
															&nbsp; You can submit multiple vacation hold requests in
															advance if you are going to be traveling in
															intervals.&nbsp; Please let us know at least three
															business days in advance when placing these requests.</FONT>
													</P>

													<P style="margin: 0in; margin-bottom: .0001pt">
														<SPAN
															style="font-size: 10.0pt; font-family: Arial; color: black">With
															temporary stops, we typically donate your newspapers to
															the Newspapers in Education program. Teachers love using
															<b>USA TODAY</b> as a tool to tie daily lessons to real
															life. This is great for students. If you opt not to
															donate your unused newspapers, your expiration date will
															automatically be extended by the number of days your
															newspaper is on hold.</SPAN><SPAN
															style='font-size: 10.0pt; font-family: "Arial \(W1\)"; mso-bidi-font-family: Arial; color: black'><B
															style="mso-bidi-font-weight: normal"> </B>
														</SPAN><SPAN
															style="font-size: 10.0pt; font-family: Arial; color: black">
															&nbsp;</SPAN>
													</P> <FONT color="#5b748a"> <SPAN
														style='font-size: 10.0pt; font-family: Arial; mso-fareast-font-family: "Times New Roman"; color: blue; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA; mso-bidi-font-weight: bold; mso-bidi-font-style: italic'><FONT
															size="2" face="Arial"><FONT size="2" face="Arial"><FONT
																	face="Arial"><SPAN
																		style="font-size: 10.0pt; font-family: Arial"><SPAN
																			style="font-size: 10.0pt; font-family: Arial"><FONT
																				face="Arial"><FONT face="Arial"><A
																						href="#section99"><FONT size="2" face="Arial"
																							color="009BBF">Back to top</FONT> </A> </FONT> </FONT> </SPAN> </SPAN> </FONT> </FONT> </FONT> </SPAN><BR>
														<BR> <SPAN
														style="font-size: 10.0pt; font-family: Arial; color: #5b748a"><B><A
																name="section27"></A><A name="section13">How can I
																	help schools when I temporarily stop my subscription? </A>
														</B> </SPAN><SPAN style="font-family: Arial; color: black"></SPAN> </FONT>
													<P style="margin: 0in; margin-bottom: .0001pt">
														<SPAN
															style="font-size: 10.0pt; font-family: Arial; color: black">You
															can donate your subscription copies to help classrooms
															through The <b>USA TODAY</b> Charitable Foundation. The
															Foundation provides newspapers and online educational
															resources (available at usatodayeducation.com) to
															teachers nationwide every day. Having students read <b>USA
																TODAY</b> creates a forum of lasting learning by linking
															classroom studies to real-world, real-life events.</SPAN>
													</P> <A href="#section99"> <FONT size="2" face="Arial"
														color="009BBF">Back to top</FONT> </A><FONT color="black"><B>
															<FONT size="2" face="Arial"> <br> <BR>
																Other Services and Questions<BR> <BR> <A
																name="section29"><FONT size="2" face="Arial"
																	color="#5b748a">How do I order multiple copies
																		of the newspaper?</FONT> </A><BR> <FONT size="2"
																face="Arial"></FONT> </FONT> </B> </FONT><FONT size="2" face="Arial"
													color="#5b748a"><FONT color="black" size="2"
														face="Arial"><SPAN
															style="FONT-SIZE: 10pt; FONT-FAMILY: Arial">For </SPAN> </FONT><FONT
														color="black" size="2" face="Arial"><SPAN
															style="FONT-SIZE: 10pt; FONT-FAMILY: Arial"><B>Hotels</B>
														</SPAN> </FONT><FONT color="black" size="2" face="Arial"><SPAN
															style="FONT-SIZE: 10pt; FONT-FAMILY: Arial">,
																please visit us online at <a
																href="http:www.amenity.usatoday.com">www.amenity.usatoday.com</a>
																or call our National Customer Service center at
																1-800-872-0001</SPAN> </FONT>. <BR> <BR> <FONT size="2"
														face="Arial"></FONT><FONT color="black" size="2"
														face="Arial"><SPAN
															style="FONT-SIZE: 10pt; FONT-FAMILY: Arial">For</SPAN> </FONT><FONT
														color="black" size="2" face="Arial"><SPAN
															style="FONT-SIZE: 10pt; FONT-FAMILY: Arial"><B>
																	Hospitals/Medical Centers</B>,</SPAN> </FONT> <FONT color="black"
														size="2" face="Arial"><SPAN
															style="FONT-SIZE: 10pt; FONT-FAMILY: Arial">please
																call our National Customer Service center at
																1-800-872-0001</SPAN> </FONT>.<BR> <BR> <FONT color="black"
														size="2" face="Arial"><SPAN
															style="FONT-SIZE: 10pt; FONT-FAMILY: Arial">For</SPAN> </FONT><FONT
														color="black" size="2" face="Arial"><SPAN
															style="FONT-SIZE: 10pt; FONT-FAMILY: Arial"> <B>Retailers,
																	Restaurants, and other businesses</B> </SPAN> </FONT><FONT color="black"
														size="2" face="Arial"><SPAN
															style="FONT-SIZE: 10pt; FONT-FAMILY: Arial">,
																please send an e-mail to retail@usatoday.com expressing
																your interest in selling <B>USA TODAY</B> or providing
																it to your customers or call our National Customer
																Service center at 1-800-872-0001</SPAN> </FONT>.<BR> <BR> <FONT
														size="2" face="Arial"></FONT><FONT color="black" size="2"
														face="Arial"><SPAN
															style="FONT-SIZE: 10pt; FONT-FAMILY: Arial">For</SPAN> </FONT><FONT
														color="black" size="2" face="Arial"><SPAN
															style="FONT-SIZE: 10pt; FONT-FAMILY: Arial"> <B>Schools,
																	Colleges and Universities</B>,</SPAN> </FONT> <FONT color="black"
														size="2" face="Arial"><SPAN
															style="FONT-SIZE: 10pt; FONT-FAMILY: Arial">please
																visit <a href="http://www.education.usatoday.com">www.education.usatoday.com</a>,
																e-mail education@usatoday.com, or call 1-800-757-TEACH
																for more information</SPAN> </FONT>.<BR> <BR> <FONT size="2"
														face="Arial"></FONT><FONT color="black" size="2"
														face="Arial"><SPAN
															style="FONT-SIZE: 10pt; FONT-FAMILY: Arial">For</SPAN> </FONT><FONT
														color="black" size="2" face="Arial"><SPAN
															style="FONT-SIZE: 10pt; FONT-FAMILY: Arial"> <B>corporate/
																	organization employee subscriptions</B> </SPAN> </FONT><FONT
														color="black" size="2" face="Arial"><SPAN
															style="FONT-SIZE: 10pt; FONT-FAMILY: Arial"><B>
															</B>- If you are ordering 10 or more copies for your
																employees, you may be eligible for a group discount. To
																learn more, please contact Wayna Zanders at 703-854-6359
																or send an email to wzanders@usatoday.com.</SPAN> </FONT><BR> <BR>
														<FONT color="black" size="2" face="Arial"><SPAN
															style="FONT-SIZE: 10pt; FONT-FAMILY: Arial">Callers
																to our National Customer Service center at
																1-800-872-0001 should press 0 to speak with one of our
																customer service representatives who are available to
																assist you Monday thru Friday from 8:00 a.m. to 7:00
																p.m. Eastern Time</SPAN> </FONT> <br> <br> <A
														name="section14">If I am purchasing a subscription as
															a gift, how do I request a gift announcement card?</A><SPAN></SPAN>
												</FONT><FONT color="#5b748a"><SPAN
														style='font-size: 12.0pt; font-family: Arial; mso-fareast-font-family: "Times New Roman"; color: black; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA'><BR>
													</SPAN><SPAN
														style='font-size: 10.0pt; font-family: "Arial \(W1\)"; mso-fareast-font-family: "Times New Roman"; mso-bidi-font-family: Arial; color: blue; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA'><B
															style="mso-bidi-font-weight: normal; color: black">USA
																TODAY</B> </SPAN><SPAN
														style='font-size: 10.0pt; font-family: Arial; mso-fareast-font-family: "Times New Roman"; color: black; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA'>
															provides FREE gift announcement cards. <SPAN
															style="mso-spacerun: yes">&nbsp;</SPAN>To send a gift
															card via email, visit <a href="/giftcard/sendcard.faces"><u>GIFT
																	CARD</u><br> </a> </SPAN><SPAN
														style='font-size: 10.0pt; font-family: Arial; mso-fareast-font-family: "Times New Roman"; color: blue; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA'><B></B>
													</SPAN><SPAN
														style='font-size: 10.0pt; font-family: Arial; mso-fareast-font-family: "Times New Roman"; color: blue; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA; mso-bidi-font-weight: bold; mso-bidi-font-style: italic'><FONT
															size="2" face="Arial"><FONT size="2" face="Arial"><FONT
																	face="Arial"><SPAN
																		style="font-size: 10.0pt; font-family: Arial"><SPAN
																			style="font-size: 10.0pt; font-family: Arial"><FONT
																				face="Arial"><FONT face="Arial"><A
																						href="#section99"><FONT size="2" face="Arial"
																							color="009BBF">Back to top</FONT> </A> </FONT> </FONT> </SPAN> </SPAN> </FONT> </FONT> </FONT> </SPAN><B>
															<FONT size="2" face="Arial"> <BR> </FONT> </B><BR>
												</FONT> <FONT face="Arial" size="2" color="black"><B><A
															name="section16"><FONT color="#5b748a" size="2"
																face="Arial">How do I subscribe to an
																	International Edition of USA TODAY?</FONT> </A> </B> </font> <BR> <FONT
													face="Arial" color="black" size="2">While
														subscriptions to USA TODAY are not available outside of
														North America, international readers who are interested in
														subscribing can do so with the electronic edition of USA
														TODAY. The electronic edition of USA TODAY is an exact
														replica of the edition available in the USA with all the
														benefits of interactivity and electronic navigation.
														Please visit <A href="http://usatoday.newspaperdirect.com"
														target="_blank">http://usatoday.newspaperdirect.com</A> to
														subscribe.</FONT> <BR> <FONT size="2" face="Arial"
													color="black"> <A href="#section99"><FONT
															size="2" face="Arial" color="009BBF">Back to top</FONT> </A>
														<BR> <BR> <FONT face="Arial" size="2"
														color="#5b748a"><B><A name="section17"><FONT
																	color="#5b748a" size="2" face="Arial">Why does
																		my address get reformatted when I submit it?</FONT> </A> </B> </FONT><BR>
														<SPAN
														style='font-size: 10.0pt; font-family: "Arial \(W1\)"; mso-bidi-font-family: Arial; color: blue'><B
															style="mso-bidi-font-weight: normal; color: black">USA
																TODAY</B> </SPAN><SPAN
														style="font-size: 10.0pt; font-family: Arial; color: black">
															runs the submitted address information through an address
															hygiene software program.&nbsp; It is recommended that
															you choose the returned address to ensure the best
															delivery of your newspaper either by mail or carrier
															delivery.&nbsp;</SPAN><BR> <A href="#section99"><FONT
															size="2" face="Arial" color="009BBF">Back to top</FONT> </A><BR>
														<BR> <FONT size="2" face="Arial" color="black"><B><SPAN
																style="font-size: 10.0pt; font-family: Arial"><FONT
																	color="black">Content Licensing, Reprints,
																		Permissions and Back Issues</FONT> </SPAN> </B><BR> <SPAN
															style="font-size: 10.0pt; font-family: Arial; color: #5b748a"><B><A
																	name="section21"><BR> How do I order hard copy
																		and digital reprints or license permission to
																		republish USA TODAY content? </A> </B> </SPAN> <SPAN
															style="font-size: 10.0pt; font-family: Arial; color: black"><br>With
																PARS International Corp., it's faster and easier than
																ever before to obtain USA TODAY hard copy and digital
																reprints or license permission to republish USA TODAY
																content. <br> <br>To request a reprint,
																e-print, poster or plaque quote, click <a
																href="http://www.gannettreprints.com/reprint-products-quote-request/">here</a>.
																(http://www.gannettreprints.com/reprint-products-quote-request/)<br>
																<br>To request permissions or licensing quote,
																click <a
																href="http://www.gannettreprints.com/permissions-licensing-quote-request/">here</a>.
																(http://www.gannettreprints.com/permissions-licensing-quote-request/)<br>
																<span
																style="font-size: 10.0pt; font-family: Arial; color: #009bbf">Back
																	to top</span> </SPAN> <BR> <B
															style="mso-bidi-font-weight: normal"><SPAN
																style="font-size: 10.0pt; font-family: Arial; color: #5b748a"><A
																	name="section22"><BR> What is a back issue?</A> </SPAN> </B><BR>
													</FONT><FONT face="Arial" size="2" color="black"><B
															style="mso-bidi-font-weight: normal"><SPAN
																style="font-size: 10.0pt; font-family: Arial; color: black">USA</SPAN>
														</B> <SPAN
															style="font-size: 10.0pt; font-family: Arial; color: black"><B
																style="mso-bidi-font-weight: normal"> TODAY</B> </SPAN><SPAN
															style="font-size: 10.0pt; font-family: Arial">
																keeps an inventory of previously published print
																editions of the newspaper.<SPAN
																style="mso-spacerun: yes">&nbsp; </SPAN>You may purchase
																one of these editions for an additional cost, plus
																shipping &amp; handling.<SPAN style="mso-spacerun: yes">&nbsp;
															</SPAN>To request a back issue, call our National Customer
																Service Center at 1-800-872-5149 (M-F 8:00AM - 7:00PM
																ET).<BR> </SPAN> </FONT> <A href="#section99"><FONT size="2"
															face="Arial" color="009BBF">Back to top 
													</A> </FONT> <BR> <SPAN
													style="font-size: 10.0pt; font-family: Arial; color: #5b748a"><B><A
															name="section23"><BR> What are hard copy
																reprints?</A> </B> </SPAN> <BR> <SPAN
													style="font-size: 10.0pt; font-family: Arial; color: black">Hard
														copy reprints are professionally printed copies of </SPAN><SPAN
													style='font-size: 10.0pt; font-family: "Arial \(W1\)"; mso-bidi-font-family: Arial; color: blue'><B
														style="mso-bidi-font-weight: normal; color: black">USA
															TODAY</B> </SPAN><SPAN
													style="font-size: 10.0pt; font-family: Arial; color: black">
														content printed on quality newspaper with the </SPAN><SPAN
													style='font-size: 10.0pt; font-family: "Arial \(W1\)"; mso-bidi-font-family: Arial; color: black'><B
														style="mso-bidi-font-weight: normal; color: black">USA
															TODAY</B> </SPAN><SPAN
													style="font-size: 10.0pt; font-family: Arial; color: black">
														header. <SPAN style="mso-spacerun: yes">&nbsp;</SPAN> </SPAN><SPAN
													style='font-size: 10.0pt; font-family: "Arial \(W1\)"; mso-bidi-font-family: Arial; color: black'><B
														style="mso-bidi-font-weight: normal; color: black">USA
															TODAY's</B> </SPAN><SPAN
													style="font-size: 10.0pt; font-family: Arial; color: black">
														hard copy reprints are printed by Scoop Reprint Resource
														and can be ordered in volumes of 100 or more. <SPAN
														style="mso-spacerun: yes"></SPAN>Your order must be
														approved by </SPAN><SPAN
													style='font-size: 10.0pt; font-family: "Arial \(W1\)"; mso-bidi-font-family: Arial; color: black'><B
														style="mso-bidi-font-weight: normal">USA TODAY</B> </SPAN><SPAN
													style="font-size: 10.0pt; font-family: Arial; color: black">
														before printing may begin and </SPAN><SPAN
													style='font-size: 10.0pt; font-family: "Arial \(W1\)"; mso-bidi-font-family: Arial; color: black'><B
														style="mso-bidi-font-weight: normal">USA TODAY </B> </SPAN><SPAN
													style="font-size: 10.0pt; font-family: Arial; color: black">reserves
														the right to refuse any request for reprint authorization
														for any reason.<BR> </SPAN> <A href="#section99"><FONT
														size="2" face="Arial" color="009BBF">Back to top</FONT> </A> <BR>
													<SPAN
													style='font-size: 10.0pt; font-family: Arial; mso-fareast-font-family: "Times New Roman"; color: #5b748a; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA'><B><A
															name="section24"><BR> What are digital reprints?</A>
													</B> </SPAN> <SPAN
													style='font-size: 10.0pt; font-family: Arial; mso-fareast-font-family: "Times New Roman"; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA'><BR>
														Digital reprints are the digital versions of hard copy
														reprints. <SPAN style="mso-spacerun: yes">&nbsp;</SPAN>Digital
														Reprints are hosted by </SPAN><SPAN
													style='font-size: 10.0pt; font-family: "Arial \(W1\)"; mso-fareast-font-family: "Times New Roman"; mso-bidi-font-family: Arial; color: blue; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA'><B
														style="mso-bidi-font-weight: normal; color: black">USA
															TODAY's</B> </SPAN><SPAN
													style='font-size: 10.0pt; font-family: Arial; mso-fareast-font-family: "Times New Roman"; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA'>
														reprint partner and can be purchased for 3, 6, 9 or 12
														month periods.<BR> </SPAN> </FONT><FONT face="Arial" size="2"
												color="#5b748a"> <A href="#section99"><FONT
														size="2" face="Arial" color="009BBF">Back to top</FONT> </A><BR>
													<BR> <B><SPAN
														style='font-size: 10.0pt; font-family: Arial; mso-fareast-font-family: "Times New Roman"; color: #5b748a; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA'><A
															name="section25">What are permissions?</A> </SPAN> </B><BR> </FONT><FONT
												face="Arial" size="2" color="black"> <SPAN
													style='font-size: 10.0pt; font-family: Arial; mso-fareast-font-family: "Times New Roman"; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA'>A
														permission is a request to reuse or republish </SPAN><SPAN
													style='font-size: 10.0pt; font-family: "Arial \(W1\)"; mso-fareast-font-family: "Times New Roman"; mso-bidi-font-family: Arial; color: blue; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA'><B
														style="mso-bidi-font-weight: normal; color: black">USA
															TODAY</B> </SPAN><SPAN
													style='font-size: 10.0pt; font-family: Arial; mso-fareast-font-family: "Times New Roman"; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA'>
														content in a newspaper, book, textbook, coursepack, eBook,
														newsletter, annual report, brochure, press kit, magazine,
														Website, e-Newspaper, CD-ROM/DVD, e-mail, or other type of
														re-use.<BR> </SPAN> </FONT><A href="#section99"><FONT size="2"
													face="Arial" color="009BBF">Back to top 
											</A> </FONT><BR> <BR> <SPAN
											style='font-size: 10.0pt; font-family: Arial; mso-fareast-font-family: "Times New Roman"; color: #5b748a; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA'><B><A
													name="section26">How do I obtain copies of the USA
														TODAY articles I have received permission to re-use?</A> </B> </SPAN> <SPAN
											style='font-size: 10.0pt; font-family: Arial; mso-fareast-font-family: "Times New Roman"; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA'><BR>
												Once licensed, you may obtain </SPAN><SPAN
											style='font-size: 10.0pt; font-family: "Arial \(W1\)"; mso-fareast-font-family: "Times New Roman"; mso-bidi-font-family: Arial; color: blue; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA'><B
												style="mso-bidi-font-weight: normal; color: black">USA
													TODAY</B> </SPAN><SPAN
											style='font-size: 10.0pt; font-family: Arial; mso-fareast-font-family: "Times New Roman"; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA'>
												articles:&nbsp;&nbsp;</SPAN>
											<UL style="margin-top: 5px; margin-bottom: 5px">
												<SPAN
													style='font-size: 10.0pt; margin-left: .5in; font-family: Arial; mso-fareast-font-family: "Times New Roman"; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA'>From
													the front page of USATODAY.com for current articles</SPAN>
												<br>
												<SPAN
													style='font-size: 10.0pt; margin-left: .5in; font-family: Arial; mso-fareast-font-family: "Times New Roman"; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA'>At
													http://archives.usatoday.com for archived articles (older
													than two weeks)</SPAN>
												<br>
											</UL> <A href="#section99"><FONT size="2" face="Arial"
												color="009BBF">Back to top</FONT> </A> <BR> <%=com.usatoday.esub.common.UTCommon
					.displayPubGraphics(request)%></TD>
									</TR>
									<TR>
									</TR>
								</TBODY>
							</TABLE>
						<%-- /tpl:put --%>
		</DIV>
	<%-- /tpl:put --%>
				<DIV id="clear"></DIV>
				<DIV id="bluebar">
					<a href="/feedback/feedback.jsp"
						style="color: white; font-weight: bold; text-decoration: none; font-size: 14px"
						title="Contact Us">Contact Us</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;1-800-872-0001&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a
						href="/faq/utfaq.jsp"
						style="color: white; font-weight: bold; text-decoration: none; font-size: 14px"
						title="Frequently Asked Questions">FAQ</a>
				</DIV>
				<%-- tpl:put name="footerMainArea" --%>
	<%-- tpl:put name="footerarea1" --%><%-- /tpl:put --%>
		<%=trackingBug.getOmnitureHTML() %>
	<%-- /tpl:put --%>
				<DIV id=graybar>
					Copyright
					<script>
						document.write(getCurrentYear());
					</script>
					USA TODAY, a division of <a href="http://www.gannett.com">Gannett
						Co. Inc.</a> <a href="/privacy/privacy.htm">Privacy
						Notice/California Privacy Notice</a>. <a
						href="/privacy/privacy.htm#adchoice">Ad Choices</a>.<br> By
					using this service, you accept our <a href="/service/service.jsp">Terms
						of Service</a>.
				</DIV>
				<DIV id=copyright>
					<P>Offers available to new subscribers only. Home delivery not
						available in some areas. Not valid with any other offer.
						Prepayment required. After the promotional period ends, the
						subscription will continue and be billed monthly at the then
						regular rate, less any applicable credits, unless USA TODAY is
						otherwise notified. Applicable taxes may not be included in the
						rate. If at any time you decide to cancel your subscription, you
						may contact customer service at 1-800-872-0001 and the full amount
						of any balance over $2.00 will be returned.</P>
				</DIV>
				<!--END OF COPYWRITE-->
				<DIV id=footer></DIV>
			</DIV>
			<!--END OF WHITEBOX-->
		</DIV>
		<!--END OF CONTENT-->
	</DIV>
</BODY>
</HTML><%-- /tpl:insert --%><%-- /tpl:insert --%>