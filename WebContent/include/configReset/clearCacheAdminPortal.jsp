<%@ page import = "java.io.*,com.usatoday.*" %>
<%
		System.out.println("Begin Clearing Cache...");

		try {
			
			// redesign configs
			com.usatoday.util.constants.UsaTodayConstants.loadProperties();
			com.usatoday.businessObjects.shopping.payment.PTIConfig.resetConfig();
			com.usatoday.businessObjects.util.Code1Config.resetConfig();
//			com.usatoday.businessObjects.products.SubscriptionTermsBO.resetDefaultForceEZPAYTerms();
			
			com.usatoday.esub.common.UTCommon.getInitValues();

		 	com.usatoday.businessObjects.products.SubscriptionProductBO.clearCachedProducts();
			com.usatoday.businessObjects.products.SubscriptionOfferManager.getInstance().clearCache();
//			com.usatoday.businessObjects.products.SubscriptionTermsBO.resetDefaultForceEZPAYTerms();
			com.usatoday.businessObjects.products.promotions.PromotionManager.getInstance().clearCache();
			com.usatoday.businessObjects.util.TaxRateManager.getInstance().clearCache();
			com.usatoday.businessObjects.util.GUIZipCodeManager.getInstance().clearCache();

		}
		catch (Exception e) {
			e.printStackTrace();
		}
	
	
		System.out.println("Cache Cleared...");
	
%>
<html>
<head>
<title>USA Today Online Subscriptions - Portal</title>
</head>
<body>
<p>This page should never be displayed.  Clear cache from Portal Admin site</p>
</body>
</html>