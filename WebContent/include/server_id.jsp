
<%@ page language="java" autoFlush="true" %>
<%
	// THIS PAGE IS DESIGNED FOR INCLUSION IN ANOTHER JSP BETWEEN THE <BODY></BODY> tags
	// IT SIMPLY ADDS A COMMENT TO THE HTML WITH SERVER IDENTITY
	//
	String serverName = "";
	StringBuffer rURL = new StringBuffer();
	String serverIP = "";
	try {
		java.net.InetAddress serverAddress = java.net.InetAddress.getLocalHost();
		serverIP = serverAddress.getHostAddress();
		serverIP = serverIP.substring(serverIP.lastIndexOf("."));
		serverName = serverAddress.getHostName();
		rURL = request.getRequestURL();
	}
	catch (Exception e) {
		
	}
	
	try {
		javax.servlet.http.Cookie[] cookieArray = request.getCookies();
		boolean cookieExists = false;
		
		for (int i = 0; (cookieArray != null) && (i < cookieArray.length); i++ ) {
			javax.servlet.http.Cookie currentCookie = cookieArray[i];
			
			if (currentCookie.getName().equalsIgnoreCase("USAT_XTRNT_SID")) {
				cookieExists = true;
				break;
			}			
		}
		
		if (!cookieExists) {  // set the cookie if not done already
			StringBuffer cookieValue = new StringBuffer();
			int length = serverName.length();
			if (length < 10) {
				cookieValue.append("0");
			}
			cookieValue.append(length);
			
			cookieValue.append(serverName);
			
			cookieValue.append(System.currentTimeMillis());
			
			javax.servlet.http.Cookie cookie = new javax.servlet.http.Cookie("USAT_XTRNT_SID", cookieValue.toString());
			cookie.setMaxAge(1800);
			cookie.setPath("/");
			
			// if in production then set the domain
			if (rURL.toString().indexOf("usatoday.com") > 0) {
				cookie.setDomain("service.usatoday.com");
			}
			
			response.addCookie(cookie);
		}
	}
	catch (Exception e) {
		System.out.println("Got exception while setting cookie: " + e.getMessage());
	}
					
%>

<!-- SID =>																				                                                                  														           SNAME:  <%=serverName%>   SIP: <%=serverIP%>  
-->