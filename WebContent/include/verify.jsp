<!DOCTYPE HTML><%@page language="java"
	contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%
	try {
		String clientHost = request.getParameter("ref");
	
		StringBuilder sb = new StringBuilder();
			
		if (clientHost == null || !clientHost.equalsIgnoreCase("service.usatoday.com")) {
			// not a valid production domain
			
			// make sure not a test domain
			if(clientHost != null && clientHost.startsWith("usat-")) {
				sb.append("<hostvalid>valid test server</hostvalid>");
		    	System.out.println("Test System Host Detected: " + clientHost);
			}
			else {
		    	sb.append("<hostvalid>false</hostvalid>");
		    	System.out.println("Invalid Host Detected: " + clientHost);
		    }
		}	
		else {
		    sb.append("<hostvalid>true</hostvalid>");
			// valid domain do nothing.
		} 
	    response.setContentType("text/xml"); 
	    response.setHeader("Cache-Control", "no-cache"); 
	    response.getWriter().write(sb.toString()); 
	    response.getWriter().close();
	}
	catch (Exception e) {
		e.printStackTrace();
	} 
 %>
<html>
<head>
<title></title>
</head>
<body>
</body>
</html>