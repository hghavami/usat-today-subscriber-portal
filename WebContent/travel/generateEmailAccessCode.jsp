<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<html>
<head>
<title>generateEmailAccessCode</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>
	<%
	try {
		// Generate and email access code to partners
		String partnerId = "";
		if (request.getParameter("pid") != null && !request.getParameter("pid").trim().equals("")) {
			partnerId = request.getParameter("pid");		
		}
		com.usatoday.olive.partners.GenerateOlivePartnersAccess.generateEmailAccessCode(partnerId.trim());
	} catch (Exception e) {
		System.out.print("Olive Partners Access Code generation and email errored, because:" + e);
	}
	%>

</body>
</html>