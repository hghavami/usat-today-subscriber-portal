<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	import="com.usatoday.olive.partners.OlivePartnersProcessor"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<html>
<head>
<title>USA TODAY E-Edition</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>
	<%
	String accessCode = "";
	try {
		// Parse the URL.
		String userEnteredURL = request.getRequestURL().toString();
		// Get the access code from the query string
		if (request.getParameter("ac") != null) {
			accessCode = request.getParameter("ac").trim();
		}
		String clientIP = request.getRemoteAddr().trim();
		System.out.print("Partner is attempting to access ee-newspaper from IP address: " + clientIP);
		com.usatoday.olive.partners.OlivePartnersProcessor.OlivePartners(application, request, response, clientIP, accessCode);
	} catch (Exception e) {
		System.out.print("Olive e-edition Partners landing page error:" + e);
	}
	%>
</body>
</html>