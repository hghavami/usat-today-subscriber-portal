<%-- tpl:insert page="/theme/themeV3/jsfTheme/subscriberPortalNoNav_JSF.jtpl" --%><!DOCTYPE HTML><%@page import="com.usatoday.business.interfaces.products.ProductIntf"%>
<%@page import="com.usatoday.businessObjects.products.ProductBO"%>
<%@page import="com.usatoday.util.MobileDeviceDetectionUtil"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%><%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%><%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%><f:view><f:loadBundle basename="resourceFile" var="labels" />
	<%-- tpl:insert page="/theme/subscriptionPortalRoot.htpl" --%><HTML>
<HEAD>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8">
<%-- tpl:put name="headerareaTop" --%>
<%
	String docProtocol = "https:";
	if (!request.isSecure()) {
		docProtocol = "http:"; 
	}
	
	// change this to any javascript you want called on page load
	String initFunction = "";
	
	com.usatoday.esub.common.WebBug trackingBug = com.usatoday.esub.common.UTCommon.getWebAnalyticsBug();
	try {
		trackingBug.deriveValuesFromRequest(request);
	}
	catch (Exception e) { 
		; // ignore
	}
	
	String pubCode = "";
	String productName = "USA TODAY";
	
    boolean isEEditionProduct = true;

	com.usatoday.businessObjects.products.promotions.PromotionSet defaultPromotionSet = null;
	com.usatoday.businessObjects.products.promotions.PromotionSet currentOfferPromotionSet = null;
	com.usatoday.business.interfaces.customer.CustomerIntf customer = null;
   	com.usatoday.businessObjects.products.promotions.PromotionSet currentAccountPromotionSet = null;
   		
	com.usatoday.business.interfaces.products.OfferIntf offer = null;   		
	try {
		offer = com.usatoday.esub.common.UTCommon.getCurrentOfferVersion2(request);
		pubCode = com.usatoday.util.constants.UsaTodayConstants.UT_PUBCODE;
		if (offer != null) {
			pubCode = offer.getPubCode();
			try {
				String brandingPubCode = offer.getProduct().getBrandingPubCode();
				ProductIntf bp = ProductBO.fetchProduct(brandingPubCode);
				productName = bp.getName();
			}
			catch (Exception teee) {
			}							
		}
		// get the default promotion set for the publication		
		defaultPromotionSet = com.usatoday.esub.common.UTCommon.getDefaultPromotionSetForPub(pubCode);
		
		// get the keycode specific promotion set
		
		if (offer != null && offer instanceof com.usatoday.business.interfaces.products.SubscriptionOfferIntf) {
			currentOfferPromotionSet = ((com.usatoday.business.interfaces.products.SubscriptionOfferIntf)offer).getPromotionSet();
		}
		boolean isSignedIn = false;

		customer = (com.usatoday.business.interfaces.customer.CustomerIntf)session.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_CUSTOMER_INFO);
		
		if (customer != null) {
			isSignedIn = true;
			com.usatoday.business.interfaces.products.SubscriptionProductIntf product = com.usatoday.businessObjects.products.SubscriptionProductBO.getSubscriptionProduct(customer.getCurrentEmailRecord().getPubCode());
			isEEditionProduct = product.isElectronicDelivery(); 
			// This promotion set is based on pub and keycode on the customer account
			currentAccountPromotionSet = com.usatoday.businessObjects.products.promotions.PromotionManager.getInstance().getPromotionsForOffer(customer.getCurrentAccount().getPubCode(), customer.getCurrentAccount().getKeyCode());
			
		}
	} catch (Exception e) {
	
	}

	String mobileCSS = "";
	if (MobileDeviceDetectionUtil.isSmallScreenDevice(request)) {
		mobileCSS = "<LINK href=\"/theme/themeV3/css/smallScreen.css\" rel=\"stylesheet\" type=\"text/css\">";
	}
%>
<script src="/common/prototype/prototype.js"></script>
<script src="/common/prototype/scriptaculous/scriptaculous.js" type="text/javascript"></script>
<link rel="stylesheet"	href="/theme/themeV3/css/stylesheet_jsfcomponents.css"	type="text/css">
<%-- tpl:put name="headerarea_Top" --%>
		<title>InvalidPartnerRequest</title>
	<%-- /tpl:put --%>
<%-- /tpl:put --%>
<LINK REL="SHORTCUT ICON" HREF="/favicon.ico">
<LINK REL="icon" href="/favicon.ico" TYPE="image/ico" />
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META http-equiv="Content-Style-Type" content="text/css">
<meta name="viewport" content="initial-scale=1.0">
<LINK href="/theme/themeV3/css/usatodayBase.css" rel="stylesheet"
	type="text/css">
<LINK href="/theme/themeV3/css/usatodayAddOn.css" rel="stylesheet"
	type="text/css">
<script src="/common/usatCommon.js"></script>
<%-- tpl:put name="headareaBottom" --%>
<%=mobileCSS %>
<%-- tpl:put name="headarea_Bottom" --%>
		<!-- Bottom Header area -->
	<%-- /tpl:put --%>
<script>
// method called on page load
function initPage(thisObj, thisEvent) {
	// insert site-wide code here
	
	// for product messageing in popUpOverlay.js
	usat_productName = "<%=productName %>";
	
	<%=initFunction %>
}
</script>
<%-- /tpl:put --%>
<script type="text/javascript">
	/*   var _gaq = _gaq || [];
	 _gaq.push(['_setAccount', 'UA-23158809-1']);
	 _gaq.push(['_trackPageview']);

	 (function() {
	 var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	 ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	 var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	 })();
	 */
</script>
</HEAD>
<body>
	<DIV id="wrapper">
		<DIV id="header">
			<div id="headerLeft" onclick="templateLogoClick();"></div>
			<div id="headerRight">
				<%-- tpl:put name="headerbodyarea" --%>
					<%-- tpl:insert page="/login/loginform.jspf" --%><div style="float: none">
	<%
		String uri = request.getRequestURI();
		String pageName = uri.substring(uri.lastIndexOf("/") + 1);
		String jScript = "";
		if (uri.indexOf("subscriptions") > 0 && uri.indexOf("thankyou.jsp") > 0) {
			try {
				com.usatoday.esub.handlers.ShoppingCartHandler shoppingCartHandler = (com.usatoday.esub.handlers.ShoppingCartHandler) session
						.getAttribute("shoppingCartHandler");
				if (shoppingCartHandler != null) {
					jScript = shoppingCartHandler.getOnePageThankYouJavaScriptTealiumText();
				}
			} catch (Exception E) {

			}
		} else {
			com.usatoday.businessObjects.products.promotions.PromotionSet currentOfferPromotionSetTealium = null;
			com.usatoday.business.interfaces.products.OfferIntf offerTealium = com.usatoday.esub.common.UTCommon
					.getCurrentOffer(request);
			if (offerTealium != null && offerTealium instanceof com.usatoday.business.interfaces.products.SubscriptionOfferIntf) {
				currentOfferPromotionSetTealium = ((com.usatoday.business.interfaces.products.SubscriptionOfferIntf) offerTealium)
						.getPromotionSet();
				jScript = currentOfferPromotionSetTealium.getPromoOnePageJavaScriptTealiumText();
				jScript = org.apache.commons.lang3.StringUtils.replace(jScript, "page_name:\"\"", "page_name:\"" + uri + "\"");
			}
		}
	%>
	<%=jScript%>
</div>
<%-- 			<f:verbatim>
				<h:outputText styleClass="outputText" id="textThankYouJS13"	escape="false" value="#{shoppingCartHandler.tealiumOnePageThankYouJavaScriptText}"></h:outputText>
			</f:verbatim>
 --%>
<%
	String isLoginDisplay = "true";
	com.usatoday.business.interfaces.customer.CustomerIntf customer2 = null;
	String loggedinDivClass = "hideMe";
	String notLoggedinDivClass = "showMeInline";
	String firstName = "";
	String lastName = "";
	String firmInfo = "";
	String accountNum = "";
	String loginErrorMessage2 = "";
	String logOut = "";
	String publication = "";
	String changeAccountLink = "";
	String earlyAlertDisplay = "display:none";

	String jScriptNoOp = "javascript:;";

	try {
		publication = (String) session.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_PUB_NAME);
		if (publication == null) {
			publication = request.getParameter("pub");
			if (publication == null) {
				publication = com.usatoday.util.constants.UsaTodayConstants.UT_PUBCODE;
			}
		}
		if (publication.equals(com.usatoday.esub.common.UTCommon.PUB_NAMES[0])) { // USATODAY publication
			logOut = com.usatoday.esub.common.UTCommon.BASE_URL + "/index.jsp?pub=UT&logsessobj=rem";
		} else {
			logOut = com.usatoday.esub.common.UTCommon.BASE_URL + "/index.jsp?pub=BW&logsessobj=rem";
		}

		// If customer is logged in remove login page, otherwise display it
		customer2 = (com.usatoday.business.interfaces.customer.CustomerIntf) session
				.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_CUSTOMER_INFO);
		if (customer2 == null) {
			// showMeInling and hideMe are defined in style sheets
			loggedinDivClass = "showMeInline";
			notLoggedinDivClass = "hideMe";
		} else {

			if (customer2.getCurrentAccount() != null) {
				firstName = customer2.getCurrentAccount().getDeliveryContact().getFirstName();
				if (firstName != null && !firstName.trim().equals("")) {
					firstName = com.usatoday.esub.common.UTCommon.formatNameCapitalization(firstName); // Format it
				}
				lastName = customer2.getCurrentAccount().getDeliveryContact().getLastName();
				if (lastName != null && !lastName.trim().equals("")) {
					lastName = com.usatoday.esub.common.UTCommon.formatNameCapitalization(lastName); // Format it
				}
				firmInfo = customer2.getCurrentAccount().getDeliveryContact().getFirmName();
				if (customer2.getNumberOfAccounts() > 1) {
					changeAccountLink = "[&nbsp;<a href=\"/account/select_accnt.jsp\">Change</a>&nbsp;]";
				}
				try {
					accountNum = customer2.getCurrentAccount().getAccountNumber();
					//				if (accountNum == null || accountNum.length() < 4) {
					if (accountNum == null) {
						accountNum = "";
					}
					//				else {
					/*
					if (accountNum.equalsIgnoreCase("340000000")) {
						accountNum = "Pending Setup";
					}
					 */
					//				}
				} catch (Exception eeee) {
					eeee.printStackTrace();
				}

				//EarlyAlert - Check to see if their paper delivery has been affected by a delay
				// HD-CONS109 SEW
				/*
				if (customer2.getCurrentAccount().isShowDeliveryAlert()) {
				earlyAlertDisplay = "display:inline";
				}
				 */

			} else {
				if (customer2.getCurrentEmailRecord() != null
						&& customer2.getCurrentEmailRecord().getEmailAddress() != null) {
					firstName = customer2.getCurrentEmailRecord().getEmailAddress();
					accountNum = "Pending Setup";
				} else {
					firstName = "";
				}
			}
		}
		loginErrorMessage2 = (String) session.getAttribute("LOGIN_FAILED_MESSAGE");
		if (loginErrorMessage2 == null || loginErrorMessage2.trim().equals("")) {
			loginErrorMessage2 = "";
		} else {
			session.removeAttribute("LOGIN_FAILED_MESSAGE");
		}
	} catch (Exception e) {
		e.printStackTrace();
		if (firstName == null) {
			firstName = "";
		}
		if (lastName == null) {
			lastName = "";
		}
	}
	String nameInfo = firstName + " " + lastName;
	String loginInfo = null;
	if (!nameInfo.trim().equals("")) {
		loginInfo = nameInfo;
	} else {
		if (firmInfo != null && !firmInfo.trim().equals("")) {
			loginInfo = firmInfo;
		}
	}
	// if no login info, then hide data
	if (loginInfo == null) {
		loggedinDivClass = "showMeInline";
		notLoggedinDivClass = "hideMe";
		loginInfo = "";
	}
%>

<%@page import="com.usatoday.util.constants.UsaTodayConstants"%><div
	id="loginDisplayDiv" class="<%=loggedinDivClass%>">
	<table border="0" width="100%" height="60px">
		<tr>
			<td>
				<TABLE class="loginFormTable">
					<TBODY>
						<TR>
							<TD class="loginFormColumn"><br /> <a
								href="/login/auth.faces">Login to Manage Account</a></TD>
						</TR>
						<tr>
							<td class="loginFormColumn" align="right"><img
								style="cursor: pointer; margin-top: 10px;"
								onclick="javascript: templateClickToChatClickedGuest(this, event);"
								alt="Click for Assistance" title="Click for Assistance"
								src="${pageContext.request.contextPath}/theme/themeV3/themeImages/chatIcon136.png"
								hspace="0" vspace="5"></td>
						</tr>
					</TBODY>
				</TABLE></td>
		</tr>
	</table>
</div>
<div id="loginHideDiv" class="<%=notLoggedinDivClass%>">
	<table width="100%" border="0" class="loginFormTable">
		<tr>
			<td nowrap align="right">
				<TABLE class="loginFormTable">
					<TBODY>
						<TR>
							<TD class="loginFormColumn" nowrap><span
								class="loginNormalPrint">Hello, <%=loginInfo%>&nbsp;&nbsp;[&nbsp;<a
									href="<%=jScriptNoOp%>"
									onclick="javascript:document.location='<%=logOut%>';_unloadPage(this, event);">Logout</a>&nbsp;]</span>
							</TD>
						</TR>
					</TBODY>
				</TABLE></TD>
		</tr>
		<tr>
			<td class="loginFormColumn" align="right"><span
				class="loginfineprint">Account #:&nbsp;<%=accountNum%>&nbsp;<%=changeAccountLink%></span>
			</td>
		</tr>
		<tr>
			<td class="loginFormColumn" align="right"><div
					id="chatIconRequestDiv">
					<img onclick="javascript: templateClickToChatClicked(this, event);"
						style="cursor: pointer; margin-top: 10px;"
						alt="Click for Assistance" title="Click for Assistance"
						src="${pageContext.request.contextPath}/theme/themeV3/themeImages/chatIcon136.png"
						hspace="0" vspace="5">
				</div>
			</td>
		</tr>
	</table>
</div>
<div id="templateChatWindowDiv" style="display: none;">
	<div id="templateChatTopDiv">
		<span onclick="templateClickToChatHide();" style="cursor: pointer;">No
			Thanks<img src="/theme/themeV3/themeImages/close_14x14.png"
			width="14" height="14" border="0"
			style="margin-left: 5px; margin-right: 5px" /> </span>
	</div>
<!-- 	<div id="tempateChatImageDiv">
		<a
			onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
			href=""><img border="0"
			src="https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY" />
		</a>
	</div>
 -->	<div id="templateChatAccountLinksDiv">
		<ul>
			<li>To report a problem with delivery, click <a
				href="/account/cp/delissue.faces"> here</a>.</li>
			<li>To place your delivery on a temporary hold, click <a
				href="/account/vacationholds/holds.faces"> here</a>.</li>
			<li>Forgot your password or login? Click <a
				href="/idpassword/forgotpassword.jsp"> here</a>.
			<li>To contact us <a href="/feedback/feedback.jsp"> click here</a>.</li>				
<!-- 			<li>To cancel your subscription, please call 1-800-872-0001 and -->
<!-- 				select option 6.</li> -->
		</ul>
	</div>

	<div id="templateChatLinksDiv">
		<ul>

			<li><a
				onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
				href="">Click to Chat</a>
			</li>
		</ul>
	</div>
	<div id="templateChatBottomDiv">
		<span class="outputText">Or, call us at:<br> <b>Call
				1-877-713-6241</b><br>(M-F 8:00 a.m. to 7 p.m. E.T.)</span>
	</div>
</div>
<div id="templateChatWindowGuestDiv" style="display: none;">
	<div id="templateChatTopDiv">
		<span onclick="templateClickToChatHideGuest();"
			style="cursor: pointer;">No Thanks<img
			src="/theme/themeV3/themeImages/close_14x14.png" width="14"
			height="14" border="0" style="margin-left: 5px; margin-right: 5px" />
		</span>
	</div>
<!-- 	<div id="tempateChatImageDiv">
		<a
			onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
			href=""><img border="0"
			src="https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY" />
		</a>
	</div>
 -->	<div id="templateChatAccountLinksDiv">
		<ul>
			<li>To manage your account online - including reporting a
				problem with delivery or placing a temporary hold on your account -
				log in by entering your e-mail address and password. Click <a
				href="/login/auth.faces"> here</a> to access the login page now.</li>
			<li>Forgot your password or login? <a
				href="/idpassword/forgotpassword.jsp"> click here</a>.</li>
			<li>To contact us <a href="/feedback/feedback.jsp"> click here</a>.</li>
			<!-- 			<li>To cancel your subscription, please call 1-800-872-0001 and -->
			<!-- 				select option 6.</li> -->
		</ul>

	</div>

	<div id="templateChatLinksDiv">
		<ul>
			<li><a
				onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
				href="">Click to Chat</a>
			</li>
		</ul>
	</div>
	<div id="templateChatBottomDiv">
		<span class="outputText">Or, call us at:<br> <b>Call
				1-877-713-6241</b><br>(M-F 8:00 a.m. to 7 p.m. E.T.)</span>
	</div>
</div>
<%-- /tpl:insert --%>

		<%-- /tpl:put --%>
			</div>
		</DIV>
		<!-- end header -->
		<DIV id="content">
			<DIV id="whitebox">
				<%-- tpl:put name="bodyAreaMainWhiteBox" --%>
<DIV id="pageContentFullWidth">
			<%-- tpl:put name="mainBodyArea" --%>
							<h1>Invalid Access</h1>
							<br>
							<br>
					Access to your complimentary e-Newspaper is invalid because either the access code is invalid or expired or you are not accessing this page from the partner hotel property IP address.		
					
						<br>
							<br>
							<a
								href="https://service.usatoday.com/subscriptions/index.jsp?pub=EE">Click
								here if you would like to subscribe to USA TODAY's e-Newspaper.</a>
							<br>
						<%-- /tpl:put --%>
</DIV>
<%-- /tpl:put --%>
				<DIV id="clear"></DIV>
				<DIV id="bluebar">
					<a href="/feedback/feedback.jsp"
						style="color: white; font-weight: bold; text-decoration: none; font-size: 14px"
						title="Contact Us">Contact Us</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;1-800-872-0001&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a
						href="/faq/utfaq.jsp"
						style="color: white; font-weight: bold; text-decoration: none; font-size: 14px"
						title="Frequently Asked Questions">FAQ</a>
				</DIV>
				<%-- tpl:put name="footerMainArea" --%>
	<%-- tpl:put name="footerArea_1" --%><%-- /tpl:put --%>
	<%=trackingBug.getOmnitureHTML() %>
		<%-- /tpl:put --%>
				<DIV id=graybar>
					Copyright
					<script>
						document.write(getCurrentYear());
					</script>
					USA TODAY, a division of <a href="http://www.gannett.com">Gannett
						Co. Inc.</a> <a href="/privacy/privacy.htm">Privacy
						Notice/California Privacy Notice</a>. <a
						href="/privacy/privacy.htm#adchoice">Ad Choices</a>.<br> By
					using this service, you accept our <a href="/service/service.jsp">Terms
						of Service</a>.
				</DIV>
				<DIV id=copyright>
					<P>Offers available to new subscribers only. Home delivery not
						available in some areas. Not valid with any other offer.
						Prepayment required. After the promotional period ends, the
						subscription will continue and be billed monthly at the then
						regular rate, less any applicable credits, unless USA TODAY is
						otherwise notified. Applicable taxes may not be included in the
						rate. If at any time you decide to cancel your subscription, you
						may contact customer service at 1-800-872-0001 and the full amount
						of any balance over $2.00 will be returned.</P>
				</DIV>
				<!--END OF COPYWRITE-->
				<DIV id=footer></DIV>
			</DIV>
			<!--END OF WHITEBOX-->
		</DIV>
		<!--END OF CONTENT-->
	</DIV>
</BODY>
</HTML><%-- /tpl:insert --%>
</f:view> <%-- /tpl:insert --%>
<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/eedition/InvalidPartnerRequest.java" --%><%-- /jsf:pagecode --%>