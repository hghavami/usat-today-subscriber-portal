<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%><%@taglib
	uri="http://struts.apache.org/tags-tiles" prefix="tiles"%><tiles:insert
	definition="shopTemplateTiles">
	<tiles:put name="documentTitle" value="Verify Subscription Account"
		direct="true" type="string"></tiles:put>
	<tiles:put name="headarea"
		value="/tilesContent/validateAccount_headarea.jsp" type="page"></tiles:put>
	<tiles:put name="bodyarea"
		value="/tilesContent/validateAccount_bodyarea.jsp" type="page"></tiles:put>
</tiles:insert>