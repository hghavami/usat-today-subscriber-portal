<html>

<head>
<title>USA Today Online Subscriptions - Portal</title>
<script language="JavaScript">
</script>
</head>

<body>
<%@ page import = "com.usatoday.esub.common.*" %>
<%
	
	// backward compatibility...capture any pub/keycode changes
	com.usatoday.business.interfaces.products.OfferIntf offer = UTCommon.getCurrentOffer(request);

	//
	// Determine where the request is going
	//
	
	try {
		String otheropt = request.getParameter(UTCommon.OTHEROPT_QUERYSTRING);
		if (otheropt.length() > 0) {
			if (otheropt.equals("FIRST")) {                    
				UTCommon.showUrl(response, UTCommon.FIRSTTIME_URL + "/first_time.jsp");
			}
			if (otheropt.equals("FAQ")) {
				if (offer.getPubCode().equals(UTCommon.PUB_NAMES[0])) {
					UTCommon.showUrl(response, UTCommon.BASE_URL + "/faq/utfaq.jsp");
				} else {
					UTCommon.showUrl(response, UTCommon.BASE_URL + "/faq/bwfaq.jsp");
				}
			}
			if (otheropt.equals("FEED")) {
				UTCommon.showUrl(response, UTCommon.BASE_URL + "/feedback/feedback.jsp");
			} 
		} 
	} catch (NullPointerException e) {
	}
	
%>

<p>This page should never be displayed.</p>

</body>

</html>
