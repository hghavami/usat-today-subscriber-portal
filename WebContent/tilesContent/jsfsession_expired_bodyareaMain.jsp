<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%><%-- tpl:metadata --%>
	<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/tilesContent/Jsfsession_expired_bodyareaMain.java" --%><%-- /jsf:pagecode --%>
<%-- /tpl:metadata --%><%@taglib uri="http://java.sun.com/jsf/core"
	prefix="f"%><%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%><%@page language="java"
	contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%><hx:viewFragment
	id="viewFragment1"><%@page
		import="com.usatoday.esub.handlers.NavigationLinksHandler"%><hx:scriptCollector
		id="scriptCollector1">
		<h1>Session Expired</h1><h:outputText styleClass="outputText" id="textSessionExpiredMessage"
			value="Your session with this web application expired or was interrupted. Please start your transaction over. "></h:outputText>
		<br>
		<br>
		<h:outputText styleClass="outputText" id="textSessionExpiredMessage2"
			value="If problems persist, please call our National Customer Service for assistance."></h:outputText>
		<br>
		<br>
		<br>
		<hx:outputLinkEx value="/feedback/feedback.jsp"
			styleClass="outputLinkEx" id="linkEx1">
			<h:outputText id="text1" styleClass="outputText" value="Contact Us"></h:outputText>
		</hx:outputLinkEx>
		<br>
		<br>
	</hx:scriptCollector>
</hx:viewFragment>