<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%><%@page import="com.usatoday.esub.common.*"%><%@taglib
	uri="http://struts.apache.org/tags-logic" prefix="logic"%><%@taglib
	uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@page import="com.usatoday.esub.common.*"%>
<%
	//Added to capture Nexternal Category for redirect  sew
	String loginsessionobject = "";
	//
	// save the Nexternal Catg value
	//
	String catg = request.getParameter(UTCommon.NEXT_CATG_PARM);
	if (catg != null) {

		try {
			session.setAttribute(UTCommon.SESSION_NEXT_CATG, catg);
		} catch (Exception exp) {
			; // ignore
		}
	}
%>
<br />
&nbsp;&nbsp;
<h3>Current USA TODAY and Sports Weekly subscribers, complete the
	information below to receive your discount and free shipping</h3>

<html:form action="/validateAccountForNexternal" method="post">
	<logic:messagesPresent>

		<table width="550">
			<tr>
				<td>
					<ul>
						<li><span class="errorMessage"><html:errors /> </span></li>
					</ul>
				</td>
			</tr>
		</table>
	</logic:messagesPresent>
	<table height="4" width="732">
		<tbody>
			<tr>
				<td align="right" class="usatFormFont" nowrap width="10"></td>
				<td align="right" class="usatFormFont" nowrap width="130"><b>Step
						1:</b>
				</td>
				<td width="5">&nbsp;</td>
				<td nowrap></td>
				<td nowrap align="center" colspan="2"></td>

			</tr>
			<tr>
				<td align="right" class="usatFormFont" nowrap width="10" height="41"></td>
				<td align="right" class="usatFormFont" nowrap height="41">Email
					Address:</td>
				<td width="5" height="41">&nbsp;</td>
				<td nowrap height="41"><html:text property="emailAddress" size="60"
						maxlength="60"></html:text>
				</td>
				<td nowrap align="center" colspan="2"></td>
			</tr>
			<tr>
				<td align="right" class="usatFormFont" nowrap height="2"></td>
				<td align="right" class="usatFormFont" nowrap height="2"></td>
				<td width="5" height="2"></td>
				<td nowrap height="2"></td>
				<td nowrap align="center" colspan="2"></td>
			</tr>

		</tbody>
	</table>
	<table height="4" width="732">
		<tbody>
			<tr>
				<td align="right" class="usatFormFont" nowrap width="10"></td>
				<td align="right" class="usatFormFont" nowrap width="130"><b>Step 2:</b></td>
				<td width="5">&nbsp;</td>
				<td nowrap></td>
				<td nowrap align="center"></td>
				<td nowrap align="right" width="129"></td>
				<td nowrap width="234"></td>
			</tr>
			<tr>
				<td align="right" class="usatFormFont" nowrap width="10" height="41"></td>
				<td align="right" class="usatFormFont" nowrap width="130" height="41">Account
					Number:</td>
				<td width="5" height="41">&nbsp;</td>
				<td nowrap height="41"><html:text property="accountNumber" size="10"
						maxlength="9"></html:text></td>
				<td nowrap align="center" height="41"><b>- OR -</b></td>
				<td align="right" class="usatFormFont" nowrap height="41">Phone Number:</td>
				<td nowrap height="41" width="234"><html:text property="phoneAreaCode"
						size="4" maxlength="3"></html:text> <html:text property="phoneExchange"
						size="4" maxlength="3">&nbsp;&nbsp;</html:text> <html:text
						property="phoneExtension" size="5" maxlength="4"></html:text></td>
			</tr>
			<tr>
				<td align="right" class="usatFormFont" nowrap height="2"></td>
				<td align="right" class="usatFormFont" nowrap width="130" height="2"><b></b></td>
				<td width="5" height="2"></td>
				<td nowrap height="2"></td>
				<td nowrap height="2"></td>
				<td nowrap height="2" width="129"></td>
				<td nowrap height="2" width="234"></td>
			</tr>

		</tbody>
	</table>
	<table>
		<tbody>
			<tr>
				<td align="right" class="usatFormFont" nowrap width="10"></td>
				<td align="right" class="usatFormFont" nowrap width="130"><b>Step
						3:</b>
				</td>
				<td width="5">&nbsp;</td>
				<td nowrap></td>
				<td nowrap align="center"></td>
				<td nowrap align="right"></td>
			</tr>
			<tr>
				<td align="right" class="usatFormFont" nowrap></td>
				<td align="right" class="usatFormFont" nowrap width="130">Delivery Zip
					Code:</td>
				<td width="5">&nbsp;</td>
				<td nowrap align="left"><html:text property="zip" size="6"
						maxlength="5"></html:text></td>
				<td nowrap align="left"></td>
				<td nowrap></td>
			</tr>
			<tr>
				<td align="left" nowrap></td>
				<td align="left" nowrap width="130"></td>
				<td width="5">&nbsp;</td>
				<td nowrap></td>
				<td nowrap></td>
				<td nowrap></td>
				<td nowrap></td>
			</tr>
			<tr>
				<td align="right"></td>
				<td align="right" width="130"></td>
				<td width="5">&nbsp;</td>
				<td colspan="3"><html:submit property="Submit"
						value="Verify Subscription"></html:submit></td>
			</tr>
		</tbody>
	</table>

</html:form>
