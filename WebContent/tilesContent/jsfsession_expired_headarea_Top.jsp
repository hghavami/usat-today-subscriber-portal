<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%><%-- jsf:pagecode language="java" location="/JavaSource/pagecode/tilesContent/Jsfsession_expired_headarea_Top.java" --%><%-- /jsf:pagecode --%><%@taglib
	uri="http://java.sun.com/jsf/core" prefix="f"%><%@taglib
	uri="http://java.sun.com/jsf/html" prefix="h"%><%@page language="java"
	contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%><f:view><%@page
		import="com.usatoday.esub.handlers.NavigationLinksHandler"%>

	<script language="JavaScript">
// method called on page load
function _initPage() {
	// insert site-wide code here
	
	// gridLeftNavGroup1Grid
	// gridLeftNavGroup2Grid
	var panel = $('gridLeftNavGroup1Grid');
	var cancels = $('linkExNavArea2Link5');
	if (panel && showNav1) {
		panel.show();
	}
	else {
		panel.hide();
	}
	
	panel = $('gridLeftNavGroup2Grid');
	if (panel && showNav2) {
		panel.show();
	}
	else {
		panel.hide();
	}	
	
	// Check if cancels are allowed and display icon
	if (cancels && showCancels) {
		cancels.show();
	} 
	else {
		cancels.hide();
	}
	
	
	// you typically would run this after all site-wide
	// execution has processed, but it's really up to
	// the design of your site to when you may want to run this
	
	if( typeof window.initPage == "function" ) {
		window.initPage();
	}

}


function _loadFAQ() {
	if( typeof loadFAQ == "function" ) {
		loadFAQ();
	}
	else {
		window.open('/faq/utfaq.jsp','FAQ', 'width=825,height=800,menubar=no,titlebar=yes,scrollbars=yes,status=no,location=no,directories=no');
	}
}


function func_Logout(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
document.location = '/index.jsp?logsessobj=rem';
}</script>

</f:view>