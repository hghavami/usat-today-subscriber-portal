<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%><%@taglib
	uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%><tiles:insert
	definition="baseThemeNoLeftNav_JSF">
	<tiles:put name="bodyareaHeaderRight"
		value="/tilesContent/jsfsession_expired_bodyareaHeaderRight.jsp"
		type="page"></tiles:put>
	<tiles:put name="headarea_Top"
		value="/tilesContent/jsfsession_expired_headarea_Top.jsp" type="page"></tiles:put>
	<tiles:put name="bodyareaMain"
		value="/tilesContent/jsfsession_expired_bodyareaMain.jsp" type="page"></tiles:put>
	<tiles:put name="footerarea1"
		value="/tilesContent/jsfsession_expired_footerarea1.jsp" type="page"></tiles:put>
	<tiles:put name="documentTitle" value="View Expired" direct="true"
		type="string"></tiles:put>
	<tiles:put name="headarea_Bottom"
		value="/tilesContent/jsfsession_expired_headarea_Bottom.jsp"
		type="page"></tiles:put>
</tiles:insert>