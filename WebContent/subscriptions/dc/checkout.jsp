<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/subscriptions/dc/Checkout.java" --%><%-- /jsf:pagecode --%>
<%-- %@page language="java" contentType="text/html;" pageEncoding="UTF-8"% --%>
<%-- tpl:insert page="/theme/themeV3/dotcomTheme/dotcomJSFDT.jtpl" --%><!doctype html><%@page
	import="com.usatoday.business.interfaces.products.ProductIntf"%>
<%@page import="com.usatoday.businessObjects.products.ProductBO"%>
<%@page import="com.usatoday.util.constants.UsaTodayConstants"%>
<!--[if IE 7]><html class="no-js lt-ie11 lt-ie10 lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie11 lt-ie10 lt-ie9" lang="en"><![endif]-->
<!--[if IE 9]><html class="no-js lt-ie11 lt-ie10" lang="en"><![endif]-->
<!--[if IE 10]><html class="no-js lt-ie11" lang="en"><![endif]-->
<!--[if gt IE 10]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<%@page language="java"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%><%@taglib
	uri="http://java.sun.com/jsf/html" prefix="h"%><%@taglib
	uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%
	String dotcomDomain = "usatoday.com";
	if (UsaTodayConstants.debug) {
		dotcomDomain = "ux-dev.usatoday.com";
	}

	com.usatoday.esub.common.WebBug trackingBug = com.usatoday.esub.common.UTCommon
			.getWebAnalyticsBug();
	try {
		trackingBug.deriveValuesFromRequest(request);
	} catch (Exception e) {
		; // ignore
	}
	String pubCode = "UT";
	String productName = "USA TODAY";
	com.usatoday.business.interfaces.products.OfferIntf offer = null;
	try {
		offer = com.usatoday.esub.common.UTCommon
				.getCurrentOfferVersion2(request);
		if (offer != null) {
			pubCode = offer.getPubCode();
			try {
				String brandingPubCode = offer.getProduct()
						.getBrandingPubCode();
				ProductIntf bp = ProductBO
						.fetchProduct(brandingPubCode);
				productName = bp.getName();
			} catch (Exception teee) {
			}
		}
	} catch (Exception ee) {
	}
%>
<head prefix="og: http://ogp.me/ns#">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<%-- tpl:put name="headarea" --%>
	<title>USA TODAY.com - Checkout</title>
<%-- /tpl:put --%>
<meta name="ROBOTS" content="NOODP, NOYDIR" />
<meta property="fb:app_id" content="251772488263583" />
<meta property="og:description" content="" />
<meta property="og:image" content="/static/images/logos/.png">
<meta property="og:url" content="" />
<meta property="og:type" content="website">
<meta property="og:title" content="" />
<meta name="viewport" content="width=1024">
<link rel="shortcut icon" href="/static/images/favicon.png">
<link rel="stylesheet" href="/static/css/main.min.css?v=off">
<!--[if lt IE 9]><link rel="stylesheet" href="/static/css/main.min.css?v=UX-122-1986-g45e8daf-ab032e7608bb11e2af2312313b0f0def"><script src="/static/js/libs/respond/respond.min.js"></script><![endif]-->
<link rel="stylesheet"
	href="/static/css/print.css?v=UX-122-1986-g45e8daf-ab032e7608bb11e2af2312313b0f0def"
	media="print">
<script type="text/javascript">
	window.site_config = {
		"ADS" : {
			"SLIDER" : {
				"REFRESHTIME" : 900000
			},
			"THRESHOLDS" : {
				"SLIDE" : [ 3, 10, 10, -1 ],
				"ASSET" : [ 2, 6, 6, -1 ]
			},
			"PROVIDER" : "DFP",
			"DFP" : {
				"ACCOUNTNAME" : "usatoday",
				"ENV" : "dev",
				"ACCOUNTID" : "7070"
			},
			"SHOW" : true
		},
		"AUTH" : {
			"FACEBOOK" : {
				"PERMISSIONS" : "user_about_me,user_groups,user_location,user_interests,user_events,user_likes,user_education_history,friends_about_me,friends_likes,publish_stream",
				"CHANNELURL" : "/static/html/channel.html",
				"APPID" : "251772488263583"
			}
		}
	};
	window.use_minified_css = true;
	window.isAdDebug = false;
</script>
<script src="/static/js/libs/modernizr/modernizr.js"></script>
<%-- tpl:put name="headarea_2" --%>
	<!--<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">-->
	<%
		if (trackingBug != null) {
			trackingBug
					.setOmniturePageNameOverride("Subscriptions:New Subscription: USATODAY.com One Page Check Out Page");
		}
	%>
	<!-- begin Marin Software Tracking Script for Elite SEM-->
	<!-- 	<script type='text/javascript'>
		var _marinClientId = "518c2822647";
		var _marinProto = (("https:" == document.location.protocol) ? "https://"
				: "http://");
		document.write(unescape("%3Cscript src='" + _marinProto
				+ "tracker.marinsm.com/tracker/" + _marinClientId
				+ ".js' type='text/javascript'%3E%3C/script%3E"));
	</script>
	<script type='text/javascript'>
		try {
			_marinTrack.trackPage();
		} catch (err) {
		}
	</script>
	<noscript>
		<img
			src="https://tracker.marinsm.com/tp?act=1&cid=518c2822647&script=no">
	</noscript>
 -->
	<!-- end Copyright Marin Software -->
	<script src="/common/prototype/prototype.js"></script>
	<script src="/common/prototype/scriptaculous/scriptaculous.js"
		type="text/javascript"></script>
	<script src="/common/usatCommon.js"></script>
	<script type="text/javascript" src="/common/popUpOverlay.js"></script>
	<script language="JavaScript" src="/common/pubDateValidator.js"></script>
	<link href="/theme/themeV3/css/dotcom.css" rel="stylesheet"
		type="text/css">
	<script type="text/javascript">
var checkoutGuaranteeDivElement;
function initPage(thisObj, thisEvent) {
	// for product messageing in popUpOverlay.js
	usat_productName = "<%=productName%>";

			usatOE_selectedTermRequiresEZPay = usatOE_isForceEZPayTermSelectedFunc();

			try {
				usatOE_updateDeliveryInfoPanelClass();
			} catch (err_2) {
			}

			try {
				usat_verifyDomain();
			} catch (err4) {
			}

			try {

				checkoutGuaranteeDivElement = $('checkoutGuaranteeDiv');

				new Draggable(checkoutGuaranteeDivElement);

			} catch (e) {
				; // ignore
			}
		}

		function func_2(thisObj, thisEvent) {
			usatOE_updateDeliveryInfoPanelClass();
		}

		function func_7(thisObj, thisEvent) {
			dateChanged(thisObj);
		}
		function func_9(thisObj, thisEvent) {

			thisObj.value = "Please Wait...";
			return true;
		}
		function func_10(thisObj, thisEvent) {
			thisObj.value = "Please Wait...";

			var windowOverlay = $('formSubmittedOverlay');

			windowOverlay.show();

			var windowOverlayInfo = $('formSubmittedOverlayInfo');
			windowOverlayInfo.show();

			setTimeout("usatOE_updateAnimation()", 200);

			return true;
		}
		function func_revalidateDelMethod(thisObj, thisEvent) {
			try {
				validateState();
				var delMethodVar = $('formOrderEntryForm:textDeliveryMethodTextDeterminedValue');

				delMethodVar.innerHTML = "Not Determined";
			} catch (e) {
				// ignore
			}
		}
		function validateState() {
			var stateVar = $('formOrderEntryForm:menuDeliveryState');
			if (stateVar.value == "HI") {
				alert("Thank you for your interest in subscribing to USA TODAY! All subscriptions to USA TODAY delivered in the state of Hawaii are handled by our partner, the Honolulu Star-Advertiser. For information about receiving delivery of USA TODAY in Hawaii, including pricing (which may vary from what is shown here), please contact the Honolulu Star-Advertiser's Customer Service Department by calling 808-538-NEWS (6397) from 5:30am to 5:00 pm Monday-Friday and 6:30am to 10:00 am on weekends (all times HST).");
				stateVar.value = "NONE";
				stateVar.focus();
				stateVar.select();
			}
		}
		function func_10(thisObj, thisEvent) {
			thisObj.value = "Please Wait...";

			var windowOverlay = $('formSubmittedOverlay');

			windowOverlay.show();

			var windowOverlayInfo = $('formSubmittedOverlayInfo');
			windowOverlayInfo.show();

			setTimeout("usatOE_updateAnimation()", 200);

			return true;
		}
	</script>
	<!-- 	<script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push([ '_setAccount', 'UA-23158809-1' ]);
		_gaq.push([ '_trackPageview' ]);

		(function() {
			var ga = document.createElement('script');
			ga.type = 'text/javascript';
			ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl'
					: 'http://www')
					+ '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(ga, s);
		})();
	</script>
 -->
<%-- /tpl:put --%>
</head>
<f:view>
	<f:loadBundle basename="resourceFile" var="labels" />
	<body class=" third-party">
		<div style="float: none">
	<%
		String uri = request.getRequestURI();
		String pageName = uri.substring(uri.lastIndexOf("/") + 1);
		String jScript = "";
		if (uri.indexOf("subscriptions") > 0 && uri.indexOf("thankyou.jsp") > 0) {
			try {
				com.usatoday.esub.handlers.ShoppingCartHandler shoppingCartHandler = (com.usatoday.esub.handlers.ShoppingCartHandler) session.getAttribute("shoppingCartHandler");
				if (shoppingCartHandler != null) {
					jScript = shoppingCartHandler.getOnePageThankYouJavaScriptTealiumText();			
				}
			} catch (Exception E) {
		
			} 
		} else {
			com.usatoday.businessObjects.products.promotions.PromotionSet currentOfferPromotionSetTealium = null;
			com.usatoday.business.interfaces.products.OfferIntf offerTealium = com.usatoday.esub.common.UTCommon.getCurrentOffer(request);
			if (offerTealium != null && offerTealium instanceof com.usatoday.business.interfaces.products.SubscriptionOfferIntf) {
				currentOfferPromotionSetTealium = ((com.usatoday.business.interfaces.products.SubscriptionOfferIntf)offer).getPromotionSet();
				jScript = currentOfferPromotionSetTealium.getPromoOnePageJavaScriptTealiumText();
				jScript = org.apache.commons.lang3.StringUtils.replace(jScript, "page_name:\"\"", "page_name:\"" + uri + "\"");
			}
		}
	%>
	<%=jScript%>
			<%-- 		<f:verbatim>
			<h:outputText styleClass="outputText" id="textThankYouJS13"	escape="false" value="#{shoppingCartHandler.tealiumOnePageThankYouJavaScriptText}"></h:outputText>
		</f:verbatim>
 --%>
		</div>
		<header id="header">
			<div class="fixed-wrap">
				<div id="masthead" class="clearfix">
					<div class="logo ">
						<a id="logo-link" data-ht="Headerusatoday"
							href="http://<%=dotcomDomain%>/">USA Today</a>
					</div>
					<div id="search-form">
						<form class="search" action="http://<%=dotcomDomain%>/search/"
							method="get">
							<input class="text-input" type="text" data-placeholder=""
								name="q"><input class="submit" type="submit"
								value="Search"><span
								class="ui-dark ui-btn cancel icon no-text">X</span>
						</form>
					</div>
				</div>
				<nav id="nav">
					<ul id="navbar">
						<li class="news text" data-section="news"><a
							class="nav-anchor"
							href="http://<%=dotcomDomain%>/news/#ht=Headernews"
							data-ht="Headernews"><span class="news-span nav-span">News</span>
						</a></li>
						<li class="sports text" data-section="sports"><a
							class="nav-anchor"
							href="http://<%=dotcomDomain%>/sports/#ht=Headersports"
							data-ht="Headersports"><span class="sports-span nav-span">Sports</span>
						</a></li>
						<li class="life text" data-section="life"><a
							class="nav-anchor"
							href="http://<%=dotcomDomain%>/life/#ht=Headerlife"
							data-ht="Headerlife"><span class="life-span nav-span">Life</span>
						</a></li>
						<li class="money text" data-section="money"><a
							class="nav-anchor"
							href="http://<%=dotcomDomain%>/money/#ht=Headermoney"
							data-ht="Headermoney"><span class="money-span nav-span">Money</span>
						</a></li>
						<li class="tech text" data-section="tech"><a
							class="nav-anchor"
							href="http://<%=dotcomDomain%>/tech/#ht=Headertech"
							data-ht="Headertech"><span class="tech-span nav-span">Tech</span>
						</a></li>
						<li class="travel text" data-section="travel"><a
							class="nav-anchor"
							href="http://<%=dotcomDomain%>/travel/#ht=Headertravel"
							data-ht="Headertravel"><span class="travel-span nav-span">Travel</span>
						</a></li>
						<li class="opinion text" data-section="opinion"><a
							class="nav-anchor"
							href="http://<%=dotcomDomain%>/opinion/#ht=Headeropinion"
							data-ht="Headeropinion"><span class="opinion-span nav-span">Opinion</span>
						</a></li>
						<li class="weather icon text" data-section="weather"><a
							class="nav-weather-link" href="http://<%=dotcomDomain%>/weather/"
							data-ht="HeaderWeather"><span class="wbtn-wrap nav-span"><span
									class="btn-temp"></span><span class="btn-label">Weather</span>
							</span> </a>
							<div class="dropdown">
								<div class="dropdown-panel">
									<div class="front-side"></div>
									<div class="back-side">
										<p class="heading">Settings</p>
										<form id="weather-dropdown-form" name="weather-dropdown-form"
											method="get">
											<p class="label">Enter City, State or Zip</p>
											<input class="ui-text-input query" type="text" value="">
											<p class="ui-btn ui-dark ui-btn2 icon cancel cancel-btn">Cancel</p>
											<p class="ui-btn ui-light ui-btn2 icon checkmark set submit">Set</p>
										</form>
										<p class="cancel-x">Cancel</p>
									</div>
								</div>
								<p class="full-forecast">
									<a href="/weather/"><span>Full Forecast</span> </a>
								</p>
							</div></li>
						<li class="media icon"><a class="icon-anchor"
							href="http://<%=dotcomDomain%>/media/latest/news/"
							data-ht="HeaderMedia"><span class="icon-span">Media</span> </a></li>
						<li class="cover icon"><a
							href="http://<%=dotcomDomain%>/cover-view/" class="icon-anchor"
							data-ht="colorbarcoverview" title="Cover view"><span
								class="icon-span">Cover View</span> </a></li>
						<!--<li class="sbs icon"><a href="#" data-ht="HeaderStateByState"><span>State by State</span></a></li>-->
						<!--<li class="timeline icon"><a href="#" data-ht="HeaderTimeline"><span>Timeline</span></a></li>-->
						<!--<li class="ugc icon"><a href="#" data-ht="HeaderUGC"><span>You Report</span></a></li>-->
					</ul>
				</nav>
				<div id="breaking" data-lastupdate="" data-type="">
					<div id="breaking-wrap">
						<p>
							<strong></strong><span class="close-btn">Close</span>
						</p>
					</div>
				</div>
				<div class="header-shadow"></div>
			</div>
		</header>
		<article id="cards" class="cards">
			<div class="card-container">
				<section class="card-wrap">
					<div class="ad_container1">
						<div id="partner__leavebehind"
							class="partner-placement-leavebehind"
							data-slot-id="partner_high_impact_"></div>
						<div id="partner_high_impact__pushdown"
							class="partner-placement pushdown" data-sizes="pushdown"
							data-slot-id="partner_high_impact_" data-shared-placement="true"></div>
						<div id="partner_high_impact__livefeed"
							class="partner-placement livefeed" data-sizes="livefeed"
							data-slot-id="partner_high_impact_" data-shared-placement="true"></div>
					</div>
					<section id="section_" class=" card " data-section-id=""
						data-subsection-id="" data-last-update="" data-template="">
						<div class="card-content">

							<!-- START THIRD PARTY CONTENT -->
							<%-- tpl:put name="bodyarea" --%>
								<hx:scriptCollector id="scriptCollectorMainOrderEntryCollector"
									preRender="#{pc_Checkout1.onPageLoadBegin}">
									<%-- 									<div style="float: none">
										<f:verbatim>
											<h:outputText styleClass="outputText" id="textPageJS3"
												escape="false"
												value="#{currentOfferHandler.onePageJavaScript3Text}"></h:outputText>
										</f:verbatim>
									</div>
 --%>
									<h1>
										<h:outputText id="textMainTableHeader"
											value="Subscribe To #{currentOfferHandler.productName}"
											styleClass="H1Class"></h:outputText>
									</h1>
									<div style="float: none">
										<f:verbatim>
											<h:outputText styleClass="outputText" id="textPageJS2"
												escape="false"
												value="#{currentOfferHandler.onePageJavaScript2Text}"></h:outputText>
										</f:verbatim>
									</div>
									<h:form styleClass="form" id="formOrderEntryForm">
										<div id="pageContentEEOrderEntryLeft">
											<div id="headerPromoTextDiv">
												<h:outputText id="textPromoText1" escape="false"
													value="#{currentOfferHandler.onePagePromotionText1}"></h:outputText>
											</div>
											<div id="headerDelvMethodTextDiv">
												<h:outputText id="textDelvMethodText" escape="false"
													value="#{newSubscriptionOrderHandler.deliveryMethodErrorText}"></h:outputText>
											</div>
											<div id="orderFormBox">

												<hx:panelLayout styleClass="panelLayoutOrderForm"
													id="layoutPageLayout">
													<f:facet name="body">
														<h:panelGroup styleClass="panelGroup"
															id="groupMainBodyPanel">

															<h:panelGrid id="gridTermsGrid" columns="1"
																styleClass="orderFormGridStyles">
																<f:facet name="header">
																	<h:panelGroup styleClass="panelGroup" id="group10">
																		<h:outputText styleClass="outputText footnote"
																			id="text7"
																			value='<span style="color:red">*</span> Required Fields'
																			escape="false"></h:outputText>
																	</h:panelGroup>
																</f:facet>
																<hx:panelFormBox helpPosition="over"
																	labelPosition="left"
																	styleClass="panelFormBoxTerms orderFormInsideGridStyles"
																	id="formBoxTermsFormBox" label="#{labels.termLabel3}"
																	widthLabel="15">
																	<hx:formItem styleClass="formItem"
																		id="formItemSubscriptionTerms"
																		errorText="#{labels.requiredFieldMsg}">
																		<h:selectOneRadio
																			disabledClass="selectOneRadio_Disabled"
																			enabledClass="selectOneRadio"
																			styleClass="selectOneRadio" id="radioTermsSelection"
																			layout="pageDirection" required="true" tabindex="1"
																			requiredMessage="#{labels.termRequiredErrorMsg}"
																			value="#{newSubscriptionOrderHandler.selectedTerm}"
																			validatorMessage="Please choose one of the available terms."
																			onclick="return usatOE_selectedTermChanged(this, event);">
																			<hx:inputHelperAssist
																				errorClass="selectOneRadio_Error" id="assist1" />
																			<f:selectItems
																				value="#{currentOfferHandler.offerTermsSelectItems}" />
																		</h:selectOneRadio>
																		<h:message for="radioTermsSelection"></h:message>

																	</hx:formItem>
																	<f:facet name="bottom">
																		<h:panelGrid styleClass="panelGrid"
																			id="gridTermsFormBoxBottomFacetGrid" columns="2"
																			cellspacing="0" cellpadding="1" rendered="false">
																			<%-- 																			rendered="#{currentOfferHandler.isForceEZPAYRate or currentOfferHandler.isForceEZPAY }"> --%>
																			<f:facet name="footer">
																				<h:panelGroup styleClass="panelGroup" id="group4">
																					<h:outputText styleClass="outputText"
																						id="textEZPayExplanationRateCustomText"
																						value="#{currentOfferHandler.forceEZPayCustomTermsText}"
																						escape="false"></h:outputText>
																				</h:panelGroup>
																			</f:facet>
																			<h:outputText styleClass="outputText"
																				id="textEZPayExplanationMessage"
																				value="#{labels.EZPayPlanTextUnderTermsMultiPub}"
																				escape="false"></h:outputText>
																			<h:outputText styleClass="requestLink"
																				id="textEZPayLearnMore"
																				value="#{currentOfferHandler.addressLearnMoreHTML}"
																				escape="false"></h:outputText>

																		</h:panelGrid>
																	</f:facet>

																</hx:panelFormBox>
															</h:panelGrid>
															<!-- end of terms grid -->


															<h:panelGrid styleClass="panelGrid orderFormGridStyles"
																id="gridStartDateInformation" columns="1"
																rendered="#{not trialCustomerHandler.isTrialConversion }">
																<hx:panelFormBox helpPosition="under"
																	labelPosition="left"
																	styleClass="panelFormBoxOrderEntryForm"
																	id="formBoxStartDateInformation">

																	<hx:formItem styleClass="formItem"
																		id="formItemStartDateFormItem"
																		label=' #{labels.SubStartDate} <span style="color:red">*</span>'>

																		<h:inputText styleClass="inputTextOrderForm"
																			id="textStartDateInput" size="12"
																			value="#{newSubscriptionOrderHandler.startDate}"
																			tabindex="11" onchange="return func_7(this, event);"
																			required="true"
																			requiredMessage="Subscription Start Date is Required.">
																			<hx:convertDateTime pattern="MM/dd/yyyy" />
																			<hx:inputHelperDatePicker id="datePicker1"
																				multiLine="false" />
																			<hx:validateDateTimeRange
																				maximum="#{currentOfferHandler.latestPossibleStartDate}"
																				minimum="#{currentOfferHandler.earliestPossibleStartDate}" />
																			<hx:inputHelperAssist errorClass="inputText_Error"
																				onfocusSuccessClass="inputTextFocusOrderForm "
																				successClass="inputTextOrderForm " validation="true" />
																		</h:inputText>
																		<h:outputText escape="false"
																			id="textStartDateLearnMore"
																			value="#{currentOfferHandler.addressLearnMoreHTML}"
																			styleClass="requestLink"></h:outputText>

																	</hx:formItem>

																</hx:panelFormBox>
																<hx:ajaxRefreshSubmit
																	target="formBoxStartDateInformation"
																	id="ajaxRefreshSubmitStartDate"></hx:ajaxRefreshSubmit>
															</h:panelGrid>

															<h:panelGrid styleClass="panelGrid orderFormGridStyles"
																id="gridDeliveryInformation" columns="1">
																<hx:panelFormBox helpPosition="under"
																	labelPosition="left"
																	styleClass="panelFormBoxOrderEntryForm"
																	id="formBoxDeliveryInformation"
																	label="#{labels.emailAddressRecipientLabel3}">

																	<hx:formItem styleClass="formItem"
																		id="formItemDeliveryFName"
																		label='#{labels.firstNameLabel} <span style="color:red">*</span>'
																		errorText="#{labels.requiredFieldMsg}">

																		<h:inputText id="textDeliveryFirstName" tabindex="20"
																			value="#{newSubscriptionOrderHandler.deliveryFirstName}"
																			maxlength="10" required="true"
																			requiredMessage="First Name is Required"
																			validatorMessage="First Name is Not Valid"
																			styleClass="inputTextOrderForm">
																			<f:validateLength minimum="1" maximum="15" />
																			<hx:inputHelperAssist errorClass="inputText_Error"
																				onfocusSuccessClass="inputTextFocusOrderForm "
																				successClass="inputTextOrderForm " validation="true" />
																		</h:inputText>

																	</hx:formItem>
																	<hx:formItem styleClass="formItem"
																		id="formItemDeliveryLastName"
																		label='#{labels.lastNameLabel} <span style="color:red">*</span>'
																		errorText="#{labels.requiredFieldMsg}">

																		<h:inputText styleClass="inputTextOrderForm"
																			id="textDeliveryLName" tabindex="21"
																			value="#{newSubscriptionOrderHandler.deliveryLastName}"
																			maxlength="15" required="true"
																			requiredMessage="Last Name is Required"
																			validatorMessage="Last Name is Not Valid">
																			<f:validateLength minimum="1" maximum="20" />
																			<hx:inputHelperAssist errorClass="inputText_Error"
																				onfocusSuccessClass="inputTextFocusOrderForm "
																				successClass="inputTextOrderForm " validation="true" />
																		</h:inputText>

																	</hx:formItem>
																	<hx:formItem styleClass="formItem"
																		id="formItemDeliveryCompanyName"
																		label="#{labels.companyNameLabel}">
																		<h:inputText styleClass="inputTextOrderForm"
																			id="textDeliveryCompanyName" tabindex="22"
																			value="#{newSubscriptionOrderHandler.deliveryCompanyName}"
																			maxlength="28">
																			<f:validateLength maximum="28" />
																			<hx:inputHelperAssist errorClass="inputText_Error"
																				onfocusSuccessClass="inputTextFocusOrderForm "
																				successClass="inputTextOrderForm " validation="true" />
																		</h:inputText>
																	</hx:formItem>

																	<hx:formItem styleClass="formItem"
																		id="formItemDeliveryAddress1"
																		label='#{labels.address} <span style="color:red">*</span>'
																		errorText="#{labels.requiredFieldMsg}">
																		<h:inputText styleClass="inputTextOrderForm"
																			id="textDeliveryAddress1Text" tabindex="23"
																			value="#{newSubscriptionOrderHandler.deliveryAddress1}"
																			maxlength="28" required="true"
																			requiredMessage="Address is Required"
																			validatorMessage="Address is Not Valid"
																			onchange="func_revalidateDelMethod(this, event);">
																			<f:validateLength minimum="1" maximum="28" />
																			<hx:inputHelperAssist errorClass="inputText_Error"
																				onfocusSuccessClass="inputTextFocusOrderForm "
																				successClass="inputTextOrderForm " validation="true" />
																		</h:inputText>
																		<h:inputText styleClass="inputTextOrderForm"
																			id="textDeliveryAptSuite" tabindex="24"
																			value="#{newSubscriptionOrderHandler.deliveryAptSuite}"
																			maxlength="28"
																			onchange="func_revalidateDelMethod(this, event);">
																			<hx:inputHelperAssist errorClass="inputText_Error"
																				onfocusSuccessClass="inputTextFocusOrderForm "
																				successClass="inputTextOrderForm " validation="true" />
																		</h:inputText>
																	</hx:formItem>
																	<hx:formItem styleClass="formItem"
																		id="formItemDeliveryAddress2"
																		label="#{labels.personalDeliveryInfolabel}"
																		rendered="true">
																		<h:inputText styleClass="inputTextOrderForm"
																			id="textDeliveryAddress2" tabindex="25"
																			value="#{newSubscriptionOrderHandler.deliveryAddress2}"
																			maxlength="35" size="40">
																			<f:validateLength minimum="0" maximum="35" />
																			<hx:inputHelperAssist errorClass="inputText_Error"
																				onfocusSuccessClass="inputTextFocusOrderForm "
																				successClass="inputTextOrderForm " validation="true" />
																		</h:inputText>
																		<h:outputText escape="false" id="textAddrLearnMore"
																			value="#{currentOfferHandler.addressLearnMoreHTML}"
																			styleClass="requestLink"></h:outputText>


																	</hx:formItem>

																	<hx:formItem styleClass="formItem"
																		id="formItemDeliveryCity"
																		label='#{labels.cityLabel} <span style="color:red">*</span>'
																		errorText="#{labels.requiredFieldMsg}">
																		<h:inputText styleClass="inputTextOrderForm"
																			id="textDeliveryCity" tabindex="26"
																			onchange="func_revalidateDelMethod(this, event);"
																			value="#{newSubscriptionOrderHandler.deliveryCity}"
																			maxlength="28" required="true"
																			requiredMessage="City is Required"
																			validatorMessage="City is Not Valid">
																			<f:validateLength minimum="1" maximum="28" />
																			<hx:inputHelperAssist errorClass="inputText_Error"
																				onfocusSuccessClass="inputTextFocusOrderForm "
																				successClass="inputTextOrderForm " validation="true" />
																		</h:inputText>
																	</hx:formItem>

																	<hx:formItem styleClass="formItem"
																		id="formItemDeliveryState"
																		label='#{labels.StateZipLabel} <span style="color:red">*</span>'
																		errorText="#{labels.requiredFieldMsg}">
																		<h:selectOneMenu
																			styleClass="selectOneMenu selectOneMenu2"
																			id="menuDeliveryState" tabindex="27"
																			onchange="func_revalidateDelMethod(this, event);"
																			requiredMessage="State is Required"
																			validatorMessage="State is Not Valid" required="true"
																			value="#{newSubscriptionOrderHandler.deliveryState}">
																			<f:selectItem itemValue="NONE" itemLabel="" />
																			<f:selectItem itemValue="AL" itemLabel="Alabama" />
																			<f:selectItem itemValue="AK" itemLabel="Alaska" />
																			<f:selectItem itemValue="AZ" itemLabel="Arizona" />
																			<f:selectItem itemValue="AR" itemLabel="Arkansas" />
																			<f:selectItem itemValue="AA"
																				itemLabel="(AA) Armed Forces Americas" />
																			<f:selectItem itemValue="AE"
																				itemLabel="(AE) Armed Forces Africa" />
																			<f:selectItem itemValue="AE"
																				itemLabel="(AE) Armed Forces Canada" />
																			<f:selectItem itemValue="AE"
																				itemLabel="(AE) Armed Forces Europe" />
																			<f:selectItem itemValue="AE"
																				itemLabel="(AE) Armed Forces Middle East" />
																			<f:selectItem itemValue="AP"
																				itemLabel="(AP) Armed Forces Pacific" />
																			<f:selectItem itemValue="CA" itemLabel="California" />
																			<f:selectItem itemValue="CO" itemLabel="Colorado" />
																			<f:selectItem itemValue="CT" itemLabel="Connecticut" />
																			<f:selectItem itemValue="DE" itemLabel="Delaware" />
																			<f:selectItem itemValue="DC"
																				itemLabel="District of Columbia" />
																			<f:selectItem itemValue="FL" itemLabel="Florida" />
																			<f:selectItem itemValue="GA" itemLabel="Georgia" />
																			<f:selectItem itemValue="HI" itemLabel="Hawaii" />
																			<f:selectItem itemValue="ID" itemLabel="Idaho" />
																			<f:selectItem itemValue="IL" itemLabel="Illinois" />
																			<f:selectItem itemValue="IN" itemLabel="Indiana" />
																			<f:selectItem itemValue="IA" itemLabel="Iowa" />
																			<f:selectItem itemValue="KS" itemLabel="Kansas" />
																			<f:selectItem itemValue="KY" itemLabel="Kentucky" />
																			<f:selectItem itemValue="LA" itemLabel="Louisiana" />
																			<f:selectItem itemValue="ME" itemLabel="Maine" />
																			<f:selectItem itemValue="MD" itemLabel="Maryland" />
																			<f:selectItem itemValue="MA"
																				itemLabel="Massachusetts" />
																			<f:selectItem itemValue="MI" itemLabel="Michigan" />
																			<f:selectItem itemValue="MN" itemLabel="Minnesota" />
																			<f:selectItem itemValue="MS" itemLabel="Mississippi" />
																			<f:selectItem itemValue="MO" itemLabel="Missouri" />
																			<f:selectItem itemValue="MT" itemLabel="Montana" />
																			<f:selectItem itemValue="NE" itemLabel="Nebraska" />
																			<f:selectItem itemValue="NV" itemLabel="Nevada" />
																			<f:selectItem itemValue="NH"
																				itemLabel="New Hampshire" />
																			<f:selectItem itemValue="NJ" itemLabel="New Jersey" />
																			<f:selectItem itemValue="NM" itemLabel="New Mexico" />
																			<f:selectItem itemValue="NY" itemLabel="New York" />
																			<f:selectItem itemValue="NC"
																				itemLabel="North Carolina" />
																			<f:selectItem itemValue="ND" itemLabel="North Dakota" />
																			<f:selectItem itemValue="OH" itemLabel="Ohio" />
																			<f:selectItem itemValue="OK" itemLabel="Oklahoma" />
																			<f:selectItem itemValue="OR" itemLabel="Oregon" />
																			<f:selectItem itemValue="PA" itemLabel="Pennsylvania" />
																			<f:selectItem itemValue="PR" itemLabel="Puerto Rico" />
																			<f:selectItem itemValue="RI" itemLabel="Rhode Island" />
																			<f:selectItem itemValue="SC"
																				itemLabel="South Carolina" />
																			<f:selectItem itemValue="SD" itemLabel="South Dakota" />
																			<f:selectItem itemValue="TN" itemLabel="Tennessee" />
																			<f:selectItem itemValue="TX" itemLabel="Texas" />
																			<f:selectItem itemValue="UT" itemLabel="Utah" />
																			<f:selectItem itemValue="VT" itemLabel="Vermont" />
																			<f:selectItem itemValue="VA" itemLabel="Virginia" />
																			<f:selectItem itemValue="WA" itemLabel="Washington" />
																			<f:selectItem itemValue="WV"
																				itemLabel="West Virginia" />
																			<f:selectItem itemValue="WI" itemLabel="Wisconsin" />
																			<f:selectItem itemValue="WY" itemLabel="Wyoming" />
																			<hx:inputHelperAssist
																				errorClass="selectOneMenu_Error"
																				onfocusSuccessClass="inputTextFocusOrderForm "
																				successClass="inputTextOrderForm " />
																		</h:selectOneMenu>
																		<h:inputText styleClass="inputTextOrderForm"
																			id="textDeliveryZip"
																			onchange="func_revalidateDelMethod(this, event);"
																			tabindex="28"
																			value="#{newSubscriptionOrderHandler.deliveryZipCode}"
																			maxlength="5" required="true" size="6"
																			requiredMessage="Zip is Required"
																			validatorMessage="Zip is Not Valid">
																			<f:validateLength minimum="5" maximum="5" />
																			<hx:validateConstraint regex="^[0-9]+$" />
																			<hx:inputHelperAssist errorClass="inputText_Error"
																				onfocusSuccessClass="inputTextFocusOrderForm "
																				successClass="inputTextOrderForm " validation="true" />
																		</h:inputText>
																	</hx:formItem>
																	<hx:formItem styleClass="formItem"
																		id="formItemDeliveryPhone"
																		label='#{labels.phoneLabel} <span style="color:red">*</span>'
																		errorText="#{labels.invalidPhoneFormat}">
																		<h:inputText styleClass="inputTextOrderForm"
																			id="textDeliveryPhoneAreaCode" size="4" tabindex="29"
																			maxlength="3"
																			value="#{newSubscriptionOrderHandler.deliveryPhoneAreaCode}"
																			required="true"
																			requiredMessage="Phone Number is Required"
																			validatorMessage="Phone Number is Not Valid">

																			<hx:inputHelperAssist errorClass="inputText_Error"
																				autoTab="true" id="assist4"
																				onfocusSuccessClass="inputTextFocusOrderForm"
																				successClass="inputTextOrderForm" />
																			<hx:validateConstraint regex="^[0-9]+$" />
																			<f:validateLength minimum="3" maximum="3" />
																		</h:inputText>
																		<h:inputText styleClass="inputTextOrderForm"
																			id="textDeliveryPhoneExchange" size="4" tabindex="30"
																			maxlength="3" readonly="false"
																			value="#{newSubscriptionOrderHandler.deliveryPhoneExchange}"
																			required="true"
																			requiredMessage="Phone Number is Required"
																			validatorMessage="Phone Number is Not Valid">

																			<hx:inputHelperAssist errorClass="inputText_Error"
																				autoTab="true" id="assist5"
																				onfocusSuccessClass="inputTextFocusOrderForm"
																				successClass="inputTextOrderForm" />
																			<hx:validateConstraint regex="^[0-9]+$" />
																			<f:validateLength minimum="3" maximum="3" />
																		</h:inputText>
																		<h:inputText styleClass="inputTextOrderForm"
																			id="textDeliveryPhoneExtension" size="5"
																			tabindex="31"
																			value="#{newSubscriptionOrderHandler.deliveryPhoneExtension}"
																			maxlength="4" required="true"
																			requiredMessage="Phone Number is Required"
																			validatorMessage="Phone Number is Not Valid">

																			<hx:validateConstraint regex="^[0-9]+$" />
																			<f:validateLength minimum="4" maximum="4" />
																			<hx:inputHelperAssist errorClass="inputText_Error"
																				onfocusSuccessClass="inputTextFocusOrderForm "
																				successClass="inputTextOrderForm " validation="true" />
																		</h:inputText>
																	</hx:formItem>
																	<hx:formItem styleClass="formItem"
																		id="formItemDeliveryWorkPhone"
																		errorText="#{labels.invalidPhoneFormat}"
																		label="#{labels.workPhoneLabel}">
																		<h:inputText styleClass="inputTextOrderForm"
																			id="textDeliveryWorkPhoneAreaCode" size="4"
																			tabindex="32" maxlength="3"
																			value="#{newSubscriptionOrderHandler.deliveryWorkPhoneAreaCode}"
																			validatorMessage="Work Phone Number is Not Valid">

																			<hx:inputHelperAssist errorClass="inputText_Error"
																				autoTab="true" id="assist44"
																				onfocusSuccessClass="inputTextFocusOrderForm"
																				successClass="inputTextOrderForm" />
																			<hx:validateConstraint regex="^[0-9]+$" />
																			<f:validateLength minimum="3" maximum="3" />
																		</h:inputText>
																		<h:inputText styleClass="inputTextOrderForm"
																			id="textDeliveryWorkPhoneExchange" size="4"
																			tabindex="33" maxlength="3" readonly="false"
																			value="#{newSubscriptionOrderHandler.deliveryWorkPhoneExchange}"
																			validatorMessage="Work Phone Number is Not Valid">

																			<hx:inputHelperAssist errorClass="inputText_Error"
																				autoTab="true" id="assist45"
																				onfocusSuccessClass="inputTextFocusOrderForm"
																				successClass="inputTextOrderForm" />
																			<hx:validateConstraint regex="^[0-9]+$" />
																			<f:validateLength minimum="3" maximum="3" />
																		</h:inputText>
																		<h:inputText styleClass="inputTextOrderForm"
																			id="textDeliveryWorkPhoneExtension" size="5"
																			tabindex="34"
																			value="#{newSubscriptionOrderHandler.deliveryWorkPhoneExtension}"
																			maxlength="4"
																			validatorMessage="Work Phone Number is Not Valid">

																			<hx:validateConstraint regex="^[0-9]+$" />
																			<f:validateLength minimum="4" maximum="4" />
																			<hx:inputHelperAssist errorClass="inputText_Error"
																				onfocusSuccessClass="inputTextFocusOrderForm "
																				successClass="inputTextOrderForm " validation="true" />
																		</h:inputText>
																	</hx:formItem>


																	<hx:formItem styleClass="formItem"
																		id="formItemEmailAddress"
																		label='#{labels.Email} <span style="color:red">*</span>'
																		errorText="#{labels.requiredFieldMsg}">
																		<h:inputText styleClass="inputTextOrderForm "
																			id="textEmailAddressRecipient" required="true"
																			tabindex="35" title="Delivery Email Address"
																			value="#{newSubscriptionOrderHandler.deliveryEmailAddress}"
																			maxlength="50"
																			requiredMessage="Delivery Email Address is Required"
																			validatorMessage="Delivery Email Address is Not Valid"
																			validator="#{pc_Checkout.handleTextEmailAddressRecipientValidate}">
																			<f:validateLength minimum="6" maximum="50" />
																			<hx:inputHelperAssist errorClass="inputText_Error"
																				onfocusSuccessClass="inputTextFocusOrderForm "
																				successClass="inputTextOrderForm " validation="true" />
																		</h:inputText>
																	</hx:formItem>
																	<hx:formItem styleClass="formItem"
																		id="formItemEmailAddressConfirm"
																		label='#{labels.confirmPurchaserEmailAddressLabel} <span style="color:red">*</span>'
																		errorText="#{labels.requiredFieldMsg}">

																		<h:inputText styleClass="inputTextOrderForm"
																			id="textEmailAddressConfirmRecipient" required="true"
																			tabindex="36" title="Confirm Delivery Email Address"
																			value="#{newSubscriptionOrderHandler.deliveryEmailAddressConfirmation}"
																			maxlength="50"
																			validatorMessage="Confirmation Email Address is Not Valid"
																			requiredMessage="Confirmation Email Address is Required"
																			validator="#{pc_Checkout.handleTextEmailAddressRecipientValidate}">
																			<f:validateLength minimum="6" maximum="50" />
																			<hx:inputHelperAssist errorClass="inputText_Error"
																				onfocusSuccessClass="inputTextFocusOrderForm "
																				successClass="inputTextOrderForm " validation="true" />
																		</h:inputText>
																	</hx:formItem>
																	<f:facet name="top">
																		<h:panelGrid styleClass="panelGrid"
																			id="gridDeliveryMethodInfoGrid"
																			rendered="#{currentOfferHandler.showDeliveryMethodCheck}"
																			width="90%" cellpadding="0" columns="1">
																			<hx:outputLinkEx value="javascript:;"
																				styleClass="outputLinkEx"
																				id="linkExDelMethodCheckHelpLink">
																				<h:outputText styleClass="outputLinkEx"
																					id="textDelMethodHelpText"
																					value="#{labels.DelMethodHelp}"></h:outputText>
																			</hx:outputLinkEx>
																		</h:panelGrid>
																	</f:facet>
																</hx:panelFormBox>
															</h:panelGrid>
															<h:panelGrid styleClass="panelGrid orderFormGridStyles"
																id="gridDetermineDeliveryOuterGrid" columns="1"
																columnClasses="panelGridColTopCentertAlign"
																rendered="#{currentOfferHandler.showDeliveryMethodCheck}">
																<h:panelGrid
																	styleClass="panelGrid orderFormInsideGridStyles"
																	id="gridDeliveryInformationPanelFooterGrid" columns="1"
																	cellpadding="1" cellspacing="1" width="100%">


																	<h:panelGrid styleClass="panelGrid"
																		columnClasses="panelGridColTopRightAlignV2, panelGridColTopLeftAlignV2"
																		id="gridDelMethodResultsGrid" columns="2"
																		cellpadding="2" cellspacing="2" width="98%">
																		<h:outputText styleClass="outputText"
																			id="textDeliveryMethod" value="#{labels.DelMethod}:">
																		</h:outputText>
																		<h:outputText styleClass="outputText"
																			id="textDeliveryMethodTextDeterminedValue"
																			value="#{newSubscriptionOrderHandler.deliveryMethodText}"></h:outputText>
																	</h:panelGrid>
																	<hx:commandExButton type="submit"
																		styleClass="commandExButton"
																		id="buttonGetDeliveryMethodButton" immediate="true"
																		onclick="return func_9(this, event);"
																		value="#{labels.DelMethodButtonLabel}" tabindex="37"
																		title="Determine Delivery Method"
																		action="#{pc_Checkout.doButtonGetDeliveryMethodButtonAction}"></hx:commandExButton>
																</h:panelGrid>
															</h:panelGrid>
															<h:panelGrid id="panelGridBillingDifferentThanDelGrid"
																columns="1" styleClass="orderFormGridStyles">
																<hx:panelFormBox helpPosition="over"
																	labelPosition="right" styleClass="panelFormBoxReversed"
																	id="formBoxBillDifferentFromDelSelectionFormBox">

																	<hx:formItem styleClass="formItem"
																		id="formItemBillDifferentFromDelSelector"
																		label="#{labels.billingDiffersFromDeliveryLabel}">
																		<h:selectBooleanCheckbox
																			styleClass="selectBooleanCheckbox"
																			id="checkboxIsBillDifferentFromDelSelector"
																			onclick="return func_2(this, event);"
																			title="Check if Billing Address is Different Than Delivery Address"
																			tabindex="41"
																			value="#{newSubscriptionOrderHandler.billingDifferentThanDelivery}">
																			<hx:behavior event="onselect" id="behavior1"></hx:behavior>
																		</h:selectBooleanCheckbox>
																	</hx:formItem>
																</hx:panelFormBox>
															</h:panelGrid>
															<h:panelGrid styleClass="panelGrid orderFormGridStyles"
																id="gridBillingAddress" columns="1">
																<hx:panelFormBox helpPosition="under"
																	labelPosition="left"
																	styleClass="panelFormBoxOrderEntryForm"
																	id="formBoxBillingAddress"
																	label="#{labels.BillingAddressLabel2}">
																	<hx:formItem styleClass="formItem"
																		id="formItemBillingFirstName"
																		label='#{labels.firstNameLabel} <span style="color:red">*</span>'>

																		<h:inputText styleClass="inputTextOrderForm"
																			id="textBillingFirstName" title="First Name"
																			tabindex="52"
																			value="#{newSubscriptionOrderHandler.billingFirstName}"
																			maxlength="10">
																			<f:validateLength minimum="1" maximum="15" />
																		</h:inputText>
																	</hx:formItem>
																	<hx:formItem styleClass="formItem"
																		id="formItemBillingLastName"
																		label='#{labels.lastNameLabel} <span style="color:red">*</span>'>

																		<h:inputText styleClass="inputTextOrderForm"
																			id="textBillingLastName" tabindex="53"
																			title="Last Name"
																			value="#{newSubscriptionOrderHandler.billingLastName}"
																			maxlength="15">
																			<f:validateLength minimum="1" maximum="20" />
																		</h:inputText>
																	</hx:formItem>

																	<hx:formItem styleClass="formItem"
																		id="formItemBillingCompanyName"
																		label="#{labels.companyNameLabel}">
																		<h:inputText styleClass="inputTextOrderForm"
																			id="textBillingCompanyName" tabindex="54"
																			value="#{newSubscriptionOrderHandler.billingCompanyName}"
																			maxlength="28">
																			<f:validateLength maximum="28" />
																		</h:inputText>
																	</hx:formItem>

																	<hx:formItem styleClass="formItem"
																		id="formItemBillingAddress1"
																		label='#{labels.address} <span style="color:red">*</span>'>
																		<h:inputText styleClass="inputTextOrderForm"
																			id="textBillingAddress1" tabindex="55"
																			title="Address 1"
																			value="#{newSubscriptionOrderHandler.billingAddress1}"
																			maxlength="28">
																			<f:validateLength minimum="1" maximum="28" />
																		</h:inputText>
																		<h:inputText styleClass="inputTextOrderForm"
																			id="textBillingAptSuite" size="8" title="Apt/Suite"
																			tabindex="56"
																			value="#{newSubscriptionOrderHandler.billingAptSuite}"
																			maxlength="28">
																			<f:validateLength minimum="1" maximum="28" />
																		</h:inputText>
																	</hx:formItem>
																	<hx:formItem styleClass="formItem"
																		id="formItemBillingAddress2"
																		label="#{labels.address2Label}" rendered="false">
																		<h:inputText styleClass="inputTextOrderForm"
																			id="textBillingAddress2" tabindex="57"
																			value="#{newSubscriptionOrderHandler.billingAddress2}"
																			maxlength="28">
																			<f:validateLength minimum="1" maximum="28" />
																		</h:inputText>
																	</hx:formItem>

																	<hx:formItem styleClass="formItem"
																		id="formItemBillingCity"
																		label='#{labels.cityLabel} <span style="color:red">*</span>'>
																		<h:inputText styleClass="inputTextOrderForm"
																			id="textBillingCity" tabindex="58" title="City"
																			value="#{newSubscriptionOrderHandler.billingCity}"
																			maxlength="28">
																			<f:validateLength minimum="1" maximum="28" />
																		</h:inputText>
																	</hx:formItem>
																	<hx:formItem styleClass="formItem"
																		id="formItemBillingState"
																		label='#{labels.StateZipLabel} <span style="color:red">*</span>'>
																		<h:selectOneMenu
																			styleClass="selectOneMenu selectOneMenu2"
																			id="menuBillingState" tabindex="58" title="State"
																			value="#{newSubscriptionOrderHandler.billingState}">
																			<f:selectItem itemValue="" itemLabel="" />
																			<f:selectItem itemValue="AL" itemLabel="Alabama" />
																			<f:selectItem itemValue="AK" itemLabel="Alaska" />
																			<f:selectItem itemValue="AZ" itemLabel="Arizona" />
																			<f:selectItem itemValue="AR" itemLabel="Arkansas" />
																			<f:selectItem itemValue="AA"
																				itemLabel="(AA) Armed Forces Americas" />
																			<f:selectItem itemValue="AE"
																				itemLabel="(AE) Armed Forces Africa" />
																			<f:selectItem itemValue="AE"
																				itemLabel="(AE) Armed Forces Canada" />
																			<f:selectItem itemValue="AE"
																				itemLabel="(AE) Armed Forces Europe" />
																			<f:selectItem itemValue="AE"
																				itemLabel="(AE) Armed Forces Middle East" />
																			<f:selectItem itemValue="AP"
																				itemLabel="(AP) Armed Forces Pacific" />
																			<f:selectItem itemValue="CA" itemLabel="California" />
																			<f:selectItem itemValue="CO" itemLabel="Colorado" />
																			<f:selectItem itemValue="CT" itemLabel="Connecticut" />
																			<f:selectItem itemValue="DE" itemLabel="Delaware" />
																			<f:selectItem itemValue="DC"
																				itemLabel="District of Columbia" />
																			<f:selectItem itemValue="FL" itemLabel="Florida" />
																			<f:selectItem itemValue="GA" itemLabel="Georgia" />
																			<f:selectItem itemValue="HI" itemLabel="Hawaii" />
																			<f:selectItem itemValue="ID" itemLabel="Idaho" />
																			<f:selectItem itemValue="IL" itemLabel="Illinois" />
																			<f:selectItem itemValue="IN" itemLabel="Indiana" />
																			<f:selectItem itemValue="IA" itemLabel="Iowa" />
																			<f:selectItem itemValue="KS" itemLabel="Kansas" />
																			<f:selectItem itemValue="KY" itemLabel="Kentucky" />
																			<f:selectItem itemValue="LA" itemLabel="Louisiana" />
																			<f:selectItem itemValue="ME" itemLabel="Maine" />
																			<f:selectItem itemValue="MD" itemLabel="Maryland" />
																			<f:selectItem itemValue="MA"
																				itemLabel="Massachusetts" />
																			<f:selectItem itemValue="MI" itemLabel="Michigan" />
																			<f:selectItem itemValue="MN" itemLabel="Minnesota" />
																			<f:selectItem itemValue="MS" itemLabel="Mississippi" />
																			<f:selectItem itemValue="MO" itemLabel="Missouri" />
																			<f:selectItem itemValue="MT" itemLabel="Montana" />
																			<f:selectItem itemValue="NE" itemLabel="Nebraska" />
																			<f:selectItem itemValue="NV" itemLabel="Nevada" />
																			<f:selectItem itemValue="NH"
																				itemLabel="New Hampshire" />
																			<f:selectItem itemValue="NJ" itemLabel="New Jersey" />
																			<f:selectItem itemValue="NM" itemLabel="New Mexico" />
																			<f:selectItem itemValue="NY" itemLabel="New York" />
																			<f:selectItem itemValue="NC"
																				itemLabel="North Carolina" />
																			<f:selectItem itemValue="ND" itemLabel="North Dakota" />
																			<f:selectItem itemValue="OH" itemLabel="Ohio" />
																			<f:selectItem itemValue="OK" itemLabel="Oklahoma" />
																			<f:selectItem itemValue="OR" itemLabel="Oregon" />
																			<f:selectItem itemValue="PA" itemLabel="Pennsylvania" />
																			<f:selectItem itemValue="PR" itemLabel="Puerto Rico" />
																			<f:selectItem itemValue="RI" itemLabel="Rhode Island" />
																			<f:selectItem itemValue="SC"
																				itemLabel="South Carolina" />
																			<f:selectItem itemValue="SD" itemLabel="South Dakota" />
																			<f:selectItem itemValue="TN" itemLabel="Tennessee" />
																			<f:selectItem itemValue="TX" itemLabel="Texas" />
																			<f:selectItem itemValue="UT" itemLabel="Utah" />
																			<f:selectItem itemValue="VT" itemLabel="Vermont" />
																			<f:selectItem itemValue="VA" itemLabel="Virginia" />
																			<f:selectItem itemValue="WA" itemLabel="Washington" />
																			<f:selectItem itemValue="WV"
																				itemLabel="West Virginia" />
																			<f:selectItem itemValue="WI" itemLabel="Wisconsin" />
																			<f:selectItem itemValue="WY" itemLabel="Wyoming" />
																		</h:selectOneMenu>
																		<h:inputText styleClass="inputTextOrderForm"
																			id="textBillingZipCode" size="6" title="Zip Code"
																			tabindex="59"
																			value="#{newSubscriptionOrderHandler.billingZipCode}"
																			maxlength="5">
																			<f:validateLength minimum="5" maximum="5" />
																			<hx:validateConstraint regex="^[0-9]+$" />
																		</h:inputText>
																	</hx:formItem>
																	<hx:formItem styleClass="formItem"
																		id="formItemBillingTelephone"
																		label="#{labels.billPhoneLabel}">
																		<h:inputText styleClass="inputTextOrderForm"
																			id="textBillingPhoneAreaCode" size="4" tabindex="60"
																			maxlength="3" title="Billing Phone Area Code"
																			value="#{newSubscriptionOrderHandler.billingPhoneAreaCode}">

																			<hx:inputHelperAssist errorClass="inputText_Error"
																				autoTab="true" id="assist2" />
																			<hx:validateConstraint regex="^[0-9]+$" />
																			<f:validateLength minimum="3" maximum="3" />
																		</h:inputText>
																		<h:inputText styleClass="inputTextOrderForm"
																			id="textBillingPhoneExchange" size="4" tabindex="61"
																			maxlength="3" title="Billing Phone Exchange"
																			value="#{newSubscriptionOrderHandler.billingPhoneExchange}">

																			<hx:inputHelperAssist errorClass="inputText_Error"
																				autoTab="true" id="assist3" />
																			<hx:validateConstraint regex="^[0-9]+$" />
																			<f:validateLength minimum="3" maximum="3" />
																		</h:inputText>
																		<h:inputText styleClass="inputTextOrderForm"
																			id="textBillingPhoneExtension" size="5" tabindex="62"
																			title="Billing Phone Last 4 Digits"
																			value="#{newSubscriptionOrderHandler.billingPhoneExtension}"
																			maxlength="4">

																			<hx:validateConstraint regex="^[0-9]+$" />
																			<f:validateLength minimum="4" maximum="4" />
																		</h:inputText>
																	</hx:formItem>
																	<hx:formItem styleClass="formItem"
																		id="giftSubscriptionFormItem"
																		label="#{labels.isGiftSubscriptionLabel}">
																		<h:selectBooleanCheckbox
																			styleClass="selectBooleanCheckbox"
																			id="giftSubscriptionCheckbox"
																			value="#{newSubscriptionOrderHandler.giftSubscription}"
																			onclick="return func_2(this, event);"
																			title="Check if this is a gift" tabindex="63"></h:selectBooleanCheckbox>
																	</hx:formItem>
																	<hx:formItem styleClass="formItem"
																		id="formItemPayersEmailAddress"
																		label='#{labels.emailAddressPayerLabel} <span style="color:red">*</span>'
																		infoText="#{labels.BillingEmailInfoMsg}">

																		<h:inputText styleClass="inputTextOrderForm"
																			id="textPurchaserEmailAddress" tabindex="64"
																			value="#{newSubscriptionOrderHandler.billingEmailAddress}"
																			maxlength="60" required="false"
																			validator="#{pc_Checkout.handleTextEmailAddressRecipientValidate}">
																			<f:validateLength minimum="1" maximum="64" />
																		</h:inputText>
																	</hx:formItem>
																</hx:panelFormBox>
															</h:panelGrid>
															<h:panelGrid styleClass="panelGrid orderFormGridStyles"
																id="gridPaymentInformationGrid" columns="1">
																<f:facet name="footer">

																</f:facet>
																<hx:panelFormBox helpPosition="right"
																	labelPosition="left"
																	styleClass="panelFormBoxOrderEntryForm"
																	id="formBoxPaymentInfo"
																	rendered="#{currentOfferHandler.isShowCreditCardPaymentOptionData}"
																	label="#{labels.paymentInfo3}">
																	<hx:formItem styleClass="formItem"
																		id="formItemCreditCardNumber"
																		label='#{labels.CCNum} <span style="color:red">*</span>'>
																		<h:inputText styleClass="inputTextOrderForm"
																			id="textCreditCardNumber" tabindex="70"
																			title="Credit Card Number"
																			value="#{newSubscriptionOrderHandler.creditCardNumber}"
																			maxlength="16"
																			validatorMessage="Credit Card Number is Not Valid">
																			<hx:validateConstraint regex="^[0-9]+$" />
																			<f:validateLength minimum="12" maximum="16" />
																			<hx:inputHelperAssist errorClass="inputText_Error"
																				validation="true" id="assist6" />
																		</h:inputText>
																	</hx:formItem>

																	<hx:formItem styleClass="formItem"
																		id="formItemCreditCardCVV"
																		label='#{labels.CCVerify} <span style="color:red">*</span>'>
																		<h:inputText styleClass="inputTextOrderForm"
																			id="textCVV" size="5" tabindex="71"
																			title="Credit Card Verification Number"
																			value="#{newSubscriptionOrderHandler.creditCardCVVNumber}"
																			validatorMessage="Credit Card Verification Number is Not Valid"
																			maxlength="4">
																			<f:validateLength minimum="3" maximum="4" />
																		</h:inputText>
										&nbsp;&nbsp;&nbsp;
										<h:outputText styleClass="requestLink" id="textCVVLearnMore"
																			value="#{currentOfferHandler.addressLearnMoreHTML}"
																			escape="false"></h:outputText>

																	</hx:formItem>
																	<hx:formItem styleClass="formItem"
																		id="formItemCCExpirationDate"
																		label='#{labels.CCExpiration} <span style="color:red">*</span>'>
																		<h:selectOneMenu
																			styleClass="selectOneMenu selectOneMenu2"
																			id="menuCCExpireMonth" tabindex="72"
																			title="Credit Card Expiration Month"
																			value="#{newSubscriptionOrderHandler.creditCardExpirationMonth}">
																			<f:selectItem itemLabel="Month" itemValue="-1"
																				id="selectItem16" />
																			<f:selectItem itemLabel="01" itemValue="01"
																				id="selectItem3" />
																			<f:selectItem itemLabel="02" itemValue="02"
																				id="selectItem4" />
																			<f:selectItem itemLabel="03" itemValue="03"
																				id="selectItem5" />
																			<f:selectItem itemLabel="04" itemValue="04"
																				id="selectItem6" />
																			<f:selectItem itemLabel="05" itemValue="05"
																				id="selectItem7" />
																			<f:selectItem itemLabel="06" itemValue="06"
																				id="selectItem8" />
																			<f:selectItem itemLabel="07" itemValue="07"
																				id="selectItem9" />
																			<f:selectItem itemLabel="08" itemValue="08"
																				id="selectItem10" />
																			<f:selectItem itemLabel="09" itemValue="09"
																				id="selectItem11" />
																			<f:selectItem itemLabel="10" itemValue="10"
																				id="selectItem12" />
																			<f:selectItem itemLabel="11" itemValue="11"
																				id="selectItem13" />
																			<f:selectItem itemLabel="12" itemValue="12"
																				id="selectItem14" />
																		</h:selectOneMenu>
																		<h:selectOneMenu
																			styleClass="selectOneMenu selectOneMenu2"
																			id="menuCCExpireYear" tabindex="73"
																			title="Credit Card Expiration Year"
																			value="#{newSubscriptionOrderHandler.creditCardExpirationYear}">
																			<f:selectItems
																				value="#{currentOfferHandler.creditCardExpirationYears}"
																				id="selectItems1" />
																		</h:selectOneMenu>
																	</hx:formItem>



																	<f:facet name="top">
																		<h:panelGrid id="panelGridCreditCardImageGrid"
																			width="85%" columns="1" cellspacing="1"
																			cellpadding="1">

																			<hx:jspPanel id="jspPanelCreditCardImages">
																				<hx:graphicImageEx styleClass="graphicImageEx"
																					id="imageExAmEx1"
																					value="/images/american_express_logo.gif"
																					hspace="5" border="0" width="42" height="26"
																					rendered="#{currentOfferHandler.isTakesAMEX}">
																				</hx:graphicImageEx>
																				<hx:graphicImageEx styleClass="graphicImageEx"
																					id="imageExDiscover1"
																					value="/images/discover_logo.gif" hspace="5"
																					border="0" width="42" height="26"
																					rendered="#{currentOfferHandler.isTakesDiscover}"></hx:graphicImageEx>
																				<hx:graphicImageEx styleClass="graphicImageEx"
																					id="imageExMasterCard1" value="/images/mc_logo.gif"
																					hspace="5" border="0" width="42" height="26"
																					rendered="#{currentOfferHandler.isTakesMasterCard}">
																				</hx:graphicImageEx>
																				<hx:graphicImageEx styleClass="graphicImageEx"
																					id="imageExVisaLogo1" value="/images/visa_logo.gif"
																					hspace="5" border="0" width="42" height="26"
																					rendered="#{currentOfferHandler.isTakesVisa}">
																				</hx:graphicImageEx>

																			</hx:jspPanel>
																		</h:panelGrid>
																	</f:facet>
																</hx:panelFormBox>
																<h:panelGrid styleClass="panelGrid orderFormGridStyles"
																	id="gridEZPAYOptionsGrid" columns="1">
																	<h:selectOneRadio
																		disabledClass="selectOneRadio_Disabled"
																		enabledClass="selectOneRadio_Enabled"
																		styleClass="selectOneRadio" id="radioRenewalOptions"
																		layout="pageDirection"
																		value="#{newSubscriptionOrderHandler.renewalMethod}"
																		tabindex="80"
																		rendered="#{currentOfferHandler.isNonEZPayRate}">
																		<f:selectItem escape="false"
																			itemLabel="Please sign me up for EZ-PAY. #{currentOfferHandler.EZPayFreeWeeksOfferText} <sup>*</sup>"
																			itemValue="AUTOPAY_PLAN" id="selectItem17" />
																		<f:selectItem
																			itemLabel="Please charge my credit card for this subscription term only."
																			itemValue="ONE_TIME_BILL" id="selectItem18" />
																	</h:selectOneRadio>
																</h:panelGrid>
																<h:panelGrid styleClass="panelGrid orderFormGridStyles"
																	id="gridEZPAYOptionsGridGift" columns="1"
																	style="display:none">
																	<h:selectOneRadio
																		disabledClass="selectOneRadio_Disabled"
																		enabledClass="selectOneRadio_Enabled"
																		styleClass="selectOneRadio"
																		id="radioRenewalOptionsGift" layout="pageDirection"
																		tabindex="81"
																		value="#{newSubscriptionOrderHandler.renewalMethodGift}"
																		rendered="#{currentOfferHandler.isNonEZPayRate}">
																		<f:selectItem escape="false"
																			itemLabel="Please sign me up for EZ-PAY. #{currentOfferHandler.EZPayFreeWeeksOfferText} <sup>2</sup>"
																			itemValue="AUTOPAY_PLAN" id="selectItem19" />
																		<f:selectItem
																			itemLabel="Please charge my credit card for this subscription term only."
																			itemValue="ONE_TIME_BILL" id="selectItem20" />
																		<f:selectItem itemValue="ONE_TIME_BILL_GIFT"
																			id="selectItem15"
																			itemLabel="Please charge my credit card for this subscription term only and send future invoices to the gift recipient." />
																	</h:selectOneRadio>
																</h:panelGrid>
																<h:panelGrid styleClass="panelGrid orderFormGridStyles"
																	id="gridEZPAYRequiredGrid" columns="1"
																	style="display:none;"
																	rendered="#{currentOfferHandler.ezPayFreeWeeks}">
																	<h:outputFormat styleClass="outputText coppaText"
																		id="requiresEZPAYText"
																		value="#{labels.requireEZPayRenwalText} "
																		escape="false">
																		<f:param
																			value="#{currentOfferHandler.EZPayFreeWeeksOffer}"
																			id="param5"></f:param>
																	</h:outputFormat>
																</h:panelGrid>
															</h:panelGrid>
															<h:panelGrid
																styleClass="panelGrid orderFormInsideGridStyles"
																frame="box" id="gridOfferDisclaimerGrid" columns="1"
																rendered="#{currentOfferHandler.isShowOfferDisclaimerText}">
																<h:outputText styleClass="outputText"
																	id="textOfferDisclaimerText" escape="false"
																	value="#{currentOfferHandler.onePageDisclaimerText}"></h:outputText>
															</h:panelGrid>
															<h:panelGrid id="COPPAGrid" cellpadding="4" columns="1"
																styleClass="orderFormGridStyles">
																<h:outputText styleClass=" outputText coppaText"
																	id="textCOPPAText" value="#{labels.OrderEntryCOPPA}"></h:outputText>
															</h:panelGrid>
															<h:panelGrid id="panelGridFormSubmissionGrid" columns="2"
																cellpadding="4" styleClass="orderFormGridStyles">


																<hx:commandExButton type="submit" value="Subscribe Now"
																	styleClass="orderpageSubmitbutton"
																	id="buttonPlaceOrder" tabindex="90"
																	onclick="return func_10(this, event);"
																	alt="Click Here to Complete Your Order"
																	action="#{pc_Checkout1.doButtonPlaceOrderAction}"></hx:commandExButton>

																<hx:jspPanel id="jspPanelGeoTrustPanel">
																	<div id="chatIconRequestDiv">
																		<img
																			onclick="javascript: checkoutGuarantee(this, event);"
																			alt="Satisfaction Guaranteed"
																			title="Click for details!"
																			src="/images/marketingimages/ckoutClickGuarantee.jpg"
																			hspace="0" vspace="5">
																	</div>
																	<!-- GeoTrust True Site[tm] Smart Icon tag. Do not edit. -->
																	<!--	<script LANGUAGE="JavaScript" TYPE="text/javascript" SRC="https://smarticon.geotrust.com/si.js"></script>  -->
																	<!-- end GeoTrust Smart Icon tag -->
																</hx:jspPanel>


															</h:panelGrid>
															<hx:jspPanel id="pleaseWaitPanel">
																<div id="formSubmittedOverlayInfo"
																	style="display: none; position: relative; z-index: 100000; border-bottom: 0px solid #90a0b8; border-top: 0px solid #90a0b8; border-left: 0px solid #90a0b8; border-right: 0px solid #90a0b8; background: white; background-color: white;">
																	<table bgcolor="white">
																		<tr>
																			<td align="center" valign="middle" nowrap><h2>Please
																					wait while we process your order</h2></td>
																			<td align="left"><img id="formSubmissionImage"
																				src="/images/squaresAnimated.gif"></td>
																		</tr>
																	</table>
																</div>
															</hx:jspPanel>
															<h:panelGrid styleClass="panelGrid orderFormGridStyles"
																id="gridRequiredEZPayFinePrintGrid" columns="1">
																<h:panelGrid styleClass="panelGrid"
																	columnClasses="panelGridColLeftAlign"
																	id="gridEZPayFinePrintGrid" width="100%" columns="1">
																	<h:outputText
																		styleClass="outputFormat orderFormInsideGridStyles  footnote"
																		id="formatEZPAYFinePrint"
																		rendered="#{not currentOfferHandler.isForceBillMe }"
																		value="#{labels.requiredLabel} #{currentOfferHandler.EZPayDisclaimer}">
																	</h:outputText>

																	<%-- 																	<h:outputFormat style="display: none" --%>
																	<!-- 																		styleClass="outputFormat orderFormInsideGridStyles  footnote" -->
																	<!-- 																		id="formatEZPAYFinePrint" -->
																	<%-- 																		value="#{labels.EzpayNonFreeFinePrintFormat}" --%>
																	<%-- 																		rendered="#{not currentOfferHandler.isForceBillMe }" --%>
																	<!-- 																		escape="false"> -->
																	<%-- 																		<f:param value="#{currentOfferHandler.EZPayFreeWeeksOffer}" --%>
																	<%-- 																			id="param4"></f:param> --%>
																	<%-- 																		<f:param --%>
																	<%-- 																			value="#{currentOfferHandler.currentOffer.product.customerServicePhone}" --%>
																	<%-- 																			id="param6"></f:param> --%>
																	<%-- 																	</h:outputFormat> --%>
																</h:panelGrid>
															</h:panelGrid>

														</h:panelGroup>
													</f:facet>
													<f:facet name="left">

													</f:facet>
													<f:facet name="right">

													</f:facet>
													<f:facet name="bottom">
														<hx:jspPanel id="jspPanel1">
															<div id="graybar">
																Copyright
																<script>
																	document
																			.write(getCurrentYear());
																</script>
																USA TODAY, a division of <a
																	href="http://www.gannett.com">Gannett Co. Inc.</a> <a
																	href="/privacy/privacy.htm">Privacy
																	Notice/California Privacy Notice</a>. <a
																	href="/privacy/privacy.htm#adchoice">Ad Choices</a>. By
																using this service, you accept our <a
																	href="/service/service.jsp">Terms of Service</a>.
															</div>
															<div id="copyright">
																<p>Offers available to new subscribers only. Home
																	delivery not available in some areas. Not valid with
																	any other offer. Prepayment required. After the
																	promotional period ends, the subscription will continue
																	and be billed monthly at the then regular rate, less
																	any applicable credits, unless USA TODAY is otherwise
																	notified. Applicable taxes may not be included in the
																	rate. If at any time you decide to cancel your
																	subscription, you may contact customer service at
																	1-800-872-0001 and the full amount of any balance over
																	$2.00 will be returned.</p>
															</div>
															<!--END OF COPYWRITE-->

														</hx:jspPanel>
													</f:facet>
													<f:facet name="top">
														<h:messages styleClass="messages"
															id="messagesAllMesssages"></h:messages>
													</f:facet>
												</hx:panelLayout>


												<!-- end orderFormDiv -->
											</div>


										</div>
										<div id="pageContentEEOrderEntryRight">
											<div class="form_RR_mod">
												<h6 class="form_RR">Contact Us</h6>
												<div class="light-shade alpha40">
													<h3 class="form_RR">Need help with your order?</h3>
													<!-- 													<div class="chat-button" -->
													<!-- 														onclick="javascript:window.open('https://livechat5.parachat.com/chat/chatstart.htm?domain=www.service.usatoday.com','wochat','width=484,height=361');return false;"> -->
													<!-- 														<a href="#"><span style="color: white">CLICK TO -->
													<!-- 																CHAT</span> </a> -->
													<!-- 													</div> -->
													<!-- 													<div class="chat-button" -->
													<!-- 														onclick="javascript:window.open('https://livechat5.parachat.com/chat/callback.htm?domain=www.service.usatoday.com','wocallback','width=484,height=361');return false;"> -->
													<!-- 														<a href="#"><span style="color: white">REQUEST -->
													<!-- 																A CALL BACK</span> </a> -->
													<!-- 													</div> -->

													<p>
														<span class="RR-bold">Call 1-877-713-6241</span><br>
														M-F 8am-7pm ET
													</p>

													<p class="follow-wrapper"></p>
													<p>
														To manage your account online - including reporting a
														problem with delivery or placing a temporary hold on your
														account - log in by entering your e-mail address and
														password. <a href="/login/auth.faces">Click here</a> to
														access the login page now. Forgot your password or login?
														<a href="/idpassword/forgotpassword.jsp">click here</a>.
														For all other inquiries, call 1-800-872-0001.
													</p>

												</div>
											</div>
											<div class="form_RR_mod">
												<h6 class="form_RR">USA TODAY Products</h6>
												<div class="light-shade-products alpha40">
													<div id="form-RR-products">


														<div class="form-RR-products-1">
															<a href="/HomeSubscribeV2"> <img
																src="/images/dc_resources/USAtoday-newspaper-print.png">
																USA TODAY Newspaper<br> </a> <a href="/HomeSubscribeV2">
																<span class="form-RR-buy-button">Buy Now</span> </a>
														</div>

														<p class="follow-wrapper"></p>

														<div class="form-RR-products-2">
															<a href="/HomeSubscribeEEV2"> <img
																src="/images/dc_resources/USAtODAY_E-news.png">
																USA TODAY e-Newspaper<br> </a> <a
																href="http://ee.usatoday.com/sample"> view sample</a> <a
																href="/HomeSubscribeEEV2"> <span
																class="form-RR-buy-button">Buy Now</span> </a>
														</div>


														<p class="follow-wrapper"></p>

														<div class="form-RR-products-1">
															<a href="/HomeSubscribeSWV2"> <img
																src="/images/dc_resources/USAtODAY_Sports.png">
																USA TODAY Sports Weekly <br> <span
																class="form-RR-buy-button">Buy Now</span> </a>
														</div>

														<p class="follow-wrapper"></p>

														<div class="form-RR-products-2">
															<a href="http://reg.e.usatoday.com"> <img
																src="/images/dc_resources/USAtODAY_E-newsletter.png">
																E-mail Newsletters & Alerts<br> </a> <a
																href="http://reg.e.usatoday.com"> <span
																class="form-RR-buy-button">Sign Up</span> </a>
														</div>

													</div>

												</div>
											</div>

											<h:panelGrid styleClass="panelGrid" id="gridGeoTrust">

												<!-- Place Secure Site Seal in this column -->
												<!-- GeoTrust True Site[tm] Smart Icon tag. Do not edit. -->
												<script LANGUAGE="JavaScript" TYPE="text/javascript"
													SRC="https://smarticon.geotrust.com/si.js"></script>
												<!-- end GeoTrust Smart Icon tag -->

											</h:panelGrid>
										</div>


										<!-- Force EZ-PAY  Learn More -->
										<hx:panelDialog type="modeless" styleClass="panelDialog"
											id="dialogEZPayLearnMore1" for="textEZPayLearnMore"
											relativeTo="textEZPayLearnMore" title="Learn More"
											align="relative" valign="relative">
											<h:panelGrid styleClass="panelGrid"
												id="gridEZPayLearnMoreGrid2" width="300">
												<h:outputFormat styleClass="outputText"
													id="formatEZPayHelpText"
													value="#{labels.EZPayNonFreePlanTextUnderTermsMultiPubFAQ1}"
													escape="false">
													<%-- 													<f:param value="#{currentOfferHandler.EZPayFreeWeeksOffer}" --%>
													<%-- 														id="param7"></f:param> --%>
													<f:param
														value="#{currentOfferHandler.currentOffer.product.customerServicePhone}"
														id="param8"></f:param>
												</h:outputFormat>
											</h:panelGrid>
											<h:panelGroup id="group1" styleClass="panelDialog_Footer">

												<hx:commandExButton id="button2"
													styleClass="commandExButton" type="submit" value="Close">
													<hx:behavior event="onclick" behaviorAction="hide;stop"
														targetAction="dialogEZPayLearnMore1" id="behaviorEZPay4"></hx:behavior>
												</hx:commandExButton>

											</h:panelGroup>
										</hx:panelDialog>
										<!-- StartDate  Learn More -->
										<hx:panelDialog type="modeless" styleClass="panelDialog"
											id="dialogStartDateLearnMore1" for="textStartDateLearnMore"
											relativeTo="textStartDateLearnMore" title="Learn More"
											align="relative" valign="relative">
											<h:panelGrid styleClass="panelGrid"
												id="gridStartDateLearnMoreGrid2" width="300">
												<h:outputFormat styleClass="outputFormat"
													id="formatGiftFutureStartDateText"
													value="#{labels.startDateInfo}" escape="false">
													<f:param
														value="#{currentOfferHandler.earliestPossibleStartDateString}"
														id="param1"></f:param>
													<f:param
														value="#{currentOfferHandler.latestPossibleStartDateString}"
														id="param2"></f:param>
													<f:param value="#{currentOfferHandler.productName}"
														id="param3"></f:param>
												</h:outputFormat>
											</h:panelGrid>
											<h:panelGroup id="groupStartDate1"
												styleClass="panelDialog_Footer">

												<hx:commandExButton id="buttonStartDate2"
													styleClass="commandExButton" type="submit" value="Close">
													<hx:behavior event="onclick" behaviorAction="hide;stop"
														targetAction="dialogStartDateLearnMore1"
														id="behaviorStartDate4"></hx:behavior>
												</hx:commandExButton>

											</h:panelGroup>
										</hx:panelDialog>
										<!-- Num Qty Learn More -->
										<hx:panelDialog type="modeless" styleClass="panelDialog"
											id="dialogQty" relativeTo="textNumCopiesLearnMoreLink"
											for="textNumCopiesLearnMoreLink" title="Learn More"
											showTitleCloseButton="true" align="relative"
											valign="relative">
											<h:panelGrid styleClass="panelGrid" id="grid2" width="250">
												<h:outputText styleClass="outputText" id="text3"
													value="#{labels.qtyLearnMore}" escape="false"></h:outputText>
											</h:panelGrid>


											<h:panelGroup id="group2" styleClass="panelDialog_Footer">

												<hx:commandExButton id="button5"
													styleClass="commandExButton" type="submit" value="Close">
													<hx:behavior event="onclick" behaviorAction="hide;stop"
														id="behavior5" targetAction="dialogQty"></hx:behavior>
												</hx:commandExButton>
											</h:panelGroup>

										</hx:panelDialog>

										<hx:panelDialog type="modeless" styleClass="panelDialog"
											id="dialogClubNum" title="Learn More"
											for="textProgInfoLearnMoreLink" align="relative"
											valign="relative" relativeTo="textProgInfoLearnMoreLink">
											<h:panelGrid styleClass="panelGrid" id="gridClubLearnMore"
												width="230">
												<h:outputText styleClass="outputText" id="textClubLearnMore"
													value="#{labels.learnMoreClubNum}" escape="false"></h:outputText>
											</h:panelGrid>

											<h:panelGroup id="group3" styleClass="panelDialog_Footer">


												<hx:commandExButton id="button1"
													styleClass="commandExButton" type="submit" value="Close">
													<hx:behavior event="onclick" behaviorAction="hide;stop"
														id="behavior2" targetAction="dialogClubNum"></hx:behavior>
												</hx:commandExButton>
											</h:panelGroup>




										</hx:panelDialog>

										<hx:panelDialog type="modeless" styleClass="panelDialog"
											id="dialogCVV" align="relative" valign="relative"
											for="textCVVLearnMore" relativeTo="textCVVLearnMore"
											title="Learn More">

											<h:panelGrid styleClass="panelGrid" id="gridCVVLearn"
												width="230">
												<f:facet name="footer">
												</f:facet>
												<f:facet name="header">
													<h:panelGroup styleClass="panelGroup" id="group6">
														<h:outputText styleClass="outputText" id="text4"
															value="#{labels.learnMoreCVV}" escape="false"></h:outputText>
													</h:panelGroup>
												</f:facet>

											</h:panelGrid>
											<h:panelGroup id="group5" styleClass="panelDialog_Footer">
												<hx:commandExButton id="button4"
													styleClass="commandExButton" type="submit" value="Close">
													<hx:behavior event="onclick" behaviorAction="hide;stop"
														id="behavior4" targetAction="dialogCVV"></hx:behavior>
												</hx:commandExButton>
											</h:panelGroup>
										</hx:panelDialog>
										<!-- Delivery Method Panel -->

										<hx:panelDialog type="modeless" styleClass="panelDialog"
											id="dialogDelMethodDialog" for="linkExDelMethodCheckHelpLink"
											relativeTo="textDelMethodHelpText" initiallyShow="false"
											title="How will my paper be delivered?    ">
											<h:panelGroup id="groupDelMethod1"
												styleClass="panelDialog_Footer">
												<h:panelGrid styleClass="panelGrid" id="gridDelMethod2"
													width="270">
													<h:outputText styleClass="outputText"
														id="textDelMethodDetailHelp"
														value="#{labels.DelMethodHelpDetail}" escape="false"></h:outputText>
												</h:panelGrid>
												<hx:commandExButton id="buttonDelMethod"
													styleClass="commandExButton" type="submit" value="Close">
													<hx:behavior event="onclick" behaviorAction="hide;stop"
														targetAction="dialogDelMethodDialog"
														id="behaviorDelMethod4"></hx:behavior>
												</hx:commandExButton>
											</h:panelGroup>
										</hx:panelDialog>
										<!-- Guarantee Window -->
										<div id="checkoutGuaranteeDiv"
											style="display: none; position: relative;">
											<div id="templateChatTopDiv">
												<span onclick="checkoutGuaranteeHide();"
													style="cursor: pointer;">Close Window<img
													src="/theme/themeV3/themeImages/close_14x14.png" width="14"
													height="14" border="0"
													style="margin-left: 5px; margin-right: 5px"> </span>
											</div>
											<div id="tempateChatImageDiv">
												<img title="Satisfaction Guaranteed!"
													src="/images/marketingimages/ckoutGuarantee.jpg" hspace="0"
													vspace="5">
											</div>

											<div id="checkoutGuaranteeDivHeader">
												<span class="USAT_BlueSubHeader">&nbsp;&nbsp;&nbsp;USA
													TODAY guarantees your satisfaction!</span>
											</div>

											<div id="checkoutGuaranteeTextDiv">
												<span class="outputText"><br>At USA TODAY, we
													want you to be 100% satisfied with your subscription. If
													you are not satisfied for any reason, we will provide you
													with a refund on all undelivered issues remaining in your
													subscription term. You can cancel your subscription by
													calling <br>1-800-872-0001.</span>
											</div>
										</div>
										<!-- End Guarantee Window -->
										<!-- Additional Address Learn More -->
										<hx:panelDialog type="modeless" styleClass="panelDialog"
											id="dialogAdditionalAddrHelp"
											relativeTo="formItemDeliveryAddress2" for="textAddrLearnMore"
											title="Learn More">

											<h:panelGroup id="groupAddlAddrHelpGroup1"
												styleClass="panelDialog_Footer">



												<h:panelGrid styleClass="panelGrid" id="grid1" columns="1"
													width="250">
													<h:outputText styleClass="outputText"
														id="textAdditionalAddrLearnMoreInfoText"
														value="#{labels.AdditionalAddrLearnMore}"></h:outputText>

												</h:panelGrid>
												<hx:commandExButton id="button3"
													styleClass="commandExButton" type="submit" value="Close">
													<hx:behavior event="onclick" behaviorAction="hide;stop"
														targetAction="dialogAdditionalAddrHelp" id="behavior3"></hx:behavior>
												</hx:commandExButton>



											</h:panelGroup>
										</hx:panelDialog>


										<hx:behaviorKeyPress key="Enter" id="behaviorKeyPress1"
											behaviorAction="click"></hx:behaviorKeyPress>

										<h:inputHidden
											value="#{currentOfferHandler.EZPayFreeWeeksOffer}"
											id="numberEZPayFreeWeeks"></h:inputHidden>

									</h:form>
									<div id="formSubmittedOverlay" style="display: none;"></div>
								</hx:scriptCollector>
								<!-- Ve Interactive USAT Only-->
								<%-- 								<%
									String veInteractive = "";
										if (pubCode.equalsIgnoreCase(UsaTodayConstants.UT_PUBCODE)) {
											veInteractive = "<script type=\"text/javascript\">var journeycode='75e65509-bb2e-4b0a-b56c-a08755cd214b';var captureConfigUrl='cdsusa.veinteractive.com/CaptureConfigService.asmx/CaptureConfig'; (function() {     var ve = document.createElement('script'); ve.type = 'text/javascript'; ve.async = true;     ve.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'configusa.veinteractive.com/vecapturev5.js';     var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ve, s);})();</script>";
										}
								%>
								<%=veInteractive%>
								<!-- end Ve Interactive UT -->
								<!-- Ve Interactive EE Only-->
								<%
									String eeVeInteractive = "";
										if (pubCode
												.equalsIgnoreCase(UsaTodayConstants.DEFAULT_UT_EE_PUBCODE)) {
											eeVeInteractive = "<input id=\"usatodayeedition\" type=\"hidden\" value=\"usatodayeedition\"><script type=\"text/javascript\">var journeycode='75e65509-bb2e-4b0a-b56c-a08755cd214b';var captureConfigUrl='cdsusa.veinteractive.com/CaptureConfigService.asmx/CaptureConfig'; (function() {     var ve = document.createElement('script'); ve.type = 'text/javascript'; ve.async = true;     ve.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'configusa.veinteractive.com/vecapturev7.js';     var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ve, s);})();</script>";
										}
								%>
								<%=eeVeInteractive%>
								<!-- end Ve Interactive EE -->

								<!-- Google remarketing -->
								<script type="text/javascript">
									/*           */
									var google_conversion_id = 1071907126;
									var google_conversion_language = "en";
									var google_conversion_format = "3";
									var google_conversion_color = "666666";
									var google_conversion_label = "-P-9CO-XhgIQtoKQ_wM";
									var google_conversion_value = 0;
									/*     */
								</script>
								<script type="text/javascript"
									src="https://www.googleadservices.com/pagead/conversion.js">
									
								</script>
								<!-- end google Remarketing -->
								<!-- - FetchBack Remarketing frame -->
								<iframe
									src='https://pixel.fetchback.com/serve/fb/pdj?cat=&name=landing&sid=4683'
									scrolling='no' width='1' height='1' marginheight='0'
									marginwidth='0' frameborder='0'></iframe>
 --%>
								<!-- - End FetchBack Remarketing frame -->
							<%-- /tpl:put --%>
							<%=trackingBug.getOmnitureHTML()%>
							<!-- END THIRD PARTY CONTENT -->

						</div>
						<div class="sh_bottom"></div>
						<div class="pageinfo data hidden">{ "asset_collection": [],
							"aws": "", "aws_id": "", "ssts": "", "blogname": "",
							"contenttype":" ", "seotitletag": "", "templatename":
							"fronts/default", "videoincluded":"" }</div>
					</section>
				</section>
			</div>
		</article>
	</body>
</f:view>
</html><%-- /tpl:insert --%>