<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/subscriptions/dc/Order_in_progress.java" --%><%-- /jsf:pagecode --%><%-- %@page
	language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"% --%><%-- tpl:insert page="/theme/themeV3/dotcomTheme/dotcomJSFDT.jtpl" --%><!doctype html><%@page
	import="com.usatoday.business.interfaces.products.ProductIntf"%>
<%@page import="com.usatoday.businessObjects.products.ProductBO"%>
<%@page import="com.usatoday.util.constants.UsaTodayConstants"%>
<!--[if IE 7]><html class="no-js lt-ie11 lt-ie10 lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie11 lt-ie10 lt-ie9" lang="en"><![endif]-->
<!--[if IE 9]><html class="no-js lt-ie11 lt-ie10" lang="en"><![endif]-->
<!--[if IE 10]><html class="no-js lt-ie11" lang="en"><![endif]-->
<!--[if gt IE 10]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<%@page language="java"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%><%@taglib
	uri="http://java.sun.com/jsf/html" prefix="h"%><%@taglib
	uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%
	String dotcomDomain = "usatoday.com";
	if (UsaTodayConstants.debug) {
		dotcomDomain = "ux-dev.usatoday.com";
	}

	com.usatoday.esub.common.WebBug trackingBug = com.usatoday.esub.common.UTCommon
			.getWebAnalyticsBug();
	try {
		trackingBug.deriveValuesFromRequest(request);
	} catch (Exception e) {
		; // ignore
	}
	String pubCode = "UT";
	String productName = "USA TODAY";
	com.usatoday.business.interfaces.products.OfferIntf offer = null;
	try {
		offer = com.usatoday.esub.common.UTCommon
				.getCurrentOfferVersion2(request);
		if (offer != null) {
			pubCode = offer.getPubCode();
			try {
				String brandingPubCode = offer.getProduct()
						.getBrandingPubCode();
				ProductIntf bp = ProductBO
						.fetchProduct(brandingPubCode);
				productName = bp.getName();
			} catch (Exception teee) {
			}
		}
	} catch (Exception ee) {
	}
%>
<head prefix="og: http://ogp.me/ns#">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<%-- tpl:put name="headarea" --%><title>USA TODAY.com Check Out In Progress</title><%-- /tpl:put --%>
<meta name="ROBOTS" content="NOODP, NOYDIR" />
<meta property="fb:app_id" content="251772488263583" />
<meta property="og:description" content="" />
<meta property="og:image" content="/static/images/logos/.png">
<meta property="og:url" content="" />
<meta property="og:type" content="website">
<meta property="og:title" content="" />
<meta name="viewport" content="width=1024">
<link rel="shortcut icon" href="/static/images/favicon.png">
<link rel="stylesheet" href="/static/css/main.min.css?v=off">
<!--[if lt IE 9]><link rel="stylesheet" href="/static/css/main.min.css?v=UX-122-1986-g45e8daf-ab032e7608bb11e2af2312313b0f0def"><script src="/static/js/libs/respond/respond.min.js"></script><![endif]-->
<link rel="stylesheet"
	href="/static/css/print.css?v=UX-122-1986-g45e8daf-ab032e7608bb11e2af2312313b0f0def"
	media="print">
<script type="text/javascript">
	window.site_config = {
		"ADS" : {
			"SLIDER" : {
				"REFRESHTIME" : 900000
			},
			"THRESHOLDS" : {
				"SLIDE" : [ 3, 10, 10, -1 ],
				"ASSET" : [ 2, 6, 6, -1 ]
			},
			"PROVIDER" : "DFP",
			"DFP" : {
				"ACCOUNTNAME" : "usatoday",
				"ENV" : "dev",
				"ACCOUNTID" : "7070"
			},
			"SHOW" : true
		},
		"AUTH" : {
			"FACEBOOK" : {
				"PERMISSIONS" : "user_about_me,user_groups,user_location,user_interests,user_events,user_likes,user_education_history,friends_about_me,friends_likes,publish_stream",
				"CHANNELURL" : "/static/html/channel.html",
				"APPID" : "251772488263583"
			}
		}
	};
	window.use_minified_css = true;
	window.isAdDebug = false;
</script>
<script src="/static/js/libs/modernizr/modernizr.js"></script>
<%-- tpl:put name="headarea_2" --%>
<script language="JavaScript" src="/common/prototype/prototype.js"></script>
<script src="/common/usatCommon.js"></script>
<script language="JavaScript">
function initPage() {
	startChecking();
}
function startChecking() {
	setTimeout('checkStatus()', 5000);
}

function checkStatus() {
	$('formCheckStatus:buttonUpdateStatus').click();
}
</script>
<meta contentType="text/html; charset=UTF-8" pageEncoding="UTF-8">
<%-- /tpl:put --%>
</head>
<f:view>
	<f:loadBundle basename="resourceFile" var="labels" />
	<body class=" third-party">
		<div style="float: none">
	<%
		String uri = request.getRequestURI();
		String pageName = uri.substring(uri.lastIndexOf("/") + 1);
		String jScript = "";
		if (uri.indexOf("subscriptions") > 0 && uri.indexOf("thankyou.jsp") > 0) {
			try {
				com.usatoday.esub.handlers.ShoppingCartHandler shoppingCartHandler = (com.usatoday.esub.handlers.ShoppingCartHandler) session.getAttribute("shoppingCartHandler");
				if (shoppingCartHandler != null) {
					jScript = shoppingCartHandler.getOnePageThankYouJavaScriptTealiumText();			
				}
			} catch (Exception E) {
		
			} 
		} else {
			com.usatoday.businessObjects.products.promotions.PromotionSet currentOfferPromotionSetTealium = null;
			com.usatoday.business.interfaces.products.OfferIntf offerTealium = com.usatoday.esub.common.UTCommon.getCurrentOffer(request);
			if (offerTealium != null && offerTealium instanceof com.usatoday.business.interfaces.products.SubscriptionOfferIntf) {
				currentOfferPromotionSetTealium = ((com.usatoday.business.interfaces.products.SubscriptionOfferIntf)offer).getPromotionSet();
				jScript = currentOfferPromotionSetTealium.getPromoOnePageJavaScriptTealiumText();
				jScript = org.apache.commons.lang3.StringUtils.replace(jScript, "page_name:\"\"", "page_name:\"" + uri + "\"");
			}
		}
	%>
	<%=jScript%>
			<%-- 		<f:verbatim>
			<h:outputText styleClass="outputText" id="textThankYouJS13"	escape="false" value="#{shoppingCartHandler.tealiumOnePageThankYouJavaScriptText}"></h:outputText>
		</f:verbatim>
 --%>
		</div>
		<header id="header">
			<div class="fixed-wrap">
				<div id="masthead" class="clearfix">
					<div class="logo ">
						<a id="logo-link" data-ht="Headerusatoday"
							href="http://<%=dotcomDomain%>/">USA Today</a>
					</div>
					<div id="search-form">
						<form class="search" action="http://<%=dotcomDomain%>/search/"
							method="get">
							<input class="text-input" type="text" data-placeholder=""
								name="q"><input class="submit" type="submit"
								value="Search"><span
								class="ui-dark ui-btn cancel icon no-text">X</span>
						</form>
					</div>
				</div>
				<nav id="nav">
					<ul id="navbar">
						<li class="news text" data-section="news"><a
							class="nav-anchor"
							href="http://<%=dotcomDomain%>/news/#ht=Headernews"
							data-ht="Headernews"><span class="news-span nav-span">News</span>
						</a></li>
						<li class="sports text" data-section="sports"><a
							class="nav-anchor"
							href="http://<%=dotcomDomain%>/sports/#ht=Headersports"
							data-ht="Headersports"><span class="sports-span nav-span">Sports</span>
						</a></li>
						<li class="life text" data-section="life"><a
							class="nav-anchor"
							href="http://<%=dotcomDomain%>/life/#ht=Headerlife"
							data-ht="Headerlife"><span class="life-span nav-span">Life</span>
						</a></li>
						<li class="money text" data-section="money"><a
							class="nav-anchor"
							href="http://<%=dotcomDomain%>/money/#ht=Headermoney"
							data-ht="Headermoney"><span class="money-span nav-span">Money</span>
						</a></li>
						<li class="tech text" data-section="tech"><a
							class="nav-anchor"
							href="http://<%=dotcomDomain%>/tech/#ht=Headertech"
							data-ht="Headertech"><span class="tech-span nav-span">Tech</span>
						</a></li>
						<li class="travel text" data-section="travel"><a
							class="nav-anchor"
							href="http://<%=dotcomDomain%>/travel/#ht=Headertravel"
							data-ht="Headertravel"><span class="travel-span nav-span">Travel</span>
						</a></li>
						<li class="opinion text" data-section="opinion"><a
							class="nav-anchor"
							href="http://<%=dotcomDomain%>/opinion/#ht=Headeropinion"
							data-ht="Headeropinion"><span class="opinion-span nav-span">Opinion</span>
						</a></li>
						<li class="weather icon text" data-section="weather"><a
							class="nav-weather-link" href="http://<%=dotcomDomain%>/weather/"
							data-ht="HeaderWeather"><span class="wbtn-wrap nav-span"><span
									class="btn-temp"></span><span class="btn-label">Weather</span>
							</span> </a>
							<div class="dropdown">
								<div class="dropdown-panel">
									<div class="front-side"></div>
									<div class="back-side">
										<p class="heading">Settings</p>
										<form id="weather-dropdown-form" name="weather-dropdown-form"
											method="get">
											<p class="label">Enter City, State or Zip</p>
											<input class="ui-text-input query" type="text" value="">
											<p class="ui-btn ui-dark ui-btn2 icon cancel cancel-btn">Cancel</p>
											<p class="ui-btn ui-light ui-btn2 icon checkmark set submit">Set</p>
										</form>
										<p class="cancel-x">Cancel</p>
									</div>
								</div>
								<p class="full-forecast">
									<a href="/weather/"><span>Full Forecast</span> </a>
								</p>
							</div></li>
						<li class="media icon"><a class="icon-anchor"
							href="http://<%=dotcomDomain%>/media/latest/news/"
							data-ht="HeaderMedia"><span class="icon-span">Media</span> </a></li>
						<li class="cover icon"><a
							href="http://<%=dotcomDomain%>/cover-view/" class="icon-anchor"
							data-ht="colorbarcoverview" title="Cover view"><span
								class="icon-span">Cover View</span> </a></li>
						<!--<li class="sbs icon"><a href="#" data-ht="HeaderStateByState"><span>State by State</span></a></li>-->
						<!--<li class="timeline icon"><a href="#" data-ht="HeaderTimeline"><span>Timeline</span></a></li>-->
						<!--<li class="ugc icon"><a href="#" data-ht="HeaderUGC"><span>You Report</span></a></li>-->
					</ul>
				</nav>
				<div id="breaking" data-lastupdate="" data-type="">
					<div id="breaking-wrap">
						<p>
							<strong></strong><span class="close-btn">Close</span>
						</p>
					</div>
				</div>
				<div class="header-shadow"></div>
			</div>
		</header>
		<article id="cards" class="cards">
			<div class="card-container">
				<section class="card-wrap">
					<div class="ad_container1">
						<div id="partner__leavebehind"
							class="partner-placement-leavebehind"
							data-slot-id="partner_high_impact_"></div>
						<div id="partner_high_impact__pushdown"
							class="partner-placement pushdown" data-sizes="pushdown"
							data-slot-id="partner_high_impact_" data-shared-placement="true"></div>
						<div id="partner_high_impact__livefeed"
							class="partner-placement livefeed" data-sizes="livefeed"
							data-slot-id="partner_high_impact_" data-shared-placement="true"></div>
					</div>
					<section id="section_" class=" card " data-section-id=""
						data-subsection-id="" data-last-update="" data-template="">
						<div class="card-content">

							<!-- START THIRD PARTY CONTENT -->
							<%-- tpl:put name="bodyarea" --%>
					<hx:scriptCollector id="scriptCollectorOrderChecker1" decode="#{pc_Error_in_progress.onPagePost}">
						<h1>Order processing...</h1>
					<h:form styleClass="form" id="formCheckStatus">
						<br>
							<h:messages styleClass="messages" id="messages1" globalOnly="false"></h:messages>
							<br>
						<br>
					Please wait while your order is processing. This page will automatically update. If it does not you may click Update Status below.
					
					
					
					<br>
							<br>
							<hx:progressBar auto="true" timeInterval="1000" proportion="20" styleClass="progressBar" id="bar1" initHidden="false"></hx:progressBar>
							<br>
							<br>
							<hx:commandExButton type="submit" value="Check Status" styleClass="commandExButton" id="buttonUpdateStatus" action="#{pc_Order_in_progress1.doButtonUpdateStatusAction}"></hx:commandExButton>

							<br>
							<br>
							<br>
						</h:form>
						<br>
						<br>
						
						<br>
					</hx:scriptCollector>

	<%-- /tpl:put --%>
							<%=trackingBug.getOmnitureHTML()%>
							<!-- END THIRD PARTY CONTENT -->

						</div>
						<div class="sh_bottom"></div>
						<div class="pageinfo data hidden">{ "asset_collection": [],
							"aws": "", "aws_id": "", "ssts": "", "blogname": "",
							"contenttype":" ", "seotitletag": "", "templatename":
							"fronts/default", "videoincluded":"" }</div>
					</section>
				</section>
			</div>
		</article>
	</body>
</f:view>
</html><%-- /tpl:insert --%>