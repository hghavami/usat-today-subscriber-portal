<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<html>
<!-- THIS PAGE IS ONLY For Google Search -->
<head>
<title>usa-today</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body onload="javascript:setTimeout('doRedirect()', 200);"">
<script language="JavaScript">

var queryString = window.top.location.search.substring(1);

function doRedirect() {
	var pubCode = getParameter(queryString, 'pub');
	var keyCode = getParameter(queryString, 'keycode');

	var redirectURL = window.location.protocol + '//' + window.location.host + '/subscriptions/order/checkout.faces';

	if (pubCode != "null") {
		redirectURL = redirectURL + '?pub=' + pubCode;
		if (keyCode != "null") {
			redirectURL = redirectURL + '&keycode=' + keyCode;
		}
	}

	window.location.replace(redirectURL);
}

function getParameter ( queryString, parameterName ) {
	// Add "=" to the parameter name (i.e. parameterName=value)
	var parameterName = parameterName + "=";
	if ( queryString.length > 0 ) {
		// Find the beginning of the string
		begin = queryString.indexOf ( parameterName );
		// If the parameter name is not found, skip it, otherwise return the value
		if ( begin != -1 ) {
			// Add the length (integer) to the beginning
			begin += parameterName.length;
			// Multiple parameters are separated by the "&" sign
			end = queryString.indexOf ( "&" , begin );
	
			if ( end == -1 ) {
				end = queryString.length
			}
			// Return the string
			// return unescape ( queryString.substring ( begin, end ) );
			return queryString.substring ( begin, end );
		} 
		
		// Return "null" if no parameter has been found
		return "null";
	} // end if query string length > 0 
	return "null";
}
</script>
</body>
</html>