<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/subscriptions/ion/Order_in_progress.java" --%><%-- /jsf:pagecode --%>
<%-- %@page
	language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"% --%>
<%-- tpl:insert page="/theme/themeV3/ionTheme/ionJSFDT.jtpl" --%><!doctype html><%@page
	import="com.usatoday.business.interfaces.products.ProductIntf"%>
<%@page import="com.usatoday.businessObjects.products.ProductBO"%>
<%@page import="com.usatoday.util.MobileDeviceDetectionUtil"%>
<%@page import="com.usatoday.util.constants.UsaTodayConstants"%>
<!--[if IE 7]><html class="no-js lt-ie11 lt-ie10 lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie11 lt-ie10 lt-ie9" lang="en"><![endif]-->
<!--[if IE 9]><html class="no-js lt-ie11 lt-ie10" lang="en"><![endif]-->
<!--[if IE 10]><html class="no-js lt-ie11" lang="en"><![endif]-->
<!--[if gt IE 10]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<%@page language="java"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%><%@taglib
	uri="http://java.sun.com/jsf/html" prefix="h"%><%@taglib
	uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%
	String dotcomDomain = "usatoday.com";
	if (UsaTodayConstants.debug) {
		dotcomDomain = "ux-dev.usatoday.com";
	}

	com.usatoday.esub.common.WebBug trackingBug = com.usatoday.esub.common.UTCommon
			.getWebAnalyticsBug();
	try {
		trackingBug.deriveValuesFromRequest(request);
	} catch (Exception e) {
		; // ignore
	}
	String pubCode = "UT";
	String productName = "USA TODAY";
	com.usatoday.business.interfaces.products.OfferIntf offer = null;
	try {
		offer = com.usatoday.esub.common.UTCommon
				.getCurrentOfferVersion2(request);
		if (offer != null) {
			pubCode = offer.getPubCode();
			try {
				String brandingPubCode = offer.getProduct()
						.getBrandingPubCode();
				ProductIntf bp = ProductBO
						.fetchProduct(brandingPubCode);
				productName = bp.getName();
			} catch (Exception teee) {
			}
		}
	} catch (Exception ee) {
	}
	String mobileCSS = "";
	if (MobileDeviceDetectionUtil.isSmallScreenDevice(request)) {
		mobileCSS = "<LINK href=\"/theme/themeV3/css/ionSmallScreen.css\" rel=\"stylesheet\" type=\"text/css\">";
	}
%>
<head prefix="og: http://ogp.me/ns#">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<%-- tpl:put name="headarea" --%>
	<title>USA TODAY.com Check Out In Progress</title>
<%-- /tpl:put --%>
<meta name="ROBOTS" content="NOODP, NOYDIR" />
<meta property="fb:app_id" content="251772488263583" />
<meta property="og:description" content="" />
<meta property="og:image" content="/static/images/logos/.png">
<meta property="og:url" content="" />
<meta property="og:type" content="website">
<meta property="og:title" content="" />
<meta name="viewport" content="width=1024">
<link rel="shortcut icon" href="/static/images/favicon.png">
<!-- <link rel="stylesheet" href="/static/css/main.ion.css?v=off"> -->
<!--[if lt IE 9]><link rel="stylesheet" href="/static/css/main.min.css?v=UX-122-1986-g45e8daf-ab032e7608bb11e2af2312313b0f0def"><script src="/static/js/libs/respond/respond.min.js"></script><![endif]-->
<link rel="stylesheet"
	href="/static/css/print.css?v=UX-122-1986-g45e8daf-ab032e7608bb11e2af2312313b0f0def"
	media="print">
<%=mobileCSS %>	
<script type="text/javascript">
	window.site_config = {
		"ADS" : {
			"SLIDER" : {
				"REFRESHTIME" : 900000
			},
			"THRESHOLDS" : {
				"SLIDE" : [ 3, 10, 10, -1 ],
				"ASSET" : [ 2, 6, 6, -1 ]
			},
			"PROVIDER" : "DFP",
			"DFP" : {
				"ACCOUNTNAME" : "usatoday",
				"ENV" : "dev",
				"ACCOUNTID" : "7070"
			},
			"SHOW" : true
		},
		"AUTH" : {
			"FACEBOOK" : {
				"PERMISSIONS" : "user_about_me,user_groups,user_location,user_interests,user_events,user_likes,user_education_history,friends_about_me,friends_likes,publish_stream",
				"CHANNELURL" : "/static/html/channel.html",
				"APPID" : "251772488263583"
			}
		}
	};
	window.use_minified_css = true;
	window.isAdDebug = false;
</script>
<script src="/static/js/libs/modernizr/modernizr.js"></script>
<%-- tpl:put name="headarea_2" --%>
	<script language="JavaScript" src="/common/prototype/prototype.js"></script>
	<script src="/common/usatCommon.js"></script>
	<script language="JavaScript">
		function initPage() {
			startChecking();
		}
		function startChecking() {
			setTimeout('checkStatus()', 5000);
		}

		function checkStatus() {
			$('formCheckStatus:buttonUpdateStatus').click();
		}
	</script>
	<meta contentType="text/html; charset=UTF-8" pageEncoding="UTF-8">
<%-- /tpl:put --%>
</head>
<f:view>
	<f:loadBundle basename="resourceFile" var="labels" />
	<body class=" third-party">
		<div style="float: none">
			<%
				String uri = request.getRequestURI();
					String pageName = uri.substring(uri.lastIndexOf("/") + 1);
					String jScript = "";
					if (uri.indexOf("subscriptions") > 0 && uri.indexOf("thankyou.jsp") > 0) {
						try {
							com.usatoday.esub.handlers.ShoppingCartHandler shoppingCartHandler = (com.usatoday.esub.handlers.ShoppingCartHandler) session
									.getAttribute("shoppingCartHandler");
							if (shoppingCartHandler != null) {
								jScript = shoppingCartHandler.getOnePageThankYouJavaScriptTealiumText();
							}
						} catch (Exception E) {

						}
					} else {
						com.usatoday.businessObjects.products.promotions.PromotionSet currentOfferPromotionSetTealium = null;
						com.usatoday.business.interfaces.products.OfferIntf offerTealium = com.usatoday.esub.common.UTCommon
								.getCurrentOffer(request);
						if (offerTealium != null
								&& offerTealium instanceof com.usatoday.business.interfaces.products.SubscriptionOfferIntf) {
							currentOfferPromotionSetTealium = ((com.usatoday.business.interfaces.products.SubscriptionOfferIntf) offer)
									.getPromotionSet();
							jScript = currentOfferPromotionSetTealium.getPromoOnePageJavaScriptTealiumText();
							jScript = org.apache.commons.lang3.StringUtils.replace(jScript, "page_name:\"\"", "page_name:\"" + uri
									+ "\"");
						}
					}
					if (jScript == null) {
						jScript = "";
					}
			%>
			<%=jScript%>
			<%-- 		<f:verbatim>
			<h:outputText styleClass="outputText" id="textThankYouJS13"	escape="false" value="#{shoppingCartHandler.tealiumOnePageThankYouJavaScriptText}"></h:outputText>
		</f:verbatim>
 --%>
		</div>
		<header id="header"> </header>
		<article id="cards" class="cards">
			<div class="card-container">
				<section class="card-wrap">
					<!-- 					<div class="ad_container1">
						<div id="partner__leavebehind"
							class="partner-placement-leavebehind"
							data-slot-id="partner_high_impact_"></div>
						<div id="partner_high_impact__pushdown"
							class="partner-placement pushdown" data-sizes="pushdown"
							data-slot-id="partner_high_impact_" data-shared-placement="true"></div>
						<div id="partner_high_impact__livefeed"
							class="partner-placement livefeed" data-sizes="livefeed"
							data-slot-id="partner_high_impact_" data-shared-placement="true"></div>
					</div>
 -->
					<section id="section_" class=" card " data-section-id=""
						data-subsection-id="" data-last-update="" data-template="">
						<div class="card-content">

							<!-- START THIRD PARTY CONTENT -->
							<%-- tpl:put name="bodyarea" --%>
								<hx:scriptCollector id="scriptCollectorOrderChecker1"
									decode="#{pc_Error_in_progress.onPagePost}">
									<h1>Order processing...</h1>
									<h:form styleClass="form" id="formCheckStatus">
										<br>
										<h:messages styleClass="messages" id="messages1"
											globalOnly="false"></h:messages>
										<br>
										<br>
					Please wait while your order is processing. This page will automatically update. If it does not you may click Update Status below.
					<br>
										<br>
										<hx:progressBar auto="true" timeInterval="1000"
											proportion="20" styleClass="progressBar" id="bar1"
											initHidden="false"></hx:progressBar>
										<br>
										<br>
										<hx:commandExButton type="submit" value="Check Status"
											styleClass="commandExButton" id="buttonUpdateStatus"
											action="#{pc_Order_in_progress1.doButtonUpdateStatusAction}"></hx:commandExButton>

										<br>
										<br>
										<br>
									</h:form>
									<br>
									<br>

									<br>
								</hx:scriptCollector>
							<%-- /tpl:put --%>
							<%=trackingBug.getOmnitureHTML()%>
							<!-- END THIRD PARTY CONTENT -->

						</div>
						<!-- 						<div class="sh_bottom"></div>
						<div class="pageinfo data hidden">{ "asset_collection":
							[], "aws": "", "aws_id": "", "ssts": "", "blogname": "",
							"contenttype":" ", "seotitletag": "", "templatename":
							"fronts/default", "videoincluded":"" }</div>
 -->
					</section>
				</section>
			</div>
		</article>
	</body>
</f:view>
</html><%-- /tpl:insert --%>