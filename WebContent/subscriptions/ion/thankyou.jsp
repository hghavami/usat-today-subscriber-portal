<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/subscriptions/ion/Thankyou.java" --%><%-- /jsf:pagecode --%>
<%-- %@page language="java" contentType="text/html; charset=UTF-8"
	 pageEncoding="UTF-8"% --%>
<%-- %@page import="com.usatoday.business.interfaces.products.ProductIntf"% --%>
<%-- %@page import="com.usatoday.businessObjects.products.ProductBO"% --%>
<%-- %@page import="com.usatoday.util.MobileDeviceDetectionUtil"% --%>
<%-- %@page
	import="com.usatoday.business.interfaces.products.promotions.HTMLPromotionIntf"% --%>
<%-- tpl:insert page="/theme/themeV3/ionTheme/ionJSFDT.jtpl" --%><!doctype html><%@page
	import="com.usatoday.business.interfaces.products.ProductIntf"%>
<%@page import="com.usatoday.businessObjects.products.ProductBO"%>
<%@page import="com.usatoday.util.MobileDeviceDetectionUtil"%>
<%@page import="com.usatoday.util.constants.UsaTodayConstants"%>
<!--[if IE 7]><html class="no-js lt-ie11 lt-ie10 lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie11 lt-ie10 lt-ie9" lang="en"><![endif]-->
<!--[if IE 9]><html class="no-js lt-ie11 lt-ie10" lang="en"><![endif]-->
<!--[if IE 10]><html class="no-js lt-ie11" lang="en"><![endif]-->
<!--[if gt IE 10]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<%@page language="java"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%><%@taglib
	uri="http://java.sun.com/jsf/html" prefix="h"%><%@taglib
	uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%
	String dotcomDomain = "usatoday.com";
	if (UsaTodayConstants.debug) {
		dotcomDomain = "ux-dev.usatoday.com";
	}

	com.usatoday.esub.common.WebBug trackingBug = com.usatoday.esub.common.UTCommon
			.getWebAnalyticsBug();
	try {
		trackingBug.deriveValuesFromRequest(request);
	} catch (Exception e) {
		; // ignore
	}
	String pubCode = "UT";
	String productName = "USA TODAY";
	com.usatoday.business.interfaces.products.OfferIntf offer = null;
	try {
		offer = com.usatoday.esub.common.UTCommon
				.getCurrentOfferVersion2(request);
		if (offer != null) {
			pubCode = offer.getPubCode();
			try {
				String brandingPubCode = offer.getProduct()
						.getBrandingPubCode();
				ProductIntf bp = ProductBO
						.fetchProduct(brandingPubCode);
				productName = bp.getName();
			} catch (Exception teee) {
			}
		}
	} catch (Exception ee) {
	}
	String mobileCSS = "";
	if (MobileDeviceDetectionUtil.isSmallScreenDevice(request)) {
		mobileCSS = "<LINK href=\"/theme/themeV3/css/ionSmallScreen.css\" rel=\"stylesheet\" type=\"text/css\">";
	}
%>
<head prefix="og: http://ogp.me/ns#">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<%-- tpl:put name="headarea" --%>
	<title>USA TODAY.com - Thank You</title>
<%-- /tpl:put --%>
<meta name="ROBOTS" content="NOODP, NOYDIR" />
<meta property="fb:app_id" content="251772488263583" />
<meta property="og:description" content="" />
<meta property="og:image" content="/static/images/logos/.png">
<meta property="og:url" content="" />
<meta property="og:type" content="website">
<meta property="og:title" content="" />
<meta name="viewport" content="width=1024">
<link rel="shortcut icon" href="/static/images/favicon.png">
<!-- <link rel="stylesheet" href="/static/css/main.ion.css?v=off"> -->
<!--[if lt IE 9]><link rel="stylesheet" href="/static/css/main.min.css?v=UX-122-1986-g45e8daf-ab032e7608bb11e2af2312313b0f0def"><script src="/static/js/libs/respond/respond.min.js"></script><![endif]-->
<link rel="stylesheet"
	href="/static/css/print.css?v=UX-122-1986-g45e8daf-ab032e7608bb11e2af2312313b0f0def"
	media="print">
<%=mobileCSS %>	
<script type="text/javascript">
	window.site_config = {
		"ADS" : {
			"SLIDER" : {
				"REFRESHTIME" : 900000
			},
			"THRESHOLDS" : {
				"SLIDE" : [ 3, 10, 10, -1 ],
				"ASSET" : [ 2, 6, 6, -1 ]
			},
			"PROVIDER" : "DFP",
			"DFP" : {
				"ACCOUNTNAME" : "usatoday",
				"ENV" : "dev",
				"ACCOUNTID" : "7070"
			},
			"SHOW" : true
		},
		"AUTH" : {
			"FACEBOOK" : {
				"PERMISSIONS" : "user_about_me,user_groups,user_location,user_interests,user_events,user_likes,user_education_history,friends_about_me,friends_likes,publish_stream",
				"CHANNELURL" : "/static/html/channel.html",
				"APPID" : "251772488263583"
			}
		}
	};
	window.use_minified_css = true;
	window.isAdDebug = false;
</script>
<script src="/static/js/libs/modernizr/modernizr.js"></script>
<%-- tpl:put name="headarea_2" --%>
	<%
		if (trackingBug != null) {
			trackingBug
					.setOmniturePageNameOverride("Subscriptions:New Subscription: USATODAY.com One Page Order Thank You Page");
		}
	%>
	<script src="/common/prototype/prototype.js" type="text/javascript"></script>
	<script src="/common/prototype/scriptaculous/scriptaculous.js"
		type="text/javascript"></script>
	<script src="/common/usatCommon.js"></script>
	<link rel="stylesheet" href="/theme/themeV3/css/dotcom.css"
		type="text/css">
	<script language="JavaScript" src="/common/eEditionScripts.js"
		type="text/javascript"></script>
	<script type="text/javascript">
var PIXEL_OID='#{shoppingCartHandler.lastOrder.orderID}';
var PIXEL_ORDER_TOTAL='#{shoppingCartHandler.lastOrder.total}';
var PIXEL_ORDER_SUBTOTAL='#{shoppingCartHandler.lastOrder.subTotal}';
var PIXEL_ORDER_SUBTOTAL_NOPUNC = '#{shoppingCartHandler.subTotalWithNoPunctuation}';
var PIXEL_KEYCODE='#{currentOfferHandler.currentOffer.keyCode}';
var PIXEL_PRODUCT='#{shoppingCartHandler.lastOrder.orderedItems[0].product.productCode}';
var PIXEL_QTY='#{shoppingCartHandler.lastOrder.orderedItems[0].quantity}';
var PIXEL_DELIVERY_EMAIL = '#{shoppingCartHandler.lastOrder.deliveryContact.emailAddress}';

function printPage(thisObj, thisEvent) {
	window.print();
	return false; 
}

function func_1(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	return openReaderWindow();
}</script>
	<script type="text/javascript">
var currentPub = "<%=pubCode%>";

		// method called on page load
		function initPage(thisObj, thisEvent) {
			// insert site-wide code here

		}
	</script>
	<script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push([ '_setAccount', 'UA-23158809-1' ]);
		_gaq.push([ '_trackPageview' ]);

		(function() {
			var ga = document.createElement('script');
			ga.type = 'text/javascript';
			ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl'
					: 'http://www')
					+ '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(ga, s);
		})();
	</script>
<%-- /tpl:put --%>
</head>
<f:view>
	<f:loadBundle basename="resourceFile" var="labels" />
	<body class=" third-party">
		<div style="float: none">
			<%
				String uri = request.getRequestURI();
					String pageName = uri.substring(uri.lastIndexOf("/") + 1);
					String jScript = "";
					if (uri.indexOf("subscriptions") > 0 && uri.indexOf("thankyou.jsp") > 0) {
						try {
							com.usatoday.esub.handlers.ShoppingCartHandler shoppingCartHandler = (com.usatoday.esub.handlers.ShoppingCartHandler) session
									.getAttribute("shoppingCartHandler");
							if (shoppingCartHandler != null) {
								jScript = shoppingCartHandler.getOnePageThankYouJavaScriptTealiumText();
							}
						} catch (Exception E) {

						}
					} else {
						com.usatoday.businessObjects.products.promotions.PromotionSet currentOfferPromotionSetTealium = null;
						com.usatoday.business.interfaces.products.OfferIntf offerTealium = com.usatoday.esub.common.UTCommon
								.getCurrentOffer(request);
						if (offerTealium != null
								&& offerTealium instanceof com.usatoday.business.interfaces.products.SubscriptionOfferIntf) {
							currentOfferPromotionSetTealium = ((com.usatoday.business.interfaces.products.SubscriptionOfferIntf) offer)
									.getPromotionSet();
							jScript = currentOfferPromotionSetTealium.getPromoOnePageJavaScriptTealiumText();
							jScript = org.apache.commons.lang3.StringUtils.replace(jScript, "page_name:\"\"", "page_name:\"" + uri
									+ "\"");
						}
					}
					if (jScript == null) {
						jScript = "";
					}
			%>
			<%=jScript%>
			<%-- 		<f:verbatim>
			<h:outputText styleClass="outputText" id="textThankYouJS13"	escape="false" value="#{shoppingCartHandler.tealiumOnePageThankYouJavaScriptText}"></h:outputText>
		</f:verbatim>
 --%>
		</div>
		<header id="header"> </header>
		<article id="cards" class="cards">
			<div class="card-container">
				<section class="card-wrap">
					<!-- 					<div class="ad_container1">
						<div id="partner__leavebehind"
							class="partner-placement-leavebehind"
							data-slot-id="partner_high_impact_"></div>
						<div id="partner_high_impact__pushdown"
							class="partner-placement pushdown" data-sizes="pushdown"
							data-slot-id="partner_high_impact_" data-shared-placement="true"></div>
						<div id="partner_high_impact__livefeed"
							class="partner-placement livefeed" data-sizes="livefeed"
							data-slot-id="partner_high_impact_" data-shared-placement="true"></div>
					</div>
 -->
					<section id="section_" class=" card " data-section-id=""
						data-subsection-id="" data-last-update="" data-template="">
						<div class="card-content">

							<!-- START THIRD PARTY CONTENT -->
							<%-- tpl:put name="bodyarea" --%>
								<!-- Conversion Pixel - Final Purchase - DO NOT MODIFY -->
								<img
									src="https://secure.adnxs.com/px?id=47934&remove=418345&t=2"
									width="1" height="1" />
								<!-- End of Conversion Pixel -->
								<hx:scriptCollector id="scriptCollector1">
									<div style="float: none">
										<f:verbatim>
											<h:outputText styleClass="outputText" id="textThankYouJS10"
												escape="false"
												value="#{shoppingCartHandler.onePageThankYouJavaScript10Text}"></h:outputText>
											<h:outputText styleClass="outputText" id="textThankYouJS13"
												escape="false"
												value="#{shoppingCartHandler.onePageThankYouJavaScript13Text}"></h:outputText>
										</f:verbatim>
									</div>
									<h:form styleClass="form" id="formMainForm">
										<h1>
											<h:outputText id="textMainTableHeader"
												value="#{labels.ThankYouLabel}" styleClass="H1Class"></h:outputText>
										</h1>
										<div id="pageContentEEOrderEntryLeft">
											<h:panelGrid styleClass="panelGridThankYouPage"
												id="gridThankYouInformation" columns="1" cellpadding="2"
												cellspacing="2">
												<h:messages styleClass="messages" id="messages1"
													layout="list" style="color: red"></h:messages>
												<hx:jspPanel id="jspPanelEEThankYouTextPanel"
													rendered="#{shoppingCartHandler.showEEWelcomePanel}">

													<ul>
														<li><h:outputText styleClass=""
																id="textThankYouText2" value="#{labels.ThanYouText2}"></h:outputText>&nbsp;
															<hx:outputLinkEx onclick="return func_1(this, event);"
																value="#{shoppingCartHandler.newUTeEditionLink}"
																styleClass="outputLinkEx" id="linkExReadNowTextLink">
																<h:outputText id="textReadNowTextLink"
																	styleClass="outputText" value="click here"></h:outputText>
															</hx:outputLinkEx>.</li>
														<li><h:outputText styleClass=""
																id="textThankYouBullet2a"
																value="#{labels.thankYouText2a}"></h:outputText>&nbsp; <hx:outputLinkEx
																value="http://service.usatoday.com/welcome.jsp"
																styleClass="outputLinkEx" id="linkExCustServiceLink">
																<h:outputText id="textCustServiceLink"
																	styleClass="outputText" value="www.usatodayservice.com"></h:outputText>
															</hx:outputLinkEx>&nbsp;<h:outputText styleClass=""
																id="textThankYouBullet2b"
																value="#{labels.thankYouText2b}"></h:outputText>
														</li>
														<li><h:outputText id="textThankYouText3"
																value="#{labels.thankYouText3}"></h:outputText>
														</li>
														<li><h:outputText id="textThankYouText4"
																value="#{labels.thankYouText4}"></h:outputText>
														</li>
													</ul>
												</hx:jspPanel>
												<hx:jspPanel id="jspPanelThankYouTextPanel"
													rendered="#{not shoppingCartHandler.showEEWelcomePanel }">
													<ul>
														<li><h:outputText id="textThankYouTextL1"
																value="#{labels.ThankYouText5}"></h:outputText>
														</li>
													</ul>
												</hx:jspPanel>

												<hx:jspPanel id="jspPanelThankYouGiftPanel"
													rendered="#{shoppingCartHandler.showGiftPanel}">
													<ul>
														<li><h:outputText id="textThankYouGift"
																value="Tell them the good News!"></h:outputText>&nbsp;&nbsp;
															<hx:outputLinkEx value="/giftcard/index.faces"
																styleClass="outputLinkEx" id="linkExGiftLink">
																<h:outputText id="textGiftLink" styleClass="outputText"
																	value="Send a Gift Card Now"></h:outputText>
															</hx:outputLinkEx>
														</li>
													</ul>
												</hx:jspPanel>

												<hx:graphicImageEx styleClass="graphicImageEx"
													id="imageExFillerImage" value="/theme/img/JSF_1x1.gif"
													width="1" hspace="1" border="0" vspace="1"></hx:graphicImageEx>


												<h:panelGrid styleClass="panelGridThankYouPage"
													id="gridPrintButtonGrid" cellpadding="0" cellspacing="0">
													<hx:outputLinkEx value="#"
														onclick="printPage(this, event);"
														styleClass="outputLinkEx" id="linkExPrintPageLink">
														<h:outputText id="textPrintPageLinkText"
															styleClass="outputText" value="#{labels.printPageLabel}"></h:outputText>
													</hx:outputLinkEx>
												</h:panelGrid>

												<hx:jspPanel id="jspPanelOrderDataPanel">
													<div id="orderFormBoxThankYou">
														<h:panelGrid styleClass="panelGridThankYouPage"
															id="gridAccountInformation"
															columnClasses="panelGridColNorm,panelGridColRightAlign"
															columns="2"
															rendered="#{customerHandler.customerDataAvailable}">
															<f:facet name="header">
																<h:panelGroup styleClass="panelGroup"
																	id="groupAccountInfoHeaderGroup">

																	<hx:panelLayout styleClass="panelLayout"
																		id="layoutOrderInfo1" width="95%" align="left">
																		<f:facet name="body"></f:facet>
																		<f:facet name="left">
																			<h:outputText styleClass="panelThankYouPage_Header"
																				id="textOrderInformationHeaderlabel"
																				value="#{labels.orderInformationLabel}"></h:outputText>
																		</f:facet>
																		<f:facet name="right"></f:facet>
																		<f:facet name="bottom"></f:facet>
																		<f:facet name="top"></f:facet>
																	</hx:panelLayout>
																</h:panelGroup>
															</f:facet>

															<h:outputText styleClass="outputText" id="textDateLabel"
																value="#{labels.DateLabel1}:"></h:outputText>

															<h:outputText styleClass="outputText"
																id="textDateOfToday"
																value="#{shoppingCartHandler.dateOrderPlaced}">
																<hx:convertDateTime />
															</h:outputText>

															<h:outputText styleClass="outputText"
																id="textSubStartDateLabel"
																value="#{labels.SubStartDate}:"></h:outputText>

															<h:outputText styleClass="outputText"
																id="textStartDateOfSub"
																value="#{shoppingCartHandler.startDateOfSub}">
																<hx:convertDateTime />
															</h:outputText>

															<h:outputText styleClass="outputText"
																id="textProductLabel"
																value="#{labels.SubscriptionProdNameLabel}:"></h:outputText>

															<h:outputText styleClass="outputText" id="textProdName"
																value="#{shoppingCartHandler.lastOrder.orderedItems[0].product.name}"></h:outputText>
															<h:outputText styleClass="outputText" id="textTermLabel"
																value="#{labels.termLabel2}:"></h:outputText>
															<h:outputText styleClass="outputText"
																id="textSelectedTerm"
																value="#{shoppingCartHandler.lastSubscriptionItemTermDuration} Months">

															</h:outputText>
															<h:outputText styleClass="outputText"
																id="textUnitPriceLabel" value="#{labels.UnitPrice}:"></h:outputText>
															<h:outputText styleClass="outputText" id="textUnitPrice"
																value="#{shoppingCartHandler.orderedSubscriptionItem.selectedTerm.price}">
																<hx:convertNumber type="currency" currencySymbol="$" />
															</h:outputText>
															<h:outputText styleClass="outputText" id="textEZPayLabel"
																value="EZ PAY:"></h:outputText>

															<h:outputText styleClass="outputText"
																id="textEZPayStatus"
																value="#{shoppingCartHandler.selectedEZPayString}">
															</h:outputText>
														</h:panelGrid>
														<h:panelGrid styleClass="panelGridThankYouPage"
															id="gridPremiumPromoGrid"
															rendered="#{shoppingCartHandler.showPremiumPromotionGrid}"
															columnClasses="panelGridColNorm,panelGridColRightAlign"
															columns="2">
															<h:outputText styleClass="outputText"
																id="textPremiumPromotionLabel" value="Promotion:"></h:outputText>
															<h:outputText styleClass="outputText"
																id="textPremiumPromotionDescriptionText"
																value="#{shoppingCartHandler.orderedSubscriptionItem.promotionalItems.descriptionText}">
															</h:outputText>

														</h:panelGrid>
														<!-- Delivery Information -->
														<h:panelGrid styleClass="panelGridThankYouPage"
															columnClasses="panelGridColNorm,panelGridColRightAlign"
															id="gridAccountInformationDelivery" columns="2"
															rendered="#{customerHandler.customerDataAvailable}">
															<f:facet name="header">
																<h:panelGroup styleClass="panelGroup"
																	id="groupDeliveryInfoGroup">
																	<hx:panelLayout styleClass="panelLayout"
																		id="layoutOrderInfo2" width="95%" align="left">
																		<f:facet name="body"></f:facet>
																		<f:facet name="left">
																			<h:outputText styleClass="panelThankYouPage_Header"
																				id="textOrderInformationDeliveryHeaderlabel"
																				value="#{labels.emailAddressRecipientLabel2}"></h:outputText>
																		</f:facet>
																		<f:facet name="right"></f:facet>
																		<f:facet name="bottom"></f:facet>
																		<f:facet name="top"></f:facet>
																	</hx:panelLayout>
																</h:panelGroup>
															</f:facet>

															<h:outputText styleClass="outputText"
																id="textDeliveryMethodLabel" value="Delivery Method:"
																rendered="#{shoppingCartHandler.showDeliveryMethod}"></h:outputText>

															<h:outputText styleClass="outputText"
																id="textDeliveryMethodDes"
																rendered="#{shoppingCartHandler.showDeliveryMethod}"
																value="#{shoppingCartHandler.deliveryMethodDescription}">
															</h:outputText>

															<h:outputText styleClass="outputText"
																id="textDeliveryEmailLabel"
																value="#{labels.EmailAddressLabel}:"></h:outputText>

															<h:outputText styleClass="outputText"
																id="textDeliveryEmail"
																value="#{shoppingCartHandler.lastOrder.deliveryContact.emailAddress}">
															</h:outputText>

															<h:outputText styleClass="outputText"
																id="textDeliveryNameLabel" value="#{labels.NameLabel}:"></h:outputText>
															<h:outputText styleClass="outputText"
																id="textDeliveryName"
																value="#{shoppingCartHandler.lastOrder.deliveryContact.firstName} #{shoppingCartHandler.lastOrder.deliveryContact.lastName}">
															</h:outputText>

															<h:outputText styleClass="outputText"
																id="textDeliveryCompanyNameLabel"
																value="#{labels.companyNameLabel}:"
																rendered="#{!empty shoppingCartHandler.lastOrder.deliveryContact.firmName}"></h:outputText>
															<h:outputText styleClass="outputText"
																id="textDeliveryCompanyName"
																value="#{shoppingCartHandler.lastOrder.deliveryContact.firmName}"
																rendered="#{!empty shoppingCartHandler.lastOrder.deliveryContact.firmName}">
															</h:outputText>

															<h:outputText styleClass="outputText"
																id="textDeliveryAddr1Label"
																value="#{labels.AddressLabel}:"></h:outputText>
															<h:outputText styleClass="outputText"
																id="textDeliveryAddr1"
																value="#{shoppingCartHandler.lastOrder.deliveryContact.UIAddress.address1}">
															</h:outputText>
															<h:outputText styleClass="outputText"
																id="textDeliveryAptSuiteLabel"></h:outputText>
															<h:outputText styleClass="outputText"
																id="textDeliveryAptSuite"
																value="#{shoppingCartHandler.lastOrder.deliveryContact.UIAddress.aptSuite}">
															</h:outputText>
															<h:outputText styleClass="outputText"
																id="textDeliveryAddr2Label"
																rendered="#{!empty shoppingCartHandler.lastOrder.deliveryContact.UIAddress.address2}">
															</h:outputText>
															<h:outputText styleClass="outputText"
																id="textDeliveryAddr2"
																value="#{shoppingCartHandler.lastOrder.deliveryContact.UIAddress.address2}"
																rendered="#{!empty shoppingCartHandler.lastOrder.deliveryContact.UIAddress.address2}">
															</h:outputText>

															<h:outputText styleClass="outputText"
																id="textDeliveryAddrStateZipLabel"></h:outputText>
															<h:outputText styleClass="outputText"
																id="textDeliveryStateZip"
																value="#{shoppingCartHandler.lastOrder.deliveryContact.UIAddress.city}, #{shoppingCartHandler.lastOrder.deliveryContact.UIAddress.state} #{shoppingCartHandler.lastOrder.deliveryContact.UIAddress.zip}">
															</h:outputText>

															<h:outputText styleClass="outputText"
																id="textDeliveryPhoneLabel"
																value="#{labels.phoneLabel}:"></h:outputText>
															<h:outputText styleClass="outputText"
																id="textDeliveryPhone"
																value="#{shoppingCartHandler.lastOrder.deliveryContact.homePhone}">
																<hx:convertMask mask="###-###-####" />
															</h:outputText>
															<h:outputText styleClass="outputText"
																id="textDeliveryWorkPhoneLabel"
																value="#{labels.workPhoneLabel}:"
																rendered="#{!empty shoppingCartHandler.lastOrder.deliveryContact.businessPhone}"></h:outputText>
															<h:outputText styleClass="outputText"
																id="textDeliveryWorkPhone"
																value="#{shoppingCartHandler.lastOrder.deliveryContact.businessPhone}"
																rendered="#{!empty shoppingCartHandler.lastOrder.deliveryContact.businessPhone}">
																<hx:convertMask mask="###-###-####" />
															</h:outputText>

														</h:panelGrid>

														<!-- Payment Information -->
														<h:panelGrid styleClass="panelGridThankYouPage"
															columnClasses="panelGridColNorm,panelGridColRightAlign"
															id="gridAccountInformationPayment" columns="2"
															rendered="#{shoppingCartHandler.paidByCreditCard}">
															<f:facet name="header">
																<h:panelGroup styleClass="panelGroup"
																	id="groupPaymentInfoGroup">
																	<hx:panelLayout styleClass="panelLayout"
																		id="layoutOrderInfo3" width="95%" align="left">
																		<f:facet name="body"></f:facet>
																		<f:facet name="left">
																			<h:outputText styleClass="panelThankYouPage_Header"
																				id="textOrderInformationPaymentHeaderlabel"
																				value="#{labels.paymentInfo2}"></h:outputText>
																		</f:facet>
																		<f:facet name="right"></f:facet>
																		<f:facet name="bottom"></f:facet>
																		<f:facet name="top"></f:facet>
																	</hx:panelLayout>
																</h:panelGroup>
															</f:facet>
															<h:outputText styleClass="outputText"
																id="textSubTotalAmountLabel"
																value="#{labels.OrderSubTotal}:"></h:outputText>
															<h:outputText styleClass="outputText"
																id="textSubTotalChargeAmount"
																value="#{shoppingCartHandler.lastOrder.subTotal}">
																<hx:convertNumber type="currency" currencySymbol="$" />
															</h:outputText>
															<h:outputText styleClass="outputText"
																id="textTaxAmountLabel" value="#{labels.OrderTaxes}:"></h:outputText>
															<h:outputText styleClass="outputText"
																id="textChargeTaxAmount"
																value="#{shoppingCartHandler.lastOrder.taxAmount}">
																<hx:convertNumber type="currency" currencySymbol="$" />
															</h:outputText>
															<h:outputText styleClass="outputText"
																id="textChargeAmountLabel" value="#{labels.OrderTotal}:"></h:outputText>
															<h:outputText styleClass="outputText"
																id="textChargeAmount"
																value="#{shoppingCartHandler.lastOrder.total}">
																<hx:convertNumber type="currency" currencySymbol="$" />
															</h:outputText>
															<h:outputText styleClass="outputText"
																id="textPaymentMethodLabel"
																value="#{labels.creditCardTypeLabel}:"></h:outputText>
															<h:outputText styleClass="outputText"
																id="textPaymentMethod"
																value="#{shoppingCartHandler.cart.paymentMethod.label}">
															</h:outputText>
															<h:outputText styleClass="outputText"
																id="textPaymentCardNumberLabel"
																value="#{labels.Number}:"></h:outputText>
															<h:outputText styleClass="outputText"
																id="textPaymentCardNumber"
																value="#{shoppingCartHandler.cart.paymentMethod.displayValue}">
															</h:outputText>

														</h:panelGrid>
														<h:panelGrid styleClass="panelGridThankYouPage"
															id="gridAccountInformationPaymentBillMe" columns="1"
															rendered="#{not shoppingCartHandler.paidByCreditCard }">
															<f:facet name="header">
																<h:panelGroup styleClass="panelGroup"
																	id="groupPaymentInfoBillMeGroup">
																	<hx:panelLayout styleClass="panelLayout"
																		id="layoutOrderInfo4" width="95%" align="left">
																		<f:facet name="body"></f:facet>
																		<f:facet name="left">
																			<h:outputText styleClass="panelThankYouPage_Header"
																				id="textOrderInformationPaymentBillMeHeaderlabel"
																				value="#{labels.paymentInfo2}"></h:outputText>
																		</f:facet>
																		<f:facet name="right"></f:facet>
																		<f:facet name="bottom"></f:facet>
																		<f:facet name="top"></f:facet>
																	</hx:panelLayout>
																</h:panelGroup>
															</f:facet>
															<h:panelGrid id="billMeOrderDetails"
																styleClass="panelGridThankYouPage"
																columnClasses="panelGridColNorm,panelGridColRightAlign"
																columns="2">
																<h:outputText styleClass="outputText"
																	id="textSubTotalAmountLabelBillMe"
																	value="#{labels.OrderSubTotal}:"></h:outputText>
																<h:outputText styleClass="outputText"
																	id="textSubTotalChargeAmountBillMe"
																	value="#{shoppingCartHandler.lastOrder.subTotal}">
																	<hx:convertNumber type="currency" currencySymbol="$" />
																</h:outputText>
																<h:outputText styleClass="outputText"
																	id="textTaxAmountLabelBillMe"
																	value="#{labels.OrderTaxes}:"></h:outputText>
																<h:outputText styleClass="outputText"
																	id="textChargeTaxAmountBillMe"
																	value="#{shoppingCartHandler.lastOrder.taxAmount}">
																	<hx:convertNumber type="currency" currencySymbol="$" />
																</h:outputText>
																<h:outputText styleClass="outputText"
																	id="textChargeAmountLabelBillMe"
																	value="#{labels.OrderTotal}:"></h:outputText>
																<h:outputText styleClass="outputText"
																	id="textChargeAmountBillMe"
																	value="#{shoppingCartHandler.lastOrder.total}">
																	<hx:convertNumber type="currency" currencySymbol="$" />
																</h:outputText>
															</h:panelGrid>
															<h:outputText styleClass="outputText" id="textBeInfoText"
																value="#{labels.BilledDescription}"></h:outputText>

														</h:panelGrid>
														<!-- billing information -->
														<h:panelGrid styleClass="panelGridThankYouPage"
															columnClasses="panelGridColNorm,panelGridColRightAlign"
															id="gridAccountInformationBilling" columns="2"
															rendered="#{not shoppingCartHandler.lastOrder.billingSameAsDelivery }">
															<f:facet name="header">
																<h:panelGroup styleClass="panelGroup"
																	id="groupBillingHeaderGroup">
																	<hx:panelLayout styleClass="panelLayout"
																		id="layoutOrderInfo5" width="95%" align="left">
																		<f:facet name="body"></f:facet>
																		<f:facet name="left">
																			<h:outputText styleClass="panelThankYouPage_Header"
																				id="textOrderInformationBillingHeaderlabel"
																				value="#{labels.BillingAddressLabel}"></h:outputText>
																		</f:facet>
																		<f:facet name="right"></f:facet>
																		<f:facet name="bottom"></f:facet>
																		<f:facet name="top"></f:facet>
																	</hx:panelLayout>

																</h:panelGroup>
															</f:facet>
															<h:outputText styleClass="outputText"
																id="textBillingEmailLabel"
																value="#{labels.EmailAddressLabel}:"></h:outputText>

															<h:outputText styleClass="outputText"
																id="textBillingEmail"
																value="#{shoppingCartHandler.lastOrder.billingContact.emailAddress}">
															</h:outputText>

															<h:outputText styleClass="outputText"
																id="textBillingNameLabel" value="#{labels.NameLabel}:"></h:outputText>
															<h:outputText styleClass="outputText"
																id="textBillingName"
																value="#{shoppingCartHandler.lastOrder.billingContact.firstName} #{shoppingCartHandler.lastOrder.billingContact.lastName}">
															</h:outputText>

															<h:outputText styleClass="outputText"
																id="textBillingCompanyLabel"
																rendered="#{!empty shoppingCartHandler.lastOrder.billingContact.firmName}"
																value="#{labels.companyNameLabel}:"></h:outputText>
															<h:outputText styleClass="outputText"
																id="textBillingCompany"
																rendered="#{!empty shoppingCartHandler.lastOrder.billingContact.firmName}"
																value="#{shoppingCartHandler.lastOrder.billingContact.firmName}">
															</h:outputText>

															<h:outputText styleClass="outputText"
																id="textBillingAddr1Label"
																value="#{labels.AddressLabel}:"></h:outputText>
															<h:outputText styleClass="outputText"
																id="textBillingAddr1"
																value="#{shoppingCartHandler.lastOrder.billingContact.UIAddress.address1}">
															</h:outputText>
															<h:outputText styleClass="outputText"
																id="textBillingAptSuiteLabel"></h:outputText>
															<h:outputText styleClass="outputText"
																id="textBillingAptSuite"
																value="#{shoppingCartHandler.lastOrder.billingContact.UIAddress.aptSuite}">
															</h:outputText>
															<h:outputText styleClass="outputText"
																id="textBillingAddr2Label"
																rendered="#{!empty shoppingCartHandler.lastOrder.billingContact.UIAddress.address2}"></h:outputText>
															<h:outputText styleClass="outputText"
																id="textBillingAddr2"
																value="#{shoppingCartHandler.lastOrder.billingContact.UIAddress.address2}"
																rendered="#{!empty shoppingCartHandler.lastOrder.billingContact.UIAddress.address2}">
															</h:outputText>

															<h:outputText styleClass="outputText"
																id="textBillingAddrStateZipLabel"></h:outputText>
															<h:outputText styleClass="outputText"
																id="textBillingStateZip"
																value="#{shoppingCartHandler.lastOrder.billingContact.UIAddress.city}, #{shoppingCartHandler.lastOrder.billingContact.UIAddress.state} #{shoppingCartHandler.lastOrder.billingContact.UIAddress.zip}">
															</h:outputText>

															<h:outputText styleClass="outputText"
																id="textBillingPhoneLabel"
																value="#{labels.billPhoneLabel}:"
																rendered="#{!empty shoppingCartHandler.lastOrder.billingContact.homePhone}"></h:outputText>
															<h:outputText styleClass="outputText"
																id="textBillingPhone"
																value="#{shoppingCartHandler.lastOrder.billingContact.homePhone}"
																rendered="#{!empty shoppingCartHandler.lastOrder.billingContact.homePhone}">
																<hx:convertMask mask="###-###-####" />
															</h:outputText>
														</h:panelGrid>
														<h:panelGrid styleClass="panelGridThankYouPage"
															id="gridAccountEZPayInformation" columns="1"
															rendered="#{shoppingCartHandler.orderedSubscriptionItem.choosingEZPay}">
														<h:outputText styleClass="outputText" id="textEZPayInfoText"
															value="#{labels.EZPayThankYouNote} #{currentOfferHandler.EZPayDisclaimer}">
														</h:outputText>

<%-- 															<h:outputText styleClass="outputText fineprint" --%>
<!-- 																id="textEZPayInfoText" -->
<%-- 																value="#{labels.EZPayThankYouInfo}"></h:outputText> --%>

														</h:panelGrid>

													</div>
												</hx:jspPanel>

											</h:panelGrid>
										</div>
										<div id="pageContentEEOrderEntryRight">

											<h:panelGrid styleClass="panelGrid" id="gridRightPanelGrid"
												cellpadding="0" cellspacing="0" columns="1">
												<div>
													<f:verbatim>
														<h:outputText styleClass="outputText"
															id="textThankYouCommJunc1" escape="false"
															value="#{shoppingCartHandler.commissionJunctionTrackingPixel}"></h:outputText>
														<h:outputText styleClass="outputText" id="textThankYouJS1"
															escape="false"
															value="#{shoppingCartHandler.onePageThankYouJavaScript1Text}"></h:outputText>
														<h:outputText styleClass="outputText" id="textThankYouJS2"
															escape="false"
															value="#{shoppingCartHandler.onePageThankYouJavaScript2Text}"></h:outputText>
														<h:outputText styleClass="outputText" id="textThankYouJS3"
															escape="false"
															value="#{shoppingCartHandler.onePageThankYouJavaScript3Text}"></h:outputText>
														<h:outputText styleClass="outputText" id="textThankYouJS4"
															escape="false"
															value="#{shoppingCartHandler.onePageThankYouJavaScript4Text}"></h:outputText>
														<h:outputText styleClass="outputText" id="textThankYouJS5"
															escape="false"
															value="#{shoppingCartHandler.onePageThankYouJavaScript5Text}"></h:outputText>
														<h:outputText styleClass="outputText" id="textThankYouJS6"
															escape="false"
															value="#{shoppingCartHandler.onePageThankYouJavaScript6Text}"></h:outputText>
														<h:outputText styleClass="outputText" id="textThankYouJS7"
															escape="false"
															value="#{shoppingCartHandler.onePageThankYouJavaScript7Text}"></h:outputText>
														<h:outputText styleClass="outputText" id="textThankYouJS8"
															escape="false"
															value="#{shoppingCartHandler.onePageThankYouJavaScript8Text}"></h:outputText>
														<h:outputText styleClass="outputText" id="textThankYouJS9"
															escape="false"
															value="#{shoppingCartHandler.onePageThankYouJavaScript9Text}"></h:outputText>
														<h:outputText styleClass="outputText"
															id="textThankYouJS12" escape="false"
															value="#{shoppingCartHandler.onePageThankYouJavaScript12Text}"></h:outputText>
													</f:verbatim>
												</div>
											</h:panelGrid>
										</div>
									</h:form>
								</hx:scriptCollector>
							<%-- /tpl:put --%>
							<%=trackingBug.getOmnitureHTML()%>
							<!-- END THIRD PARTY CONTENT -->

						</div>
						<!-- 						<div class="sh_bottom"></div>
						<div class="pageinfo data hidden">{ "asset_collection":
							[], "aws": "", "aws_id": "", "ssts": "", "blogname": "",
							"contenttype":" ", "seotitletag": "", "templatename":
							"fronts/default", "videoincluded":"" }</div>
 -->
					</section>
				</section>
			</div>
		</article>
	</body>
</f:view>
</html><%-- /tpl:insert --%>