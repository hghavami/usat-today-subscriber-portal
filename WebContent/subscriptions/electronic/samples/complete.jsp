<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/subscriptions/electronic/samples/Complete.java" --%><%-- /jsf:pagecode --%>
<%-- tpl:insert page="/theme/themeV3/jsfTheme/subscriberPortalNoNav_JSF.jtpl" --%><!DOCTYPE HTML><%@page import="com.usatoday.business.interfaces.products.ProductIntf"%>
<%@page import="com.usatoday.businessObjects.products.ProductBO"%>
<%@page import="com.usatoday.util.MobileDeviceDetectionUtil"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%><%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%><%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%><f:view><f:loadBundle basename="resourceFile" var="labels" />
	<%-- tpl:insert page="/theme/subscriptionPortalRoot.htpl" --%><HTML>
<HEAD>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8">
<%-- tpl:put name="headerareaTop" --%>
<%
	String docProtocol = "https:";
	if (!request.isSecure()) {
		docProtocol = "http:"; 
	}
	
	// change this to any javascript you want called on page load
	String initFunction = "";
	
	com.usatoday.esub.common.WebBug trackingBug = com.usatoday.esub.common.UTCommon.getWebAnalyticsBug();
	try {
		trackingBug.deriveValuesFromRequest(request);
	}
	catch (Exception e) { 
		; // ignore
	}
	
	String pubCode = "";
	String productName = "USA TODAY";
	
    boolean isEEditionProduct = true;

	com.usatoday.businessObjects.products.promotions.PromotionSet defaultPromotionSet = null;
	com.usatoday.businessObjects.products.promotions.PromotionSet currentOfferPromotionSet = null;
	com.usatoday.business.interfaces.customer.CustomerIntf customer = null;
   	com.usatoday.businessObjects.products.promotions.PromotionSet currentAccountPromotionSet = null;
   		
	com.usatoday.business.interfaces.products.OfferIntf offer = null;   		
	try {
		offer = com.usatoday.esub.common.UTCommon.getCurrentOfferVersion2(request);
		pubCode = com.usatoday.util.constants.UsaTodayConstants.UT_PUBCODE;
		if (offer != null) {
			pubCode = offer.getPubCode();
			try {
				String brandingPubCode = offer.getProduct().getBrandingPubCode();
				ProductIntf bp = ProductBO.fetchProduct(brandingPubCode);
				productName = bp.getName();
			}
			catch (Exception teee) {
			}							
		}
		// get the default promotion set for the publication		
		defaultPromotionSet = com.usatoday.esub.common.UTCommon.getDefaultPromotionSetForPub(pubCode);
		
		// get the keycode specific promotion set
		
		if (offer != null && offer instanceof com.usatoday.business.interfaces.products.SubscriptionOfferIntf) {
			currentOfferPromotionSet = ((com.usatoday.business.interfaces.products.SubscriptionOfferIntf)offer).getPromotionSet();
		}
		boolean isSignedIn = false;

		customer = (com.usatoday.business.interfaces.customer.CustomerIntf)session.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_CUSTOMER_INFO);
		
		if (customer != null) {
			isSignedIn = true;
			com.usatoday.business.interfaces.products.SubscriptionProductIntf product = com.usatoday.businessObjects.products.SubscriptionProductBO.getSubscriptionProduct(customer.getCurrentEmailRecord().getPubCode());
			isEEditionProduct = product.isElectronicDelivery(); 
			// This promotion set is based on pub and keycode on the customer account
			currentAccountPromotionSet = com.usatoday.businessObjects.products.promotions.PromotionManager.getInstance().getPromotionsForOffer(customer.getCurrentAccount().getPubCode(), customer.getCurrentAccount().getKeyCode());
			
		}
	} catch (Exception e) {
	
	}

	String mobileCSS = "";
	if (MobileDeviceDetectionUtil.isSmallScreenDevice(request)) {
		mobileCSS = "<LINK href=\"/theme/themeV3/css/smallScreen.css\" rel=\"stylesheet\" type=\"text/css\">";
	}
%>
<script src="/common/prototype/prototype.js"></script>
<script src="/common/prototype/scriptaculous/scriptaculous.js" type="text/javascript"></script>
<link rel="stylesheet"	href="/theme/themeV3/css/stylesheet_jsfcomponents.css"	type="text/css">
<%-- tpl:put name="headerarea_Top" --%><title>Sample Subscription - Thank You</title><%-- /tpl:put --%>
<%-- /tpl:put --%>
<LINK REL="SHORTCUT ICON" HREF="/favicon.ico">
<LINK REL="icon" href="/favicon.ico" TYPE="image/ico" />
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META http-equiv="Content-Style-Type" content="text/css">
<meta name="viewport" content="initial-scale=1.0">
<LINK href="/theme/themeV3/css/usatodayBase.css" rel="stylesheet"
	type="text/css">
<LINK href="/theme/themeV3/css/usatodayAddOn.css" rel="stylesheet"
	type="text/css">
<script src="/common/usatCommon.js"></script>
<%-- tpl:put name="headareaBottom" --%>
<%=mobileCSS %>
<%-- tpl:put name="headarea_Bottom" --%>
<script language="JavaScript" src="/common/eEditionScripts.js"></script>
			
<script type="text/javascript">
function printPage(thisObject, thisEvent) {
	window.print();
	return false;
}

function func_1(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	return openReaderWindow();
}</script>
<%-- /tpl:put --%>
<script>
// method called on page load
function initPage(thisObj, thisEvent) {
	// insert site-wide code here
	
	// for product messageing in popUpOverlay.js
	usat_productName = "<%=productName %>";
	
	<%=initFunction %>
}
</script>
<%-- /tpl:put --%>
<script type="text/javascript">
	/*   var _gaq = _gaq || [];
	 _gaq.push(['_setAccount', 'UA-23158809-1']);
	 _gaq.push(['_trackPageview']);

	 (function() {
	 var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	 ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	 var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	 })();
	 */
</script>
</HEAD>
<body>
	<DIV id="wrapper">
		<DIV id="header">
			<div id="headerLeft" onclick="templateLogoClick();"></div>
			<div id="headerRight">
				<%-- tpl:put name="headerbodyarea" --%>
					<%-- tpl:insert page="/login/loginform.jspf" --%><div style="float: none">
	<%
		String uri = request.getRequestURI();
		String pageName = uri.substring(uri.lastIndexOf("/") + 1);
		String jScript = "";
		if (uri.indexOf("subscriptions") > 0 && uri.indexOf("thankyou.jsp") > 0) {
			try {
				com.usatoday.esub.handlers.ShoppingCartHandler shoppingCartHandler = (com.usatoday.esub.handlers.ShoppingCartHandler) session
						.getAttribute("shoppingCartHandler");
				if (shoppingCartHandler != null) {
					jScript = shoppingCartHandler.getOnePageThankYouJavaScriptTealiumText();
				}
			} catch (Exception E) {

			}
		} else {
			com.usatoday.businessObjects.products.promotions.PromotionSet currentOfferPromotionSetTealium = null;
			com.usatoday.business.interfaces.products.OfferIntf offerTealium = com.usatoday.esub.common.UTCommon
					.getCurrentOffer(request);
			if (offerTealium != null && offerTealium instanceof com.usatoday.business.interfaces.products.SubscriptionOfferIntf) {
				currentOfferPromotionSetTealium = ((com.usatoday.business.interfaces.products.SubscriptionOfferIntf) offerTealium)
						.getPromotionSet();
				jScript = currentOfferPromotionSetTealium.getPromoOnePageJavaScriptTealiumText();
				jScript = org.apache.commons.lang3.StringUtils.replace(jScript, "page_name:\"\"", "page_name:\"" + uri + "\"");
			}
		}
	%>
	<%=jScript%>
</div>
<%-- 			<f:verbatim>
				<h:outputText styleClass="outputText" id="textThankYouJS13"	escape="false" value="#{shoppingCartHandler.tealiumOnePageThankYouJavaScriptText}"></h:outputText>
			</f:verbatim>
 --%>
<%
	String isLoginDisplay = "true";
	com.usatoday.business.interfaces.customer.CustomerIntf customer2 = null;
	String loggedinDivClass = "hideMe";
	String notLoggedinDivClass = "showMeInline";
	String firstName = "";
	String lastName = "";
	String firmInfo = "";
	String accountNum = "";
	String loginErrorMessage2 = "";
	String logOut = "";
	String publication = "";
	String changeAccountLink = "";
	String earlyAlertDisplay = "display:none";

	String jScriptNoOp = "javascript:;";

	try {
		publication = (String) session.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_PUB_NAME);
		if (publication == null) {
			publication = request.getParameter("pub");
			if (publication == null) {
				publication = com.usatoday.util.constants.UsaTodayConstants.UT_PUBCODE;
			}
		}
		if (publication.equals(com.usatoday.esub.common.UTCommon.PUB_NAMES[0])) { // USATODAY publication
			logOut = com.usatoday.esub.common.UTCommon.BASE_URL + "/index.jsp?pub=UT&logsessobj=rem";
		} else {
			logOut = com.usatoday.esub.common.UTCommon.BASE_URL + "/index.jsp?pub=BW&logsessobj=rem";
		}

		// If customer is logged in remove login page, otherwise display it
		customer2 = (com.usatoday.business.interfaces.customer.CustomerIntf) session
				.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_CUSTOMER_INFO);
		if (customer2 == null) {
			// showMeInling and hideMe are defined in style sheets
			loggedinDivClass = "showMeInline";
			notLoggedinDivClass = "hideMe";
		} else {

			if (customer2.getCurrentAccount() != null) {
				firstName = customer2.getCurrentAccount().getDeliveryContact().getFirstName();
				if (firstName != null && !firstName.trim().equals("")) {
					firstName = com.usatoday.esub.common.UTCommon.formatNameCapitalization(firstName); // Format it
				}
				lastName = customer2.getCurrentAccount().getDeliveryContact().getLastName();
				if (lastName != null && !lastName.trim().equals("")) {
					lastName = com.usatoday.esub.common.UTCommon.formatNameCapitalization(lastName); // Format it
				}
				firmInfo = customer2.getCurrentAccount().getDeliveryContact().getFirmName();
				if (customer2.getNumberOfAccounts() > 1) {
					changeAccountLink = "[&nbsp;<a href=\"/account/select_accnt.jsp\">Change</a>&nbsp;]";
				}
				try {
					accountNum = customer2.getCurrentAccount().getAccountNumber();
					//				if (accountNum == null || accountNum.length() < 4) {
					if (accountNum == null) {
						accountNum = "";
					}
					//				else {
					/*
					if (accountNum.equalsIgnoreCase("340000000")) {
						accountNum = "Pending Setup";
					}
					 */
					//				}
				} catch (Exception eeee) {
					eeee.printStackTrace();
				}

				//EarlyAlert - Check to see if their paper delivery has been affected by a delay
				// HD-CONS109 SEW
				/*
				if (customer2.getCurrentAccount().isShowDeliveryAlert()) {
				earlyAlertDisplay = "display:inline";
				}
				 */

			} else {
				if (customer2.getCurrentEmailRecord() != null
						&& customer2.getCurrentEmailRecord().getEmailAddress() != null) {
					firstName = customer2.getCurrentEmailRecord().getEmailAddress();
					accountNum = "Pending Setup";
				} else {
					firstName = "";
				}
			}
		}
		loginErrorMessage2 = (String) session.getAttribute("LOGIN_FAILED_MESSAGE");
		if (loginErrorMessage2 == null || loginErrorMessage2.trim().equals("")) {
			loginErrorMessage2 = "";
		} else {
			session.removeAttribute("LOGIN_FAILED_MESSAGE");
		}
	} catch (Exception e) {
		e.printStackTrace();
		if (firstName == null) {
			firstName = "";
		}
		if (lastName == null) {
			lastName = "";
		}
	}
	String nameInfo = firstName + " " + lastName;
	String loginInfo = null;
	if (!nameInfo.trim().equals("")) {
		loginInfo = nameInfo;
	} else {
		if (firmInfo != null && !firmInfo.trim().equals("")) {
			loginInfo = firmInfo;
		}
	}
	// if no login info, then hide data
	if (loginInfo == null) {
		loggedinDivClass = "showMeInline";
		notLoggedinDivClass = "hideMe";
		loginInfo = "";
	}
%>

<%@page import="com.usatoday.util.constants.UsaTodayConstants"%><div
	id="loginDisplayDiv" class="<%=loggedinDivClass%>">
	<table border="0" width="100%" height="60px">
		<tr>
			<td>
				<TABLE class="loginFormTable">
					<TBODY>
						<TR>
							<TD class="loginFormColumn"><br /> <a
								href="/login/auth.faces">Login to Manage Account</a></TD>
						</TR>
						<tr>
							<td class="loginFormColumn" align="right"><img
								style="cursor: pointer; margin-top: 10px;"
								onclick="javascript: templateClickToChatClickedGuest(this, event);"
								alt="Click for Assistance" title="Click for Assistance"
								src="${pageContext.request.contextPath}/theme/themeV3/themeImages/chatIcon136.png"
								hspace="0" vspace="5"></td>
						</tr>
					</TBODY>
				</TABLE></td>
		</tr>
	</table>
</div>
<div id="loginHideDiv" class="<%=notLoggedinDivClass%>">
	<table width="100%" border="0" class="loginFormTable">
		<tr>
			<td nowrap align="right">
				<TABLE class="loginFormTable">
					<TBODY>
						<TR>
							<TD class="loginFormColumn" nowrap><span
								class="loginNormalPrint">Hello, <%=loginInfo%>&nbsp;&nbsp;[&nbsp;<a
									href="<%=jScriptNoOp%>"
									onclick="javascript:document.location='<%=logOut%>';_unloadPage(this, event);">Logout</a>&nbsp;]</span>
							</TD>
						</TR>
					</TBODY>
				</TABLE></TD>
		</tr>
		<tr>
			<td class="loginFormColumn" align="right"><span
				class="loginfineprint">Account #:&nbsp;<%=accountNum%>&nbsp;<%=changeAccountLink%></span>
			</td>
		</tr>
		<tr>
			<td class="loginFormColumn" align="right"><div
					id="chatIconRequestDiv">
					<img onclick="javascript: templateClickToChatClicked(this, event);"
						style="cursor: pointer; margin-top: 10px;"
						alt="Click for Assistance" title="Click for Assistance"
						src="${pageContext.request.contextPath}/theme/themeV3/themeImages/chatIcon136.png"
						hspace="0" vspace="5">
				</div>
			</td>
		</tr>
	</table>
</div>
<div id="templateChatWindowDiv" style="display: none;">
	<div id="templateChatTopDiv">
		<span onclick="templateClickToChatHide();" style="cursor: pointer;">No
			Thanks<img src="/theme/themeV3/themeImages/close_14x14.png"
			width="14" height="14" border="0"
			style="margin-left: 5px; margin-right: 5px" /> </span>
	</div>
<!-- 	<div id="tempateChatImageDiv">
		<a
			onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
			href=""><img border="0"
			src="https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY" />
		</a>
	</div>
 -->	<div id="templateChatAccountLinksDiv">
		<ul>
			<li>To report a problem with delivery, click <a
				href="/account/cp/delissue.faces"> here</a>.</li>
			<li>To place your delivery on a temporary hold, click <a
				href="/account/vacationholds/holds.faces"> here</a>.</li>
			<li>Forgot your password or login? Click <a
				href="/idpassword/forgotpassword.jsp"> here</a>.
			<li>To contact us <a href="/feedback/feedback.jsp"> click here</a>.</li>				
<!-- 			<li>To cancel your subscription, please call 1-800-872-0001 and -->
<!-- 				select option 6.</li> -->
		</ul>
	</div>

	<div id="templateChatLinksDiv">
		<ul>

			<li><a
				onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
				href="">Click to Chat</a>
			</li>
		</ul>
	</div>
	<div id="templateChatBottomDiv">
		<span class="outputText">Or, call us at:<br> <b>Call
				1-877-713-6241</b><br>(M-F 8:00 a.m. to 7 p.m. E.T.)</span>
	</div>
</div>
<div id="templateChatWindowGuestDiv" style="display: none;">
	<div id="templateChatTopDiv">
		<span onclick="templateClickToChatHideGuest();"
			style="cursor: pointer;">No Thanks<img
			src="/theme/themeV3/themeImages/close_14x14.png" width="14"
			height="14" border="0" style="margin-left: 5px; margin-right: 5px" />
		</span>
	</div>
<!-- 	<div id="tempateChatImageDiv">
		<a
			onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
			href=""><img border="0"
			src="https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY" />
		</a>
	</div>
 -->	<div id="templateChatAccountLinksDiv">
		<ul>
			<li>To manage your account online - including reporting a
				problem with delivery or placing a temporary hold on your account -
				log in by entering your e-mail address and password. Click <a
				href="/login/auth.faces"> here</a> to access the login page now.</li>
			<li>Forgot your password or login? <a
				href="/idpassword/forgotpassword.jsp"> click here</a>.</li>
			<li>To contact us <a href="/feedback/feedback.jsp"> click here</a>.</li>
			<!-- 			<li>To cancel your subscription, please call 1-800-872-0001 and -->
			<!-- 				select option 6.</li> -->
		</ul>

	</div>

	<div id="templateChatLinksDiv">
		<ul>
			<li><a
				onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
				href="">Click to Chat</a>
			</li>
		</ul>
	</div>
	<div id="templateChatBottomDiv">
		<span class="outputText">Or, call us at:<br> <b>Call
				1-877-713-6241</b><br>(M-F 8:00 a.m. to 7 p.m. E.T.)</span>
	</div>
</div>
<%-- /tpl:insert --%>

		<%-- /tpl:put --%>
			</div>
		</DIV>
		<!-- end header -->
		<DIV id="content">
			<DIV id="whitebox">
				<%-- tpl:put name="bodyAreaMainWhiteBox" --%>
<DIV id="pageContentFullWidth">
			<%-- tpl:put name="mainBodyArea" --%>
			<hx:scriptCollector id="scriptCollectorSampleComplete">
				<h:form styleClass="form" id="formSampleComplete">
				<h1 style="margin-left: 4px"><h:outputText id="textMainTableHeader" value="#{labels.ThankYouLabel}" styleClass="H1Class"></h:outputText></h1>
				
					<div id="pageContent70">
						<h:panelGrid styleClass="panelGrid" id="gridThankYouInformation"
						columns="1" cellpadding="2" cellspacing="2" width="385">
						
						<hx:jspPanel id="jspPanelThankYouTextPanel">
							<ul>
							   <li><h:outputText styleClass="" id="textThankYouText2"
									value="#{labels.ThankYouSample1Text}"></h:outputText>&nbsp; 
							   <hx:outputLinkEx onclick="return func_1(this, event);"
									value="#{trialCustomerHandler.currentInstance.electronicReadNowURL}"
									styleClass="outputLinkEx" id="linkExReadNowTextLink">
									<h:outputText id="textReadNowTextLink" styleClass="outputText"
										value="click here"></h:outputText>
								</hx:outputLinkEx>.</li>
							   <li><h:outputText styleClass="" id="textThankYouBullet2a"
									value="#{labels.thankYouText2a}"></h:outputText>&nbsp;
							   <hx:outputLinkEx
									value="http://service.usatoday.com/login/trialLogin.faces"
									styleClass="outputLinkEx" id="linkExCustServiceLink">
									<h:outputText id="textCustServiceLink" styleClass="outputText"
										value="www.usatodayservice.com"></h:outputText>
								</hx:outputLinkEx>&nbsp;<h:outputText styleClass=""
									id="textThankYouBullet2b" value="#{labels.thankYouText2b}"></h:outputText></li>
					           <li><h:outputText id="textThankYouText3"
									value="#{labels.thankYouText3}"></h:outputText></li>
						       <li><h:outputText id="textThankYouText4" value="#{labels.thankYouText4}"></h:outputText></li>
						     </ul>						
						</hx:jspPanel>
						<hx:jspPanel id="jspPanelPartnerTextPanel">
							<h:outputText styleClass="outputText" id="textCustomPartnerText" escape="false" value="#{trialPartnerHandler.partner.completePageCustomHTML}" style="margin-left: 5px"></h:outputText>

						</hx:jspPanel>

						<h:panelGrid styleClass="panelGrid" id="gridPrintButtonGrid"
							width="300" cellpadding="0" cellspacing="0"
							style="text-align: left">
							<hx:outputLinkEx value="#" onclick="printPage(this, event);"
								styleClass="outputLinkEx" id="linkExPrintPageLink"
								style="margin-left: 10px; font-size: 10pt">
								<h:outputText id="textPrintPageLinkText" styleClass="outputText"
									value="#{labels.printPageLabel}"></h:outputText>
							</hx:outputLinkEx>
						</h:panelGrid>

						<hx:jspPanel id="jspPanelOrderDataPanel">
							<div id="orderFormBoxThankYou">
							<h:panelGrid styleClass="panelGrid" id="gridAccountInformation" columns="2" width="300" style="margin-left: 5px; font-family: Arial; font-size: 11px">
								<f:facet name="header">
									<h:panelGroup styleClass="panelGroup" id="groupAccountInfoHeaderGroup" style="text-align: left">
										
										<hx:panelLayout styleClass="panelLayout" id="layoutOrderInfo1" width="290" align="left">
											<f:facet name="body"></f:facet>
											<f:facet name="left">
												<h:outputText styleClass="panelThankYouPage_Header"
													id="textOrderInformationHeaderlabel"
													value="#{labels.orderInformationLabel}"></h:outputText>
											</f:facet>
											<f:facet name="right"></f:facet>
											<f:facet name="bottom"></f:facet>
											<f:facet name="top"></f:facet>
										</hx:panelLayout>
									</h:panelGroup>
								</f:facet>
								
								<h:outputText styleClass="outputText" id="textDateLabel"
									style="font-weight: bold" value="#{labels.DateLabel1}:"></h:outputText>

								<h:outputText styleClass="outputText" id="textDateOfToday"
									value="#{trialCustomerHandler.currentInstance.startDate}">
									<hx:convertDateTime />
								</h:outputText>

								<h:outputText styleClass="outputText" id="textProductLabel"
									style="font-weight: bold"
									value="#{labels.SubscriptionProdNameLabel}:"></h:outputText>

								<h:outputText styleClass="outputText" id="textProdName"
									value="#{trialCustomerHandler.currentInstance.product.name}"></h:outputText>
								<h:outputText styleClass="outputText" id="textTermLabel"
									style="font-weight: bold" value="#{labels.termLabel}:"></h:outputText>
								<h:outputText styleClass="outputText" id="textSelectedTerm"
									value="#{trialPartnerHandler.partner.durationInDays} day (trial subscription)">

								</h:outputText>
							</h:panelGrid>				
							<!-- Delivery Information -->	
							<h:panelGrid styleClass="panelGrid" id="gridAccountInformationDelivery" columns="2" width="300" style="margin-left: 5px; font-family: Arial; font-size: 11px" >
								<f:facet name="header">
									<h:panelGroup styleClass="panelGroup" id="groupDeliveryInfoGroup">
										<hx:panelLayout styleClass="panelLayout" id="layoutOrderInfo2" width="290" align="left">
											<f:facet name="body"></f:facet>
											<f:facet name="left">
												<h:outputText styleClass="panelThankYouPage_Header"
													id="textOrderInformationDeliveryHeaderlabel"
													value="#{labels.emailAddressRecipientLabel}"></h:outputText>
											</f:facet>
											<f:facet name="right"></f:facet>
											<f:facet name="bottom"></f:facet>
											<f:facet name="top"></f:facet>
										</hx:panelLayout>											
									</h:panelGroup>
								</f:facet>
								<h:outputText styleClass="outputText"
									id="textDeliveryEmailLabel" style="font-weight: bold"
									value="#{labels.EmailAddressLabel}:"></h:outputText>

								<h:outputText styleClass="outputText" id="textDeliveryEmail"
									value="#{trialCustomerHandler.trialCustomer.emailAddress}">
								</h:outputText>

								<h:outputText styleClass="outputText" id="textDeliveryNameLabel"
									value="#{labels.NameLabel}:" style="font-weight: bold"></h:outputText>
								<h:outputText styleClass="outputText" id="textDeliveryName"
									value="#{trialCustomerHandler.currentInstance.trial.contactInformation.firstName} #{trialCustomerHandler.currentInstance.trial.contactInformation.lastName}">
								</h:outputText>

								<h:outputText styleClass="outputText"
									id="textDeliveryAddr1Label" value="#{labels.AddressLabel}:"
									style="font-weight: bold"></h:outputText>
								<h:outputText styleClass="outputText" id="textDeliveryAddr1"
									value="#{trialCustomerHandler.currentInstance.trial.contactInformation.uiAddress.address1}">
								</h:outputText>
								<h:outputText styleClass="outputText"
									id="textDeliveryAptSuiteLabel" style="font-weight: bold"></h:outputText>
								<h:outputText styleClass="outputText" id="textDeliveryAptSuite"
									value="#{trialCustomerHandler.currentInstance.trial.contactInformation.uiAddress.aptSuite}">
								</h:outputText>
								<h:outputText styleClass="outputText"
									id="textDeliveryAddr2Label" style="font-weight: bold"></h:outputText>
								<h:outputText styleClass="outputText" id="textDeliveryAddr2"
									value="#{trialCustomerHandler.currentInstance.trial.contactInformation.uiAddress.address2}">
								</h:outputText>

								<h:outputText styleClass="outputText"
									id="textDeliveryAddrStateZipLabel" style="font-weight: bold"></h:outputText>
								<h:outputText styleClass="outputText" id="textDeliveryStateZip"
									value="#{trialCustomerHandler.currentInstance.trial.contactInformation.uiAddress.city}, #{trialCustomerHandler.currentInstance.trial.contactInformation.uiAddress.state} #{trialCustomerHandler.currentInstance.trial.contactInformation.uiAddress.zip}">
								</h:outputText>

							</h:panelGrid>
							
							</div>												
						</hx:jspPanel>
					</h:panelGrid>
					</div>
					<div id="pageContent30">
						<h:panelGrid styleClass="panelGrid" id="gridRightPanelGrid" cellpadding="0" cellspacing="0">
							<hx:jspPanel id="jspPanelRightPanelTop">
								<table border="0" cellpadding="0" cellspacing="0" width="270">

								<tr>
								   <td></td>
								   <td><img src="/images/eEdition/spacer.gif" width="1" height="1" border="0" alt="" /></td>
								   <td></td>
								   <td><img src="/images/eEdition/spacer.gif" width="1" height="1" border="0" alt="" /></td>
								</tr>
								<tr>
								   <td colspan="3"><img name="computer_art_2_button_v2_r1_c1"
										src="/images/eEdition/computer_art_2_button_v2_r1_c1_260w.jpg"
										width="260" height="192" border="0"
										id="computer_art_2_button_v2_r1_c1" alt="" /></td>
								   <td><img src="/images/eEdition/spacer.gif" width="1" height="210" border="0" alt="" /></td>
								  </tr>
								  <tr>
								   <td colspan="3" align="center">
								    <div id="pageContentNoMarginsButton">
								   	<hx:outputLinkEx styleClass="outputLinkEx"
										id="linkExReadNowLink"
										onclick="return func_1(this, event);" value="#{trialCustomerHandler.currentInstance.defaultElectronicReadNowURL}">
										<hx:graphicImageEx styleClass="graphicImageEx"
											id="imageExReadNowButtonImage"
											value="/images/eEdition/computer_art_2_button_v2_r2_c1_116w.gif"
											width="116" height="40" hspace="0" vspace="0" border="0"
											align="center"></hx:graphicImageEx>
									</hx:outputLinkEx>
									<table style="width: 100%;margin-top: -5px;">
										<tr>
											<td align="center">
												<table>
													<tr>
														<td>
															<hx:outputLinkEx styleClass="outputLinkEx" id="linkExOpenDesktopVersion" value="#{trialCustomerHandler.currentInstance.electronicReadNowURL}" onclick="return func_1(this, event);">
														<h:outputText id="text1111" 
															value="desktop" styleClass="outputText"></h:outputText>
												</hx:outputLinkEx>
														</td>
														<td>
															<h:outputText styleClass="outputText" id="text2" value="|"></h:outputText>
														</td>
														<td>
															<hx:outputLinkEx styleClass="outputLinkEx" id="linkExOpenMobileVersion" value="#{trialCustomerHandler.currentInstance.alternateElectronicReadNowURL}" onclick="return func_1(this, event);">
														<h:outputText id="text1" styleClass="outputText"
															value="mobile"></h:outputText>
												</hx:outputLinkEx>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									</div>
									</td>
								   <td><img src="/images/eEdition/spacer.gif" width="1" height="1" border="0" alt="" /></td>
								  </tr>
								  
								  <tr>
								   <td></td>
								   <td></td>
								   <td></td>
								   <td><img src="/images/eEdition/spacer.gif" width="1" height="18" border="0" alt="" /></td>
								  </tr>
								</table>
							
							</hx:jspPanel>
						</h:panelGrid>
					    </div>
				<br></h:form></hx:scriptCollector>
		<%-- /tpl:put --%>
</DIV>
<%-- /tpl:put --%>
				<DIV id="clear"></DIV>
				<DIV id="bluebar">
					<a href="/feedback/feedback.jsp"
						style="color: white; font-weight: bold; text-decoration: none; font-size: 14px"
						title="Contact Us">Contact Us</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;1-800-872-0001&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a
						href="/faq/utfaq.jsp"
						style="color: white; font-weight: bold; text-decoration: none; font-size: 14px"
						title="Frequently Asked Questions">FAQ</a>
				</DIV>
				<%-- tpl:put name="footerMainArea" --%>
	<%-- tpl:put name="footerArea_1" --%>
			
		<%-- /tpl:put --%>
	<%=trackingBug.getOmnitureHTML() %>
		<%-- /tpl:put --%>
				<DIV id=graybar>
					Copyright
					<script>
						document.write(getCurrentYear());
					</script>
					USA TODAY, a division of <a href="http://www.gannett.com">Gannett
						Co. Inc.</a> <a href="/privacy/privacy.htm">Privacy
						Notice/California Privacy Notice</a>. <a
						href="/privacy/privacy.htm#adchoice">Ad Choices</a>.<br> By
					using this service, you accept our <a href="/service/service.jsp">Terms
						of Service</a>.
				</DIV>
				<DIV id=copyright>
					<P>Offers available to new subscribers only. Home delivery not
						available in some areas. Not valid with any other offer.
						Prepayment required. After the promotional period ends, the
						subscription will continue and be billed monthly at the then
						regular rate, less any applicable credits, unless USA TODAY is
						otherwise notified. Applicable taxes may not be included in the
						rate. If at any time you decide to cancel your subscription, you
						may contact customer service at 1-800-872-0001 and the full amount
						of any balance over $2.00 will be returned.</P>
				</DIV>
				<!--END OF COPYWRITE-->
				<DIV id=footer></DIV>
			</DIV>
			<!--END OF WHITEBOX-->
		</DIV>
		<!--END OF CONTENT-->
	</DIV>
</BODY>
</HTML><%-- /tpl:insert --%>
</f:view> <%-- /tpl:insert --%>