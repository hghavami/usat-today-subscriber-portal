<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/subscriptions/electronic/samples/Index.java" --%><%-- /jsf:pagecode --%><%-- tpl:insert page="/theme/themeV3/jsfTheme/subscriberPortalNoNav_JSF.jtpl" --%><!DOCTYPE HTML><%@page import="com.usatoday.business.interfaces.products.ProductIntf"%>
<%@page import="com.usatoday.businessObjects.products.ProductBO"%>
<%@page import="com.usatoday.util.MobileDeviceDetectionUtil"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%><%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%><%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%><f:view><f:loadBundle basename="resourceFile" var="labels" />
	<%-- tpl:insert page="/theme/subscriptionPortalRoot.htpl" --%><HTML>
<HEAD>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8">
<%-- tpl:put name="headerareaTop" --%>
<%
	String docProtocol = "https:";
	if (!request.isSecure()) {
		docProtocol = "http:"; 
	}
	
	// change this to any javascript you want called on page load
	String initFunction = "";
	
	com.usatoday.esub.common.WebBug trackingBug = com.usatoday.esub.common.UTCommon.getWebAnalyticsBug();
	try {
		trackingBug.deriveValuesFromRequest(request);
	}
	catch (Exception e) { 
		; // ignore
	}
	
	String pubCode = "";
	String productName = "USA TODAY";
	
    boolean isEEditionProduct = true;

	com.usatoday.businessObjects.products.promotions.PromotionSet defaultPromotionSet = null;
	com.usatoday.businessObjects.products.promotions.PromotionSet currentOfferPromotionSet = null;
	com.usatoday.business.interfaces.customer.CustomerIntf customer = null;
   	com.usatoday.businessObjects.products.promotions.PromotionSet currentAccountPromotionSet = null;
   		
	com.usatoday.business.interfaces.products.OfferIntf offer = null;   		
	try {
		offer = com.usatoday.esub.common.UTCommon.getCurrentOfferVersion2(request);
		pubCode = com.usatoday.util.constants.UsaTodayConstants.UT_PUBCODE;
		if (offer != null) {
			pubCode = offer.getPubCode();
			try {
				String brandingPubCode = offer.getProduct().getBrandingPubCode();
				ProductIntf bp = ProductBO.fetchProduct(brandingPubCode);
				productName = bp.getName();
			}
			catch (Exception teee) {
			}							
		}
		// get the default promotion set for the publication		
		defaultPromotionSet = com.usatoday.esub.common.UTCommon.getDefaultPromotionSetForPub(pubCode);
		
		// get the keycode specific promotion set
		
		if (offer != null && offer instanceof com.usatoday.business.interfaces.products.SubscriptionOfferIntf) {
			currentOfferPromotionSet = ((com.usatoday.business.interfaces.products.SubscriptionOfferIntf)offer).getPromotionSet();
		}
		boolean isSignedIn = false;

		customer = (com.usatoday.business.interfaces.customer.CustomerIntf)session.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_CUSTOMER_INFO);
		
		if (customer != null) {
			isSignedIn = true;
			com.usatoday.business.interfaces.products.SubscriptionProductIntf product = com.usatoday.businessObjects.products.SubscriptionProductBO.getSubscriptionProduct(customer.getCurrentEmailRecord().getPubCode());
			isEEditionProduct = product.isElectronicDelivery(); 
			// This promotion set is based on pub and keycode on the customer account
			currentAccountPromotionSet = com.usatoday.businessObjects.products.promotions.PromotionManager.getInstance().getPromotionsForOffer(customer.getCurrentAccount().getPubCode(), customer.getCurrentAccount().getKeyCode());
			
		}
	} catch (Exception e) {
	
	}

	String mobileCSS = "";
	if (MobileDeviceDetectionUtil.isSmallScreenDevice(request)) {
		mobileCSS = "<LINK href=\"/theme/themeV3/css/smallScreen.css\" rel=\"stylesheet\" type=\"text/css\">";
	}
%>
<script src="/common/prototype/prototype.js"></script>
<script src="/common/prototype/scriptaculous/scriptaculous.js" type="text/javascript"></script>
<link rel="stylesheet"	href="/theme/themeV3/css/stylesheet_jsfcomponents.css"	type="text/css">
<%-- tpl:put name="headerarea_Top" --%><title>USA TODAY eEdition Trial Subscription Sign Up</title>
<meta name="description" content="USA TODAY e-Newspaper, a convenient new way to read your daily newspaper on your computer, in the same format you love. It combines the speed of the Internet with the organization of a newspaper. Daily Editor picks, Weekend Extra, Puzzles and Games, Snapshots and more." />
<meta name="keywords" content="replica, eedition, news,world news,international news,us news,travel news,financial news,money news,finance news,economic news,economy news,sports news,technology news,science news,weather,weather news,national news" />
<%-- /tpl:put --%>
<%-- /tpl:put --%>
<LINK REL="SHORTCUT ICON" HREF="/favicon.ico">
<LINK REL="icon" href="/favicon.ico" TYPE="image/ico" />
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META http-equiv="Content-Style-Type" content="text/css">
<meta name="viewport" content="initial-scale=1.0">
<LINK href="/theme/themeV3/css/usatodayBase.css" rel="stylesheet"
	type="text/css">
<LINK href="/theme/themeV3/css/usatodayAddOn.css" rel="stylesheet"
	type="text/css">
<script src="/common/usatCommon.js"></script>
<%-- tpl:put name="headareaBottom" --%>
<%=mobileCSS %>
<%-- tpl:put name="headarea_Bottom" --%>
<% 	
	initFunction += " initThisPage(thisObj, thisEvent);";
%>
<script src="/common/prototype/scriptaculous/scriptaculous.js?load=effects,dragdrop" type="text/javascript"></script>
<script type="text/javascript" src="/common/popUpOverlay.js"></script>
<script type="text/javascript">
function func_OpenPartnerWindow(thisObj, thisEvent, pURL) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
_JumpURLinNewWindow(pURL);

}</script><script type="text/javascript">
<!--
function _JumpURLinNewWindow(url) 
{
  if (url != "")
  {
    window.open(url, '_blank');
  }
}

function initThisPage(thisObj, thisEvent) {
	try {
		var showOverlayField = $('formSampleForm:showOverlayPopUp');
	
		if (showOverlayField.value == "true") {
			keyCodeOverlayEnabled = true;
		}
	}
	catch (err) {
	} 
}

function onbeforeunloadPage(thisObj, thisEvent) {
	if( popOffer_programEnabled && keyCodeOverlayEnabled) {
		return popUpOnBeforeUnload(thisObj, thisEvent);
	}
}

function unloadPage(thisObj, thisEvent) {
	if (popOffer_programEnabled && keyCodeOverlayEnabled) {
		popOffer_unloadingPage = true;
		closeSpecialOffer(thisObj, thisEvent);
	}
}

//-->
</script>
<script type="text/javascript" src="/common/videoWindow.js"></script>
<%-- /tpl:put --%>
<script>
// method called on page load
function initPage(thisObj, thisEvent) {
	// insert site-wide code here
	
	// for product messageing in popUpOverlay.js
	usat_productName = "<%=productName %>";
	
	<%=initFunction %>
}
</script>
<%-- /tpl:put --%>
<script type="text/javascript">
	/*   var _gaq = _gaq || [];
	 _gaq.push(['_setAccount', 'UA-23158809-1']);
	 _gaq.push(['_trackPageview']);

	 (function() {
	 var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	 ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	 var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	 })();
	 */
</script>
</HEAD>
<body>
	<DIV id="wrapper">
		<DIV id="header">
			<div id="headerLeft" onclick="templateLogoClick();"></div>
			<div id="headerRight">
				<%-- tpl:put name="headerbodyarea" --%>
					<%-- tpl:insert page="/login/loginform.jspf" --%><div style="float: none">
	<%
		String uri = request.getRequestURI();
		String pageName = uri.substring(uri.lastIndexOf("/") + 1);
		String jScript = "";
		if (uri.indexOf("subscriptions") > 0 && uri.indexOf("thankyou.jsp") > 0) {
			try {
				com.usatoday.esub.handlers.ShoppingCartHandler shoppingCartHandler = (com.usatoday.esub.handlers.ShoppingCartHandler) session
						.getAttribute("shoppingCartHandler");
				if (shoppingCartHandler != null) {
					jScript = shoppingCartHandler.getOnePageThankYouJavaScriptTealiumText();
				}
			} catch (Exception E) {

			}
		} else {
			com.usatoday.businessObjects.products.promotions.PromotionSet currentOfferPromotionSetTealium = null;
			com.usatoday.business.interfaces.products.OfferIntf offerTealium = com.usatoday.esub.common.UTCommon
					.getCurrentOffer(request);
			if (offerTealium != null && offerTealium instanceof com.usatoday.business.interfaces.products.SubscriptionOfferIntf) {
				currentOfferPromotionSetTealium = ((com.usatoday.business.interfaces.products.SubscriptionOfferIntf) offerTealium)
						.getPromotionSet();
				jScript = currentOfferPromotionSetTealium.getPromoOnePageJavaScriptTealiumText();
				jScript = org.apache.commons.lang3.StringUtils.replace(jScript, "page_name:\"\"", "page_name:\"" + uri + "\"");
			}
		}
	%>
	<%=jScript%>
</div>
<%-- 			<f:verbatim>
				<h:outputText styleClass="outputText" id="textThankYouJS13"	escape="false" value="#{shoppingCartHandler.tealiumOnePageThankYouJavaScriptText}"></h:outputText>
			</f:verbatim>
 --%>
<%
	String isLoginDisplay = "true";
	com.usatoday.business.interfaces.customer.CustomerIntf customer2 = null;
	String loggedinDivClass = "hideMe";
	String notLoggedinDivClass = "showMeInline";
	String firstName = "";
	String lastName = "";
	String firmInfo = "";
	String accountNum = "";
	String loginErrorMessage2 = "";
	String logOut = "";
	String publication = "";
	String changeAccountLink = "";
	String earlyAlertDisplay = "display:none";

	String jScriptNoOp = "javascript:;";

	try {
		publication = (String) session.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_PUB_NAME);
		if (publication == null) {
			publication = request.getParameter("pub");
			if (publication == null) {
				publication = com.usatoday.util.constants.UsaTodayConstants.UT_PUBCODE;
			}
		}
		if (publication.equals(com.usatoday.esub.common.UTCommon.PUB_NAMES[0])) { // USATODAY publication
			logOut = com.usatoday.esub.common.UTCommon.BASE_URL + "/index.jsp?pub=UT&logsessobj=rem";
		} else {
			logOut = com.usatoday.esub.common.UTCommon.BASE_URL + "/index.jsp?pub=BW&logsessobj=rem";
		}

		// If customer is logged in remove login page, otherwise display it
		customer2 = (com.usatoday.business.interfaces.customer.CustomerIntf) session
				.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_CUSTOMER_INFO);
		if (customer2 == null) {
			// showMeInling and hideMe are defined in style sheets
			loggedinDivClass = "showMeInline";
			notLoggedinDivClass = "hideMe";
		} else {

			if (customer2.getCurrentAccount() != null) {
				firstName = customer2.getCurrentAccount().getDeliveryContact().getFirstName();
				if (firstName != null && !firstName.trim().equals("")) {
					firstName = com.usatoday.esub.common.UTCommon.formatNameCapitalization(firstName); // Format it
				}
				lastName = customer2.getCurrentAccount().getDeliveryContact().getLastName();
				if (lastName != null && !lastName.trim().equals("")) {
					lastName = com.usatoday.esub.common.UTCommon.formatNameCapitalization(lastName); // Format it
				}
				firmInfo = customer2.getCurrentAccount().getDeliveryContact().getFirmName();
				if (customer2.getNumberOfAccounts() > 1) {
					changeAccountLink = "[&nbsp;<a href=\"/account/select_accnt.jsp\">Change</a>&nbsp;]";
				}
				try {
					accountNum = customer2.getCurrentAccount().getAccountNumber();
					//				if (accountNum == null || accountNum.length() < 4) {
					if (accountNum == null) {
						accountNum = "";
					}
					//				else {
					/*
					if (accountNum.equalsIgnoreCase("340000000")) {
						accountNum = "Pending Setup";
					}
					 */
					//				}
				} catch (Exception eeee) {
					eeee.printStackTrace();
				}

				//EarlyAlert - Check to see if their paper delivery has been affected by a delay
				// HD-CONS109 SEW
				/*
				if (customer2.getCurrentAccount().isShowDeliveryAlert()) {
				earlyAlertDisplay = "display:inline";
				}
				 */

			} else {
				if (customer2.getCurrentEmailRecord() != null
						&& customer2.getCurrentEmailRecord().getEmailAddress() != null) {
					firstName = customer2.getCurrentEmailRecord().getEmailAddress();
					accountNum = "Pending Setup";
				} else {
					firstName = "";
				}
			}
		}
		loginErrorMessage2 = (String) session.getAttribute("LOGIN_FAILED_MESSAGE");
		if (loginErrorMessage2 == null || loginErrorMessage2.trim().equals("")) {
			loginErrorMessage2 = "";
		} else {
			session.removeAttribute("LOGIN_FAILED_MESSAGE");
		}
	} catch (Exception e) {
		e.printStackTrace();
		if (firstName == null) {
			firstName = "";
		}
		if (lastName == null) {
			lastName = "";
		}
	}
	String nameInfo = firstName + " " + lastName;
	String loginInfo = null;
	if (!nameInfo.trim().equals("")) {
		loginInfo = nameInfo;
	} else {
		if (firmInfo != null && !firmInfo.trim().equals("")) {
			loginInfo = firmInfo;
		}
	}
	// if no login info, then hide data
	if (loginInfo == null) {
		loggedinDivClass = "showMeInline";
		notLoggedinDivClass = "hideMe";
		loginInfo = "";
	}
%>

<%@page import="com.usatoday.util.constants.UsaTodayConstants"%><div
	id="loginDisplayDiv" class="<%=loggedinDivClass%>">
	<table border="0" width="100%" height="60px">
		<tr>
			<td>
				<TABLE class="loginFormTable">
					<TBODY>
						<TR>
							<TD class="loginFormColumn"><br /> <a
								href="/login/auth.faces">Login to Manage Account</a></TD>
						</TR>
						<tr>
							<td class="loginFormColumn" align="right"><img
								style="cursor: pointer; margin-top: 10px;"
								onclick="javascript: templateClickToChatClickedGuest(this, event);"
								alt="Click for Assistance" title="Click for Assistance"
								src="${pageContext.request.contextPath}/theme/themeV3/themeImages/chatIcon136.png"
								hspace="0" vspace="5"></td>
						</tr>
					</TBODY>
				</TABLE></td>
		</tr>
	</table>
</div>
<div id="loginHideDiv" class="<%=notLoggedinDivClass%>">
	<table width="100%" border="0" class="loginFormTable">
		<tr>
			<td nowrap align="right">
				<TABLE class="loginFormTable">
					<TBODY>
						<TR>
							<TD class="loginFormColumn" nowrap><span
								class="loginNormalPrint">Hello, <%=loginInfo%>&nbsp;&nbsp;[&nbsp;<a
									href="<%=jScriptNoOp%>"
									onclick="javascript:document.location='<%=logOut%>';_unloadPage(this, event);">Logout</a>&nbsp;]</span>
							</TD>
						</TR>
					</TBODY>
				</TABLE></TD>
		</tr>
		<tr>
			<td class="loginFormColumn" align="right"><span
				class="loginfineprint">Account #:&nbsp;<%=accountNum%>&nbsp;<%=changeAccountLink%></span>
			</td>
		</tr>
		<tr>
			<td class="loginFormColumn" align="right"><div
					id="chatIconRequestDiv">
					<img onclick="javascript: templateClickToChatClicked(this, event);"
						style="cursor: pointer; margin-top: 10px;"
						alt="Click for Assistance" title="Click for Assistance"
						src="${pageContext.request.contextPath}/theme/themeV3/themeImages/chatIcon136.png"
						hspace="0" vspace="5">
				</div>
			</td>
		</tr>
	</table>
</div>
<div id="templateChatWindowDiv" style="display: none;">
	<div id="templateChatTopDiv">
		<span onclick="templateClickToChatHide();" style="cursor: pointer;">No
			Thanks<img src="/theme/themeV3/themeImages/close_14x14.png"
			width="14" height="14" border="0"
			style="margin-left: 5px; margin-right: 5px" /> </span>
	</div>
<!-- 	<div id="tempateChatImageDiv">
		<a
			onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
			href=""><img border="0"
			src="https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY" />
		</a>
	</div>
 -->	<div id="templateChatAccountLinksDiv">
		<ul>
			<li>To report a problem with delivery, click <a
				href="/account/cp/delissue.faces"> here</a>.</li>
			<li>To place your delivery on a temporary hold, click <a
				href="/account/vacationholds/holds.faces"> here</a>.</li>
			<li>Forgot your password or login? Click <a
				href="/idpassword/forgotpassword.jsp"> here</a>.
			<li>To contact us <a href="/feedback/feedback.jsp"> click here</a>.</li>				
<!-- 			<li>To cancel your subscription, please call 1-800-872-0001 and -->
<!-- 				select option 6.</li> -->
		</ul>
	</div>

	<div id="templateChatLinksDiv">
		<ul>

			<li><a
				onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
				href="">Click to Chat</a>
			</li>
		</ul>
	</div>
	<div id="templateChatBottomDiv">
		<span class="outputText">Or, call us at:<br> <b>Call
				1-877-713-6241</b><br>(M-F 8:00 a.m. to 7 p.m. E.T.)</span>
	</div>
</div>
<div id="templateChatWindowGuestDiv" style="display: none;">
	<div id="templateChatTopDiv">
		<span onclick="templateClickToChatHideGuest();"
			style="cursor: pointer;">No Thanks<img
			src="/theme/themeV3/themeImages/close_14x14.png" width="14"
			height="14" border="0" style="margin-left: 5px; margin-right: 5px" />
		</span>
	</div>
<!-- 	<div id="tempateChatImageDiv">
		<a
			onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
			href=""><img border="0"
			src="https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY" />
		</a>
	</div>
 -->	<div id="templateChatAccountLinksDiv">
		<ul>
			<li>To manage your account online - including reporting a
				problem with delivery or placing a temporary hold on your account -
				log in by entering your e-mail address and password. Click <a
				href="/login/auth.faces"> here</a> to access the login page now.</li>
			<li>Forgot your password or login? <a
				href="/idpassword/forgotpassword.jsp"> click here</a>.</li>
			<li>To contact us <a href="/feedback/feedback.jsp"> click here</a>.</li>
			<!-- 			<li>To cancel your subscription, please call 1-800-872-0001 and -->
			<!-- 				select option 6.</li> -->
		</ul>

	</div>

	<div id="templateChatLinksDiv">
		<ul>
			<li><a
				onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
				href="">Click to Chat</a>
			</li>
		</ul>
	</div>
	<div id="templateChatBottomDiv">
		<span class="outputText">Or, call us at:<br> <b>Call
				1-877-713-6241</b><br>(M-F 8:00 a.m. to 7 p.m. E.T.)</span>
	</div>
</div>
<%-- /tpl:insert --%>

		<%-- /tpl:put --%>
			</div>
		</DIV>
		<!-- end header -->
		<DIV id="content">
			<DIV id="whitebox">
				<%-- tpl:put name="bodyAreaMainWhiteBox" --%>
<DIV id="pageContentFullWidth">
			<%-- tpl:put name="mainBodyArea" --%>
<div id="nav" style="width: auto; padding: 0px">
<hx:scriptCollector id="scriptCollectorTopNavCollector" preRender="#{pc_Index.onPageLoadBegin1}">
	<hx:jspPanel id="jspPanelNavBottomEditionTrialLogin">
			<h3>Already Signed Up?</h3>
			<a
				href="${pageContext.request.contextPath}/login/trialLogin.faces"
				style="background-color: white; text-decoration: none; border-width: 0px; border-style: none"><img
				src="${pageContext.request.contextPath}/images/eEdition/welcome_page_graphic_v2_r4_c3.gif"
				alt="login button" width="138" height="25" border="0" /></a>
			<br />
            <br />
		</hx:jspPanel>
	<h:panelGrid styleClass="panelGrid" id="gridLogoGridNav" width="159" cellpadding="0" cellspacing="0" columns="1" rendered="#{trialPartnerHandler.isShowLogoImage}">
		<hx:jspPanel id="jspPanelLinkableLogoPanel" rendered="#{trialPartnerHandler.isShowLogoImageURL}">
			<img src="${trialPartnerHandler.partner.logoImageURL}" 
				alt="" width="155" border="0" hspace="0" vspace="0" align="left" onclick="return func_OpenPartnerWindow(this, event, '${trialPartnerHandler.parnterLogoLinkURL}');" style="cursor: pointer"/>
		</hx:jspPanel>
		<hx:graphicImageEx styleClass="graphicImageEx"
				id="imageExPartnerLogoNoLink"
				value="#{trialPartnerHandler.partner.logoImageURL}" border="0"
				hspace="0" vspace="0"
				rendered="#{not trialPartnerHandler.isShowLogoImageURL}"
				style="z-index: 50; margin-top: 20px;" width="155"></hx:graphicImageEx>
	</h:panelGrid>
	
	</hx:scriptCollector>
</div>	<div id="pageContent" style="padding: 0px">	
			<h1 style="margin-left: 4px"><h:outputText id="textMainTableHeader" value="USA TODAY e-Newspaper Trial" styleClass="H1Class"></h:outputText></h1>
			<hx:scriptCollector id="scriptCollectorSampleForm" preRender="#{pc_Index.onPageLoadBegin}">
				<h:form styleClass="form" id="formSampleForm">
							<h:panelGrid styleClass="panelGrid" id="gridMainBodyGrid"
								cellpadding="0" cellspacing="0" columns="1" width="745"
								style="margin-left: 2px; margin-right: 0px; padding-left: 0px; z-index: 50; padding-right: 0px">

								<hx:jspPanel id="jspPanelFormPanel">


									<h:panelGrid styleClass="panelGrid" id="gridPartnerNotValid"
										rendered="#{not trialPartnerHandler.isPartnerValid }"
										columns="1" width="475" cellpadding="2">
										<h:outputText styleClass="outputText" id="textInformation1"
											value='The requested page or offer is invalid or no longer available.'
											style="font-weight: bold"></h:outputText>
										<h:outputText styleClass="outputText" id="textInformation2"
											style="font-weight: bold"
											value='To check our current subscription offers, click <a href="/welcome.jsp">here</a>.'
											escape="false"></h:outputText>
									</h:panelGrid>


									<hx:panelLayout styleClass="panelLayout" id="layoutPageLayout"
										rendered="#{trialPartnerHandler.isPartnerValid}" width="750"
										style="vertical-align: top">
										<f:facet name="body">

											<h:panelGroup id="groupMainBodyPanel">
												<h:panelGrid styleClass="panelGrid"
													id="gridPartnerOfferDescGridv2"
													rendered="#{trialPartnerHandler.isPartnerValid}"
													columns="1" width="475" cellpadding="1" bgcolor="white">
													<h:outputText styleClass="outputText"
														id="textOfferDescription"
														value="#{trialPartnerHandler.partner.description}"
														escape="false" style="margin-bottom: 15px;"></h:outputText>
												</h:panelGrid>

															<h:panelGrid id="gridPartnerGrid" width="475" columns="1"
																rendered="#{trialPartnerHandler.partner.isShowClubNumber}"
																style="margin-top: 5px;">
																<hx:panelFormBox helpPosition="over"
																	labelPosition="left" styleClass="panelFormBox"
																	id="formBoxPartnerFormBox" widthLabel="150"
																	label="#{trialPartnerHandler.partner.partnerName} Trial Subscription">
																	<hx:formItem styleClass="formItem"
																		id="formItemPartnerItem1" label="Partner Name:">
																		<h:inputText styleClass="inputText"
																			id="textPartnerName" readonly="true" disabled="true"
																			size="25"
																			value="#{trialPartnerHandler.partner.partnerName}"
																			tabindex="1"></h:inputText>

																	</hx:formItem>
																	<hx:formItem styleClass="formItem"
																		id="formItemClubNumber"
																		label="#{trialPartnerHandler.clubNumberFieldLabel}">
																		<h:inputText styleClass="inputText"
																			id="textHiltonHonorsNumber" size="25"
																			value="#{newSampleOrderHandler.partnerClubNumber}"
																			maxlength="15" tabindex="2">
																			<f:validateLength maximum="15"></f:validateLength>
																		</h:inputText>
																	</hx:formItem>

																	<f:facet name="bottom">

																	</f:facet>
																	<f:facet name="right">

																	</f:facet>
																	<f:facet name="top">

																	</f:facet>
																</hx:panelFormBox>
															</h:panelGrid>
															<h:panelGrid styleClass="panelGrid"
													id="gridDeliveryInformation" width="475" columns="1">
																<hx:panelFormBox helpPosition="under"
																	labelPosition="left" styleClass="panelFormBox"
																	id="formBoxDeliveryInformation"
																	label="#{labels.emailAddressRecipientLabel2}"
																	widthLabel="150">

																	<hx:formItem styleClass="formItem"
																		id="formItemDeliveryFirstName"
																		label="#{labels.nameLabel} *"
																		errorText="#{labels.requiredFieldMsg}">

																		<h:inputText styleClass="inputText"
																			id="textDeliveryFirstName" size="18" tabindex="20"
																			value="#{newSampleOrderHandler.deliveryFirstName}"
																			maxlength="10" required="true"
																			requiredMessage="First Name is Required"
																			validatorMessage="First Name is Not Valid">
																			<f:validateLength minimum="1" maximum="10"></f:validateLength>
																		</h:inputText>
																		<h:inputText styleClass="inputText"
																			id="textDeliveryLastName" size="20"
																			style="margin-left: 5px" tabindex="21"
																			value="#{newSampleOrderHandler.deliveryLastName}"
																			maxlength="15" required="true"
																			requiredMessage="Last Name is Required"
																			validatorMessage="Last Name is Not Valid">
																			<f:validateLength minimum="1" maximum="15"></f:validateLength>
																		</h:inputText>

																	</hx:formItem>
																	<hx:formItem styleClass="formItem"
																		id="formItemDeliveryAddress1"
																		label="#{labels.address} *"
																		errorText="#{labels.requiredFieldMsg}">
																		<h:inputText styleClass="inputText"
																			id="textDeliveryAddress1" size="28" tabindex="22"
																			value="#{newSampleOrderHandler.deliveryAddress1}"
																			maxlength="28" required="true"
																			requiredMessage="Address is Required"
																			validatorMessage="Address is Not Valid">
																			<f:validateLength minimum="1" maximum="28"></f:validateLength>
																		</h:inputText>
																		<h:inputText styleClass="inputText"
																			id="textDeliveryAptSuite" tabindex="23" size="8"
																			style="margin-left: 5px"
																			value="#{newSampleOrderHandler.deliveryAptSuite}"
																			maxlength="28">
																			<f:validateLength maximum="28"></f:validateLength>
																		</h:inputText>
																	</hx:formItem>
																	<hx:formItem styleClass="formItem"
																		id="formItemDeliveryAddress2"
																		label="#{labels.address2Label}:" rendered="false">
																		<h:inputText styleClass="inputText"
																			id="textDeliveryAddress2" size="30" tabindex="24"
																			value="#{newSampleOrderHandler.deliveryAddress2}"
																			maxlength="28">
																			<f:validateLength minimum="0" maximum="28"></f:validateLength>
																		</h:inputText>
																	</hx:formItem>
																	<hx:formItem styleClass="formItem"
																		id="formItemDeliveryCity"
																		label="#{labels.cityLabel} *"
																		errorText="#{labels.requiredFieldMsg}">
																		<h:inputText styleClass="inputText"
																			id="textDeliveryCity" size="30" tabindex="26"
																			value="#{newSampleOrderHandler.deliveryCity}"
																			maxlength="28" required="true"
																			requiredMessage="City is Required"
																			validatorMessage="City is Not Valid">
																			<f:validateLength minimum="1" maximum="28"></f:validateLength>
																		</h:inputText>
																	</hx:formItem>
																	<hx:formItem styleClass="formItem"
																		id="formItemDeliveryState"
																		label="#{labels.StateZipLabel} *"
																		errorText="#{labels.requiredFieldMsg}">
																		<h:selectOneMenu styleClass="selectOneMenu"
																			id="menuDeliveryState" tabindex="27"
																			requiredMessage="State is Required"
																			validatorMessage="State is Not Valid" required="true"
																			value="#{newSampleOrderHandler.deliveryState}">
																			<f:selectItem itemValue="NONE" itemLabel="" />
																			<f:selectItem itemValue="AL" itemLabel="Alabama" />
																			<f:selectItem itemValue="AK" itemLabel="Alaska" />
																			<f:selectItem itemValue="AZ" itemLabel="Arizona" />
																			<f:selectItem itemValue="AR" itemLabel="Arkansas" />
																			<f:selectItem itemValue="AA"
																				itemLabel="(AA) Armed Forces Americas" />
																			<f:selectItem itemValue="AE"
																				itemLabel="(AE) Armed Forces Africa" />
																			<f:selectItem itemValue="AE"
																				itemLabel="(AE) Armed Forces Canada" />
																			<f:selectItem itemValue="AE"
																				itemLabel="(AE) Armed Forces Europe" />
																			<f:selectItem itemValue="AE"
																				itemLabel="(AE) Armed Forces Middle East" />
																			<f:selectItem itemValue="AP"
																				itemLabel="(AP) Armed Forces Pacific" />
																			<f:selectItem itemValue="CA" itemLabel="California" />
																			<f:selectItem itemValue="CO" itemLabel="Colorado" />
																			<f:selectItem itemValue="CT" itemLabel="Connecticut" />
																			<f:selectItem itemValue="DE" itemLabel="Delaware" />
																			<f:selectItem itemValue="DC"
																				itemLabel="District of Columbia" />
																			<f:selectItem itemValue="FL" itemLabel="Florida" />
																			<f:selectItem itemValue="GA" itemLabel="Georgia" />
																			<f:selectItem itemValue="HI" itemLabel="Hawaii" />
																			<f:selectItem itemValue="ID" itemLabel="Idaho" />
																			<f:selectItem itemValue="IL" itemLabel="Illinois" />
																			<f:selectItem itemValue="IN" itemLabel="Indiana" />
																			<f:selectItem itemValue="IA" itemLabel="Iowa" />
																			<f:selectItem itemValue="KS" itemLabel="Kansas" />
																			<f:selectItem itemValue="KY" itemLabel="Kentucky" />
																			<f:selectItem itemValue="LA" itemLabel="Louisiana" />
																			<f:selectItem itemValue="ME" itemLabel="Maine" />
																			<f:selectItem itemValue="MD" itemLabel="Maryland" />
																			<f:selectItem itemValue="MA"
																				itemLabel="Massachusetts" />
																			<f:selectItem itemValue="MI" itemLabel="Michigan" />
																			<f:selectItem itemValue="MN" itemLabel="Minnesota" />
																			<f:selectItem itemValue="MS" itemLabel="Mississippi" />
																			<f:selectItem itemValue="MO" itemLabel="Missouri" />
																			<f:selectItem itemValue="MT" itemLabel="Montana" />
																			<f:selectItem itemValue="NE" itemLabel="Nebraska" />
																			<f:selectItem itemValue="NV" itemLabel="Nevada" />
																			<f:selectItem itemValue="NH"
																				itemLabel="New Hampshire" />
																			<f:selectItem itemValue="NJ" itemLabel="New Jersey" />
																			<f:selectItem itemValue="NM" itemLabel="New Mexico" />
																			<f:selectItem itemValue="NY" itemLabel="New York" />
																			<f:selectItem itemValue="NC"
																				itemLabel="North Carolina" />
																			<f:selectItem itemValue="ND" itemLabel="North Dakota" />
																			<f:selectItem itemValue="OH" itemLabel="Ohio" />
																			<f:selectItem itemValue="OK" itemLabel="Oklahoma" />
																			<f:selectItem itemValue="OR" itemLabel="Oregon" />
																			<f:selectItem itemValue="PA" itemLabel="Pennsylvania" />
																			<f:selectItem itemValue="RI" itemLabel="Rhode Island" />
																			<f:selectItem itemValue="SC"
																				itemLabel="South Carolina" />
																			<f:selectItem itemValue="SD" itemLabel="South Dakota" />
																			<f:selectItem itemValue="TN" itemLabel="Tennessee" />
																			<f:selectItem itemValue="TX" itemLabel="Texas" />
																			<f:selectItem itemValue="UT" itemLabel="Utah" />
																			<f:selectItem itemValue="VT" itemLabel="Vermont" />
																			<f:selectItem itemValue="VA" itemLabel="Virginia" />
																			<f:selectItem itemValue="WA" itemLabel="Washington" />
																			<f:selectItem itemValue="WV"
																				itemLabel="West Virginia" />
																			<f:selectItem itemValue="WI" itemLabel="Wisconsin" />
																			<f:selectItem itemValue="WY" itemLabel="Wyoming" />
																		</h:selectOneMenu>
																		<h:inputText styleClass="inputText"
																			id="textDeliveryZip" tabindex="28" size="6"
																			style="margin-left: 5px"
																			value="#{newSampleOrderHandler.deliveryZipCode}"
																			maxlength="5" required="true"
																			requiredMessage="Zip Code is Required"
																			validatorMessage="Zip is Not Valid">
																			<f:validateLength minimum="5" maximum="5"></f:validateLength>
																			<hx:validateConstraint regex="^[0-9]+$" />
																		</h:inputText>
																	</hx:formItem>
																	<hx:formItem styleClass="formItem"
																		id="formItemDeliveryPhone"
																		label="#{labels.phoneLabel}"
																		errorText="#{labels.invalidPhoneFormat}">
																		<h:inputText styleClass="inputText"
																			id="textDeliveryPhoneAreaCode" size="4" tabindex="29"
																			maxlength="3"
																			value="#{newSampleOrderHandler.deliveryPhoneAreaCode}"
																			required="false"
																			requiredMessage="Phone Number is Required"
																			validatorMessage="Phone Number is Not Valid">

																			<hx:inputHelperAssist errorClass="inputText_Error"
																				autoTab="true" id="assist4" />
																			<hx:validateConstraint regex="^[0-9]+$" />
																			<f:validateLength minimum="3" maximum="3"></f:validateLength>
																		</h:inputText>
																		<h:inputText styleClass="inputText"
																			id="textDeliveryPhoneExchange" size="4"
																			style="margin-left: 4px" tabindex="30" maxlength="3"
																			readonly="false"
																			value="#{newSampleOrderHandler.deliveryPhoneExchange}"
																			required="false"
																			requiredMessage="Phone Number is Required"
																			validatorMessage="Phone Number is Not Valid">

																			<hx:inputHelperAssist errorClass="inputText_Error"
																				autoTab="true" id="assist5" />
																			<hx:validateConstraint regex="^[0-9]+$" />
																			<f:validateLength minimum="3" maximum="3"></f:validateLength>
																		</h:inputText>
																		<h:inputText styleClass="inputText"
																			id="textDeliveryPhoneExtension"
																			style="margin-left: 4px" size="5" tabindex="31"
																			value="#{newSampleOrderHandler.deliveryPhoneExtension}"
																			maxlength="4" required="false"
																			requiredMessage="Phone Number is Required"
																			validatorMessage="Phone Number is Not Valid">

																			<hx:validateConstraint regex="^[0-9]+$" />
																			<f:validateLength minimum="4" maximum="4"></f:validateLength>
																		</h:inputText>
																	</hx:formItem>
																	<hx:formItem styleClass="formItem"
																		id="formItemEmailAddress" label="#{labels.Email} *"
																		errorText="#{labels.requiredFieldMsg}">
																		<h:inputText styleClass="inputText"
																			id="textEmailAddressRecipient" size="30"
																			required="true" tabindex="32"
																			title="Delivery Email Address"
																			value="#{newSampleOrderHandler.deliveryEmailAddress}"
																			maxlength="60"
																			requiredMessage="Email Address is Required"
																			validatorMessage="Delivery Email Address is Not Valid">
																			<f:validateLength minimum="6" maximum="60"></f:validateLength>
																		</h:inputText>
																	</hx:formItem>
																	<hx:formItem styleClass="formItem"
																		id="formItemEmailAddressConfirm"
																		label="#{labels.cnfrmEmailAddr} *"
																		errorText="#{labels.requiredFieldMsg}">

																		<h:inputText styleClass="inputText"
																			id="textEmailAddressConfirmRecipient" size="30"
																			required="true" tabindex="33"
																			title="Confirm Delivery Email Address"
																			value="#{newSampleOrderHandler.deliveryEmailAddressConfirmation}"
																			maxlength="60"
																			validatorMessage="Confirmation Email Address is Not Valid"
																			requiredMessage="Confirmation Email Address is Required">
																			<f:validateLength minimum="6" maximum="60"></f:validateLength>
																		</h:inputText>
																	</hx:formItem>
																	<hx:formItem styleClass="formItem" id="formItem2"
																		label="Password *"
																		infoText="(6-10 alpha numeric characters)">
																		<h:inputSecret styleClass="inputSecret"
																			id="secretUserPassword"
																			value="#{newSampleOrderHandler.password}"
																			tabindex="34" required="true"
																			validatorMessage="Passwords must be between 6 and 10 alpha-numeric characters."
																			maxlength="15" size="15"
																			requiredMessage="Password is required.">
																			<f:validateLength minimum="6" maximum="10"></f:validateLength>
																			<hx:validateConstraint regex="^[A-Za-z0-9]+$" />
																		</h:inputSecret>
																	</hx:formItem>
																	<hx:formItem styleClass="formItem"
																		id="formItemConfirmPassword"
																		label="Re-type Password *">
																		<h:inputSecret styleClass="inputSecret"
																			id="secretPasswordConfirmation"
																			value="#{newSampleOrderHandler.passwordConfirmation}"
																			tabindex="35" required="true"
																			requiredMessage="Please re-enter the password for confirmation"
																			validatorMessage="Passwords should be 6 to 10 alpha-numeric characters."
																			maxlength="15" size="15">
																			<f:validateLength minimum="6" maximum="10"></f:validateLength>
																			<hx:validateConstraint regex="^[A-Za-z0-9]+$" />
																		</h:inputSecret>
																	</hx:formItem>

																	<h:outputText styleClass="outputText" id="text1"
																		value="* Required Fields"></h:outputText>
																</hx:panelFormBox>
															</h:panelGrid>


												<h:panelGrid id="panelGridFormSubmissionGrid" columns="1"
													width="475" style="text-align: center">

													<h:outputText styleClass="outputText" id="textCOPPAText"
														value="#{labels.TrialEntryCOPPA}"></h:outputText>
													<hx:commandExButton type="submit"
														value="Start My Trial Subscription"
														styleClass="commandExButton" id="buttonPlaceOrder"
														tabindex="90"
														action="#{pc_Index.doButtonPlaceOrderAction}"></hx:commandExButton>

												</h:panelGrid>
												<h:panelGrid styleClass="panelGrid"
													id="gridPartnerDisclaimerText" width="475" columns="1"
													cellpadding="0" cellspacing="0"
													rendered="#{trialPartnerHandler.includesDisclaimerCustomText}"
													style="margin-bottom: 5px; margin-top: 0px; text-align: center">
													<h:panelGrid styleClass="panelGrid" frame="box"
														id="gridInnerDisclaimerGrid" width="100%" columns="1"
														rendered="#{trialPartnerHandler.isShowDisclaimerText}"
														style="margin-left: 0px; margin-top: 10px; text-align: center">
														<h:outputText styleClass="outputText"
															id="textDisclaimerText"
															value="* #{trialPartnerHandler.partner.disclaimerCustomHTML}"
															style="font-size: 9pt" escape="false"></h:outputText>
													</h:panelGrid>
												</h:panelGrid>
											</h:panelGroup>

										</f:facet>
										<f:facet name="left">

										</f:facet>
										<f:facet name="right">
											<h:panelGrid styleClass="panelGrid" id="gridRightSideGrid"
												columns="1" cellpadding="0"
												style="vertical-align: top; text-align: right">
												<h:panelGrid styleClass="panelGrid" id="gridRightPanel1"
													columns="1" style="vertical-align: top; text-align: center">


													<hx:outputLinkEx styleClass="outputLinkEx"
														id="linkExOpenReaderSample" target="_blank"
														style="border-width: 0px"
														value="http://ee.usatoday.com/Sample" tabindex="150">
														<img
															src="${pageContext.request.contextPath}/images/eEdition/Subscriber_Portal_EE_Order_Page_EE_255x152.jpg"
															alt="USA TODAY e-Newspaper sample" width="230" height="152"
															border="0" hspace="0" vspace="0">
													</hx:outputLinkEx>
													<hx:jspPanel id="jspPanelTopSampleTextPanel">
														<h:outputText styleClass="outputText"
															id="textSampleText1Text" escape="false"
															value="#{labels.trialSampleText1}"></h:outputText>
													</hx:jspPanel>
													<h:panelGrid styleClass="panelGrid" id="gridVideoGrid"
														columns="1" style="text-align: center" width="230">

														<hx:outputLinkEx styleClass="outputLinkEx"
															id="linkExVideoLink_B"
															onclick="return openVideoScreen('http://service.usatoday.com/video/eeintrovideo.html');"
															value=";"
															style="background-color: white; border-style: none"
															title="USA TODAY e-Newspaper Video" tabindex="99">
															<hx:graphicImageEx styleClass="graphicImageEx"
																id="imageExVideoSweepsGraphic_B"
																value="/images/marketingimages/eevideo.gif" border="0"
																hspace="0" vspace="4"
																style="margin: 0px; float: none; text-align: center"
																title="USA TODAY e-Newspaper Video"></hx:graphicImageEx>
														</hx:outputLinkEx>
														<h:outputText styleClass="outputText" id="textEEVideoText"
															value="Watch a virtual tour of the<br>e-Newspaper and learn more<br>about its exciting features."
															escape="false"></h:outputText>

													</h:panelGrid>

													<hx:outputLinkEx styleClass="outputLinkEx"
														id="linkExWeekendSample" target="_blank"
														value="http://ee.usatoday.com/extrasample" tabindex="151">
														<img
															src="${pageContext.request.contextPath}/images/eEdition/230w_Weekend_Extra_v2.jpg"
															alt="USA TODAY e-Newspaper sample" width="230" height="176"
															border="0" hspace="0" vspace="0">
													</hx:outputLinkEx>
													<hx:jspPanel id="jspPanelTopWeekendSampleTextPanel">
														<h:outputText styleClass="outputText"
															id="textSampleWeekendText1Text" escape="false"
															value="#{labels.trialSampleText2}"></h:outputText>
													</hx:jspPanel>
												</h:panelGrid>
											</h:panelGrid>
										</f:facet>
										<f:facet name="bottom">

										</f:facet>
										<f:facet name="top">
											<h:messages styleClass="messages" id="messagesAllMesssages"></h:messages>
										</f:facet>
									</hx:panelLayout>



									<!-- End jspPanelFormPanel -->
								</hx:jspPanel>


								<h:inputHidden value="#{trialPartnerHandler.partnerID}"
									id="partnerID"></h:inputHidden>
								<hx:inputHelperSetFocus id="setFocus1"></hx:inputHelperSetFocus>
								<h:inputHidden value="#{newSampleOrderHandler.ncsRepID}"
									id="csrID"></h:inputHidden>





								<!-- end mainBodyGrid -->
							</h:panelGrid>
							<!-- BEGIN On Exit Pop Area - DO NOT CHANGE NAMES OF DIV ID -->
					<hx:jspPanel id="jspPanelPopOverlayPanel" rendered="#{currentOfferHandler.isShowOrderPathOverlayPopUp}">
						<div id="popunder" style="display: none"></div>
						<div id="leavingofferdiv"
							style="display: none; z-index: 100000; border-bottom: 0px solid #90a0b8; border-top: 0px solid #90a0b8; border-left: 0px solid #90a0b8; border-right: 0px solid #90a0b8; background: white; position: absolute; top: 3000px; left: 200px; background-color: white;">
						<h:panelGroup styleClass="panelGroup"
							id="groupSpecialOfferGroupBox" style="height: 100%; width: 100%">
							<h:panelGrid styleClass="panelGrid"
								id="gridSpecialOfferLayoutGrid" width="100%" columns="1"
								style="text-align: right">
								<hx:outputLinkEx styleClass="outputLinkEx"
									id="linkExCloseSpecialOffer" value="#"
									onclick="return closeSpecialOffer();">
									<h:outputText id="textCloseSpecialOfferText"
										styleClass="outputText" value="Close [X]"></h:outputText>
								</hx:outputLinkEx>
								<hx:outputLinkEx styleClass="outputLinkEx"
									id="linkExSpecialOfferLink" value="#{currentOfferHandler.orderPathOverlayPopUpImageLinkURL}">
									<hx:graphicImageEx styleClass="graphicImageEx"
										id="imageExSpecialOfferImage"
										value="#{currentOfferHandler.orderPathOverlayPopUpImagePath}" style="padding: 0px"></hx:graphicImageEx>
								</hx:outputLinkEx>
							</h:panelGrid>
						</h:panelGroup></div>
					</hx:jspPanel>
					<h:inputHidden
						value="#{currentOfferHandler.isShowOrderPathOverlayPopUp}"
						id="showOverlayPopUp"></h:inputHidden>
					<!-- END On Exit Pop Area -->
							
					</h:form>




			</hx:scriptCollector>
			</div><%-- /tpl:put --%>
</DIV>
<%-- /tpl:put --%>
				<DIV id="clear"></DIV>
				<DIV id="bluebar">
					<a href="/feedback/feedback.jsp"
						style="color: white; font-weight: bold; text-decoration: none; font-size: 14px"
						title="Contact Us">Contact Us</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;1-800-872-0001&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a
						href="/faq/utfaq.jsp"
						style="color: white; font-weight: bold; text-decoration: none; font-size: 14px"
						title="Frequently Asked Questions">FAQ</a>
				</DIV>
				<%-- tpl:put name="footerMainArea" --%>
	<%-- tpl:put name="footerArea_1" --%>
					<%-- /tpl:put --%>
	<%=trackingBug.getOmnitureHTML() %>
		<%-- /tpl:put --%>
				<DIV id=graybar>
					Copyright
					<script>
						document.write(getCurrentYear());
					</script>
					USA TODAY, a division of <a href="http://www.gannett.com">Gannett
						Co. Inc.</a> <a href="/privacy/privacy.htm">Privacy
						Notice/California Privacy Notice</a>. <a
						href="/privacy/privacy.htm#adchoice">Ad Choices</a>.<br> By
					using this service, you accept our <a href="/service/service.jsp">Terms
						of Service</a>.
				</DIV>
				<DIV id=copyright>
					<P>Offers available to new subscribers only. Home delivery not
						available in some areas. Not valid with any other offer.
						Prepayment required. After the promotional period ends, the
						subscription will continue and be billed monthly at the then
						regular rate, less any applicable credits, unless USA TODAY is
						otherwise notified. Applicable taxes may not be included in the
						rate. If at any time you decide to cancel your subscription, you
						may contact customer service at 1-800-872-0001 and the full amount
						of any balance over $2.00 will be returned.</P>
				</DIV>
				<!--END OF COPYWRITE-->
				<DIV id=footer></DIV>
			</DIV>
			<!--END OF WHITEBOX-->
		</DIV>
		<!--END OF CONTENT-->
	</DIV>
</BODY>
</HTML><%-- /tpl:insert --%>
</f:view> <%-- /tpl:insert --%>