<%--<%@page import="com.usatoday.business.interfaces.products.SubscriptionOfferIntf"%>--%>
<%-- tpl:insert page="/theme/themeV3/jsfTheme/subscriberPortalNoLeftNav_Checkout_JSF.jtpl" --%><!DOCTYPE HTML><%@page import="com.usatoday.business.interfaces.products.ProductIntf"%>
<%@page import="com.usatoday.businessObjects.products.ProductBO"%>
<%@page import="com.usatoday.util.MobileDeviceDetectionUtil"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%><%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%><%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="com.usatoday.business.interfaces.products.promotions.HTMLPromotionIntf"%><f:view><f:loadBundle basename="resourceFile" var="labels" />
	<%-- tpl:insert page="/theme/subscriptionPortalRoot.htpl" --%><HTML>
<HEAD>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8">
<%-- tpl:put name="headerareaTop" --%>
<%
	String docProtocol = "https:";
	if (!request.isSecure()) { 
		docProtocol = "http:"; 
	}
// following work around to get around problem with RAD 8.0.2 automated links in templates - APE
	String subcribeJS = "javascript:_subscribeToPub();"; 
	String subcribeIntJS = "javascript:_subscribeToInternationalEdition();"; 
	String backIssueJS = "javascript:_shopUSAT();";
	
	// change this to any javascript you want called on page load
	String initFunction = "";
	String unloadFunction = "";
	String onBeforeUnloadFunction = "";
	String navigationSubscribeLinkValue = "/subscriptions/index.jsp";
	String electronicEditionLink = "/subscriptions/order/checkout.faces?pub=EE";
	String internationalEditionLink = "/international/welcomeint.jsp";
	String FAQLink = "/faq/utfaq.jsp";
	String subscribeByMail = "/subscriptions/subscribebymail.html";
	String dynamicNavigation = "";
	
	com.usatoday.esub.common.WebBug trackingBug = com.usatoday.esub.common.UTCommon.getWebAnalyticsBug();
	try {
		trackingBug.deriveValuesFromRequest(request);
	}
	catch (Exception e) {
		; // ignore
	}
	
	String pubCode = "";
	String productName = "USA TODAY";
	
    boolean isEEditionProduct = true;

	com.usatoday.businessObjects.products.promotions.PromotionSet defaultPromotionSet = null;
	com.usatoday.businessObjects.products.promotions.PromotionSet currentOfferPromotionSet = null;
	com.usatoday.business.interfaces.customer.CustomerIntf customer = null;
   	com.usatoday.businessObjects.products.promotions.PromotionSet currentAccountPromotionSet = null;
   		
	com.usatoday.business.interfaces.products.OfferIntf offer = null;   		
	try {
		offer = com.usatoday.esub.common.UTCommon.getCurrentOfferVersion2(request);
		pubCode = com.usatoday.util.constants.UsaTodayConstants.UT_PUBCODE;
		if (offer != null) {
			pubCode = offer.getPubCode();
			try {
				String brandingPubCode = offer.getProduct().getBrandingPubCode();
				ProductIntf bp = ProductBO.fetchProduct(brandingPubCode);
				productName = bp.getName();
			}
			catch (Exception teee) {
			}							
		}
		// get the default promotion set for the publication		
		defaultPromotionSet = com.usatoday.esub.common.UTCommon.getDefaultPromotionSetForPub(pubCode);
		
		// get the keycode specific promotion set
		
		if (offer != null && offer instanceof com.usatoday.business.interfaces.products.SubscriptionOfferIntf) {
			currentOfferPromotionSet = ((com.usatoday.business.interfaces.products.SubscriptionOfferIntf)offer).getPromotionSet();
		}
		boolean isSignedIn = false;
		customer = (com.usatoday.business.interfaces.customer.CustomerIntf)session.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_CUSTOMER_INFO);
		if (customer != null) {
			isSignedIn = true;
			com.usatoday.business.interfaces.products.SubscriptionProductIntf product = com.usatoday.businessObjects.products.SubscriptionProductBO.getSubscriptionProduct(customer.getCurrentEmailRecord().getPubCode());
			isEEditionProduct = product.isElectronicDelivery(); 
			// This promotion set is based on pub and keycode on the customer account
			currentAccountPromotionSet = com.usatoday.businessObjects.products.promotions.PromotionManager.getInstance().getPromotionsForOffer(customer.getCurrentAccount().getPubCode(), customer.getCurrentAccount().getKeyCode());
			if (isEEditionProduct) {
			}
		}

		if (pubCode.equalsIgnoreCase(com.usatoday.util.constants.UsaTodayConstants.SW_PUBCODE)) {
			navigationSubscribeLinkValue = "/subscriptions/order/checkout.faces";
			electronicEditionLink = "http://www.usatoday.com/marketing/brand_mkt/splash/spw_electronic_edition/index.html";
			internationalEditionLink = "/international/welcomeswint.jsp";
			FAQLink = "/faq/bwfaq.jsp";
			subscribeByMail = "/subscriptions/subscribebymail_sw.html";
		}
		
		if (currentOfferPromotionSet != null) {
			com.usatoday.business.interfaces.products.promotions.HTMLPromotionIntf dPromo = currentOfferPromotionSet.getDynamicNavigationOrderEntryHTML();
			if (dPromo != null) {
				dynamicNavigation = dPromo.getPromotionalHTML();
			}
		}
	} catch (Exception e) {
	
	}
	
	String mobileCSS = "";
	if (MobileDeviceDetectionUtil.isSmallScreenDevice(request)) {
		mobileCSS = "<LINK href=\"/theme/themeV3/css/smallScreen.css\" rel=\"stylesheet\" type=\"text/css\">";
	}
 %>
<script src="/common/prototype/prototype.js"></script>
<script src="/common/prototype/scriptaculous/scriptaculous.js" type="text/javascript"></script>
<link rel="stylesheet"	href="/theme/themeV3/css/stylesheet_jsfcomponents.css"	type="text/css">
<%-- tpl:put name="headerarea_Top" --%>
		<%--<%@page import="com.usatoday.business.interfaces.products.SubscriptionOfferIntf"%>--%>
		<!-- begin Marin Software Tracking Script for Elite SEM-->
		<!-- 		<script type='text/javascript'> -->
		<!-- 		var _marinClientId = "518c2822647"; -->
		<!-- 		var _marinProto = (("https:" == document.location.protocol) ? "https://" -->
		<!-- 				: "http://"); -->
		<!-- 		document.write(unescape("%3Cscript src='" + _marinProto -->
		<!-- 				+ "tracker.marinsm.com/tracker/" + _marinClientId -->
		<!-- 				+ ".js' type='text/javascript'%3E%3C/script%3E")); -->
		<!-- 	</script> -->
		<!-- 		<script type='text/javascript'> -->
		<!-- 		try { -->
		<!-- 			_marinTrack.trackPage(); -->
		<!-- 		} catch (err) { -->
		<!-- 		} -->
		<!-- 	</script> -->
		<!-- 		<noscript> -->
		<!-- 			<img -->
		<!-- 				src="https://tracker.marinsm.com/tp?act=1&cid=518c2822647&script=no"> -->
		<!-- 		</noscript> -->
		<!-- end Copyright Marin Software -->
		<%-- 			<f:verbatim>
				<h:outputText styleClass="outputText" id="textPageJS2"
					escape="false"
					value="#{currentOfferHandler.onePageJavaScript2Text}"></h:outputText>
				<h:outputText styleClass="outputText" id="textPageJS3"
					escape="false"
					value="#{currentOfferHandler.onePageJavaScript3Text}"></h:outputText>
			</f:verbatim>
 --%>
		<%

	String offerExpiredMessageClass = "none";
	String keyCode = request.getParameter("keycode");		

	initFunction += "initThisPage(thisObj, thisEvent);";
	onBeforeUnloadFunction += "; onbeforeunloadThisPage(thisObj, thisEvent);";
	unloadFunction += "; unloadThisPage(thisObj, thisEvent);";
	
	try {
		com.usatoday.business.interfaces.products.SubscriptionOfferIntf sOffer = (com.usatoday.business.interfaces.products.SubscriptionOfferIntf)offer;
		
		//System.out.println("Keycode on Request: '" + keyCode + "'");
		if (keyCode != null && keyCode.trim().length()==5) {
			//System.out.println("Offer keyCode: '" + offer.getKeyCode() + "'");
			if (!keyCode.equalsIgnoreCase(sOffer.getKeyCode())) {
				// user came in on different keycode then currently being offered.
				// show the offer expired message
				offerExpiredMessageClass = "inline";
			}			 
		}
			
	}
	catch (Exception eeee) {
	}		  
%>
		<title>USA TODAY - Checkout</title>
		<META http-equiv="PRAGMA" content="NO-CACHE">
		<META http-equiv="CACHE-CONTROL"
			content="no-store,NO-CACHE,must-revalidate,post-check=0,pre-check=0,max-age=0">
		<META http-equiv="Expires" content="-1">
	<%-- /tpl:put --%>
		<%-- /tpl:put --%>
<LINK REL="SHORTCUT ICON" HREF="/favicon.ico">
<LINK REL="icon" href="/favicon.ico" TYPE="image/ico" />
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META http-equiv="Content-Style-Type" content="text/css">
<meta name="viewport" content="initial-scale=1.0">
<LINK href="/theme/themeV3/css/usatodayBase.css" rel="stylesheet"
	type="text/css">
<LINK href="/theme/themeV3/css/usatodayAddOn.css" rel="stylesheet"
	type="text/css">
<script src="/common/usatCommon.js"></script>
<%-- tpl:put name="headareaBottom" --%>
<%=mobileCSS %>
<%-- tpl:put name="headarea_Bottom" --%>
		<script type="text/javascript" src="/common/videoWindow.js"></script>
		<script type="text/javascript" src="/common/keepalive.js"></script>
		<script type="text/javascript" src="/common/popUpOverlay.js"></script>
		<script language="JavaScript" src="/common/pubDateValidator.js"></script>
		<script type="text/javascript">
var checkoutGuaranteeDivElement;

function initThisPage(thisObj, thisEvent) {

	try {
		document.getElementById('offerExpiredSection').style.display = "<%=offerExpiredMessageClass%>";
	}
	catch (err2) {
	}

	usatOE_selectedTermRequiresEZPay = usatOE_isForceEZPayTermSelectedFunc();
	
	try {
		usatOE_updateDeliveryInfoPanelClass();
	}
	catch (err_2) {
	}

	try {
		var isForceBillMe = $('formOrderEntryForm:isForceBillMe').value;
		if (isForceBillMe != "true") {
			// not forced bill me but may be optional bill me
			var choseBillMe = $('formOrderEntryForm:radioPaymentMethod:1');
			
			if(choseBillMe.checked) {
				usatOE_updatePaymentPanelClass("B");
			}
			else {
				usatOE_updatePaymentPanelClass("CC");
			}
		}
		else {
			usatOE_updatePaymentPanelClass("B");
			usatOE_showRenewalOptions = false;
		}
	}
	catch (err3) {
		try {
			usatOE_updatePaymentPanelClass("CC");
		}
		catch (err_33) {
		}
	}

	try {
		usat_verifyDomain();
	}
	catch (err4) {
	}
	
	try {
	
		checkoutGuaranteeDivElement= $('checkoutGuaranteeDiv');
	
		new Draggable(checkoutGuaranteeDivElement);
	
	}	
	catch (e) {
		; // ignore
	}

	try {
		var showOverlayField = $('formOrderEntryForm:showOverlayPopUp');
	
		if (showOverlayField.value == "true") {
			keyCodeOverlayEnabled = true;
		}
	}
	catch (err) {
	} 

	startKeepAlive();
	
	//var paymentMethodLocal = '${newSubscriptionOrderHandler.paymentMethod}';
}
		
function onbeforeunloadThisPage(thisObj, thisEvent) {
	if( popOffer_programEnabled && keyCodeOverlayEnabled) {
		return popUpOnBeforeUnload(thisObj, thisEvent);
	}
}

function unloadThisPage(thisObj, thisEvent) {
	if (popOffer_programEnabled && keyCodeOverlayEnabled) {
		popOffer_unloadingPage = true;
		closeSpecialOffer(thisObj, thisEvent);
	}
}

function func_2(thisObj, thisEvent) {
	//use 'thisObj' to refer directly to this component instead of keyword 'this'
	//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	usatOE_updateDeliveryInfoPanelClass();
}
function func_3(thisObj, thisEvent) {
	//use 'thisObj' to refer directly to this component instead of keyword 'this'
	//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	
	window.open('/faq/utfaq.jsp#section6','FAQ', 'width=800,height=800,menubar=no,titlebar=yes,scrollbars=yes,status=no,location=no,directories=no');
}
// func_4 - enable/disable the credit card payment
function func_4(thisObj, thisEvent) {
	//use 'thisObj' to refer directly to this component instead of keyword 'this'
	//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	usatOE_updatePaymentPanelClass(thisObj.value);
	  
}


function func_7(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
 
 // validate the date
 dateChanged(thisObj);

}
function func_8(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'

popOffer_programEnabled = false;
}
function func_9(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'

// turn off keycode overlay since it pops in chromoe
//keyCodeOverlayEnabled = false;
popOffer_programEnabled = false;

thisObj.value = "Please Wait...";
return true;
}
function func_revalidateDelMethod(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	try {
		validateState();
		var delMethodVar = $('formOrderEntryForm:textDeliveryMethodTextDeterminedValue');
	
		delMethodVar.innerHTML = "Not Determined";
	}
	catch (e) {
		// ignore
	}
}
function validateState(){
	var stateVar = $('formOrderEntryForm:menuDeliveryState');
	var pub = "<%=pubCode %>";
	var isEEdition = "<%=isEEditionProduct%>";
	if (pub == "UT" && stateVar.value == "HI") {
		alert ("Thank you for your interest in subscribing to USA TODAY! All subscriptions to USA TODAY delivered in the state of Hawaii are handled by our partner, the Honolulu Star-Advertiser. For information about receiving delivery of USA TODAY in Hawaii, including pricing (which may vary from what is shown here), please contact the Honolulu Star-Advertiser's Customer Service Department by calling 808-538-NEWS (6397) from 5:30am to 5:00 pm Monday-Friday and 6:30am to 10:00 am on weekends (all times HST).");
		stateVar.value = "NONE";
		stateVar.focus();
		stateVar.select();
	}
}
function func_10(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	thisObj.value = "Please Wait...";

	popOffer_programEnabled = false;

	var windowOverlay = $('formSubmittedOverlay');

	windowOverlay.show();

	var windowOverlayInfo = $('formSubmittedOverlayInfo');
	windowOverlayInfo.show();
	
	setTimeout("usatOE_updateAnimation()", 200);
	
	return true;
}
</script>
	<%-- /tpl:put --%>
<script>
var currentPub = "<%=pubCode %>";

// method called on page load
function initPage(thisObj, thisEvent) {
	// insert site-wide code here

	// for product messageing in popUpOverlay.js
	usat_productName = "<%=productName %>";

	<%=initFunction %>
}
	function unloadPage(thisObj, thisEvent) {		
		<%=unloadFunction %>
	}
				
	function onBeforeUnloadPage(thisObj, thisEvent) {		 
		<%=onBeforeUnloadFunction %>
	}
	
	function subscribeToPub() {
		document.location = "<%=navigationSubscribeLinkValue%>";
	}	
	
	function subscribeToElectronicEdition() {
		document.location = "<%=electronicEditionLink %>";
	}
	
	function subscribeToElectronicEditionOrderPage() {
		document.location = "<%=electronicEditionLink %>";	
	}
	
	function subscribeToInternationalEdition() {
		document.location = "<%=internationalEditionLink%>";
	}

	function loadFAQ() {
		window.open('<%=FAQLink%>','FAQ', 'width=825,height=800,menubar=no,titlebar=yes,scrollbars=yes,status=no,location=no,directories=no');
	}

	function subscribeByMail() {
		document.location = "<%=subscribeByMail%>";
	}
</script>
<%-- /tpl:put --%>
<script type="text/javascript">
	/*   var _gaq = _gaq || [];
	 _gaq.push(['_setAccount', 'UA-23158809-1']);
	 _gaq.push(['_trackPageview']);

	 (function() {
	 var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	 ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	 var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	 })();
	 */
</script>
</HEAD>
<body>
	<DIV id="wrapper">
		<DIV id="header">
			<div id="headerLeft" onclick="templateLogoClick();"></div>
			<div id="headerRight">
				<%-- tpl:put name="headerbodyarea" --%>
					<%-- tpl:insert page="/login/loginform.jspf" --%><div style="float: none">
	<%
		String uri = request.getRequestURI();
		String pageName = uri.substring(uri.lastIndexOf("/") + 1);
		String jScript = "";
		if (uri.indexOf("subscriptions") > 0 && uri.indexOf("thankyou.jsp") > 0) {
			try {
				com.usatoday.esub.handlers.ShoppingCartHandler shoppingCartHandler = (com.usatoday.esub.handlers.ShoppingCartHandler) session
						.getAttribute("shoppingCartHandler");
				if (shoppingCartHandler != null) {
					jScript = shoppingCartHandler.getOnePageThankYouJavaScriptTealiumText();
				}
			} catch (Exception E) {

			}
		} else {
			com.usatoday.businessObjects.products.promotions.PromotionSet currentOfferPromotionSetTealium = null;
			com.usatoday.business.interfaces.products.OfferIntf offerTealium = com.usatoday.esub.common.UTCommon
					.getCurrentOffer(request);
			if (offerTealium != null && offerTealium instanceof com.usatoday.business.interfaces.products.SubscriptionOfferIntf) {
				currentOfferPromotionSetTealium = ((com.usatoday.business.interfaces.products.SubscriptionOfferIntf) offerTealium)
						.getPromotionSet();
				jScript = currentOfferPromotionSetTealium.getPromoOnePageJavaScriptTealiumText();
				jScript = org.apache.commons.lang3.StringUtils.replace(jScript, "page_name:\"\"", "page_name:\"" + uri + "\"");
			}
		}
	%>
	<%=jScript%>
</div>
<%-- 			<f:verbatim>
				<h:outputText styleClass="outputText" id="textThankYouJS13"	escape="false" value="#{shoppingCartHandler.tealiumOnePageThankYouJavaScriptText}"></h:outputText>
			</f:verbatim>
 --%>
<%
	String isLoginDisplay = "true";
	com.usatoday.business.interfaces.customer.CustomerIntf customer2 = null;
	String loggedinDivClass = "hideMe";
	String notLoggedinDivClass = "showMeInline";
	String firstName = "";
	String lastName = "";
	String firmInfo = "";
	String accountNum = "";
	String loginErrorMessage2 = "";
	String logOut = "";
	String publication = "";
	String changeAccountLink = "";
	String earlyAlertDisplay = "display:none";

	String jScriptNoOp = "javascript:;";

	try {
		publication = (String) session.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_PUB_NAME);
		if (publication == null) {
			publication = request.getParameter("pub");
			if (publication == null) {
				publication = com.usatoday.util.constants.UsaTodayConstants.UT_PUBCODE;
			}
		}
		if (publication.equals(com.usatoday.esub.common.UTCommon.PUB_NAMES[0])) { // USATODAY publication
			logOut = com.usatoday.esub.common.UTCommon.BASE_URL + "/index.jsp?pub=UT&logsessobj=rem";
		} else {
			logOut = com.usatoday.esub.common.UTCommon.BASE_URL + "/index.jsp?pub=BW&logsessobj=rem";
		}

		// If customer is logged in remove login page, otherwise display it
		customer2 = (com.usatoday.business.interfaces.customer.CustomerIntf) session
				.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_CUSTOMER_INFO);
		if (customer2 == null) {
			// showMeInling and hideMe are defined in style sheets
			loggedinDivClass = "showMeInline";
			notLoggedinDivClass = "hideMe";
		} else {

			if (customer2.getCurrentAccount() != null) {
				firstName = customer2.getCurrentAccount().getDeliveryContact().getFirstName();
				if (firstName != null && !firstName.trim().equals("")) {
					firstName = com.usatoday.esub.common.UTCommon.formatNameCapitalization(firstName); // Format it
				}
				lastName = customer2.getCurrentAccount().getDeliveryContact().getLastName();
				if (lastName != null && !lastName.trim().equals("")) {
					lastName = com.usatoday.esub.common.UTCommon.formatNameCapitalization(lastName); // Format it
				}
				firmInfo = customer2.getCurrentAccount().getDeliveryContact().getFirmName();
				if (customer2.getNumberOfAccounts() > 1) {
					changeAccountLink = "[&nbsp;<a href=\"/account/select_accnt.jsp\">Change</a>&nbsp;]";
				}
				try {
					accountNum = customer2.getCurrentAccount().getAccountNumber();
					//				if (accountNum == null || accountNum.length() < 4) {
					if (accountNum == null) {
						accountNum = "";
					}
					//				else {
					/*
					if (accountNum.equalsIgnoreCase("340000000")) {
						accountNum = "Pending Setup";
					}
					 */
					//				}
				} catch (Exception eeee) {
					eeee.printStackTrace();
				}

				//EarlyAlert - Check to see if their paper delivery has been affected by a delay
				// HD-CONS109 SEW
				/*
				if (customer2.getCurrentAccount().isShowDeliveryAlert()) {
				earlyAlertDisplay = "display:inline";
				}
				 */

			} else {
				if (customer2.getCurrentEmailRecord() != null
						&& customer2.getCurrentEmailRecord().getEmailAddress() != null) {
					firstName = customer2.getCurrentEmailRecord().getEmailAddress();
					accountNum = "Pending Setup";
				} else {
					firstName = "";
				}
			}
		}
		loginErrorMessage2 = (String) session.getAttribute("LOGIN_FAILED_MESSAGE");
		if (loginErrorMessage2 == null || loginErrorMessage2.trim().equals("")) {
			loginErrorMessage2 = "";
		} else {
			session.removeAttribute("LOGIN_FAILED_MESSAGE");
		}
	} catch (Exception e) {
		e.printStackTrace();
		if (firstName == null) {
			firstName = "";
		}
		if (lastName == null) {
			lastName = "";
		}
	}
	String nameInfo = firstName + " " + lastName;
	String loginInfo = null;
	if (!nameInfo.trim().equals("")) {
		loginInfo = nameInfo;
	} else {
		if (firmInfo != null && !firmInfo.trim().equals("")) {
			loginInfo = firmInfo;
		}
	}
	// if no login info, then hide data
	if (loginInfo == null) {
		loggedinDivClass = "showMeInline";
		notLoggedinDivClass = "hideMe";
		loginInfo = "";
	}
%>

<%@page import="com.usatoday.util.constants.UsaTodayConstants"%><div
	id="loginDisplayDiv" class="<%=loggedinDivClass%>">
	<table border="0" width="100%" height="60px">
		<tr>
			<td>
				<TABLE class="loginFormTable">
					<TBODY>
						<TR>
							<TD class="loginFormColumn"><br /> <a
								href="/login/auth.faces">Login to Manage Account</a></TD>
						</TR>
						<tr>
							<td class="loginFormColumn" align="right"><img
								style="cursor: pointer; margin-top: 10px;"
								onclick="javascript: templateClickToChatClickedGuest(this, event);"
								alt="Click for Assistance" title="Click for Assistance"
								src="${pageContext.request.contextPath}/theme/themeV3/themeImages/chatIcon136.png"
								hspace="0" vspace="5"></td>
						</tr>
					</TBODY>
				</TABLE></td>
		</tr>
	</table>
</div>
<div id="loginHideDiv" class="<%=notLoggedinDivClass%>">
	<table width="100%" border="0" class="loginFormTable">
		<tr>
			<td nowrap align="right">
				<TABLE class="loginFormTable">
					<TBODY>
						<TR>
							<TD class="loginFormColumn" nowrap><span
								class="loginNormalPrint">Hello, <%=loginInfo%>&nbsp;&nbsp;[&nbsp;<a
									href="<%=jScriptNoOp%>"
									onclick="javascript:document.location='<%=logOut%>';_unloadPage(this, event);">Logout</a>&nbsp;]</span>
							</TD>
						</TR>
					</TBODY>
				</TABLE></TD>
		</tr>
		<tr>
			<td class="loginFormColumn" align="right"><span
				class="loginfineprint">Account #:&nbsp;<%=accountNum%>&nbsp;<%=changeAccountLink%></span>
			</td>
		</tr>
		<tr>
			<td class="loginFormColumn" align="right"><div
					id="chatIconRequestDiv">
					<img onclick="javascript: templateClickToChatClicked(this, event);"
						style="cursor: pointer; margin-top: 10px;"
						alt="Click for Assistance" title="Click for Assistance"
						src="${pageContext.request.contextPath}/theme/themeV3/themeImages/chatIcon136.png"
						hspace="0" vspace="5">
				</div>
			</td>
		</tr>
	</table>
</div>
<div id="templateChatWindowDiv" style="display: none;">
	<div id="templateChatTopDiv">
		<span onclick="templateClickToChatHide();" style="cursor: pointer;">No
			Thanks<img src="/theme/themeV3/themeImages/close_14x14.png"
			width="14" height="14" border="0"
			style="margin-left: 5px; margin-right: 5px" /> </span>
	</div>
<!-- 	<div id="tempateChatImageDiv">
		<a
			onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
			href=""><img border="0"
			src="https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY" />
		</a>
	</div>
 -->	<div id="templateChatAccountLinksDiv">
		<ul>
			<li>To report a problem with delivery, click <a
				href="/account/cp/delissue.faces"> here</a>.</li>
			<li>To place your delivery on a temporary hold, click <a
				href="/account/vacationholds/holds.faces"> here</a>.</li>
			<li>Forgot your password or login? Click <a
				href="/idpassword/forgotpassword.jsp"> here</a>.
			<li>To contact us <a href="/feedback/feedback.jsp"> click here</a>.</li>				
<!-- 			<li>To cancel your subscription, please call 1-800-872-0001 and -->
<!-- 				select option 6.</li> -->
		</ul>
	</div>

	<div id="templateChatLinksDiv">
		<ul>

			<li><a
				onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
				href="">Click to Chat</a>
			</li>
		</ul>
	</div>
	<div id="templateChatBottomDiv">
		<span class="outputText">Or, call us at:<br> <b>Call
				1-877-713-6241</b><br>(M-F 8:00 a.m. to 7 p.m. E.T.)</span>
	</div>
</div>
<div id="templateChatWindowGuestDiv" style="display: none;">
	<div id="templateChatTopDiv">
		<span onclick="templateClickToChatHideGuest();"
			style="cursor: pointer;">No Thanks<img
			src="/theme/themeV3/themeImages/close_14x14.png" width="14"
			height="14" border="0" style="margin-left: 5px; margin-right: 5px" />
		</span>
	</div>
<!-- 	<div id="tempateChatImageDiv">
		<a
			onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
			href=""><img border="0"
			src="https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY" />
		</a>
	</div>
 -->	<div id="templateChatAccountLinksDiv">
		<ul>
			<li>To manage your account online - including reporting a
				problem with delivery or placing a temporary hold on your account -
				log in by entering your e-mail address and password. Click <a
				href="/login/auth.faces"> here</a> to access the login page now.</li>
			<li>Forgot your password or login? <a
				href="/idpassword/forgotpassword.jsp"> click here</a>.</li>
			<li>To contact us <a href="/feedback/feedback.jsp"> click here</a>.</li>
			<!-- 			<li>To cancel your subscription, please call 1-800-872-0001 and -->
			<!-- 				select option 6.</li> -->
		</ul>

	</div>

	<div id="templateChatLinksDiv">
		<ul>
			<li><a
				onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
				href="">Click to Chat</a>
			</li>
		</ul>
	</div>
	<div id="templateChatBottomDiv">
		<span class="outputText">Or, call us at:<br> <b>Call
				1-877-713-6241</b><br>(M-F 8:00 a.m. to 7 p.m. E.T.)</span>
	</div>
</div>
<%-- /tpl:insert --%>			
		<%-- /tpl:put --%>
			</div>
		</DIV>
		<!-- end header -->
		<DIV id="content">
			<DIV id="whitebox">
				<%-- tpl:put name="bodyAreaMainWhiteBox" --%>
<DIV id=nav>
	<DIV id="navPromo">
		<%-- tpl:put name="navigationArea_Top" --%>
								<!-- Insert Page Level top Nav Elements -->
							<%-- /tpl:put --%>
	</div>
	<DIV id=leftNavList>
		<UL>
		  <LI><A href="/products.html">USA TODAY Products</A></LI>
		  <LI><A href="/account/accounthistory/accountSummary.faces">Manage account</A></LI>
		  <LI><A href="/subscriptions/index.jsp?pub=EE">Electronic Newspaper</A></LI>
	
		  <%=dynamicNavigation %> 
		</UL>
	</DIV>
	<DIV id="navPromo">
		<%-- tpl:put name="navigationArea_Bottom" --%>
								<hx:scriptCollector id="scriptCollectorBottomNavCollector"
									preRender="#{pc_Checkout.onPageLoadBegin}">
									<h:form id="formBottomNavForm" styleClass="form">
										<h:panelGrid styleClass="panelGrid"
											id="gridLeftColHTMLSpot1Grid" columns="1"
											style="text-align: center" width="160">
											<h:outputText styleClass="" id="leftColHTMLSpot1Text"
												value="#{currentOfferHandler.onePgLftClSpt1HTM}"
												escape="false"></h:outputText>
										</h:panelGrid>
										<hx:jspPanel id="jspPanelBottomNavCustomPanel"
											rendered="#{trialPartnerHandler.isShowLogoImage}">
											<hx:outputLinkEx styleClass="outputLinkEx"
												id="linkExTopNavLogoLink"
												value="#{trialPartnerHandler.parnterLogoLinkURL}"
												target="_blank"
												rendered="#{trialPartnerHandler.isShowLogoImageURL}"
												style="background-color: white; z-index: 50; margin: 0px; padding: 0px; border-width: 0px">
												<hx:graphicImageEx styleClass="graphicImageEx"
													id="imageExPartnerLogo"
													value="#{trialPartnerHandler.partner.logoImageURL}"
													style="z-index: 50" border="0" width="155" hspace="0"
													vspace="0"></hx:graphicImageEx>
											</hx:outputLinkEx>
											<hx:graphicImageEx styleClass="graphicImageEx"
												id="imageExPartnerLogoNoLink"
												value="#{trialPartnerHandler.partner.logoImageURL}"
												style="z-index: 50" border="0" width="155" hspace="0"
												vspace="0"
												rendered="#{not trialPartnerHandler.isShowLogoImageURL}"></hx:graphicImageEx>
										</hx:jspPanel>

										<h:panelGrid id="leftColImageSpot1"
											rendered="#{currentOfferHandler.renderLeftColumnImageSpot1}">
											<hx:outputLinkEx styleClass="outputLinkEx"
												id="linkExLeftColImageSpot1Link" target="_blank"
												style="border-width: 0px"
												value="#{currentOfferHandler.onePageLeftColumnImageSpot1ImageOnClickURL}"
												rendered="#{currentOfferHandler.renderLeftColmnImageSpot1ImageAsClickable }">
												<hx:graphicImageEx styleClass="" id="imageExLeftColImage1"
													value="#{currentOfferHandler.onePageLeftColumnImageSpot1ImagePath}"
													border="0"
													alt="#{currentOfferHandler.onePageLeftColumnImageSpot1ImageAltText}"
													title="#{currentOfferHandler.onePageLeftColumnImageSpot1ImageAltText}"
													style="padding-left: 0px" align="center">
												</hx:graphicImageEx>
											</hx:outputLinkEx>
											<hx:graphicImageEx styleClass=""
												id="imageExLeftColImage1NoLink"
												value="#{currentOfferHandler.onePageLeftColumnImageSpot1ImagePath}"
												border="0"
												alt="#{currentOfferHandler.onePageLeftColumnImageSpot1ImageAltText}"
												title="#{currentOfferHandler.onePageLeftColumnImageSpot1ImageAltText}"
												style="padding-left: 0px"
												rendered="#{not currentOfferHandler.renderLeftColmnImageSpot1ImageAsClickable }">
											</hx:graphicImageEx>
										</h:panelGrid>

										<h:panelGrid styleClass="panelGrid" id="gridVideoGridLeftCol"
											columns="1" style="text-align: center" width="160"
											rendered="#{currentOfferHandler.renderLeftColumnVideoSpot1}">

											<hx:outputLinkEx styleClass="outputLinkEx"
												id="linkExVideoLink_LeftCol"
												onclick="return openVideoScreen('#{currentOfferHandler.onePageLeftColumnVideoSpot1ImageOnClickURL}');"
												value=";"
												style="background-color: white; border-style: none"
												title="#{currentOfferHandler.onePageLeftColumnVideoSpot1ImageAltText}"
												tabindex="100">
												<hx:graphicImageEx styleClass="graphicImageEx"
													id="imageExVideoSweepsGraphic_LeftCol"
													value="#{currentOfferHandler.onePageLeftColumnVideoSpot1ImagePath}"
													border="0" hspace="0" vspace="4"
													style="margin: 0px; float: left; text-align: center"
													title="#{currentOfferHandler.onePageLeftColumnVideoSpot1ImageAltText}"></hx:graphicImageEx>
											</hx:outputLinkEx>
											<h:outputText styleClass="outputText"
												id="textEEVideoTextLeftCol"
												value="#{currentOfferHandler.onePgLtClVdSpt1HT}"
												escape="false"></h:outputText>
										</h:panelGrid>
										<h:panelGrid id="leftColImageSpot2"
											rendered="#{currentOfferHandler.renderLeftColumnImageSpot2}">
											<hx:outputLinkEx styleClass="outputLinkEx"
												id="linkExLeftColImageSpot2Link" style="border-width: 0px"
												value="#{currentOfferHandler.onePageLeftColumnImageSpot2ImageOnClickURL}"
												rendered="#{currentOfferHandler.renderLeftColmnImageSpot2ImageAsClickable }">
												<hx:graphicImageEx styleClass="" id="imageExLeftColImage2"
													value="#{currentOfferHandler.onePageLeftColumnImageSpot2ImagePath}"
													border="0"
													alt="#{currentOfferHandler.onePageLeftColumnImageSpot2ImageAltText}"
													title="#{currentOfferHandler.onePageLeftColumnImageSpot2ImageAltText}"
													style="padding-left: 0px">
												</hx:graphicImageEx>
											</hx:outputLinkEx>
											<hx:graphicImageEx styleClass=""
												id="imageExLeftColImage2NoLink"
												value="#{currentOfferHandler.onePageLeftColumnImageSpot2ImagePath}"
												border="0"
												alt="#{currentOfferHandler.onePageLeftColumnImageSpot2ImageAltText}"
												title="#{currentOfferHandler.onePageLeftColumnImageSpot2ImageAltText}"
												style="padding-left: 0px"
												rendered="#{not currentOfferHandler.renderLeftColmnImageSpot2ImageAsClickable }">
											</hx:graphicImageEx>
										</h:panelGrid>
									</h:form>
								</hx:scriptCollector>
							<%-- /tpl:put --%>
	</div>
</DIV>
<DIV id=pageContent>
	<%-- tpl:put name="MainBodyArea" --%>
							<hx:scriptCollector id="scriptCollectorMainOrderEntryCollector"
								preRender="#{pc_Checkout.onPageLoadBegin}"
								postRender="#{pc_Checkout.onPageLoadEnd}">
								<h:form styleClass="form" id="formOrderEntryForm">
									<h1>
										<h:outputText id="textMainTableHeader"
											value="Subscribe To #{currentOfferHandler.productName}"
											styleClass="H1Class"></h:outputText>
									</h1>
									<h:panelGrid styleClass="panelGrid checkOutPageMainGrid"
										id="gridMainBodyGrid" columns="1">
										<hx:jspPanel id="jspPanelFormPanel">

											<div id="pageContentEEOrderEntryLeft">
												<f:verbatim>
													<DIV id="offerExpiredSection"
														style="margin-bottom: 10px; display: none;">
														<TABLE border="1" cellpadding="2">
															<TBODY>
																<TR>
																	<TD align="center"><B>This offer has expired.<br>We
																			apologize for any inconvenience. Please check the
																			current offer terms below.</B></TD>
																</TR>
															</TBODY>
														</TABLE>

													</DIV>
												</f:verbatim>
												<div id="headerPromoTextDiv">
													<h:outputText id="textPromoText1" escape="false"
														value="#{currentOfferHandler.onePagePromotionText1}"></h:outputText>
												</div>
												<div id="headerDelvMethodTextDiv">
													<h:outputText id="textDelvMethodText" escape="false"
														value="#{newSubscriptionOrderHandler.deliveryMethodErrorText}"
														style="color: red"></h:outputText>
												</div>
												<div id="orderFormBox">

													<hx:panelLayout styleClass="panelLayoutOrderForm"
														id="layoutPageLayout">
														<f:facet name="body">
															<h:panelGroup styleClass="panelGroup"
																id="groupMainBodyPanel" style="margin-bottom: 5px;">

																<h:panelGrid id="gridTermsGrid" columns="1"
																	styleClass="orderFormGridStyles">
																	<f:facet name="header">
																		<h:panelGroup styleClass="panelGroup" id="group10"
																			style="width: 100%; text-align: left">
																			<h:outputText styleClass="outputText footnote"
																				id="text7"
																				value='<span style="color:red">*</span> Required Fields'
																				escape="false"></h:outputText>
																		</h:panelGroup>
																	</f:facet>
																	<hx:panelFormBox helpPosition="over"
																		labelPosition="left"
																		styleClass="panelFormBoxTerms orderFormInsideGridStyles"
																		id="formBoxTermsFormBox" label="#{labels.termLabel}"
																		widthLabel="15"
																		rendered="#{not premiumPromotionHandler.orderHasPremiumPromotion }">
																		<hx:formItem styleClass="formItem"
																			id="formItemSubscriptionTerms"
																			errorText="#{labels.requiredFieldMsg}">
																			<h:selectOneRadio
																				disabledClass="selectOneRadio_Disabled"
																				enabledClass="selectOneRadio"
																				styleClass="selectOneRadio" id="radioTermsSelection"
																				layout="pageDirection" required="true" tabindex="1"
																				requiredMessage="#{labels.termRequiredErrorMsg}"
																				value="#{newSubscriptionOrderHandler.selectedTerm}"
																				validatorMessage="Please choose one of the available terms."
																				onclick="return usatOE_selectedTermChanged(this, event);">
																				<hx:inputHelperAssist
																					errorClass="selectOneRadio_Error" id="assist1" />
																				<f:selectItems
																					value="#{currentOfferHandler.offerTermsSelectItems}" />
																			</h:selectOneRadio>
																			<h:message for="radioTermsSelection"></h:message>

																		</hx:formItem>
																		<f:facet name="bottom">
																			<h:panelGrid styleClass="panelGrid"
																				id="gridTermsFormBoxBottomFacetGrid"
																				style="text-align: left" columns="2" cellspacing="0"
																				cellpadding="1" rendered="false">
<%-- 																				rendered="#{currentOfferHandler.isForceEZPAYRate or currentOfferHandler.isForceEZPAY }"> --%>
																				<f:facet name="footer">
																					<h:panelGroup styleClass="panelGroup" id="group4">
																						<h:outputText styleClass="outputText"
																							id="textEZPayExplanationRateCustomText"
																							style="color: navy; font-family: Arial; font-weight: bold; font-size: 10px"
																							value="#{currentOfferHandler.forceEZPayCustomTermsText}"
																							escape="false"></h:outputText>
																					</h:panelGroup>
																				</f:facet>
																				<h:outputText styleClass="outputText"
																					id="textEZPayExplanationMessage"
																					style="color: navy; font-family: Arial; font-weight: bold; font-size: 10px"
																					value="#{labels.EZPayPlanTextUnderTermsMultiPub}"
																					escape="false"></h:outputText>
																				<h:outputText styleClass="outputText"
																					id="textEZPayLearnMore"
																					value="#{currentOfferHandler.addressLearnMoreHTML}"
																					escape="false"
																					style="color: #00529b; cursor: pointer"></h:outputText>

																			</h:panelGrid>
																		</f:facet>
																		<f:facet name="top">
																			<hx:panelSection styleClass="panelSection"
																				id="sectionNewOfferCode1" initClosed="true"
																				rendered="#{currentOfferHandler.isShowOfferOverride}"
																				style="margin-top: 10px">
																				<f:facet name="closed">
																					<hx:jspPanel id="jspPanel4">
																						<h:outputText
																							id="textOfferOverrridePanelHeaderClosedText"
																							styleClass="outputText"
																							style="padding-left: 15px"
																							value="#{labels.OrderPageOfferOverrideClosedLabel}"></h:outputText>
																					</hx:jspPanel>
																				</f:facet>
																				<f:facet name="opened">
																					<hx:jspPanel id="jspPanel3">

																						<h:outputText id="text2" styleClass="outputText"
																							style="padding-left: 15px"
																							value="#{labels.OrderPageOfferOverrideOpenLabel}"></h:outputText>
																					</hx:jspPanel>
																				</f:facet>
																				<h:panelGrid
																					styleClass="panelGrid orderFormInsideGridStyles"
																					id="gridOffOverrideGrid">
																					<hx:panelFormBox labelPosition="left"
																						styleClass="panelFormBox"
																						id="formBoxOfferCodeEntry1" widthLabel="125"
																						helpPosition="over">

																						<hx:formItem styleClass="formItem" id="formItem1"
																							infoText="#{labels.OfferCodeInformation}"
																							label="#{labels.OfferCodeInputLabel} :">
																							<h:inputText styleClass="inputTextOrderForm"
																								id="textNewOfferCode" size="8"
																								value="#{newSubscriptionOrderHandler.customerEnteredOfferCode}"
																								maxlength="8">
																								<f:validateLength minimum="3" maximum="5"></f:validateLength>
																								<hx:validateConstraint regex="^[A-Za-z0-9]+$" />
																								<hx:inputHelperAssist
																									errorClass="inputText_Error" validation="true"
																									id="assist7"
																									onfocusSuccessClass="inputTextFocusOrderForm "
																									successClass="inputTextOrderForm " />
																							</h:inputText>
																							<hx:commandExButton type="submit"
																								styleClass="commandExButton"
																								id="buttonProcessOfferCode"
																								action="#{pc_Checkout.doButtonProcessOfferCodeAction}"
																								immediate="true" style="padding-left: 5px"
																								value="#{labels.Apply}">
																								<f:param
																									value="#{newSubscriptionOrderHandler.customerEnteredOfferCode}"
																									name="custOfferCodeParm" id="custOfferCodeParm"></f:param>
																							</hx:commandExButton>
																						</hx:formItem>
																						<f:facet name="right">
																							<h:panelGrid styleClass="panelGrid"
																								id="gridNewOfferBottomGrid1" width="100%"
																								columns="2" style="text-align: left">


																								<h:commandLink styleClass="commandLink"
																									id="linkRevertOfferLink"
																									rendered="#{currentOfferHandler.showPreviousOfferRevertLink}"
																									action="#{pc_Checkout.doLinkRevertOfferLinkAction}"
																									immediate="true" style="padding-left: 5px"
																									onclick="return func_8(this, event);">
																									<h:outputText id="textRevertOfferLinkText"
																										styleClass="outputText" style="font-size: 9pt"
																										value="#{labels.OrderPageShowPreviousOffer}"></h:outputText>
																								</h:commandLink>
																							</h:panelGrid>
																						</f:facet>
																					</hx:panelFormBox>
																				</h:panelGrid>

																			</hx:panelSection>
																		</f:facet>
																	</hx:panelFormBox>
																	<h:panelGrid id="gridPremiumPromotionGrid" columns="1"
																		rendered="#{premiumPromotionHandler.orderHasPremiumPromotion }"
																		styleClass="orderFormGridStyles">
																		<f:facet name="header">
																			<h:panelGroup styleClass="panelGroup"
																				id="groupPromoTermsHeaderGroup8" layout="block"
																				style="width: 95%; text-align: left">
																				<h:outputText
																					styleClass="panelFormBoxOrderEntryForm_Header"
																					id="textPrmotionsTermLabel"
																					value="#{labels.termLabel}"></h:outputText>
																			</h:panelGroup>
																		</f:facet>
																		<h:panelGrid styleClass="panelGrid"
																			id="gridPremiumPromotionTermsGrid" columns="2"
																			columnClasses="panelFormBox_Label-Cell,panelFormBox_Content-Cell">
																			<h:outputText styleClass="outputText"
																				id="textPromoDescriptionLabel"
																				value="#{labels.Description}:"
																				style="font-weight: bold"></h:outputText>
																			<h:outputText styleClass="outputText"
																				id="textPromoDescription"
																				value="#{premiumPromotionHandler.promotionDescription}"></h:outputText>
																			<h:outputText styleClass="outputText"
																				id="textSelectedTermLabel"
																				value="#{labels.termLabel2}:"
																				style="font-weight: bold"></h:outputText>
																			<h:outputText styleClass="outputText"
																				id="textSelectedTerm"
																				value="#{newSubscriptionOrderHandler.selectedTermText}"
																				escape="false"></h:outputText>
																			<h:outputText styleClass="outputText" id="text5"></h:outputText>
																			<hx:outputLinkEx styleClass="outputLinkEx"
																				id="linkExPromoPage"
																				value="#{premiumPromotionHandler.promotion.landingPage}">
																				<h:outputText id="text6" styleClass="outputText"
																					value="(Back to Offer Page)"></h:outputText>
																			</hx:outputLinkEx>
																		</h:panelGrid>
																	</h:panelGrid>
																</h:panelGrid>
																<!-- end of terms grid -->



																<h:panelGrid styleClass="panelGrid orderFormGridStyles"
																	id="gridQuantityInformation" columns="1"
																	rendered="false">
																	<hx:panelFormBox helpPosition="under"
																		labelPosition="left" styleClass="panelFormBox"
																		id="formBoxQuantityInformation">
																		<hx:formItem styleClass="formItem"
																			id="formItemQuantityInformation"
																			label="Number Of Copies:">
																			<h:selectOneMenu styleClass="selectOneMenu"
																				id="menuOrderQuantity"
																				value="#{newSubscriptionOrderHandler.quantity}">

																				<f:selectItems
																					value="#{currentOfferHandler.orderQtySelectItems}"
																					id="selectItems2" />
																				<hx:inputHelperAssist
																					errorClass="selectOneMenu_Error"
																					successClass="inputTextOrderForm "
																					onfocusSuccessClass="inputTextFocusOrderForm " />
																			</h:selectOneMenu>
																			<h:outputText escape="false"
																				id="textNumCopiesLearnMoreLink"
																				value="#{currentOfferHandler.addressLearnMoreHTML}"
																				style="margin-left: 10px; color: #00529b; cursor: pointer"></h:outputText>
																		</hx:formItem>
																	</hx:panelFormBox>
																</h:panelGrid>

																<h:panelGrid styleClass="panelGrid orderFormGridStyles"
																	id="gridPartnerInformation" columns="1"
																	rendered="#{(trialCustomerHandler.currentInstance.isShowClubNumber) || (currentOfferHandler.currentOffer.clubNumberRequired)}">
																	<hx:panelFormBox helpPosition="under"
																		labelPosition="left" styleClass="panelFormBox"
																		id="formBoxPartnerInformation"
																		label="#{newSubscriptionOrderHandler.partnerName} Program Information">
																		<hx:formItem styleClass="formItem"
																			id="formItemPartnerClubNumber"
																			label="#{trialCustomerHandler.currentInstance.clubNumberLabel}"
																			infoText="(Enter your member number, claim code or college alumni program here.)">
																			<h:inputText styleClass="inputTextOrderForm"
																				id="textClubNumber"
																				value="#{newSubscriptionOrderHandler.clubNumber}"
																				size="20" tabindex="10" maxlength="15">
																				<hx:inputHelperAssist errorClass="inputText_Error"
																					onfocusSuccessClass="inputTextFocusOrderForm "
																					successClass="inputTextOrderForm "
																					validation="true" />
																			</h:inputText>

																			<h:outputText escape="false"
																				id="textProgInfoLearnMoreLink"
																				value="#{currentOfferHandler.whatsThisHTML}"
																				style="margin-left: 10px; color: #00529b; cursor: pointer"></h:outputText>

																		</hx:formItem>
																	</hx:panelFormBox>
																</h:panelGrid>

																<h:panelGrid styleClass="panelGrid orderFormGridStyles"
																	id="gridStartDateInformation" columns="1"
																	rendered="#{not trialCustomerHandler.isTrialConversion }">
																	<hx:panelFormBox helpPosition="under"
																		labelPosition="left"
																		styleClass="panelFormBoxOrderEntryForm"
																		id="formBoxStartDateInformation">

																		<hx:formItem styleClass="formItem"
																			id="formItemStartDateFormItem"
																			label=' #{labels.SubStartDate} <span style="color:red">*</span>'>

																			<h:inputText styleClass="inputTextOrderForm"
																				id="textStartDateInput" size="12"
																				value="#{newSubscriptionOrderHandler.startDate}"
																				tabindex="11" onchange="return func_7(this, event);"
																				required="true"
																				requiredMessage="Subscription Start Date is Required.">
																				<hx:convertDateTime pattern="MM/dd/yyyy" />
																				<hx:inputHelperDatePicker id="datePicker1"
																					multiLine="false" />
																				<hx:validateDateTimeRange
																					maximum="#{currentOfferHandler.latestPossibleStartDate}"
																					minimum="#{currentOfferHandler.earliestPossibleStartDate}" />
																				<hx:inputHelperAssist errorClass="inputText_Error"
																					onfocusSuccessClass="inputTextFocusOrderForm "
																					successClass="inputTextOrderForm "
																					validation="true" />
																			</h:inputText>
																			<h:outputText escape="false"
																				id="textStartDateLearnMore"
																				value="#{currentOfferHandler.addressLearnMoreHTML}"
																				style="margin-left: 15px; color: #00529b; cursor: pointer"
																				styleClass="requestLink"></h:outputText>

																		</hx:formItem>

																	</hx:panelFormBox>
																	<hx:ajaxRefreshSubmit
																		target="formBoxStartDateInformation"
																		id="ajaxRefreshSubmitStartDate"></hx:ajaxRefreshSubmit>
																</h:panelGrid>

																<h:panelGrid styleClass="panelGrid orderFormGridStyles"
																	id="gridDeliveryInformation" columns="1">
																	<hx:panelFormBox helpPosition="under"
																		labelPosition="left"
																		styleClass="panelFormBoxOrderEntryForm"
																		id="formBoxDeliveryInformation"
																		label="#{labels.emailAddressRecipientLabel}">

																		<hx:formItem styleClass="formItem"
																			id="formItemDeliveryFName"
																			label='#{labels.firstNameLabel} <span style="color:red">*</span>'
																			errorText="#{labels.requiredFieldMsg}">

																			<h:inputText id="textDeliveryFirstName" tabindex="20"
																				value="#{newSubscriptionOrderHandler.deliveryFirstName}"
																				maxlength="10" required="true"
																				requiredMessage="First Name is Required"
																				validatorMessage="First Name is Not Valid"
																				styleClass="inputTextOrderForm">
																				<f:validateLength minimum="1" maximum="15"></f:validateLength>
																				<hx:inputHelperAssist errorClass="inputText_Error"
																					onfocusSuccessClass="inputTextFocusOrderForm "
																					successClass="inputTextOrderForm "
																					validation="true" />
																			</h:inputText>

																		</hx:formItem>
																		<hx:formItem styleClass="formItem"
																			id="formItemDeliveryLastName"
																			label='#{labels.lastNameLabel} <span style="color:red">*</span>'
																			errorText="#{labels.requiredFieldMsg}">

																			<h:inputText styleClass="inputTextOrderForm"
																				id="textDeliveryLName" tabindex="21"
																				value="#{newSubscriptionOrderHandler.deliveryLastName}"
																				maxlength="15" required="true"
																				requiredMessage="Last Name is Required"
																				validatorMessage="Last Name is Not Valid">
																				<f:validateLength minimum="1" maximum="20"></f:validateLength>
																				<hx:inputHelperAssist errorClass="inputText_Error"
																					onfocusSuccessClass="inputTextFocusOrderForm "
																					successClass="inputTextOrderForm "
																					validation="true" />
																			</h:inputText>

																		</hx:formItem>
																		<hx:formItem styleClass="formItem"
																			id="formItemDeliveryCompanyName"
																			label="#{labels.companyNameLabel}">
																			<h:inputText styleClass="inputTextOrderForm"
																				id="textDeliveryCompanyName" tabindex="22"
																				value="#{newSubscriptionOrderHandler.deliveryCompanyName}"
																				maxlength="28">
																				<f:validateLength maximum="40"></f:validateLength>
																				<hx:inputHelperAssist errorClass="inputText_Error"
																					onfocusSuccessClass="inputTextFocusOrderForm "
																					successClass="inputTextOrderForm "
																					validation="true" />
																			</h:inputText>
																		</hx:formItem>

																		<hx:formItem styleClass="formItem"
																			id="formItemDeliveryAddress1"
																			label='#{labels.address} <span style="color:red">*</span>'
																			errorText="#{labels.requiredFieldMsg}">
																			<h:inputText styleClass="inputTextOrderForm"
																				id="textDeliveryAddress1Text" tabindex="23"
																				value="#{newSubscriptionOrderHandler.deliveryAddress1}"
																				maxlength="28" required="true"
																				requiredMessage="Address is Required"
																				validatorMessage="Address is Not Valid"
																				onchange="func_revalidateDelMethod(this, event);">
																				<f:validateLength minimum="1" maximum="28"></f:validateLength>
																				<hx:inputHelperAssist errorClass="inputText_Error"
																					onfocusSuccessClass="inputTextFocusOrderForm "
																					successClass="inputTextOrderForm "
																					validation="true" />
																			</h:inputText>
																			<h:inputText styleClass="inputTextOrderForm"
																				id="textDeliveryAptSuite" tabindex="24"
																				style="margin-left: 5px" size="8"
																				value="#{newSubscriptionOrderHandler.deliveryAptSuite}"
																				maxlength="28"
																				onchange="func_revalidateDelMethod(this, event);">
																				<hx:inputHelperAssist errorClass="inputText_Error"
																					onfocusSuccessClass="inputTextFocusOrderForm "
																					successClass="inputTextOrderForm "
																					validation="true" />
																			</h:inputText>
																		</hx:formItem>
																		<hx:formItem styleClass="formItem"
																			id="formItemDeliveryAddress2"
																			label="#{labels.personalDeliveryInfolabel}" rendered="true">
																			<h:inputText styleClass="inputTextOrderForm"
																				id="textDeliveryAddress2" tabindex="25"
																				value="#{newSubscriptionOrderHandler.deliveryAddress2}"
																				maxlength="35" size="40">
																				<f:validateLength minimum="0" maximum="35"></f:validateLength>
																				<hx:inputHelperAssist errorClass="inputText_Error"
																					onfocusSuccessClass="inputTextFocusOrderForm "
																					successClass="inputTextOrderForm " validation="true" />
																			</h:inputText>
																			<h:outputText escape="false" id="textAddrLearnMore"
																				value="#{currentOfferHandler.addressLearnMoreHTML}"
																				style="margin-left: 10px; color: #00529b; cursor: pointer"
																				styleClass="requestLink"></h:outputText>


																		</hx:formItem>

																		<hx:formItem styleClass="formItem"
																			id="formItemDeliveryCity"
																			label='#{labels.cityLabel} <span style="color:red">*</span>'
																			errorText="#{labels.requiredFieldMsg}">
																			<h:inputText styleClass="inputTextOrderForm"
																				id="textDeliveryCity" tabindex="26"
																				onchange="func_revalidateDelMethod(this, event);"
																				value="#{newSubscriptionOrderHandler.deliveryCity}"
																				maxlength="28" required="true"
																				requiredMessage="City is Required"
																				validatorMessage="City is Not Valid">
																				<f:validateLength minimum="1" maximum="28"></f:validateLength>
																				<hx:inputHelperAssist errorClass="inputText_Error"
																					onfocusSuccessClass="inputTextFocusOrderForm "
																					successClass="inputTextOrderForm "
																					validation="true" />
																			</h:inputText>
																		</hx:formItem>

																		<hx:formItem styleClass="formItem"
																			id="formItemDeliveryState"
																			label='#{labels.StateZipLabel} <span style="color:red">*</span>'
																			errorText="#{labels.requiredFieldMsg}">
																			<h:selectOneMenu
																				styleClass="selectOneMenu selectOneMenu2"
																				id="menuDeliveryState" tabindex="27"
																				onchange="func_revalidateDelMethod(this, event);"
																				requiredMessage="State is Required"
																				validatorMessage="State is Not Valid"
																				required="true"
																				value="#{newSubscriptionOrderHandler.deliveryState}">
																				<f:selectItem itemValue="NONE" itemLabel="" />
																				<f:selectItem itemValue="AL" itemLabel="Alabama" />
																				<f:selectItem itemValue="AK" itemLabel="Alaska" />
																				<f:selectItem itemValue="AZ" itemLabel="Arizona" />
																				<f:selectItem itemValue="AR" itemLabel="Arkansas" />
																				<f:selectItem itemValue="AA"
																					itemLabel="(AA) Armed Forces Americas" />
																				<f:selectItem itemValue="AE"
																					itemLabel="(AE) Armed Forces Africa" />
																				<f:selectItem itemValue="AE"
																					itemLabel="(AE) Armed Forces Canada" />
																				<f:selectItem itemValue="AE"
																					itemLabel="(AE) Armed Forces Europe" />
																				<f:selectItem itemValue="AE"
																					itemLabel="(AE) Armed Forces Middle East" />
																				<f:selectItem itemValue="AP"
																					itemLabel="(AP) Armed Forces Pacific" />
																				<f:selectItem itemValue="CA" itemLabel="California" />
																				<f:selectItem itemValue="CO" itemLabel="Colorado" />
																				<f:selectItem itemValue="CT" itemLabel="Connecticut" />
																				<f:selectItem itemValue="DE" itemLabel="Delaware" />
																				<f:selectItem itemValue="DC"
																					itemLabel="District of Columbia" />
																				<f:selectItem itemValue="FL" itemLabel="Florida" />
																				<f:selectItem itemValue="GA" itemLabel="Georgia" />
																				<f:selectItem itemValue="HI" itemLabel="Hawaii" />
																				<f:selectItem itemValue="ID" itemLabel="Idaho" />
																				<f:selectItem itemValue="IL" itemLabel="Illinois" />
																				<f:selectItem itemValue="IN" itemLabel="Indiana" />
																				<f:selectItem itemValue="IA" itemLabel="Iowa" />
																				<f:selectItem itemValue="KS" itemLabel="Kansas" />
																				<f:selectItem itemValue="KY" itemLabel="Kentucky" />
																				<f:selectItem itemValue="LA" itemLabel="Louisiana" />
																				<f:selectItem itemValue="ME" itemLabel="Maine" />
																				<f:selectItem itemValue="MD" itemLabel="Maryland" />
																				<f:selectItem itemValue="MA"
																					itemLabel="Massachusetts" />
																				<f:selectItem itemValue="MI" itemLabel="Michigan" />
																				<f:selectItem itemValue="MN" itemLabel="Minnesota" />
																				<f:selectItem itemValue="MS" itemLabel="Mississippi" />
																				<f:selectItem itemValue="MO" itemLabel="Missouri" />
																				<f:selectItem itemValue="MT" itemLabel="Montana" />
																				<f:selectItem itemValue="NE" itemLabel="Nebraska" />
																				<f:selectItem itemValue="NV" itemLabel="Nevada" />
																				<f:selectItem itemValue="NH"
																					itemLabel="New Hampshire" />
																				<f:selectItem itemValue="NJ" itemLabel="New Jersey" />
																				<f:selectItem itemValue="NM" itemLabel="New Mexico" />
																				<f:selectItem itemValue="NY" itemLabel="New York" />
																				<f:selectItem itemValue="NC"
																					itemLabel="North Carolina" />
																				<f:selectItem itemValue="ND"
																					itemLabel="North Dakota" />
																				<f:selectItem itemValue="OH" itemLabel="Ohio" />
																				<f:selectItem itemValue="OK" itemLabel="Oklahoma" />
																				<f:selectItem itemValue="OR" itemLabel="Oregon" />
																				<f:selectItem itemValue="PA"
																					itemLabel="Pennsylvania" />
																				<f:selectItem itemValue="PR" itemLabel="Puerto Rico" />
																				<f:selectItem itemValue="RI"
																					itemLabel="Rhode Island" />
																				<f:selectItem itemValue="SC"
																					itemLabel="South Carolina" />
																				<f:selectItem itemValue="SD"
																					itemLabel="South Dakota" />
																				<f:selectItem itemValue="TN" itemLabel="Tennessee" />
																				<f:selectItem itemValue="TX" itemLabel="Texas" />
																				<f:selectItem itemValue="UT" itemLabel="Utah" />
																				<f:selectItem itemValue="VT" itemLabel="Vermont" />
																				<f:selectItem itemValue="VA" itemLabel="Virginia" />
																				<f:selectItem itemValue="WA" itemLabel="Washington" />
																				<f:selectItem itemValue="WV"
																					itemLabel="West Virginia" />
																				<f:selectItem itemValue="WI" itemLabel="Wisconsin" />
																				<f:selectItem itemValue="WY" itemLabel="Wyoming" />
																				<hx:inputHelperAssist
																					errorClass="selectOneMenu_Error"
																					onfocusSuccessClass="inputTextFocusOrderForm "
																					successClass="inputTextOrderForm " />
																			</h:selectOneMenu>
																			<h:inputText styleClass="inputTextOrderForm"
																				id="textDeliveryZip"
																				onchange="func_revalidateDelMethod(this, event);"
																				tabindex="28" style="margin-left: 5px"
																				value="#{newSubscriptionOrderHandler.deliveryZipCode}"
																				maxlength="5" required="true" size="6"
																				requiredMessage="Zip is Required"
																				validatorMessage="Zip is Not Valid">
																				<f:validateLength minimum="5" maximum="5"></f:validateLength>
																				<hx:validateConstraint regex="^[0-9]+$" />
																				<hx:inputHelperAssist errorClass="inputText_Error"
																					onfocusSuccessClass="inputTextFocusOrderForm "
																					successClass="inputTextOrderForm "
																					validation="true" />
																			</h:inputText>
																		</hx:formItem>
																		<hx:formItem styleClass="formItem"
																			id="formItemDeliveryPhone"
																			label='#{labels.phoneLabel} <span style="color:red">*</span>'
																			errorText="#{labels.invalidPhoneFormat}">
																			<h:inputText styleClass="inputTextOrderForm"
																				id="textDeliveryPhoneAreaCode" size="4"
																				tabindex="29" maxlength="3"
																				value="#{newSubscriptionOrderHandler.deliveryPhoneAreaCode}"
																				required="true"
																				requiredMessage="Phone Number is Required"
																				validatorMessage="Phone Number is Not Valid">

																				<hx:inputHelperAssist errorClass="inputText_Error"
																					autoTab="true" id="assist4"
																					onfocusSuccessClass="inputTextFocusOrderForm"
																					successClass="inputTextOrderForm" />
																				<hx:validateConstraint regex="^[0-9]+$" />
																				<f:validateLength minimum="3" maximum="3"></f:validateLength>
																			</h:inputText>
																			<h:inputText styleClass="inputTextOrderForm"
																				id="textDeliveryPhoneExchange" size="4"
																				style="margin-left: 4px" tabindex="30" maxlength="3"
																				readonly="false"
																				value="#{newSubscriptionOrderHandler.deliveryPhoneExchange}"
																				required="true"
																				requiredMessage="Phone Number is Required"
																				validatorMessage="Phone Number is Not Valid">

																				<hx:inputHelperAssist errorClass="inputText_Error"
																					autoTab="true" id="assist5"
																					onfocusSuccessClass="inputTextFocusOrderForm"
																					successClass="inputTextOrderForm" />
																				<hx:validateConstraint regex="^[0-9]+$" />
																				<f:validateLength minimum="3" maximum="3"></f:validateLength>
																			</h:inputText>
																			<h:inputText styleClass="inputTextOrderForm"
																				id="textDeliveryPhoneExtension"
																				style="margin-left: 4px" size="5" tabindex="31"
																				value="#{newSubscriptionOrderHandler.deliveryPhoneExtension}"
																				maxlength="4" required="true"
																				requiredMessage="Phone Number is Required"
																				validatorMessage="Phone Number is Not Valid">

																				<hx:validateConstraint regex="^[0-9]+$" />
																				<f:validateLength minimum="4" maximum="4"></f:validateLength>
																				<hx:inputHelperAssist errorClass="inputText_Error"
																					onfocusSuccessClass="inputTextFocusOrderForm "
																					successClass="inputTextOrderForm "
																					validation="true" />
																			</h:inputText>
																		</hx:formItem>
																		<hx:formItem styleClass="formItem"
																			id="formItemDeliveryWorkPhone"
																			errorText="#{labels.invalidPhoneFormat}"
																			label="#{labels.workPhoneLabel}">
																			<h:inputText styleClass="inputTextOrderForm"
																				id="textDeliveryWorkPhoneAreaCode" size="4"
																				tabindex="32" maxlength="3"
																				value="#{newSubscriptionOrderHandler.deliveryWorkPhoneAreaCode}"
																				validatorMessage="Work Phone Number is Not Valid">

																				<hx:inputHelperAssist errorClass="inputText_Error"
																					autoTab="true" id="assist44"
																					onfocusSuccessClass="inputTextFocusOrderForm"
																					successClass="inputTextOrderForm" />
																				<hx:validateConstraint regex="^[0-9]+$" />
																				<f:validateLength minimum="3" maximum="3"></f:validateLength>
																			</h:inputText>
																			<h:inputText styleClass="inputTextOrderForm"
																				id="textDeliveryWorkPhoneExchange" size="4"
																				style="margin-left: 4px" tabindex="33" maxlength="3"
																				readonly="false"
																				value="#{newSubscriptionOrderHandler.deliveryWorkPhoneExchange}"
																				validatorMessage="Work Phone Number is Not Valid">

																				<hx:inputHelperAssist errorClass="inputText_Error"
																					autoTab="true" id="assist45"
																					onfocusSuccessClass="inputTextFocusOrderForm"
																					successClass="inputTextOrderForm" />
																				<hx:validateConstraint regex="^[0-9]+$" />
																				<f:validateLength minimum="3" maximum="3"></f:validateLength>
																			</h:inputText>
																			<h:inputText styleClass="inputTextOrderForm"
																				id="textDeliveryWorkPhoneExtension"
																				style="margin-left: 4px" size="5" tabindex="34"
																				value="#{newSubscriptionOrderHandler.deliveryWorkPhoneExtension}"
																				maxlength="4"
																				validatorMessage="Work Phone Number is Not Valid">

																				<hx:validateConstraint regex="^[0-9]+$" />
																				<f:validateLength minimum="4" maximum="4"></f:validateLength>
																				<hx:inputHelperAssist errorClass="inputText_Error"
																					onfocusSuccessClass="inputTextFocusOrderForm "
																					successClass="inputTextOrderForm "
																					validation="true" />
																			</h:inputText>
																		</hx:formItem>


																		<hx:formItem styleClass="formItem"
																			id="formItemEmailAddress"
																			label='#{labels.Email} <span style="color:red">*</span>'
																			errorText="#{labels.requiredFieldMsg}">
																			<h:inputText styleClass="inputTextOrderForm "
																				id="textEmailAddressRecipient" required="true"
																				tabindex="35" title="Delivery Email Address"
																				value="#{newSubscriptionOrderHandler.deliveryEmailAddress}"
																				maxlength="50"
																				requiredMessage="Delivery Email Address is Required"
																				validatorMessage="Delivery Email Address is Not Valid"
																				validator="#{pc_Checkout.handleTextEmailAddressRecipientValidate}">
																				<f:validateLength minimum="6" maximum="50"></f:validateLength>
																				<hx:inputHelperAssist errorClass="inputText_Error"
																					onfocusSuccessClass="inputTextFocusOrderForm "
																					successClass="inputTextOrderForm "
																					validation="true" />
																			</h:inputText>
																		</hx:formItem>
																		<hx:formItem styleClass="formItem"
																			id="formItemEmailAddressConfirm"
																			label='#{labels.confirmPurchaserEmailAddressLabel} <span style="color:red">*</span>'
																			errorText="#{labels.requiredFieldMsg}">

																			<h:inputText styleClass="inputTextOrderForm"
																				id="textEmailAddressConfirmRecipient"
																				required="true" tabindex="36"
																				title="Confirm Delivery Email Address"
																				value="#{newSubscriptionOrderHandler.deliveryEmailAddressConfirmation}"
																				maxlength="50"
																				validatorMessage="Confirmation Email Address is Not Valid"
																				requiredMessage="Confirmation Email Address is Required"
																				validator="#{pc_Checkout.handleTextEmailAddressRecipientValidate}">
																				<f:validateLength minimum="6" maximum="50"></f:validateLength>
																				<hx:inputHelperAssist errorClass="inputText_Error"
																					onfocusSuccessClass="inputTextFocusOrderForm "
																					successClass="inputTextOrderForm "
																					validation="true" />
																			</h:inputText>
																		</hx:formItem>
																		<f:facet name="top">
																			<h:panelGrid styleClass="panelGrid"
																				id="gridDeliveryMethodInfoGrid"
																				rendered="#{currentOfferHandler.showDeliveryMethodCheck}"
																				width="90%" style="text-align: right"
																				cellpadding="0" columns="1">
																				<hx:outputLinkEx value="javascript:;"
																					styleClass="outputLinkEx"
																					id="linkExDelMethodCheckHelpLink">
																					<h:outputText styleClass="outputLinkEx"
																						id="textDelMethodHelpText"
																						value="#{labels.DelMethodHelp}"
																						style="font-family: Arial; font-size: 7pt; cursor: pointer"></h:outputText>
																				</hx:outputLinkEx>
																			</h:panelGrid>
																		</f:facet>
																	</hx:panelFormBox>
																</h:panelGrid>
																<h:panelGrid styleClass="panelGrid orderFormGridStyles"
																	id="gridDetermineDeliveryOuterGrid" columns="1"
																	columnClasses="panelGridColTopCentertAlign"
																	rendered="#{currentOfferHandler.showDeliveryMethodCheck}"
																	style="width: 80%; margin-top: 15px; text-align: center">
																	<h:panelGrid styleClass="panelGrid"
																		id="gridDeliveryInformationPanelFooterGrid"
																		columns="1" cellpadding="1" cellspacing="1"
																		width="100%" style="text-align: center">


																		<h:panelGrid styleClass="panelGrid"
																			columnClasses="panelGridColTopRightAlignV2, panelGridColTopLeftAlignV2"
																			id="gridDelMethodResultsGrid" columns="2"
																			cellpadding="2" cellspacing="2" width="98%">
																			<h:outputText styleClass="outputText"
																				id="textDeliveryMethod"
																				style="font-weight: bold; font-size: 9pt"
																				value="#{labels.DelMethod}:">
																			</h:outputText>
																			<h:outputText styleClass="outputText"
																				id="textDeliveryMethodTextDeterminedValue"
																				value="#{newSubscriptionOrderHandler.deliveryMethodText}"
																				style="color: navy; font-weight: bold; font-weight: bold; font-size: 9pt"></h:outputText>
																		</h:panelGrid>
																		<hx:commandExButton type="submit"
																			styleClass="commandExButton"
																			id="buttonGetDeliveryMethodButton" immediate="true"
																			onclick="return func_9(this, event);"
																			value="#{labels.DelMethodButtonLabel}"
																			style="width: auto" tabindex="37"
																			title="Determine Delivery Method"
																			action="#{pc_Checkout.doButtonGetDeliveryMethodButtonAction}"></hx:commandExButton>
																	</h:panelGrid>
																</h:panelGrid>
																<h:panelGrid id="panelGridBillingDifferentThanDelGrid"
																	columns="1" styleClass="orderFormGridStyles">
																	<hx:panelFormBox helpPosition="over"
																		labelPosition="right"
																		styleClass="panelFormBoxReversed"
																		id="formBoxBillDifferentFromDelSelectionFormBox">

																		<hx:formItem styleClass="formItem"
																			id="formItemBillDifferentFromDelSelector"
																			label="#{labels.billingDiffersFromDeliveryLabel}">
																			<h:selectBooleanCheckbox
																				styleClass="selectBooleanCheckbox"
																				id="checkboxIsBillDifferentFromDelSelector"
																				onclick="return func_2(this, event);"
																				title="Check if Billing Address is Different Than Delivery Address"
																				tabindex="41" style="margin-right: 5px"
																				value="#{newSubscriptionOrderHandler.billingDifferentThanDelivery}">
																				<hx:behavior event="onselect" id="behavior1"></hx:behavior>
																			</h:selectBooleanCheckbox>
																		</hx:formItem>
																	</hx:panelFormBox>
																</h:panelGrid>
																<h:panelGrid styleClass="panelGrid orderFormGridStyles"
																	id="gridBillingAddress" columns="1">
																	<hx:panelFormBox helpPosition="under"
																		labelPosition="left"
																		styleClass="panelFormBoxOrderEntryForm"
																		id="formBoxBillingAddress"
																		label="#{labels.BillingAddressLabel}">
																		<hx:formItem styleClass="formItem"
																			id="formItemBillingFirstName"
																			label='#{labels.firstNameLabel} <span style="color:red">*</span>'>

																			<h:inputText styleClass="inputTextOrderForm"
																				id="textBillingFirstName" title="First Name"
																				tabindex="52"
																				value="#{newSubscriptionOrderHandler.billingFirstName}"
																				maxlength="10">
																				<f:validateLength minimum="1" maximum="15"></f:validateLength>
																			</h:inputText>
																		</hx:formItem>
																		<hx:formItem styleClass="formItem"
																			id="formItemBillingLastName"
																			label='#{labels.lastNameLabel} <span style="color:red">*</span>'>

																			<h:inputText styleClass="inputTextOrderForm"
																				id="textBillingLastName" tabindex="53"
																				title="Last Name"
																				value="#{newSubscriptionOrderHandler.billingLastName}"
																				maxlength="15">
																				<f:validateLength minimum="1" maximum="20"></f:validateLength>
																			</h:inputText>
																		</hx:formItem>

																		<hx:formItem styleClass="formItem"
																			id="formItemBillingCompanyName"
																			label="#{labels.companyNameLabel}">
																			<h:inputText styleClass="inputTextOrderForm"
																				id="textBillingCompanyName" tabindex="54"
																				value="#{newSubscriptionOrderHandler.billingCompanyName}"
																				maxlength="28">
																				<f:validateLength maximum="40"></f:validateLength>
																			</h:inputText>
																		</hx:formItem>

																		<hx:formItem styleClass="formItem"
																			id="formItemBillingAddress1"
																			label='#{labels.address} <span style="color:red">*</span>'>
																			<h:inputText styleClass="inputTextOrderForm"
																				id="textBillingAddress1" tabindex="55"
																				title="Address 1"
																				value="#{newSubscriptionOrderHandler.billingAddress1}"
																				maxlength="28">
																				<f:validateLength minimum="1" maximum="28"></f:validateLength>
																			</h:inputText>
																			<h:inputText styleClass="inputTextOrderForm"
																				id="textBillingAptSuite" size="8"
																				style="margin-left: 5px" title="Apt/Suite"
																				tabindex="56"
																				value="#{newSubscriptionOrderHandler.billingAptSuite}"
																				maxlength="28">
																				<f:validateLength minimum="1" maximum="28"></f:validateLength>
																			</h:inputText>
																		</hx:formItem>
																		<hx:formItem styleClass="formItem"
																			id="formItemBillingAddress2"
																			label="#{labels.address2Label}" rendered="false">
																			<h:inputText styleClass="inputTextOrderForm"
																				id="textBillingAddress2" tabindex="57"
																				value="#{newSubscriptionOrderHandler.billingAddress2}"
																				maxlength="28">
																				<f:validateLength minimum="1" maximum="28"></f:validateLength>
																			</h:inputText>
																		</hx:formItem>

																		<hx:formItem styleClass="formItem"
																			id="formItemBillingCity"
																			label='#{labels.cityLabel} <span style="color:red">*</span>'>
																			<h:inputText styleClass="inputTextOrderForm"
																				id="textBillingCity" tabindex="58" title="City"
																				value="#{newSubscriptionOrderHandler.billingCity}"
																				maxlength="28">
																				<f:validateLength minimum="1" maximum="28"></f:validateLength>
																			</h:inputText>
																		</hx:formItem>
																		<hx:formItem styleClass="formItem"
																			id="formItemBillingState"
																			label='#{labels.StateZipLabel} <span style="color:red">*</span>'>
																			<h:selectOneMenu
																				styleClass="selectOneMenu selectOneMenu2"
																				id="menuBillingState" tabindex="58" title="State"
																				value="#{newSubscriptionOrderHandler.billingState}">
																				<f:selectItem itemValue="" itemLabel="" />
																				<f:selectItem itemValue="AL" itemLabel="Alabama" />
																				<f:selectItem itemValue="AK" itemLabel="Alaska" />
																				<f:selectItem itemValue="AZ" itemLabel="Arizona" />
																				<f:selectItem itemValue="AR" itemLabel="Arkansas" />
																				<f:selectItem itemValue="AA"
																					itemLabel="(AA) Armed Forces Americas" />
																				<f:selectItem itemValue="AE"
																					itemLabel="(AE) Armed Forces Africa" />
																				<f:selectItem itemValue="AE"
																					itemLabel="(AE) Armed Forces Canada" />
																				<f:selectItem itemValue="AE"
																					itemLabel="(AE) Armed Forces Europe" />
																				<f:selectItem itemValue="AE"
																					itemLabel="(AE) Armed Forces Middle East" />
																				<f:selectItem itemValue="AP"
																					itemLabel="(AP) Armed Forces Pacific" />
																				<f:selectItem itemValue="CA" itemLabel="California" />
																				<f:selectItem itemValue="CO" itemLabel="Colorado" />
																				<f:selectItem itemValue="CT" itemLabel="Connecticut" />
																				<f:selectItem itemValue="DE" itemLabel="Delaware" />
																				<f:selectItem itemValue="DC"
																					itemLabel="District of Columbia" />
																				<f:selectItem itemValue="FL" itemLabel="Florida" />
																				<f:selectItem itemValue="GA" itemLabel="Georgia" />
																				<f:selectItem itemValue="HI" itemLabel="Hawaii" />
																				<f:selectItem itemValue="ID" itemLabel="Idaho" />
																				<f:selectItem itemValue="IL" itemLabel="Illinois" />
																				<f:selectItem itemValue="IN" itemLabel="Indiana" />
																				<f:selectItem itemValue="IA" itemLabel="Iowa" />
																				<f:selectItem itemValue="KS" itemLabel="Kansas" />
																				<f:selectItem itemValue="KY" itemLabel="Kentucky" />
																				<f:selectItem itemValue="LA" itemLabel="Louisiana" />
																				<f:selectItem itemValue="ME" itemLabel="Maine" />
																				<f:selectItem itemValue="MD" itemLabel="Maryland" />
																				<f:selectItem itemValue="MA"
																					itemLabel="Massachusetts" />
																				<f:selectItem itemValue="MI" itemLabel="Michigan" />
																				<f:selectItem itemValue="MN" itemLabel="Minnesota" />
																				<f:selectItem itemValue="MS" itemLabel="Mississippi" />
																				<f:selectItem itemValue="MO" itemLabel="Missouri" />
																				<f:selectItem itemValue="MT" itemLabel="Montana" />
																				<f:selectItem itemValue="NE" itemLabel="Nebraska" />
																				<f:selectItem itemValue="NV" itemLabel="Nevada" />
																				<f:selectItem itemValue="NH"
																					itemLabel="New Hampshire" />
																				<f:selectItem itemValue="NJ" itemLabel="New Jersey" />
																				<f:selectItem itemValue="NM" itemLabel="New Mexico" />
																				<f:selectItem itemValue="NY" itemLabel="New York" />
																				<f:selectItem itemValue="NC"
																					itemLabel="North Carolina" />
																				<f:selectItem itemValue="ND"
																					itemLabel="North Dakota" />
																				<f:selectItem itemValue="OH" itemLabel="Ohio" />
																				<f:selectItem itemValue="OK" itemLabel="Oklahoma" />
																				<f:selectItem itemValue="OR" itemLabel="Oregon" />
																				<f:selectItem itemValue="PA"
																					itemLabel="Pennsylvania" />
																				<f:selectItem itemValue="PR" itemLabel="Puerto Rico" />
																				<f:selectItem itemValue="RI"
																					itemLabel="Rhode Island" />
																				<f:selectItem itemValue="SC"
																					itemLabel="South Carolina" />
																				<f:selectItem itemValue="SD"
																					itemLabel="South Dakota" />
																				<f:selectItem itemValue="TN" itemLabel="Tennessee" />
																				<f:selectItem itemValue="TX" itemLabel="Texas" />
																				<f:selectItem itemValue="UT" itemLabel="Utah" />
																				<f:selectItem itemValue="VT" itemLabel="Vermont" />
																				<f:selectItem itemValue="VA" itemLabel="Virginia" />
																				<f:selectItem itemValue="WA" itemLabel="Washington" />
																				<f:selectItem itemValue="WV"
																					itemLabel="West Virginia" />
																				<f:selectItem itemValue="WI" itemLabel="Wisconsin" />
																				<f:selectItem itemValue="WY" itemLabel="Wyoming" />
																			</h:selectOneMenu>
																			<h:inputText styleClass="inputTextOrderForm"
																				id="textBillingZipCode" size="6"
																				style="margin-left: 5px" title="Zip Code"
																				tabindex="59"
																				value="#{newSubscriptionOrderHandler.billingZipCode}"
																				maxlength="5">
																				<f:validateLength minimum="5" maximum="5"></f:validateLength>
																				<hx:validateConstraint regex="^[0-9]+$" />
																			</h:inputText>
																		</hx:formItem>
																		<hx:formItem styleClass="formItem"
																			id="formItemBillingTelephone"
																			label="#{labels.billPhoneLabel}">
																			<h:inputText styleClass="inputTextOrderForm"
																				id="textBillingPhoneAreaCode" size="4" tabindex="60"
																				maxlength="3" title="Billing Phone Area Code"
																				value="#{newSubscriptionOrderHandler.billingPhoneAreaCode}">

																				<hx:inputHelperAssist errorClass="inputText_Error"
																					autoTab="true" id="assist2" />
																				<hx:validateConstraint regex="^[0-9]+$" />
																				<f:validateLength minimum="3" maximum="3"></f:validateLength>
																			</h:inputText>
																			<h:inputText styleClass="inputTextOrderForm"
																				id="textBillingPhoneExchange" size="4"
																				style="margin-left: 4px" tabindex="61" maxlength="3"
																				title="Billing Phone Exchange"
																				value="#{newSubscriptionOrderHandler.billingPhoneExchange}">

																				<hx:inputHelperAssist errorClass="inputText_Error"
																					autoTab="true" id="assist3" />
																				<hx:validateConstraint regex="^[0-9]+$" />
																				<f:validateLength minimum="3" maximum="3"></f:validateLength>
																			</h:inputText>
																			<h:inputText styleClass="inputTextOrderForm"
																				id="textBillingPhoneExtension"
																				style="margin-left: 4px" size="5" tabindex="62"
																				title="Billing Phone Last 4 Digits"
																				value="#{newSubscriptionOrderHandler.billingPhoneExtension}"
																				maxlength="4">

																				<hx:validateConstraint regex="^[0-9]+$" />
																				<f:validateLength minimum="4" maximum="4"></f:validateLength>
																			</h:inputText>
																		</hx:formItem>
																		<hx:formItem styleClass="formItem"
																			id="giftSubscriptionFormItem"
																			label="#{labels.isGiftSubscriptionLabel}">
																			<h:selectBooleanCheckbox
																				styleClass="selectBooleanCheckbox"
																				id="giftSubscriptionCheckbox"
																				value="#{newSubscriptionOrderHandler.giftSubscription}"
																				onclick="return func_2(this, event);"
																				title="Check if this is a gift" tabindex="63"></h:selectBooleanCheckbox>
																		</hx:formItem>
																		<hx:formItem styleClass="formItem"
																			id="formItemPayersEmailAddress"
																			label='#{labels.emailAddressPayerLabel} <span style="color:red">*</span>'
																			infoText="#{labels.BillingEmailInfoMsg}">

																			<h:inputText styleClass="inputTextOrderForm"
																				id="textPurchaserEmailAddress" tabindex="64"
																				value="#{newSubscriptionOrderHandler.billingEmailAddress}"
																				maxlength="50" required="false"
																				validator="#{pc_Checkout.handleTextEmailAddressRecipientValidate}">
																				<f:validateLength minimum="1" maximum="50"></f:validateLength>
																			</h:inputText>
																		</hx:formItem>

																	</hx:panelFormBox>
																</h:panelGrid>
																<h:panelGrid styleClass="panelGrid orderFormGridStyles"
																	id="gridPaymentInformationGridLabelGrid" columns="1">
																	<hx:panelFormBox helpPosition="over"
																		labelPosition="left"
																		styleClass="panelFormBoxOrderEntryForm"
																		id="formBoxPaymentHeaderFormBox"
																		label="#{labels.paymentInfo}" widthLabel="135">
																		<f:facet name="top">

																		</f:facet>
																		<hx:formItem styleClass="formItem"
																			id="formItemBillMeFormItem"
																			rendered="#{currentOfferHandler.isShowPaymentOptionSelector}"
																			label="#{labels.paymentMethod} :"
																			style="font-weight: bold">
																			<h:selectOneRadio
																				disabledClass="selectOneRadio_Disabled"
																				enabledClass="selectOneRadio_Enabled"
																				styleClass="selectOneRadio" id="radioPaymentMethod"
																				layout="lineDirection"
																				value="#{newSubscriptionOrderHandler.paymentMethod}"
																				onclick="return func_4(this, event);">
																				<f:selectItem itemLabel="Credit Card" itemValue="CC"
																					id="selectItem1" />
																				<f:selectItem itemLabel="Bill Me" itemValue="B"
																					id="selectItem2" />
																			</h:selectOneRadio>
																		</hx:formItem>
																	</hx:panelFormBox>
																</h:panelGrid>
																<h:panelGrid styleClass="panelGrid orderFormGridStyles"
																	id="gridForceBillMeInfoGrid" columns="1"
																	rendered="#{currentOfferHandler.isForceBillMe}">
																	<h:outputText styleClass="outputText"
																		id="textForceBillMeText"
																		style="font-weight: bold; font-size: 11pt"
																		escape="false"
																		value="#{labels.ForceBillMeInformation}"></h:outputText>
																</h:panelGrid>
																<h:panelGrid styleClass="panelGrid orderFormGridStyles"
																	id="gridPaymentInformationGrid" columns="1">
																	<f:facet name="footer">

																	</f:facet>
																	<hx:panelFormBox helpPosition="right"
																		labelPosition="left"
																		styleClass="panelFormBoxOrderEntryForm"
																		id="formBoxPaymentInfo"
																		rendered="#{currentOfferHandler.isShowCreditCardPaymentOptionData}">
																		<hx:formItem styleClass="formItem"
																			id="formItemCreditCardNumber"
																			label='#{labels.CCNum} <span style="color:red">*</span>'>
																			<h:inputText styleClass="inputTextOrderForm"
																				id="textCreditCardNumber" tabindex="70"
																				title="Credit Card Number"
																				value="#{newSubscriptionOrderHandler.creditCardNumber}"
																				maxlength="16"
																				validatorMessage="Credit Card Number is Not Valid">
																				<hx:validateConstraint regex="^[0-9]+$" />
																				<f:validateLength minimum="12" maximum="16"></f:validateLength>
																				<hx:inputHelperAssist errorClass="inputText_Error"
																					validation="true" id="assist6" />
																			</h:inputText>
																		</hx:formItem>

																		<hx:formItem styleClass="formItem"
																			id="formItemCreditCardCVV"
																			label='#{labels.CCVerify} <span style="color:red">*</span>'>
																			<h:inputText styleClass="inputTextOrderForm"
																				id="textCVV" size="5" tabindex="71"
																				title="Credit Card Verification Number"
																				value="#{newSubscriptionOrderHandler.creditCardCVVNumber}"
																				validatorMessage="Credit Card Verification Number is Not Valid"
																				maxlength="4">
																				<f:validateLength minimum="3" maximum="4"></f:validateLength>
																			</h:inputText>
													&nbsp;&nbsp;&nbsp;
													<h:outputText styleClass="outputText" id="textCVVLearnMore"
																				style="margin-left: 10px; color: #00529b; cursor: pointer"
																				value="#{currentOfferHandler.addressLearnMoreHTML}"
																				escape="false"></h:outputText>

																		</hx:formItem>
																		<hx:formItem styleClass="formItem"
																			id="formItemCCExpirationDate"
																			label='#{labels.CCExpiration} <span style="color:red">*</span>'>
																			<h:selectOneMenu
																				styleClass="selectOneMenu selectOneMenu2"
																				id="menuCCExpireMonth" tabindex="72"
																				title="Credit Card Expiration Month"
																				value="#{newSubscriptionOrderHandler.creditCardExpirationMonth}">
																				<f:selectItem itemLabel="Month" itemValue="-1"
																					id="selectItem16" />
																				<f:selectItem itemLabel="01" itemValue="01"
																					id="selectItem3" />
																				<f:selectItem itemLabel="02" itemValue="02"
																					id="selectItem4" />
																				<f:selectItem itemLabel="03" itemValue="03"
																					id="selectItem5" />
																				<f:selectItem itemLabel="04" itemValue="04"
																					id="selectItem6" />
																				<f:selectItem itemLabel="05" itemValue="05"
																					id="selectItem7" />
																				<f:selectItem itemLabel="06" itemValue="06"
																					id="selectItem8" />
																				<f:selectItem itemLabel="07" itemValue="07"
																					id="selectItem9" />
																				<f:selectItem itemLabel="08" itemValue="08"
																					id="selectItem10" />
																				<f:selectItem itemLabel="09" itemValue="09"
																					id="selectItem11" />
																				<f:selectItem itemLabel="10" itemValue="10"
																					id="selectItem12" />
																				<f:selectItem itemLabel="11" itemValue="11"
																					id="selectItem13" />
																				<f:selectItem itemLabel="12" itemValue="12"
																					id="selectItem14" />
																			</h:selectOneMenu>
																			<h:selectOneMenu
																				styleClass="selectOneMenu selectOneMenu2"
																				id="menuCCExpireYear" style="margin-left: 5px"
																				tabindex="73" title="Credit Card Expiration Year"
																				value="#{newSubscriptionOrderHandler.creditCardExpirationYear}">
																				<f:selectItems
																					value="#{currentOfferHandler.creditCardExpirationYears}"
																					id="selectItems1" />
																			</h:selectOneMenu>
																		</hx:formItem>



																		<f:facet name="top">
																			<h:panelGrid id="panelGridCreditCardImageGrid"
																				width="85%" columns="1" cellspacing="1"
																				cellpadding="1"
																				style="float: right; text-align: center">

																				<hx:jspPanel id="jspPanelCreditCardImages">
																					<hx:graphicImageEx styleClass="graphicImageEx"
																						id="imageExAmEx1"
																						value="/images/american_express_logo.gif"
																						hspace="5" border="0" width="42" height="26"
																						rendered="#{currentOfferHandler.isTakesAMEX}"
																						style="margin-left: 8px"></hx:graphicImageEx>
																					<hx:graphicImageEx styleClass="graphicImageEx"
																						id="imageExDiscover1"
																						value="/images/discover_logo.gif" hspace="5"
																						border="0" width="42" height="26"
																						rendered="#{currentOfferHandler.isTakesDiscover}"
																						style="margin-left: 8px"></hx:graphicImageEx>
																					<hx:graphicImageEx styleClass="graphicImageEx"
																						id="imageExMasterCard1"
																						value="/images/mc_logo.gif" hspace="5" border="0"
																						width="42" height="26"
																						rendered="#{currentOfferHandler.isTakesMasterCard}"
																						style="margin-left: 8px"></hx:graphicImageEx>
																					<hx:graphicImageEx styleClass="graphicImageEx"
																						id="imageExVisaLogo1"
																						value="/images/visa_logo.gif" hspace="5"
																						border="0" width="42" height="26"
																						rendered="#{currentOfferHandler.isTakesVisa}"
																						style="margin-left: 8px"></hx:graphicImageEx>

																				</hx:jspPanel>
																			</h:panelGrid>
																		</f:facet>
																	</hx:panelFormBox>
																	<h:panelGrid styleClass="panelGrid orderFormGridStyles"
																		id="gridEZPAYOptionsGrid" columns="1">
																		<h:selectOneRadio
																			disabledClass="selectOneRadio_Disabled"
																			enabledClass="selectOneRadio_Enabled"
																			styleClass="selectOneRadio" id="radioRenewalOptions"
																			layout="pageDirection"
																			value="#{newSubscriptionOrderHandler.renewalMethod}"
																			tabindex="80"
																			rendered="#{currentOfferHandler.isNonEZPayRate}">
																			<f:selectItem escape="false"
																				itemLabel="Please sign me up for EZ-PAY. #{currentOfferHandler.EZPayFreeWeeksOfferText}<sup>*</sup>"
																				itemValue="AUTOPAY_PLAN" id="selectItem17" />
																			<f:selectItem
																				itemLabel="Please charge my credit card for this subscription term only."
																				itemValue="ONE_TIME_BILL" id="selectItem18" />
																		</h:selectOneRadio>
																	</h:panelGrid>
																	<h:panelGrid styleClass="panelGrid orderFormGridStyles"
																		id="gridEZPAYOptionsGridGift" columns="1"
																		style="display:none">
																		<h:selectOneRadio
																			disabledClass="selectOneRadio_Disabled"
																			enabledClass="selectOneRadio_Enabled"
																			styleClass="selectOneRadio"
																			id="radioRenewalOptionsGift" layout="pageDirection"
																			tabindex="81"
																			value="#{newSubscriptionOrderHandler.renewalMethodGift}"
																			rendered="#{currentOfferHandler.isNonEZPayRate}">
																			<f:selectItem escape="false"
																				itemLabel="Please sign me up for EZ-PAY. #{currentOfferHandler.EZPayFreeWeeksOfferText}! <sup>2</sup>"
																				itemValue="AUTOPAY_PLAN" id="selectItem19" />
																			<f:selectItem
																				itemLabel="Please charge my credit card for this subscription term only."
																				itemValue="ONE_TIME_BILL" id="selectItem20" />
																			<f:selectItem itemValue="ONE_TIME_BILL_GIFT"
																				id="selectItem15"
																				itemLabel="Please charge my credit card for this subscription term only and send future invoices to the gift recipient." />
																		</h:selectOneRadio>
																	</h:panelGrid>
																	<h:panelGrid styleClass="panelGrid orderFormGridStyles"
																		id="gridEZPAYRequiredGrid" columns="1"
																		style="display:none;"
																		rendered="#{currentOfferHandler.ezPayFreeWeeks}">
																		<h:outputFormat styleClass="outputText coppaText"
																			id="requiresEZPAYText"
																			value="#{labels.requireEZPayRenwalText} "
																			escape="false">
																			<f:param
																				value="#{currentOfferHandler.EZPayFreeWeeksOffer}"
																				id="param5"></f:param>
																		</h:outputFormat>
																	</h:panelGrid>
																</h:panelGrid>
																<h:panelGrid
																	styleClass="panelGrid orderFormInsideGridStyles"
																	frame="box" id="gridTrialDisclaimerGrid" columns="1"
																	rendered="#{trialPartnerHandler.isShowDisclaimerText}"
																	style="margin-left: 3px; margin-top: 5px">
																	<h:outputText styleClass="outputText"
																		id="textDisclaimerText"
																		value="#{trialPartnerHandler.partner.disclaimerCustomHTML}"
																		style="font-family: Arial; font-size: 8pt"
																		escape="false"></h:outputText>
																</h:panelGrid>
																<h:panelGrid
																	styleClass="panelGrid orderFormInsideGridStyles"
																	frame="box" id="gridOfferDisclaimerGrid" columns="1"
																	style="margin-left: 3px; margin-top: 5px"
																	rendered="#{currentOfferHandler.isShowOfferDisclaimerText}">
																	<h:outputText styleClass="outputText"
																		id="textOfferDisclaimerText"
																		style="font-family: Arial;font-size: 8pt"
																		escape="false"
																		value="#{currentOfferHandler.onePageDisclaimerText}"></h:outputText>
																</h:panelGrid>
																<h:panelGrid id="COPPAGrid" cellpadding="4" columns="1"
																	styleClass="orderFormGridStyles">
																	<h:outputText styleClass=" outputText coppaText"
																		id="textCOPPAText" value="#{labels.OrderEntryCOPPA}"></h:outputText>
																</h:panelGrid>
																<h:panelGrid id="panelGridFormSubmissionGrid"
																	columns="3" cellpadding="4"
																	styleClass="orderFormGridStyles">

																	<hx:graphicImageEx styleClass="graphicImageEx"
																		id="imageEx3" value="/images/1px.gif" hspace="50"
																		width="1" height="1"></hx:graphicImageEx>
																	<hx:commandExButton type="submit" value="Subscribe Now"
																		styleClass="orderpageSubmitbutton"
																		id="buttonPlaceOrder" tabindex="90"
																		action="#{pc_Checkout.doButtonPlaceOrderAction}"
																		onclick="return func_10(this, event);"
																		alt="Click Here to Complete Your Order"></hx:commandExButton>

																	<hx:jspPanel id="jspPanelGeoTrustPanel">
																		<div id="chatIconRequestDiv">
																			<img
																				onclick="javascript: checkoutGuarantee(this, event);"
																				style="cursor: pointer; margin-top: 0px;"
																				alt="Satisfaction Guaranteed"
																				title="Click for details!"
																				src="/images/marketingimages/ckoutClickGuarantee.jpg"
																				hspace="0" vspace="5">
																		</div>
																		<!-- GeoTrust True Site[tm] Smart Icon tag. Do not edit. -->
																		<!--	<script LANGUAGE="JavaScript" TYPE="text/javascript" SRC="https://smarticon.geotrust.com/si.js"></script>  -->
																		<!-- end GeoTrust Smart Icon tag -->
																	</hx:jspPanel>
																	<hx:graphicImageEx styleClass="graphicImageEx"
																		id="imageEx33333" value="/images/1px.gif" hspace="50"
																		width="1" height="1"></hx:graphicImageEx>
																	<hx:jspPanel id="pleaseWaitPanel">
																		<div id="formSubmittedOverlayInfo"
																			style="display: none; position: relative; z-index: 100000; border-bottom: 0px solid #90a0b8; border-top: 0px solid #90a0b8; border-left: 0px solid #90a0b8; border-right: 0px solid #90a0b8; background: white; background-color: white;">
																			<table bgcolor="white">
																				<tr>
																					<td align="center" valign="middle" nowrap><h2>Please
																							wait while we process your order</h2></td>
																					<td align="left"><img id="formSubmissionImage"
																						src="/images/squaresAnimated.gif" /></td>
																				</tr>
																			</table>
																		</div>
																	</hx:jspPanel>
																</h:panelGrid>
																<h:panelGrid styleClass="panelGrid orderFormGridStyles"
																	id="gridRequiredEZPayFinePrintGrid" columns="1">
																	<h:panelGrid styleClass="panelGrid"
																		columnClasses="panelGridColLeftAlign"
																		id="gridEZPayFinePrintGrid" width="100%" columns="1"
																		style="top: -10px; line-height: normal">
																		<h:outputText
																			styleClass="outputFormat orderFormInsideGridStyles  footnote"
																			id="formatEZPAYFinePrint" rendered="#{not currentOfferHandler.isForceBillMe }"
																			value="#{labels.requiredLabel} #{currentOfferHandler.EZPayDisclaimer}">
																		</h:outputText>
																	</h:panelGrid>
																</h:panelGrid>

															</h:panelGroup>
														</f:facet>
														<f:facet name="left">

														</f:facet>
														<f:facet name="right">

														</f:facet>
														<f:facet name="bottom">

														</f:facet>
														<f:facet name="top">
															<h:messages styleClass="messages"
																id="messagesAllMesssages"></h:messages>
														</f:facet>
													</hx:panelLayout>


													<!-- end orderFormDiv -->
												</div>


											</div>
											<div id="pageContentEEOrderEntryRight" align="center">

												<h:panelGrid styleClass="panelGrid"
													id="gridCustServicePhone" columns="1"
													style="margin-bottom: 5px">
													<h:outputText styleClass="outputText"
														id="custServicePhoneText"
														value="#{currentOfferHandler.customerServicePhone}"
														escape="false"></h:outputText>
												</h:panelGrid>

												<h:panelGrid id="rightColImageSpot1"
													rendered="#{currentOfferHandler.renderRightColumnImageSpot1}">
													<hx:outputLinkEx styleClass="outputLinkEx"
														id="linkExRightColImageSpot1Link" target="_self"
														style="border-width: 0px"
														value="#{currentOfferHandler.onePageRightColumnImageSpot1ImageOnClickURL}"
														rendered="#{currentOfferHandler.renderRightColmnImageSpot1ImageAsClickable}">
														<hx:graphicImageEx id="imageExRightColImage1"
															value="#{currentOfferHandler.onePageRightColumnImageSpot1ImagePath}"
															border="0"
															alt="#{currentOfferHandler.onePageRightColumnImageSpot1ImageAltText}"
															title="#{currentOfferHandler.onePageRightColumnImageSpot1ImageAltText}"
															style="padding-left: 0px;">
														</hx:graphicImageEx>
													</hx:outputLinkEx>
													<hx:graphicImageEx id="imageExRightColImage1NoLink"
														value="#{currentOfferHandler.onePageRightColumnImageSpot1ImagePath}"
														border="0"
														alt="#{currentOfferHandler.onePageRightColumnImageSpot1ImageAltText}"
														title="#{currentOfferHandler.onePageRightColumnImageSpot1ImageAltText}"
														style="padding-left: 0px;"
														rendered="#{not currentOfferHandler.renderRightColmnImageSpot1ImageAsClickable}">
													</hx:graphicImageEx>
												</h:panelGrid>
												<h:panelGrid styleClass="panelGrid"
													id="gridRightColHTMLSpot1Grid" columns="1"
													style="text-align: center">
													<h:outputText styleClass="" id="RightColHTMLSpot1Text"
														value="#{currentOfferHandler.onePageRightColumnHTMLSpot1HTML}"
														escape="false"></h:outputText>
												</h:panelGrid>
												<h:panelGrid styleClass="panelGrid" id="gridVideoGrid"
													columns="1" style="text-align: center" width="230"
													rendered="#{currentOfferHandler.renderRightColumnVideoSpot1}">


													<hx:outputLinkEx styleClass="outputLinkEx"
														id="linkExVideoLink_B"
														onclick="return openVideoScreen('#{currentOfferHandler.onePageRightColumnVideoSpot1ImageOnClickURL}');"
														value=";"
														style="background-color: white; border-style: none"
														title="#{currentOfferHandler.onePageRightColumnVideoSpot1ImageAltText}"
														tabindex="99">
														<hx:graphicImageEx styleClass="graphicImageEx"
															id="imageExVideoSweepsGraphic_B"
															value="#{currentOfferHandler.onePageRightColumnVideoSpot1ImagePath}"
															border="0" hspace="0" vspace="4"
															style="margin: 0px; float: none; text-align: center;clear: static; padding-left: 0px"
															title="#{currentOfferHandler.onePageRightColumnVideoSpot1ImageAltText}"></hx:graphicImageEx>
													</hx:outputLinkEx>
													<h:outputText styleClass="outputText" id="textEEVideoText"
														value="#{currentOfferHandler.onePageRightColumnVideoSpot1HTML}"
														escape="false"></h:outputText>
												</h:panelGrid>

												<h:panelGrid id="rightColImageSpot2" columns="1"
													rendered="#{currentOfferHandler.renderRightColumnImageSpot2}">
													<hx:outputLinkEx styleClass="outputLinkEx"
														id="linkExRightColSpot2Link" target="_blank"
														style="border-width: 0px"
														value="#{currentOfferHandler.onePageRightColumnImageSpot2ImageOnClickURL}"
														rendered="#{currentOfferHandler.renderRightColmnImageSpot2ImageAsClickable}">
														<hx:graphicImageEx styleClass=""
															id="imageExRightColImage2"
															value="#{currentOfferHandler.onePageRightColumnImageSpot2ImagePath}"
															border="0"
															alt="#{currentOfferHandler.onePageRightColumnImageSpot2ImageAltText}"
															title="#{currentOfferHandler.onePageRightColumnImageSpot2ImageAltText}"
															style="clear: static; padding-left: 0px">
														</hx:graphicImageEx>

													</hx:outputLinkEx>
													<hx:graphicImageEx styleClass=""
														id="imageExRightColImage2NoLink"
														value="#{currentOfferHandler.onePageRightColumnImageSpot2ImagePath}"
														border="0"
														alt="#{currentOfferHandler.onePageRightColumnImageSpot2ImageAltText}"
														title="#{currentOfferHandler.onePageRightColumnImageSpot2ImageAltText}"
														style="clear: static; padding-left: 0px"
														rendered="#{not currentOfferHandler.renderRightColmnImageSpot2ImageAsClickable}">
													</hx:graphicImageEx>
												</h:panelGrid>
												<h:panelGrid styleClass="panelGrid"
													id="gridRightColHTMLSpot2Grid" columns="1"
													style="text-align: center">
													<h:outputText styleClass="" id="RightColHTMLSpot2Text"
														value="#{currentOfferHandler.onePageRightColumnHTMLSpot2HTML}"
														escape="false"></h:outputText>
												</h:panelGrid>
												<h:panelGrid id="rightColImageSpot3" columns="1"
													rendered="#{currentOfferHandler.renderRightColumnImageSpot3}">
													<hx:outputLinkEx styleClass="outputLinkEx"
														id="linkExRightColSpot3Link" style="border-width: 0px"
														value="#{currentOfferHandler.onePageRightColumnImageSpot3ImageOnClickURL}"
														rendered="#{currentOfferHandler.renderRightColmnImageSpot3ImageAsClickable}">
														<hx:graphicImageEx styleClass=""
															id="imageExRightColImage3"
															value="#{currentOfferHandler.onePageRightColumnImageSpot3ImagePath}"
															border="0"
															alt="#{currentOfferHandler.onePageRightColumnImageSpot3ImageAltText}"
															title="#{currentOfferHandler.onePageRightColumnImageSpot3ImageAltText}"
															style="clear: static; padding-left: 0px">
														</hx:graphicImageEx>
													</hx:outputLinkEx>
													<hx:graphicImageEx styleClass=""
														id="imageExRightColImage3NoLink"
														value="#{currentOfferHandler.onePageRightColumnImageSpot3ImagePath}"
														border="0"
														alt="#{currentOfferHandler.onePageRightColumnImageSpot3ImageAltText}"
														title="#{currentOfferHandler.onePageRightColumnImageSpot3ImageAltText}"
														style="clear: static; padding-left: 0px"
														rendered="#{not currentOfferHandler.renderRightColmnImageSpot3ImageAsClickable}">
													</hx:graphicImageEx>
												</h:panelGrid>
												<h:panelGrid styleClass="panelGrid" id="gridGeoTrust">

													<!-- Place Secure Site Seal in this column -->
													<!-- GeoTrust True Site[tm] Smart Icon tag. Do not edit. -->
													<script LANGUAGE="JavaScript" TYPE="text/javascript"
														SRC="https://smarticon.geotrust.com/si.js"></script>
													<!-- end GeoTrust Smart Icon tag -->

												</h:panelGrid>
											</div>

											<!-- End jspPanelFormPanel -->
										</hx:jspPanel>


										<!-- end mainBodyGrid -->
									</h:panelGrid>

									<!-- Force EZ-PAY  Learn More -->
									<hx:panelDialog type="modeless" styleClass="panelDialog"
										id="dialogEZPayLearnMore1" for="textEZPayLearnMore"
										relativeTo="textEZPayLearnMore" title="Learn More"
										align="relative" valign="relative">
										<h:panelGrid styleClass="panelGrid"
											id="gridEZPayLearnMoreGrid2" width="300">
											<h:outputFormat styleClass="outputText"
												id="formatEZPayHelpText"
												value="#{labels.EZPayNonFreePlanTextUnderTermsMultiPubFAQ}"
												escape="false">
												<%-- 												<f:param value="#{currentOfferHandler.EZPayFreeWeeksOffer}" --%>
												<%-- 													id="param7"></f:param> --%>
												<f:param
													value="#{currentOfferHandler.currentOffer.product.customerServicePhone}"
													id="param8"></f:param>
											</h:outputFormat>
										</h:panelGrid>
										<h:panelGroup id="group1" styleClass="panelDialog_Footer">

											<hx:commandExButton id="button2" styleClass="commandExButton"
												type="submit" value="Close">
												<hx:behavior event="onclick" behaviorAction="hide;stop"
													targetAction="dialogEZPayLearnMore1" id="behaviorEZPay4"></hx:behavior>
											</hx:commandExButton>

										</h:panelGroup>
									</hx:panelDialog>
									<!-- StartDate  Learn More -->
									<hx:panelDialog type="modeless" styleClass="panelDialog"
										id="dialogStartDateLearnMore1" for="textStartDateLearnMore"
										relativeTo="textStartDateLearnMore" title="Learn More"
										align="relative" valign="relative">
										<h:panelGrid styleClass="panelGrid"
											id="gridStartDateLearnMoreGrid2" width="300">
											<h:outputFormat styleClass="outputFormat"
												id="formatGiftFutureStartDateText"
												value="#{labels.startDateInfo}" escape="false">
												<f:param
													value="#{currentOfferHandler.earliestPossibleStartDateString}"
													id="param1"></f:param>
												<f:param
													value="#{currentOfferHandler.latestPossibleStartDateString}"
													id="param2"></f:param>
												<f:param value="#{currentOfferHandler.productName}"
													id="param3"></f:param>
											</h:outputFormat>
										</h:panelGrid>
										<h:panelGroup id="groupStartDate1"
											styleClass="panelDialog_Footer">

											<hx:commandExButton id="buttonStartDate2"
												styleClass="commandExButton" type="submit" value="Close">
												<hx:behavior event="onclick" behaviorAction="hide;stop"
													targetAction="dialogStartDateLearnMore1"
													id="behaviorStartDate4"></hx:behavior>
											</hx:commandExButton>

										</h:panelGroup>
									</hx:panelDialog>
									<!-- Num Qty Learn More -->
									<hx:panelDialog type="modeless" styleClass="panelDialog"
										id="dialogQty" relativeTo="textNumCopiesLearnMoreLink"
										for="textNumCopiesLearnMoreLink" title="Learn More"
										showTitleCloseButton="true" align="relative" valign="relative">
										<h:panelGrid styleClass="panelGrid" id="grid2" width="250">
											<h:outputText styleClass="outputText" id="text3"
												value="#{labels.qtyLearnMore}" escape="false"></h:outputText>
										</h:panelGrid>


										<h:panelGroup id="group2" styleClass="panelDialog_Footer">

											<hx:commandExButton id="button5" styleClass="commandExButton"
												type="submit" value="Close">
												<hx:behavior event="onclick" behaviorAction="hide;stop"
													id="behavior5" targetAction="dialogQty"></hx:behavior>
											</hx:commandExButton>
										</h:panelGroup>

									</hx:panelDialog>

									<hx:panelDialog type="modeless" styleClass="panelDialog"
										id="dialogClubNum" title="Learn More"
										for="textProgInfoLearnMoreLink" align="relative"
										valign="relative" relativeTo="textProgInfoLearnMoreLink">
										<h:panelGrid styleClass="panelGrid" id="gridClubLearnMore"
											width="230">
											<h:outputText styleClass="outputText" id="textClubLearnMore"
												value="#{labels.learnMoreClubNum}" escape="false"></h:outputText>
										</h:panelGrid>

										<h:panelGroup id="group3" styleClass="panelDialog_Footer">


											<hx:commandExButton id="button1" styleClass="commandExButton"
												type="submit" value="Close">
												<hx:behavior event="onclick" behaviorAction="hide;stop"
													id="behavior2" targetAction="dialogClubNum"></hx:behavior>
											</hx:commandExButton>
										</h:panelGroup>




									</hx:panelDialog>

									<hx:panelDialog type="modeless" styleClass="panelDialog"
										id="dialogCVV" align="relative" valign="relative"
										for="textCVVLearnMore" relativeTo="textCVVLearnMore"
										title="Learn More">

										<h:panelGrid styleClass="panelGrid" id="gridCVVLearn"
											width="230">
											<f:facet name="footer">
												<h:panelGroup styleClass="panelGroup" id="group7">

												</h:panelGroup>
											</f:facet>
											<f:facet name="header">
												<h:panelGroup styleClass="panelGroup" id="group6">
													<h:outputText styleClass="outputText" id="text4"
														value="#{labels.learnMoreCVV}" escape="false"></h:outputText>
												</h:panelGroup>
											</f:facet>

										</h:panelGrid>
										<h:panelGroup id="group5" styleClass="panelDialog_Footer">
											<hx:commandExButton id="button4" styleClass="commandExButton"
												type="submit" value="Close">
												<hx:behavior event="onclick" behaviorAction="hide;stop"
													id="behavior4" targetAction="dialogCVV"></hx:behavior>
											</hx:commandExButton>
										</h:panelGroup>
									</hx:panelDialog>
									<!-- Delivery Method Panel -->

									<hx:panelDialog type="modeless" styleClass="panelDialog"
										id="dialogDelMethodDialog" for="linkExDelMethodCheckHelpLink"
										relativeTo="textDelMethodHelpText" initiallyShow="false"
										title="How will my paper be delivered?    ">
										<h:panelGroup id="groupDelMethod1"
											styleClass="panelDialog_Footer">
											<h:panelGrid styleClass="panelGrid" id="gridDelMethod2"
												width="270" style="text-align: left">
												<h:outputText styleClass="outputText"
													id="textDelMethodDetailHelp"
													value="#{labels.DelMethodHelpDetail}" escape="false"></h:outputText>
											</h:panelGrid>
											<hx:commandExButton id="buttonDelMethod"
												styleClass="commandExButton" type="submit" value="Close">
												<hx:behavior event="onclick" behaviorAction="hide;stop"
													targetAction="dialogDelMethodDialog"
													id="behaviorDelMethod4"></hx:behavior>
											</hx:commandExButton>
										</h:panelGroup>
									</hx:panelDialog>
									<!-- Guarantee Window -->
									<div id="checkoutGuaranteeDiv" style="display: none;">
										<div id="templateChatTopDiv">
											<span onclick="checkoutGuaranteeHide();"
												style="cursor: pointer;">Close Window<img
												src="/theme/themeV3/themeImages/close_14x14.png" width="14"
												height="14" border="0"
												style="margin-left: 5px; margin-right: 5px" /> </span>
										</div>
										<div id="tempateChatImageDiv">
											<img title="Satisfaction Guaranteed!"
												src="/images/marketingimages/ckoutGuarantee.jpg" hspace="0"
												vspace="5">
										</div>

										<div id="checkoutGuaranteeDivHeader">
											<span class="USAT_BlueSubHeader">&nbsp;&nbsp;&nbsp;USA
												TODAY guarantees your satisfaction!</span>
										</div>

										<div id="checkoutGuaranteeTextDiv">
											<span class="outputText"><br>At USA TODAY, we
												want you to be 100% satisfied with your subscription. If you
												are not satisfied for any reason, we will provide you with a
												refund on all undelivered issues remaining in your
												subscription term. You can cancel your subscription by
												calling <br>1-800-872-0001.</span>
										</div>
									</div>
									<!-- End Guarantee Window -->
									<!-- Additional Address Learn More -->
									<hx:panelDialog type="modeless" styleClass="panelDialog"
										id="dialogAdditionalAddrHelp"
										relativeTo="formItemDeliveryAddress2" for="textAddrLearnMore"
										title="Learn More">

										<h:panelGroup id="groupAddlAddrHelpGroup1"
											styleClass="panelDialog_Footer">



											<h:panelGrid styleClass="panelGrid" id="grid1" columns="1"
												width="250" style="text-align: left">
												<h:outputText styleClass="outputText"
													id="textAdditionalAddrLearnMoreInfoText"
													value="#{labels.AdditionalAddrLearnMore}"></h:outputText>

											</h:panelGrid>
											<hx:commandExButton id="button3" styleClass="commandExButton"
												type="submit" value="Close">
												<hx:behavior event="onclick" behaviorAction="hide;stop"
													targetAction="dialogAdditionalAddrHelp" id="behavior3"></hx:behavior>
											</hx:commandExButton>



										</h:panelGroup>
									</hx:panelDialog>
									<!-- Delivery Method Panel -->
									<hx:inputHelperSetFocus id="setFocus1"></hx:inputHelperSetFocus>
									<h:inputHidden value="#{newSubscriptionOrderHandler.startDate}"
										id="startDate"
										rendered="#{trialCustomerHandler.isTrialConversion}">
										<hx:convertDateTime />
									</h:inputHidden>
									<!-- BEGIN On Exit Pop Area - DO NOT CHANGE NAMES OF DIV ID -->
									<hx:jspPanel id="jspPanelPopOverlayPanel"
										rendered="#{currentOfferHandler.isShowOrderPathOverlayPopUp}">
										<div id="popunder" style="display: none"></div>
										<div id="leavingofferdiv" style="display: none;">
											<h:panelGroup styleClass="panelGroup"
												id="groupSpecialOfferGroupBox"
												style="height: 100%; width: 100%">
												<h:panelGrid styleClass="panelGrid"
													id="gridSpecialOfferLayoutGrid" width="100%" columns="1"
													style="text-align: right">
													<hx:outputLinkEx styleClass="outputLinkEx"
														id="linkExCloseSpecialOffer" value="#"
														onclick="return closeSpecialOffer();">
														<h:outputText id="textCloseSpecialOfferText"
															styleClass="outputText" value="Close [X]"></h:outputText>
													</hx:outputLinkEx>
													<hx:outputLinkEx styleClass="outputLinkEx"
														id="linkExSpecialOfferLink"
														value="#{currentOfferHandler.orderPathOverlayPopUpImageLinkURL}">
														<hx:graphicImageEx styleClass="graphicImageEx"
															id="tooimageExSpecialOfferImage"
															value="#{currentOfferHandler.orderPathOverlayPopUpImagePath}"
															style="padding: 0px"></hx:graphicImageEx>
													</hx:outputLinkEx>
												</h:panelGrid>
											</h:panelGroup>
										</div>
									</hx:jspPanel>

									<h:inputHidden
										value="#{currentOfferHandler.isShowOrderPathOverlayPopUp}"
										id="showOverlayPopUp"></h:inputHidden>
									<!-- END On Exit Pop Area -->
									<f:verbatim>
										<h:outputText styleClass="outputText" id="textScriptInsert1"
											escape="false"
											value="#{currentOfferHandler.onePageJavaScriptText}"></h:outputText>
									</f:verbatim>
									<h:inputHidden value="#{currentOfferHandler.isForceBillMe}"
										id="isForceBillMe"></h:inputHidden>
									<h:inputHidden
										value="#{newSubscriptionOrderHandler.paymentMethod}"
										id="paymentMethodHidden"
										rendered="#{currentOfferHandler.isForceBillMe}"></h:inputHidden>

									<hx:behaviorKeyPress key="Enter" id="behaviorKeyPress1"
										behaviorAction="click"></hx:behaviorKeyPress>

									<h:inputHidden
										value="#{currentOfferHandler.EZPayFreeWeeksOffer}"
										id="numberEZPayFreeWeeks"></h:inputHidden>
								</h:form>
								<div id="formSubmittedOverlay" style="display: none;"></div>

							</hx:scriptCollector>

							<!-- Ve Interactive USAT Only-->
							<%
// 				String veInteractive = "";
// 				if (pubCode.equalsIgnoreCase(UsaTodayConstants.UT_PUBCODE)) {
// 					veInteractive = "<script type=\"text/javascript\">var journeycode='75e65509-bb2e-4b0a-b56c-a08755cd214b';var captureConfigUrl='cdsusa.veinteractive.com/CaptureConfigService.asmx/CaptureConfig'; (function() {     var ve = document.createElement('script'); ve.type = 'text/javascript'; ve.async = true;     ve.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'configusa.veinteractive.com/vecapturev5.js';     var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ve, s);})();</script>";
// 				}
				 %>
							<%-- 						<%=veInteractive %> --%>
							<!-- end Ve Interactive UT -->
							<!-- Ve Interactive EE Only-->
							<%
// 				String eeVeInteractive = "";
// 				if (pubCode.equalsIgnoreCase(UsaTodayConstants.DEFAULT_UT_EE_PUBCODE)) {
				
// 					eeVeInteractive = "<input id=\"usatodayeedition\" type=\"hidden\" value=\"usatodayeedition\"><script type=\"text/javascript\">var journeycode='75e65509-bb2e-4b0a-b56c-a08755cd214b';var captureConfigUrl='cdsusa.veinteractive.com/CaptureConfigService.asmx/CaptureConfig'; (function() {     var ve = document.createElement('script'); ve.type = 'text/javascript'; ve.async = true;     ve.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'configusa.veinteractive.com/vecapturev7.js';     var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ve, s);})();</script>";
// 				}
				 %>
							<%-- 						<%=eeVeInteractive %>			 --%>

							<!-- end Ve Interactive EE -->
							<!-- Google remarketing -->
							<!-- 				<script type="text/javascript"> -->
							<!-- 					/*           */ -->
							<!-- 					var google_conversion_id = 1071907126; -->
							<!-- 					var google_conversion_language = "en"; -->
							<!-- 					var google_conversion_format = "3"; -->
							<!-- 					var google_conversion_color = "666666"; -->
							<!-- 					var google_conversion_label = "-P-9CO-XhgIQtoKQ_wM"; -->
							<!-- 					var google_conversion_value = 0; -->

							<!-- 					// above setting for UT -->
							<!-- 					if (currentPub == "BW") { -->
							<!-- 						google_conversion_label = "6rriCN-ZhgIQtoKQ_wM"; -->
							<!-- 					} else if (currentPub == "EE") { -->
							<!-- 						google_conversion_label = "uOqFCOeYhgIQtoKQ_wM"; -->
							<!-- 					} -->
							<!-- 					/*     */ -->
							<!-- 				</script> -->
							<!-- 							<script type="text/javascript" -->
							<%-- 								src="<%=docProtocol %>//www.googleadservices.com/pagead/conversion.js"> --%>

							<!-- 							</script> -->
							<!-- end google Remarketing -->
							<!-- - FetchBack Remarketing frame -->
							<!-- 							<iframe -->
							<!-- 								src='https://pixel.fetchback.com/serve/fb/pdj?cat=&name=landing&sid=4683' -->
							<!-- 								scrolling='no' width='1' height='1' marginheight='0' marginwidth='0' -->
							<!-- 								frameborder='0'></iframe> -->
							<!-- - End FetchBack Remarketing frame -->
							<!-- end google Remarketing -->
							<!-- - FetchBack Remarketing frame -->
							<!-- 			<iframe src='https://pixel.fetchback.com/serve/fb/pdj?cat=&name=landing&sid=4683' scrolling='no' width='1' height='1' marginheight='0' marginwidth='0' frameborder='0'></iframe> -->
							<!-- - End FetchBack Remarketing frame -->
						<%-- /tpl:put --%>
</DIV>
<%-- /tpl:put --%>
				<DIV id="clear"></DIV>
				<DIV id="bluebar">
					<a href="/feedback/feedback.jsp"
						style="color: white; font-weight: bold; text-decoration: none; font-size: 14px"
						title="Contact Us">Contact Us</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;1-800-872-0001&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a
						href="/faq/utfaq.jsp"
						style="color: white; font-weight: bold; text-decoration: none; font-size: 14px"
						title="Frequently Asked Questions">FAQ</a>
				</DIV>
				<%-- tpl:put name="footerMainArea" --%>
	<%-- tpl:put name="footerArea_1" --%>
						<!-- optional footer content area -->

					<%-- /tpl:put --%>
	<%=trackingBug.getOmnitureHTML() %>
<%-- /tpl:put --%>
				<DIV id=graybar>
					Copyright
					<script>
						document.write(getCurrentYear());
					</script>
					USA TODAY, a division of <a href="http://www.gannett.com">Gannett
						Co. Inc.</a> <a href="/privacy/privacy.htm">Privacy
						Notice/California Privacy Notice</a>. <a
						href="/privacy/privacy.htm#adchoice">Ad Choices</a>.<br> By
					using this service, you accept our <a href="/service/service.jsp">Terms
						of Service</a>.
				</DIV>
				<DIV id=copyright>
					<P>Offers available to new subscribers only. Home delivery not
						available in some areas. Not valid with any other offer.
						Prepayment required. After the promotional period ends, the
						subscription will continue and be billed monthly at the then
						regular rate, less any applicable credits, unless USA TODAY is
						otherwise notified. Applicable taxes may not be included in the
						rate. If at any time you decide to cancel your subscription, you
						may contact customer service at 1-800-872-0001 and the full amount
						of any balance over $2.00 will be returned.</P>
				</DIV>
				<!--END OF COPYWRITE-->
				<DIV id=footer></DIV>
			</DIV>
			<!--END OF WHITEBOX-->
		</DIV>
		<!--END OF CONTENT-->
	</DIV>
</BODY>
</HTML><%-- /tpl:insert --%>
</f:view><%-- /tpl:insert --%>
<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/subscriptions/order/Checkout.java" --%><%-- /jsf:pagecode --%>