<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/subscriptions/order/Thankyou.java" --%><%-- /jsf:pagecode --%>
<%-- tpl:insert page="/theme/themeV3/jsfTheme/subscriberPortalLeftNav_JSF.jtpl" --%><!DOCTYPE HTML><%@page import="com.usatoday.business.interfaces.products.ProductIntf"%>
<%@page import="com.usatoday.businessObjects.products.ProductBO"%>
<%@page import="com.usatoday.util.MobileDeviceDetectionUtil"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%><%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%><%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="com.usatoday.business.interfaces.products.promotions.HTMLPromotionIntf"%><f:view><f:loadBundle basename="resourceFile" var="labels" />
	<%-- tpl:insert page="/theme/subscriptionPortalRoot.htpl" --%><HTML>
<HEAD>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8">
<%-- tpl:put name="headerareaTop" --%>
<%
	String docProtocol = "https:";
	if (!request.isSecure()) { 
		docProtocol = "http:";
	}
// following work around to get around problem with RAD 8.0.2 automated links in templates - APE
	String subcribeJS = "javascript:_subscribeToPub();"; 
	String subcribeIntJS = "javascript:_subscribeToInternationalEdition();"; 
	String backIssueJS = "javascript:_shopUSAT();";
	
	// change this to any javascript you want called on page load
	String initFunction = "";
	String unloadFunction = "";
	String onBeforeUnloadFunction = "";
	String navigationSubscribeLinkValue = "/subscriptions/index.jsp";
	String electronicEditionLink = "/subscriptions/order/checkout.faces?pub=EE";
	String internationalEditionLink = "/international/welcomeint.jsp";
	String FAQLink = "/faq/utfaq.jsp"; 
	String subscribeByMail = "/subscriptions/subscribebymail.html";
	String dynamicNavigation = "";
	
	com.usatoday.esub.common.WebBug trackingBug = com.usatoday.esub.common.UTCommon.getWebAnalyticsBug();
	try {
		trackingBug.deriveValuesFromRequest(request);
	}
	catch (Exception e) {
		; // ignore
	}
	
	String pubCode = "";
	String productName = "USA TODAY";
	
    boolean isEEditionProduct = true;

	com.usatoday.businessObjects.products.promotions.PromotionSet defaultPromotionSet = null;
	com.usatoday.businessObjects.products.promotions.PromotionSet currentOfferPromotionSet = null;
	com.usatoday.business.interfaces.customer.CustomerIntf customer = null;
   	com.usatoday.businessObjects.products.promotions.PromotionSet currentAccountPromotionSet = null;
   		
	com.usatoday.business.interfaces.products.OfferIntf offer = null;   		
	try {
		offer = com.usatoday.esub.common.UTCommon.getCurrentOfferVersion2(request);
		pubCode = com.usatoday.util.constants.UsaTodayConstants.UT_PUBCODE;
		if (offer != null) {
			pubCode = offer.getPubCode();
			try {
				String brandingPubCode = offer.getProduct().getBrandingPubCode();
				ProductIntf bp = ProductBO.fetchProduct(brandingPubCode);
				productName = bp.getName();
			}
			catch (Exception teee) {
			}				
		}
		// get the default promotion set for the publication		
		defaultPromotionSet = com.usatoday.esub.common.UTCommon.getDefaultPromotionSetForPub(pubCode);
		
		// get the keycode specific promotion set
		
		if (offer != null && offer instanceof com.usatoday.business.interfaces.products.SubscriptionOfferIntf) {
			currentOfferPromotionSet = ((com.usatoday.business.interfaces.products.SubscriptionOfferIntf)offer).getPromotionSet();
		}
		boolean isSignedIn = false;
		customer = (com.usatoday.business.interfaces.customer.CustomerIntf)session.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_CUSTOMER_INFO);
		if (customer != null) {
			isSignedIn = true;
			com.usatoday.business.interfaces.products.SubscriptionProductIntf product = com.usatoday.businessObjects.products.SubscriptionProductBO.getSubscriptionProduct(customer.getCurrentEmailRecord().getPubCode());
			isEEditionProduct = product.isElectronicDelivery(); 
			// This promotion set is based on pub and keycode on the customer account
			currentAccountPromotionSet = com.usatoday.businessObjects.products.promotions.PromotionManager.getInstance().getPromotionsForOffer(customer.getCurrentAccount().getPubCode(), customer.getCurrentAccount().getKeyCode());
			if (isEEditionProduct) {
			}
		}

		if (pubCode.equalsIgnoreCase(com.usatoday.util.constants.UsaTodayConstants.SW_PUBCODE)) {
			navigationSubscribeLinkValue = "/subscriptions/order/checkout.faces";
			electronicEditionLink = "http://www.usatoday.com/marketing/brand_mkt/splash/spw_electronic_edition/index.html";
			internationalEditionLink = "/international/welcomeswint.jsp";
			FAQLink = "/faq/bwfaq.jsp";
			subscribeByMail = "/subscriptions/subscribebymail_sw.html";
		}
		
		if (currentOfferPromotionSet != null) {
			com.usatoday.business.interfaces.products.promotions.HTMLPromotionIntf dPromo = currentOfferPromotionSet.getDynamicNavigationOrderEntryHTML();
			if (dPromo != null) {
				dynamicNavigation = dPromo.getPromotionalHTML();
			}
		}
	} catch (Exception e) {
	
	}
	
	String mobileCSS = "";
	if (MobileDeviceDetectionUtil.isSmallScreenDevice(request)) {
		mobileCSS = "<LINK href=\"/theme/themeV3/css/smallScreen.css\" rel=\"stylesheet\" type=\"text/css\">";
	}
	
 %>
<script src="/common/prototype/prototype.js"></script>
<script src="/common/prototype/scriptaculous/scriptaculous.js" type="text/javascript"></script>
<link rel="stylesheet"	href="/theme/themeV3/css/stylesheet_jsfcomponents.css"	type="text/css">
<%-- tpl:put name="headerarea_Top" --%>
		<title>USA TODAY</title>
	<%-- /tpl:put --%>
		<%-- /tpl:put --%>
<LINK REL="SHORTCUT ICON" HREF="/favicon.ico">
<LINK REL="icon" href="/favicon.ico" TYPE="image/ico" />
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META http-equiv="Content-Style-Type" content="text/css">
<meta name="viewport" content="initial-scale=1.0">
<LINK href="/theme/themeV3/css/usatodayBase.css" rel="stylesheet"
	type="text/css">
<LINK href="/theme/themeV3/css/usatodayAddOn.css" rel="stylesheet"
	type="text/css">
<script src="/common/usatCommon.js"></script>
<%-- tpl:put name="headareaBottom" --%>
<%=mobileCSS %>
<%-- tpl:put name="headarea_Bottom" --%>
		<!-- Bottom Header area -->
	<%-- /tpl:put --%>
<script>
var currentPub = "<%=pubCode %>";

// method called on page load
function initPage(thisObj, thisEvent) {
	// insert site-wide code here

	<%=initFunction %>
}
	function unloadPage(thisObj, thisEvent) {		
		<%=unloadFunction %>
	}
				
	function onBeforeUnloadPage(thisObj, thisEvent) {		 
		<%=onBeforeUnloadFunction %>
	}
	
	function subscribeToPub() {
		document.location = "<%=navigationSubscribeLinkValue%>";
	}	
	
	function subscribeToElectronicEdition() {
		document.location = "<%=electronicEditionLink %>";
	}
	
	function subscribeToElectronicEditionOrderPage() {
		document.location = "<%=electronicEditionLink %>";	
	}
	
	function subscribeToInternationalEdition() {
		document.location = "<%=internationalEditionLink%>";
	}

	function loadFAQ() {
		window.open('<%=FAQLink%>','FAQ', 'width=825,height=800,menubar=no,titlebar=yes,scrollbars=yes,status=no,location=no,directories=no');
	}

	function subscribeByMail() {
		document.location = "<%=subscribeByMail%>";
	}
</script>
<%-- /tpl:put --%>
<script type="text/javascript">
	/*   var _gaq = _gaq || [];
	 _gaq.push(['_setAccount', 'UA-23158809-1']);
	 _gaq.push(['_trackPageview']);

	 (function() {
	 var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	 ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	 var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	 })();
	 */
</script>
</HEAD>
<body>
	<DIV id="wrapper">
		<DIV id="header">
			<div id="headerLeft" onclick="templateLogoClick();"></div>
			<div id="headerRight">
				<%-- tpl:put name="headerbodyarea" --%>
					<%-- tpl:insert page="/login/loginform.jspf" --%><div style="float: none">
	<%
		String uri = request.getRequestURI();
		String pageName = uri.substring(uri.lastIndexOf("/") + 1);
		String jScript = "";
		if (uri.indexOf("subscriptions") > 0 && uri.indexOf("thankyou.jsp") > 0) {
			try {
				com.usatoday.esub.handlers.ShoppingCartHandler shoppingCartHandler = (com.usatoday.esub.handlers.ShoppingCartHandler) session
						.getAttribute("shoppingCartHandler");
				if (shoppingCartHandler != null) {
					jScript = shoppingCartHandler.getOnePageThankYouJavaScriptTealiumText();
				}
			} catch (Exception E) {

			}
		} else {
			com.usatoday.businessObjects.products.promotions.PromotionSet currentOfferPromotionSetTealium = null;
			com.usatoday.business.interfaces.products.OfferIntf offerTealium = com.usatoday.esub.common.UTCommon
					.getCurrentOffer(request);
			if (offerTealium != null && offerTealium instanceof com.usatoday.business.interfaces.products.SubscriptionOfferIntf) {
				currentOfferPromotionSetTealium = ((com.usatoday.business.interfaces.products.SubscriptionOfferIntf) offerTealium)
						.getPromotionSet();
				jScript = currentOfferPromotionSetTealium.getPromoOnePageJavaScriptTealiumText();
				jScript = org.apache.commons.lang3.StringUtils.replace(jScript, "page_name:\"\"", "page_name:\"" + uri + "\"");
			}
		}
	%>
	<%=jScript%>
</div>
<%-- 			<f:verbatim>
				<h:outputText styleClass="outputText" id="textThankYouJS13"	escape="false" value="#{shoppingCartHandler.tealiumOnePageThankYouJavaScriptText}"></h:outputText>
			</f:verbatim>
 --%>
<%
	String isLoginDisplay = "true";
	com.usatoday.business.interfaces.customer.CustomerIntf customer2 = null;
	String loggedinDivClass = "hideMe";
	String notLoggedinDivClass = "showMeInline";
	String firstName = "";
	String lastName = "";
	String firmInfo = "";
	String accountNum = "";
	String loginErrorMessage2 = "";
	String logOut = "";
	String publication = "";
	String changeAccountLink = "";
	String earlyAlertDisplay = "display:none";

	String jScriptNoOp = "javascript:;";

	try {
		publication = (String) session.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_PUB_NAME);
		if (publication == null) {
			publication = request.getParameter("pub");
			if (publication == null) {
				publication = com.usatoday.util.constants.UsaTodayConstants.UT_PUBCODE;
			}
		}
		if (publication.equals(com.usatoday.esub.common.UTCommon.PUB_NAMES[0])) { // USATODAY publication
			logOut = com.usatoday.esub.common.UTCommon.BASE_URL + "/index.jsp?pub=UT&logsessobj=rem";
		} else {
			logOut = com.usatoday.esub.common.UTCommon.BASE_URL + "/index.jsp?pub=BW&logsessobj=rem";
		}

		// If customer is logged in remove login page, otherwise display it
		customer2 = (com.usatoday.business.interfaces.customer.CustomerIntf) session
				.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_CUSTOMER_INFO);
		if (customer2 == null) {
			// showMeInling and hideMe are defined in style sheets
			loggedinDivClass = "showMeInline";
			notLoggedinDivClass = "hideMe";
		} else {

			if (customer2.getCurrentAccount() != null) {
				firstName = customer2.getCurrentAccount().getDeliveryContact().getFirstName();
				if (firstName != null && !firstName.trim().equals("")) {
					firstName = com.usatoday.esub.common.UTCommon.formatNameCapitalization(firstName); // Format it
				}
				lastName = customer2.getCurrentAccount().getDeliveryContact().getLastName();
				if (lastName != null && !lastName.trim().equals("")) {
					lastName = com.usatoday.esub.common.UTCommon.formatNameCapitalization(lastName); // Format it
				}
				firmInfo = customer2.getCurrentAccount().getDeliveryContact().getFirmName();
				if (customer2.getNumberOfAccounts() > 1) {
					changeAccountLink = "[&nbsp;<a href=\"/account/select_accnt.jsp\">Change</a>&nbsp;]";
				}
				try {
					accountNum = customer2.getCurrentAccount().getAccountNumber();
					//				if (accountNum == null || accountNum.length() < 4) {
					if (accountNum == null) {
						accountNum = "";
					}
					//				else {
					/*
					if (accountNum.equalsIgnoreCase("340000000")) {
						accountNum = "Pending Setup";
					}
					 */
					//				}
				} catch (Exception eeee) {
					eeee.printStackTrace();
				}

				//EarlyAlert - Check to see if their paper delivery has been affected by a delay
				// HD-CONS109 SEW
				/*
				if (customer2.getCurrentAccount().isShowDeliveryAlert()) {
				earlyAlertDisplay = "display:inline";
				}
				 */

			} else {
				if (customer2.getCurrentEmailRecord() != null
						&& customer2.getCurrentEmailRecord().getEmailAddress() != null) {
					firstName = customer2.getCurrentEmailRecord().getEmailAddress();
					accountNum = "Pending Setup";
				} else {
					firstName = "";
				}
			}
		}
		loginErrorMessage2 = (String) session.getAttribute("LOGIN_FAILED_MESSAGE");
		if (loginErrorMessage2 == null || loginErrorMessage2.trim().equals("")) {
			loginErrorMessage2 = "";
		} else {
			session.removeAttribute("LOGIN_FAILED_MESSAGE");
		}
	} catch (Exception e) {
		e.printStackTrace();
		if (firstName == null) {
			firstName = "";
		}
		if (lastName == null) {
			lastName = "";
		}
	}
	String nameInfo = firstName + " " + lastName;
	String loginInfo = null;
	if (!nameInfo.trim().equals("")) {
		loginInfo = nameInfo;
	} else {
		if (firmInfo != null && !firmInfo.trim().equals("")) {
			loginInfo = firmInfo;
		}
	}
	// if no login info, then hide data
	if (loginInfo == null) {
		loggedinDivClass = "showMeInline";
		notLoggedinDivClass = "hideMe";
		loginInfo = "";
	}
%>

<%@page import="com.usatoday.util.constants.UsaTodayConstants"%><div
	id="loginDisplayDiv" class="<%=loggedinDivClass%>">
	<table border="0" width="100%" height="60px">
		<tr>
			<td>
				<TABLE class="loginFormTable">
					<TBODY>
						<TR>
							<TD class="loginFormColumn"><br /> <a
								href="/login/auth.faces">Login to Manage Account</a></TD>
						</TR>
						<tr>
							<td class="loginFormColumn" align="right"><img
								style="cursor: pointer; margin-top: 10px;"
								onclick="javascript: templateClickToChatClickedGuest(this, event);"
								alt="Click for Assistance" title="Click for Assistance"
								src="${pageContext.request.contextPath}/theme/themeV3/themeImages/chatIcon136.png"
								hspace="0" vspace="5"></td>
						</tr>
					</TBODY>
				</TABLE></td>
		</tr>
	</table>
</div>
<div id="loginHideDiv" class="<%=notLoggedinDivClass%>">
	<table width="100%" border="0" class="loginFormTable">
		<tr>
			<td nowrap align="right">
				<TABLE class="loginFormTable">
					<TBODY>
						<TR>
							<TD class="loginFormColumn" nowrap><span
								class="loginNormalPrint">Hello, <%=loginInfo%>&nbsp;&nbsp;[&nbsp;<a
									href="<%=jScriptNoOp%>"
									onclick="javascript:document.location='<%=logOut%>';_unloadPage(this, event);">Logout</a>&nbsp;]</span>
							</TD>
						</TR>
					</TBODY>
				</TABLE></TD>
		</tr>
		<tr>
			<td class="loginFormColumn" align="right"><span
				class="loginfineprint">Account #:&nbsp;<%=accountNum%>&nbsp;<%=changeAccountLink%></span>
			</td>
		</tr>
		<tr>
			<td class="loginFormColumn" align="right"><div
					id="chatIconRequestDiv">
					<img onclick="javascript: templateClickToChatClicked(this, event);"
						style="cursor: pointer; margin-top: 10px;"
						alt="Click for Assistance" title="Click for Assistance"
						src="${pageContext.request.contextPath}/theme/themeV3/themeImages/chatIcon136.png"
						hspace="0" vspace="5">
				</div>
			</td>
		</tr>
	</table>
</div>
<div id="templateChatWindowDiv" style="display: none;">
	<div id="templateChatTopDiv">
		<span onclick="templateClickToChatHide();" style="cursor: pointer;">No
			Thanks<img src="/theme/themeV3/themeImages/close_14x14.png"
			width="14" height="14" border="0"
			style="margin-left: 5px; margin-right: 5px" /> </span>
	</div>
<!-- 	<div id="tempateChatImageDiv">
		<a
			onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
			href=""><img border="0"
			src="https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY" />
		</a>
	</div>
 -->	<div id="templateChatAccountLinksDiv">
		<ul>
			<li>To report a problem with delivery, click <a
				href="/account/cp/delissue.faces"> here</a>.</li>
			<li>To place your delivery on a temporary hold, click <a
				href="/account/vacationholds/holds.faces"> here</a>.</li>
			<li>Forgot your password or login? Click <a
				href="/idpassword/forgotpassword.jsp"> here</a>.
			<li>To contact us <a href="/feedback/feedback.jsp"> click here</a>.</li>				
<!-- 			<li>To cancel your subscription, please call 1-800-872-0001 and -->
<!-- 				select option 6.</li> -->
		</ul>
	</div>

	<div id="templateChatLinksDiv">
		<ul>

			<li><a
				onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
				href="">Click to Chat</a>
			</li>
		</ul>
	</div>
	<div id="templateChatBottomDiv">
		<span class="outputText">Or, call us at:<br> <b>Call
				1-877-713-6241</b><br>(M-F 8:00 a.m. to 7 p.m. E.T.)</span>
	</div>
</div>
<div id="templateChatWindowGuestDiv" style="display: none;">
	<div id="templateChatTopDiv">
		<span onclick="templateClickToChatHideGuest();"
			style="cursor: pointer;">No Thanks<img
			src="/theme/themeV3/themeImages/close_14x14.png" width="14"
			height="14" border="0" style="margin-left: 5px; margin-right: 5px" />
		</span>
	</div>
<!-- 	<div id="tempateChatImageDiv">
		<a
			onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
			href=""><img border="0"
			src="https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY" />
		</a>
	</div>
 -->	<div id="templateChatAccountLinksDiv">
		<ul>
			<li>To manage your account online - including reporting a
				problem with delivery or placing a temporary hold on your account -
				log in by entering your e-mail address and password. Click <a
				href="/login/auth.faces"> here</a> to access the login page now.</li>
			<li>Forgot your password or login? <a
				href="/idpassword/forgotpassword.jsp"> click here</a>.</li>
			<li>To contact us <a href="/feedback/feedback.jsp"> click here</a>.</li>
			<!-- 			<li>To cancel your subscription, please call 1-800-872-0001 and -->
			<!-- 				select option 6.</li> -->
		</ul>

	</div>

	<div id="templateChatLinksDiv">
		<ul>
			<li><a
				onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
				href="">Click to Chat</a>
			</li>
		</ul>
	</div>
	<div id="templateChatBottomDiv">
		<span class="outputText">Or, call us at:<br> <b>Call
				1-877-713-6241</b><br>(M-F 8:00 a.m. to 7 p.m. E.T.)</span>
	</div>
</div>
<%-- /tpl:insert --%>		
		<%-- /tpl:put --%>
			</div>
		</DIV>
		<!-- end header -->
		<DIV id="content">
			<DIV id="whitebox">
				<%-- tpl:put name="bodyAreaMainWhiteBox" --%>
<DIV id=nav>
	<DIV id="navPromo">
		<%-- tpl:put name="navigationArea_Top" --%><%-- /tpl:put --%>
	</div>
	<DIV id=leftNavList>
		<UL>
		  <LI><A href="/products.html">USA TODAY Products</A></LI>
		  <LI><A href="<%=subcribeJS%>">Subscribe</A></LI>
		  <LI><A href="/account/accounthistory/accountSummary.faces">Manage account</A></LI>
		  <LI><A href="<%=subcribeIntJS%>">Outside the USA</A></LI>
		  <LI><A href="<%=backIssueJS%>">Order back issues</A></LI>
		  <%=dynamicNavigation %>
		</UL>
	</DIV>
	<DIV id="navPromo">
		<%-- tpl:put name="navigationArea_Bottom" --%>
								<f:verbatim>
	<h:outputText styleClass="outputText" id="textThankYouJS13" escape="false" value="#{shoppingCartHandler.onePageThankYouJavaScript13Text}"></h:outputText>
</f:verbatim>

						</div>
						<!-- Conversion Pixel - Final Purchase - DO NOT MODIFY -->
						<img src="https://secure.adnxs.com/px?id=47934&remove=418345&t=2"
							width="1" height="1" />
						<!-- End of Conversion Pixel -->
						<hx:scriptCollector id="scriptCollectorBottomNavCollector">
							<hx:jspPanel id="jspPanelBottomNavCustomPanel"
								rendered="#{trialPartnerHandler.isShowLogoImage}">
								<hx:outputLinkEx styleClass="outputLinkEx" id="linkExTopNavLogoLink"
									value="#{trialPartnerHandler.parnterLogoLinkURL}" target="_blank"
									rendered="#{trialPartnerHandler.isShowLogoImageURL}"
									style="background-color: white; z-index: 50; margin: 0px; padding: 0px; border-width: 0px">
									<hx:graphicImageEx styleClass="graphicImageEx" id="imageExPartnerLogo"
										value="#{trialPartnerHandler.partner.logoImageURL}"
										style="z-index: 50" border="0" width="155" hspace="0" vspace="0"></hx:graphicImageEx>
								</hx:outputLinkEx>
								<hx:graphicImageEx styleClass="graphicImageEx"
									id="imageExPartnerLogoNoLink"
									value="#{trialPartnerHandler.partner.logoImageURL}" style="z-index: 50"
									border="0" width="155" hspace="0" vspace="0"
									rendered="#{not trialPartnerHandler.isShowLogoImageURL}"></hx:graphicImageEx>
							</hx:jspPanel>
							<h:panelGrid id="leftColImageSpot1"
								rendered="#{currentOfferHandler.renderOnePaqeThankYouLeftColumnImageSpot1}">
								<hx:outputLinkEx styleClass="outputLinkEx"
									id="linkExLeftColImageSpot1Link" target="_blank"
									style="background-color: white; z-index: 50; margin: 0px; padding: 0px; border-width: 0px"
									value="#{currentOfferHandler.onePageThankYouLeftColumnImageSpot1ImageOnClickURL}"
									rendered="#{currentOfferHandler.renderOnePageThankYouLeftColmnImageSpot1ImageAsClickable}">
									<hx:graphicImageEx styleClass="" id="imageExLeftColImage1"
										value="#{currentOfferHandler.onePageThankYouLeftColumnImageSpot1ImagePath}"
										border="0"
										title="#{currentOfferHandler.onePageThankYouLeftColumnImageSpot1ImageAltText}"
										style="padding-left: 0px" align="left">
									</hx:graphicImageEx>
								</hx:outputLinkEx>
								<hx:graphicImageEx styleClass="" id="imageExLeftColImage1NoLink"
									value="#{currentOfferHandler.onePageThankYouLeftColumnImageSpot1ImagePath}"
									border="0"
									alt="#{currentOfferHandler.onePageThankYouLeftColumnImageSpot1ImageAltText}"
									title="#{currentOfferHandler.onePageThankYouLeftColumnImageSpot1ImageAltText}"
									style="padding-left: 0px"
									rendered="#{not currentOfferHandler.renderOnePageThankYouLeftColmnImageSpot1ImageAsClickable }"
									align="left">
								</hx:graphicImageEx>
							</h:panelGrid>
						</hx:scriptCollector>
				<%-- /tpl:put --%>
	</div>
</DIV>
<DIV id=pageContent>
	<%-- tpl:put name="MainBodyArea" --%>
				<hx:scriptCollector id="scriptCollector1">
					<h:form styleClass="form" id="formMainForm">
						<h1>
							<h:outputText id="textMainTableHeader" value="#{labels.ThankYouLabel}"
								styleClass="H1Class"></h:outputText>
						</h1>
						<div id="pageContent70">
							<h:panelGrid styleClass="panelGridThankYouPage"
								id="gridThankYouInformation" columns="1" cellpadding="2" cellspacing="2">
								<h:messages styleClass="messages" id="messages1" layout="list"
									style="color: red"></h:messages>
								<hx:jspPanel id="jspPanelEEThankYouTextPanel"
									rendered="#{shoppingCartHandler.showEEWelcomePanel}">

									<ul>
										<li><h:outputText styleClass="" id="textThankYouText2"
												value="#{labels.ThanYouText2}"></h:outputText>&nbsp; <hx:outputLinkEx
												onclick="return func_1(this, event);"
												value="#{shoppingCartHandler.newUTeEditionLink}"
												styleClass="outputLinkEx" id="linkExReadNowTextLink">
												<h:outputText id="textReadNowTextLink" styleClass="outputText"
													value="click here"></h:outputText>
											</hx:outputLinkEx>.</li>
										<li><h:outputText styleClass="" id="textThankYouBullet2a"
												value="#{labels.thankYouText2a}"></h:outputText>&nbsp; <hx:outputLinkEx
												value="http://service.usatoday.com/welcome.jsp"
												styleClass="outputLinkEx" id="linkExCustServiceLink">
												<h:outputText id="textCustServiceLink" styleClass="outputText"
													value="www.usatodayservice.com"></h:outputText>
											</hx:outputLinkEx>&nbsp;<h:outputText styleClass="" id="textThankYouBullet2b"
												value="#{labels.thankYouText2b}"></h:outputText>
										</li>
										<li><h:outputText id="textThankYouText3"
												value="#{labels.thankYouText3}"></h:outputText>
										</li>
										<li><h:outputText id="textThankYouText4"
												value="#{labels.thankYouText4}"></h:outputText>
										</li>
									</ul>
								</hx:jspPanel>
								<hx:jspPanel id="jspPanelThankYouTextPanel"
									rendered="#{not shoppingCartHandler.showEEWelcomePanel }">
									<ul>
										<li><h:outputText id="textThankYouTextL1"
												value="#{labels.ThankYouText5}"></h:outputText>
										</li>
									</ul>
								</hx:jspPanel>

								<hx:jspPanel id="jspPanelThankYouGiftPanel"
									rendered="#{shoppingCartHandler.showGiftPanel}">
									<ul>
										<li><h:outputText id="textThankYouGift"
												value="Tell them the good News!"></h:outputText>&nbsp;&nbsp; <hx:outputLinkEx
												value="/giftcard/index.faces" styleClass="outputLinkEx"
												id="linkExGiftLink">
												<h:outputText id="textGiftLink" styleClass="outputText"
													value="Send a Gift Card Now"></h:outputText>
											</hx:outputLinkEx>
										</li>
									</ul>
								</hx:jspPanel>

								<hx:graphicImageEx styleClass="graphicImageEx" id="imageExFillerImage"
									value="/theme/img/JSF_1x1.gif" width="1" hspace="1" border="0"
									style="position: static; padding-bottom: 10px; margin: 0px; left: auto"
									vspace="1"></hx:graphicImageEx>


								<h:panelGrid styleClass="panelGridThankYouPage" id="gridPrintButtonGrid"
									cellpadding="0" cellspacing="0" style="text-align: left">
									<hx:outputLinkEx value="#" onclick="printPage(this, event);"
										styleClass="outputLinkEx" id="linkExPrintPageLink"
										style="margin-left: 10px; font-size: 10pt">
										<h:outputText id="textPrintPageLinkText" styleClass="outputText"
											value="#{labels.printPageLabel}"></h:outputText>
									</hx:outputLinkEx>
								</h:panelGrid>

								<hx:jspPanel id="jspPanelOrderDataPanel">
									<div id="orderFormBoxThankYou">
										<h:panelGrid styleClass="panelGridThankYouPage"
											id="gridAccountInformation"
											columnClasses="panelGridColNorm,panelGridColRightAlign" columns="2"
											rendered="#{customerHandler.customerDataAvailable}">
											<f:facet name="header">
												<h:panelGroup styleClass="panelGroup"
													id="groupAccountInfoHeaderGroup" style="text-align: left">

													<hx:panelLayout styleClass="panelLayout" id="layoutOrderInfo1"
														width="95%" align="left">
														<f:facet name="body"></f:facet>
														<f:facet name="left">
															<h:outputText styleClass="panelThankYouPage_Header"
																id="textOrderInformationHeaderlabel"
																value="#{labels.orderInformationLabel}"></h:outputText>
														</f:facet>
														<f:facet name="right"></f:facet>
														<f:facet name="bottom"></f:facet>
														<f:facet name="top"></f:facet>
													</hx:panelLayout>
												</h:panelGroup>
											</f:facet>

											<h:outputText styleClass="outputText" id="textDateLabel"
												style="font-weight: bold" value="#{labels.DateLabel1}:"></h:outputText>

											<h:outputText styleClass="outputText" id="textDateOfToday"
												value="#{shoppingCartHandler.dateOrderPlaced}">
												<hx:convertDateTime />
											</h:outputText>

											<h:outputText styleClass="outputText" id="textSubStartDateLabel"
												style="font-weight: bold" value="#{labels.SubStartDate}:"></h:outputText>

											<h:outputText styleClass="outputText" id="textStartDateOfSub"
												value="#{shoppingCartHandler.startDateOfSub}">
												<hx:convertDateTime />
											</h:outputText>

											<h:outputText styleClass="outputText" id="textProductLabel"
												style="font-weight: bold"
												value="#{labels.SubscriptionProdNameLabel}:"></h:outputText>

											<h:outputText styleClass="outputText" id="textProdName"
												value="#{shoppingCartHandler.lastOrder.orderedItems[0].product.name}"></h:outputText>
											<h:outputText styleClass="outputText" id="textTermLabel"
												style="font-weight: bold" value="#{labels.termLabel2}:"></h:outputText>
											<h:outputText styleClass="outputText" id="textSelectedTerm"
												value="#{shoppingCartHandler.lastSubscriptionItemTermDuration}">

											</h:outputText>
											<h:outputText styleClass="outputText" id="textUnitPriceLabel"
												style="font-weight: bold" value="#{labels.UnitPrice}:"></h:outputText>
											<h:outputText styleClass="outputText" id="textUnitPrice"
												value="#{shoppingCartHandler.orderedSubscriptionItem.selectedTerm.price}">
												<hx:convertNumber type="currency" currencySymbol="$" />
											</h:outputText>
											<h:outputText styleClass="outputText" id="textEZPayLabel"
												value="EZ PAY:" style="font-weight: bold"></h:outputText>

											<h:outputText styleClass="outputText" id="textEZPayStatus"
												value="#{shoppingCartHandler.selectedEZPayString}">
											</h:outputText>
										</h:panelGrid>
										<h:panelGrid styleClass="panelGridThankYouPage"
											id="gridPremiumPromoGrid"
											rendered="#{shoppingCartHandler.showPremiumPromotionGrid}"
											columnClasses="panelGridColNorm,panelGridColRightAlign" columns="2">
											<h:outputText styleClass="outputText" id="textPremiumPromotionLabel"
												value="Promotion:" style="font-weight: bold"></h:outputText>
											<h:outputText styleClass="outputText"
												id="textPremiumPromotionDescriptionText"
												value="#{shoppingCartHandler.orderedSubscriptionItem.promotionalItems.descriptionText}">
											</h:outputText>

										</h:panelGrid>
										<!-- Delivery Information -->
										<h:panelGrid styleClass="panelGridThankYouPage"
											columnClasses="panelGridColNorm,panelGridColRightAlign"
											id="gridAccountInformationDelivery" columns="2"
											rendered="#{customerHandler.customerDataAvailable}">
											<f:facet name="header">
												<h:panelGroup styleClass="panelGroup" id="groupDeliveryInfoGroup">
													<hx:panelLayout styleClass="panelLayout" id="layoutOrderInfo2"
														width="95%" align="left">
														<f:facet name="body"></f:facet>
														<f:facet name="left">
															<h:outputText styleClass="panelThankYouPage_Header"
																id="textOrderInformationDeliveryHeaderlabel"
																value="#{labels.emailAddressRecipientLabel2}"></h:outputText>
														</f:facet>
														<f:facet name="right"></f:facet>
														<f:facet name="bottom"></f:facet>
														<f:facet name="top"></f:facet>
													</hx:panelLayout>
												</h:panelGroup>
											</f:facet>

											<h:outputText styleClass="outputText" id="textDeliveryMethodLabel"
												style="font-weight: bold" value="Delivery Method:"
												rendered="#{shoppingCartHandler.showDeliveryMethod}"></h:outputText>

											<h:outputText styleClass="outputText" id="textDeliveryMethodDes"
												rendered="#{shoppingCartHandler.showDeliveryMethod}"
												value="#{shoppingCartHandler.deliveryMethodDescription}">
											</h:outputText>

											<h:outputText styleClass="outputText" id="textDeliveryEmailLabel"
												style="font-weight: bold" value="#{labels.EmailAddressLabel}:"></h:outputText>

											<h:outputText styleClass="outputText" id="textDeliveryEmail"
												value="#{shoppingCartHandler.lastOrder.deliveryContact.emailAddress}">
											</h:outputText>

											<h:outputText styleClass="outputText" id="textDeliveryNameLabel"
												value="#{labels.NameLabel}:" style="font-weight: bold"></h:outputText>
											<h:outputText styleClass="outputText" id="textDeliveryName"
												value="#{shoppingCartHandler.lastOrder.deliveryContact.firstName} #{shoppingCartHandler.lastOrder.deliveryContact.lastName}">
											</h:outputText>

											<h:outputText styleClass="outputText"
												id="textDeliveryCompanyNameLabel"
												value="#{labels.companyNameLabel}:" style="font-weight: bold"
												rendered="#{!empty shoppingCartHandler.lastOrder.deliveryContact.firmName}"></h:outputText>
											<h:outputText styleClass="outputText" id="textDeliveryCompanyName"
												value="#{shoppingCartHandler.lastOrder.deliveryContact.firmName}"
												rendered="#{!empty shoppingCartHandler.lastOrder.deliveryContact.firmName}">
											</h:outputText>

											<h:outputText styleClass="outputText" id="textDeliveryAddr1Label"
												value="#{labels.AddressLabel}:" style="font-weight: bold"></h:outputText>
											<h:outputText styleClass="outputText" id="textDeliveryAddr1"
												value="#{shoppingCartHandler.lastOrder.deliveryContact.UIAddress.address1}">
											</h:outputText>
											<h:outputText styleClass="outputText" id="textDeliveryAptSuiteLabel"
												style="font-weight: bold"></h:outputText>
											<h:outputText styleClass="outputText" id="textDeliveryAptSuite"
												value="#{shoppingCartHandler.lastOrder.deliveryContact.UIAddress.aptSuite}">
											</h:outputText>
											<h:outputText styleClass="outputText" id="textDeliveryAddr2Label"
												style="font-weight: bold"
												rendered="#{!empty shoppingCartHandler.lastOrder.deliveryContact.UIAddress.address2}">
											</h:outputText>
											<h:outputText styleClass="outputText" id="textDeliveryAddr2"
												value="#{shoppingCartHandler.lastOrder.deliveryContact.UIAddress.address2}"
												rendered="#{!empty shoppingCartHandler.lastOrder.deliveryContact.UIAddress.address2}">
											</h:outputText>

											<h:outputText styleClass="outputText"
												id="textDeliveryAddrStateZipLabel" style="font-weight: bold"></h:outputText>
											<h:outputText styleClass="outputText" id="textDeliveryStateZip"
												value="#{shoppingCartHandler.lastOrder.deliveryContact.UIAddress.city}, #{shoppingCartHandler.lastOrder.deliveryContact.UIAddress.state} #{shoppingCartHandler.lastOrder.deliveryContact.UIAddress.zip}">
											</h:outputText>

											<h:outputText styleClass="outputText" id="textDeliveryPhoneLabel"
												style="font-weight: bold" value="#{labels.phoneLabel}:"></h:outputText>
											<h:outputText styleClass="outputText" id="textDeliveryPhone"
												value="#{shoppingCartHandler.lastOrder.deliveryContact.homePhone}">
												<hx:convertMask mask="###-###-####" />
											</h:outputText>
											<h:outputText styleClass="outputText" id="textDeliveryWorkPhoneLabel"
												style="font-weight: bold" value="#{labels.workPhoneLabel}:"
												rendered="#{!empty shoppingCartHandler.lastOrder.deliveryContact.businessPhone}"></h:outputText>
											<h:outputText styleClass="outputText" id="textDeliveryWorkPhone"
												value="#{shoppingCartHandler.lastOrder.deliveryContact.businessPhone}"
												rendered="#{!empty shoppingCartHandler.lastOrder.deliveryContact.businessPhone}">
												<hx:convertMask mask="###-###-####" />
											</h:outputText>

										</h:panelGrid>

										<!-- Payment Information -->
										<h:panelGrid styleClass="panelGridThankYouPage"
											columnClasses="panelGridColNorm,panelGridColRightAlign"
											id="gridAccountInformationPayment" columns="2"
											rendered="#{shoppingCartHandler.paidByCreditCard}">
											<f:facet name="header">
												<h:panelGroup styleClass="panelGroup" id="groupPaymentInfoGroup">
													<hx:panelLayout styleClass="panelLayout" id="layoutOrderInfo3"
														width="95%" align="left">
														<f:facet name="body"></f:facet>
														<f:facet name="left">
															<h:outputText styleClass="panelThankYouPage_Header"
																id="textOrderInformationPaymentHeaderlabel"
																value="#{labels.paymentInfo2}"></h:outputText>
														</f:facet>
														<f:facet name="right"></f:facet>
														<f:facet name="bottom"></f:facet>
														<f:facet name="top"></f:facet>
													</hx:panelLayout>
												</h:panelGroup>
											</f:facet>
											<h:outputText styleClass="outputText" id="textSubTotalAmountLabel"
												style="font-weight: bold" value="#{labels.OrderSubTotal}:"></h:outputText>
											<h:outputText styleClass="outputText" id="textSubTotalChargeAmount"
												value="#{shoppingCartHandler.lastOrder.subTotal}">
												<hx:convertNumber type="currency" currencySymbol="$" />
											</h:outputText>
											<h:outputText styleClass="outputText" id="textTaxAmountLabel"
												style="font-weight: bold" value="#{labels.OrderTaxes}:"></h:outputText>
											<h:outputText styleClass="outputText" id="textChargeTaxAmount"
												value="#{shoppingCartHandler.lastOrder.taxAmount}">
												<hx:convertNumber type="currency" currencySymbol="$" />
											</h:outputText>
											<h:outputText styleClass="outputText" id="textChargeAmountLabel"
												style="font-weight: bold" value="#{labels.OrderTotal}:"></h:outputText>
											<h:outputText styleClass="outputText" id="textChargeAmount"
												value="#{shoppingCartHandler.lastOrder.total}">
												<hx:convertNumber type="currency" currencySymbol="$" />
											</h:outputText>
											<h:outputText styleClass="outputText" id="textPaymentMethodLabel"
												value="#{labels.creditCardTypeLabel}:" style="font-weight: bold"></h:outputText>
											<h:outputText styleClass="outputText" id="textPaymentMethod"
												value="#{shoppingCartHandler.cart.paymentMethod.label}">
											</h:outputText>
											<h:outputText styleClass="outputText" id="textPaymentCardNumberLabel"
												value="#{labels.Number}:" style="font-weight: bold"></h:outputText>
											<h:outputText styleClass="outputText" id="textPaymentCardNumber"
												value="#{shoppingCartHandler.cart.paymentMethod.displayValue}">
											</h:outputText>

										</h:panelGrid>
										<h:panelGrid styleClass="panelGridThankYouPage"
											id="gridAccountInformationPaymentBillMe" columns="1"
											rendered="#{not shoppingCartHandler.paidByCreditCard }">
											<f:facet name="header">
												<h:panelGroup styleClass="panelGroup"
													id="groupPaymentInfoBillMeGroup">
													<hx:panelLayout styleClass="panelLayout" id="layoutOrderInfo4"
														width="95%" align="left">
														<f:facet name="body"></f:facet>
														<f:facet name="left">
															<h:outputText styleClass="panelThankYouPage_Header"
																id="textOrderInformationPaymentBillMeHeaderlabel"
																value="#{labels.paymentInfo2}"></h:outputText>
														</f:facet>
														<f:facet name="right"></f:facet>
														<f:facet name="bottom"></f:facet>
														<f:facet name="top"></f:facet>
													</hx:panelLayout>
												</h:panelGroup>
											</f:facet>
											<h:panelGrid id="billMeOrderDetails"
												styleClass="panelGridThankYouPage"
												columnClasses="panelGridColNorm,panelGridColRightAlign" columns="2">
												<h:outputText styleClass="outputText"
													id="textSubTotalAmountLabelBillMe" style="font-weight: bold"
													value="#{labels.OrderSubTotal}:"></h:outputText>
												<h:outputText styleClass="outputText"
													id="textSubTotalChargeAmountBillMe"
													value="#{shoppingCartHandler.lastOrder.subTotal}">
													<hx:convertNumber type="currency" currencySymbol="$" />
												</h:outputText>
												<h:outputText styleClass="outputText" id="textTaxAmountLabelBillMe"
													style="font-weight: bold" value="#{labels.OrderTaxes}:"></h:outputText>
												<h:outputText styleClass="outputText" id="textChargeTaxAmountBillMe"
													value="#{shoppingCartHandler.lastOrder.taxAmount}">
													<hx:convertNumber type="currency" currencySymbol="$" />
												</h:outputText>
												<h:outputText styleClass="outputText"
													id="textChargeAmountLabelBillMe" style="font-weight: bold"
													value="#{labels.OrderTotal}:"></h:outputText>
												<h:outputText styleClass="outputText" id="textChargeAmountBillMe"
													value="#{shoppingCartHandler.lastOrder.total}">
													<hx:convertNumber type="currency" currencySymbol="$" />
												</h:outputText>
											</h:panelGrid>
											<h:outputText styleClass="outputText" id="textBeInfoText"
												value="#{labels.BilledDescription}"></h:outputText>

										</h:panelGrid>
										<!-- billing information -->
										<h:panelGrid styleClass="panelGridThankYouPage"
											columnClasses="panelGridColNorm,panelGridColRightAlign"
											id="gridAccountInformationBilling" columns="2"
											rendered="#{not shoppingCartHandler.lastOrder.billingSameAsDelivery }">
											<f:facet name="header">
												<h:panelGroup styleClass="panelGroup" id="groupBillingHeaderGroup">
													<hx:panelLayout styleClass="panelLayout" id="layoutOrderInfo5"
														width="95%" align="left">
														<f:facet name="body"></f:facet>
														<f:facet name="left">
															<h:outputText styleClass="panelThankYouPage_Header"
																id="textOrderInformationBillingHeaderlabel"
																value="#{labels.BillingAddressLabel}"></h:outputText>
														</f:facet>
														<f:facet name="right"></f:facet>
														<f:facet name="bottom"></f:facet>
														<f:facet name="top"></f:facet>
													</hx:panelLayout>

												</h:panelGroup>
											</f:facet>
											<h:outputText styleClass="outputText" id="textBillingEmailLabel"
												style="font-weight: bold" value="#{labels.EmailAddressLabel}:"></h:outputText>

											<h:outputText styleClass="outputText" id="textBillingEmail"
												value="#{shoppingCartHandler.lastOrder.billingContact.emailAddress}">
											</h:outputText>

											<h:outputText styleClass="outputText" id="textBillingNameLabel"
												value="#{labels.NameLabel}:" style="font-weight: bold"></h:outputText>
											<h:outputText styleClass="outputText" id="textBillingName"
												value="#{shoppingCartHandler.lastOrder.billingContact.firstName} #{shoppingCartHandler.lastOrder.billingContact.lastName}">
											</h:outputText>

											<h:outputText styleClass="outputText" id="textBillingCompanyLabel"
												rendered="#{!empty shoppingCartHandler.lastOrder.billingContact.firmName}"
												value="#{labels.companyNameLabel}:" style="font-weight: bold"></h:outputText>
											<h:outputText styleClass="outputText" id="textBillingCompany"
												rendered="#{!empty shoppingCartHandler.lastOrder.billingContact.firmName}"
												value="#{shoppingCartHandler.lastOrder.billingContact.firmName}">
											</h:outputText>

											<h:outputText styleClass="outputText" id="textBillingAddr1Label"
												value="#{labels.AddressLabel}:" style="font-weight: bold"></h:outputText>
											<h:outputText styleClass="outputText" id="textBillingAddr1"
												value="#{shoppingCartHandler.lastOrder.billingContact.UIAddress.address1}">
											</h:outputText>
											<h:outputText styleClass="outputText" id="textBillingAptSuiteLabel"
												style="font-weight: bold"></h:outputText>
											<h:outputText styleClass="outputText" id="textBillingAptSuite"
												value="#{shoppingCartHandler.lastOrder.billingContact.UIAddress.aptSuite}">
											</h:outputText>
											<h:outputText styleClass="outputText" id="textBillingAddr2Label"
												style="font-weight: bold"
												rendered="#{!empty shoppingCartHandler.lastOrder.billingContact.UIAddress.address2}"></h:outputText>
											<h:outputText styleClass="outputText" id="textBillingAddr2"
												value="#{shoppingCartHandler.lastOrder.billingContact.UIAddress.address2}"
												rendered="#{!empty shoppingCartHandler.lastOrder.billingContact.UIAddress.address2}">
											</h:outputText>

											<h:outputText styleClass="outputText"
												id="textBillingAddrStateZipLabel" style="font-weight: bold"></h:outputText>
											<h:outputText styleClass="outputText" id="textBillingStateZip"
												value="#{shoppingCartHandler.lastOrder.billingContact.UIAddress.city}, #{shoppingCartHandler.lastOrder.billingContact.UIAddress.state} #{shoppingCartHandler.lastOrder.billingContact.UIAddress.zip}">
											</h:outputText>

											<h:outputText styleClass="outputText" id="textBillingPhoneLabel"
												style="font-weight: bold" value="#{labels.billPhoneLabel}:"
												rendered="#{!empty shoppingCartHandler.lastOrder.billingContact.homePhone}"></h:outputText>
											<h:outputText styleClass="outputText" id="textBillingPhone"
												value="#{shoppingCartHandler.lastOrder.billingContact.homePhone}"
												rendered="#{!empty shoppingCartHandler.lastOrder.billingContact.homePhone}">
												<hx:convertMask mask="###-###-####" />
											</h:outputText>
										</h:panelGrid>
										<h:panelGrid styleClass="panelGridThankYouPage"
											id="gridAccountEZPayInformation" columns="1"
											rendered="#{shoppingCartHandler.orderedSubscriptionItem.choosingEZPay}">
											<h:outputText styleClass="outputText" id="textEZPayInfoText"
												value="#{labels.EZPayThankYouNote} #{currentOfferHandler.EZPayDisclaimer}">
											</h:outputText>

											<%-- 								<h:outputText styleClass="outputText fineprint" --%>
											<%-- 									id="textEZPayInfoText" value="#{labels.EZPayThankYouInfo}"></h:outputText> --%>

										</h:panelGrid>

									</div>
								</hx:jspPanel>
							</h:panelGrid>
						</div>
						<div id="pageContent30" style="border-width: 0px; border-style: none">
							<h:panelGrid styleClass="panelGrid" id="gridRightPanelGrid"
								cellpadding="0" cellspacing="0" columns="1">
								<hx:jspPanel id="jspPanelRightPanelTop"
									rendered="#{shoppingCartHandler.showElectronicLinks}">
									<table border="0" cellpadding="0" cellspacing="0" width="270"
										class="tblEEditionReaderBorder">

										<tr>
											<td></td>
											<td><img src="/images/eEdition/spacer.gif" width="1" height="1"
												border="0" alt="" />
											</td>
											<td></td>
											<td><img src="/images/eEdition/spacer.gif" width="1" height="1"
												border="0" alt="" />
											</td>
										</tr>
										<tr>
											<td colspan="3"><img name="computer_art_2_button_v2_r1_c1"
												src="/images/eEdition/eEditionSummary229x41.jpg" border="0"
												id="computer_art_2_button_v2_r1_c1" alt=""
												style="margin-bottom: 10px; margin-top: 10px" /></td>
											<td><img src="/images/eEdition/spacer.gif" width="1" height="50"
												border="0" alt="" />
											</td>
										</tr>
										<tr>
											<td colspan="3" align="center">
												<div id="pageContentNoMarginsButton">
													<hx:outputLinkEx styleClass="outputLinkEx" id="linkExReadNowLink"
														value="#{shoppingCartHandler.newDefaultUTeEditionLink}"
														onclick="return func_1(this, event);">
														<hx:graphicImageEx styleClass="graphicImageEx"
															id="imageExReadNowButtonImage"
															value="/images/eEdition/computer_art_2_button_v2_r2_c1_116w.gif"
															width="116" height="40" hspace="0" vspace="0" border="0"
															align="center"></hx:graphicImageEx>
													</hx:outputLinkEx>
												</div>
												<table style="width: 100%; margin-top: -5px;">
													<tr>
														<td align="center">
															<table>
																<tr>
																	<td><hx:outputLinkEx styleClass="outputLinkEx"
																			id="linkExOpenDesktopVersion"
																			value="#{shoppingCartHandler.newUTeEditionLink}"
																			onclick="return func_1(this, event);">
																			<h:outputText id="text1111" value="desktop"
																				styleClass="outputText"></h:outputText>
																		</hx:outputLinkEx></td>
																	<td><h:outputText styleClass="outputText" id="text2"
																			value="|"></h:outputText></td>
																	<td><hx:outputLinkEx styleClass="outputLinkEx"
																			id="linkExOpenMobileVersion"
																			value="#{shoppingCartHandler.alternateUTeEditionLink}"
																			onclick="return func_1(this, event);">
																			<h:outputText id="text1" styleClass="outputText"
																				value="mobile"></h:outputText>
																		</hx:outputLinkEx></td>
																</tr>
															</table></td>
													</tr>
												</table></td>
											<td><img src="/images/eEdition/spacer.gif" width="1" height="1"
												border="0" alt="" />
											</td>
										</tr>

										<tr>
											<td></td>
											<td></td>
											<td></td>
											<td><img src="/images/eEdition/spacer.gif" width="1" height="10"
												border="0" alt="" />
											</td>
										</tr>
									</table>

								</hx:jspPanel>
								<br />
								<h:panelGrid columns="1" id="rightColumnCustomPanelGrid">
									<h:panelGrid styleClass="panelGrid" id="gridRightColCustomSpot3"
										columns="1"
										rendered="#{currentOfferHandler.renderOnePaqeThankYouRightColumnImageSpot2}">
										<hx:outputLinkEx styleClass="outputLinkEx"
											id="linkExRightColImageSpot2Link" style="border-width: 0px"
											value="#{currentOfferHandler.onePageThankYouRightColumnImageSpot2ImageOnClickURL}"
											rendered="#{currentOfferHandler.renderOnePageThankYouRightColmnImageSpot2ImageAsClickable}">
											<hx:graphicImageEx styleClass="" id="imageExRightColImage2"
												value="#{currentOfferHandler.onePageThankYouRightColumnImageSpot2ImagePath}"
												border="0"
												alt="#{currentOfferHandler.onePageThankYouRightColumnImageSpot2ImageAltText}"
												title="#{currentOfferHandler.onePageThankYouRightColumnImageSpot2ImageAltText}"
												style="padding-left: 0px">
											</hx:graphicImageEx>
										</hx:outputLinkEx>
										<hx:graphicImageEx styleClass="" id="imageExRightColImage2NoLink"
											value="#{currentOfferHandler.onePageThankYouRightColumnImageSpot2ImagePath}"
											border="0"
											alt="#{currentOfferHandler.onePageThankYouRightColumnImageSpot2ImageAltText}"
											title="#{currentOfferHandler.onePageThankYouRightColumnImageSpot2ImageAltText}"
											style="padding-left: 0px"
											rendered="#{not currentOfferHandler.renderOnePageThankYouRightColmnImageSpot2ImageAsClickable}">
										</hx:graphicImageEx>
									</h:panelGrid>
									<br />
									<h:panelGrid styleClass="panelGrid" id="gridRightColCustomSpot4"
										columns="1">
										<h:outputText styleClass="" id="RightColHTMLSpot2Text"
											value="#{currentOfferHandler.onePageThankYouRightColHTMLSpot2}"
											escape="false"></h:outputText>
									</h:panelGrid>
									<h:panelGrid styleClass="panelGrid" id="gridRightColCustomSpot1"
										columns="1"
										rendered="#{currentOfferHandler.renderOnePaqeThankYouRightColumnImageSpot1}">
										<hx:outputLinkEx styleClass="outputLinkEx"
											id="linkExRightColImageSpot1Link" style="border-width: 0px"
											value="#{currentOfferHandler.onePageThankYouRightColumnImageSpot1ImageOnClickURL}"
											rendered="#{currentOfferHandler.renderOnePageThankYouRightColmnImageSpot1ImageAsClickable}">
											<hx:graphicImageEx styleClass="" id="imageExRightColImage1"
												value="#{currentOfferHandler.onePageThankYouRightColumnImageSpot1ImagePath}"
												border="0"
												alt="#{currentOfferHandler.onePageThankYouRightColumnImageSpot1ImageAltText}"
												title="#{currentOfferHandler.onePageThankYouRightColumnImageSpot1ImageAltText}"
												style="padding-left: 0px">
											</hx:graphicImageEx>
										</hx:outputLinkEx>
										<hx:graphicImageEx styleClass="" id="imageExRightColImage1NoLink"
											value="#{currentOfferHandler.onePageThankYouRightColumnImageSpot1ImagePath}"
											border="0"
											alt="#{currentOfferHandler.onePageThankYouRightColumnImageSpot1ImageAltText}"
											title="#{currentOfferHandler.onePageThankYouRightColumnImageSpot1ImageAltText}"
											style="padding-left: 0px"
											rendered="#{not currentOfferHandler.renderOnePageThankYouRightColmnImageSpot1ImageAsClickable}">
										</hx:graphicImageEx>
									</h:panelGrid>
									<br />
									<h:panelGrid styleClass="panelGrid" id="gridRightColCustomSpot2"
										columns="1">
										<h:outputText styleClass="" id="RightColHTMLSpot1Text"
											value="#{currentOfferHandler.onePageThankYouRightColHTMLSpot1}"
											escape="false"></h:outputText>
									</h:panelGrid>
									<h:panelGrid styleClass="panelGrid" id="gridRightColEZPAY" columns="1"
										rendered="#{shoppingCartHandler.orderedSubscriptionItem.choosingEZPay}">
										<hx:graphicImageEx styleClass="" id="imageExRightColImageEZPay"
											value="/images/marketingimages/shim.gif" border="0" alt="" title=""
											style="padding-left: 0px">
										</hx:graphicImageEx>
									</h:panelGrid>

								</h:panelGrid>

							</h:panelGrid>
							<div style="float: none">
								<f:verbatim>
									<h:outputText styleClass="outputText" id="textThankYouCommJunc1"
										escape="false"
										value="#{shoppingCartHandler.commissionJunctionTrackingPixel}"></h:outputText>
									<h:outputText styleClass="outputText" id="textThankYouJS1"
										escape="false"
										value="#{shoppingCartHandler.onePageThankYouJavaScript1Text}"></h:outputText>
									<h:outputText styleClass="outputText" id="textThankYouJS2"
										escape="false"
										value="#{shoppingCartHandler.onePageThankYouJavaScript2Text}"></h:outputText>
									<h:outputText styleClass="outputText" id="textThankYouJS3"
										escape="false"
										value="#{shoppingCartHandler.onePageThankYouJavaScript3Text}"></h:outputText>
									<h:outputText styleClass="outputText" id="textThankYouJS4"
										escape="false"
										value="#{shoppingCartHandler.onePageThankYouJavaScript4Text}"></h:outputText>
									<h:outputText styleClass="outputText" id="textThankYouJS5"
										escape="false"
										value="#{shoppingCartHandler.onePageThankYouJavaScript5Text}"></h:outputText>
									<h:outputText styleClass="outputText" id="textThankYouJS6"
										escape="false"
										value="#{shoppingCartHandler.onePageThankYouJavaScript6Text}"></h:outputText>
									<h:outputText styleClass="outputText" id="textThankYouJS7"
										escape="false"
										value="#{shoppingCartHandler.onePageThankYouJavaScript7Text}"></h:outputText>
									<h:outputText styleClass="outputText" id="textThankYouJS8"
										escape="false"
										value="#{shoppingCartHandler.onePageThankYouJavaScript8Text}"></h:outputText>
									<h:outputText styleClass="outputText" id="textThankYouJS9"
										escape="false"
										value="#{shoppingCartHandler.onePageThankYouJavaScript9Text}"></h:outputText>
									<h:outputText styleClass="outputText" id="textThankYouJS12"
										escape="false"
										value="#{shoppingCartHandler.onePageThankYouJavaScript12Text}"></h:outputText>
								</f:verbatim>
							</div>
						</div>
					</h:form>
				</hx:scriptCollector>
			<%-- /tpl:put --%>
</DIV>
<%-- /tpl:put --%>
				<DIV id="clear"></DIV>
				<DIV id="bluebar">
					<a href="/feedback/feedback.jsp"
						style="color: white; font-weight: bold; text-decoration: none; font-size: 14px"
						title="Contact Us">Contact Us</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;1-800-872-0001&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a
						href="/faq/utfaq.jsp"
						style="color: white; font-weight: bold; text-decoration: none; font-size: 14px"
						title="Frequently Asked Questions">FAQ</a>
				</DIV>
				<%-- tpl:put name="footerMainArea" --%>
	<%-- tpl:put name="footerArea_1" --%>
				<!-- 						<script type="text/javascript">
							try {
								var linkShare = '<img src="https://track.linksynergy.com/ep?mid=36825&ord='
										+ PIXEL_OID
										+ '&skulist='
										+ PIXEL_PRODUCT
										+ '&qlist='
										+ PIXEL_QTY
										+ '&amtlist='
										+ PIXEL_ORDER_SUBTOTAL_NOPUNC
										+ '&cur=USD">';
								document.write(linkShare);
							} catch (e) {

							}

							try {
								// FetchBack Remarketing frame
								var fetchBack = '<iframe src="https://pixel.fetchback.com/serve/fb/pdj?cat=&name=success&sid=4683&oid='
										+ PIXEL_OID
										+ '&crv='
										+ PIXEL_ORDER_SUBTOTAL
										+ '" scrolling="no" width="1" height="1" marginheight="0" marginwidth="0" frameborder="0">_$tag____';
								document.write(fetchBack);

								// End FetchBack Remarketing frame
							} catch (expe) {

							}
						</script>
 -->
			<%-- /tpl:put --%>
	<%=trackingBug.getOmnitureHTML() %>
<%-- /tpl:put --%>
				<DIV id=graybar>
					Copyright
					<script>
						document.write(getCurrentYear());
					</script>
					USA TODAY, a division of <a href="http://www.gannett.com">Gannett
						Co. Inc.</a> <a href="/privacy/privacy.htm">Privacy
						Notice/California Privacy Notice</a>. <a
						href="/privacy/privacy.htm#adchoice">Ad Choices</a>.<br> By
					using this service, you accept our <a href="/service/service.jsp">Terms
						of Service</a>.
				</DIV>
				<DIV id=copyright>
					<P>Offers available to new subscribers only. Home delivery not
						available in some areas. Not valid with any other offer.
						Prepayment required. After the promotional period ends, the
						subscription will continue and be billed monthly at the then
						regular rate, less any applicable credits, unless USA TODAY is
						otherwise notified. Applicable taxes may not be included in the
						rate. If at any time you decide to cancel your subscription, you
						may contact customer service at 1-800-872-0001 and the full amount
						of any balance over $2.00 will be returned.</P>
				</DIV>
				<!--END OF COPYWRITE-->
				<DIV id=footer></DIV>
			</DIV>
			<!--END OF WHITEBOX-->
		</DIV>
		<!--END OF CONTENT-->
	</DIV>
</BODY>
</HTML><%-- /tpl:insert --%>
</f:view><%-- /tpl:insert --%>