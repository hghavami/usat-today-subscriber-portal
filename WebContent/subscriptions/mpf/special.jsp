<%
	String msg = "";

////  CHANGE FOLLOWING TO TRUE IF MPFs EXIST   ///////////////////////
////  MPF NO LONGER SUPPORTED WITH MOVE TO GENESYS  
boolean mpfActive = false;

if (!mpfActive) {
	try {
		// if no MPF's redirect to welcome page
		com.usatoday.esub.common.UTCommon.showUrl(response, "/welcome.jsp");
	}catch (Exception e) {
		// ignore
	}
	return;
}
else {
        msg = (String) session.getAttribute("MSG");
        if (msg != null) {
            session.removeAttribute("MSG");
        } else {
            msg = "";
        }

}

%>
<%-- tpl:insert page="/theme/subscriptionPortalJSPPage.jtpl" --%><!DOCTYPE HTML><%@page import="com.usatoday.businessObjects.products.ProductBO"%>
<%@page import="com.usatoday.business.interfaces.products.ProductIntf"%>
<%@page import="com.usatoday.util.MobileDeviceDetectionUtil"%>
<%-- tpl:insert page="/theme/subscriptionPortalRoot.htpl" --%><HTML>
<HEAD>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8">
<%-- tpl:put name="headerareaTop" --%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%
	String docProtocol = "https:";
	if (!request.isSecure()) { 
		docProtocol = "http:";
	} 
	// following work around to get around problem with RAD 8.0.2 automated links in templates - APE
	String subcribeJS = "javascript:_subscribeToPub();"; 
	String subcribeIntJS = "javascript:_subscribeToInternationalEdition();"; 
	String backIssueJS = "javascript:_shopUSAT();";
	 
	String navigationSubscribeLinkValue = "/subscriptions/index.jsp";
	String navigationGiveGiftLinkValue = "/subscriptions/order/checkout.faces";
	String electronicEditionLink = "/subscriptions/order/checkout.faces?pub=EE";
	String internationalEditionLink = "/international/welcomeint.jsp";
	String FAQLink = "/faq/utfaq.jsp";
	String subscribeByMail = "/subscriptions/subscribebymail.html";
	String dynamicNavigation = "";
	
	String closingHREF = "";
	String printEditionCSClass = "showMe";
	String eEditionCSClass = "hideMe";
	String ourPledgeCSClass = "showMe";
	
	// Following 4 attributes are only modified in the sub-template for Order Entry
	String navigationPromoImg = "/images/shim.gif";
	String navigationPromoImgAltText = "";
	String navigationPromoImgHREF = "";
	String navClosingHref = "";
	String navigationPromoFlash = "";
	String navigationPromoImgClassName = "promoImages";
	
	String navPubImg = "/theme/images/nav_top.jpg";
	String initFunction = "";  // pages using this template can change this value to be the name of a javascript function that will
							   // be called when the page loads.
	String unloadFunction = "";
	String onBeforeUnloadFunction = "";

	boolean isSignedIn = false;
	String pubCode = "";
	String productName = "USA TODAY";
    boolean isEEditionProduct = false;
   	com.usatoday.businessObjects.products.promotions.PromotionSet currentAccountPromotionSet = null;
	
	com.usatoday.business.interfaces.customer.CustomerIntf customer = null;
	// check if logged in and/or log in from cookie
	try {

		customer = (com.usatoday.business.interfaces.customer.CustomerIntf)session.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_CUSTOMER_INFO);

		if (customer != null) {
			isSignedIn = true;
			com.usatoday.business.interfaces.products.SubscriptionProductIntf product = com.usatoday.businessObjects.products.SubscriptionProductBO.getSubscriptionProduct(customer.getCurrentEmailRecord().getPubCode());
			isEEditionProduct = product.isElectronicDelivery(); 
			if (isEEditionProduct) {
				// hide the Buttons for vaca holds and complaints
				printEditionCSClass = "hideMe";
				// This promotion set is based on pub and keycode on the customer account
				currentAccountPromotionSet = com.usatoday.businessObjects.products.promotions.PromotionManager.getInstance().getPromotionsForOffer(customer.getCurrentAccount().getPubCode(), customer.getCurrentAccount().getKeyCode());
			}
			
		}
	}
	catch (Exception e){ 
		;
	}
		
	// Promotional Stuff
	com.usatoday.businessObjects.products.promotions.PromotionSet defaultPromotionSet = null;
	com.usatoday.businessObjects.products.promotions.PromotionSet currentOfferPromotionSet = null;
	
	// Omniture tracking
	com.usatoday.esub.common.WebBug trackingBug = com.usatoday.esub.common.UTCommon.getWebAnalyticsBug();
	try {
		
		com.usatoday.business.interfaces.products.OfferIntf offer = com.usatoday.esub.common.UTCommon.getCurrentOffer(request);
		
		pubCode = com.usatoday.util.constants.UsaTodayConstants.UT_PUBCODE;
		if (offer != null) {
			pubCode = offer.getPubCode();
			try {
				String brandingPubCode = offer.getProduct().getBrandingPubCode();
				ProductIntf bp = ProductBO.fetchProduct(brandingPubCode);
				productName = bp.getName();
			}
			catch (Exception teee) {
			}				
		}
		
		
		if (pubCode.equalsIgnoreCase(com.usatoday.util.constants.UsaTodayConstants.SW_PUBCODE)) {
				navigationSubscribeLinkValue = "/subscriptions/order/checkout.faces";
				navPubImg = "/theme/images/nav_top_sw2.jpg";
				electronicEditionLink = "http://www.usatoday.com/marketing/brand_mkt/splash/spw_electronic_edition/index.html";
				internationalEditionLink = "/international/welcomeswint.jsp";
				FAQLink = "/faq/bwfaq.jsp";
				subscribeByMail = "/subscriptions/subscribebymail_sw.html";
		}

		// get the default promotion set for the publication		
		defaultPromotionSet = com.usatoday.esub.common.UTCommon.getDefaultPromotionSetForPub(pubCode);
		
		// get the keycode specific promotion set
		
		if (offer != null && offer instanceof com.usatoday.business.interfaces.products.SubscriptionOfferIntf) {
			currentOfferPromotionSet = ((com.usatoday.business.interfaces.products.SubscriptionOfferIntf)offer).getPromotionSet();
		}

		if (currentOfferPromotionSet != null) {
			com.usatoday.business.interfaces.products.promotions.HTMLPromotionIntf dPromo = currentOfferPromotionSet.getDynamicNavigationOrderEntryHTML();
			if (dPromo != null) {
				dynamicNavigation = dPromo.getPromotionalHTML();
			}
		}
		
		com.usatoday.business.interfaces.products.promotions.ImagePromotionIntf imagePromo = null;
		

		// Check for EE cancels allowed flag and keycode		
		if (com.usatoday.util.constants.UsaTodayConstants.EE_CANCELS_ALLOWED_PROP && isEEditionProduct && currentAccountPromotionSet != null) {						// Is it enabled in the properties file
			eEditionCSClass = "showMe";		
			if (currentAccountPromotionSet.getEECancelsAllowed() != null) {
				com.usatoday.util.constants.UsaTodayConstants.EE_CANCELS_ALLOWED = false;				// Flag to be used through our the app
				eEditionCSClass = "HideMe";
			}
		}
 
 
		if (!pubCode.equals(com.usatoday.util.constants.UsaTodayConstants.UT_PUBCODE)) {
			ourPledgeCSClass = "HideMe";
		}

		// set up navigation pane promotional graphic
		imagePromo = null;
		
		if (currentOfferPromotionSet != null && currentOfferPromotionSet.getTemplateNavigationPromoImage() != null) {
			imagePromo = currentOfferPromotionSet.getTemplateNavigationPromoImage();
		}
		else {
			imagePromo = defaultPromotionSet.getTemplateNavigationPromoImage();
		}
		
		if (imagePromo != null) {
			navigationPromoImg = imagePromo.getImagePathString();

			if (navigationPromoImg.endsWith(".swf")) {
				navigationPromoImgClassName = "hideMe";
				navigationPromoFlash = "<object classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" codebase=\"https://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0\" height=\"90\" width=\"150\"> " +
						"<param name=\"movie\" value=\"" + navigationPromoImg + "\" /> " +
						"<param name=\"quality\" value=\"best\" />" +
						"<param name=\"play\" value=\"true\" /> " +
						"<embed height=\"90\" pluginspage=\"https://www.macromedia.com/go/getflashplayer\" src=\"" + navigationPromoImg + "\" type=\"application/x-shockwave-flash\" width=\"150\" quality=\"best\" play=\"true\"></embed> " +
					    "</object>";
				navigationPromoImg = "/images/shim.gif";
			}			
			else {
				navigationPromoImgAltText = (imagePromo.getImageAltText() == null) ? "" : "alt=\"" + imagePromo.getImageAltText() + "\"";
				navigationPromoImgHREF = (imagePromo.getImageLinkToURL() == null || imagePromo.getImageLinkToURL().length()==0) ? "" : "<a href=\"" + imagePromo.getImageLinkToURL() + "\">";
				if (navigationPromoImgHREF.length() > 0) {
					navClosingHref = "</a>";
				}
			}
		}

		// figure out tracking
		trackingBug.deriveValuesFromRequest(request);
	} 
 	catch (Exception e) {	
		System.out.println("Exception building Page Template Inside Pages: " + e.getMessage());
		request.getRequestDispatcher("/err/err.html").forward(request, response);
		return;
	}    
	
	String mobileCSS = "";
	if (MobileDeviceDetectionUtil.isSmallScreenDevice(request)) {
		mobileCSS = "<LINK href=\"/theme/themeV3/css/smallScreen.css\" rel=\"stylesheet\" type=\"text/css\">";
	}
			
%>
<script src="/common/prototype/prototype.js"></script>
<script src="/common/prototype/scriptaculous/scriptaculous.js" type="text/javascript"></script>
	<%-- tpl:put name="headerarea_Top" --%><%-- /tpl:put --%>
	<%-- /tpl:put --%>
<LINK REL="SHORTCUT ICON" HREF="/favicon.ico">
<LINK REL="icon" href="/favicon.ico" TYPE="image/ico" />
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META http-equiv="Content-Style-Type" content="text/css">
<meta name="viewport" content="initial-scale=1.0">
<LINK href="/theme/themeV3/css/usatodayBase.css" rel="stylesheet"
	type="text/css">
<LINK href="/theme/themeV3/css/usatodayAddOn.css" rel="stylesheet"
	type="text/css">
<script src="/common/usatCommon.js"></script>
<%-- tpl:put name="headareaBottom" --%>
<%=mobileCSS %>
		<%-- tpl:put name="headarea_Bottom" --%>
<title>Special USA TODAY Deals</title>
<script type="text/javascript">
function func_1(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	if (thisObj.checked == true) {
		document.mpfForm.mpfSendToGiftPayer.disabled=false;
	}
	else {
		document.mpfForm.mpfSendToGiftPayer.disabled=true;
		document.mpfForm.mpfSendToGiftPayer.checked=false;
	}
}

function validateForm() {
 
 return (true);

}

function onLoadFunc() {
	if (document.mpfForm.mpfIsGift.checked == true) {
		document.mpfForm.mpfSendToGiftPayer.disabled=false;
	}
}

</script>
<%-- /tpl:put --%>
<SCRIPT>			
	var currentPub = "<%=pubCode %>";
	
	function initPage(thisObj, thisEvent) {
		try {
			document.getElementById('navPromoImage').src = "<%=navigationPromoImg%>";	
			document.getElementById('navPromoImage').className = "<%=navigationPromoImgClassName%>";
		}
		catch (err){}
		
		// for product messageing in popUpOverlay.js
		usat_productName = "<%=productName %>";
	
		<%=initFunction %>
	}	
			
	function unloadPage(thisObj, thisEvent) {		
		<%=unloadFunction %>
	}
				
	function onBeforeUnloadPage(thisObj, thisEvent) {		 
		<%=onBeforeUnloadFunction %>
	}
	
	function subscribeToPub() {
		document.location = "<%=navigationSubscribeLinkValue%>";
	}	
	
	function giveAGift() {
		document.location = "<%=navigationGiveGiftLinkValue%>";
	}
	
	function subscribeToElectronicEdition() {
		document.location = "<%=electronicEditionLink %>";
	}
	
	function subscribeToElectronicEditionOrderPage() {
		document.location = "<%=electronicEditionLink %>";
	}
	
	function subscribeToInternationalEdition() {
		document.location = "<%=internationalEditionLink%>";
	}

	function loadFAQ() {
		window.open('<%=FAQLink%>','FAQ', 'width=825,height=800,menubar=no,titlebar=yes,scrollbars=yes,status=no,location=no,directories=no');
	}

	function subscribeByMail() {
		document.location = "<%=subscribeByMail%>";
	}
			
</SCRIPT>
	<%-- /tpl:put --%>
<script type="text/javascript">
	/*   var _gaq = _gaq || [];
	 _gaq.push(['_setAccount', 'UA-23158809-1']);
	 _gaq.push(['_trackPageview']);

	 (function() {
	 var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	 ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	 var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	 })();
	 */
</script>
</HEAD>
<body>
	<DIV id="wrapper">
		<DIV id="header">
			<div id="headerLeft" onclick="templateLogoClick();"></div>
			<div id="headerRight">
				<%-- tpl:put name="headerbodyarea" --%>
					<%-- tpl:insert page="/login/loginform.jspf" --%><div style="float: none">
	<%
		String uri = request.getRequestURI();
		String pageName = uri.substring(uri.lastIndexOf("/") + 1);
		String jScript = "";
		if (uri.indexOf("subscriptions") > 0 && uri.indexOf("thankyou.jsp") > 0) {
			try {
				com.usatoday.esub.handlers.ShoppingCartHandler shoppingCartHandler = (com.usatoday.esub.handlers.ShoppingCartHandler) session
						.getAttribute("shoppingCartHandler");
				if (shoppingCartHandler != null) {
					jScript = shoppingCartHandler.getOnePageThankYouJavaScriptTealiumText();
				}
			} catch (Exception E) {

			}
		} else {
			com.usatoday.businessObjects.products.promotions.PromotionSet currentOfferPromotionSetTealium = null;
			com.usatoday.business.interfaces.products.OfferIntf offerTealium = com.usatoday.esub.common.UTCommon
					.getCurrentOffer(request);
			if (offerTealium != null && offerTealium instanceof com.usatoday.business.interfaces.products.SubscriptionOfferIntf) {
				currentOfferPromotionSetTealium = ((com.usatoday.business.interfaces.products.SubscriptionOfferIntf) offerTealium)
						.getPromotionSet();
				jScript = currentOfferPromotionSetTealium.getPromoOnePageJavaScriptTealiumText();
				jScript = org.apache.commons.lang3.StringUtils.replace(jScript, "page_name:\"\"", "page_name:\"" + uri + "\"");
			}
		}
	%>
	<%=jScript%>
</div>
<%-- 			<f:verbatim>
				<h:outputText styleClass="outputText" id="textThankYouJS13"	escape="false" value="#{shoppingCartHandler.tealiumOnePageThankYouJavaScriptText}"></h:outputText>
			</f:verbatim>
 --%>
<%
	String isLoginDisplay = "true";
	com.usatoday.business.interfaces.customer.CustomerIntf customer2 = null;
	String loggedinDivClass = "hideMe";
	String notLoggedinDivClass = "showMeInline";
	String firstName = "";
	String lastName = "";
	String firmInfo = "";
	String accountNum = "";
	String loginErrorMessage2 = "";
	String logOut = "";
	String publication = "";
	String changeAccountLink = "";
	String earlyAlertDisplay = "display:none";

	String jScriptNoOp = "javascript:;";

	try {
		publication = (String) session.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_PUB_NAME);
		if (publication == null) {
			publication = request.getParameter("pub");
			if (publication == null) {
				publication = com.usatoday.util.constants.UsaTodayConstants.UT_PUBCODE;
			}
		}
		if (publication.equals(com.usatoday.esub.common.UTCommon.PUB_NAMES[0])) { // USATODAY publication
			logOut = com.usatoday.esub.common.UTCommon.BASE_URL + "/index.jsp?pub=UT&logsessobj=rem";
		} else {
			logOut = com.usatoday.esub.common.UTCommon.BASE_URL + "/index.jsp?pub=BW&logsessobj=rem";
		}

		// If customer is logged in remove login page, otherwise display it
		customer2 = (com.usatoday.business.interfaces.customer.CustomerIntf) session
				.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_CUSTOMER_INFO);
		if (customer2 == null) {
			// showMeInling and hideMe are defined in style sheets
			loggedinDivClass = "showMeInline";
			notLoggedinDivClass = "hideMe";
		} else {

			if (customer2.getCurrentAccount() != null) {
				firstName = customer2.getCurrentAccount().getDeliveryContact().getFirstName();
				if (firstName != null && !firstName.trim().equals("")) {
					firstName = com.usatoday.esub.common.UTCommon.formatNameCapitalization(firstName); // Format it
				}
				lastName = customer2.getCurrentAccount().getDeliveryContact().getLastName();
				if (lastName != null && !lastName.trim().equals("")) {
					lastName = com.usatoday.esub.common.UTCommon.formatNameCapitalization(lastName); // Format it
				}
				firmInfo = customer2.getCurrentAccount().getDeliveryContact().getFirmName();
				if (customer2.getNumberOfAccounts() > 1) {
					changeAccountLink = "[&nbsp;<a href=\"/account/select_accnt.jsp\">Change</a>&nbsp;]";
				}
				try {
					accountNum = customer2.getCurrentAccount().getAccountNumber();
					//				if (accountNum == null || accountNum.length() < 4) {
					if (accountNum == null) {
						accountNum = "";
					}
					//				else {
					/*
					if (accountNum.equalsIgnoreCase("340000000")) {
						accountNum = "Pending Setup";
					}
					 */
					//				}
				} catch (Exception eeee) {
					eeee.printStackTrace();
				}

				//EarlyAlert - Check to see if their paper delivery has been affected by a delay
				// HD-CONS109 SEW
				/*
				if (customer2.getCurrentAccount().isShowDeliveryAlert()) {
				earlyAlertDisplay = "display:inline";
				}
				 */

			} else {
				if (customer2.getCurrentEmailRecord() != null
						&& customer2.getCurrentEmailRecord().getEmailAddress() != null) {
					firstName = customer2.getCurrentEmailRecord().getEmailAddress();
					accountNum = "Pending Setup";
				} else {
					firstName = "";
				}
			}
		}
		loginErrorMessage2 = (String) session.getAttribute("LOGIN_FAILED_MESSAGE");
		if (loginErrorMessage2 == null || loginErrorMessage2.trim().equals("")) {
			loginErrorMessage2 = "";
		} else {
			session.removeAttribute("LOGIN_FAILED_MESSAGE");
		}
	} catch (Exception e) {
		e.printStackTrace();
		if (firstName == null) {
			firstName = "";
		}
		if (lastName == null) {
			lastName = "";
		}
	}
	String nameInfo = firstName + " " + lastName;
	String loginInfo = null;
	if (!nameInfo.trim().equals("")) {
		loginInfo = nameInfo;
	} else {
		if (firmInfo != null && !firmInfo.trim().equals("")) {
			loginInfo = firmInfo;
		}
	}
	// if no login info, then hide data
	if (loginInfo == null) {
		loggedinDivClass = "showMeInline";
		notLoggedinDivClass = "hideMe";
		loginInfo = "";
	}
%>

<%@page import="com.usatoday.util.constants.UsaTodayConstants"%><div
	id="loginDisplayDiv" class="<%=loggedinDivClass%>">
	<table border="0" width="100%" height="60px">
		<tr>
			<td>
				<TABLE class="loginFormTable">
					<TBODY>
						<TR>
							<TD class="loginFormColumn"><br /> <a
								href="/login/auth.faces">Login to Manage Account</a></TD>
						</TR>
						<tr>
							<td class="loginFormColumn" align="right"><img
								style="cursor: pointer; margin-top: 10px;"
								onclick="javascript: templateClickToChatClickedGuest(this, event);"
								alt="Click for Assistance" title="Click for Assistance"
								src="${pageContext.request.contextPath}/theme/themeV3/themeImages/chatIcon136.png"
								hspace="0" vspace="5"></td>
						</tr>
					</TBODY>
				</TABLE></td>
		</tr>
	</table>
</div>
<div id="loginHideDiv" class="<%=notLoggedinDivClass%>">
	<table width="100%" border="0" class="loginFormTable">
		<tr>
			<td nowrap align="right">
				<TABLE class="loginFormTable">
					<TBODY>
						<TR>
							<TD class="loginFormColumn" nowrap><span
								class="loginNormalPrint">Hello, <%=loginInfo%>&nbsp;&nbsp;[&nbsp;<a
									href="<%=jScriptNoOp%>"
									onclick="javascript:document.location='<%=logOut%>';_unloadPage(this, event);">Logout</a>&nbsp;]</span>
							</TD>
						</TR>
					</TBODY>
				</TABLE></TD>
		</tr>
		<tr>
			<td class="loginFormColumn" align="right"><span
				class="loginfineprint">Account #:&nbsp;<%=accountNum%>&nbsp;<%=changeAccountLink%></span>
			</td>
		</tr>
		<tr>
			<td class="loginFormColumn" align="right"><div
					id="chatIconRequestDiv">
					<img onclick="javascript: templateClickToChatClicked(this, event);"
						style="cursor: pointer; margin-top: 10px;"
						alt="Click for Assistance" title="Click for Assistance"
						src="${pageContext.request.contextPath}/theme/themeV3/themeImages/chatIcon136.png"
						hspace="0" vspace="5">
				</div>
			</td>
		</tr>
	</table>
</div>
<div id="templateChatWindowDiv" style="display: none;">
	<div id="templateChatTopDiv">
		<span onclick="templateClickToChatHide();" style="cursor: pointer;">No
			Thanks<img src="/theme/themeV3/themeImages/close_14x14.png"
			width="14" height="14" border="0"
			style="margin-left: 5px; margin-right: 5px" /> </span>
	</div>
<!-- 	<div id="tempateChatImageDiv">
		<a
			onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
			href=""><img border="0"
			src="https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY" />
		</a>
	</div>
 -->	<div id="templateChatAccountLinksDiv">
		<ul>
			<li>To report a problem with delivery, click <a
				href="/account/cp/delissue.faces"> here</a>.</li>
			<li>To place your delivery on a temporary hold, click <a
				href="/account/vacationholds/holds.faces"> here</a>.</li>
			<li>Forgot your password or login? Click <a
				href="/idpassword/forgotpassword.jsp"> here</a>.
			<li>To contact us <a href="/feedback/feedback.jsp"> click here</a>.</li>				
<!-- 			<li>To cancel your subscription, please call 1-800-872-0001 and -->
<!-- 				select option 6.</li> -->
		</ul>
	</div>

	<div id="templateChatLinksDiv">
		<ul>

			<li><a
				onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
				href="">Click to Chat</a>
			</li>
		</ul>
	</div>
	<div id="templateChatBottomDiv">
		<span class="outputText">Or, call us at:<br> <b>Call
				1-877-713-6241</b><br>(M-F 8:00 a.m. to 7 p.m. E.T.)</span>
	</div>
</div>
<div id="templateChatWindowGuestDiv" style="display: none;">
	<div id="templateChatTopDiv">
		<span onclick="templateClickToChatHideGuest();"
			style="cursor: pointer;">No Thanks<img
			src="/theme/themeV3/themeImages/close_14x14.png" width="14"
			height="14" border="0" style="margin-left: 5px; margin-right: 5px" />
		</span>
	</div>
<!-- 	<div id="tempateChatImageDiv">
		<a
			onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
			href=""><img border="0"
			src="https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY" />
		</a>
	</div>
 -->	<div id="templateChatAccountLinksDiv">
		<ul>
			<li>To manage your account online - including reporting a
				problem with delivery or placing a temporary hold on your account -
				log in by entering your e-mail address and password. Click <a
				href="/login/auth.faces"> here</a> to access the login page now.</li>
			<li>Forgot your password or login? <a
				href="/idpassword/forgotpassword.jsp"> click here</a>.</li>
			<li>To contact us <a href="/feedback/feedback.jsp"> click here</a>.</li>
			<!-- 			<li>To cancel your subscription, please call 1-800-872-0001 and -->
			<!-- 				select option 6.</li> -->
		</ul>

	</div>

	<div id="templateChatLinksDiv">
		<ul>
			<li><a
				onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
				href="">Click to Chat</a>
			</li>
		</ul>
	</div>
	<div id="templateChatBottomDiv">
		<span class="outputText">Or, call us at:<br> <b>Call
				1-877-713-6241</b><br>(M-F 8:00 a.m. to 7 p.m. E.T.)</span>
	</div>
</div>
<%-- /tpl:insert --%>			
				<%-- /tpl:put --%>
			</div>
		</DIV>
		<!-- end header -->
		<DIV id="content">
			<DIV id="whitebox">
				<%-- tpl:put name="bodyAreaMainWhiteBox" --%>
<DIV id=nav>
	<DIV id=leftNavList>
		<UL>
		  <LI><A href="/products.html">USA TODAY Products</A></LI>
		  <LI><A href="<%=subcribeJS%>">Subscribe</A></LI>
		  <LI><A href="/account/accounthistory/accountSummary.faces">Manage account</A></LI>
		  <LI><A href="<%=subcribeIntJS%>">Outside the USA</A></LI>
		  <LI><A href="<%=backIssueJS%>">Order back issues</A></LI>
		  <%=dynamicNavigation %>
		</UL>
	</DIV>
	<DIV id="navPromo">
		<%-- tpl:put name="navigationArea" --%>
			<%=navigationPromoImgHREF %><img class="hideMe" id="navPromoImage" <%=navigationPromoImgAltText %> src="/images/shim.gif" width="150" height="90"><%=navClosingHref %><%=navigationPromoFlash%>
	    <%-- /tpl:put --%>
	</div>
</DIV>
	<DIV id=pageContent>
		<%-- tpl:put name="MainBodyArea" --%>
<form action="/subscriptions/mpfProcessorV2.do" method="post" name="mpfForm" onsubmit="return validateForm()">
<table border="0" cellpadding="3" cellspacing="0" width="740">
	<tbody>
		<tr>
			<td align="center" colspan="5"><%=msg%></td>
		</tr>
		<tr>
			<td colspan="5" align="center"><b> <font size="+2">Subscribe to </font><font size="+2" color="#195ea3">USA TODAY</font><font size="+2"> And Earn 800 AMEX Bonus Miles Plus<br>A 
			Free Gift Card!</font></b></td>
		</tr>
		<tr>
			<td></td>
			<td height="20"></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td colspan="5" bgcolor="#195ea3" align="center"><b><font color="white" size="+1">&lt;&lt;</font> <font color="white" size="+1">Choose Your Subscription Term</font><font color="white">
			</font><font color="white" size="+1">&gt;&gt;</font></b></td>
		</tr>
		<tr>
			<td colspan="5" align="center"></td>
		</tr>
		<tr>
			<td align="right" valign="middle" nowrap="nowrap" colspan="4">
			<center>
			<table width="550" border="0" cellpadding="3" cellspacing="0">
				<tbody>
					<tr>
						<td align="left" valign="top"><input type="radio" name="mpfTerms" value="UT_RGCAK_DI_156.00_052" checked></td>
						<td><b>Best Deal!!</b> 52 Weeks of USA TODAY for $156.00 </td>
					</tr>
					<tr>
						<td align="left" valign="top"><input type="radio" name="mpfTerms" value="UT_RGCAK_DN_117.00_039"></td>
						<td><b>Great Deal! </b>39 Weeks of USA TODAY for $117.00 </td>
					</tr>
					<tr>
						<td align="left" valign="top"><input type="radio" name="mpfTerms" value="UT_RGCAK_DH_78.00_026"></td>
						<td><b>Good Deal!</b> 26 Weeks of USA TODAY for $78.00</td>
					</tr>
				</tbody>
			</table>
			</center>
			</td>
			<td></td>

		</tr>
		<tr>
			<td width="293"></td>
			<td height="5" width="298"></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td colspan="5">
			<center>
			<table border="0" cellpadding="3">
				<tbody>
					<tr>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td bgcolor="#195ea3" align="center" colspan="2"><font size="+1" color="white"><b>
						&lt;&lt;Premium Offer&gt;&gt;</b></font></td></tr>
					<tr>
						<td align="right"><b>Enter your American Express Club Number:</b></td>
						<td align="left"><input type="text" name="NUMBER_MA_CLUB" size="20"></td>
					</tr>
					<tr>
						<td height="30" valign="top" align="right"><font size="-1">(Please allow 2
						billing cycles for your miles to show up)</font></td>
						<td></td>
					</tr>
					<tr>
						<td><b>Choose Your Gift Card:</b></td>
						<td></td>
					</tr>
					<tr>
						<td colspan="2">
						<center>
						<table border="0" cellpadding="0" cellspacing="0">
							<tbody>
								<tr>
									<td align="center"><input type="radio" value="$10 MOVIE.COM" name="GIFTCARD_MC_GIFTCARD" onclick="javasript:mpfForm.GIFTCARD_MB_GIFTCARD.checked=false;" checked></td>
									<td><b>$10 <a href="http://www.movie.com" target="_blank">Movie.com</a>
									Gift Card</b></td>
								</tr>
								<tr>
									<td><input type="radio" name="GIFTCARD_MB_GIFTCARD" value="$12 STARBUCKS" onclick="javasript:mpfForm.GIFTCARD_MC_GIFTCARD.checked=false;"></td>
									<td><b>$12 <a href="http://www.starbucks.com" target="_blank">Starbucks</a> Gift Card</b></td>
								</tr>
							</tbody>
						</table>
						</center>
						</td></tr>
				</tbody>
			</table>
			</center>
			</td>
		</tr>
		<tr>
			<td colspan="5">
			<center>
			
			</center>
			</td>
		</tr>
		<tr>
			<td align="center" colspan="5" height="15">

			<hr width="80%" size="5">
			</td>
		</tr>
		<tr>
			<td align="right" valign="top"></td>
			<td align="left" valign="top"></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td align="right" valign="top"></td>
			<td align="left" valign="top"></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td align="center" valign="middle" colspan="4">
			<center>
			
			</center>
			</td>
			<td></td>
		</tr>
		<tr>
			<td align="right" valign="middle" height="15"></td>
			<td align="left" valign="middle"></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td align="center" colspan="4"><input type="submit" name="mpfSubscribe" value="Subscribe"></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
	</tbody>
</table>
<input type="hidden" name="mpfPremiumPromoCode" value="MM"> <input type="hidden" name="mpfDescriptionText" value="AMEX Bonus Points &amp; Gift Card"> <input type="hidden" name="mpfEmailText" value="Please allow for 4 to 6 weeks delivery time of your selected Gift Card."><input type="hidden" name="mpfOfferType" value="NEW_GIFT"></form>
<table width="95%">
	<tbody>
		<tr>
			<td width="115"></td>
			<td align="left"><font face="Arial Narrow" color="#5b748a" size="1">USA
			TODAY offers various promotional programs or premium incentives in
			connection with subscription sales. Discounts or premium incentives<br>
			&nbsp;&nbsp;compare the specific offer to the Newsstand
			price and not to any other promotional programs or premium incentives
			that may exist.<br>
			</font></td>
		</tr>
		<tr>
			<td valign="top"></td>
			<td valign="top">
			<p><font face="Arial" color="#000000" size="1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&copy;
			Copyright 2005 <a href="http://www.usatoday.com/marketing/credit.htm">USA
			TODAY</a>, a division of <a href="http://www.gannett.com">Gannett Co.</a>
			Inc. <a onclick="javascript:window.open('/privacy/privacy.htm','PrivacyPolicy', 'width=485,height=500,menubar=no,titlebar=yes,scrollbars=yes,status=no,location=no,directories=no');" href="javascript:;">Privacy Policy, </a><a onclick="javascript:window.open('/service/service.jsp','TermsofService', 'width=485,height=500,menubar=no,titlebar=yes,scrollbars=yes,status=no,location=no,directories=no');" href="javascript:;">Terms of Service</a></font></p>
			</td>
		</tr>
	</tbody>
</table>
<p><br>
</p>
<%-- /tpl:put --%>
	</DIV>
	<p>&nbsp;</p><%-- /tpl:put --%>
				<DIV id="clear"></DIV>
				<DIV id="bluebar">
					<a href="/feedback/feedback.jsp"
						style="color: white; font-weight: bold; text-decoration: none; font-size: 14px"
						title="Contact Us">Contact Us</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;1-800-872-0001&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a
						href="/faq/utfaq.jsp"
						style="color: white; font-weight: bold; text-decoration: none; font-size: 14px"
						title="Frequently Asked Questions">FAQ</a>
				</DIV>
				<%-- tpl:put name="footerMainArea" --%>
		<%-- tpl:put name="footerArea_1" --%><!-- Insert Footer Ad Here --><%-- /tpl:put --%>
		<%=trackingBug.getOmnitureHTML() %>
	<%-- /tpl:put --%>
				<DIV id=graybar>
					Copyright
					<script>
						document.write(getCurrentYear());
					</script>
					USA TODAY, a division of <a href="http://www.gannett.com">Gannett
						Co. Inc.</a> <a href="/privacy/privacy.htm">Privacy
						Notice/California Privacy Notice</a>. <a
						href="/privacy/privacy.htm#adchoice">Ad Choices</a>.<br> By
					using this service, you accept our <a href="/service/service.jsp">Terms
						of Service</a>.
				</DIV>
				<DIV id=copyright>
					<P>Offers available to new subscribers only. Home delivery not
						available in some areas. Not valid with any other offer.
						Prepayment required. After the promotional period ends, the
						subscription will continue and be billed monthly at the then
						regular rate, less any applicable credits, unless USA TODAY is
						otherwise notified. Applicable taxes may not be included in the
						rate. If at any time you decide to cancel your subscription, you
						may contact customer service at 1-800-872-0001 and the full amount
						of any balance over $2.00 will be returned.</P>
				</DIV>
				<!--END OF COPYWRITE-->
				<DIV id=footer></DIV>
			</DIV>
			<!--END OF WHITEBOX-->
		</DIV>
		<!--END OF CONTENT-->
	</DIV>
</BODY>
</HTML><%-- /tpl:insert --%>
<%-- /tpl:insert --%>