<%-- tpl:insert page="/theme/themeV3/jsfTheme/subscriberPortalLeftNavAccount_JSF.jtpl" --%><!DOCTYPE HTML><%@page import="com.usatoday.business.interfaces.products.ProductIntf"%>
<%@page import="com.usatoday.businessObjects.products.ProductBO"%>
<%@page import="com.usatoday.util.MobileDeviceDetectionUtil"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%><%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%><%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<f:view><f:loadBundle basename="resourceFile" var="labels" /><%-- tpl:insert page="/theme/subscriptionPortalRoot.htpl" --%><HTML>
<HEAD>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8">
<%-- tpl:put name="headerareaTop" --%><%
	String docProtocol = "https:"; 
	if (!request.isSecure()) {
		docProtocol = "http:";
	}
	// following work around to get around problem with RAD 8.0.2 automated links in templates - APE
	String subcribeJS = "javascript:_subscribeToPub();"; 
	String backIssueJS = "javascript:_shopUSAT();";
	
	// change this to any javascript you want called on page load
	String initFunction = "";
	String dynamicNavigation = "";
	
	com.usatoday.esub.common.WebBug trackingBug = com.usatoday.esub.common.UTCommon.getWebAnalyticsBug();
	try {
		trackingBug.deriveValuesFromRequest(request);
	}
	catch (Exception e) {
		; // ignore
	}

	// not shown if electronic pub   
	String suspendResumeDeliveryNavOption = "<LI><A href=\"/account/vacationholds/holds.faces\">Suspend/Resume delivery</A></LI>";
	String suspendResumeDeliveryMobileOption = "<option value=\"/account/vacationholds/holds.faces\">Suspend delivery</option><option value=\"/account/vacationholds/holds.faces\">Resume delivery</option>";
	
	String reportDeliveryProblemNavOption = "<LI><A href=\"/account/cp/delissue.faces\">Report delivery problem</A></LI>";
	String reportDeliveryProblemMobileOption = "<option value=\"/account/cp/delissue.faces\">Report a problem</option>";
	
	String cancelSubscriptionNavOption = "";
	String cancelSubscriptionMobileOption = "";

	String changeAccountLinkMobile = "";
		
	String pubCode = "";
	String productName = "USA TODAY";
	 
    boolean isEEditionProduct = true;

	com.usatoday.businessObjects.products.promotions.PromotionSet defaultPromotionSet = null;
	com.usatoday.businessObjects.products.promotions.PromotionSet currentOfferPromotionSet = null;
	com.usatoday.business.interfaces.customer.CustomerIntf customer = null;
   	com.usatoday.businessObjects.products.promotions.PromotionSet currentAccountPromotionSet = null;
   		
	com.usatoday.business.interfaces.products.OfferIntf offer = null;   		
	try {
		offer = com.usatoday.esub.common.UTCommon.getCurrentOfferVersion2(request);
		pubCode = com.usatoday.util.constants.UsaTodayConstants.UT_PUBCODE;
		if (offer != null) {
			pubCode = offer.getPubCode();
		}
		// get the default promotion set for the publication		
		defaultPromotionSet = com.usatoday.esub.common.UTCommon.getDefaultPromotionSetForPub(pubCode);
		
		// get the keycode specific promotion set
		
		if (offer != null && offer instanceof com.usatoday.business.interfaces.products.SubscriptionOfferIntf) {
			currentOfferPromotionSet = ((com.usatoday.business.interfaces.products.SubscriptionOfferIntf)offer).getPromotionSet();
		}
		
		if (currentOfferPromotionSet != null) {
			com.usatoday.business.interfaces.products.promotions.HTMLPromotionIntf dPromo = currentOfferPromotionSet.getDynamicNavigationCustServiceHTML();
			if (dPromo != null) {
				dynamicNavigation = dPromo.getPromotionalHTML();
			}
		}
				
		boolean isSignedIn = false;
		customer = (com.usatoday.business.interfaces.customer.CustomerIntf)session.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_CUSTOMER_INFO);
		if (customer != null) {
			isSignedIn = true;
			com.usatoday.business.interfaces.products.SubscriptionProductIntf product = com.usatoday.businessObjects.products.SubscriptionProductBO.getSubscriptionProduct(customer.getCurrentEmailRecord().getPubCode());
			isEEditionProduct = product.isElectronicDelivery(); 
			// This promotion set is based on pub and keycode on the customer account
			currentAccountPromotionSet = com.usatoday.businessObjects.products.promotions.PromotionManager.getInstance().getPromotionsForOffer(customer.getCurrentAccount().getPubCode(), customer.getCurrentAccount().getKeyCode());
			if (isEEditionProduct) {
				suspendResumeDeliveryNavOption = ""; // clear vacation holds option
				suspendResumeDeliveryMobileOption = "";
				reportDeliveryProblemNavOption = ""; // clear deliveyr problem link
				reportDeliveryProblemMobileOption = "";
//				cancelSubscriptionNavOption = "<LI><A href=\"/account/cancels/cancelSubscription.faces\">Cancel subscription</A></LI>"; // show cancels 			
//				cancelSubscriptionMobileOption = "<option value=\"/account/cancels/cancelSubscription.faces\">Cancel subscription</option>"; 
			}
			
			try {
				String brandingPubCode = product.getBrandingPubCode();
				ProductIntf bp = ProductBO.fetchProduct(brandingPubCode);
				productName = bp.getName();
			}
			catch (Exception teee) {
			}							
			
			if (customer.getNumberOfAccounts() > 1) {
				changeAccountLinkMobile = "<option value=\"/account/select_accnt.jsp\">Change Account</option>";
			}
			
		}
				
	} catch (Exception e) {
	
	}
	String mobileCSS = "";
	if (MobileDeviceDetectionUtil.isSmallScreenDevice(request)) {
		mobileCSS = "<LINK href=\"/theme/themeV3/css/smallScreen.css\" rel=\"stylesheet\" type=\"text/css\">";
	}
 %>
<script src="/common/prototype/prototype.js"></script>
<script src="/common/prototype/scriptaculous/scriptaculous.js" type="text/javascript"></script>
<link rel="stylesheet"	href="/theme/themeV3/css/stylesheet_jsfcomponents.css"	type="text/css">
<%-- tpl:put name="headerarea_Top" --%>
		<title>USA TODAY - Pay Bill</title>
	<%-- /tpl:put --%>
<%-- /tpl:put --%>
<LINK REL="SHORTCUT ICON" HREF="/favicon.ico">
<LINK REL="icon" href="/favicon.ico" TYPE="image/ico" />
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META http-equiv="Content-Style-Type" content="text/css">
<meta name="viewport" content="initial-scale=1.0">
<LINK href="/theme/themeV3/css/usatodayBase.css" rel="stylesheet"
	type="text/css">
<LINK href="/theme/themeV3/css/usatodayAddOn.css" rel="stylesheet"
	type="text/css">
<script src="/common/usatCommon.js"></script>
<%-- tpl:put name="headareaBottom" --%>
<%=mobileCSS %>
<%-- tpl:put name="headarea_Bottom" --%>
		<META http-equiv="PRAGMA" content="NO-CACHE">
		<META http-equiv="CACHE-CONTROL"
			content="no-store,NO-CACHE,must-revalidate,post-check=0,pre-check=0,max-age=0">
		<META http-equiv="Expires" content="-1">
		<%
	initFunction += "; startKeepAlive(); initThisPage(thisObj, thisEvent);";
 %>
		<!-- begin Marin Software Tracking Script -->
		<script type='text/javascript'>
			var _marinClientId = "518c2822647";
			var _marinProto = (("https:" == document.location.protocol) ? "https://"
					: "http://");
			document.write(unescape("%3Cscript src='" + _marinProto
					+ "tracker.marinsm.com/tracker/" + _marinClientId
					+ ".js' type='text/javascript'%3E%3C/script%3E"));
		</script>
		<script type='text/javascript'>
			try {
				_marinTrack.processOrders();
			} catch (err) {
			}
		</script>
		<noscript>
			<img
				src="https://tracker.marinsm.com/tp?act=2&cid=518c2822647&trans=UTM:T|order-id||total|||||UTM:I|order-id|Renewal|||price|" />
		</noscript>
		<!-- end Copyright Marin Software -->
		<div style="float: none">
			<f:verbatim>
				<h:outputText styleClass="outputText" id="textPageJS2" escape="false"
					value="#{currentOfferHandler.onePageJavaScript2Text}"></h:outputText>
				<h:outputText styleClass="outputText" id="textPageJS3" escape="false"
					value="#{currentOfferHandler.onePageJavaScript3Text}"></h:outputText>
			</f:verbatim>
		</div>
		<script type="text/javascript" src="/common/keepalive.js"></script>
		<script type="text/javascript" src="/common/popUpOverlay.js"></script>

		<script type="text/javascript">

// VerifyExistingCCInfoDIV
// NewCCInfoDiv
var verifyExistingDiv;
var newCCInfoDiv;
var showOnFileOptions;

function initCCInfoPanels() {
	showOnFileOptions = $('formOrderEntryForm:renewWithCardOnFailAvailable');
	verifyExistingDiv = $('VerifyExistingCCInfoDIV');
	newCCInfoDiv = $('NewCCInfoDIV');
	
	try {
		if (showOnFileOptions.value == "false") {
			newCCInfoDiv.show();
		}
		else {
			var radioUseExisting = $('formOrderEntryForm:radioRenewalPaymentOption:0');
			var radioDontUseExisting = $('formOrderEntryForm:radioRenewalPaymentOption:1');
			if (radioUseExisting.checked) {
				verifyExistingDiv.show();
			}
			else {
				if (radioDontUseExisting.checked) {
					newCCInfoDiv.show();
				}
			}
		}		
	}
	catch (e) {
	}
		
}

var checkoutGuaranteeDivElement;

function initThisPage(thisEvent, thisObj) {
	try {
		initCCInfoPanels();
		usatOE_selectedTermRequiresEZPay = usatOE_isForceEZPayTermSelectedFunc();
		usatOE_updatePaymentPanelClass("CC");
	}	
	catch (e) {
		; // ignore
	}
	try {
	
		checkoutGuaranteeDivElement= $('checkoutGuaranteeDiv');
		new Draggable(checkoutGuaranteeDivElement);
	
	}	
	catch (e) {
		; // ignore
	}
}
function func_10(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	thisObj.value = "Please Wait...";

	popOffer_programEnabled = false;

	var windowOverlay = $('formSubmittedOverlay');

	windowOverlay.show();

	var windowOverlayInfo = $('formSubmittedOverlayInfo');
	windowOverlayInfo.show();
	
	setTimeout("usatOE_updateAnimation()", 200);
	
	return true;
}

function func_11(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'

//show the correct panel
	if (thisObj.value == "current_cc_info") {
		verifyExistingDiv.show();
		newCCInfoDiv.hide();
	}
	else {
		verifyExistingDiv.hide();
		newCCInfoDiv.show();
	}
}</script>
	<%-- /tpl:put --%>
<script>
// method called on page load
function initPage(thisObj, thisEvent) {
	// insert site-wide code here

	// for product messageing in popUpOverlay.js
	usat_productName = "<%=productName %>";

	<%=initFunction %>
}
</script>
<%-- /tpl:put --%>
<script type="text/javascript">
	/*   var _gaq = _gaq || [];
	 _gaq.push(['_setAccount', 'UA-23158809-1']);
	 _gaq.push(['_trackPageview']);

	 (function() {
	 var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	 ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	 var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	 })();
	 */
</script>
</HEAD>
<body>
	<DIV id="wrapper">
		<DIV id="header">
			<div id="headerLeft" onclick="templateLogoClick();"></div>
			<div id="headerRight">
				<%-- tpl:put name="headerbodyarea" --%>
					<%-- tpl:insert page="/login/loginform.jspf" --%><div style="float: none">
	<%
		String uri = request.getRequestURI();
		String pageName = uri.substring(uri.lastIndexOf("/") + 1);
		String jScript = "";
		if (uri.indexOf("subscriptions") > 0 && uri.indexOf("thankyou.jsp") > 0) {
			try {
				com.usatoday.esub.handlers.ShoppingCartHandler shoppingCartHandler = (com.usatoday.esub.handlers.ShoppingCartHandler) session
						.getAttribute("shoppingCartHandler");
				if (shoppingCartHandler != null) {
					jScript = shoppingCartHandler.getOnePageThankYouJavaScriptTealiumText();
				}
			} catch (Exception E) {

			}
		} else {
			com.usatoday.businessObjects.products.promotions.PromotionSet currentOfferPromotionSetTealium = null;
			com.usatoday.business.interfaces.products.OfferIntf offerTealium = com.usatoday.esub.common.UTCommon
					.getCurrentOffer(request);
			if (offerTealium != null && offerTealium instanceof com.usatoday.business.interfaces.products.SubscriptionOfferIntf) {
				currentOfferPromotionSetTealium = ((com.usatoday.business.interfaces.products.SubscriptionOfferIntf) offerTealium)
						.getPromotionSet();
				jScript = currentOfferPromotionSetTealium.getPromoOnePageJavaScriptTealiumText();
				jScript = org.apache.commons.lang3.StringUtils.replace(jScript, "page_name:\"\"", "page_name:\"" + uri + "\"");
			}
		}
	%>
	<%=jScript%>
</div>
<%-- 			<f:verbatim>
				<h:outputText styleClass="outputText" id="textThankYouJS13"	escape="false" value="#{shoppingCartHandler.tealiumOnePageThankYouJavaScriptText}"></h:outputText>
			</f:verbatim>
 --%>
<%
	String isLoginDisplay = "true";
	com.usatoday.business.interfaces.customer.CustomerIntf customer2 = null;
	String loggedinDivClass = "hideMe";
	String notLoggedinDivClass = "showMeInline";
	String firstName = "";
	String lastName = "";
	String firmInfo = "";
	String accountNum = "";
	String loginErrorMessage2 = "";
	String logOut = "";
	String publication = "";
	String changeAccountLink = "";
	String earlyAlertDisplay = "display:none";

	String jScriptNoOp = "javascript:;";

	try {
		publication = (String) session.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_PUB_NAME);
		if (publication == null) {
			publication = request.getParameter("pub");
			if (publication == null) {
				publication = com.usatoday.util.constants.UsaTodayConstants.UT_PUBCODE;
			}
		}
		if (publication.equals(com.usatoday.esub.common.UTCommon.PUB_NAMES[0])) { // USATODAY publication
			logOut = com.usatoday.esub.common.UTCommon.BASE_URL + "/index.jsp?pub=UT&logsessobj=rem";
		} else {
			logOut = com.usatoday.esub.common.UTCommon.BASE_URL + "/index.jsp?pub=BW&logsessobj=rem";
		}

		// If customer is logged in remove login page, otherwise display it
		customer2 = (com.usatoday.business.interfaces.customer.CustomerIntf) session
				.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_CUSTOMER_INFO);
		if (customer2 == null) {
			// showMeInling and hideMe are defined in style sheets
			loggedinDivClass = "showMeInline";
			notLoggedinDivClass = "hideMe";
		} else {

			if (customer2.getCurrentAccount() != null) {
				firstName = customer2.getCurrentAccount().getDeliveryContact().getFirstName();
				if (firstName != null && !firstName.trim().equals("")) {
					firstName = com.usatoday.esub.common.UTCommon.formatNameCapitalization(firstName); // Format it
				}
				lastName = customer2.getCurrentAccount().getDeliveryContact().getLastName();
				if (lastName != null && !lastName.trim().equals("")) {
					lastName = com.usatoday.esub.common.UTCommon.formatNameCapitalization(lastName); // Format it
				}
				firmInfo = customer2.getCurrentAccount().getDeliveryContact().getFirmName();
				if (customer2.getNumberOfAccounts() > 1) {
					changeAccountLink = "[&nbsp;<a href=\"/account/select_accnt.jsp\">Change</a>&nbsp;]";
				}
				try {
					accountNum = customer2.getCurrentAccount().getAccountNumber();
					//				if (accountNum == null || accountNum.length() < 4) {
					if (accountNum == null) {
						accountNum = "";
					}
					//				else {
					/*
					if (accountNum.equalsIgnoreCase("340000000")) {
						accountNum = "Pending Setup";
					}
					 */
					//				}
				} catch (Exception eeee) {
					eeee.printStackTrace();
				}

				//EarlyAlert - Check to see if their paper delivery has been affected by a delay
				// HD-CONS109 SEW
				/*
				if (customer2.getCurrentAccount().isShowDeliveryAlert()) {
				earlyAlertDisplay = "display:inline";
				}
				 */

			} else {
				if (customer2.getCurrentEmailRecord() != null
						&& customer2.getCurrentEmailRecord().getEmailAddress() != null) {
					firstName = customer2.getCurrentEmailRecord().getEmailAddress();
					accountNum = "Pending Setup";
				} else {
					firstName = "";
				}
			}
		}
		loginErrorMessage2 = (String) session.getAttribute("LOGIN_FAILED_MESSAGE");
		if (loginErrorMessage2 == null || loginErrorMessage2.trim().equals("")) {
			loginErrorMessage2 = "";
		} else {
			session.removeAttribute("LOGIN_FAILED_MESSAGE");
		}
	} catch (Exception e) {
		e.printStackTrace();
		if (firstName == null) {
			firstName = "";
		}
		if (lastName == null) {
			lastName = "";
		}
	}
	String nameInfo = firstName + " " + lastName;
	String loginInfo = null;
	if (!nameInfo.trim().equals("")) {
		loginInfo = nameInfo;
	} else {
		if (firmInfo != null && !firmInfo.trim().equals("")) {
			loginInfo = firmInfo;
		}
	}
	// if no login info, then hide data
	if (loginInfo == null) {
		loggedinDivClass = "showMeInline";
		notLoggedinDivClass = "hideMe";
		loginInfo = "";
	}
%>

<%@page import="com.usatoday.util.constants.UsaTodayConstants"%><div
	id="loginDisplayDiv" class="<%=loggedinDivClass%>">
	<table border="0" width="100%" height="60px">
		<tr>
			<td>
				<TABLE class="loginFormTable">
					<TBODY>
						<TR>
							<TD class="loginFormColumn"><br /> <a
								href="/login/auth.faces">Login to Manage Account</a></TD>
						</TR>
						<tr>
							<td class="loginFormColumn" align="right"><img
								style="cursor: pointer; margin-top: 10px;"
								onclick="javascript: templateClickToChatClickedGuest(this, event);"
								alt="Click for Assistance" title="Click for Assistance"
								src="${pageContext.request.contextPath}/theme/themeV3/themeImages/chatIcon136.png"
								hspace="0" vspace="5"></td>
						</tr>
					</TBODY>
				</TABLE></td>
		</tr>
	</table>
</div>
<div id="loginHideDiv" class="<%=notLoggedinDivClass%>">
	<table width="100%" border="0" class="loginFormTable">
		<tr>
			<td nowrap align="right">
				<TABLE class="loginFormTable">
					<TBODY>
						<TR>
							<TD class="loginFormColumn" nowrap><span
								class="loginNormalPrint">Hello, <%=loginInfo%>&nbsp;&nbsp;[&nbsp;<a
									href="<%=jScriptNoOp%>"
									onclick="javascript:document.location='<%=logOut%>';_unloadPage(this, event);">Logout</a>&nbsp;]</span>
							</TD>
						</TR>
					</TBODY>
				</TABLE></TD>
		</tr>
		<tr>
			<td class="loginFormColumn" align="right"><span
				class="loginfineprint">Account #:&nbsp;<%=accountNum%>&nbsp;<%=changeAccountLink%></span>
			</td>
		</tr>
		<tr>
			<td class="loginFormColumn" align="right"><div
					id="chatIconRequestDiv">
					<img onclick="javascript: templateClickToChatClicked(this, event);"
						style="cursor: pointer; margin-top: 10px;"
						alt="Click for Assistance" title="Click for Assistance"
						src="${pageContext.request.contextPath}/theme/themeV3/themeImages/chatIcon136.png"
						hspace="0" vspace="5">
				</div>
			</td>
		</tr>
	</table>
</div>
<div id="templateChatWindowDiv" style="display: none;">
	<div id="templateChatTopDiv">
		<span onclick="templateClickToChatHide();" style="cursor: pointer;">No
			Thanks<img src="/theme/themeV3/themeImages/close_14x14.png"
			width="14" height="14" border="0"
			style="margin-left: 5px; margin-right: 5px" /> </span>
	</div>
<!-- 	<div id="tempateChatImageDiv">
		<a
			onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
			href=""><img border="0"
			src="https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY" />
		</a>
	</div>
 -->	<div id="templateChatAccountLinksDiv">
		<ul>
			<li>To report a problem with delivery, click <a
				href="/account/cp/delissue.faces"> here</a>.</li>
			<li>To place your delivery on a temporary hold, click <a
				href="/account/vacationholds/holds.faces"> here</a>.</li>
			<li>Forgot your password or login? Click <a
				href="/idpassword/forgotpassword.jsp"> here</a>.
			<li>To contact us <a href="/feedback/feedback.jsp"> click here</a>.</li>				
<!-- 			<li>To cancel your subscription, please call 1-800-872-0001 and -->
<!-- 				select option 6.</li> -->
		</ul>
	</div>

	<div id="templateChatLinksDiv">
		<ul>

			<li><a
				onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
				href="">Click to Chat</a>
			</li>
		</ul>
	</div>
	<div id="templateChatBottomDiv">
		<span class="outputText">Or, call us at:<br> <b>Call
				1-877-713-6241</b><br>(M-F 8:00 a.m. to 7 p.m. E.T.)</span>
	</div>
</div>
<div id="templateChatWindowGuestDiv" style="display: none;">
	<div id="templateChatTopDiv">
		<span onclick="templateClickToChatHideGuest();"
			style="cursor: pointer;">No Thanks<img
			src="/theme/themeV3/themeImages/close_14x14.png" width="14"
			height="14" border="0" style="margin-left: 5px; margin-right: 5px" />
		</span>
	</div>
<!-- 	<div id="tempateChatImageDiv">
		<a
			onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
			href=""><img border="0"
			src="https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY" />
		</a>
	</div>
 -->	<div id="templateChatAccountLinksDiv">
		<ul>
			<li>To manage your account online - including reporting a
				problem with delivery or placing a temporary hold on your account -
				log in by entering your e-mail address and password. Click <a
				href="/login/auth.faces"> here</a> to access the login page now.</li>
			<li>Forgot your password or login? <a
				href="/idpassword/forgotpassword.jsp"> click here</a>.</li>
			<li>To contact us <a href="/feedback/feedback.jsp"> click here</a>.</li>
			<!-- 			<li>To cancel your subscription, please call 1-800-872-0001 and -->
			<!-- 				select option 6.</li> -->
		</ul>

	</div>

	<div id="templateChatLinksDiv">
		<ul>
			<li><a
				onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
				href="">Click to Chat</a>
			</li>
		</ul>
	</div>
	<div id="templateChatBottomDiv">
		<span class="outputText">Or, call us at:<br> <b>Call
				1-877-713-6241</b><br>(M-F 8:00 a.m. to 7 p.m. E.T.)</span>
	</div>
</div>
<%-- /tpl:insert --%>
		<%-- /tpl:put --%>
			</div>
		</DIV>
		<!-- end header -->
		<DIV id="content">
			<DIV id="whitebox">
				<%-- tpl:put name="bodyAreaMainWhiteBox" --%>
<div id="mobileAccountNav">
	<h4>My Account Options:</h4>
	<select name="accountNavLinksSelect" onchange="javascript: mobileNavChanged(this, event);">
		<option value="noop" selected="selected">&nbsp;</option>
	 	<option value="/account/accounthistory/accountSummary.faces">Account Information</option>
		<option value="/custserviceindex.jsp?custopt=RENEW">Pay Bill</option>
		<%=suspendResumeDeliveryMobileOption %>
		<%=reportDeliveryProblemMobileOption %>
		<option value="/custserviceindex.jsp?custopt=DELA">Change delivery info</option>
		<option value="/custserviceindex.jsp?custopt=BILA">Change billing info</option>
		<option value="/idpassword/changeIDpassword.jsp">Change email</option>
		<option value="/idpassword/changeIDpassword.jsp">Change password</option>
		<option value="/welcome.jsp">Give a gift</option>
		<option value="http://onlinestore.usatoday.com">Order back issues</option>
		<%=cancelSubscriptionMobileOption %>
		<%=changeAccountLinkMobile %>
		<option value="<%=logOut%>">Log Out</option>
	</select>
	<hr />	
</div>
	<DIV id=nav>
	<DIV id="navPromo">
		<%-- tpl:put name="navigationArea_Top" --%><%-- /tpl:put --%>
	</div>
	<DIV id=leftNavList>
		<UL>
		  <LI><A href="/account/accounthistory/accountSummary.faces">Account information</A></LI>
		  <LI><A href="/custserviceindex.jsp?custopt=RENEW">Pay Bill</A></LI>
		  <%=suspendResumeDeliveryNavOption %>
		  <%=reportDeliveryProblemNavOption %>
		  <LI><A href="/custserviceindex.jsp?custopt=DELA">Change delivery info</A></LI>
		  <LI><A href="/custserviceindex.jsp?custopt=BILA">Change billing info</A></LI>
		  <LI><A href="/idpassword/changeIDpassword.jsp">Change e-mail/password</A></LI>
		  <LI><A href="<%=subcribeJS%>">Give a gift</A></LI>
		  <LI><A href="<%=backIssueJS%>">Order back issues</A></LI>
		  <%=cancelSubscriptionNavOption %>
		  <%=dynamicNavigation %>
		</UL>
	</DIV>
	<DIV id="navPromo">
		<%-- tpl:put name="navigationArea_Bottom" --%><%-- /tpl:put --%>
	</div>
</DIV>
<DIV id=pageContent>
	<%-- tpl:put name="MainBodyArea" --%>
							<hx:scriptCollector id="renewalFormScriptCollector"
								preRender="#{pc_Renew.onPageLoadBegin}">
								<h1>
									<h:outputText id="textMainTableHeader" value="Pay Bill"
										styleClass="H1Class"></h:outputText>
								</h1>
								<div id="pageContent70">
									<h:form id="formOrderEntryForm">
										<h:messages styleClass="messages" id="messages1"></h:messages>
										<hx:jspPanel id="jspPanel1"
											rendered="#{renewalHandler.subscriptionActive}">
											<hx:panelFormBox helpPosition="over" labelPosition="left"
												styleClass="panelFormBoxTerms renewalOrderFormInsideGridStyles"
												id="formBoxTermsFormBox" widthLabel="15" label="#{labels.termLabel}">
												<hx:formItem styleClass="formItem" id="formItemSubscriptionTerms"
													errorText="#{labels.requiredFieldMsg}">
													<h:selectOneRadio disabledClass="selectOneRadio_Disabled"
														enabledClass="selectOneRadio_Enabled"
														styleClass="selectOneRadio usatTermsFontNormal"
														id="radioTermsSelection" layout="pageDirection" required="true"
														tabindex="1" requiredMessage="#{labels.termRequiredErrorMsg}"
														onclick="return usatOE_selectedTermChanged(this, event);"
														validatorMessage="Please choose one of the available terms."
														value="#{renewalHandler.selectedTerm}">
														<hx:inputHelperAssist errorClass="selectOneRadio_Error"
															id="assist1" />
														<f:selectItems
															value="#{customerHandler.currentAccount.renewalOfferTermsSelectItems}" />
													</h:selectOneRadio>

												</hx:formItem>
												<f:facet name="bottom">
													<h:panelGrid styleClass="panelGrid"
														id="gridTermsFormBoxBottomFacetGrid"
														style="margin-bottom: 10px; text-align: left" columns="1"
														cellspacing="0" cellpadding="1"
														rendered="#{customerHandler.currentAccount.showEZPayRenewalText}">

														<h:outputText styleClass="outputText"
															id="textEZPayExplanationMessage"
															style="color: navy; font-family: Arial; font-weight: bold; font-size: 10px"
															value="#{labels.EZPayPlanTextUnderTermsMultiPubRenewal}"
															escape="false"></h:outputText>
														<h:outputText styleClass="outputText" id="textEZPayLearnMore"
															value='<span style="font-family: Arial; font-size: 7pt;">Learn More...</span>'
															escape="false" style="color: #00529b; cursor: pointer"></h:outputText>

													</h:panelGrid>
												</f:facet>
											</hx:panelFormBox>
											<hx:panelFormBox helpPosition="over" labelPosition="left"
												styleClass="panelFormBoxTerms renewalOrderFormInsideGridStyles"
												id="formBoxPaymentHeaderSelection"
												label="#{labels.paymentInfoRenewals}">
											</hx:panelFormBox>
											<hx:panelFormBox
												styleClass="panelFormBoxTerms renewalOrderFormInsideGridStyles"
												id="formBoxPaymentSelection" helpPosition="over"
												labelPosition="left" widthLabel="15">
												<hx:formItem styleClass="formItem"
													rendered="#{customerHandler.currentAccount.showRenewWithCardOnFileOption}"
													id="formItemUseExistingCardItem" escape="false"
													errorText="#{labels.requiredFieldMsg}"
													infoText="Please choose one of these payment options:">
													<h:selectOneRadio disabledClass="selectOneRadio_Disabled"
														enabledClass="selectOneRadio_Enabled" styleClass="selectOneRadio"
														id="radioRenewalPaymentOption" layout="pageDirection"
														value="#{renewalHandler.selectedPaymentOption}"
														onclick="return func_11(this, event);" required="true"
														requiredMessage="#{labels.requiredFieldMsg}">
														<f:selectItem
															itemLabel="Use your existing credit card ending with #{customerHandler.currentAccount.account.creditCardNum}"
															itemValue="current_cc_info" id="selectItem1" />
														<f:selectItem itemLabel="Enter new credit card information"
															itemValue="new_cc_info" id="selectItem2" />
														<hx:inputHelperAssist errorClass="selectOneRadio_Error"
															id="assist2" errorAction="focus" />
													</h:selectOneRadio>
												</hx:formItem>
											</hx:panelFormBox>
											<DIV id="VerifyExistingCCInfoDIV" style="display: none;">
												<hx:panelFormBox helpPosition="right"
													rendered="#{customerHandler.currentAccount.showRenewWithCardOnFileOption}"
													labelPosition="left" styleClass="panelFormBoxOrderEntryForm"
													id="formBoxExistingCardVerifyExpirationForm">
													<hx:formItem styleClass="formItem"
														id="formItemExistingCCExpirationDate"
														label='#{labels.CCExpiration} <span style="color:red">*</span>'>
														<h:selectOneMenu styleClass="selectOneMenu selectOneMenu2"
															id="menuExistingCCExpireMonth" tabindex="72"
															title="Credit Card Expiration Month"
															value="#{renewalHandler.existingCreditCardExpirationMonth}">
															<f:selectItem itemLabel="01" itemValue="01" id="selectItem311" />
															<f:selectItem itemLabel="02" itemValue="02" id="selectItem411" />
															<f:selectItem itemLabel="03" itemValue="03" id="selectItem511" />
															<f:selectItem itemLabel="04" itemValue="04" id="selectItem611" />
															<f:selectItem itemLabel="05" itemValue="05" id="selectItem711" />
															<f:selectItem itemLabel="06" itemValue="06" id="selectItem811" />
															<f:selectItem itemLabel="07" itemValue="07" id="selectItem911" />
															<f:selectItem itemLabel="08" itemValue="08" id="selectItem1011" />
															<f:selectItem itemLabel="09" itemValue="09" id="selectItem1111" />
															<f:selectItem itemLabel="10" itemValue="10" id="selectItem1211" />
															<f:selectItem itemLabel="11" itemValue="11" id="selectItem1311" />
															<f:selectItem itemLabel="12" itemValue="12" id="selectItem1411" />
														</h:selectOneMenu>
														<h:selectOneMenu styleClass="selectOneMenu selectOneMenu2"
															id="menuExistingCCExpireYear" style="margin-left: 5px"
															tabindex="73" title="Credit Card Expiration Year"
															value="#{renewalHandler.existingCreditCardExpirationYear}">
															<f:selectItems
																value="#{renewalHandler.creditCardExpirationYears}"
																id="selectItemsExistingCCExpirationYears" />
														</h:selectOneMenu>
													</hx:formItem>
													<f:facet name="top">
														<h:panelGrid styleClass="panelGrid"
															id="gridExistingCCpayementheader">
															<h:outputText styleClass="outputText" id="text3"
																value="Please check the following:" style="font-weight: bold"></h:outputText>
														</h:panelGrid>
													</f:facet>
												</hx:panelFormBox>
											</DIV>
											<DIV id="NewCCInfoDIV" style="display: none;">
												<hx:panelFormBox helpPosition="right" labelPosition="left"
													styleClass="panelFormBoxOrderEntryForm" id="formBoxPaymentInfo">
													<hx:formItem styleClass="formItem" id="formItemCreditCardNumber"
														label='#{labels.CCNum} <span style="color:red">*</span>'>
														<h:inputText styleClass="inputTextOrderForm"
															id="textCreditCardNumber" tabindex="70"
															title="Credit Card Number"
															value="#{renewalHandler.creditCardNumber}" maxlength="16"
															validatorMessage="Credit Card Number is Not Valid">
															<hx:validateConstraint regex="^[0-9]+$" />
															<f:validateLength minimum="12" maximum="16"></f:validateLength>
															<hx:inputHelperAssist errorClass="inputText_Error"
																validation="true" id="assist6" />
														</h:inputText>
													</hx:formItem>

													<hx:formItem styleClass="formItem" id="formItemCreditCardCVV"
														label='#{labels.CCVerify} <span style="color:red">*</span>'>
														<h:inputText styleClass="inputTextOrderForm" id="textCVV" size="5"
															tabindex="71" title="Credit Card Verification Number"
															value="#{renewalHandler.creditCardVerificationNumber}"
															validatorMessage="Credit Card Verification Number is Not Valid"
															maxlength="4">
															<f:validateLength minimum="3" maximum="4"></f:validateLength>
														</h:inputText>
													&nbsp;&nbsp;&nbsp;
													<h:outputText styleClass="outputText" id="textCVVLearnMore"
															style="margin-left: 10px; color: #00529b; cursor: pointer"
															value='<span style="font-family: Arial; font-size: 7pt;">Learn More...</span>'
															escape="false"></h:outputText>

													</hx:formItem>
													<hx:formItem styleClass="formItem" id="formItemCCExpirationDate"
														label='#{labels.CCExpiration} <span style="color:red">*</span>'>
														<h:selectOneMenu styleClass="selectOneMenu selectOneMenu2"
															id="menuCCExpireMonth" tabindex="72"
															title="Credit Card Expiration Month"
															value="#{renewalHandler.creditCardExpirationMonth}">
															<f:selectItem itemLabel="Month" itemValue="-1" id="selectItem16" />
															<f:selectItem itemLabel="01" itemValue="01" id="selectItem3" />
															<f:selectItem itemLabel="02" itemValue="02" id="selectItem4" />
															<f:selectItem itemLabel="03" itemValue="03" id="selectItem5" />
															<f:selectItem itemLabel="04" itemValue="04" id="selectItem6" />
															<f:selectItem itemLabel="05" itemValue="05" id="selectItem7" />
															<f:selectItem itemLabel="06" itemValue="06" id="selectItem8" />
															<f:selectItem itemLabel="07" itemValue="07" id="selectItem9" />
															<f:selectItem itemLabel="08" itemValue="08" id="selectItem10" />
															<f:selectItem itemLabel="09" itemValue="09" id="selectItem11" />
															<f:selectItem itemLabel="10" itemValue="10" id="selectItem12" />
															<f:selectItem itemLabel="11" itemValue="11" id="selectItem13" />
															<f:selectItem itemLabel="12" itemValue="12" id="selectItem14" />
														</h:selectOneMenu>
														<h:selectOneMenu styleClass="selectOneMenu selectOneMenu2"
															id="menuCCExpireYear" style="margin-left: 5px" tabindex="73"
															title="Credit Card Expiration Year"
															value="#{renewalHandler.creditCardExpirationYear}">
															<f:selectItems
																value="#{renewalHandler.creditCardExpirationYears}"
																id="selectItemsCCExpirationYears" />
														</h:selectOneMenu>
													</hx:formItem>



													<f:facet name="top">
														<h:panelGrid id="panelGridCreditCardImageGrid" width="85%"
															columns="1" cellspacing="1" cellpadding="1"
															style="float: right; text-align: center">

															<hx:jspPanel id="jspPanelCreditCardImages">
																<hx:graphicImageEx styleClass="graphicImageEx" id="imageExAmEx1"
																	value="/images/american_express_logo.gif" hspace="5" border="0"
																	width="42" height="26" style="margin-left: 8px"></hx:graphicImageEx>
																<hx:graphicImageEx styleClass="graphicImageEx"
																	id="imageExDiscover1" value="/images/discover_logo.gif"
																	hspace="5" border="0" width="42" height="26"
																	style="margin-left: 8px"></hx:graphicImageEx>
																<hx:graphicImageEx styleClass="graphicImageEx"
																	id="imageExMasterCard1" value="/images/mc_logo.gif" hspace="5"
																	border="0" width="42" height="26" style="margin-left: 8px"></hx:graphicImageEx>
																<hx:graphicImageEx styleClass="graphicImageEx"
																	id="imageExVisaLogo1" value="/images/visa_logo.gif" hspace="5"
																	border="0" width="42" height="26" style="margin-left: 8px"></hx:graphicImageEx>

															</hx:jspPanel>
														</h:panelGrid>
													</f:facet>
												</hx:panelFormBox>
											</DIV>
											<h:panelGrid styleClass="panelGrid" id="gridOnEZPayGrid"
												cellspacing="5"
												rendered="#{customerHandler.currentAccount.account.onEZPay}">
												<h:outputText styleClass="outputText" id="text4"
													value="#{labels.Renew_CustOnEZPay}" escape="false"></h:outputText>

											</h:panelGrid>
											<h:panelGrid styleClass="panelGrid" id="gridNotOnEZPayGrid"
												cellspacing="5"
												rendered="#{not customerHandler.currentAccount.account.onEZPay}">
												<h:outputText styleClass="outputText" id="text10"
													value="#{labels.renewalNotEZPayInfoText}"></h:outputText>

												<h:panelGrid styleClass="panelGrid orderFormGridStyles"
													id="gridEZPAYRequiredGrid" columns="1" style="display:none;">
													<h:outputText styleClass="outputText" id="textForcedEzPayInfoText"
														value="#{labels.requireEZPayRenwalTextRenewals} " escape="false"></h:outputText>

												</h:panelGrid>
												<h:panelGrid styleClass="panelGrid" id="gridEZPAYOptionsGrid">
													<h:selectOneRadio disabledClass="selectOneRadio_Disabled"
														enabledClass="selectOneRadio_Enabled" styleClass="selectOneRadio"
														id="radioRenewalOptions" layout="pageDirection"
														value="#{renewalHandler.selectedRenewalOption}">
														<f:selectItem escape="false" itemValue="AUTOPAY_PLAN"
															id="selectItem15" itemLabel="#{labels.renewalPaymenetOption1}" />
														<f:selectItem escape="false" itemValue="BILL_ME" id="selectItem17"
															itemLabel="#{labels.renewalPaymenetOption2}" />
													</h:selectOneRadio>
												</h:panelGrid>
											</h:panelGrid>
											<h:panelGrid id="COPPAGrid" cellpadding="4" columns="1"
												styleClass="panelGrid orderFormGridStyles" style="margin-top: 10px">
												<h:outputText styleClass=" outputText coppaText" id="textCOPPAText"
													value="#{labels.OrderEntryCOPPA}"></h:outputText>
											</h:panelGrid>
											<h:panelGrid id="panelGridFormSubmissionGrid" columns="3"
												cellpadding="4" styleClass="orderFormGridStyles">

												<hx:graphicImageEx styleClass="graphicImageEx" id="imageEx3"
													value="/images/1px.gif" hspace="50" width="1" height="1"></hx:graphicImageEx>
												<hx:commandExButton type="submit" value="   Submit   "
													styleClass="orderpageSubmitbutton" id="buttonPlaceOrder"
													tabindex="90" onclick="return func_10(this, event);"
													action="#{pc_Renew.submitRenewalAction}"
													alt="Click Here to Complete Your Order"></hx:commandExButton>

												<hx:jspPanel id="jspPanelGeoTrustPanel">

													<img onclick="javascript: checkoutGuarantee(this, event);"
														style="cursor: pointer; margin-top: 0px;"
														alt="Satisfaction Guaranteed" title="Click for details!"
														src="/images/marketingimages/ckoutClickGuarantee.jpg" hspace="0"
														vspace="5">
												</hx:jspPanel>
												<hx:graphicImageEx styleClass="graphicImageEx" id="imageEx33333"
													value="/images/1px.gif" hspace="50" width="1" height="1"></hx:graphicImageEx>

											</h:panelGrid>
											<hx:jspPanel id="pleaseWaitPanel">
												<div id="formSubmittedOverlayInfo"
													style="display: none; position: relative; z-index: 100000; border-bottom: 0px solid #90a0b8; border-top: 0px solid #90a0b8; border-left: 0px solid #90a0b8; border-right: 0px solid #90a0b8; background: white; background-color: white;">
													<table bgcolor="white">
														<tr>
															<td align="center" valign="middle" nowrap><h2>Please
																	wait while we process your order</h2></td>
															<td align="left"><img id="formSubmissionImage"
																src="/images/squaresAnimated.gif" /></td>
														</tr>
													</table>
												</div>
											</hx:jspPanel>
											<h:panelGrid styleClass="panelGrid" id="gridRenewalDisclaimerGrid"
												cellspacing="5">
												<h:outputText
													styleClass="outputText orderFormInsideGridStyles  footnote"
													id="textEZPayDisclaimer"
													value="#{customerHandler.currentAccount.EZPayDisclaimer}"></h:outputText>
												<%-- 												<h:outputFormat --%>
												<!-- 													styleClass="outputText orderFormInsideGridStyles  footnote" -->
												<%-- 													id="textEZPayDisclaimer" value="#{labels.RenewEZPayDisclaimer}" --%>
												<!-- 													escape="false"> -->
												<%-- 													<f:param --%>
												<%-- 														value="#{customerHandler.currentAccount.account.product.customerServicePhone}" --%>
												<%-- 														id="param111"></f:param> --%>
												<%-- 												</h:outputFormat> --%>
											</h:panelGrid>
											<h:inputHidden
												value="#{customerHandler.currentAccount.cardOnFileProcessing}"
												id="renewWithCardOnFailAvailable"></h:inputHidden>
											<h:inputHidden id="showOverlayPopUp"></h:inputHidden>
										</hx:jspPanel>
									</h:form>
								</div>
								<!-- page content 70 -->
								<div id="pageContent30" style="border-style: none">
									<h:panelGrid styleClass="panelGrid tblStatusBlk"
										id="gridCustomerInfoBorderGrid" cellpadding="0" cellspacing="0">
										<h:panelGrid styleClass="panelGrid" id="gridProductDataGrid"
											columnClasses="panelGridColTopLeftAlignV2,panelGridColTopLeftAlignV2"
											columns="2" cellpadding="2" width="100%" cellspacing="2">
											<f:facet name="header">
												<h:panelGroup id="group2" style="background-color: #4268b4">
													<h:panelGrid styleClass="panelGrid tblStatusBlkTH" id="grid1"
														width="100%" columns="1" cellpadding="0" cellspacing="0">
														<h:outputText id="text2" styleClass="outputText tblStatusBlkTH"
															value="Account Information"></h:outputText>
													</h:panelGrid>
												</h:panelGroup>
											</f:facet>
											<h:outputText id="textProductNameLabel"
												value="#{labels.Publication} :" style="font-weight: bold"></h:outputText>
											<h:outputText styleClass="outputText" id="textProductName"
												value="#{customerHandler.currentAccount.account.product.name}"></h:outputText>
											<h:outputText id="textAccountNumberLabel"
												value="#{labels.accountNumberLabel} :" style="font-weight: bold"></h:outputText>
											<h:outputText styleClass="outputText" id="textAccountNumber"
												value="#{customerHandler.currentAccount.account.accountNumber}"></h:outputText>
										</h:panelGrid>
										<h:panelGrid styleClass="panelGrid" id="gridDeliveryInfoGrid"
											cellpadding="1" cellspacing="1" width="100%">
											<f:facet name="header">
												<h:panelGroup id="group3" style="background-color: #4268b4">
													<h:panelGrid styleClass="panelGrid tblStatusBlkTH"
														id="gridDelInfoHeadGrid" width="100%" columns="1" cellpadding="0"
														cellspacing="0">
														<h:outputText id="textDeliveryInfoHeaderText"
															styleClass="outputText tblStatusBlkTH"
															value="Delivery Information"></h:outputText>
													</h:panelGrid>
												</h:panelGroup>
											</f:facet>
											<h:outputText styleClass="outputText" id="text1"
												value="#{customerHandler.currentAccount.deliveryNameForDisplay}"></h:outputText>
											<h:outputText styleClass="outputText" id="text5"
												value="#{customerHandler.currentAccount.addressLine1ForDisplay}"></h:outputText>
											<h:outputText styleClass="outputText" id="text6"
												value="#{customerHandler.currentAccount.addressLine2ForDisplay}"></h:outputText>
											<h:outputText styleClass="outputText" id="text7"
												value="#{customerHandler.currentAccount.cityStateZipForDisplay}"></h:outputText>
											<h:outputText styleClass="outputText" id="text8"
												value="#{customerHandler.currentAccount.deliveryPhoneForDisplay}"></h:outputText>
											<h:outputText styleClass="outputText" id="text9"
												value="#{customerHandler.currentAccount.numberOfCopiesForRenewalDisplay}"></h:outputText>
										</h:panelGrid>
										<h:panelGrid styleClass="panelGrid" id="gridBillingInfoGrid"
											cellpadding="1" cellspacing="1" width="100%">
											<f:facet name="header">
												<h:panelGroup id="groupBillingInfoHeader3"
													style="background-color: #4268b4">
													<h:panelGrid styleClass="panelGrid tblStatusBlkTH"
														id="gridBillingInfoHeadGrid" width="100%" columns="1"
														cellpadding="0" cellspacing="0">
														<h:outputText id="textBillingInfoHeaderText"
															styleClass="outputText tblStatusBlkTH"
															value="Billing Information"></h:outputText>
													</h:panelGrid>
												</h:panelGroup>
											</f:facet>
											<h:outputText styleClass="outputText" id="textBill1"
												value="#{customerHandler.currentAccount.billingNameForDisplay}"></h:outputText>
											<h:outputText styleClass="outputText" id="textBill5"
												rendered="#{not customerHandler.currentAccount.billingSameAsDelivery}"
												value="#{customerHandler.currentAccount.billingAddressLine1ForDisplay}"></h:outputText>
											<h:outputText styleClass="outputText" id="textBill6"
												rendered="#{not customerHandler.currentAccount.billingSameAsDelivery}"
												value="#{customerHandler.currentAccount.billingAddressLine2ForDisplay}"></h:outputText>
											<h:outputText styleClass="outputText" id="textBill7"
												rendered="#{not customerHandler.currentAccount.billingSameAsDelivery}"
												value="#{customerHandler.currentAccount.billingCityStateZipForDisplay}"></h:outputText>
											<h:outputText styleClass="outputText" id="textBill8"
												rendered="#{not customerHandler.currentAccount.billingSameAsDelivery}"
												value="#{customerHandler.currentAccount.billingPhoneForDisplay}"></h:outputText>
										</h:panelGrid>
										<h:panelGrid styleClass="panelGrid" id="gridRenewalMarketingGrid"
											cellpadding="1" cellspacing="1" width="100%">
											<hx:graphicImageEx styleClass="graphicImageEx"
												id="imageExRenewalMarkeingImg1" value="/images/shim.gif" width="1"
												height="1" style="display: none"></hx:graphicImageEx>
										</h:panelGrid>
									</h:panelGrid>
								</div>

								<!-- EZ Pay learn more -->
								<hx:panelDialog type="modeless" styleClass="panelDialog"
									id="dialogEZPayLearnMore1" for="textEZPayLearnMore"
									relativeTo="textEZPayLearnMore" title="Learn More" align="relative"
									valign="relative">
									<h:panelGrid styleClass="panelGrid" id="gridEZPayLearnMoreGrid2"
										width="300">
										<h:outputFormat styleClass="outputText" id="textEZPayHelpText"
											value="#{labels.EZPayPlanTextUnderTermsMultiPubFAQRenewal}"
											escape="false">
											<f:param
												value="#{customerHandler.currentAccount.account.product.customerServicePhone}"
												id="param1"></f:param>
										</h:outputFormat>
									</h:panelGrid>
									<h:panelGroup id="group1" styleClass="panelDialog_Footer">

										<hx:commandExButton id="button2" styleClass="commandExButton"
											type="submit" value="Close">
											<hx:behavior event="onclick" behaviorAction="hide;stop"
												targetAction="dialogEZPayLearnMore1" id="behaviorEZPay4"></hx:behavior>
										</hx:commandExButton>

									</h:panelGroup>
								</hx:panelDialog>
								<!-- CVV Learn More -->
								<hx:panelDialog type="modeless" styleClass="panelDialog" id="dialogCVV"
									align="relative" valign="relative" for="textCVVLearnMore"
									relativeTo="textCVVLearnMore" title="Learn More">

									<h:panelGrid styleClass="panelGrid" id="gridCVVLearn" width="230">
										<f:facet name="footer">
										</f:facet>
										<f:facet name="header">
											<h:panelGroup styleClass="panelGroup" id="group6666">
												<h:outputText styleClass="outputText" id="text44444"
													value="#{labels.learnMoreCVV}" escape="false"></h:outputText>
											</h:panelGroup>
										</f:facet>

									</h:panelGrid>
									<h:panelGroup id="group5" styleClass="panelDialog_Footer">
										<hx:commandExButton id="button4444" styleClass="commandExButton"
											type="submit" value="Close">
											<hx:behavior event="onclick" behaviorAction="hide;stop"
												id="behavior4444" targetAction="dialogCVV"></hx:behavior>
										</hx:commandExButton>
									</h:panelGroup>
								</hx:panelDialog>
								<!-- Guarantee Window -->
								<div id="checkoutGuaranteeDiv" style="display: none;">
									<div id="templateChatTopDiv">
										<span onclick="checkoutGuaranteeHide();" style="cursor: pointer;">Close
											Window<img src="/theme/themeV3/themeImages/close_14x14.png"
											width="14" height="14" border="0"
											style="margin-left: 5px; margin-right: 5px" /> </span>
									</div>
									<div id="tempateChatImageDiv">
										<img title="Satisfaction Guaranteed!"
											src="/images/marketingimages/ckoutGuarantee.jpg" hspace="0"
											vspace="5">
									</div>

									<div id="checkoutGuaranteeDivHeader">
										<span class="USAT_BlueSubHeader">&nbsp;&nbsp;&nbsp;USA TODAY
											guarantees your satisfaction!</span>
									</div>

									<div id="checkoutGuaranteeTextDiv">
										<span class="outputText"><br>At USA TODAY, we want you to
											be 100% satisfied with your subscription. If you are not satisfied
											for any reason, we will provide you with a refund on all undelivered
											issues remaining in your subscription term. You can cancel your
											subscription by calling <br>1-800-872-0001.</span>
									</div>
								</div>
								<!-- End Guarantee Window -->
								<div id="formSubmittedOverlay" style="display: none;"></div>
							</hx:scriptCollector>
						<%-- /tpl:put --%>
</DIV>
<%-- /tpl:put --%>
				<DIV id="clear"></DIV>
				<DIV id="bluebar">
					<a href="/feedback/feedback.jsp"
						style="color: white; font-weight: bold; text-decoration: none; font-size: 14px"
						title="Contact Us">Contact Us</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;1-800-872-0001&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a
						href="/faq/utfaq.jsp"
						style="color: white; font-weight: bold; text-decoration: none; font-size: 14px"
						title="Frequently Asked Questions">FAQ</a>
				</DIV>
				<%-- tpl:put name="footerMainArea" --%>
			<%-- tpl:put name="footerArea_1" --%><%-- /tpl:put --%>
			<%=trackingBug.getOmnitureHTML() %>
		<%-- /tpl:put --%>
				<DIV id=graybar>
					Copyright
					<script>
						document.write(getCurrentYear());
					</script>
					USA TODAY, a division of <a href="http://www.gannett.com">Gannett
						Co. Inc.</a> <a href="/privacy/privacy.htm">Privacy
						Notice/California Privacy Notice</a>. <a
						href="/privacy/privacy.htm#adchoice">Ad Choices</a>.<br> By
					using this service, you accept our <a href="/service/service.jsp">Terms
						of Service</a>.
				</DIV>
				<DIV id=copyright>
					<P>Offers available to new subscribers only. Home delivery not
						available in some areas. Not valid with any other offer.
						Prepayment required. After the promotional period ends, the
						subscription will continue and be billed monthly at the then
						regular rate, less any applicable credits, unless USA TODAY is
						otherwise notified. Applicable taxes may not be included in the
						rate. If at any time you decide to cancel your subscription, you
						may contact customer service at 1-800-872-0001 and the full amount
						of any balance over $2.00 will be returned.</P>
				</DIV>
				<!--END OF COPYWRITE-->
				<DIV id=footer></DIV>
			</DIV>
			<!--END OF WHITEBOX-->
		</DIV>
		<!--END OF CONTENT-->
	</DIV>
</BODY>
</HTML><%-- /tpl:insert --%>
</f:view><%-- /tpl:insert --%>
<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/subscriptions/renewals/Renew.java" --%><%-- /jsf:pagecode --%>