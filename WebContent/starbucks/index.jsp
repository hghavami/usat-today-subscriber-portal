<%@page import="com.usatoday.util.MobileDeviceDetectionUtil"%>
<%@ page import = "com.usatoday.esub.common.*, com.usatoday.esub.account.*, com.usatoday.businessObjects.util.*, com.usatoday.keys.*"%>
<%@page import="com.usatoday.util.MobileDeviceDetectionUtil"%>
<%@ page
	import="com.usatoday.esub.common.*, com.usatoday.esub.account.*, com.usatoday.businessObjects.util.*, com.usatoday.keys.*"%>
<%
	// Validate the hash from the URL.
	// parameters: URL and secret key
	// return: TRUE - valid hash, FALSE - invalid.

	String secretKey = "";
	StarbuckSDNKey sbySDNKey = null;
	String urlURI = "";
	String timestamp = "";
	String validationHash = "";
	String storeZipcode = "";
	String urlBase = "";
	String latitude = "";
	String longitude = "";
	String address = "";
	String woeid = ""; //Where On Earth ID
	String companyId = ""; 

	try {
		// Parse the URL.
		String userEnteredURL = request.getRequestURL().toString();
		// Get the query string from the URL.
		urlURI = request.getQueryString().toString();

		// Get the timestamp from the query string,
		timestamp = request.getParameter("ts");

		// Get the timestamp from the query string,
		validationHash = request.getParameter("vh");

		// Get store's zip code

		storeZipcode = request.getParameter("zip");

		// Get the base URL
		urlBase = userEnteredURL;
		urlBase.replaceFirst("https", "http");

		// Get the latitude and longitude
		latitude = request.getParameter("lat");
		longitude = request.getParameter("long");

		// Get address and Where On Earth ID 
		address = request.getParameter("addr");
		woeid = request.getParameter("woeid");
		
		// Check whether URL is sent from Starbucks or Yahoo
		companyId = request.getParameter("cmpid");		
		if (companyId == null) {
			companyId = "";		
		}

	} catch (Exception e) {
		System.out.print("Starbucks landing page error:" + e);
	}

	// Fetch the secret key for Starbucks server or cache
	try {
		sbySDNKey = StartbucksSDNKeyProcessor.starbucksSDNGetKey(application, request, response, companyId);
		secretKey = sbySDNKey.getSecretKey();
		application.setAttribute("starbuckEeditionAccessKey", sbySDNKey);
	} catch (Exception e) {
		if ((StarbuckSDNKey) application.getAttribute("starbuckEeditionAccessKey") != null) {
			sbySDNKey = (StarbuckSDNKey) application.getAttribute("starbuckEeditionAccessKey");
			secretKey = sbySDNKey.getSecretKey();
		}
	}

	// Calculate the HMAC SHA1 based on the base URL, timestamp and the secret key.
	// Message Authentication Code. 
	String str = urlBase + "," + timestamp + "," + secretKey;
	// Calculate and return the SHA1 hash.  	
	String mac = com.usatoday.businessObjects.util.Sha1OneWayHash.SHA1(str);

	if (mac.equals(validationHash)) {
		try {
			boolean useAlternate = MobileDeviceDetectionUtil.isMobileDevice(request);

			com.usatoday.business.interfaces.products.ProductIntf product = null;

			Object oProd = application.getAttribute("StarbucksProduct");
			if (oProd == null) {
				product = com.usatoday.businessObjects.products.ProductBO.fetchProductIncludeInactive("EESB");
				application.setAttribute("StarbucksProduct", product);
			} else {
				product = (com.usatoday.business.interfaces.products.ProductIntf) oProd;
			}

			String zipcodeEmail = storeZipcode + "@starbucks.com";
			String eEditionLink = "http://ee.usatoday.com/sdn";
			String eEditionLink1 = com.usatoday.businessObjects.products.utils.ElectronicEditionFulfillmentHelper
					.buildOliveFulfillmentURL(product, zipcodeEmail, eEditionLink, "1000000", useAlternate);
			response.sendRedirect(eEditionLink1);
			System.out.print("Successfully redirected from Starbucks to USA TODAY e-Edition.");
		} catch (Exception e) {
			System.out.print("Starbucks landing page error:" + e);
		}
	} else {
		response.sendRedirect("http://service.usatoday.com/starbucks2");
		System.out.print("Starbucks to USA TODAY e-Edition authorization failed.");
	}

	/*                                                                                                                                                              
	 // Example URL.
	 url = "http://www.wsj.com/home.php?ts=1269870433&lat=37%2E3378&long=-122%2E0425&addr=1685%2D87%20Hollenbeck%20Ave%2E%2C%20Sunnyvale%2C%20CA&zip=94087&WOEID=12797148&vh=e29e5edea90c4977a1ba01f3fab461ba6e7e4be3";

	 // Get the secret key.
	 secretKey = "1ab47bd9e3082";

  if (mac.equals(validationHash)) {
	try {
       	boolean useAlternate = MobileDeviceDetectionUtil.isMobileDevice(request);
	
		com.usatoday.business.interfaces.products.ProductIntf product = null;
		
		Object oProd = application.getAttribute("StarbucksProduct");
		if (oProd == null) {
			product = com.usatoday.businessObjects.products.ProductBO.fetchProductIncludeInactive("EESB"); 
			application.setAttribute("StarbucksProduct", product);
		}
		else {
			product = (com.usatoday.business.interfaces.products.ProductIntf)oProd;
		}
		
		String zipcodeEmail = storeZipcode + "@starbucks.com";
		String eEditionLink = "http://ee.usatoday.com/sdn";
		String eEditionLink1 = com.usatoday.businessObjects.products.utils.ElectronicEditionFulfillmentHelper.buildOliveFulfillmentURL(product, zipcodeEmail, eEditionLink, "1000000", useAlternate);
		response.sendRedirect(eEditionLink1);
		System.out.print("Successfully redirected from Starbucks to USA TODAY e-Edition.");
	} catch (Exception e) {
		System.out.print("Starbucks landing page error:" + e);		
	}
  } else {
		response.sendRedirect("http://service.usatoday.com/starbucks2");
  		System.out.print("Starbucks to USA TODAY e-Edition authorization failed.");
  }

	 if (validateHashFromUrl(url, secretKey)) {
	 echo "hash is valid!\n";
	 } else {
	 echo "hash is invalid!\n";
	 }
	
	 Function getKey() {
	 key = fetchCurrentSecretKeyFromCache();
	 current_time = timestamp(); // Current Unix time.

	 If ((key != NULL) and (key.next_update > current_time)) {
	 return key;
	 }

	 // Fetch new key from 
	 // keys.starbucks.yahoo.com/sdnkeys/v1/getKey.php
	 new_key = getKeyFromSDNKeyWebService();

	 if (new_key != NULL) {
	 storeCurrentSecretKeyToCache(new_key);

	 if ((key != NULL) and (key.expiration > current_time)) {
	 // Store the old value of the secret key so it can be used for
	 // hash validation until it expires (in parallel to the new key).
	 storePreviousSecretKeyToCache(key);
	 }
	 }

	 return new_key;
	 }

	 return key; //The web service request failed return the key from the cache, NULL if nothing existed in the cache.
	 }
	 */
%>

<%@page import="com.usatoday.esub.ncs.handlers.CustomerHandler"%>
<%@page import="com.usatoday.esub.ncs.handlers.CancelsHandler"%>
<%@page import="com.usatoday.esub.cookies.AuthenticationCookieProcessor"%>
<%@page import="com.usatoday.keys.StarbuckSDNKey"%><html>
<head>
<title>USA Today Online Subscriptions - Portal</title>
</head>
<body>
	<p>This page should never be displayed.</p>
</body>
</html>