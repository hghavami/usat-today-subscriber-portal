<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>
<html>
<head>
<title>Terms of Service</title>
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;
	mso-font-charset:0;
	mso-generic-font-family:roman;
	mso-font-pitch:variable;
	mso-font-signature:-1610611985 1107304683 0 0 415 0;}
@font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;
	mso-font-charset:0;
	mso-generic-font-family:swiss;
	mso-font-pitch:variable;
	mso-font-signature:-520081665 -1073717157 41 0 66047 0;}
@font-face
	{font-family:"Arial \(W1\)";
	mso-font-alt:Arial;
	mso-font-charset:0;
	mso-generic-font-family:swiss;
	mso-font-pitch:variable;
	mso-font-signature:0 -2147483648 8 0 511 0;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-parent:"";
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";
	mso-fareast-font-family:"Times New Roman";}
p.MsoHeader, li.MsoHeader, div.MsoHeader
	{mso-style-unhide:no;
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	tab-stops:center 3.0in right 6.0in;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";
	mso-fareast-font-family:"Times New Roman";}
p.MsoFooter, li.MsoFooter, div.MsoFooter
	{mso-style-unhide:no;
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	tab-stops:center 3.0in right 6.0in;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";
	mso-fareast-font-family:"Times New Roman";}
a:link, span.MsoHyperlink
	{mso-style-unhide:no;
	color:#5B748A;
	mso-text-animation:none;
	font-weight:bold;
	text-decoration:none;
	text-underline:none;
	text-decoration:none;
	text-line-through:none;}
a:visited, span.MsoHyperlinkFollowed
	{mso-style-noshow:yes;
	mso-style-priority:99;
	color:purple;
	mso-themecolor:followedhyperlink;
	text-decoration:underline;
	text-underline:single;}
p
	{mso-style-unhide:no;
	mso-margin-top-alt:auto;
	margin-right:0in;
	mso-margin-bottom-alt:auto;
	margin-left:0in;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"Arial","sans-serif";
	mso-fareast-font-family:"Times New Roman";}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{mso-style-noshow:yes;
	mso-style-unhide:no;
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:8.0pt;
	font-family:"Tahoma","sans-serif";
	mso-fareast-font-family:"Times New Roman";}
p.Heading326, li.Heading326, div.Heading326
	{mso-style-name:"Heading 326";
	mso-style-unhide:no;
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	mso-outline-level:4;
	font-size:13.5pt;
	font-family:"Times New Roman","serif";
	mso-fareast-font-family:"Times New Roman";
	color:#442E0D;
	font-weight:bold;}
p.NormalWeb27, li.NormalWeb27, div.NormalWeb27
	{mso-style-name:"Normal \(Web\)27";
	mso-style-unhide:no;
	mso-margin-top-alt:auto;
	margin-right:0in;
	mso-margin-bottom-alt:auto;
	margin-left:0in;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";
	mso-fareast-font-family:"Times New Roman";}
span.DeltaViewInsertion
	{mso-style-name:"DeltaView Insertion";
	mso-style-unhide:no;
	mso-style-parent:"";
	color:blue;
	letter-spacing:0pt;
	text-decoration:underline;
	text-underline:double;}
span.DeltaViewMoveDestination
	{mso-style-name:"DeltaView Move Destination";
	mso-style-unhide:no;
	mso-style-parent:"";
	color:#00C000;
	letter-spacing:0pt;
	text-decoration:underline;
	text-underline:double;}
span.DeltaViewDeletion
	{mso-style-name:"DeltaView Deletion";
	mso-style-unhide:no;
	mso-style-parent:"";
	color:red;
	letter-spacing:0pt;
	text-decoration:line-through;}
p.inside-copy, li.inside-copy, div.inside-copy
	{mso-style-name:inside-copy;
	mso-style-unhide:no;
	mso-margin-top-alt:auto;
	margin-right:0in;
	mso-margin-bottom-alt:auto;
	margin-left:0in;
	line-height:11.25pt;
	mso-pagination:widow-orphan;
	mso-layout-grid-align:none;
	text-autospace:none;
	font-size:9.0pt;
	font-family:"Times New Roman","serif";
	mso-fareast-font-family:"Times New Roman";
	color:black;}
span.GramE
	{mso-style-name:"";
	mso-gram-e:yes;}
-->
</style>
</head>
<body link="#5B748A" vlink=purple style='tab-interval:.5in'>
<table border="0" width="440" cellspacing="0" cellpadding="0">
	<tbody>
		<tr>
			<td>

			<p><img height="63" width="68" src="/images/usa_logo.gif"
				align="top" alt=""></p>
			</td>
			<td valign="top" align="right">

			
			</td>
		</tr>
	</tbody>
</table>
<div class=WordSection1>

<p style='tab-stops:162.1pt'><span style='font-size:12.0pt;color:black'>&nbsp;</span></p>

<p class=MsoNormal><span style='font-family:"Arial","sans-serif";color:black;
display:none;mso-hide:all'>&nbsp;</span></p>

<p class=MsoNormal align=center style='text-align:center'><b><span
style='font-family:"Arial","sans-serif";color:black'><br>
<strong><span style='font-family:"Arial","sans-serif"'> Online Subscriptions and Services
</span></strong></span></b></p>

<p class=MsoNormal align=center style='text-align:center'>
<strong><span style='font-family:"Arial","sans-serif";
  color:black'>USA</span></strong><strong><span
style='font-family:"Arial","sans-serif";color:black'> TODAY Print Newspaper and
e-Newspaper</span></strong></p>

<p class=MsoNormal align=center style='text-align:center'><strong><span
style='font-family:"Arial","sans-serif";color:black'>Terms of Service </span></strong><span
style='font-family:"Arial","sans-serif";color:black'></span></p>

<p class=MsoNormal style='margin-bottom:12.0pt'><span style='font-family:"Arial","sans-serif";
color:black'><br>
Last Updated: December 30, 2010</span></p>

<p class=MsoNormal style='margin-bottom:12.0pt'><span style='font-family:"Arial","sans-serif";
color:black'>These Terms of Service govern your use of the USA TODAY
Online Subscription Site (the &quot;Site&quot;) only and do not govern your use
of other USA TODAY services, such as services offered by USATODAY.com or USA TODAY
EDUCATION.<br>
<br>
The Site is owned and operated by USA TODAY (&quot;we&quot; or
&quot;us&quot;). This Site is for the sale and services of subscriptions to the 
USA TODAY print newspaper and the e-Newspaper, an electronic replica of the print
newspaper.</span></p>

<p class=MsoNormal style='margin-bottom:12.0pt'><span style='font-family:"Arial","sans-serif";
color:black'> By using this Site, you
agree to be bound by these Terms of Service and to use the Site in accordance
with these Terms of Service, our <a
href="/privacy/privacy.htm"><b>Privacy Notice,</b></a>
and any additional terms and conditions that are
referenced herein or that otherwise may apply to specific sections of the Site
or to products and services that we make available to you through the Site (all
of which are deemed part of these Terms of Service). Accessing the Site, in any
manner, whether automated or otherwise, constitutes use of the Site and your
agreement to be bound by these Terms of Service.<br>
<br>
We reserve the right to change these Terms of Service or to impose new
condition on use of the Site, from time to time, in which case we will post the
revised Terms of Service on this website and update the "Last Updated" date to
reflect the date of the changes. By
continuing to use the Site after we post any such changes, you accept the Terms
of Service, as modified.</span></p>

<p class=MsoNormal><span
style='font-family:"Arial","sans-serif";color:black;text-decoration:none;
text-underline:none'>We also reserve the right to deny access to the Site or
any features of the Site to anyone who violates these Terms of Service or who,
in our sole judgment, interferes with the ability of others to enjoy our </span><span style='font-family:"Arial","sans-serif";
color:black;text-decoration:none;text-underline:none'>Site or infringes the
rights of others.</span><span style='font-family:"Arial","sans-serif";
color:black'><br>
<br>
<b>Rights and Restrictions Relating to Site Content</b><br>
<br>
<b>Your Limited Right to Use Site Materials.</b> &nbsp;&nbsp;This Site and all
the materials available on the Site are the property of us and/or our
affiliates or licensors, and are protected by copyright, trademark, and other
intellectual property laws. The Site is provided solely for your personal
noncommercial use. You may not use the Site or the materials available on the
Site in a manner that constitutes an infringement of our rights or that has not
been authorized by us. More specifically, unless explicitly authorized in these
Terms of Service or by the owner of the materials, you many not modify, copy, reproduce,
republish, upload, post, transmit, translate, sell, create derivative works,
exploit, or distribute in any manner or medium (including by email or other
electronic means) any material from the Site. You may, however, from time to
time, download and /or print one copy of individual pages of the Site for your
personal, non-commercial use, provided that you keep intact all copyright and
other proprietary notices. For
information about requesting permission to reproduce or distribute materials
from the Site, please contact us.<br>
<br>
<b>Our Right to Use Materials You Submit or Post. </b>When you submit or post
any material via the Site, you grant us, and anyone authorized by us, a
royalty-free, perpetual, irrevocable, non-exclusive, unrestricted, worldwide
license to use, copy modify, transmit, sell, exploit, create derivative works
from, distribute, and/or publicly perform or display such material, in whole or
in part, in any manner or medium, now known or hereafter developed, for any
purpose that we choose. The foregoing grant shall include the right to exploit
any proprietary rights in such posting or submission, including, but not
limited to, rights under copyright, trademark, service mark or patent laws
under any relevant jurisdiction. Also, in connection with the exercise of such
rights, you grant us, and anyone authorized by us, the right to identify you as
the author of any of your postings or submissions by name, email address or
screen name, as we deem appropriate. You
understand that the technical processing and transmission of the Site,
including content submitted by you, may involve transmissions over various
networks, and may involve changes to the content to conform and adapt it to
technical requirements of connecting networks or devices. You will not receive any compensation of any
kind for the use of any materials submitted by you. You acknowledge and agree that we may
preserve content and materials submitted by you, and may also disclose such
content and materials if required to do so by law or if, in our business
judgment, such preservation or disclosure is reasonably necessary to: (a)
comply with legal process; (b) enforce these Terms of Service; (c) respond to
claims that any content or materials submitted by you violate the rights of
third parties; or (d) protect the rights, property, or personal safety of our
Site, us, our affiliates, our officers, directors, employees, representatives,
our licensors, other users, and/or the public.<br>
<br>
<span class=GramE><b>Limitations on Linking and Framing.</b></span> You are
free to establish a hypertext link to the Site so long as the link does not
state or imply any sponsorship of your website by us or by the Site.
&nbsp;&nbsp;However, you may not, without our prior written permission, frame
or inline link any content of the Site, or incorporate into another website or
other service any of our material, content, or intellectual property of the
Site, the USA TODAY print newspaper, its parent or affiliate companies or any of their
licensors. <br>
</span></p>
<br>
<p class=MsoNormal><span class=GramE><b><span style='font-family:"Arial","sans-serif";
color:black'>Notice of Copyright Infringement.</span></b></span><span
style='font-family:"Arial","sans-serif";color:black'> &nbsp;If you are a
copyright owner who believes your copyrighted material has been reproduced,
posted or distributed via the Site in a manner that constitutes copyright
infringement, please inform our designated copyright agent by sending written
notice by U.S. Mail to Gannett Co., Inc., 7950 Jones Branch Drive, McLean, VA
22107, Attn: M. Faris, or by email to mfaris@gannett.com. Please include the
following information in your written notice: (1) a detailed description of the
copyrighted work that is allegedly infringed upon; (2) a description of the
location of the allegedly infringing material on the Site; (3) your contact
information, including your address, telephone number, and, if available, email
address; (4) a statement by you indicating that you have a good-faith belief
that the allegedly infringing use is not authorized by the copyright owner, its
agent, or the law; (5) a statement by you, made under penalty of perjury,
affirming that the information in your notice is accurate and that you are
authorized to act on the copyright owner's behalf; and (6) an electronic or
physical signature of the copyright owner or someone authorized on the owner's
behalf to assert infringement of copyright and to submit the statement. Please
note that the contact information provided in this paragraph is for suspected
copyright infringement only. Contact information for other matters is provided
elsewhere in these Terms of Service or on the Site.</span></p>

<p class=MsoNormal><span style='font-family:"Arial","sans-serif";color:black'><br>
<b>Online Subscription Purchases</b><br>
<br>
The Site allows you to purchase subscriptions to the various editions of the USA TODAY print
newspaper and e-Newspaper and to obtain other related services. You agree to pay
and to be financially responsible for all purchases, including any applicable
sales tax, made by you, or someone acting on your behalf, through the Site. You
also agree to abide by all purchase or subscription terms set forth in these
Terms of Service or on the online subscription order form you complete. You
agree to use the Site and to purchase services or products through the Site for
legitimate purposes only. You also agree not to make any purchases for
speculative, false or fraudulent purposes or for the purpose of anticipating
demand for a particular product or service. You agree only to purchase goods or
services for yourself or for another person for whom you are legally permitted
to do so. When making a purchase for a third party that requires you to submit
the third party's personal information to us, you represent that you have
obtained the express consent of such third party to provide us with such third
party's personal information.<br>
<br>
<b>Order and Registration Forms</b><br>
<br>
To use certain features of the Site, such as placing
online orders, accessing your account information online, or accessing your
e-Newspaper, you must complete the Site's online order form and/or registration
forms. By completing these forms, you certify that you are 18 years of age or
older. You agree to provide true, accurate, current and complete information
about yourself as prompted by the Service's applicable forms. If we have
reasonable grounds to suspect that such information is untrue, inaccurate, not
current or incomplete, we have the right to suspend or terminate your account
and refuse any and all current or future use of the Service (or any portion
thereof) by you. Our use of any personally identifiable information you provide
to us as part of the online order form or registration process is governed by
the terms of our <a href="/privacy/privacy.htm"><b>Privacy
Notice</b></a>. <br>
<br>
<b style='mso-bidi-font-weight:normal'>Responsibility for your Username and</b>
<b>Password</b><br>
<br>
To use certain features of the Site, you will need a
username and password, which you will receive through the Site's account
registration process. <span style='mso-bidi-font-weight:bold'>We reserve the
right to reject or terminate the use of any username that we deem offensive or
inappropriate. In addition, we also
reserve the right to terminate the use of any username or account, or to deny
access to the Site or any features of the Site, to anyone who violates these
Terms of Service or who, in our sole judgment, interferes with the ability of
others to enjoy our website or infringes the rights of others. You are responsible for maintaining
the confidentiality of the password and account, and are responsible for all
activities (whether by you or by others) that occur under your password,
username, or account. Your responsibilities include the payment of all fees and
charges associated with the use of the Site, whether or not the use is made by
you personally or by someone else using your username, password or account. You
agree to notify us immediately of any unauthorized use of your password,
username, or account or any other breach of security by sending an email to us
via our <a href="/feedback/feedback.jsp"><b>Feedback </b></a>page. You also agree to
ensure that you exit from your account at the end of each session. We cannot
and will not be liable for any loss or damage arising from your failure to
protect your password, username or account information. </span></p>

<p class=Heading326 style='background:white;vertical-align:top'><span
style='font-size:12.0pt;font-family:"Arial","sans-serif";color:black'>&nbsp;</span></p>

<p class=Heading326 style='background:white;vertical-align:top'><span
style='font-size:12.0pt;font-family:"Arial","sans-serif";color:black'>Online
Commerce</span></p>

<p class=NormalWeb27 style='background:white;vertical-align:top'><span
style='font-family:"Arial","sans-serif";color:black'>Certain sections of this
Site may allow you to purchase different types of products and services online
that are provided by third parties. We are not responsible for the quality,
accuracy, timeliness, reliability or any other aspect of these products and
services. If you make a purchase from a merchant on our Site, or on a third-party
website that you have accessed via a link on our Site, the information obtained
during your visit to that merchant's online store or site, and the information
that you give as part of the transaction, such as your credit card number and
contact information, may be collected by both the merchant and us. A merchant
may have privacy and data collection practices that are different from ours. We
have no responsibility or liability for these independent policies. In
addition, when you purchase products or services on or through the Site, you
may be subject to additional terms and conditions that specifically apply to
your purchase or use of such products or services. For more information
regarding a merchant, its online store, its privacy policies, and/or any
additional terms and conditions that may apply, visit that merchant's website
and click on its information links or contact the merchant directly. You
release us and our affiliates from any damages that you incur, and agree not to
assert any claims against us or any of our affiliates, arising from your
purchase or use of any products or services made available by third parties
through the Site.</span></p>

<p class=NormalWeb27 style='background:white;vertical-align:top'><span
style='font-family:"Arial","sans-serif";color:black'>You agree to be
financially responsible for all purchases made by you or someone acting on your
behalf through the Site. You agree to use the Site, and to purchase services or
products through the Site, for legitimate, non-commercial purposes only. You
also agree not to make any purchases for speculative, false or fraudulent
purposes or for the purpose of anticipating demand for a particular product or
service. You agree to only purchase goods or services for yourself or for
another person for whom you are legally permitted to do so. When making a
purchase for a third party that requires you to submit the third party's
personal information to us or a merchant, you represent that you have obtained
the express consent of such third party to provide such third party's personal
information.</span></p>
<br>
<p class=Heading326 style='page-break-after:avoid;background:white;vertical-align:
top'><span style='font-size:12.0pt;font-family:"Arial","sans-serif";color:black'>Limitation
on Use of Company Directories</span></p>

<p class=NormalWeb27 style='page-break-after:avoid;background:white;vertical-align:
top'><span style='font-family:"Arial","sans-serif";color:black'>The information
contained in any company directories that may be provided on the Site is
provided for business lookup purposes and is not to be used for marketing or
telemarketing applications. This information may not be copied or redistributed
and is provided &quot;AS IS&quot; without warranty of any kind. In no event
will we or our suppliers be liable in any way with regard to such information.</span></p>

<p class=NormalWeb27 style='page-break-after:avoid;background:white;vertical-align:
top'><b style='mso-bidi-font-weight:normal'><span style='font-family:"Arial","sans-serif"'>Privacy</span></b></p>

<p class=NormalWeb27 style='page-break-after:avoid;background:white;vertical-align:
top'><span style='font-family:"Arial","sans-serif";color:black'>We respect the
privacy of the users of our Site. Please take a moment to review our <a href="/privacy/privacy.htm">Privacy Notice</a>
.</span></p>
<br>
<p class=NormalWeb27 style='page-break-after:avoid;background:white;vertical-align:
top'><b style='mso-bidi-font-weight:normal'><span style='font-family:"Arial","sans-serif"'>Modifications
to, or Discontinuation of, the Site</span></b></p>

<p style='background:white;vertical-align:top'><span
style='font-size:12.0pt;font-family:"Arial","sans-serif";color:black;
font-weight:normal;mso-bidi-font-weight:bold'>We reserve the right at any time
and from time to time to modify or discontinue, temporarily or permanently, the
Site, or any portion thereof, with or without notice. You agree that we will not be liable to you
or to any third party for any modification, suspension or discontinuance of the
Site or any portion thereof.</span></p>

<p class=MsoNormal style='margin-bottom:12.0pt'><b><span style='font-family:
"Arial","sans-serif";color:black'>Disclaimer</span></b><span style='font-family:
"Arial","sans-serif";color:black'><br>
<br>
Throughout the Site, we have provided links and pointers to Internet sites
maintained by third parties. Our linking to such third party sites does not
imply an endorsement or sponsorship of such sites, or the information, products
or services offered on or through the sites. In addition, neither we nor our
parent or subsidiary companies nor their respective affiliates operate or
control in any respect any information, products or services that third parties
may provide on or through the Service or on websites linked to from the
Service. <br>
<br>
THE INFORMATION, PRODUCTS AND
SERVICES OFFERED ON OR THROUGH THE SITE OR ANY THIRD-PARTY SITES LINKED TO FROM
THIS SITE ARE PROVIDED &quot;AS
IS&quot; AND WITHOUT WARRANTIES OF
ANY KIND, EITHER EXPRESS OR
IMPLIED. TO THE FULLEST EXTENT PERMISSIBLE PURSUANT TO APPLICABLE LAW, WE
DISCLAIM ALL WARRANTIES, EXPRESS
OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE. WE DO NOT WARRANT THAT THE SITE OR ANY OF ITS FUNCTIONS
WILL BE UNINTERRUPTED OR ERROR-FREE, THAT DEFECTS WILL BE CORRECTED, OR THAT
ANY PART OF THIS SITE, OR THE SERVERS THAT MAKE IT AVAILABLE, ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS. <br>
<br>
WE DO NOT WARRANT OR MAKE ANY REPRESENTATIONS REGARDING THE USE OR THE RESULTS
OF THE USE OF THE SITE OR MATERIALS IN THIS SITE, OR IN THIRD-PARTY SITES IN
TERMS OF THEIR CORRECTNESS, ACCURACY, TIMELINESS, RELIABILITY OR OTHERWISE. <br>
<br>
YOU MUST PROVIDE AND ARE SOLELY RESPONSIBLE FOR ALL
HARDWARE AND/OR SOFTWARE NECESSARY
TO ACCESS THE SITE. YOU ASSUME THE ENTIRE COST 
OF AND RESPONSIBILITY FOR ANY
DAMAGE TO, AND ALL NECESSARY MAINTENANCE, REPAIR OR CORRECTION OF,
THAT HARDWARE AND/OR SOFTWARE. <br>
<br>
</span><span style='font-family:"Arial","sans-serif"'>The Site is provided for
informational purposes only, and is not intended for trading or investing
purposes, or for commercial use. <span style='color:black'>THE SITE SHOULD NOT
BE USED IN ANY HIGH RISK ACTIVITIES WHERE DAMAGE OR INJURY TO PERSONS,
PROPERTY, ENVIRONMENT, FINANCES OR BUSINESS MAY
RESULT IF AN ERROR OCCURS. YOU EXPRESSLY ASSUME ALL
RISK FOR SUCH USE.</span></span></p>

<p class=MsoNormal style='margin-bottom:12.0pt'><span style='font-family:"Arial","sans-serif";
color:black;text-decoration:none;text-underline:none'>Your interactions with
companies, organizations and/or individuals found on or through our Site,
including any purchases, transactions, or other dealings, and any terms,
conditions, warranties or representations associated with such dealings, are
solely between you and such companies, organizations and/or individuals. You
agree that we</span><span
style='font-family:"Arial","sans-serif";color:black'> will not be<span style='color:black;
text-decoration:none;text-underline:none'> responsible or</span> liable for any loss</span><span style='font-family:"Arial","sans-serif"'>
or damage <span style='color:black;text-decoration:
none;text-underline:none'>of any sort incurred as the result of any such
dealings. </span> You also agree that, if
there is a dispute between users of this Site, or between a user and any third
party, we are under no obligation to become involved, and you agree to release
us and our affiliates from any claims, demands and damages of every kind or
nature, known or unknown, suspected and unsuspected, disclosed and undisclosed,
arising out of or in any way related to such dispute and/or our Site.</span><span
style='font-family:"Arial","sans-serif";color:black'><br>
<br>
<b>Limitation of Liability</b><br>
<br>
Under no circumstances, including, but not limited to, negligence, will we, or our
subsidiaries, parent companies or affiliates, be liable for any direct,
indirect, incidental, special or consequential damages that result from the use
of, or the inability to use, the Site, including its materials, products, or
services, third party materials, products, or services made available through
the Site, or any part of the Site, even if we are advised beforehand of the
possibility of such damages. (Because some states do not allow the exclusion or
limitation of certain categories of damages, the above limitation may not apply
to you. In such states, our liability, and the liability of our subsidiaries,
parent companies and affiliates, is limited to the fullest extent permitted by
such state law.) You specifically acknowledge and agree that we are not liable
for any defamatory, offensive or illegal conduct of any user. If you are
dissatisfied with the Site, or any materials, <span class=GramE>products,</span>
or services on the Site, or with any of the terms and conditions in these Terms
of Service, your sole and exclusive remedy is to discontinue using the Site.</span></p>

<p class=NormalWeb27 style='background:white;vertical-align:top'><b
style='mso-bidi-font-weight:normal'><span style='font-family:"Arial","sans-serif"'>Indemnification</span></b></p>

<p class=NormalWeb27 style='background:white;vertical-align:top'><span
style='font-family:"Arial","sans-serif"'>You agree to indemnify and hold
harmless us, our affiliates, and each of our and their respective directors,
officers, managers, employees, shareholders, agents, representatives and
licensors, from and against any and all losses, expenses, damages and costs,
including reasonable attorneys' fees, that arise out of your use of the Site,
violation of these Terms of Service by you or any other person using your account,
or your violation of any rights of another. We reserve the right to take over
the exclusive defense of any claim for which we are entitled to indemnification
under this section. In such event, you agree to provide us with <span
style='color:black'>such cooperation as is reasonably requested by us. </span></span></p>

<p class=MsoNormal style='margin-bottom:12.0pt'><b style='mso-bidi-font-weight:
normal'><span style='font-family:"Arial","sans-serif";color:black'>Suspension
and</span></b><span style='font-family:"Arial","sans-serif";color:black'> <b>Termination</b><br>
<br>
Customers may terminate their subscriptions by calling National Customer
Service at 1-800-872-0001 
and speaking to a representative.</span></p>

<p class=MsoNormal><span style='font-family:"Arial","sans-serif";color:black'>You
agree that, in our sole discretion, we may suspend or terminate your password,
account (or any part thereof) or use of the Site, or any part of the Site, and
remove and discard any materials that you submit to the Site, at any time, for
any reason, without notice. In the event of suspension or termination, you are
no longer authorized to access the part of the Site affected by such suspension
or termination. You agree that we will not be liable to you or any third-party
for any suspension or termination of your password, account (or any part
thereof) or use of the Site, or any removal of any materials that you have
submitted to the Site. In the event
that we suspend or terminate your access to and/or use of the Site, you will
continue to be bound by the Terms of Service that were in effect as of the date
of your suspension or termination.  In
addition, you are liable for any fees or other charges you incurred prior to
the termination of your rights to use the Site.</span></p>

<p class=MsoNormal style='page-break-after:avoid'><b><span style='font-family:
"Arial","sans-serif";color:black'>&nbsp;</span></b></p>

<p class=MsoNormal style='mso-pagination:widow-orphan lines-together;
page-break-after:avoid'><b><span style='font-family:"Arial","sans-serif";
color:black'>Other</span></b><span style='font-family:"Arial","sans-serif";
color:black'><br>
<br>
This agreement constitutes the entire agreement
between us and you with respect to the subject matter contained in this
agreement and supersedes all previous and contemporaneous agreements, proposals
and communications, whether written or oral. You also may be subject to
additional terms and conditions that may apply when you use certain products or
services available in or through the Site, including the products and services
of a third party that are provided through the Site. In the event of any
conflict between any such third-party terms and conditions and these Terms of
Service, these Terms of Service will govern. This agreement will be governed by
and construed in accordance with the laws of the State of New York, without giving effect to any principles
or conflicts of law. </span></p>

<p class=MsoNormal style='mso-pagination:widow-orphan lines-together;
page-break-after:avoid'><span style='font-family:"Arial","sans-serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:12.0pt'><span style='font-family:"Arial","sans-serif"'>This
agreement is personal to you and you may not assign it to anyone. If any
provision of this agreement is found to be unlawful, void or for any reason
unenforceable, then that provision will be deemed severable from this agreement
and will not affect the validity and enforceability of any remaining
provisions. These Terms of Service are
not intended to benefit any third party, and do not create any third party
beneficiaries. Accordingly, these Terms
of Service may only be invoked or enforced by you or us. You agree that
regardless of any statute or law to the contrary, any claim or cause of action
that you may have arising out of or related to use of the Site or these Terms
of Service must be filed by you within one year after such claim or cause of
action arose or be forever barred.<a name=privacy></a></span></p>

</div>

</body>

</html>
