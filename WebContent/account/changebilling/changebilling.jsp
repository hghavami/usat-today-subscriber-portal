<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/account/changebilling/ChangeBilling.java" --%><%-- /jsf:pagecode --%>
<%-- tpl:insert page="/theme/themeV3/jsfTheme/subscriberPortalLeftNavAccount_JSF.jtpl" --%><!DOCTYPE HTML><%@page import="com.usatoday.business.interfaces.products.ProductIntf"%>
<%@page import="com.usatoday.businessObjects.products.ProductBO"%>
<%@page import="com.usatoday.util.MobileDeviceDetectionUtil"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%><%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%><%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<f:view><f:loadBundle basename="resourceFile" var="labels" /><%-- tpl:insert page="/theme/subscriptionPortalRoot.htpl" --%><HTML>
<HEAD>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8">
<%-- tpl:put name="headerareaTop" --%><%
	String docProtocol = "https:"; 
	if (!request.isSecure()) {
		docProtocol = "http:";
	}
	// following work around to get around problem with RAD 8.0.2 automated links in templates - APE
	String subcribeJS = "javascript:_subscribeToPub();"; 
	String backIssueJS = "javascript:_shopUSAT();";
	
	// change this to any javascript you want called on page load
	String initFunction = "";
	String dynamicNavigation = "";
	
	com.usatoday.esub.common.WebBug trackingBug = com.usatoday.esub.common.UTCommon.getWebAnalyticsBug();
	try {
		trackingBug.deriveValuesFromRequest(request);
	}
	catch (Exception e) {
		; // ignore
	}

	// not shown if electronic pub   
	String suspendResumeDeliveryNavOption = "<LI><A href=\"/account/vacationholds/holds.faces\">Suspend/Resume delivery</A></LI>";
	String suspendResumeDeliveryMobileOption = "<option value=\"/account/vacationholds/holds.faces\">Suspend delivery</option><option value=\"/account/vacationholds/holds.faces\">Resume delivery</option>";
	
	String reportDeliveryProblemNavOption = "<LI><A href=\"/account/cp/delissue.faces\">Report delivery problem</A></LI>";
	String reportDeliveryProblemMobileOption = "<option value=\"/account/cp/delissue.faces\">Report a problem</option>";
	
	String cancelSubscriptionNavOption = "";
	String cancelSubscriptionMobileOption = "";

	String changeAccountLinkMobile = "";
		
	String pubCode = "";
	String productName = "USA TODAY";
	 
    boolean isEEditionProduct = true;

	com.usatoday.businessObjects.products.promotions.PromotionSet defaultPromotionSet = null;
	com.usatoday.businessObjects.products.promotions.PromotionSet currentOfferPromotionSet = null;
	com.usatoday.business.interfaces.customer.CustomerIntf customer = null;
   	com.usatoday.businessObjects.products.promotions.PromotionSet currentAccountPromotionSet = null;
   		
	com.usatoday.business.interfaces.products.OfferIntf offer = null;   		
	try {
		offer = com.usatoday.esub.common.UTCommon.getCurrentOfferVersion2(request);
		pubCode = com.usatoday.util.constants.UsaTodayConstants.UT_PUBCODE;
		if (offer != null) {
			pubCode = offer.getPubCode();
		}
		// get the default promotion set for the publication		
		defaultPromotionSet = com.usatoday.esub.common.UTCommon.getDefaultPromotionSetForPub(pubCode);
		
		// get the keycode specific promotion set
		
		if (offer != null && offer instanceof com.usatoday.business.interfaces.products.SubscriptionOfferIntf) {
			currentOfferPromotionSet = ((com.usatoday.business.interfaces.products.SubscriptionOfferIntf)offer).getPromotionSet();
		}
		
		if (currentOfferPromotionSet != null) {
			com.usatoday.business.interfaces.products.promotions.HTMLPromotionIntf dPromo = currentOfferPromotionSet.getDynamicNavigationCustServiceHTML();
			if (dPromo != null) {
				dynamicNavigation = dPromo.getPromotionalHTML();
			}
		}
				
		boolean isSignedIn = false;
		customer = (com.usatoday.business.interfaces.customer.CustomerIntf)session.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_CUSTOMER_INFO);
		if (customer != null) {
			isSignedIn = true;
			com.usatoday.business.interfaces.products.SubscriptionProductIntf product = com.usatoday.businessObjects.products.SubscriptionProductBO.getSubscriptionProduct(customer.getCurrentEmailRecord().getPubCode());
			isEEditionProduct = product.isElectronicDelivery(); 
			// This promotion set is based on pub and keycode on the customer account
			currentAccountPromotionSet = com.usatoday.businessObjects.products.promotions.PromotionManager.getInstance().getPromotionsForOffer(customer.getCurrentAccount().getPubCode(), customer.getCurrentAccount().getKeyCode());
			if (isEEditionProduct) {
				suspendResumeDeliveryNavOption = ""; // clear vacation holds option
				suspendResumeDeliveryMobileOption = "";
				reportDeliveryProblemNavOption = ""; // clear deliveyr problem link
				reportDeliveryProblemMobileOption = "";
//				cancelSubscriptionNavOption = "<LI><A href=\"/account/cancels/cancelSubscription.faces\">Cancel subscription</A></LI>"; // show cancels 			
//				cancelSubscriptionMobileOption = "<option value=\"/account/cancels/cancelSubscription.faces\">Cancel subscription</option>"; 
			}
			
			try {
				String brandingPubCode = product.getBrandingPubCode();
				ProductIntf bp = ProductBO.fetchProduct(brandingPubCode);
				productName = bp.getName();
			}
			catch (Exception teee) {
			}							
			
			if (customer.getNumberOfAccounts() > 1) {
				changeAccountLinkMobile = "<option value=\"/account/select_accnt.jsp\">Change Account</option>";
			}
			
		}
				
	} catch (Exception e) {
	
	}
	String mobileCSS = "";
	if (MobileDeviceDetectionUtil.isSmallScreenDevice(request)) {
		mobileCSS = "<LINK href=\"/theme/themeV3/css/smallScreen.css\" rel=\"stylesheet\" type=\"text/css\">";
	}
 %>
<script src="/common/prototype/prototype.js"></script>
<script src="/common/prototype/scriptaculous/scriptaculous.js" type="text/javascript"></script>
<link rel="stylesheet"	href="/theme/themeV3/css/stylesheet_jsfcomponents.css"	type="text/css">
<%-- tpl:put name="headerarea_Top" --%>
<%
	initFunction += "initThisPage(thisObj, thisEvent);";
%>

<title>Change Billing Address</title>
<script src="/common/trimString.js"></script>		
<script type="text/javascript">
function func_1(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'

	// In case customer blanks these out, this will cause the blanks instead of the current values to be used
	if (trim($('formChangeBillingAddress:FirstName').value) == "" ) {
		$('formChangeBillingAddress:FirstName').value = " "; 		
	}
	if (trim($('formChangeBillingAddress:LastName').value) == "" ) {
		$('formChangeBillingAddress:LastName').value = " "; 		
	}
	if (trim($('formChangeBillingAddress:FirmName').value) == "" ) {
		$('formChangeBillingAddress:FirmName').value = " "; 		
	}
	if (trim($('formChangeBillingAddress:StreetAddress').value) == "" ) {
		$('formChangeBillingAddress:StreetAddress').value = " "; 		
	}
	if (trim($('formChangeBillingAddress:AptDeptSuite').value) == "" ) {
		$('formChangeBillingAddress:AptDeptSuite').value = " "; 		
	}
	if (trim($('formChangeBillingAddress:AdditionalAddress1').value) == "" ) {
		$('formChangeBillingAddress:AdditionalAddress1').value = " "; 		
	}
	if (trim($('formChangeBillingAddress:City').value) == "" ) {
		$('formChangeBillingAddress:City').value = " "; 		
	}
	if (trim($('formChangeBillingAddress:menuBillingState').value) == "" ) {
		$('formChangeBillingAddress:menuBillingState').value = " "; 		
	}
	if (trim($('formChangeBillingAddress:Zip').value) == "" ) {
		$('formChangeBillingAddress:Zip').value = " "; 		
	}
/*	if (trim($('formChangeBillingAddress:HomePhoneAreaCode').value) == "" ) {
		$('formChangeBillingAddress:HomePhoneAreaCode').value = " "; 		
	}
	if (trim($('formChangeBillingAddress:HomePhoneExchange').value) == "" ) {
		$('formChangeBillingAddress:HomePhoneExchange').value = " "; 		
	}
	if (trim($('formChangeBillingAddress:HomePhoneExtension').value) == "" ) {
		$('formChangeBillingAddress:HomePhoneExtension').value = " "; 		
	}
*/}

function showPaymentPanelClass() {

  try {

   	var panelCreditCardInformation = $('formChangeBillingAddress:creditCardInfogrid');
	   	panelCreditCardInformation.show();
  }
  catch (err3) {
  }	
}

function hidePaymentPanelClass() {

  try {

   	var panelCreditCardInformation = $('formChangeBillingAddress:creditCardInfogrid');
	   	panelCreditCardInformation.hide();
  }
  catch (err1) {
  }	
}

function func_2(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	try {
		var ezPaySignupCheckbox = $('formChangeBillingAddress:ezPaySignupCheckbox');
		
		if (ezPaySignupCheckbox.checked) {
			$('formChangeBillingAddress:textCreditCardNumber').value = "";
			$('formChangeBillingAddress:menuCCExpireMonth').value = -1;
			$('formChangeBillingAddress:menuCCExpireYear').value = -1;
			showPaymentPanelClass();
		} else {
		
			hidePaymentPanelClass();
		}
	} catch (err4) {
	
	}
}

function initThisPage(thisObj, thisEvent) {

	try {
		func_1(thisObj, thisEvent);	
		hidePaymentPanelClass();
		// Check for EZPay and show CC info
		var isAccountOnEzPay = $('formChangeBillingAddress:isAccountOnEZPay').value;

		if (isAccountOnEzPay == "true") {
			showPaymentPanelClass();		
		} else {
			func_2(thisObj, thisEvent);
		}
	} catch (err2) {
	
	}
}

</script>
	<%-- /tpl:put --%>
<%-- /tpl:put --%>
<LINK REL="SHORTCUT ICON" HREF="/favicon.ico">
<LINK REL="icon" href="/favicon.ico" TYPE="image/ico" />
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META http-equiv="Content-Style-Type" content="text/css">
<meta name="viewport" content="initial-scale=1.0">
<LINK href="/theme/themeV3/css/usatodayBase.css" rel="stylesheet"
	type="text/css">
<LINK href="/theme/themeV3/css/usatodayAddOn.css" rel="stylesheet"
	type="text/css">
<script src="/common/usatCommon.js"></script>
<%-- tpl:put name="headareaBottom" --%>
<%=mobileCSS %>
<%-- tpl:put name="headarea_Bottom" --%>
		<!-- Bottom Header area -->
	<%-- /tpl:put --%>
<script>
// method called on page load
function initPage(thisObj, thisEvent) {
	// insert site-wide code here

	// for product messageing in popUpOverlay.js
	usat_productName = "<%=productName %>";

	<%=initFunction %>
}
</script>
<%-- /tpl:put --%>
<script type="text/javascript">
	/*   var _gaq = _gaq || [];
	 _gaq.push(['_setAccount', 'UA-23158809-1']);
	 _gaq.push(['_trackPageview']);

	 (function() {
	 var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	 ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	 var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	 })();
	 */
</script>
</HEAD>
<body>
	<DIV id="wrapper">
		<DIV id="header">
			<div id="headerLeft" onclick="templateLogoClick();"></div>
			<div id="headerRight">
				<%-- tpl:put name="headerbodyarea" --%>
					<%-- tpl:insert page="/login/loginform.jspf" --%><div style="float: none">
	<%
		String uri = request.getRequestURI();
		String pageName = uri.substring(uri.lastIndexOf("/") + 1);
		String jScript = "";
		if (uri.indexOf("subscriptions") > 0 && uri.indexOf("thankyou.jsp") > 0) {
			try {
				com.usatoday.esub.handlers.ShoppingCartHandler shoppingCartHandler = (com.usatoday.esub.handlers.ShoppingCartHandler) session
						.getAttribute("shoppingCartHandler");
				if (shoppingCartHandler != null) {
					jScript = shoppingCartHandler.getOnePageThankYouJavaScriptTealiumText();
				}
			} catch (Exception E) {

			}
		} else {
			com.usatoday.businessObjects.products.promotions.PromotionSet currentOfferPromotionSetTealium = null;
			com.usatoday.business.interfaces.products.OfferIntf offerTealium = com.usatoday.esub.common.UTCommon
					.getCurrentOffer(request);
			if (offerTealium != null && offerTealium instanceof com.usatoday.business.interfaces.products.SubscriptionOfferIntf) {
				currentOfferPromotionSetTealium = ((com.usatoday.business.interfaces.products.SubscriptionOfferIntf) offerTealium)
						.getPromotionSet();
				jScript = currentOfferPromotionSetTealium.getPromoOnePageJavaScriptTealiumText();
				jScript = org.apache.commons.lang3.StringUtils.replace(jScript, "page_name:\"\"", "page_name:\"" + uri + "\"");
			}
		}
	%>
	<%=jScript%>
</div>
<%-- 			<f:verbatim>
				<h:outputText styleClass="outputText" id="textThankYouJS13"	escape="false" value="#{shoppingCartHandler.tealiumOnePageThankYouJavaScriptText}"></h:outputText>
			</f:verbatim>
 --%>
<%
	String isLoginDisplay = "true";
	com.usatoday.business.interfaces.customer.CustomerIntf customer2 = null;
	String loggedinDivClass = "hideMe";
	String notLoggedinDivClass = "showMeInline";
	String firstName = "";
	String lastName = "";
	String firmInfo = "";
	String accountNum = "";
	String loginErrorMessage2 = "";
	String logOut = "";
	String publication = "";
	String changeAccountLink = "";
	String earlyAlertDisplay = "display:none";

	String jScriptNoOp = "javascript:;";

	try {
		publication = (String) session.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_PUB_NAME);
		if (publication == null) {
			publication = request.getParameter("pub");
			if (publication == null) {
				publication = com.usatoday.util.constants.UsaTodayConstants.UT_PUBCODE;
			}
		}
		if (publication.equals(com.usatoday.esub.common.UTCommon.PUB_NAMES[0])) { // USATODAY publication
			logOut = com.usatoday.esub.common.UTCommon.BASE_URL + "/index.jsp?pub=UT&logsessobj=rem";
		} else {
			logOut = com.usatoday.esub.common.UTCommon.BASE_URL + "/index.jsp?pub=BW&logsessobj=rem";
		}

		// If customer is logged in remove login page, otherwise display it
		customer2 = (com.usatoday.business.interfaces.customer.CustomerIntf) session
				.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_CUSTOMER_INFO);
		if (customer2 == null) {
			// showMeInling and hideMe are defined in style sheets
			loggedinDivClass = "showMeInline";
			notLoggedinDivClass = "hideMe";
		} else {

			if (customer2.getCurrentAccount() != null) {
				firstName = customer2.getCurrentAccount().getDeliveryContact().getFirstName();
				if (firstName != null && !firstName.trim().equals("")) {
					firstName = com.usatoday.esub.common.UTCommon.formatNameCapitalization(firstName); // Format it
				}
				lastName = customer2.getCurrentAccount().getDeliveryContact().getLastName();
				if (lastName != null && !lastName.trim().equals("")) {
					lastName = com.usatoday.esub.common.UTCommon.formatNameCapitalization(lastName); // Format it
				}
				firmInfo = customer2.getCurrentAccount().getDeliveryContact().getFirmName();
				if (customer2.getNumberOfAccounts() > 1) {
					changeAccountLink = "[&nbsp;<a href=\"/account/select_accnt.jsp\">Change</a>&nbsp;]";
				}
				try {
					accountNum = customer2.getCurrentAccount().getAccountNumber();
					//				if (accountNum == null || accountNum.length() < 4) {
					if (accountNum == null) {
						accountNum = "";
					}
					//				else {
					/*
					if (accountNum.equalsIgnoreCase("340000000")) {
						accountNum = "Pending Setup";
					}
					 */
					//				}
				} catch (Exception eeee) {
					eeee.printStackTrace();
				}

				//EarlyAlert - Check to see if their paper delivery has been affected by a delay
				// HD-CONS109 SEW
				/*
				if (customer2.getCurrentAccount().isShowDeliveryAlert()) {
				earlyAlertDisplay = "display:inline";
				}
				 */

			} else {
				if (customer2.getCurrentEmailRecord() != null
						&& customer2.getCurrentEmailRecord().getEmailAddress() != null) {
					firstName = customer2.getCurrentEmailRecord().getEmailAddress();
					accountNum = "Pending Setup";
				} else {
					firstName = "";
				}
			}
		}
		loginErrorMessage2 = (String) session.getAttribute("LOGIN_FAILED_MESSAGE");
		if (loginErrorMessage2 == null || loginErrorMessage2.trim().equals("")) {
			loginErrorMessage2 = "";
		} else {
			session.removeAttribute("LOGIN_FAILED_MESSAGE");
		}
	} catch (Exception e) {
		e.printStackTrace();
		if (firstName == null) {
			firstName = "";
		}
		if (lastName == null) {
			lastName = "";
		}
	}
	String nameInfo = firstName + " " + lastName;
	String loginInfo = null;
	if (!nameInfo.trim().equals("")) {
		loginInfo = nameInfo;
	} else {
		if (firmInfo != null && !firmInfo.trim().equals("")) {
			loginInfo = firmInfo;
		}
	}
	// if no login info, then hide data
	if (loginInfo == null) {
		loggedinDivClass = "showMeInline";
		notLoggedinDivClass = "hideMe";
		loginInfo = "";
	}
%>

<%@page import="com.usatoday.util.constants.UsaTodayConstants"%><div
	id="loginDisplayDiv" class="<%=loggedinDivClass%>">
	<table border="0" width="100%" height="60px">
		<tr>
			<td>
				<TABLE class="loginFormTable">
					<TBODY>
						<TR>
							<TD class="loginFormColumn"><br /> <a
								href="/login/auth.faces">Login to Manage Account</a></TD>
						</TR>
						<tr>
							<td class="loginFormColumn" align="right"><img
								style="cursor: pointer; margin-top: 10px;"
								onclick="javascript: templateClickToChatClickedGuest(this, event);"
								alt="Click for Assistance" title="Click for Assistance"
								src="${pageContext.request.contextPath}/theme/themeV3/themeImages/chatIcon136.png"
								hspace="0" vspace="5"></td>
						</tr>
					</TBODY>
				</TABLE></td>
		</tr>
	</table>
</div>
<div id="loginHideDiv" class="<%=notLoggedinDivClass%>">
	<table width="100%" border="0" class="loginFormTable">
		<tr>
			<td nowrap align="right">
				<TABLE class="loginFormTable">
					<TBODY>
						<TR>
							<TD class="loginFormColumn" nowrap><span
								class="loginNormalPrint">Hello, <%=loginInfo%>&nbsp;&nbsp;[&nbsp;<a
									href="<%=jScriptNoOp%>"
									onclick="javascript:document.location='<%=logOut%>';_unloadPage(this, event);">Logout</a>&nbsp;]</span>
							</TD>
						</TR>
					</TBODY>
				</TABLE></TD>
		</tr>
		<tr>
			<td class="loginFormColumn" align="right"><span
				class="loginfineprint">Account #:&nbsp;<%=accountNum%>&nbsp;<%=changeAccountLink%></span>
			</td>
		</tr>
		<tr>
			<td class="loginFormColumn" align="right"><div
					id="chatIconRequestDiv">
					<img onclick="javascript: templateClickToChatClicked(this, event);"
						style="cursor: pointer; margin-top: 10px;"
						alt="Click for Assistance" title="Click for Assistance"
						src="${pageContext.request.contextPath}/theme/themeV3/themeImages/chatIcon136.png"
						hspace="0" vspace="5">
				</div>
			</td>
		</tr>
	</table>
</div>
<div id="templateChatWindowDiv" style="display: none;">
	<div id="templateChatTopDiv">
		<span onclick="templateClickToChatHide();" style="cursor: pointer;">No
			Thanks<img src="/theme/themeV3/themeImages/close_14x14.png"
			width="14" height="14" border="0"
			style="margin-left: 5px; margin-right: 5px" /> </span>
	</div>
<!-- 	<div id="tempateChatImageDiv">
		<a
			onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
			href=""><img border="0"
			src="https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY" />
		</a>
	</div>
 -->	<div id="templateChatAccountLinksDiv">
		<ul>
			<li>To report a problem with delivery, click <a
				href="/account/cp/delissue.faces"> here</a>.</li>
			<li>To place your delivery on a temporary hold, click <a
				href="/account/vacationholds/holds.faces"> here</a>.</li>
			<li>Forgot your password or login? Click <a
				href="/idpassword/forgotpassword.jsp"> here</a>.
			<li>To contact us <a href="/feedback/feedback.jsp"> click here</a>.</li>				
<!-- 			<li>To cancel your subscription, please call 1-800-872-0001 and -->
<!-- 				select option 6.</li> -->
		</ul>
	</div>

	<div id="templateChatLinksDiv">
		<ul>

			<li><a
				onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
				href="">Click to Chat</a>
			</li>
		</ul>
	</div>
	<div id="templateChatBottomDiv">
		<span class="outputText">Or, call us at:<br> <b>Call
				1-877-713-6241</b><br>(M-F 8:00 a.m. to 7 p.m. E.T.)</span>
	</div>
</div>
<div id="templateChatWindowGuestDiv" style="display: none;">
	<div id="templateChatTopDiv">
		<span onclick="templateClickToChatHideGuest();"
			style="cursor: pointer;">No Thanks<img
			src="/theme/themeV3/themeImages/close_14x14.png" width="14"
			height="14" border="0" style="margin-left: 5px; margin-right: 5px" />
		</span>
	</div>
<!-- 	<div id="tempateChatImageDiv">
		<a
			onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
			href=""><img border="0"
			src="https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY" />
		</a>
	</div>
 -->	<div id="templateChatAccountLinksDiv">
		<ul>
			<li>To manage your account online - including reporting a
				problem with delivery or placing a temporary hold on your account -
				log in by entering your e-mail address and password. Click <a
				href="/login/auth.faces"> here</a> to access the login page now.</li>
			<li>Forgot your password or login? <a
				href="/idpassword/forgotpassword.jsp"> click here</a>.</li>
			<li>To contact us <a href="/feedback/feedback.jsp"> click here</a>.</li>
			<!-- 			<li>To cancel your subscription, please call 1-800-872-0001 and -->
			<!-- 				select option 6.</li> -->
		</ul>

	</div>

	<div id="templateChatLinksDiv">
		<ul>
			<li><a
				onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
				href="">Click to Chat</a>
			</li>
		</ul>
	</div>
	<div id="templateChatBottomDiv">
		<span class="outputText">Or, call us at:<br> <b>Call
				1-877-713-6241</b><br>(M-F 8:00 a.m. to 7 p.m. E.T.)</span>
	</div>
</div>
<%-- /tpl:insert --%>
		<%-- /tpl:put --%>
			</div>
		</DIV>
		<!-- end header -->
		<DIV id="content">
			<DIV id="whitebox">
				<%-- tpl:put name="bodyAreaMainWhiteBox" --%>
<div id="mobileAccountNav">
	<h4>My Account Options:</h4>
	<select name="accountNavLinksSelect" onchange="javascript: mobileNavChanged(this, event);">
		<option value="noop" selected="selected">&nbsp;</option>
	 	<option value="/account/accounthistory/accountSummary.faces">Account Information</option>
		<option value="/custserviceindex.jsp?custopt=RENEW">Pay Bill</option>
		<%=suspendResumeDeliveryMobileOption %>
		<%=reportDeliveryProblemMobileOption %>
		<option value="/custserviceindex.jsp?custopt=DELA">Change delivery info</option>
		<option value="/custserviceindex.jsp?custopt=BILA">Change billing info</option>
		<option value="/idpassword/changeIDpassword.jsp">Change email</option>
		<option value="/idpassword/changeIDpassword.jsp">Change password</option>
		<option value="/welcome.jsp">Give a gift</option>
		<option value="http://onlinestore.usatoday.com">Order back issues</option>
		<%=cancelSubscriptionMobileOption %>
		<%=changeAccountLinkMobile %>
		<option value="<%=logOut%>">Log Out</option>
	</select>
	<hr />	
</div>
	<DIV id=nav>
	<DIV id="navPromo">
		<%-- tpl:put name="navigationArea_Top" --%>

	<%-- /tpl:put --%>
	</div>
	<DIV id=leftNavList>
		<UL>
		  <LI><A href="/account/accounthistory/accountSummary.faces">Account information</A></LI>
		  <LI><A href="/custserviceindex.jsp?custopt=RENEW">Pay Bill</A></LI>
		  <%=suspendResumeDeliveryNavOption %>
		  <%=reportDeliveryProblemNavOption %>
		  <LI><A href="/custserviceindex.jsp?custopt=DELA">Change delivery info</A></LI>
		  <LI><A href="/custserviceindex.jsp?custopt=BILA">Change billing info</A></LI>
		  <LI><A href="/idpassword/changeIDpassword.jsp">Change e-mail/password</A></LI>
		  <LI><A href="<%=subcribeJS%>">Give a gift</A></LI>
		  <LI><A href="<%=backIssueJS%>">Order back issues</A></LI>
		  <%=cancelSubscriptionNavOption %>
		  <%=dynamicNavigation %>
		</UL>
	</DIV>
	<DIV id="navPromo">
		<%-- tpl:put name="navigationArea_Bottom" --%><%-- /tpl:put --%>
	</div>
</DIV>
<DIV id=pageContent>
	<%-- tpl:put name="MainBodyArea" --%>
							<h1>Change Credit Card and Billing Information</h1>
							<hx:scriptCollector id="scriptCollector1"
								preRender="#{pc_ChangeBillingNew.onPageLoadBegin}">
								<h:form styleClass="form" id="formChangeBillingAddress">
									<hx:panelLayout styleClass="panelLayout" id="layout1">
										<f:facet name="body">
											<h:panelGroup styleClass="panelGroup" id="group1">
												<h:panelGrid styleClass="panelGrid" id="creditCardTextEzPay"
													rendered="#{customerHandler.currentAccount.account.onEZPay}">
													<h:outputText styleClass="outputText" id="ezPaytext"
														value="#{labels.EZPayChangeBillingAddressText}"
														escape="false"></h:outputText>
												</h:panelGrid>
												<h:panelGrid styleClass="panelGrid"
													id="creditCardTextNoEzPay"
													rendered="#{!customerHandler.currentAccount.account.onEZPay}">
													<hx:panelFormBox id="formBox3" styleClass="panelFormBox"
														helpPosition="over" labelPosition="right">
														<h:outputText styleClass="outputText" id="nonEzPayText"
															value="#{labels.changeBillingCCInfoText}"></h:outputText>
														<hx:formItem styleClass="formItem"
															id="ezPaySignupFormItem"
															label="#{labels.EZPaySignUpText}">
															<h:selectBooleanCheckbox
																styleClass="selectBooleanCheckbox"
																id="ezPaySignupCheckbox"
																value="#{changeBillingAddressHandler.signupForEzpay}"
																onclick="return func_2(this, event);">
																<hx:behavior event="onselect" id="behavior1"></hx:behavior>
															</h:selectBooleanCheckbox>&nbsp;
											</hx:formItem>
													</hx:panelFormBox>
												</h:panelGrid>
												<h:panelGrid styleClass="panelGrid" id="creditCardInfogrid">
													<h:outputText styleClass="outputText" id="extraLine1">&nbsp;</h:outputText>
													<hx:panelFormBox helpPosition="right" labelPosition="left"
														styleClass="panelFormBox" id="formBoxPaymentInfo"
														label="#{labels.CCInfoLabel}">
														<hx:formItem styleClass="formItem"
															id="formItemCreditCardNumber"
															label='#{labels.CCNum} <span style="color:red">*</span>'>
															<h:inputText styleClass="inputText"
																id="textCreditCardNumber" title="Credit Card Number"
																value="#{changeBillingAddressHandler.creditCardNumber}"
																maxlength="16">
																<f:validateLength maximum="16"></f:validateLength>
																<hx:inputHelperAssist errorClass="inputText_Error"
																	id="assist6"/>
															</h:inputText>
														</hx:formItem>
														<hx:formItem styleClass="formItem"
															id="formItemCCExpirationDate"
															label='#{labels.CCExpiration} <span style="color:red">*</span>'>
															<h:selectOneMenu styleClass="selectOneMenu"
																id="menuCCExpireMonth"
																title="Credit Card Expiration Month"
																value="#{changeBillingAddressHandler.creditCardExpirationMonth}">
																<f:selectItem itemLabel="Month" itemValue="-1"
																	id="selectItem16" />
																<f:selectItem itemLabel="01" itemValue="01"
																	id="selectItem3" />
																<f:selectItem itemLabel="02" itemValue="02"
																	id="selectItem4" />
																<f:selectItem itemLabel="03" itemValue="03"
																	id="selectItem5" />
																<f:selectItem itemLabel="04" itemValue="04"
																	id="selectItem6" />
																<f:selectItem itemLabel="05" itemValue="05"
																	id="selectItem7" />
																<f:selectItem itemLabel="06" itemValue="06"
																	id="selectItem8" />
																<f:selectItem itemLabel="07" itemValue="07"
																	id="selectItem9" />
																<f:selectItem itemLabel="08" itemValue="08"
																	id="selectItem10" />
																<f:selectItem itemLabel="09" itemValue="09"
																	id="selectItem11" />
																<f:selectItem itemLabel="10" itemValue="10"
																	id="selectItem12" />
																<f:selectItem itemLabel="11" itemValue="11"
																	id="selectItem13" />
																<f:selectItem itemLabel="12" itemValue="12"
																	id="selectItem14" />
															</h:selectOneMenu>
															<h:selectOneMenu styleClass="selectOneMenu"
																id="menuCCExpireYear" style="margin-left: 5px"
																title="Credit Card Expiration Year"
																value="#{changeBillingAddressHandler.creditCardExpirationYear}">
																<f:selectItems
																	value="#{currentOfferHandler.creditCardExpirationYears}"
																	id="selectItems1" />
															</h:selectOneMenu>
														</hx:formItem>
														<f:facet name="top">
															<h:panelGrid id="panelGridCreditCardImageGrid"
																width="85%" columns="1" cellspacing="1" cellpadding="1"
																style="float: right; text-align: center">
																<h:outputText styleClass="outputText" id="extraLine3">&nbsp;</h:outputText>
																<hx:jspPanel id="jspPanelCreditCardImages">
																	<hx:graphicImageEx styleClass="graphicImageEx"
																		id="imageExAmEx1"
																		value="/images/american_express_logo.gif" hspace="5"
																		border="0" width="42" height="26"
																		rendered="#{currentOfferHandler.isTakesAMEX}"
																		style="margin-left: 8px"></hx:graphicImageEx>
																	<hx:graphicImageEx styleClass="graphicImageEx"
																		id="imageExDiscover1"
																		value="/images/discover_logo.gif" hspace="5"
																		border="0" width="42" height="26"
																		rendered="#{currentOfferHandler.isTakesDiscover}"
																		style="margin-left: 8px"></hx:graphicImageEx>
																	<hx:graphicImageEx styleClass="graphicImageEx"
																		id="imageExMasterCard1" value="/images/mc_logo.gif"
																		hspace="5" border="0" width="42" height="26"
																		rendered="#{currentOfferHandler.isTakesMasterCard}"
																		style="margin-left: 8px"></hx:graphicImageEx>
																	<hx:graphicImageEx styleClass="graphicImageEx"
																		id="imageExVisaLogo1" value="/images/visa_logo.gif"
																		hspace="5" border="0" width="42" height="26"
																		rendered="#{currentOfferHandler.isTakesVisa}"
																		style="margin-left: 8px"></hx:graphicImageEx>

																</hx:jspPanel>
															</h:panelGrid>
														</f:facet>
													</hx:panelFormBox>
													<h:outputText styleClass="outputText"
														id="creditCardPaymentInfotext"
														value="#{labels.CCPaymentInfoText}" escape="false">
													</h:outputText>
												</h:panelGrid>
												<hx:jspPanel id="jspPanel3">
													<h:panelGrid styleClass="panelGrid" id="grid5" width="600">
														<h:outputText styleClass="outputText" id="extraLine2">&nbsp;
											</h:outputText>
														<hx:panelFormBox id="formboxBillingInformation"
															styleClass="panelFormBox" labelPosition="left"
															widthLabel="155" helpPosition="under"
															label="#{labels.BillingAddressLabel}">
															<h:outputText styleClass="outputText" id="extraLine4">&nbsp;</h:outputText>
															<hx:formItem styleClass="formItem"
																id="formAccountNumberLabel"
																label="#{labels.accountNumberLabel} #{labels.colonLabel} &nbsp;&nbsp;">
																<h:outputText styleClass="outputText" id="AccountNumber"
																	value="#{customerHandler.currentAccount.account.accountNumber}">
																</h:outputText>
															</hx:formItem>
															<hx:formItem styleClass="formItem"
																id="formFirstNameLabel"
																label="#{labels.firstNameLabel} #{labels.colonLabel} #{labels.requiredLabel}" errorText="#{labels.firstNameRequired}">
																<h:inputText styleClass="inputText" id="FirstName"
																	value="#{changeBillingAddressHandler.firstName}"
																	onchange="return func_1(this, event);" size="20" tabindex="1"
																	maxlength="15">
																	<f:validateLength maximum="15"></f:validateLength>
																	<hx:inputHelperAssist errorClass="inputText_Error"
																		autoTab="true" id="assist2" />
																</h:inputText>
															</hx:formItem>
															<hx:formItem styleClass="formItem" id="formLastNameLabel"
																label="#{labels.lastNameLabel}  #{labels.colonLabel} #{labels.requiredLabel}" errorText="#{labels.lastNameRequired}">
																<h:inputText styleClass="inputText" id="LastName"
																	value="#{changeBillingAddressHandler.lastName}"
																	onchange="return func_1(this, event);" size="20" tabindex="2"
																	maxlength="20">
																	<f:validateLength maximum="20"></f:validateLength>
																	<hx:inputHelperAssist errorClass="inputText_Error"
																		autoTab="true" id="assist3" />
																</h:inputText>
															</hx:formItem>
															<hx:formItem styleClass="formItem" id="formFirmNameLabel"
																label="#{labels.companyNameLabel} #{labels.colonLabel} &nbsp;&nbsp;">
																<h:inputText styleClass="inputText" id="FirmName"
																	value="#{changeBillingAddressHandler.firmName}"
																	onchange="return func_1(this, event);" size="35" tabindex="3"
																	maxlength="40">
																	<f:validateLength maximum="40"></f:validateLength>
																	<hx:inputHelperAssist errorClass="inputText_Error"
																		autoTab="true" id="assist4" />
																</h:inputText>
															</hx:formItem>
															<hx:formItem styleClass="formItem"
																id="formStreetAddressLabel"
																label="#{labels.StreetAddress}  #{labels.colonLabel} #{labels.requiredLabel}" errorText="#{labels.address1Required}">
																<h:inputText styleClass="inputText" id="StreetAddress"
																	value="#{changeBillingAddressHandler.streetAddress}"
																	validatorMessage="#{labels.addressInvalid}"
																	onchange="return func_1(this, event);" size="35"
																	tabindex="4" maxlength="28">
																	<f:validateLength maximum="28"></f:validateLength>
																	<hx:inputHelperAssist errorClass="inputText_Error"
																		autoTab="true" id="assist7" />
																</h:inputText>
															</hx:formItem>
															<hx:formItem styleClass="formItem" id="formAptSuiteLabel"
																label="#{labels.apartmentSuiteLabel} #{labels.colonLabel} &nbsp;&nbsp;">
																<h:inputText styleClass="inputText" id="AptDeptSuite"
																	value="#{changeBillingAddressHandler.aptDeptSuite}"
																	onchange="return func_1(this, event);" size="35"
																	tabindex="5" maxlength="28">
																	<f:validateLength maximum="28"></f:validateLength>
																	<hx:inputHelperAssist errorClass="inputText_Error"
																		autoTab="true" id="assist10" />
																</h:inputText>
															</hx:formItem>
															<hx:formItem styleClass="formItem" id="formAddlAddrLabel"
																label="#{labels.additionalAddress} #{labels.colonLabel} &nbsp;&nbsp;">
																<h:inputText styleClass="inputText"
																	id="AdditionalAddress1"
																	value="#{changeBillingAddressHandler.addlAddr1}"
																	onchange="return func_1(this, event);" size="35"
																	tabindex="6" maxlength="28">
																	<f:validateLength maximum="28"></f:validateLength>
																	<hx:inputHelperAssist errorClass="inputText_Error"
																		autoTab="true" id="assist11" />
																</h:inputText>
															</hx:formItem>
															<hx:formItem styleClass="formItem" id="formCityLabel"
																label="#{labels.city} #{labels.colonLabel} #{labels.requiredLabel}"
																errorText="#{labels.cityRequired}">
																<h:inputText styleClass="inputText" id="City"
																	value="#{changeBillingAddressHandler.city}"
																	onchange="return func_1(this, event);" size="35"
																	tabindex="7" maxlength="28">
																	<f:validateLength maximum="28"></f:validateLength>
																	<hx:inputHelperAssist errorClass="inputText_Error"
																		autoTab="true" id="assist12" />
																</h:inputText>
															</hx:formItem>
															<hx:formItem styleClass="formItem" id="formStateLabel"
																label="#{labels.State} #{labels.colonLabel} #{labels.requiredLabel}"
																errorText="#{labels.stateRequired}">
																<h:selectOneMenu styleClass="selectOneMenu"
																	id="menuBillingState"
																	value="#{changeBillingAddressHandler.state}"
																	onchange="return func_1(this, event);" tabindex="8">
																	<f:selectItem itemLabel="" />
																	<f:selectItem itemValue="AL" itemLabel="Alabama" />
																	<f:selectItem itemValue="AK" itemLabel="Alaska" />
																	<f:selectItem itemValue="AZ" itemLabel="Arizona" />
																	<f:selectItem itemValue="AR" itemLabel="Arkansas" />
																	<f:selectItem itemValue="AA"
																		itemLabel="(AA) Armed Forces Americas" />
																	<f:selectItem itemValue="AE"
																		itemLabel="(AE) Armed Forces Africa" />
																	<f:selectItem itemValue="AE"
																		itemLabel="(AE) Armed Forces Canada" />
																	<f:selectItem itemValue="AE"
																		itemLabel="(AE) Armed Forces Europe" />
																	<f:selectItem itemValue="AE"
																		itemLabel="(AE) Armed Forces Middle East" />
																	<f:selectItem itemValue="AP"
																		itemLabel="(AP) Armed Forces Pacific" />
																	<f:selectItem itemValue="CA" itemLabel="California" />
																	<f:selectItem itemValue="CO" itemLabel="Colorado" />
																	<f:selectItem itemValue="CT" itemLabel="Connecticut" />
																	<f:selectItem itemValue="DE" itemLabel="Delaware" />
																	<f:selectItem itemValue="DC"
																		itemLabel="District of Columbia" />
																	<f:selectItem itemValue="FL" itemLabel="Florida" />
																	<f:selectItem itemValue="GA" itemLabel="Georgia" />
																	<f:selectItem itemValue="HI" itemLabel="Hawaii" />
																	<f:selectItem itemValue="ID" itemLabel="Idaho" />
																	<f:selectItem itemValue="IL" itemLabel="Illinois" />
																	<f:selectItem itemValue="IN" itemLabel="Indiana" />
																	<f:selectItem itemValue="IA" itemLabel="Iowa" />
																	<f:selectItem itemValue="KS" itemLabel="Kansas" />
																	<f:selectItem itemValue="KY" itemLabel="Kentucky" />
																	<f:selectItem itemValue="LA" itemLabel="Louisiana" />
																	<f:selectItem itemValue="ME" itemLabel="Maine" />
																	<f:selectItem itemValue="MD" itemLabel="Maryland" />
																	<f:selectItem itemValue="MA" itemLabel="Massachusetts" />
																	<f:selectItem itemValue="MI" itemLabel="Michigan" />
																	<f:selectItem itemValue="MN" itemLabel="Minnesota" />
																	<f:selectItem itemValue="MS" itemLabel="Mississippi" />
																	<f:selectItem itemValue="MO" itemLabel="Missouri" />
																	<f:selectItem itemValue="MT" itemLabel="Montana" />
																	<f:selectItem itemValue="NE" itemLabel="Nebraska" />
																	<f:selectItem itemValue="NV" itemLabel="Nevada" />
																	<f:selectItem itemValue="NH" itemLabel="New Hampshire" />
																	<f:selectItem itemValue="NJ" itemLabel="New Jersey" />
																	<f:selectItem itemValue="NM" itemLabel="New Mexico" />
																	<f:selectItem itemValue="NY" itemLabel="New York" />
																	<f:selectItem itemValue="NC" itemLabel="North Carolina" />
																	<f:selectItem itemValue="ND" itemLabel="North Dakota" />
																	<f:selectItem itemValue="OH" itemLabel="Ohio" />
																	<f:selectItem itemValue="OK" itemLabel="Oklahoma" />
																	<f:selectItem itemValue="OR" itemLabel="Oregon" />
																	<f:selectItem itemValue="PA" itemLabel="Pennsylvania" />
																	<f:selectItem itemValue="PR" itemLabel="Puerto Rico" />
																	<f:selectItem itemValue="RI" itemLabel="Rhode Island" />
																	<f:selectItem itemValue="SC" itemLabel="South Carolina" />
																	<f:selectItem itemValue="SD" itemLabel="South Dakota" />
																	<f:selectItem itemValue="TN" itemLabel="Tennessee" />
																	<f:selectItem itemValue="TX" itemLabel="Texas" />
																	<f:selectItem itemValue="UT" itemLabel="Utah" />
																	<f:selectItem itemValue="VT" itemLabel="Vermont" />
																	<f:selectItem itemValue="VA" itemLabel="Virginia" />
																	<f:selectItem itemValue="WA" itemLabel="Washington" />
																	<f:selectItem itemValue="WV" itemLabel="West Virginia" />
																	<f:selectItem itemValue="WI" itemLabel="Wisconsin" />
																	<f:selectItem itemValue="WY" itemLabel="Wyoming" />
																</h:selectOneMenu>
															</hx:formItem>
															<hx:formItem styleClass="formItem" id="formZipLabel"
																label="#{labels.ZipLabel} #{labels.colonLabel} #{labels.requiredLabel}"
																errorText="#{labels.zipInvalid}">
																<h:inputText styleClass="inputText" id="Zip"
																	value="#{changeBillingAddressHandler.zip}"
																	onchange="return func_1(this, event);" tabindex="9"
																	size="6" maxlength="5">
																	<f:validateLength maximum="5"></f:validateLength>
																	<hx:inputHelperAssist errorClass="inputText_Error"
																		autoTab="true" id="assist1" />
																</h:inputText>
															</hx:formItem>
															<hx:formItem styleClass="formItem" id="formHomePhone"
																label="#{labels.billPhoneLabel} #{labels.colonLabel} #{labels.requiredLabel}"
																errorText="#{labels.invalidPhoneFormat}">
																<h:inputText styleClass="inputText" id="HomePhoneAreaCode"
																	value="#{changeBillingAddressHandler.homePhoneAreaCode}"
																	size="4" onchange="return func_1(this, event);" tabindex="10"
																	maxlength="3">
																	<hx:inputHelperAssist errorClass="inputText_Error"
																		autoTab="true" id="assist5" />
																</h:inputText>
																<h:inputText styleClass="inputText" id="HomePhoneExchange"
																	value="#{changeBillingAddressHandler.homePhoneExchange}"
																	size="4" maxlength="3" onchange="return func_1(this, event);"
																	tabindex="11">
																	<hx:inputHelperAssist errorClass="inputText_Error"
																		autoTab="true" id="assist8" />
																</h:inputText>
																<h:inputText styleClass="inputText" id="HomePhoneExtension"
																	size="5"
																	value="#{changeBillingAddressHandler.homePhoneExtension}"
																	tabindex="12" onchange="return func_1(this, event);"
																	maxlength="4">
																	<hx:inputHelperAssist errorClass="inputText_Error" id="assist9" />
																</h:inputText>
															</hx:formItem>

															<f:facet name="top"></f:facet>
															<f:facet name="bottom"></f:facet>
														</hx:panelFormBox>

														<h:panelGrid styleClass="panelGrid" id="grid14"
															columns="1">
															<h:outputText styleClass="outputText" id="RequiredText1"
																value="* Denotes a required field."></h:outputText>
														</h:panelGrid>
													</h:panelGrid>
												</hx:jspPanel>
												<h:panelGrid styleClass="panelGrid" id="grid2">
													<hx:commandExButton type="submit" value="Continue"
														styleClass="commandExButton" id="continueButton"
														action="#{pc_ChangeBillingNew.doButtonSubmitChangeBillingAddressAction}" tabindex="13"></hx:commandExButton>

												</h:panelGrid>
												<h:inputHidden id="isAccountOnEZPay"
													value="#{changeBillingAddressHandler.onEzpay}"></h:inputHidden>
											</h:panelGroup>
										</f:facet>
										<f:facet name="left"></f:facet>
										<f:facet name="right"></f:facet>
										<f:facet name="bottom"></f:facet>
										<f:facet name="top">
											<h:messages styleClass="messages" id="messagesAllMesssages"
												layout="list"></h:messages>
										</f:facet>
									</hx:panelLayout>
								</h:form>
							</hx:scriptCollector>
						<%-- /tpl:put --%>
</DIV>
<%-- /tpl:put --%>
				<DIV id="clear"></DIV>
				<DIV id="bluebar">
					<a href="/feedback/feedback.jsp"
						style="color: white; font-weight: bold; text-decoration: none; font-size: 14px"
						title="Contact Us">Contact Us</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;1-800-872-0001&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a
						href="/faq/utfaq.jsp"
						style="color: white; font-weight: bold; text-decoration: none; font-size: 14px"
						title="Frequently Asked Questions">FAQ</a>
				</DIV>
				<%-- tpl:put name="footerMainArea" --%>
			<%-- tpl:put name="footerArea_1" --%><%-- /tpl:put --%>
			<%=trackingBug.getOmnitureHTML() %>
		<%-- /tpl:put --%>
				<DIV id=graybar>
					Copyright
					<script>
						document.write(getCurrentYear());
					</script>
					USA TODAY, a division of <a href="http://www.gannett.com">Gannett
						Co. Inc.</a> <a href="/privacy/privacy.htm">Privacy
						Notice/California Privacy Notice</a>. <a
						href="/privacy/privacy.htm#adchoice">Ad Choices</a>.<br> By
					using this service, you accept our <a href="/service/service.jsp">Terms
						of Service</a>.
				</DIV>
				<DIV id=copyright>
					<P>Offers available to new subscribers only. Home delivery not
						available in some areas. Not valid with any other offer.
						Prepayment required. After the promotional period ends, the
						subscription will continue and be billed monthly at the then
						regular rate, less any applicable credits, unless USA TODAY is
						otherwise notified. Applicable taxes may not be included in the
						rate. If at any time you decide to cancel your subscription, you
						may contact customer service at 1-800-872-0001 and the full amount
						of any balance over $2.00 will be returned.</P>
				</DIV>
				<!--END OF COPYWRITE-->
				<DIV id=footer></DIV>
			</DIV>
			<!--END OF WHITEBOX-->
		</DIV>
		<!--END OF CONTENT-->
	</DIV>
</BODY>
</HTML><%-- /tpl:insert --%>
</f:view><%-- /tpl:insert --%>