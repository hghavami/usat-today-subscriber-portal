<%-- tpl:insert page="/theme/themeV3/jsfTheme/subscriberPortalLeftNavAccount_JSF.jtpl" --%><!DOCTYPE HTML><%@page import="com.usatoday.business.interfaces.products.ProductIntf"%>
<%@page import="com.usatoday.businessObjects.products.ProductBO"%>
<%@page import="com.usatoday.util.MobileDeviceDetectionUtil"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%><%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%><%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<f:view><f:loadBundle basename="resourceFile" var="labels" /><%-- tpl:insert page="/theme/subscriptionPortalRoot.htpl" --%><HTML>
<HEAD>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8">
<%-- tpl:put name="headerareaTop" --%><%
	String docProtocol = "https:"; 
	if (!request.isSecure()) {
		docProtocol = "http:";
	}
	// following work around to get around problem with RAD 8.0.2 automated links in templates - APE
	String subcribeJS = "javascript:_subscribeToPub();"; 
	String backIssueJS = "javascript:_shopUSAT();";
	
	// change this to any javascript you want called on page load
	String initFunction = "";
	String dynamicNavigation = "";
	
	com.usatoday.esub.common.WebBug trackingBug = com.usatoday.esub.common.UTCommon.getWebAnalyticsBug();
	try {
		trackingBug.deriveValuesFromRequest(request);
	}
	catch (Exception e) {
		; // ignore
	}

	// not shown if electronic pub   
	String suspendResumeDeliveryNavOption = "<LI><A href=\"/account/vacationholds/holds.faces\">Suspend/Resume delivery</A></LI>";
	String suspendResumeDeliveryMobileOption = "<option value=\"/account/vacationholds/holds.faces\">Suspend delivery</option><option value=\"/account/vacationholds/holds.faces\">Resume delivery</option>";
	
	String reportDeliveryProblemNavOption = "<LI><A href=\"/account/cp/delissue.faces\">Report delivery problem</A></LI>";
	String reportDeliveryProblemMobileOption = "<option value=\"/account/cp/delissue.faces\">Report a problem</option>";
	
	String cancelSubscriptionNavOption = "";
	String cancelSubscriptionMobileOption = "";

	String changeAccountLinkMobile = "";
		
	String pubCode = "";
	String productName = "USA TODAY";
	 
    boolean isEEditionProduct = true;

	com.usatoday.businessObjects.products.promotions.PromotionSet defaultPromotionSet = null;
	com.usatoday.businessObjects.products.promotions.PromotionSet currentOfferPromotionSet = null;
	com.usatoday.business.interfaces.customer.CustomerIntf customer = null;
   	com.usatoday.businessObjects.products.promotions.PromotionSet currentAccountPromotionSet = null;
   		
	com.usatoday.business.interfaces.products.OfferIntf offer = null;   		
	try {
		offer = com.usatoday.esub.common.UTCommon.getCurrentOfferVersion2(request);
		pubCode = com.usatoday.util.constants.UsaTodayConstants.UT_PUBCODE;
		if (offer != null) {
			pubCode = offer.getPubCode();
		}
		// get the default promotion set for the publication		
		defaultPromotionSet = com.usatoday.esub.common.UTCommon.getDefaultPromotionSetForPub(pubCode);
		
		// get the keycode specific promotion set
		
		if (offer != null && offer instanceof com.usatoday.business.interfaces.products.SubscriptionOfferIntf) {
			currentOfferPromotionSet = ((com.usatoday.business.interfaces.products.SubscriptionOfferIntf)offer).getPromotionSet();
		}
		
		if (currentOfferPromotionSet != null) {
			com.usatoday.business.interfaces.products.promotions.HTMLPromotionIntf dPromo = currentOfferPromotionSet.getDynamicNavigationCustServiceHTML();
			if (dPromo != null) {
				dynamicNavigation = dPromo.getPromotionalHTML();
			}
		}
				
		boolean isSignedIn = false;
		customer = (com.usatoday.business.interfaces.customer.CustomerIntf)session.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_CUSTOMER_INFO);
		if (customer != null) {
			isSignedIn = true;
			com.usatoday.business.interfaces.products.SubscriptionProductIntf product = com.usatoday.businessObjects.products.SubscriptionProductBO.getSubscriptionProduct(customer.getCurrentEmailRecord().getPubCode());
			isEEditionProduct = product.isElectronicDelivery(); 
			// This promotion set is based on pub and keycode on the customer account
			currentAccountPromotionSet = com.usatoday.businessObjects.products.promotions.PromotionManager.getInstance().getPromotionsForOffer(customer.getCurrentAccount().getPubCode(), customer.getCurrentAccount().getKeyCode());
			if (isEEditionProduct) {
				suspendResumeDeliveryNavOption = ""; // clear vacation holds option
				suspendResumeDeliveryMobileOption = "";
				reportDeliveryProblemNavOption = ""; // clear deliveyr problem link
				reportDeliveryProblemMobileOption = "";
//				cancelSubscriptionNavOption = "<LI><A href=\"/account/cancels/cancelSubscription.faces\">Cancel subscription</A></LI>"; // show cancels 			
//				cancelSubscriptionMobileOption = "<option value=\"/account/cancels/cancelSubscription.faces\">Cancel subscription</option>"; 
			}
			
			try {
				String brandingPubCode = product.getBrandingPubCode();
				ProductIntf bp = ProductBO.fetchProduct(brandingPubCode);
				productName = bp.getName();
			}
			catch (Exception teee) {
			}							
			
			if (customer.getNumberOfAccounts() > 1) {
				changeAccountLinkMobile = "<option value=\"/account/select_accnt.jsp\">Change Account</option>";
			}
			
		}
				
	} catch (Exception e) {
	
	}
	String mobileCSS = "";
	if (MobileDeviceDetectionUtil.isSmallScreenDevice(request)) {
		mobileCSS = "<LINK href=\"/theme/themeV3/css/smallScreen.css\" rel=\"stylesheet\" type=\"text/css\">";
	}
 %>
<script src="/common/prototype/prototype.js"></script>
<script src="/common/prototype/scriptaculous/scriptaculous.js" type="text/javascript"></script>
<link rel="stylesheet"	href="/theme/themeV3/css/stylesheet_jsfcomponents.css"	type="text/css">
<%-- tpl:put name="headerarea_Top" --%><title>USA TODAY Subscription - Account Information</title><%-- /tpl:put --%>
<%-- /tpl:put --%>
<LINK REL="SHORTCUT ICON" HREF="/favicon.ico">
<LINK REL="icon" href="/favicon.ico" TYPE="image/ico" />
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META http-equiv="Content-Style-Type" content="text/css">
<meta name="viewport" content="initial-scale=1.0">
<LINK href="/theme/themeV3/css/usatodayBase.css" rel="stylesheet"
	type="text/css">
<LINK href="/theme/themeV3/css/usatodayAddOn.css" rel="stylesheet"
	type="text/css">
<script src="/common/usatCommon.js"></script>
<%-- tpl:put name="headareaBottom" --%>
<%=mobileCSS %>
<%-- tpl:put name="headarea_Bottom" --%>
<%
	// Remove any change delivery address handlers	
	try {
		session.removeAttribute("changeDeliveryAddressHandler");
	} catch (Exception e) {
	
	}
	
	// Remove any change billing address handlers	
	try {
		session.removeAttribute("changeBillingAddressHandler");
	} catch (Exception e) {
	
	}
	
	

	//begin EarlyAlert - control popup behavior.
	//String earlyAlertDisplay = "display:none";
	/*
	String earlyAlertDisplay = "display:none";
	if (customer.getCurrentAccount.showDeliveryAlert()) {
     earlyAlertDisplay = "display:inline";
	}
	*/
	
	/*
	String earlyAlertDisplay ="";
	try {
		
		String earlyAlertSession = (String) session.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_EARLY_ALERT_ACCOUNT);
		if (earlyAlertSession == null) {
		  earlyAlertDisplay = "display:none;";
		  } else {
		  earlyAlertDisplay = "display:inline;";
		  }
		
		//show EarlyAlert once  
		session.setAttribute(com.usatoday.esub.common.UTCommon.SESSION_EARLY_ALERT_ACCOUNT, null);
							
			
	} catch (Exception e) {
	
	}
	//End EarlyAlert
	*/
	initFunction += "; initAcctInfoPage(thisObj, thisEvent);";
%>
<script src="/common/eEditionScripts.js"></script>
<script type="text/javascript" src="/common/popUpOverlay.js"></script>	
<script type="text/javascript">
  function initAcctInfoPage(thisObj, thisEvent) {
	  try {
		var showOverlayField = $('formMainForm:showOverlayPopUp');
	
		if (showOverlayField.value == "true") {
			keyCodeOverlayEnabled = true;
		}
  	  }
	  catch (err) {
	  } 
  }

function globalMessageDivHide() {
	try {  
		templateShowChatOnMouseOver = false;
		Effect.Shrink('globalMessageDiv');
		return false;
	}
	catch (e) {
	        return false;
	}
}
function onbeforeunloadPage(thisObj, thisEvent) {
	if( popOffer_programEnabled && keyCodeOverlayEnabled) {
		return popUpOnBeforeUnload(thisObj, thisEvent);
	}
}

function unloadPage(thisObj, thisEvent) {
	if (popOffer_programEnabled && keyCodeOverlayEnabled) {
		popOffer_unloadingPage = true;
		closeSpecialOffer(thisObj, thisEvent);
	}
}

function func_1(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
 return openReaderWindow();
}

</script>
<%-- /tpl:put --%>
<script>
// method called on page load
function initPage(thisObj, thisEvent) {
	// insert site-wide code here

	// for product messageing in popUpOverlay.js
	usat_productName = "<%=productName %>";

	<%=initFunction %>
}
</script>
<%-- /tpl:put --%>
<script type="text/javascript">
	/*   var _gaq = _gaq || [];
	 _gaq.push(['_setAccount', 'UA-23158809-1']);
	 _gaq.push(['_trackPageview']);

	 (function() {
	 var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	 ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	 var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	 })();
	 */
</script>
</HEAD>
<body>
	<DIV id="wrapper">
		<DIV id="header">
			<div id="headerLeft" onclick="templateLogoClick();"></div>
			<div id="headerRight">
				<%-- tpl:put name="headerbodyarea" --%>
					<%-- tpl:insert page="/login/loginform.jspf" --%><div style="float: none">
	<%
		String uri = request.getRequestURI();
		String pageName = uri.substring(uri.lastIndexOf("/") + 1);
		String jScript = "";
		if (uri.indexOf("subscriptions") > 0 && uri.indexOf("thankyou.jsp") > 0) {
			try {
				com.usatoday.esub.handlers.ShoppingCartHandler shoppingCartHandler = (com.usatoday.esub.handlers.ShoppingCartHandler) session
						.getAttribute("shoppingCartHandler");
				if (shoppingCartHandler != null) {
					jScript = shoppingCartHandler.getOnePageThankYouJavaScriptTealiumText();
				}
			} catch (Exception E) {

			}
		} else {
			com.usatoday.businessObjects.products.promotions.PromotionSet currentOfferPromotionSetTealium = null;
			com.usatoday.business.interfaces.products.OfferIntf offerTealium = com.usatoday.esub.common.UTCommon
					.getCurrentOffer(request);
			if (offerTealium != null && offerTealium instanceof com.usatoday.business.interfaces.products.SubscriptionOfferIntf) {
				currentOfferPromotionSetTealium = ((com.usatoday.business.interfaces.products.SubscriptionOfferIntf) offerTealium)
						.getPromotionSet();
				jScript = currentOfferPromotionSetTealium.getPromoOnePageJavaScriptTealiumText();
				jScript = org.apache.commons.lang3.StringUtils.replace(jScript, "page_name:\"\"", "page_name:\"" + uri + "\"");
			}
		}
	%>
	<%=jScript%>
</div>
<%-- 			<f:verbatim>
				<h:outputText styleClass="outputText" id="textThankYouJS13"	escape="false" value="#{shoppingCartHandler.tealiumOnePageThankYouJavaScriptText}"></h:outputText>
			</f:verbatim>
 --%>
<%
	String isLoginDisplay = "true";
	com.usatoday.business.interfaces.customer.CustomerIntf customer2 = null;
	String loggedinDivClass = "hideMe";
	String notLoggedinDivClass = "showMeInline";
	String firstName = "";
	String lastName = "";
	String firmInfo = "";
	String accountNum = "";
	String loginErrorMessage2 = "";
	String logOut = "";
	String publication = "";
	String changeAccountLink = "";
	String earlyAlertDisplay = "display:none";

	String jScriptNoOp = "javascript:;";

	try {
		publication = (String) session.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_PUB_NAME);
		if (publication == null) {
			publication = request.getParameter("pub");
			if (publication == null) {
				publication = com.usatoday.util.constants.UsaTodayConstants.UT_PUBCODE;
			}
		}
		if (publication.equals(com.usatoday.esub.common.UTCommon.PUB_NAMES[0])) { // USATODAY publication
			logOut = com.usatoday.esub.common.UTCommon.BASE_URL + "/index.jsp?pub=UT&logsessobj=rem";
		} else {
			logOut = com.usatoday.esub.common.UTCommon.BASE_URL + "/index.jsp?pub=BW&logsessobj=rem";
		}

		// If customer is logged in remove login page, otherwise display it
		customer2 = (com.usatoday.business.interfaces.customer.CustomerIntf) session
				.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_CUSTOMER_INFO);
		if (customer2 == null) {
			// showMeInling and hideMe are defined in style sheets
			loggedinDivClass = "showMeInline";
			notLoggedinDivClass = "hideMe";
		} else {

			if (customer2.getCurrentAccount() != null) {
				firstName = customer2.getCurrentAccount().getDeliveryContact().getFirstName();
				if (firstName != null && !firstName.trim().equals("")) {
					firstName = com.usatoday.esub.common.UTCommon.formatNameCapitalization(firstName); // Format it
				}
				lastName = customer2.getCurrentAccount().getDeliveryContact().getLastName();
				if (lastName != null && !lastName.trim().equals("")) {
					lastName = com.usatoday.esub.common.UTCommon.formatNameCapitalization(lastName); // Format it
				}
				firmInfo = customer2.getCurrentAccount().getDeliveryContact().getFirmName();
				if (customer2.getNumberOfAccounts() > 1) {
					changeAccountLink = "[&nbsp;<a href=\"/account/select_accnt.jsp\">Change</a>&nbsp;]";
				}
				try {
					accountNum = customer2.getCurrentAccount().getAccountNumber();
					//				if (accountNum == null || accountNum.length() < 4) {
					if (accountNum == null) {
						accountNum = "";
					}
					//				else {
					/*
					if (accountNum.equalsIgnoreCase("340000000")) {
						accountNum = "Pending Setup";
					}
					 */
					//				}
				} catch (Exception eeee) {
					eeee.printStackTrace();
				}

				//EarlyAlert - Check to see if their paper delivery has been affected by a delay
				// HD-CONS109 SEW
				/*
				if (customer2.getCurrentAccount().isShowDeliveryAlert()) {
				earlyAlertDisplay = "display:inline";
				}
				 */

			} else {
				if (customer2.getCurrentEmailRecord() != null
						&& customer2.getCurrentEmailRecord().getEmailAddress() != null) {
					firstName = customer2.getCurrentEmailRecord().getEmailAddress();
					accountNum = "Pending Setup";
				} else {
					firstName = "";
				}
			}
		}
		loginErrorMessage2 = (String) session.getAttribute("LOGIN_FAILED_MESSAGE");
		if (loginErrorMessage2 == null || loginErrorMessage2.trim().equals("")) {
			loginErrorMessage2 = "";
		} else {
			session.removeAttribute("LOGIN_FAILED_MESSAGE");
		}
	} catch (Exception e) {
		e.printStackTrace();
		if (firstName == null) {
			firstName = "";
		}
		if (lastName == null) {
			lastName = "";
		}
	}
	String nameInfo = firstName + " " + lastName;
	String loginInfo = null;
	if (!nameInfo.trim().equals("")) {
		loginInfo = nameInfo;
	} else {
		if (firmInfo != null && !firmInfo.trim().equals("")) {
			loginInfo = firmInfo;
		}
	}
	// if no login info, then hide data
	if (loginInfo == null) {
		loggedinDivClass = "showMeInline";
		notLoggedinDivClass = "hideMe";
		loginInfo = "";
	}
%>

<%@page import="com.usatoday.util.constants.UsaTodayConstants"%><div
	id="loginDisplayDiv" class="<%=loggedinDivClass%>">
	<table border="0" width="100%" height="60px">
		<tr>
			<td>
				<TABLE class="loginFormTable">
					<TBODY>
						<TR>
							<TD class="loginFormColumn"><br /> <a
								href="/login/auth.faces">Login to Manage Account</a></TD>
						</TR>
						<tr>
							<td class="loginFormColumn" align="right"><img
								style="cursor: pointer; margin-top: 10px;"
								onclick="javascript: templateClickToChatClickedGuest(this, event);"
								alt="Click for Assistance" title="Click for Assistance"
								src="${pageContext.request.contextPath}/theme/themeV3/themeImages/chatIcon136.png"
								hspace="0" vspace="5"></td>
						</tr>
					</TBODY>
				</TABLE></td>
		</tr>
	</table>
</div>
<div id="loginHideDiv" class="<%=notLoggedinDivClass%>">
	<table width="100%" border="0" class="loginFormTable">
		<tr>
			<td nowrap align="right">
				<TABLE class="loginFormTable">
					<TBODY>
						<TR>
							<TD class="loginFormColumn" nowrap><span
								class="loginNormalPrint">Hello, <%=loginInfo%>&nbsp;&nbsp;[&nbsp;<a
									href="<%=jScriptNoOp%>"
									onclick="javascript:document.location='<%=logOut%>';_unloadPage(this, event);">Logout</a>&nbsp;]</span>
							</TD>
						</TR>
					</TBODY>
				</TABLE></TD>
		</tr>
		<tr>
			<td class="loginFormColumn" align="right"><span
				class="loginfineprint">Account #:&nbsp;<%=accountNum%>&nbsp;<%=changeAccountLink%></span>
			</td>
		</tr>
		<tr>
			<td class="loginFormColumn" align="right"><div
					id="chatIconRequestDiv">
					<img onclick="javascript: templateClickToChatClicked(this, event);"
						style="cursor: pointer; margin-top: 10px;"
						alt="Click for Assistance" title="Click for Assistance"
						src="${pageContext.request.contextPath}/theme/themeV3/themeImages/chatIcon136.png"
						hspace="0" vspace="5">
				</div>
			</td>
		</tr>
	</table>
</div>
<div id="templateChatWindowDiv" style="display: none;">
	<div id="templateChatTopDiv">
		<span onclick="templateClickToChatHide();" style="cursor: pointer;">No
			Thanks<img src="/theme/themeV3/themeImages/close_14x14.png"
			width="14" height="14" border="0"
			style="margin-left: 5px; margin-right: 5px" /> </span>
	</div>
<!-- 	<div id="tempateChatImageDiv">
		<a
			onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
			href=""><img border="0"
			src="https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY" />
		</a>
	</div>
 -->	<div id="templateChatAccountLinksDiv">
		<ul>
			<li>To report a problem with delivery, click <a
				href="/account/cp/delissue.faces"> here</a>.</li>
			<li>To place your delivery on a temporary hold, click <a
				href="/account/vacationholds/holds.faces"> here</a>.</li>
			<li>Forgot your password or login? Click <a
				href="/idpassword/forgotpassword.jsp"> here</a>.
			<li>To contact us <a href="/feedback/feedback.jsp"> click here</a>.</li>				
<!-- 			<li>To cancel your subscription, please call 1-800-872-0001 and -->
<!-- 				select option 6.</li> -->
		</ul>
	</div>

	<div id="templateChatLinksDiv">
		<ul>

			<li><a
				onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
				href="">Click to Chat</a>
			</li>
		</ul>
	</div>
	<div id="templateChatBottomDiv">
		<span class="outputText">Or, call us at:<br> <b>Call
				1-877-713-6241</b><br>(M-F 8:00 a.m. to 7 p.m. E.T.)</span>
	</div>
</div>
<div id="templateChatWindowGuestDiv" style="display: none;">
	<div id="templateChatTopDiv">
		<span onclick="templateClickToChatHideGuest();"
			style="cursor: pointer;">No Thanks<img
			src="/theme/themeV3/themeImages/close_14x14.png" width="14"
			height="14" border="0" style="margin-left: 5px; margin-right: 5px" />
		</span>
	</div>
<!-- 	<div id="tempateChatImageDiv">
		<a
			onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
			href=""><img border="0"
			src="https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY" />
		</a>
	</div>
 -->	<div id="templateChatAccountLinksDiv">
		<ul>
			<li>To manage your account online - including reporting a
				problem with delivery or placing a temporary hold on your account -
				log in by entering your e-mail address and password. Click <a
				href="/login/auth.faces"> here</a> to access the login page now.</li>
			<li>Forgot your password or login? <a
				href="/idpassword/forgotpassword.jsp"> click here</a>.</li>
			<li>To contact us <a href="/feedback/feedback.jsp"> click here</a>.</li>
			<!-- 			<li>To cancel your subscription, please call 1-800-872-0001 and -->
			<!-- 				select option 6.</li> -->
		</ul>

	</div>

	<div id="templateChatLinksDiv">
		<ul>
			<li><a
				onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
				href="">Click to Chat</a>
			</li>
		</ul>
	</div>
	<div id="templateChatBottomDiv">
		<span class="outputText">Or, call us at:<br> <b>Call
				1-877-713-6241</b><br>(M-F 8:00 a.m. to 7 p.m. E.T.)</span>
	</div>
</div>
<%-- /tpl:insert --%>
		<%-- /tpl:put --%>
			</div>
		</DIV>
		<!-- end header -->
		<DIV id="content">
			<DIV id="whitebox">
				<%-- tpl:put name="bodyAreaMainWhiteBox" --%>
<div id="mobileAccountNav">
	<h4>My Account Options:</h4>
	<select name="accountNavLinksSelect" onchange="javascript: mobileNavChanged(this, event);">
		<option value="noop" selected="selected">&nbsp;</option>
	 	<option value="/account/accounthistory/accountSummary.faces">Account Information</option>
		<option value="/custserviceindex.jsp?custopt=RENEW">Pay Bill</option>
		<%=suspendResumeDeliveryMobileOption %>
		<%=reportDeliveryProblemMobileOption %>
		<option value="/custserviceindex.jsp?custopt=DELA">Change delivery info</option>
		<option value="/custserviceindex.jsp?custopt=BILA">Change billing info</option>
		<option value="/idpassword/changeIDpassword.jsp">Change email</option>
		<option value="/idpassword/changeIDpassword.jsp">Change password</option>
		<option value="/welcome.jsp">Give a gift</option>
		<option value="http://onlinestore.usatoday.com">Order back issues</option>
		<%=cancelSubscriptionMobileOption %>
		<%=changeAccountLinkMobile %>
		<option value="<%=logOut%>">Log Out</option>
	</select>
	<hr />	
</div>
	<DIV id=nav>
	<DIV id="navPromo">
		<%-- tpl:put name="navigationArea_Top" --%><%-- /tpl:put --%>
	</div>
	<DIV id=leftNavList>
		<UL>
		  <LI><A href="/account/accounthistory/accountSummary.faces">Account information</A></LI>
		  <LI><A href="/custserviceindex.jsp?custopt=RENEW">Pay Bill</A></LI>
		  <%=suspendResumeDeliveryNavOption %>
		  <%=reportDeliveryProblemNavOption %>
		  <LI><A href="/custserviceindex.jsp?custopt=DELA">Change delivery info</A></LI>
		  <LI><A href="/custserviceindex.jsp?custopt=BILA">Change billing info</A></LI>
		  <LI><A href="/idpassword/changeIDpassword.jsp">Change e-mail/password</A></LI>
		  <LI><A href="<%=subcribeJS%>">Give a gift</A></LI>
		  <LI><A href="<%=backIssueJS%>">Order back issues</A></LI>
		  <%=cancelSubscriptionNavOption %>
		  <%=dynamicNavigation %>
		</UL>
	</DIV>
	<DIV id="navPromo">
		<%-- tpl:put name="navigationArea_Bottom" --%><%-- /tpl:put --%>
	</div>
</DIV>
<DIV id=pageContent>
	<%-- tpl:put name="MainBodyArea" --%>
							<hx:scriptCollector id="scriptCollector1" preRender="#{pc_AccountSummary.onPageLoadBegin}">
								<h1>
									<h:outputText id="mainHeader" value="#{labels.MyAccountLabel}"
										styleClass="H1Class"></h:outputText>
								</h1>
								<h:messages styleClass="messages" id="messages1"></h:messages>
								<div id="pageContent50Left">
									<div id="accountInformationBox">
										<h:panelGrid styleClass="panelGrid"
											id="gridAccountInformation" columns="2" width="100%"
											columnClasses="panelGridColTopLeftAlignV2,panelGridColTopLeftAlignV2"
											style="margin-left: 5px; font-family: Arial; "
											rendered="#{customerHandler.customerDataAvailable}">

											<h:outputText styleClass="outputText" id="textAccountLabel"
												value="Account Number:" style="font-weight: bold"></h:outputText>
											<h:outputText styleClass="outputText" id="textAccountNumber"
												value="#{customerHandler.customer.currentAccount.accountNumber}"></h:outputText>
											<h:outputText styleClass="outputText"
												id="textAccountProductName" value="Product Name:"
												style="font-weight: bold"></h:outputText>
											<h:outputText styleClass="outputText"
												id="textAccountProdName"
												value="#{customerHandler.customer.currentAccount.product.name}"></h:outputText>
											<h:outputText styleClass="outputText"
												id="textCompanyNameLabel" value="Company Name:"
												style="font-weight: bold"></h:outputText>
											<h:outputText styleClass="outputText" id="textCompanyName"
												value="#{customerHandler.customer.currentAccount.deliveryContact.firmName}"></h:outputText>
											<h:outputText styleClass="outputText" id="textNameLabel"
												value="Subscriber Name:" style="font-weight: bold"></h:outputText>
											<h:outputText styleClass="outputText" id="text3"
												value="#{customerHandler.customer.currentAccount.deliveryContact.firstName} #{customerHandler.customer.currentAccount.deliveryContact.lastName}"></h:outputText>
											<h:outputText styleClass="outputText" id="textAddress1Label"
												value="Subscriber Address:" style="font-weight: bold"></h:outputText>
											<h:panelGrid id="deliverAddrGrid">
												<h:outputText styleClass="outputText" id="textAddress1"
													value="#{customerHandler.customer.currentAccount.deliveryContact.UIAddress.address1} #{customerHandler.customer.currentAccount.deliveryContact.UIAddress.aptSuite}"></h:outputText>
												<h:outputText styleClass="outputText" id="textAddress2"
													value="#{customerHandler.customer.currentAccount.deliveryContact.UIAddress.address2}"></h:outputText>
												<h:outputText styleClass="outputText" id="textAddressCSZ"
													value="#{customerHandler.customer.currentAccount.deliveryContact.UIAddress.city}, #{customerHandler.customer.currentAccount.deliveryContact.UIAddress.state} #{customerHandler.customer.currentAccount.deliveryContact.UIAddress.zip}"></h:outputText>
											</h:panelGrid>
											<h:outputText styleClass="outputText" id="textPhoneLabel"
												value="Home Phone:" style="font-weight: bold"></h:outputText>
											<h:outputText styleClass="outputText" id="textPhoneNum"
												value="#{customerHandler.customer.currentAccount.deliveryContact.homePhoneNumberFormatted}"></h:outputText>
											<h:outputText styleClass="outputText" id="textStartDateLabel"
												value="Subscriber Since:" style="font-weight: bold"></h:outputText>
											<h:outputText styleClass="outputText" id="textStartDate"
												value="#{customerHandler.customer.currentAccount.startDate}">
												<hx:convertDateTime />
											</h:outputText>
											<h:outputText styleClass="outputText"
												id="textSubscriptionEnd" value="Paid Through:"
												style="font-weight: bold"></h:outputText>

											<h:outputText styleClass="outputText" id="textExpirationDate"
												value="#{customerHandler.customer.currentAccount.expirationDate}">
												<hx:convertDateTime />
											</h:outputText>
											<h:outputText styleClass="outputText" id="textEZPayLabel"
												value="EZ-PAY:" style="font-weight: bold"></h:outputText>
											<h:outputText styleClass="outputText" id="textOnEZPay"
												value="#{customerHandler.currentAccountEZPAyStatusString}">
											</h:outputText>
											<h:outputText styleClass="outputText" id="textAPlaceHolder"></h:outputText>
											<hx:outputLinkEx styleClass="outputLinkEx"
												id="linkExUpdateEmailPreferencesLink"
												value="#{customerHandler.exactTargetEmailPreferenceLink}"
												target="_blank"
												rendered="#{customerHandler.isCurrentAccountUTBranded}">
												<h:outputText id="textUpdateEmailPreferenceLinkText"
													styleClass="outputText"
													value="#{labels.updateDailyEmailPrefsLabel}"></h:outputText>
											</hx:outputLinkEx>
										</h:panelGrid>
									</div>
								</div>
								<div id="pageContent50Right">
									<h:panelGrid id="gridUTCompanionGrid" columns="1" width="300"
										columnClasses="panelGridColTopCentertAlign"
										style="width: 300px; text-align: center"
										rendered="#{customerHandler.isCurrentAccountUTBranded}">
										<h:panelGrid id="notExpiredAccountGrid" 
											style="width: 100%; text-align: center" columns="1"
											rendered="#{customerHandler.customer.currentAccount.expiredSubscriptionAccessRenewed}">
											<hx:outputLinkEx styleClass="outputLinkEx"
												id="linkExReadNowLink_2"
												value="#{customerHandler.defaultUTeReaderLink}"
												onclick="return func_1(this, event);">
												<img name="computer_art_2_button_v2_r1_c1"
													src="${pageContext.request.contextPath}/images/eEdition/eEditionSummary229x41.jpg"
													border="0"
													id="computer_art_2_button_v2_r1_c1" alt="" hspace="10">
											</hx:outputLinkEx>
											<hx:jspPanel id="eReaderLinkPanel">
												<div id="pageContentNoMarginsButton">
													<hx:outputLinkEx styleClass="outputLinkEx"
														id="linkExReadNowLink"
														value="#{customerHandler.defaultUTeReaderLink}"
														onclick="return func_1(this, event);">
														<hx:graphicImageEx styleClass="graphicImageEx"
															id="imageExReadNowButtonImage"
															value="/images/eEdition/computer_art_2_button_v2_r2_c1_116w.gif"
															width="116" height="40" hspace="0" vspace="0" border="0"
															align="center"></hx:graphicImageEx>
													</hx:outputLinkEx>
												</div>
												<table style="width: 100%;margin-top: -5px;">
													<tr>
														<td align="center">
															<table>
																<tr>
																	<td>
																		<hx:outputLinkEx styleClass="outputLinkEx" id="linkExOpenDesktopVersion" value="#{customerHandler.UTeReaderLink}" onclick="return func_1(this, event);">
																	<h:outputText id="text1111" 
																		value="desktop" styleClass="outputText"></h:outputText>
															</hx:outputLinkEx>
																	</td>
																	<td>
																		<h:outputText styleClass="outputText" id="text2" value="|"></h:outputText>
																	</td>
																	<td>
																		<hx:outputLinkEx styleClass="outputLinkEx" id="linkExOpenMobileVersion" value="#{customerHandler.alternateUTeReaderLink}" onclick="return func_1(this, event);">
																	<h:outputText id="text1" styleClass="outputText"
																		value="mobile"></h:outputText>
															</hx:outputLinkEx>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>

											</hx:jspPanel>
																						
											<hx:outputLinkEx value="/faq/eeFAQ.html"
												styleClass="outputLinkEx" id="linkExEEFAQ1" style="margin-top: 10px">
												<h:outputText id="textEEFAQtext" styleClass="outputText"
													value="e-Newspaper FAQ"></h:outputText>
											</hx:outputLinkEx>
										</h:panelGrid>

										<hx:jspPanel id="RenewRequiredPanel"
											rendered="#{not customerHandler.customer.currentAccount.expiredSubscriptionAccessRenewed }">
											<div id="leftNavList">
												<ul>
													<li style='background-image: url("/images/1px.gif")'><a
														href="/custserviceindex.jsp?custopt=RENEW">Pay
															bill</a></li>
												</ul>
											</div>
										</hx:jspPanel>
									</h:panelGrid>
									<!-- end UT Panel -->
									<h:panelGrid id="gridBWCompanionGrid" columns="1" width="300"
										columnClasses="panelGridColTopCentertAlign"
										style="width: 300px; text-align: center"
										rendered="#{customerHandler.isCurrentAccountBWBranded}">
										<h:panelGrid id="notExpiredAccountGrid2"
											style="width: 100%; text-align: center" columns="1"
											rendered="#{customerHandler.customer.currentAccount.expiredSubscriptionAccessRenewed}">
											<hx:outputLinkEx styleClass="outputLinkEx"
												id="linkExReadNowLink_22"
												value="#{customerHandler.sportsWeeklyeReaderLink}">
												<img name="computer_art_2_button_v2_r1_c1"
													src="${pageContext.request.contextPath}/images/eEdition/swEEComp.gif"
													width="280" height="210" border="0"
													id="computer_art_2_button_v2_r1_c1" alt="" hspace="10">
											</hx:outputLinkEx>
											<hx:jspPanel id="eReaderLinkBWPanel">
												<div id="pageContentNoMarginsButton">
													<hx:outputLinkEx styleClass="outputLinkEx"
														id="linkExReadNowBWLink"
														value="#{customerHandler.sportsWeeklyeReaderLink}">
														<hx:graphicImageEx styleClass="graphicImageEx"
															id="imageExReadNowBWButtonImage"
															value="/images/eEdition/computer_art_2_button_v2_r2_c1_116w.gif"
															width="116" height="40" hspace="0" vspace="0" border="0"
															align="center"></hx:graphicImageEx>
													</hx:outputLinkEx>
												</div>
											</hx:jspPanel>
											<h:outputText styleClass="outputText, usatFormFont"
												id="textBWInfoText1"
												value="#{labels.SportsWeeklyEEInfoText}" escape="false"></h:outputText>
										</h:panelGrid>

										<hx:jspPanel id="renewBWRequiredPanel"
											rendered="#{not customerHandler.customer.currentAccount.expiredSubscriptionAccessRenewed }">
											<div id="leftNavList">
												<ul>
													<li style='background-image: url("/images/1px.gif")'><a
														href="/custserviceindex.jsp?custopt=RENEW">Renew/Pay
															bill</a></li>
												</ul>
											</div>
										</hx:jspPanel>
									</h:panelGrid>
									<!-- end BW Panel -->
								</div>
								<div id="clear"></div>
								<!-- Begin promotional spots -->
								<h:panelGrid styleClass="panelGrid, lowerPromoSpot" id="gridPromoSpotOutterPanel1" columns="1" rendered="#{customerHandler.currentAccountPubDefaultOffer.isShowAccountInfoPromoSection}" style="margin-top: 0px">
									<f:facet name="header">
										<h:panelGroup styleClass="panelGroup"
											id="groupPromoHeaderGroup1"
											style="width: 100%; text-align: left">
											<h3 style="padding-bottom: 0px">
												<h:outputText styleClass="outputText"
													id="textPromoHeaderText" value="Promotional Offers:"></h:outputText>
											</h3>
										</h:panelGroup>
									</f:facet>
									<h:panelGrid styleClass="panelGrid" id="gridInnerPromoSpots"
										columns="3"
										columnClasses="lowerPromoLeft, lowerPromoMiddle, lowerPromoRight"
										style="width: 100%; margin: 0px; padding: 0px">
										<hx:graphicImageEx styleClass="graphicImageEx"
											id="imageExLowLeft"
											value="#{customerHandler.currentAccountPubDefaultOffer.accountInfoPageImageSpot1ImagePath}"
											alt="#{customerHandler.currentAccountPubDefaultOffer.accountInfoPageImageSpot1ImageAltText}"
											rendered="#{not customerHandler.currentAccountPubDefaultOffer.renderAccountInfoImageSpot1ImageAsClickable }"></hx:graphicImageEx>
										<hx:outputLinkEx styleClass="outputLinkEx"
											id="linkExLowLeftPromoImage1"
											rendered="#{customerHandler.currentAccountPubDefaultOffer.renderAccountInfoImageSpot1ImageAsClickable}"
											value="#{customerHandler.currentAccountPubDefaultOffer.accountInfoPageImageSpot1ImageOnClickURL}">
											<hx:graphicImageEx styleClass="graphicImageEx"
												id="imageExLowLeftClickable"
												value="#{customerHandler.currentAccountPubDefaultOffer.accountInfoPageImageSpot1ImagePath}"
												alt="#{customerHandler.currentAccountPubDefaultOffer.accountInfoPageImageSpot1ImageAltText}"></hx:graphicImageEx>
										</hx:outputLinkEx>

										<hx:graphicImageEx styleClass="graphicImageEx"
											id="imageExLowMiddle"
											value="#{customerHandler.currentAccountPubDefaultOffer.accountInfoPageImageSpot2ImagePath}"
											alt="#{customerHandler.currentAccountPubDefaultOffer.accountInfoPageImageSpot2ImageAltText}"
											rendered="#{not customerHandler.currentAccountPubDefaultOffer.renderAccountInfoImageSpot2ImageAsClickable }"></hx:graphicImageEx>
										<hx:outputLinkEx styleClass="outputLinkEx"
											id="linkExLowMiddlePromoImage1"
											rendered="#{customerHandler.currentAccountPubDefaultOffer.renderAccountInfoImageSpot2ImageAsClickable}"
											value="#{customerHandler.currentAccountPubDefaultOffer.accountInfoPageImageSpot2ImageOnClickURL}">
											<hx:graphicImageEx styleClass="graphicImageEx"
												id="imageExLowMiddleClickable"
												value="#{customerHandler.currentAccountPubDefaultOffer.accountInfoPageImageSpot2ImagePath}"
												alt="#{customerHandler.currentAccountPubDefaultOffer.accountInfoPageImageSpot2ImageAltText}"></hx:graphicImageEx>
										</hx:outputLinkEx>

										<hx:graphicImageEx styleClass="graphicImageEx"
											id="imageExLowRight"
											value="#{customerHandler.currentAccountPubDefaultOffer.accountInfoPageImageSpot3ImagePath}"
											alt="#{customerHandler.currentAccountPubDefaultOffer.accountInfoPageImageSpot3ImageAltText}"
											rendered="#{not customerHandler.currentAccountPubDefaultOffer.renderAccountInfoImageSpot3ImageAsClickable }"></hx:graphicImageEx>
										<hx:outputLinkEx styleClass="outputLinkEx"
											id="linkExLowRightPromoImage1"
											rendered="#{customerHandler.currentAccountPubDefaultOffer.renderAccountInfoImageSpot3ImageAsClickable}"
											value="#{customerHandler.currentAccountPubDefaultOffer.accountInfoPageImageSpot3ImageOnClickURL}">
											<hx:graphicImageEx styleClass="graphicImageEx"
												id="imageExLowRightClickable"
												value="#{customerHandler.currentAccountPubDefaultOffer.accountInfoPageImageSpot3ImagePath}"
												alt="#{customerHandler.currentAccountPubDefaultOffer.accountInfoPageImageSpot3ImageAltText}"></hx:graphicImageEx>
										</hx:outputLinkEx>

									</h:panelGrid>
								</h:panelGrid>
								
								<!-- BEGIN On Exit Pop Area - DO NOT CHANGE NAMES OF DIV ID -->
								<hx:jspPanel id="jspPanelPopOverlayPanel"
									rendered="#{customerHandler.currentAccount.showCustomerServicePopOverlay}">
									<div id="popunder" style="display: none"></div>
									<div id="leavingofferdiv"
										style="display: none;">
										<h:panelGroup styleClass="panelGroup"
											id="groupSpecialOfferGroupBox"
											style="height: 100%; width: 100%">
											<h:panelGrid styleClass="panelGrid"
												id="gridSpecialOfferLayoutGrid" width="100%" columns="1"
												style="text-align: right">
												<hx:outputLinkEx styleClass="outputLinkEx"
													id="linkExCloseSpecialOffer" value="#"
													onclick="return closeSpecialOffer();">
													<h:outputText id="textCloseSpecialOfferText"
														styleClass="outputText" value="Close [X]"></h:outputText>
												</hx:outputLinkEx>
												<hx:outputLinkEx styleClass="outputLinkEx"
													id="linkExSpecialOfferLink"
													value="#{customerHandler.currentAccount.customerServicePopOverlayImageLinkURL}">
													<hx:graphicImageEx styleClass="graphicImageEx"
														id="imageExSpecialOfferImage"
														value="#{customerHandler.currentAccount.customerServicePopOverlayImagePath}"></hx:graphicImageEx>
												</hx:outputLinkEx>
											</h:panelGrid>
										</h:panelGroup>
									</div>
								</hx:jspPanel>
								<h:inputHidden
									value="#{customerHandler.currentAccount.showCustomerServicePopOverlay}"
									id="showOverlayPopUp"></h:inputHidden>
								<!-- END On Exit Pop Area -->

							</hx:scriptCollector>
							
							 <div id="globalMessageDiv" style="<%=earlyAlertDisplay%>">
							 <div id="templateChatTopDiv">
								<span onclick="globalMessageDivHide();" style="cursor: pointer;">Acknowledged<img
								src="/theme/themeV3/themeImages/close_14x14.png" width="14"
								height="14" border="0" style="margin-left: 5px; margin-right: 5px"/>
								</span>
							</div>
    						<div id="globalMessageBottomDiv">
							<span class="outputText" >As a valued subscriber of USA TODAY, we wanted to let you know that the delivery of today's newspaper will be delayed. We apologize for this inconvenience and appreciate your patience.<br><br>As a reminder, you can read the e-Newspaper of USA TODAY online!  The e-Newspaper is an exact digital replica of USA TODAY and is included with your subscription at no additional charge.<br>
							<br>Thank you,<br>USA TODAY, National Customer Service</span>
							</div>
 							</div>
						<%-- /tpl:put --%>
</DIV>
<%-- /tpl:put --%>
				<DIV id="clear"></DIV>
				<DIV id="bluebar">
					<a href="/feedback/feedback.jsp"
						style="color: white; font-weight: bold; text-decoration: none; font-size: 14px"
						title="Contact Us">Contact Us</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;1-800-872-0001&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a
						href="/faq/utfaq.jsp"
						style="color: white; font-weight: bold; text-decoration: none; font-size: 14px"
						title="Frequently Asked Questions">FAQ</a>
				</DIV>
				<%-- tpl:put name="footerMainArea" --%>
			<%-- tpl:put name="footerArea_1" --%><%-- /tpl:put --%>
			<%=trackingBug.getOmnitureHTML() %>
		<%-- /tpl:put --%>
				<DIV id=graybar>
					Copyright
					<script>
						document.write(getCurrentYear());
					</script>
					USA TODAY, a division of <a href="http://www.gannett.com">Gannett
						Co. Inc.</a> <a href="/privacy/privacy.htm">Privacy
						Notice/California Privacy Notice</a>. <a
						href="/privacy/privacy.htm#adchoice">Ad Choices</a>.<br> By
					using this service, you accept our <a href="/service/service.jsp">Terms
						of Service</a>.
				</DIV>
				<DIV id=copyright>
					<P>Offers available to new subscribers only. Home delivery not
						available in some areas. Not valid with any other offer.
						Prepayment required. After the promotional period ends, the
						subscription will continue and be billed monthly at the then
						regular rate, less any applicable credits, unless USA TODAY is
						otherwise notified. Applicable taxes may not be included in the
						rate. If at any time you decide to cancel your subscription, you
						may contact customer service at 1-800-872-0001 and the full amount
						of any balance over $2.00 will be returned.</P>
				</DIV>
				<!--END OF COPYWRITE-->
				<DIV id=footer></DIV>
			</DIV>
			<!--END OF WHITEBOX-->
		</DIV>
		<!--END OF CONTENT-->
	</DIV>
</BODY>
</HTML><%-- /tpl:insert --%>
</f:view><%-- /tpl:insert --%>
<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/account/accounthistory/AccountSummary.java" --%><%-- /jsf:pagecode --%>