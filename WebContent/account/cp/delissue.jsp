<%-- tpl:insert page="/theme/themeV3/jsfTheme/subscriberPortalLeftNavAccount_JSF.jtpl" --%><!DOCTYPE HTML><%@page import="com.usatoday.business.interfaces.products.ProductIntf"%>
<%@page import="com.usatoday.businessObjects.products.ProductBO"%>
<%@page import="com.usatoday.util.MobileDeviceDetectionUtil"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%><%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%><%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<f:view><f:loadBundle basename="resourceFile" var="labels" /><%-- tpl:insert page="/theme/subscriptionPortalRoot.htpl" --%><HTML>
<HEAD>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8">
<%-- tpl:put name="headerareaTop" --%><%
	String docProtocol = "https:"; 
	if (!request.isSecure()) {
		docProtocol = "http:";
	}
	// following work around to get around problem with RAD 8.0.2 automated links in templates - APE
	String subcribeJS = "javascript:_subscribeToPub();"; 
	String backIssueJS = "javascript:_shopUSAT();";
	
	// change this to any javascript you want called on page load
	String initFunction = "";
	String dynamicNavigation = "";
	
	com.usatoday.esub.common.WebBug trackingBug = com.usatoday.esub.common.UTCommon.getWebAnalyticsBug();
	try {
		trackingBug.deriveValuesFromRequest(request);
	}
	catch (Exception e) {
		; // ignore
	}

	// not shown if electronic pub   
	String suspendResumeDeliveryNavOption = "<LI><A href=\"/account/vacationholds/holds.faces\">Suspend/Resume delivery</A></LI>";
	String suspendResumeDeliveryMobileOption = "<option value=\"/account/vacationholds/holds.faces\">Suspend delivery</option><option value=\"/account/vacationholds/holds.faces\">Resume delivery</option>";
	
	String reportDeliveryProblemNavOption = "<LI><A href=\"/account/cp/delissue.faces\">Report delivery problem</A></LI>";
	String reportDeliveryProblemMobileOption = "<option value=\"/account/cp/delissue.faces\">Report a problem</option>";
	
	String cancelSubscriptionNavOption = "";
	String cancelSubscriptionMobileOption = "";

	String changeAccountLinkMobile = "";
		
	String pubCode = "";
	String productName = "USA TODAY";
	 
    boolean isEEditionProduct = true;

	com.usatoday.businessObjects.products.promotions.PromotionSet defaultPromotionSet = null;
	com.usatoday.businessObjects.products.promotions.PromotionSet currentOfferPromotionSet = null;
	com.usatoday.business.interfaces.customer.CustomerIntf customer = null;
   	com.usatoday.businessObjects.products.promotions.PromotionSet currentAccountPromotionSet = null;
   		
	com.usatoday.business.interfaces.products.OfferIntf offer = null;   		
	try {
		offer = com.usatoday.esub.common.UTCommon.getCurrentOfferVersion2(request);
		pubCode = com.usatoday.util.constants.UsaTodayConstants.UT_PUBCODE;
		if (offer != null) {
			pubCode = offer.getPubCode();
		}
		// get the default promotion set for the publication		
		defaultPromotionSet = com.usatoday.esub.common.UTCommon.getDefaultPromotionSetForPub(pubCode);
		
		// get the keycode specific promotion set
		
		if (offer != null && offer instanceof com.usatoday.business.interfaces.products.SubscriptionOfferIntf) {
			currentOfferPromotionSet = ((com.usatoday.business.interfaces.products.SubscriptionOfferIntf)offer).getPromotionSet();
		}
		
		if (currentOfferPromotionSet != null) {
			com.usatoday.business.interfaces.products.promotions.HTMLPromotionIntf dPromo = currentOfferPromotionSet.getDynamicNavigationCustServiceHTML();
			if (dPromo != null) {
				dynamicNavigation = dPromo.getPromotionalHTML();
			}
		}
				
		boolean isSignedIn = false;
		customer = (com.usatoday.business.interfaces.customer.CustomerIntf)session.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_CUSTOMER_INFO);
		if (customer != null) {
			isSignedIn = true;
			com.usatoday.business.interfaces.products.SubscriptionProductIntf product = com.usatoday.businessObjects.products.SubscriptionProductBO.getSubscriptionProduct(customer.getCurrentEmailRecord().getPubCode());
			isEEditionProduct = product.isElectronicDelivery(); 
			// This promotion set is based on pub and keycode on the customer account
			currentAccountPromotionSet = com.usatoday.businessObjects.products.promotions.PromotionManager.getInstance().getPromotionsForOffer(customer.getCurrentAccount().getPubCode(), customer.getCurrentAccount().getKeyCode());
			if (isEEditionProduct) {
				suspendResumeDeliveryNavOption = ""; // clear vacation holds option
				suspendResumeDeliveryMobileOption = "";
				reportDeliveryProblemNavOption = ""; // clear deliveyr problem link
				reportDeliveryProblemMobileOption = "";
//				cancelSubscriptionNavOption = "<LI><A href=\"/account/cancels/cancelSubscription.faces\">Cancel subscription</A></LI>"; // show cancels 			
//				cancelSubscriptionMobileOption = "<option value=\"/account/cancels/cancelSubscription.faces\">Cancel subscription</option>"; 
			}
			
			try {
				String brandingPubCode = product.getBrandingPubCode();
				ProductIntf bp = ProductBO.fetchProduct(brandingPubCode);
				productName = bp.getName();
			}
			catch (Exception teee) {
			}							
			
			if (customer.getNumberOfAccounts() > 1) {
				changeAccountLinkMobile = "<option value=\"/account/select_accnt.jsp\">Change Account</option>";
			}
			
		}
				
	} catch (Exception e) {
	
	}
	String mobileCSS = "";
	if (MobileDeviceDetectionUtil.isSmallScreenDevice(request)) {
		mobileCSS = "<LINK href=\"/theme/themeV3/css/smallScreen.css\" rel=\"stylesheet\" type=\"text/css\">";
	}
 %>
<script src="/common/prototype/prototype.js"></script>
<script src="/common/prototype/scriptaculous/scriptaculous.js" type="text/javascript"></script>
<link rel="stylesheet"	href="/theme/themeV3/css/stylesheet_jsfcomponents.css"	type="text/css">
<%-- tpl:put name="headerarea_Top" --%><title>USA Today Online Subscriptions - Report A Delivery Problem</title>
<% initFunction += "initThisPage();"; %>
<script src="/common/complaint.js"></script>
<script type="text/javascript">
function initThisPage() {
	var dField = $('form1:textIssueDate1');
	if (dField.value.length > 0) {
		$('form1:sectionIssueDate2').show();
	}
	dField = $('form1:textIssueDate2');
	if (dField.value.length > 0) {
		$('form1:sectionIssueDate3').show();
	}
	dField = $('form1:textIssueDate3');
	if (dField.value.length > 0) {
		$('form1:sectionIssueDate4').show();
	}
	dField = $('form1:textIssueDate4');
	if (dField.value.length > 0) {
		$('form1:sectionIssueDate5').show();
	}	
}
</script><%-- /tpl:put --%>
<%-- /tpl:put --%>
<LINK REL="SHORTCUT ICON" HREF="/favicon.ico">
<LINK REL="icon" href="/favicon.ico" TYPE="image/ico" />
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META http-equiv="Content-Style-Type" content="text/css">
<meta name="viewport" content="initial-scale=1.0">
<LINK href="/theme/themeV3/css/usatodayBase.css" rel="stylesheet"
	type="text/css">
<LINK href="/theme/themeV3/css/usatodayAddOn.css" rel="stylesheet"
	type="text/css">
<script src="/common/usatCommon.js"></script>
<%-- tpl:put name="headareaBottom" --%>
<%=mobileCSS %>
<%-- tpl:put name="headarea_Bottom" --%><!-- Bottom Header area --><%-- /tpl:put --%>
<script>
// method called on page load
function initPage(thisObj, thisEvent) {
	// insert site-wide code here

	// for product messageing in popUpOverlay.js
	usat_productName = "<%=productName %>";

	<%=initFunction %>
}
</script>
<%-- /tpl:put --%>
<script type="text/javascript">
	/*   var _gaq = _gaq || [];
	 _gaq.push(['_setAccount', 'UA-23158809-1']);
	 _gaq.push(['_trackPageview']);

	 (function() {
	 var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	 ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	 var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	 })();
	 */
</script>
</HEAD>
<body>
	<DIV id="wrapper">
		<DIV id="header">
			<div id="headerLeft" onclick="templateLogoClick();"></div>
			<div id="headerRight">
				<%-- tpl:put name="headerbodyarea" --%>
					<%-- tpl:insert page="/login/loginform.jspf" --%><div style="float: none">
	<%
		String uri = request.getRequestURI();
		String pageName = uri.substring(uri.lastIndexOf("/") + 1);
		String jScript = "";
		if (uri.indexOf("subscriptions") > 0 && uri.indexOf("thankyou.jsp") > 0) {
			try {
				com.usatoday.esub.handlers.ShoppingCartHandler shoppingCartHandler = (com.usatoday.esub.handlers.ShoppingCartHandler) session
						.getAttribute("shoppingCartHandler");
				if (shoppingCartHandler != null) {
					jScript = shoppingCartHandler.getOnePageThankYouJavaScriptTealiumText();
				}
			} catch (Exception E) {

			}
		} else {
			com.usatoday.businessObjects.products.promotions.PromotionSet currentOfferPromotionSetTealium = null;
			com.usatoday.business.interfaces.products.OfferIntf offerTealium = com.usatoday.esub.common.UTCommon
					.getCurrentOffer(request);
			if (offerTealium != null && offerTealium instanceof com.usatoday.business.interfaces.products.SubscriptionOfferIntf) {
				currentOfferPromotionSetTealium = ((com.usatoday.business.interfaces.products.SubscriptionOfferIntf) offerTealium)
						.getPromotionSet();
				jScript = currentOfferPromotionSetTealium.getPromoOnePageJavaScriptTealiumText();
				jScript = org.apache.commons.lang3.StringUtils.replace(jScript, "page_name:\"\"", "page_name:\"" + uri + "\"");
			}
		}
	%>
	<%=jScript%>
</div>
<%-- 			<f:verbatim>
				<h:outputText styleClass="outputText" id="textThankYouJS13"	escape="false" value="#{shoppingCartHandler.tealiumOnePageThankYouJavaScriptText}"></h:outputText>
			</f:verbatim>
 --%>
<%
	String isLoginDisplay = "true";
	com.usatoday.business.interfaces.customer.CustomerIntf customer2 = null;
	String loggedinDivClass = "hideMe";
	String notLoggedinDivClass = "showMeInline";
	String firstName = "";
	String lastName = "";
	String firmInfo = "";
	String accountNum = "";
	String loginErrorMessage2 = "";
	String logOut = "";
	String publication = "";
	String changeAccountLink = "";
	String earlyAlertDisplay = "display:none";

	String jScriptNoOp = "javascript:;";

	try {
		publication = (String) session.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_PUB_NAME);
		if (publication == null) {
			publication = request.getParameter("pub");
			if (publication == null) {
				publication = com.usatoday.util.constants.UsaTodayConstants.UT_PUBCODE;
			}
		}
		if (publication.equals(com.usatoday.esub.common.UTCommon.PUB_NAMES[0])) { // USATODAY publication
			logOut = com.usatoday.esub.common.UTCommon.BASE_URL + "/index.jsp?pub=UT&logsessobj=rem";
		} else {
			logOut = com.usatoday.esub.common.UTCommon.BASE_URL + "/index.jsp?pub=BW&logsessobj=rem";
		}

		// If customer is logged in remove login page, otherwise display it
		customer2 = (com.usatoday.business.interfaces.customer.CustomerIntf) session
				.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_CUSTOMER_INFO);
		if (customer2 == null) {
			// showMeInling and hideMe are defined in style sheets
			loggedinDivClass = "showMeInline";
			notLoggedinDivClass = "hideMe";
		} else {

			if (customer2.getCurrentAccount() != null) {
				firstName = customer2.getCurrentAccount().getDeliveryContact().getFirstName();
				if (firstName != null && !firstName.trim().equals("")) {
					firstName = com.usatoday.esub.common.UTCommon.formatNameCapitalization(firstName); // Format it
				}
				lastName = customer2.getCurrentAccount().getDeliveryContact().getLastName();
				if (lastName != null && !lastName.trim().equals("")) {
					lastName = com.usatoday.esub.common.UTCommon.formatNameCapitalization(lastName); // Format it
				}
				firmInfo = customer2.getCurrentAccount().getDeliveryContact().getFirmName();
				if (customer2.getNumberOfAccounts() > 1) {
					changeAccountLink = "[&nbsp;<a href=\"/account/select_accnt.jsp\">Change</a>&nbsp;]";
				}
				try {
					accountNum = customer2.getCurrentAccount().getAccountNumber();
					//				if (accountNum == null || accountNum.length() < 4) {
					if (accountNum == null) {
						accountNum = "";
					}
					//				else {
					/*
					if (accountNum.equalsIgnoreCase("340000000")) {
						accountNum = "Pending Setup";
					}
					 */
					//				}
				} catch (Exception eeee) {
					eeee.printStackTrace();
				}

				//EarlyAlert - Check to see if their paper delivery has been affected by a delay
				// HD-CONS109 SEW
				/*
				if (customer2.getCurrentAccount().isShowDeliveryAlert()) {
				earlyAlertDisplay = "display:inline";
				}
				 */

			} else {
				if (customer2.getCurrentEmailRecord() != null
						&& customer2.getCurrentEmailRecord().getEmailAddress() != null) {
					firstName = customer2.getCurrentEmailRecord().getEmailAddress();
					accountNum = "Pending Setup";
				} else {
					firstName = "";
				}
			}
		}
		loginErrorMessage2 = (String) session.getAttribute("LOGIN_FAILED_MESSAGE");
		if (loginErrorMessage2 == null || loginErrorMessage2.trim().equals("")) {
			loginErrorMessage2 = "";
		} else {
			session.removeAttribute("LOGIN_FAILED_MESSAGE");
		}
	} catch (Exception e) {
		e.printStackTrace();
		if (firstName == null) {
			firstName = "";
		}
		if (lastName == null) {
			lastName = "";
		}
	}
	String nameInfo = firstName + " " + lastName;
	String loginInfo = null;
	if (!nameInfo.trim().equals("")) {
		loginInfo = nameInfo;
	} else {
		if (firmInfo != null && !firmInfo.trim().equals("")) {
			loginInfo = firmInfo;
		}
	}
	// if no login info, then hide data
	if (loginInfo == null) {
		loggedinDivClass = "showMeInline";
		notLoggedinDivClass = "hideMe";
		loginInfo = "";
	}
%>

<%@page import="com.usatoday.util.constants.UsaTodayConstants"%><div
	id="loginDisplayDiv" class="<%=loggedinDivClass%>">
	<table border="0" width="100%" height="60px">
		<tr>
			<td>
				<TABLE class="loginFormTable">
					<TBODY>
						<TR>
							<TD class="loginFormColumn"><br /> <a
								href="/login/auth.faces">Login to Manage Account</a></TD>
						</TR>
						<tr>
							<td class="loginFormColumn" align="right"><img
								style="cursor: pointer; margin-top: 10px;"
								onclick="javascript: templateClickToChatClickedGuest(this, event);"
								alt="Click for Assistance" title="Click for Assistance"
								src="${pageContext.request.contextPath}/theme/themeV3/themeImages/chatIcon136.png"
								hspace="0" vspace="5"></td>
						</tr>
					</TBODY>
				</TABLE></td>
		</tr>
	</table>
</div>
<div id="loginHideDiv" class="<%=notLoggedinDivClass%>">
	<table width="100%" border="0" class="loginFormTable">
		<tr>
			<td nowrap align="right">
				<TABLE class="loginFormTable">
					<TBODY>
						<TR>
							<TD class="loginFormColumn" nowrap><span
								class="loginNormalPrint">Hello, <%=loginInfo%>&nbsp;&nbsp;[&nbsp;<a
									href="<%=jScriptNoOp%>"
									onclick="javascript:document.location='<%=logOut%>';_unloadPage(this, event);">Logout</a>&nbsp;]</span>
							</TD>
						</TR>
					</TBODY>
				</TABLE></TD>
		</tr>
		<tr>
			<td class="loginFormColumn" align="right"><span
				class="loginfineprint">Account #:&nbsp;<%=accountNum%>&nbsp;<%=changeAccountLink%></span>
			</td>
		</tr>
		<tr>
			<td class="loginFormColumn" align="right"><div
					id="chatIconRequestDiv">
					<img onclick="javascript: templateClickToChatClicked(this, event);"
						style="cursor: pointer; margin-top: 10px;"
						alt="Click for Assistance" title="Click for Assistance"
						src="${pageContext.request.contextPath}/theme/themeV3/themeImages/chatIcon136.png"
						hspace="0" vspace="5">
				</div>
			</td>
		</tr>
	</table>
</div>
<div id="templateChatWindowDiv" style="display: none;">
	<div id="templateChatTopDiv">
		<span onclick="templateClickToChatHide();" style="cursor: pointer;">No
			Thanks<img src="/theme/themeV3/themeImages/close_14x14.png"
			width="14" height="14" border="0"
			style="margin-left: 5px; margin-right: 5px" /> </span>
	</div>
<!-- 	<div id="tempateChatImageDiv">
		<a
			onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
			href=""><img border="0"
			src="https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY" />
		</a>
	</div>
 -->	<div id="templateChatAccountLinksDiv">
		<ul>
			<li>To report a problem with delivery, click <a
				href="/account/cp/delissue.faces"> here</a>.</li>
			<li>To place your delivery on a temporary hold, click <a
				href="/account/vacationholds/holds.faces"> here</a>.</li>
			<li>Forgot your password or login? Click <a
				href="/idpassword/forgotpassword.jsp"> here</a>.
			<li>To contact us <a href="/feedback/feedback.jsp"> click here</a>.</li>				
<!-- 			<li>To cancel your subscription, please call 1-800-872-0001 and -->
<!-- 				select option 6.</li> -->
		</ul>
	</div>

	<div id="templateChatLinksDiv">
		<ul>

			<li><a
				onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
				href="">Click to Chat</a>
			</li>
		</ul>
	</div>
	<div id="templateChatBottomDiv">
		<span class="outputText">Or, call us at:<br> <b>Call
				1-877-713-6241</b><br>(M-F 8:00 a.m. to 7 p.m. E.T.)</span>
	</div>
</div>
<div id="templateChatWindowGuestDiv" style="display: none;">
	<div id="templateChatTopDiv">
		<span onclick="templateClickToChatHideGuest();"
			style="cursor: pointer;">No Thanks<img
			src="/theme/themeV3/themeImages/close_14x14.png" width="14"
			height="14" border="0" style="margin-left: 5px; margin-right: 5px" />
		</span>
	</div>
<!-- 	<div id="tempateChatImageDiv">
		<a
			onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
			href=""><img border="0"
			src="https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY" />
		</a>
	</div>
 -->	<div id="templateChatAccountLinksDiv">
		<ul>
			<li>To manage your account online - including reporting a
				problem with delivery or placing a temporary hold on your account -
				log in by entering your e-mail address and password. Click <a
				href="/login/auth.faces"> here</a> to access the login page now.</li>
			<li>Forgot your password or login? <a
				href="/idpassword/forgotpassword.jsp"> click here</a>.</li>
			<li>To contact us <a href="/feedback/feedback.jsp"> click here</a>.</li>
			<!-- 			<li>To cancel your subscription, please call 1-800-872-0001 and -->
			<!-- 				select option 6.</li> -->
		</ul>

	</div>

	<div id="templateChatLinksDiv">
		<ul>
			<li><a
				onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
				href="">Click to Chat</a>
			</li>
		</ul>
	</div>
	<div id="templateChatBottomDiv">
		<span class="outputText">Or, call us at:<br> <b>Call
				1-877-713-6241</b><br>(M-F 8:00 a.m. to 7 p.m. E.T.)</span>
	</div>
</div>
<%-- /tpl:insert --%>
		<%-- /tpl:put --%>
			</div>
		</DIV>
		<!-- end header -->
		<DIV id="content">
			<DIV id="whitebox">
				<%-- tpl:put name="bodyAreaMainWhiteBox" --%>
<div id="mobileAccountNav">
	<h4>My Account Options:</h4>
	<select name="accountNavLinksSelect" onchange="javascript: mobileNavChanged(this, event);">
		<option value="noop" selected="selected">&nbsp;</option>
	 	<option value="/account/accounthistory/accountSummary.faces">Account Information</option>
		<option value="/custserviceindex.jsp?custopt=RENEW">Pay Bill</option>
		<%=suspendResumeDeliveryMobileOption %>
		<%=reportDeliveryProblemMobileOption %>
		<option value="/custserviceindex.jsp?custopt=DELA">Change delivery info</option>
		<option value="/custserviceindex.jsp?custopt=BILA">Change billing info</option>
		<option value="/idpassword/changeIDpassword.jsp">Change email</option>
		<option value="/idpassword/changeIDpassword.jsp">Change password</option>
		<option value="/welcome.jsp">Give a gift</option>
		<option value="http://onlinestore.usatoday.com">Order back issues</option>
		<%=cancelSubscriptionMobileOption %>
		<%=changeAccountLinkMobile %>
		<option value="<%=logOut%>">Log Out</option>
	</select>
	<hr />	
</div>
	<DIV id=nav>
	<DIV id="navPromo">
		<%-- tpl:put name="navigationArea_Top" --%><%-- /tpl:put --%>
	</div>
	<DIV id=leftNavList>
		<UL>
		  <LI><A href="/account/accounthistory/accountSummary.faces">Account information</A></LI>
		  <LI><A href="/custserviceindex.jsp?custopt=RENEW">Pay Bill</A></LI>
		  <%=suspendResumeDeliveryNavOption %>
		  <%=reportDeliveryProblemNavOption %>
		  <LI><A href="/custserviceindex.jsp?custopt=DELA">Change delivery info</A></LI>
		  <LI><A href="/custserviceindex.jsp?custopt=BILA">Change billing info</A></LI>
		  <LI><A href="/idpassword/changeIDpassword.jsp">Change e-mail/password</A></LI>
		  <LI><A href="<%=subcribeJS%>">Give a gift</A></LI>
		  <LI><A href="<%=backIssueJS%>">Order back issues</A></LI>
		  <%=cancelSubscriptionNavOption %>
		  <%=dynamicNavigation %>
		</UL>
	</DIV>
	<DIV id="navPromo">
		<%-- tpl:put name="navigationArea_Bottom" --%><%-- /tpl:put --%>
	</div>
</DIV>
<DIV id=pageContent>
	<%-- tpl:put name="MainBodyArea" --%>
							<hx:scriptCollector id="scriptCollectorComplaints" preRender="#{pc_Delissue.onPageLoadBegin}">
								<h:form styleClass="form" id="form1">
									<h1>
										<h:outputText id="mainHeader"
											value="#{labels.DelProblemHeader}" styleClass="H1Class"></h:outputText>
									</h1>
									<div id="pageContent70">
										<h:messages styleClass="messages" id="messages1"></h:messages>
										<h:panelGrid styleClass="panelGrid" id="grid4" rendered="#{complaintHandler.subscriptionActive}">
										<hx:panelFormBox helpPosition="under" labelPosition="over"
											styleClass="panelFormBoxFull" id="formBox1">
											<hx:formItem styleClass="formItem" id="formItemIssueType"
												label='#{labels.DelProblemIssueHeader} <span style="color:red">*</span>'
												style="margin-bottom: 3px; font-weight: bold">
												<h:selectOneMenu styleClass="selectOneMenu"
													id="menuIssueType" required="true"
													value="#{complaintHandler.issueType}">
													<f:selectItems
														value="#{customerHandler.currentAccount.complatintIssueOptionSelectItems}"
														id="selectItems2" />
												</h:selectOneMenu>
											</hx:formItem>

											<hx:formItem styleClass="formItem" id="formItemIssueDate1"
												label='#{labels.DelProblemOccurrence} <span style="color:red">*</span>'
												style="margin-bottom: 3px; font-weight: bold;"
												infoText="mm/dd/yyyy format, or click calendar icon">
												<h:inputText styleClass="inputText" id="textIssueDate1"
													required="true" value="#{complaintHandler.date1}"
													onchange="cp_dateChanged(this);"
													validatorMessage="Please specify the date the issue occurred"
													requiredMessage="Date is required"
													converterMessage="Invalid Date Format. Enter date in MM/DD/YYYY format.">
													<hx:convertDateTime pattern="MM/dd/yyyy" />
													<hx:inputHelperDatePicker id="datePicker1" />
													<hx:validateDateTimeRange maximum="#{now}" />
													<hx:behavior event="onchange" id="behavior1"
														behaviorAction="show" targetAction="sectionIssueDate2"></hx:behavior>
												</h:inputText>
											</hx:formItem>
											<f:facet name="top">
												<h:panelGrid styleClass="panelGrid" id="grid3" width="100%"
													columns="1">
													<h:outputText styleClass="outputText footnote" id="text7"
														value='<span style="color:red">*</span> Required Fields'
														escape="false"></h:outputText>
												</h:panelGrid>
											</f:facet>
										</hx:panelFormBox>
										<hx:panelSection styleClass="panelSection panelFormBoxFull"
											id="sectionIssueDate2" initClosed="true"
											style="display: none;">
											<f:facet name="closed">
												<hx:jspPanel id="jspPanel2">
													<h:outputText id="text2" styleClass="outputText"
														value="Add another occurrence"></h:outputText>
												</hx:jspPanel>
											</f:facet>
											<f:facet name="opened">
												<hx:jspPanel id="jspPanel1">
												</hx:jspPanel>
											</f:facet>
											<hx:panelFormBox helpPosition="under" labelPosition="over"
												styleClass="panelFormBox" id="formBoxIssueDate2"
												style="margin-left: 0px;">
												<hx:formItem styleClass="formItem" id="IssueDate2"
													style="margin-bottom: 3px; font-weight: bold; padding-left 0px;">
													<h:inputText styleClass="inputText" id="textIssueDate2"
														value="#{complaintHandler.date2}"
														onchange="cp_dateChanged(this);"
														converterMessage="#{labels.DelProblemDateFormat}">
														<hx:convertDateTime pattern="MM/dd/yyyy" />
														<hx:inputHelperDatePicker id="datePicker2" />
														<hx:validateDateTimeRange maximum="#{now}" />
														<hx:behavior event="onchange" id="behavior2"
															behaviorAction="show" targetAction="sectionIssueDate3"></hx:behavior>
													</h:inputText>
												</hx:formItem>
											</hx:panelFormBox>
										</hx:panelSection>
										<hx:panelSection styleClass="panelSection panelFormBoxFull"
											id="sectionIssueDate3" initClosed="true" style="display:none">
											<f:facet name="closed">
												<hx:jspPanel id="jspPanel4">
													<h:outputText id="text3" styleClass="outputText"
														value="Add another occurrence"></h:outputText>
												</hx:jspPanel>
											</f:facet>
											<f:facet name="opened">
												<hx:jspPanel id="jspPanel3">
												</hx:jspPanel>
											</f:facet>
											<hx:panelFormBox helpPosition="under" labelPosition="over"
												styleClass="panelFormBox" id="formBoxIssueDate3"
												style="margin-left: 0px;">
												<hx:formItem styleClass="formItem" id="IssueDate3"
													style="margin-bottom: 3px; font-weight: bold; padding-left 0px;">
													<h:inputText styleClass="inputText" id="textIssueDate3"
														value="#{complaintHandler.date3}"
														onchange="cp_dateChanged(this);"
														converterMessage="#{labels.DelProblemDateFormat}">
														<hx:convertDateTime pattern="MM/dd/yyyy" />
														<hx:inputHelperDatePicker id="datePicker3" />
														<hx:validateDateTimeRange maximum="#{now}" />
														<hx:behavior event="onchange" id="behavior3"
															behaviorAction="show" targetAction="sectionIssueDate4"></hx:behavior>
													</h:inputText>
												</hx:formItem>
											</hx:panelFormBox>
										</hx:panelSection>
										<hx:panelSection styleClass="panelSection panelFormBoxFull"
											id="sectionIssueDate4" initClosed="true" style="display:none">
											<f:facet name="closed">
												<hx:jspPanel id="jspPanel44">
													<h:outputText id="text33" styleClass="outputText"
														value="Add another occurrence"></h:outputText>
												</hx:jspPanel>
											</f:facet>
											<f:facet name="opened">
												<hx:jspPanel id="jspPanel33">
												</hx:jspPanel>
											</f:facet>
											<hx:panelFormBox helpPosition="under" labelPosition="over"
												styleClass="panelFormBox" id="formBoxIssueDate4"
												style="margin-left: 0px;">
												<hx:formItem styleClass="formItem" id="IssueDate4"
													style="margin-bottom: 3px; font-weight: bold; padding-left 0px;">
													<h:inputText styleClass="inputText" id="textIssueDate4"
														value="#{complaintHandler.date4}"
														onchange="cp_dateChanged(this);"
														converterMessage="#{labels.DelProblemDateFormat}">
														<hx:convertDateTime pattern="MM/dd/yyyy" />
														<hx:inputHelperDatePicker id="datePicker4" />
														<hx:validateDateTimeRange maximum="#{now}" />
														<hx:behavior event="onchange" id="behavior4"
															behaviorAction="show" targetAction="sectionIssueDate5"></hx:behavior>
													</h:inputText>
												</hx:formItem>
											</hx:panelFormBox>
										</hx:panelSection>
										<hx:panelSection styleClass="panelSection panelFormBoxFull"
											id="sectionIssueDate5" initClosed="true" style="display:none">
											<f:facet name="closed">
												<hx:jspPanel id="jspPanel444">
													<h:outputText id="text333" styleClass="outputText"
														value="Add another occurrence"></h:outputText>
												</hx:jspPanel>
											</f:facet>
											<f:facet name="opened">
												<hx:jspPanel id="jspPanel333">
												</hx:jspPanel>
											</f:facet>
											<hx:panelFormBox helpPosition="under" labelPosition="over"
												styleClass="panelFormBox" id="formBoxIssueDate5"
												style="margin-left: 0px;">
												<hx:formItem styleClass="formItem" id="IssueDate5"
													escape="false"
													infoText="If your issue has occurred more than 5 times,<br />please call National Customer Service."
													style="margin-bottom: 3px; font-weight: bold; padding-left 0px;">
													<h:inputText styleClass="inputText" id="textIssueDate5"
														value="#{complaintHandler.date5}"
														onchange="cp_dateChanged(this);"
														converterMessage="#{labels.DelProblemDateFormat}">
														<hx:convertDateTime pattern="MM/dd/yyyy" />
														<hx:inputHelperDatePicker id="datePicker5" />
														<hx:validateDateTimeRange maximum="#{now}" />
													</h:inputText>
												</hx:formItem>
											</hx:panelFormBox>
										</hx:panelSection>
										<h:panelGrid styleClass="panelSection panelFormBoxFull"
											id="grid2">
											<hx:commandExButton type="submit" value="Submit"
												styleClass="commandExButton" id="buttonSubmitComplaint"
												style="margin-left: 5px; margin-bottom: 15px; margin-top: 20px"
												action="#{pc_Delissue.doButtonSubmitComplaintAction}"></hx:commandExButton>
										</h:panelGrid>
										</h:panelGrid>
										</div>
										<h:panelGrid styleClass="panelGrid" id="grid5" rendered="#{complaintHandler.subscriptionActive}">
									<div id="pageContent30" style="border-style: none">
										<h:panelGrid styleClass="panelGrid tblStatusBlk"
											id="gridCustomerInfoBorderGrid" cellpadding="0"
											cellspacing="0">
											<h:panelGrid styleClass="panelGrid" id="gridProductDataGrid"
												columnClasses="panelGridColTopLeftAlignV2,panelGridColTopLeftAlignV2"
												columns="2" cellpadding="2" width="100%" cellspacing="2">
												<f:facet name="header">
													<h:panelGroup id="group2" style="background-color: #4268b4">
														<h:panelGrid styleClass="panelGrid tblStatusBlkTH"
															id="grid1" width="100%" columns="1" cellpadding="0"
															cellspacing="0">
															<h:outputText id="textAccountInfoTextHeader"
																styleClass="outputText tblStatusBlkTH"
																value="Account Information"></h:outputText>
														</h:panelGrid>
													</h:panelGroup>
												</f:facet>
												<h:outputText id="textProductNameLabel"
													value="#{labels.Publication} :" style="font-weight: bold"></h:outputText>
												<h:outputText styleClass="outputText" id="textProductName"
													value="#{customerHandler.currentAccount.account.product.name}"></h:outputText>
												<h:outputText id="textAccountNumberLabel"
													value="#{labels.accountNumberLabel} :"
													style="font-weight: bold"></h:outputText>
												<h:outputText styleClass="outputText" id="textAccountNumber"
													value="#{customerHandler.currentAccount.account.accountNumber}"></h:outputText>
											</h:panelGrid>
											<h:panelGrid styleClass="panelGrid" id="gridDeliveryInfoGrid"
												cellpadding="1" cellspacing="1" width="100%">
												<f:facet name="header">
													<h:panelGroup id="group3" style="background-color: #4268b4">
														<h:panelGrid styleClass="panelGrid tblStatusBlkTH"
															id="gridDelInfoHeadGrid" width="100%" columns="1"
															cellpadding="0" cellspacing="0">
															<h:outputText id="textDeliveryInfoHeaderText"
																styleClass="outputText tblStatusBlkTH"
																value="Delivery Information"></h:outputText>
														</h:panelGrid>
													</h:panelGroup>
												</f:facet>
												<h:outputText styleClass="outputText" id="text1"
													value="#{customerHandler.currentAccount.deliveryNameForDisplay}"></h:outputText>
												<h:outputText styleClass="outputText" id="text5"
													value="#{customerHandler.currentAccount.addressLine1ForDisplay}"></h:outputText>
												<h:outputText styleClass="outputText" id="text6"
													value="#{customerHandler.currentAccount.addressLine2ForDisplay}"></h:outputText>
												<h:outputText styleClass="outputText" id="textCityStateZip"
													value="#{customerHandler.currentAccount.cityStateZipForDisplay}"></h:outputText>
												<h:outputText styleClass="outputText" id="textPhone"
													value="#{customerHandler.currentAccount.deliveryPhoneForDisplay}"></h:outputText>
												<h:outputText styleClass="outputText" id="textNumbercopies"
													value="#{customerHandler.currentAccount.numberOfCopiesForRenewalDisplay}"></h:outputText>
											</h:panelGrid>
										</h:panelGrid>
									</div>
									<hx:panelDialog type="modal" styleClass="panelDialog"
										id="dialogErrorDialog" initiallyShow="false" title="Unexpected Error" style="width: 500px;">
										<h:panelGroup id="groupErrorContents" styleClass="panelDialog_Footer">
											<hx:jspPanel id="jspPanelErrorContent">
												<h:outputText id="textErrorRawResponse" value="#{complaintHandler.rawAPIResponse}"></h:outputText>
											</hx:jspPanel>
											<hx:commandExButton id="button2" styleClass="commandExButton"
												type="submit" value="OK">
												<hx:behavior event="onclick" behaviorAction="hide;stop"
													targetAction="dialogErrorDialog" id="behavior6"></hx:behavior>
											</hx:commandExButton>
											<hx:commandExButton id="button1" styleClass="commandExButton"
												type="reset" value="Cancel">
												<hx:behavior event="onclick" behaviorAction="hide;stop"
													id="behavior5" targetAction="dialogErrorDialog"></hx:behavior>
											</hx:commandExButton>
										</h:panelGroup>
									</hx:panelDialog>
									<br>
								</h:panelGrid>
								</h:form>
							</hx:scriptCollector>
						<%-- /tpl:put --%>
</DIV>
<%-- /tpl:put --%>
				<DIV id="clear"></DIV>
				<DIV id="bluebar">
					<a href="/feedback/feedback.jsp"
						style="color: white; font-weight: bold; text-decoration: none; font-size: 14px"
						title="Contact Us">Contact Us</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;1-800-872-0001&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a
						href="/faq/utfaq.jsp"
						style="color: white; font-weight: bold; text-decoration: none; font-size: 14px"
						title="Frequently Asked Questions">FAQ</a>
				</DIV>
				<%-- tpl:put name="footerMainArea" --%>
			<%-- tpl:put name="footerArea_1" --%><%-- /tpl:put --%>
			<%=trackingBug.getOmnitureHTML() %>
		<%-- /tpl:put --%>
				<DIV id=graybar>
					Copyright
					<script>
						document.write(getCurrentYear());
					</script>
					USA TODAY, a division of <a href="http://www.gannett.com">Gannett
						Co. Inc.</a> <a href="/privacy/privacy.htm">Privacy
						Notice/California Privacy Notice</a>. <a
						href="/privacy/privacy.htm#adchoice">Ad Choices</a>.<br> By
					using this service, you accept our <a href="/service/service.jsp">Terms
						of Service</a>.
				</DIV>
				<DIV id=copyright>
					<P>Offers available to new subscribers only. Home delivery not
						available in some areas. Not valid with any other offer.
						Prepayment required. After the promotional period ends, the
						subscription will continue and be billed monthly at the then
						regular rate, less any applicable credits, unless USA TODAY is
						otherwise notified. Applicable taxes may not be included in the
						rate. If at any time you decide to cancel your subscription, you
						may contact customer service at 1-800-872-0001 and the full amount
						of any balance over $2.00 will be returned.</P>
				</DIV>
				<!--END OF COPYWRITE-->
				<DIV id=footer></DIV>
			</DIV>
			<!--END OF WHITEBOX-->
		</DIV>
		<!--END OF CONTENT-->
	</DIV>
</BODY>
</HTML><%-- /tpl:insert --%>
</f:view><%-- /tpl:insert --%>
<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/account/cp/Delissue.java" --%><%-- /jsf:pagecode --%>