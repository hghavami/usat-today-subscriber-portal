<%@ page import = "com.usatoday.esub.common.*" %>
<%

	WebBug bug = UTCommon.getWebAnalyticsBug();
	bug.setCurrentPage(request.getRequestURI());

%>
<html>

<head>
<title>USA Today Online Subscriptions - Cancel Transaction</title>
</head>

<body>
<p><font face="Arial">

</font>

<table border="0" width="798" cellspacing="0" cellpadding="0">
  <tr>
    <td width="100%">
      <h2 align="center"><span class="USAT_Header">Transaction
		Cancelled</span></h2>
      <p align="left"><span
			class="USAT_BlueSubHeader">At your request, your transaction has been
		cancelled.&nbsp; Please, note that your credit card has not been
		charged for this transaction.&nbsp; If you have arrived to this screen
		by mistake, you can click the Back button and continue with your
		transaction.</span></p>
      <p><font face="Arial">&nbsp;</font>
    </td>
  </tr>
  <tr>
    <td width="100%">
      <hr width="100%">
      <p align="center"><font face="Arial">
</font>

    </td>
  </tr>
</table>

<p>&nbsp;</p>

</body>
</html>