<%! 
public String displayMessage(javax.servlet.http.HttpServletRequest request) {
	javax.servlet.http.HttpSession session = request.getSession(false);
	if (session == null) {
		// no session
		return null; 
	}

	String message = (String) session.getAttribute("MESSAGE");

	if (message == null) {
		// no message in session
		message = "";
	}
	else {
		session.removeAttribute("MESSAGE");
	}

	return "<font color=#FF0000>" + message + "</font>";
}
public String displayResult(javax.servlet.http.HttpServletRequest request) {
	
	java.lang.StringBuffer result = new java.lang.StringBuffer();
	javax.servlet.http.HttpSession session = request.getSession(false);
	com.usatoday.business.interfaces.customer.CustomerIntf customer = null;
	try {
		customer = (com.usatoday.business.interfaces.customer.CustomerIntf) session.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_CUSTOMER_INFO);
	} catch (Exception e) {
	}
	int itemcount = 0;
	String pubCodeName = null;
	java.util.HashMap<String, com.usatoday.business.interfaces.customer.SubscriberAccountIntf> accounts = customer.getAccounts();
    

	java.util.Collection<com.usatoday.business.interfaces.customer.EmailRecordIntf> emails = customer.getEmailRecords().values();
	java.util.Set<String> emailKeys = customer.getEmailRecords().keySet();
	for (String emailKey : emailKeys) {
		com.usatoday.business.interfaces.customer.EmailRecordIntf email = customer.getEmailRecords().get(emailKey);
		
        String currentAccountNum = email.getAccountNumber();
        customer.setCurrentAccount(currentAccountNum); 
        // Define various interfaces
        com.usatoday.business.interfaces.customer.SubscriberAccountIntf account = customer.getCurrentAccount();

		
		if (account != null && !email.isStartDateInFuture()) {
			itemcount++;
			// valid account that is fully set up
	        com.usatoday.business.interfaces.UIAddressIntf deliveryAddr = account.getDeliveryContact().getUIAddress();        
	        com.usatoday.business.interfaces.products.SubscriptionProductIntf product = null;
	        if ( (itemcount % 2) == 0) {
	       		result.append("<tr>");
	        }
	        else {
	           result.append("<tr class=\"rowClass2\">");
	        }
			//result.append("<tr>");
	        
	        try {
	        	product = account.getProduct();
	        }
	        catch (Exception e) {
	        	;
	        }
	        
			
			result.append("<td width=\"20%\" valign=\"top\">");
			result.append("<input type=\"radio\" name=\"SELECTED\" value=\"");
			result.append(currentAccountNum + "/" + account.getPubCode() + "\">");
			
			if (product != null) {
				pubCodeName = product.getName();
			}
			else {
				pubCodeName = "Not Available";
			}
			result.append("<font face=\"Arial\" font size=\"2\">");
			result.append(pubCodeName);
			result.append("</font></td>");
	
			result.append("<td width=\"20%\" valign=\"top\">");
			result.append("<font face=\"Arial\" font size=\"2\">");		
			result.append(currentAccountNum);
			result.append("</font></td>");
			
			result.append("<td width=\"25%\" valign=\"top\">");
			result.append("<font face=\"Arial\" font size=\"2\">");
			result.append(account.getDeliveryContact().getFirstName() + " " + account.getDeliveryContact().getLastName());
			result.append("</font></td>");
			
			result.append("<td width=\"35%\" valign=\"top\">");
			result.append("<font face=\"Arial\" font size=\"2\">");		
			result.append(deliveryAddr.getAddress1() + " " + "<br>");
			if (deliveryAddr.getAddress2() != null && !deliveryAddr.getAddress2().trim().equals("")) { 
			    result.append(deliveryAddr.getAddress2() + " " + "<br>");
			}
			result.append(deliveryAddr.getCity() + ", " + deliveryAddr.getState() + " " + deliveryAddr.getZip());
			result.append("</font></td>");
			
			result.append("</tr>");
		}
		else {
			// just have an email address
			if (email.isStartDateInFuture()) {
				itemcount++;
				// put something that notes we know about the account
		        com.usatoday.business.interfaces.products.SubscriptionProductIntf product = null;
		        com.usatoday.business.interfaces.UIAddressIntf deliveryAddr = null;

		        if ( (itemcount % 2) == 0) {
		       		result.append("<tr>");
		        }
		        else {
		           result.append("<tr class=\"rowClass2\">");
		        }
				//result.append("<tr>");
		        
		        try {
		        	product = com.usatoday.businessObjects.products.SubscriptionProductBO.getSubscriptionProduct(email.getPubCode());
		        	deliveryAddr = account.getDeliveryContact().getUIAddress();
		        }
		        catch (Exception e) {
		        	;
		        }
		                
		        
				currentAccountNum = email.getAccountNumber();
				
				// hd-cons92
				//if (currentAccountNum.length() == 0 || currentAccountNum.equalsIgnoreCase("340000000")) {
		 
				if (currentAccountNum.length() == 0 ) {
					currentAccountNum = "Not yet assigned";
				}
			 
				
				result.append("<td width=\"20%\" valign=\"top\">");
				
				if (product != null) {
					pubCodeName = product.getName();
				}
				else {
					pubCodeName = "Not Available";
				}
				result.append("<font face=\"Arial\" font size=\"2\">");
				result.append(pubCodeName);
				result.append("</font><br><font face=\"Arial\" font size=\"1\">");
				
				try {
					org.joda.time.DateTime startDate = com.usatoday.businessObjects.util.UsatDateTimeFormatter.convertYYYYMMDDToDateTime(email.getPermStartDate());
					result.append("(Start date: ").append(startDate.toString("MM/dd/yyyy")).append(")");
				}
				catch(Exception e) {
					result.append("(Start date unknown)");
				}
				
				
				result.append("</font></td>");
		
				result.append("<td width=\"20%\" valign=\"top\">");
				result.append("<font face=\"Arial\" font size=\"2\">");		
				result.append(currentAccountNum);
				result.append("</font></td>");
				
				result.append("<td width=\"25%\" valign=\"top\">");
				result.append("<font face=\"Arial\" font size=\"2\">");
				if (account != null) {
					result.append(account.getDeliveryContact().getFirstName()).append(" ");
					result.append(account.getDeliveryContact().getLastName());
				}
				else {
					result.append("Setup pending");
				}
				result.append("</font></td>");
				
				result.append("<td width=\"35%\" valign=\"top\">");
				result.append("<font face=\"Arial\" font size=\"2\">");	
				if (deliveryAddr != null) {
					result.append(deliveryAddr.getAddress1() + " " + "<br>");
					if (deliveryAddr.getAddress2() != null && !deliveryAddr.getAddress2().trim().equals("")) { 
					    result.append(deliveryAddr.getAddress2() + " " + "<br>");
					}
					result.append(deliveryAddr.getCity() + ", " + deliveryAddr.getState() + " " + deliveryAddr.getZip());
				}
				else {	
					result.append("Setup pending");
				}
				result.append("</font></td>");
				
				result.append("</tr>");
			} 
			else if (customer.hasAccountWithNoAccountNumber() ) {
				// recent account
				itemcount++;
				
				
		        com.usatoday.business.interfaces.products.SubscriptionProductIntf product = null;
		        if ( (itemcount % 2) == 0) {
		       		result.append("<tr>");
		        }
		        else {
		           result.append("<tr class=\"rowClass2\">");
		        }
		        
		        try {
		        	product = com.usatoday.businessObjects.products.SubscriptionProductBO.getSubscriptionProduct(email.getPubCode());
		        }
		        catch (Exception e) {
		        	;
		        }
		        
				currentAccountNum = "Not yet assigned";
				
				result.append("<td width=\"20%\" valign=\"top\">");
				
				if (product != null) {
					pubCodeName = product.getName();
				}
				else {
					pubCodeName = "Not Available";
				}
				
				// append selection link to recent ut/ee accounts
				if (product.isElectronicDelivery() || product.getBrandingPubCode().equalsIgnoreCase("UT")) {
					result.append("<input type=\"radio\" name=\"SELECTED\" value=\"");
					result.append(emailKey + "/" + email.getPubCode() + "\">");
				}
							
				result.append("<font face=\"Arial\" font size=\"2\">");
				result.append(pubCodeName);
				result.append("</font><br><font face=\"Arial\" font size=\"1\">");
				
				try {
					if (!email.getPermStartDate().equalsIgnoreCase("00000000") ) {
						org.joda.time.DateTime startDate = com.usatoday.businessObjects.util.UsatDateTimeFormatter.convertYYYYMMDDToDateTime(email.getPermStartDate());
						result.append("(Start date: ").append(startDate.toString("MM/dd/yyyy")).append(")");
					}
				}
				catch(Exception e) {
					; // ignore
				}
				
				
				result.append("</font></td>");
		
				result.append("<td width=\"20%\" valign=\"top\">");
				result.append("<font face=\"Arial\" font size=\"2\">");		
				result.append(currentAccountNum);
				result.append("</font></td>");
				
				result.append("<td width=\"25%\" valign=\"top\">");
				result.append("<font face=\"Arial\" font size=\"2\">");
				result.append("Setup pending");
				result.append("</font></td>");
				
				result.append("<td width=\"35%\" valign=\"top\">");
				result.append("<font face=\"Arial\" font size=\"2\">");		
				result.append("Setup pending");
				result.append("</font></td>");
				
				result.append("</tr>");
				
			}
			else {
				// ignore it
				continue;
			}
		}
	}		

	
	return result.toString();
}
%>
<%-- tpl:insert page="/theme/subscriptionPortalJSPNoNavigation.jtpl" --%><!DOCTYPE HTML><%@page import="com.usatoday.business.interfaces.products.ProductIntf"%>
<%@page import="com.usatoday.businessObjects.products.ProductBO"%>
<%@page import="com.usatoday.util.MobileDeviceDetectionUtil"%><%-- tpl:insert page="/theme/subscriptionPortalRoot.htpl" --%><HTML>
<HEAD>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8">
<%-- tpl:put name="headerareaTop" --%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%
	String docProtocol = "https:";  
	if (!request.isSecure()) {
		docProtocol = "http:";
	}
	String initFunction = "";  // override this value if need be on page load
	
	com.usatoday.esub.common.WebBug trackingBug = com.usatoday.esub.common.UTCommon.getWebAnalyticsBug();
	String pubCode = com.usatoday.util.constants.UsaTodayConstants.UT_PUBCODE;
	String productName = "USA TODAY";
	com.usatoday.business.interfaces.products.OfferIntf offer = null;
	try {
		
		offer = com.usatoday.esub.common.UTCommon.getCurrentOffer(request);
		
		if (offer != null) {
			pubCode = offer.getPubCode();
			try {
				String brandingPubCode = offer.getProduct().getBrandingPubCode();
				ProductIntf bp = ProductBO.fetchProduct(brandingPubCode);
				productName = bp.getName();
			}
			catch (Exception teee) {
			}				
		}
		// figure out tracking
		trackingBug.deriveValuesFromRequest(request);
	}
	catch (Exception e) {
		; // ignore
	}

	com.usatoday.businessObjects.products.promotions.PromotionSet defaultPromotionSet = null;
	//com.usatoday.businessObjects.products.promotions.PromotionSet currentOfferPromotionSet = null;
	
	com.usatoday.business.interfaces.products.promotions.ImagePromotionIntf imagePromo = null;
	
	String mobileCSS = "";
	if (MobileDeviceDetectionUtil.isSmallScreenDevice(request)) {
		mobileCSS = "<LINK href=\"/theme/themeV3/css/smallScreen.css\" rel=\"stylesheet\" type=\"text/css\">";
	}	
%>
<script language="JavaScript" src="/common/prototype/prototype.js"></script>
<script src="/common/prototype/scriptaculous/scriptaculous.js" type="text/javascript"></script>
<%-- tpl:put name="headarea_Top" --%><title>USA Today Online Subscriptions - Select Account</title><%-- /tpl:put --%>
<%-- /tpl:put --%>
<LINK REL="SHORTCUT ICON" HREF="/favicon.ico">
<LINK REL="icon" href="/favicon.ico" TYPE="image/ico" />
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META http-equiv="Content-Style-Type" content="text/css">
<meta name="viewport" content="initial-scale=1.0">
<LINK href="/theme/themeV3/css/usatodayBase.css" rel="stylesheet"
	type="text/css">
<LINK href="/theme/themeV3/css/usatodayAddOn.css" rel="stylesheet"
	type="text/css">
<script src="/common/usatCommon.js"></script>
<%-- tpl:put name="headareaBottom" --%>
<%=mobileCSS %>
<%-- tpl:put name="headarea_Bottom" --%>
<%
	String pubcode = "";
	String accountPath = "";
	try {
		pubcode = (String) session.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_PUB_NAME);	
		//accountPath = (String)session.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_ACCOUNT_PATH);
		//if (accountPath == null || accountPath.trim().length() == 0) {
			accountPath = "/account/accounthistory/accountSummary.faces";
		//}
		//else {
			session.removeAttribute(com.usatoday.esub.common.UTCommon.SESSION_ACCOUNT_PATH);
		//}
	}
	catch (Exception e) {
		}
		
%>
<%-- /tpl:put --%>
<script>			
	function initPage() {

		// for product messageing in popUpOverlay.js
		usat_productName = "<%=productName %>";
				
		<%=initFunction %>
	}	
</script>		
	<%-- /tpl:put --%>
<script type="text/javascript">
	/*   var _gaq = _gaq || [];
	 _gaq.push(['_setAccount', 'UA-23158809-1']);
	 _gaq.push(['_trackPageview']);

	 (function() {
	 var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	 ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	 var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	 })();
	 */
</script>
</HEAD>
<body>
	<DIV id="wrapper">
		<DIV id="header">
			<div id="headerLeft" onclick="templateLogoClick();"></div>
			<div id="headerRight">
				<%-- tpl:put name="headerbodyarea" --%>
					<%-- tpl:insert page="/login/loginform.jspf" --%><div style="float: none">
	<%
		String uri = request.getRequestURI();
		String pageName = uri.substring(uri.lastIndexOf("/") + 1);
		String jScript = "";
		if (uri.indexOf("subscriptions") > 0 && uri.indexOf("thankyou.jsp") > 0) {
			try {
				com.usatoday.esub.handlers.ShoppingCartHandler shoppingCartHandler = (com.usatoday.esub.handlers.ShoppingCartHandler) session
						.getAttribute("shoppingCartHandler");
				if (shoppingCartHandler != null) {
					jScript = shoppingCartHandler.getOnePageThankYouJavaScriptTealiumText();
				}
			} catch (Exception E) {

			}
		} else {
			com.usatoday.businessObjects.products.promotions.PromotionSet currentOfferPromotionSetTealium = null;
			com.usatoday.business.interfaces.products.OfferIntf offerTealium = com.usatoday.esub.common.UTCommon
					.getCurrentOffer(request);
			if (offerTealium != null && offerTealium instanceof com.usatoday.business.interfaces.products.SubscriptionOfferIntf) {
				currentOfferPromotionSetTealium = ((com.usatoday.business.interfaces.products.SubscriptionOfferIntf) offerTealium)
						.getPromotionSet();
				jScript = currentOfferPromotionSetTealium.getPromoOnePageJavaScriptTealiumText();
				jScript = org.apache.commons.lang3.StringUtils.replace(jScript, "page_name:\"\"", "page_name:\"" + uri + "\"");
			}
		}
	%>
	<%=jScript%>
</div>
<%-- 			<f:verbatim>
				<h:outputText styleClass="outputText" id="textThankYouJS13"	escape="false" value="#{shoppingCartHandler.tealiumOnePageThankYouJavaScriptText}"></h:outputText>
			</f:verbatim>
 --%>
<%
	String isLoginDisplay = "true";
	com.usatoday.business.interfaces.customer.CustomerIntf customer2 = null;
	String loggedinDivClass = "hideMe";
	String notLoggedinDivClass = "showMeInline";
	String firstName = "";
	String lastName = "";
	String firmInfo = "";
	String accountNum = "";
	String loginErrorMessage2 = "";
	String logOut = "";
	String publication = "";
	String changeAccountLink = "";
	String earlyAlertDisplay = "display:none";

	String jScriptNoOp = "javascript:;";

	try {
		publication = (String) session.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_PUB_NAME);
		if (publication == null) {
			publication = request.getParameter("pub");
			if (publication == null) {
				publication = com.usatoday.util.constants.UsaTodayConstants.UT_PUBCODE;
			}
		}
		if (publication.equals(com.usatoday.esub.common.UTCommon.PUB_NAMES[0])) { // USATODAY publication
			logOut = com.usatoday.esub.common.UTCommon.BASE_URL + "/index.jsp?pub=UT&logsessobj=rem";
		} else {
			logOut = com.usatoday.esub.common.UTCommon.BASE_URL + "/index.jsp?pub=BW&logsessobj=rem";
		}

		// If customer is logged in remove login page, otherwise display it
		customer2 = (com.usatoday.business.interfaces.customer.CustomerIntf) session
				.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_CUSTOMER_INFO);
		if (customer2 == null) {
			// showMeInling and hideMe are defined in style sheets
			loggedinDivClass = "showMeInline";
			notLoggedinDivClass = "hideMe";
		} else {

			if (customer2.getCurrentAccount() != null) {
				firstName = customer2.getCurrentAccount().getDeliveryContact().getFirstName();
				if (firstName != null && !firstName.trim().equals("")) {
					firstName = com.usatoday.esub.common.UTCommon.formatNameCapitalization(firstName); // Format it
				}
				lastName = customer2.getCurrentAccount().getDeliveryContact().getLastName();
				if (lastName != null && !lastName.trim().equals("")) {
					lastName = com.usatoday.esub.common.UTCommon.formatNameCapitalization(lastName); // Format it
				}
				firmInfo = customer2.getCurrentAccount().getDeliveryContact().getFirmName();
				if (customer2.getNumberOfAccounts() > 1) {
					changeAccountLink = "[&nbsp;<a href=\"/account/select_accnt.jsp\">Change</a>&nbsp;]";
				}
				try {
					accountNum = customer2.getCurrentAccount().getAccountNumber();
					//				if (accountNum == null || accountNum.length() < 4) {
					if (accountNum == null) {
						accountNum = "";
					}
					//				else {
					/*
					if (accountNum.equalsIgnoreCase("340000000")) {
						accountNum = "Pending Setup";
					}
					 */
					//				}
				} catch (Exception eeee) {
					eeee.printStackTrace();
				}

				//EarlyAlert - Check to see if their paper delivery has been affected by a delay
				// HD-CONS109 SEW
				/*
				if (customer2.getCurrentAccount().isShowDeliveryAlert()) {
				earlyAlertDisplay = "display:inline";
				}
				 */

			} else {
				if (customer2.getCurrentEmailRecord() != null
						&& customer2.getCurrentEmailRecord().getEmailAddress() != null) {
					firstName = customer2.getCurrentEmailRecord().getEmailAddress();
					accountNum = "Pending Setup";
				} else {
					firstName = "";
				}
			}
		}
		loginErrorMessage2 = (String) session.getAttribute("LOGIN_FAILED_MESSAGE");
		if (loginErrorMessage2 == null || loginErrorMessage2.trim().equals("")) {
			loginErrorMessage2 = "";
		} else {
			session.removeAttribute("LOGIN_FAILED_MESSAGE");
		}
	} catch (Exception e) {
		e.printStackTrace();
		if (firstName == null) {
			firstName = "";
		}
		if (lastName == null) {
			lastName = "";
		}
	}
	String nameInfo = firstName + " " + lastName;
	String loginInfo = null;
	if (!nameInfo.trim().equals("")) {
		loginInfo = nameInfo;
	} else {
		if (firmInfo != null && !firmInfo.trim().equals("")) {
			loginInfo = firmInfo;
		}
	}
	// if no login info, then hide data
	if (loginInfo == null) {
		loggedinDivClass = "showMeInline";
		notLoggedinDivClass = "hideMe";
		loginInfo = "";
	}
%>

<%@page import="com.usatoday.util.constants.UsaTodayConstants"%><div
	id="loginDisplayDiv" class="<%=loggedinDivClass%>">
	<table border="0" width="100%" height="60px">
		<tr>
			<td>
				<TABLE class="loginFormTable">
					<TBODY>
						<TR>
							<TD class="loginFormColumn"><br /> <a
								href="/login/auth.faces">Login to Manage Account</a></TD>
						</TR>
						<tr>
							<td class="loginFormColumn" align="right"><img
								style="cursor: pointer; margin-top: 10px;"
								onclick="javascript: templateClickToChatClickedGuest(this, event);"
								alt="Click for Assistance" title="Click for Assistance"
								src="${pageContext.request.contextPath}/theme/themeV3/themeImages/chatIcon136.png"
								hspace="0" vspace="5"></td>
						</tr>
					</TBODY>
				</TABLE></td>
		</tr>
	</table>
</div>
<div id="loginHideDiv" class="<%=notLoggedinDivClass%>">
	<table width="100%" border="0" class="loginFormTable">
		<tr>
			<td nowrap align="right">
				<TABLE class="loginFormTable">
					<TBODY>
						<TR>
							<TD class="loginFormColumn" nowrap><span
								class="loginNormalPrint">Hello, <%=loginInfo%>&nbsp;&nbsp;[&nbsp;<a
									href="<%=jScriptNoOp%>"
									onclick="javascript:document.location='<%=logOut%>';_unloadPage(this, event);">Logout</a>&nbsp;]</span>
							</TD>
						</TR>
					</TBODY>
				</TABLE></TD>
		</tr>
		<tr>
			<td class="loginFormColumn" align="right"><span
				class="loginfineprint">Account #:&nbsp;<%=accountNum%>&nbsp;<%=changeAccountLink%></span>
			</td>
		</tr>
		<tr>
			<td class="loginFormColumn" align="right"><div
					id="chatIconRequestDiv">
					<img onclick="javascript: templateClickToChatClicked(this, event);"
						style="cursor: pointer; margin-top: 10px;"
						alt="Click for Assistance" title="Click for Assistance"
						src="${pageContext.request.contextPath}/theme/themeV3/themeImages/chatIcon136.png"
						hspace="0" vspace="5">
				</div>
			</td>
		</tr>
	</table>
</div>
<div id="templateChatWindowDiv" style="display: none;">
	<div id="templateChatTopDiv">
		<span onclick="templateClickToChatHide();" style="cursor: pointer;">No
			Thanks<img src="/theme/themeV3/themeImages/close_14x14.png"
			width="14" height="14" border="0"
			style="margin-left: 5px; margin-right: 5px" /> </span>
	</div>
<!-- 	<div id="tempateChatImageDiv">
		<a
			onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
			href=""><img border="0"
			src="https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY" />
		</a>
	</div>
 -->	<div id="templateChatAccountLinksDiv">
		<ul>
			<li>To report a problem with delivery, click <a
				href="/account/cp/delissue.faces"> here</a>.</li>
			<li>To place your delivery on a temporary hold, click <a
				href="/account/vacationholds/holds.faces"> here</a>.</li>
			<li>Forgot your password or login? Click <a
				href="/idpassword/forgotpassword.jsp"> here</a>.
			<li>To contact us <a href="/feedback/feedback.jsp"> click here</a>.</li>				
<!-- 			<li>To cancel your subscription, please call 1-800-872-0001 and -->
<!-- 				select option 6.</li> -->
		</ul>
	</div>

	<div id="templateChatLinksDiv">
		<ul>

			<li><a
				onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
				href="">Click to Chat</a>
			</li>
		</ul>
	</div>
	<div id="templateChatBottomDiv">
		<span class="outputText">Or, call us at:<br> <b>Call
				1-877-713-6241</b><br>(M-F 8:00 a.m. to 7 p.m. E.T.)</span>
	</div>
</div>
<div id="templateChatWindowGuestDiv" style="display: none;">
	<div id="templateChatTopDiv">
		<span onclick="templateClickToChatHideGuest();"
			style="cursor: pointer;">No Thanks<img
			src="/theme/themeV3/themeImages/close_14x14.png" width="14"
			height="14" border="0" style="margin-left: 5px; margin-right: 5px" />
		</span>
	</div>
<!-- 	<div id="tempateChatImageDiv">
		<a
			onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
			href=""><img border="0"
			src="https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY" />
		</a>
	</div>
 -->	<div id="templateChatAccountLinksDiv">
		<ul>
			<li>To manage your account online - including reporting a
				problem with delivery or placing a temporary hold on your account -
				log in by entering your e-mail address and password. Click <a
				href="/login/auth.faces"> here</a> to access the login page now.</li>
			<li>Forgot your password or login? <a
				href="/idpassword/forgotpassword.jsp"> click here</a>.</li>
			<li>To contact us <a href="/feedback/feedback.jsp"> click here</a>.</li>
			<!-- 			<li>To cancel your subscription, please call 1-800-872-0001 and -->
			<!-- 				select option 6.</li> -->
		</ul>

	</div>

	<div id="templateChatLinksDiv">
		<ul>
			<li><a
				onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
				href="">Click to Chat</a>
			</li>
		</ul>
	</div>
	<div id="templateChatBottomDiv">
		<span class="outputText">Or, call us at:<br> <b>Call
				1-877-713-6241</b><br>(M-F 8:00 a.m. to 7 p.m. E.T.)</span>
	</div>
</div>
<%-- /tpl:insert --%>
		
	<%-- /tpl:put --%>
			</div>
		</DIV>
		<!-- end header -->
		<DIV id="content">
			<DIV id="whitebox">
				<%-- tpl:put name="bodyAreaMainWhiteBox" --%>
		<DIV id="pageContentFullWidth">
			<%-- tpl:put name="bodyarea" --%>
<h1>Multiple Accounts Found for E-mail/Password</h1>
<table border="0" width="100%" cellspacing="0" cellpadding="0">
	<tr>
		<td width="100%">
		<p><span class="USAT_BlueSubHeader"><font color="black">To continue,
								please select one of the following accounts:</font></span></p>
		<form method="POST" action="/MultiLogin">
		<table border="0" width="100%">
			<tr>
            	<td width="20%" bgcolor="#5AA2C6"><font color="FFFFF" font face="Arial"><b>Publication</b></td>
            	<td width="20%" bgcolor="#5AA2C6"><font color="FFFFF" font face="Arial"><b>Account Number</b></td>
            	<td width="20%" bgcolor="#5AA2C6"><font color="FFFFF" font face="Arial"><b>Delivery Name</b></td>
            	<td width="20%" bgcolor="#5AA2C6"><font color="FFFFF" font face="Arial"><b>Delivery Address</b></td>
			</tr>
			<%=this.displayMessage(request)%>
			<%=this.displayResult(request) %>
		</table>
		<p align="left"><input type="submit" value="Submit" name="B1"></p>
    <input type="hidden" name="FAILUREURL" value="/account/select_accnt.jsp">
    <input type="hidden" name="SUCCESSURL" value="<%=accountPath%>">
			
		</form>
		<hr width="100%">
		<p align="center">
		</p></td>
	</tr>
</table>
<%-- /tpl:put --%>
		</DIV>
	<%-- /tpl:put --%>
				<DIV id="clear"></DIV>
				<DIV id="bluebar">
					<a href="/feedback/feedback.jsp"
						style="color: white; font-weight: bold; text-decoration: none; font-size: 14px"
						title="Contact Us">Contact Us</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;1-800-872-0001&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a
						href="/faq/utfaq.jsp"
						style="color: white; font-weight: bold; text-decoration: none; font-size: 14px"
						title="Frequently Asked Questions">FAQ</a>
				</DIV>
				<%-- tpl:put name="footerMainArea" --%>
	<%-- tpl:put name="footerarea1" --%><!-- Insert Footer Ad Here --><%-- /tpl:put --%>
		<%=trackingBug.getOmnitureHTML() %>
	<%-- /tpl:put --%>
				<DIV id=graybar>
					Copyright
					<script>
						document.write(getCurrentYear());
					</script>
					USA TODAY, a division of <a href="http://www.gannett.com">Gannett
						Co. Inc.</a> <a href="/privacy/privacy.htm">Privacy
						Notice/California Privacy Notice</a>. <a
						href="/privacy/privacy.htm#adchoice">Ad Choices</a>.<br> By
					using this service, you accept our <a href="/service/service.jsp">Terms
						of Service</a>.
				</DIV>
				<DIV id=copyright>
					<P>Offers available to new subscribers only. Home delivery not
						available in some areas. Not valid with any other offer.
						Prepayment required. After the promotional period ends, the
						subscription will continue and be billed monthly at the then
						regular rate, less any applicable credits, unless USA TODAY is
						otherwise notified. Applicable taxes may not be included in the
						rate. If at any time you decide to cancel your subscription, you
						may contact customer service at 1-800-872-0001 and the full amount
						of any balance over $2.00 will be returned.</P>
				</DIV>
				<!--END OF COPYWRITE-->
				<DIV id=footer></DIV>
			</DIV>
			<!--END OF WHITEBOX-->
		</DIV>
		<!--END OF CONTENT-->
	</DIV>
</BODY>
</HTML><%-- /tpl:insert --%><%-- /tpl:insert --%>