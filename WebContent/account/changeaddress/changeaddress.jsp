<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/account/changeaddress/Changeaddress.java" --%><%-- /jsf:pagecode --%><%-- tpl:insert page="/theme/themeV3/jsfTheme/subscriberPortalLeftNavAccount_JSF.jtpl" --%><!DOCTYPE HTML><%@page import="com.usatoday.business.interfaces.products.ProductIntf"%>
<%@page import="com.usatoday.businessObjects.products.ProductBO"%>
<%@page import="com.usatoday.util.MobileDeviceDetectionUtil"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%><%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%><%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<f:view><f:loadBundle basename="resourceFile" var="labels" /><%-- tpl:insert page="/theme/subscriptionPortalRoot.htpl" --%><HTML>
<HEAD>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8">
<%-- tpl:put name="headerareaTop" --%><%
	String docProtocol = "https:"; 
	if (!request.isSecure()) {
		docProtocol = "http:";
	}
	// following work around to get around problem with RAD 8.0.2 automated links in templates - APE
	String subcribeJS = "javascript:_subscribeToPub();"; 
	String backIssueJS = "javascript:_shopUSAT();";
	
	// change this to any javascript you want called on page load
	String initFunction = "";
	String dynamicNavigation = "";
	
	com.usatoday.esub.common.WebBug trackingBug = com.usatoday.esub.common.UTCommon.getWebAnalyticsBug();
	try {
		trackingBug.deriveValuesFromRequest(request);
	}
	catch (Exception e) {
		; // ignore
	}

	// not shown if electronic pub   
	String suspendResumeDeliveryNavOption = "<LI><A href=\"/account/vacationholds/holds.faces\">Suspend/Resume delivery</A></LI>";
	String suspendResumeDeliveryMobileOption = "<option value=\"/account/vacationholds/holds.faces\">Suspend delivery</option><option value=\"/account/vacationholds/holds.faces\">Resume delivery</option>";
	
	String reportDeliveryProblemNavOption = "<LI><A href=\"/account/cp/delissue.faces\">Report delivery problem</A></LI>";
	String reportDeliveryProblemMobileOption = "<option value=\"/account/cp/delissue.faces\">Report a problem</option>";
	
	String cancelSubscriptionNavOption = "";
	String cancelSubscriptionMobileOption = "";

	String changeAccountLinkMobile = "";
		
	String pubCode = "";
	String productName = "USA TODAY";
	 
    boolean isEEditionProduct = true;

	com.usatoday.businessObjects.products.promotions.PromotionSet defaultPromotionSet = null;
	com.usatoday.businessObjects.products.promotions.PromotionSet currentOfferPromotionSet = null;
	com.usatoday.business.interfaces.customer.CustomerIntf customer = null;
   	com.usatoday.businessObjects.products.promotions.PromotionSet currentAccountPromotionSet = null;
   		
	com.usatoday.business.interfaces.products.OfferIntf offer = null;   		
	try {
		offer = com.usatoday.esub.common.UTCommon.getCurrentOfferVersion2(request);
		pubCode = com.usatoday.util.constants.UsaTodayConstants.UT_PUBCODE;
		if (offer != null) {
			pubCode = offer.getPubCode();
		}
		// get the default promotion set for the publication		
		defaultPromotionSet = com.usatoday.esub.common.UTCommon.getDefaultPromotionSetForPub(pubCode);
		
		// get the keycode specific promotion set
		
		if (offer != null && offer instanceof com.usatoday.business.interfaces.products.SubscriptionOfferIntf) {
			currentOfferPromotionSet = ((com.usatoday.business.interfaces.products.SubscriptionOfferIntf)offer).getPromotionSet();
		}
		
		if (currentOfferPromotionSet != null) {
			com.usatoday.business.interfaces.products.promotions.HTMLPromotionIntf dPromo = currentOfferPromotionSet.getDynamicNavigationCustServiceHTML();
			if (dPromo != null) {
				dynamicNavigation = dPromo.getPromotionalHTML();
			}
		}
				
		boolean isSignedIn = false;
		customer = (com.usatoday.business.interfaces.customer.CustomerIntf)session.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_CUSTOMER_INFO);
		if (customer != null) {
			isSignedIn = true;
			com.usatoday.business.interfaces.products.SubscriptionProductIntf product = com.usatoday.businessObjects.products.SubscriptionProductBO.getSubscriptionProduct(customer.getCurrentEmailRecord().getPubCode());
			isEEditionProduct = product.isElectronicDelivery(); 
			// This promotion set is based on pub and keycode on the customer account
			currentAccountPromotionSet = com.usatoday.businessObjects.products.promotions.PromotionManager.getInstance().getPromotionsForOffer(customer.getCurrentAccount().getPubCode(), customer.getCurrentAccount().getKeyCode());
			if (isEEditionProduct) {
				suspendResumeDeliveryNavOption = ""; // clear vacation holds option
				suspendResumeDeliveryMobileOption = "";
				reportDeliveryProblemNavOption = ""; // clear deliveyr problem link
				reportDeliveryProblemMobileOption = "";
//				cancelSubscriptionNavOption = "<LI><A href=\"/account/cancels/cancelSubscription.faces\">Cancel subscription</A></LI>"; // show cancels 			
//				cancelSubscriptionMobileOption = "<option value=\"/account/cancels/cancelSubscription.faces\">Cancel subscription</option>"; 
			}
			
			try {
				String brandingPubCode = product.getBrandingPubCode();
				ProductIntf bp = ProductBO.fetchProduct(brandingPubCode);
				productName = bp.getName();
			}
			catch (Exception teee) {
			}							
			
			if (customer.getNumberOfAccounts() > 1) {
				changeAccountLinkMobile = "<option value=\"/account/select_accnt.jsp\">Change Account</option>";
			}
			
		}
				
	} catch (Exception e) {
	
	}
	String mobileCSS = "";
	if (MobileDeviceDetectionUtil.isSmallScreenDevice(request)) {
		mobileCSS = "<LINK href=\"/theme/themeV3/css/smallScreen.css\" rel=\"stylesheet\" type=\"text/css\">";
	}
 %>
<script src="/common/prototype/prototype.js"></script>
<script src="/common/prototype/scriptaculous/scriptaculous.js" type="text/javascript"></script>
<link rel="stylesheet"	href="/theme/themeV3/css/stylesheet_jsfcomponents.css"	type="text/css">
<%-- tpl:put name="headerarea_Top" --%>
<%
	initFunction += "initThisPage(thisObj, thisEvent);";
%>
<title>Change Delivery Address</title>
<script src="/common/trimString.js"></script>
<script type="text/javascript">

function initThisPage(thisObj, thisEvent) {
	func_1(thisObj, thisEvent);
}
function func_1(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'

	if ($('formChangeDeliveryAddress:AdditionalAddressRadio1') != null) {
// Unfortunately, looping through the radiobutton has not worked. Radiobutton[] keeps being undefined 
//		for (var i = 0;  i < radioButton.length;  i++)

		if ($('formChangeDeliveryAddress:AdditionalAddressRadio1:0') != null) {
			$('formChangeDeliveryAddress:AdditionalAddressRadio1:0').checked = false;
		}
		if ($('formChangeDeliveryAddress:AdditionalAddressRadio1:1') != null) {
			$('formChangeDeliveryAddress:AdditionalAddressRadio1:1').checked = false;
		}
		if ($('formChangeDeliveryAddress:AdditionalAddressRadio1:2') != null) {
			$('formChangeDeliveryAddress:AdditionalAddressRadio1:2').checked = false;
		}
		if ($('formChangeDeliveryAddress:AdditionalAddressRadio1:3') != null) {
			$('formChangeDeliveryAddress:AdditionalAddressRadio1:3').checked = false;
		}
		if ($('formChangeDeliveryAddress:AdditionalAddressRadio1:4') != null) {
			$('formChangeDeliveryAddress:AdditionalAddressRadio1:4').checked = false;
		}
		if ($('formChangeDeliveryAddress:AdditionalAddressRadio1:5') != null) {
			$('formChangeDeliveryAddress:AdditionalAddressRadio1:5').checked = false;
		}
		if ($('formChangeDeliveryAddress:AdditionalAddressRadio1:6') != null) {
			$('formChangeDeliveryAddress:AdditionalAddressRadio1:6').checked = false;
		}
		if ($('formChangeDeliveryAddress:AdditionalAddressRadio1:7') != null) {
			$('formChangeDeliveryAddress:AdditionalAddressRadio1:7').checked = false;
		}
		if ($('formChangeDeliveryAddress:AdditionalAddressRadio1:8') != null) {
			$('formChangeDeliveryAddress:AdditionalAddressRadio1:8').checked = false;
		}
		if ($('formChangeDeliveryAddress:AdditionalAddressRadio1:9') != null) {
			$('formChangeDeliveryAddress:AdditionalAddressRadio1:9').checked = false;
		}
	}
	// In case customer blanks these out, this will cause the blanks instead of the current values to be used
	if (trim($('formChangeDeliveryAddress:FirmName').value) == "" ) {
		$('formChangeDeliveryAddress:FirmName').value = "   "; 		
	}
	if (trim($('formChangeDeliveryAddress:AptDeptSuite').value) == "" ) {
		$('formChangeDeliveryAddress:AptDeptSuite').value = "   "; 		
	}
	if (trim($('formChangeDeliveryAddress:AdditionalAddress1').value) == "" ) {
		$('formChangeDeliveryAddress:AdditionalAddress1').value = "   "; 		
	}
	validateState();
}

function validateState(){
	var stateVar = $('formChangeDeliveryAddress:menuDeliveryState');
	var pub = "<%=pubCode %>";
	var isEEdition = "<%=isEEditionProduct%>";
	if (pub == "UT" && isEEdition == "false" && stateVar.value == "HI") {
		var alertMsg = "All subscriptions to USA TODAY delivered in the state of\n";
			alertMsg = alertMsg + "Hawaii are handled by our partner, the Honolulu\n";
			alertMsg = alertMsg + "Star-Advertiser.\n\n";
			alertMsg = alertMsg + "Therefore, moving your delivery of USA TODAY from its current\n";
			alertMsg = alertMsg + "location to the state of Hawaii, is a two steps process:\n"; 
			alertMsg = alertMsg + "1)\t Purchase a new USA TODAY subscription from the Honolulu\n";
			alertMsg = alertMsg + "\t Star-Advertiser for delivery in Hawaii.\n";
			alertMsg = alertMsg + "\t a.\t For information about receiving delivery of\n";
			alertMsg = alertMsg + "\t\t USA TODAY in Hawaii, including pricing\n";
			alertMsg = alertMsg + "\t\t (which may vary from what is shown here), please\n";
			alertMsg = alertMsg + "\t\t contact the Honolulu Star-Advertiser\'s\n";
			alertMsg = alertMsg + "\t\t Customer Service Department by calling\n";
			alertMsg = alertMsg + "\t\t \"808-538-NEWS (6397)\" from 5:30am to 5:00 pm\n";
			alertMsg = alertMsg + "\t\t Monday-Friday and 6:30am to 10:00 am\n";
			alertMsg = alertMsg + "\t\t on weekends (all times HST).\n";
			alertMsg = alertMsg + "2)\t Stop delivery on the current subscription to USA TODAY\n";
			alertMsg = alertMsg + "\t which is being delivered to a state other than Hawaii.\n";
			alertMsg = alertMsg + "\t a.\t If you are permanently leaving your current\n";
			alertMsg = alertMsg + "\t\t location, please call USA TODAY\'s National\n";
			alertMsg = alertMsg + "\t\t Customer Service Center and request to cancel\n";
			alertMsg = alertMsg + "\t\t your subscription.  This can be done by calling\n";
			alertMsg = alertMsg + "\t\t 1-800-872-0001 and Monday thru Friday from\n";
			alertMsg = alertMsg + "\t\t 8:00 a.m. to 7:00 p.m. EST.  When you call, please\n";
			alertMsg = alertMsg + "\t\t select option 6 to speak with a customer service\n";
			alertMsg = alertMsg + "\t\t representative who can assist you.\n";  
			alertMsg = alertMsg + "\t b.\t If your move from your current location is only\n";
			alertMsg = alertMsg + "\t\t temporary, you may place a temporary hold on\n";
			alertMsg = alertMsg + "\t\t the current delivery. This can be done by clicking\n";
			alertMsg = alertMsg + "\t\t the \"Suspend/Resume delivery\" link or by calling\n";
			alertMsg = alertMsg + "\t\t 1-800-872-0001.";
		alert(alertMsg);
		stateVar.value = "";
		stateVar.focus();
		stateVar.select();
	}
}

//	radioButton.ClearSelection();

</script><%-- /tpl:put --%>
<%-- /tpl:put --%>
<LINK REL="SHORTCUT ICON" HREF="/favicon.ico">
<LINK REL="icon" href="/favicon.ico" TYPE="image/ico" />
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META http-equiv="Content-Style-Type" content="text/css">
<meta name="viewport" content="initial-scale=1.0">
<LINK href="/theme/themeV3/css/usatodayBase.css" rel="stylesheet"
	type="text/css">
<LINK href="/theme/themeV3/css/usatodayAddOn.css" rel="stylesheet"
	type="text/css">
<script src="/common/usatCommon.js"></script>
<%-- tpl:put name="headareaBottom" --%>
<%=mobileCSS %>
<%-- tpl:put name="headarea_Bottom" --%>
					<!-- Bottom Header area -->
				<%-- /tpl:put --%>
<script>
// method called on page load
function initPage(thisObj, thisEvent) {
	// insert site-wide code here

	// for product messageing in popUpOverlay.js
	usat_productName = "<%=productName %>";

	<%=initFunction %>
}
</script>
<%-- /tpl:put --%>
<script type="text/javascript">
	/*   var _gaq = _gaq || [];
	 _gaq.push(['_setAccount', 'UA-23158809-1']);
	 _gaq.push(['_trackPageview']);

	 (function() {
	 var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	 ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	 var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	 })();
	 */
</script>
</HEAD>
<body>
	<DIV id="wrapper">
		<DIV id="header">
			<div id="headerLeft" onclick="templateLogoClick();"></div>
			<div id="headerRight">
				<%-- tpl:put name="headerbodyarea" --%>
					<%-- tpl:insert page="/login/loginform.jspf" --%><div style="float: none">
	<%
		String uri = request.getRequestURI();
		String pageName = uri.substring(uri.lastIndexOf("/") + 1);
		String jScript = "";
		if (uri.indexOf("subscriptions") > 0 && uri.indexOf("thankyou.jsp") > 0) {
			try {
				com.usatoday.esub.handlers.ShoppingCartHandler shoppingCartHandler = (com.usatoday.esub.handlers.ShoppingCartHandler) session
						.getAttribute("shoppingCartHandler");
				if (shoppingCartHandler != null) {
					jScript = shoppingCartHandler.getOnePageThankYouJavaScriptTealiumText();
				}
			} catch (Exception E) {

			}
		} else {
			com.usatoday.businessObjects.products.promotions.PromotionSet currentOfferPromotionSetTealium = null;
			com.usatoday.business.interfaces.products.OfferIntf offerTealium = com.usatoday.esub.common.UTCommon
					.getCurrentOffer(request);
			if (offerTealium != null && offerTealium instanceof com.usatoday.business.interfaces.products.SubscriptionOfferIntf) {
				currentOfferPromotionSetTealium = ((com.usatoday.business.interfaces.products.SubscriptionOfferIntf) offerTealium)
						.getPromotionSet();
				jScript = currentOfferPromotionSetTealium.getPromoOnePageJavaScriptTealiumText();
				jScript = org.apache.commons.lang3.StringUtils.replace(jScript, "page_name:\"\"", "page_name:\"" + uri + "\"");
			}
		}
	%>
	<%=jScript%>
</div>
<%-- 			<f:verbatim>
				<h:outputText styleClass="outputText" id="textThankYouJS13"	escape="false" value="#{shoppingCartHandler.tealiumOnePageThankYouJavaScriptText}"></h:outputText>
			</f:verbatim>
 --%>
<%
	String isLoginDisplay = "true";
	com.usatoday.business.interfaces.customer.CustomerIntf customer2 = null;
	String loggedinDivClass = "hideMe";
	String notLoggedinDivClass = "showMeInline";
	String firstName = "";
	String lastName = "";
	String firmInfo = "";
	String accountNum = "";
	String loginErrorMessage2 = "";
	String logOut = "";
	String publication = "";
	String changeAccountLink = "";
	String earlyAlertDisplay = "display:none";

	String jScriptNoOp = "javascript:;";

	try {
		publication = (String) session.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_PUB_NAME);
		if (publication == null) {
			publication = request.getParameter("pub");
			if (publication == null) {
				publication = com.usatoday.util.constants.UsaTodayConstants.UT_PUBCODE;
			}
		}
		if (publication.equals(com.usatoday.esub.common.UTCommon.PUB_NAMES[0])) { // USATODAY publication
			logOut = com.usatoday.esub.common.UTCommon.BASE_URL + "/index.jsp?pub=UT&logsessobj=rem";
		} else {
			logOut = com.usatoday.esub.common.UTCommon.BASE_URL + "/index.jsp?pub=BW&logsessobj=rem";
		}

		// If customer is logged in remove login page, otherwise display it
		customer2 = (com.usatoday.business.interfaces.customer.CustomerIntf) session
				.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_CUSTOMER_INFO);
		if (customer2 == null) {
			// showMeInling and hideMe are defined in style sheets
			loggedinDivClass = "showMeInline";
			notLoggedinDivClass = "hideMe";
		} else {

			if (customer2.getCurrentAccount() != null) {
				firstName = customer2.getCurrentAccount().getDeliveryContact().getFirstName();
				if (firstName != null && !firstName.trim().equals("")) {
					firstName = com.usatoday.esub.common.UTCommon.formatNameCapitalization(firstName); // Format it
				}
				lastName = customer2.getCurrentAccount().getDeliveryContact().getLastName();
				if (lastName != null && !lastName.trim().equals("")) {
					lastName = com.usatoday.esub.common.UTCommon.formatNameCapitalization(lastName); // Format it
				}
				firmInfo = customer2.getCurrentAccount().getDeliveryContact().getFirmName();
				if (customer2.getNumberOfAccounts() > 1) {
					changeAccountLink = "[&nbsp;<a href=\"/account/select_accnt.jsp\">Change</a>&nbsp;]";
				}
				try {
					accountNum = customer2.getCurrentAccount().getAccountNumber();
					//				if (accountNum == null || accountNum.length() < 4) {
					if (accountNum == null) {
						accountNum = "";
					}
					//				else {
					/*
					if (accountNum.equalsIgnoreCase("340000000")) {
						accountNum = "Pending Setup";
					}
					 */
					//				}
				} catch (Exception eeee) {
					eeee.printStackTrace();
				}

				//EarlyAlert - Check to see if their paper delivery has been affected by a delay
				// HD-CONS109 SEW
				/*
				if (customer2.getCurrentAccount().isShowDeliveryAlert()) {
				earlyAlertDisplay = "display:inline";
				}
				 */

			} else {
				if (customer2.getCurrentEmailRecord() != null
						&& customer2.getCurrentEmailRecord().getEmailAddress() != null) {
					firstName = customer2.getCurrentEmailRecord().getEmailAddress();
					accountNum = "Pending Setup";
				} else {
					firstName = "";
				}
			}
		}
		loginErrorMessage2 = (String) session.getAttribute("LOGIN_FAILED_MESSAGE");
		if (loginErrorMessage2 == null || loginErrorMessage2.trim().equals("")) {
			loginErrorMessage2 = "";
		} else {
			session.removeAttribute("LOGIN_FAILED_MESSAGE");
		}
	} catch (Exception e) {
		e.printStackTrace();
		if (firstName == null) {
			firstName = "";
		}
		if (lastName == null) {
			lastName = "";
		}
	}
	String nameInfo = firstName + " " + lastName;
	String loginInfo = null;
	if (!nameInfo.trim().equals("")) {
		loginInfo = nameInfo;
	} else {
		if (firmInfo != null && !firmInfo.trim().equals("")) {
			loginInfo = firmInfo;
		}
	}
	// if no login info, then hide data
	if (loginInfo == null) {
		loggedinDivClass = "showMeInline";
		notLoggedinDivClass = "hideMe";
		loginInfo = "";
	}
%>

<%@page import="com.usatoday.util.constants.UsaTodayConstants"%><div
	id="loginDisplayDiv" class="<%=loggedinDivClass%>">
	<table border="0" width="100%" height="60px">
		<tr>
			<td>
				<TABLE class="loginFormTable">
					<TBODY>
						<TR>
							<TD class="loginFormColumn"><br /> <a
								href="/login/auth.faces">Login to Manage Account</a></TD>
						</TR>
						<tr>
							<td class="loginFormColumn" align="right"><img
								style="cursor: pointer; margin-top: 10px;"
								onclick="javascript: templateClickToChatClickedGuest(this, event);"
								alt="Click for Assistance" title="Click for Assistance"
								src="${pageContext.request.contextPath}/theme/themeV3/themeImages/chatIcon136.png"
								hspace="0" vspace="5"></td>
						</tr>
					</TBODY>
				</TABLE></td>
		</tr>
	</table>
</div>
<div id="loginHideDiv" class="<%=notLoggedinDivClass%>">
	<table width="100%" border="0" class="loginFormTable">
		<tr>
			<td nowrap align="right">
				<TABLE class="loginFormTable">
					<TBODY>
						<TR>
							<TD class="loginFormColumn" nowrap><span
								class="loginNormalPrint">Hello, <%=loginInfo%>&nbsp;&nbsp;[&nbsp;<a
									href="<%=jScriptNoOp%>"
									onclick="javascript:document.location='<%=logOut%>';_unloadPage(this, event);">Logout</a>&nbsp;]</span>
							</TD>
						</TR>
					</TBODY>
				</TABLE></TD>
		</tr>
		<tr>
			<td class="loginFormColumn" align="right"><span
				class="loginfineprint">Account #:&nbsp;<%=accountNum%>&nbsp;<%=changeAccountLink%></span>
			</td>
		</tr>
		<tr>
			<td class="loginFormColumn" align="right"><div
					id="chatIconRequestDiv">
					<img onclick="javascript: templateClickToChatClicked(this, event);"
						style="cursor: pointer; margin-top: 10px;"
						alt="Click for Assistance" title="Click for Assistance"
						src="${pageContext.request.contextPath}/theme/themeV3/themeImages/chatIcon136.png"
						hspace="0" vspace="5">
				</div>
			</td>
		</tr>
	</table>
</div>
<div id="templateChatWindowDiv" style="display: none;">
	<div id="templateChatTopDiv">
		<span onclick="templateClickToChatHide();" style="cursor: pointer;">No
			Thanks<img src="/theme/themeV3/themeImages/close_14x14.png"
			width="14" height="14" border="0"
			style="margin-left: 5px; margin-right: 5px" /> </span>
	</div>
<!-- 	<div id="tempateChatImageDiv">
		<a
			onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
			href=""><img border="0"
			src="https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY" />
		</a>
	</div>
 -->	<div id="templateChatAccountLinksDiv">
		<ul>
			<li>To report a problem with delivery, click <a
				href="/account/cp/delissue.faces"> here</a>.</li>
			<li>To place your delivery on a temporary hold, click <a
				href="/account/vacationholds/holds.faces"> here</a>.</li>
			<li>Forgot your password or login? Click <a
				href="/idpassword/forgotpassword.jsp"> here</a>.
			<li>To contact us <a href="/feedback/feedback.jsp"> click here</a>.</li>				
<!-- 			<li>To cancel your subscription, please call 1-800-872-0001 and -->
<!-- 				select option 6.</li> -->
		</ul>
	</div>

	<div id="templateChatLinksDiv">
		<ul>

			<li><a
				onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
				href="">Click to Chat</a>
			</li>
		</ul>
	</div>
	<div id="templateChatBottomDiv">
		<span class="outputText">Or, call us at:<br> <b>Call
				1-877-713-6241</b><br>(M-F 8:00 a.m. to 7 p.m. E.T.)</span>
	</div>
</div>
<div id="templateChatWindowGuestDiv" style="display: none;">
	<div id="templateChatTopDiv">
		<span onclick="templateClickToChatHideGuest();"
			style="cursor: pointer;">No Thanks<img
			src="/theme/themeV3/themeImages/close_14x14.png" width="14"
			height="14" border="0" style="margin-left: 5px; margin-right: 5px" />
		</span>
	</div>
<!-- 	<div id="tempateChatImageDiv">
		<a
			onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
			href=""><img border="0"
			src="https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY" />
		</a>
	</div>
 -->	<div id="templateChatAccountLinksDiv">
		<ul>
			<li>To manage your account online - including reporting a
				problem with delivery or placing a temporary hold on your account -
				log in by entering your e-mail address and password. Click <a
				href="/login/auth.faces"> here</a> to access the login page now.</li>
			<li>Forgot your password or login? <a
				href="/idpassword/forgotpassword.jsp"> click here</a>.</li>
			<li>To contact us <a href="/feedback/feedback.jsp"> click here</a>.</li>
			<!-- 			<li>To cancel your subscription, please call 1-800-872-0001 and -->
			<!-- 				select option 6.</li> -->
		</ul>

	</div>

	<div id="templateChatLinksDiv">
		<ul>
			<li><a
				onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
				href="">Click to Chat</a>
			</li>
		</ul>
	</div>
	<div id="templateChatBottomDiv">
		<span class="outputText">Or, call us at:<br> <b>Call
				1-877-713-6241</b><br>(M-F 8:00 a.m. to 7 p.m. E.T.)</span>
	</div>
</div>
<%-- /tpl:insert --%>
		<%-- /tpl:put --%>
			</div>
		</DIV>
		<!-- end header -->
		<DIV id="content">
			<DIV id="whitebox">
				<%-- tpl:put name="bodyAreaMainWhiteBox" --%>
<div id="mobileAccountNav">
	<h4>My Account Options:</h4>
	<select name="accountNavLinksSelect" onchange="javascript: mobileNavChanged(this, event);">
		<option value="noop" selected="selected">&nbsp;</option>
	 	<option value="/account/accounthistory/accountSummary.faces">Account Information</option>
		<option value="/custserviceindex.jsp?custopt=RENEW">Pay Bill</option>
		<%=suspendResumeDeliveryMobileOption %>
		<%=reportDeliveryProblemMobileOption %>
		<option value="/custserviceindex.jsp?custopt=DELA">Change delivery info</option>
		<option value="/custserviceindex.jsp?custopt=BILA">Change billing info</option>
		<option value="/idpassword/changeIDpassword.jsp">Change email</option>
		<option value="/idpassword/changeIDpassword.jsp">Change password</option>
		<option value="/welcome.jsp">Give a gift</option>
		<option value="http://onlinestore.usatoday.com">Order back issues</option>
		<%=cancelSubscriptionMobileOption %>
		<%=changeAccountLinkMobile %>
		<option value="<%=logOut%>">Log Out</option>
	</select>
	<hr />	
</div>
	<DIV id=nav>
	<DIV id="navPromo">
		<%-- tpl:put name="navigationArea_Top" --%>
				<%-- /tpl:put --%>
	</div>
	<DIV id=leftNavList>
		<UL>
		  <LI><A href="/account/accounthistory/accountSummary.faces">Account information</A></LI>
		  <LI><A href="/custserviceindex.jsp?custopt=RENEW">Pay Bill</A></LI>
		  <%=suspendResumeDeliveryNavOption %>
		  <%=reportDeliveryProblemNavOption %>
		  <LI><A href="/custserviceindex.jsp?custopt=DELA">Change delivery info</A></LI>
		  <LI><A href="/custserviceindex.jsp?custopt=BILA">Change billing info</A></LI>
		  <LI><A href="/idpassword/changeIDpassword.jsp">Change e-mail/password</A></LI>
		  <LI><A href="<%=subcribeJS%>">Give a gift</A></LI>
		  <LI><A href="<%=backIssueJS%>">Order back issues</A></LI>
		  <%=cancelSubscriptionNavOption %>
		  <%=dynamicNavigation %>
		</UL>
	</DIV>
	<DIV id="navPromo">
		<%-- tpl:put name="navigationArea_Bottom" --%><%-- /tpl:put --%>
	</div>
</DIV>
<DIV id=pageContent>
	<%-- tpl:put name="MainBodyArea" --%>
							<h1>Change Delivery Information</h1>
							<hx:scriptCollector id="scriptCollector1"
								preRender="#{pc_Changeaddressjsf.onPageLoadBegin}">
								<h:form styleClass="form" id="formChangeDeliveryAddress">
									<hx:panelLayout styleClass="panelLayout" id="layout1">
										<f:facet name="body"></f:facet>
										<f:facet name="left"></f:facet>
										<f:facet name="right"></f:facet>
										<f:facet name="bottom"></f:facet>
										<f:facet name="top">
											<h:messages styleClass="messages" id="messagesAllMesssages"
												layout="list"></h:messages>
										</f:facet>
									</hx:panelLayout>
									<hx:panelSection styleClass="panelSection" id="section1"
										title="New Delivery Information" initClosed="false">
										<f:facet name="closed">
											<hx:jspPanel id="jspPanel2">
												<h:outputText id="text3" styleClass="outputText"
													value="Click to Enter Delivery Information"></h:outputText>
											</hx:jspPanel>
										</f:facet>
										<f:facet name="opened">
											<hx:jspPanel id="jspPanel1">
												<h:outputText id="text2" styleClass="outputText"
													value="Enter Delivery Information"
													rendered="#{changeDeliveryAddressHandler.additionalDeliveryAddressExist}"></h:outputText>
											</hx:jspPanel>
										</f:facet>
										<hx:jspPanel id="jspPanel3">
											<h:panelGrid styleClass="panelGrid" id="grid5" width="600">
												<hx:panelFormBox id="formboxDeliveryInformation"
													styleClass="panelFormBox" labelPosition="left"
													widthLabel="155" helpPosition="under">
													<hx:formItem styleClass="formItem"
														id="formAccountNumberLabel"
														label="#{labels.accountNumberLabel} #{labels.colonLabel} &nbsp;&nbsp;">
														<h:outputText styleClass="outputText" id="AccountNumber"
															value="#{customerHandler.currentAccount.account.accountNumber}">
														</h:outputText>
													</hx:formItem>
													<hx:formItem styleClass="formItem" id="formFirstNameLabel"
														label="#{labels.firstNameLabel} #{labels.colonLabel} #{labels.requiredLabel}"
														errorText="#{labels.requiredFieldMsg}">
														<h:inputText styleClass="inputText" id="FirstName"
															required="true"
															value="#{changeDeliveryAddressHandler.firstName}"
															requiredMessage="#{labels.firstNameRequired}"
															onchange="return func_1(this, event);" size="20"
															tabindex="1" maxlength="15">
															<f:validateLength maximum="15"></f:validateLength>
															<hx:inputHelperAssist errorClass="inputText_Error" autoTab="true"
																id="assist9" />
														</h:inputText>
													</hx:formItem>
													<hx:formItem styleClass="formItem" id="formLastNameLabel"
														label="#{labels.lastNameLabel}  #{labels.colonLabel} #{labels.requiredLabel}"
														errorText="#{labels.requiredFieldMsg}">
														<h:inputText styleClass="inputText" id="LastName"
															value="#{changeDeliveryAddressHandler.lastName}"
															required="true"
															requiredMessage="#{labels.lastNameRequired}"
															onchange="return func_1(this, event);" size="20"
															tabindex="2" maxlength="20">
															<f:validateLength maximum="20"></f:validateLength>
															<hx:inputHelperAssist errorClass="inputText_Error" autoTab="true"
																id="assist10" />
														</h:inputText>
													</hx:formItem>
													<hx:formItem styleClass="formItem" id="formFirmNameLabel"
														label="#{labels.companyNameLabel} #{labels.colonLabel} &nbsp;&nbsp;">
														<h:inputText styleClass="inputText" id="FirmName"
															value="#{changeDeliveryAddressHandler.firmName}"
															onchange="return func_1(this, event);" size="35"
															tabindex="3" maxlength="40">
															<f:validateLength maximum="40"></f:validateLength>
															<hx:inputHelperAssist errorClass="inputText_Error" autoTab="true"
																id="assist11" />
														</h:inputText>
													</hx:formItem>
													<hx:formItem styleClass="formItem"
														id="formStreetAddressLabel"
														label="#{labels.StreetAddress}  #{labels.colonLabel} #{labels.requiredLabel}"
														errorText="#{labels.requiredFieldMsg}">
														<h:inputText styleClass="inputText" id="StreetAddress"
															required="true"
															value="#{changeDeliveryAddressHandler.streetAddress}"
															requiredMessage="#{labels.address1Required}"
															validatorMessage="#{labels.addressInvalid}"
															onchange="return func_1(this, event);" size="35"
															tabindex="4">
															<f:validateLength maximum="28"></f:validateLength>
														</h:inputText>
													</hx:formItem>
													<hx:formItem styleClass="formItem" id="formAptSuiteLabel"
														label="#{labels.apartmentSuiteLabel} #{labels.colonLabel} &nbsp;&nbsp;">
														<h:inputText styleClass="inputText" id="AptDeptSuite"
															value="#{changeDeliveryAddressHandler.aptDeptSuite}"
															onchange="return func_1(this, event);" size="35"
															tabindex="5">
														</h:inputText>
													</hx:formItem>
													<hx:formItem styleClass="formItem" id="formAddlAddrLabel"
														label="#{labels.personalDeliveryInfolabel}" rendered="true">
														<h:inputText styleClass="inputText"
															id="AdditionalAddress1"
															value="#{changeDeliveryAddressHandler.addlAddr1}"
															onchange="return func_1(this, event);" size="40"
															tabindex="6" maxlength="35">
															<f:validateLength maximum="35"></f:validateLength>
															<hx:inputHelperAssist errorClass="inputText_Error"
																autoTab="true" id="assist4" />
														</h:inputText>
													</hx:formItem>
													<hx:formItem styleClass="formItem" id="formCityLabel"
														label="#{labels.city} #{labels.colonLabel} #{labels.requiredLabel}"
														errorText="#{labels.requiredFieldMsg}">
														<h:inputText styleClass="inputText" id="City"
															value="#{changeDeliveryAddressHandler.city}"
															requiredMessage="City is Required"
															validatorMessage="#{labels.cityInvalid}" required="true"
															onchange="return func_1(this, event);" size="35"
															tabindex="7">
															<f:validateLength maximum="28"></f:validateLength>
														</h:inputText>
													</hx:formItem>
													<hx:formItem styleClass="formItem" id="formStateLabel"
														label="#{labels.State} #{labels.colonLabel} #{labels.requiredLabel}"
														errorText="#{labels.requiredFieldMsg}">
														<h:selectOneMenu styleClass="selectOneMenu"
															id="menuDeliveryState"
															requiredMessage="#{labels.StateRequired}" required="true"
															value="#{changeDeliveryAddressHandler.state}"
															onchange="return func_1(this, event);" tabindex="8">
															<f:selectItem itemLabel="" />
															<f:selectItem itemValue="AL" itemLabel="Alabama" />
															<f:selectItem itemValue="AK" itemLabel="Alaska" />
															<f:selectItem itemValue="AZ" itemLabel="Arizona" />
															<f:selectItem itemValue="AR" itemLabel="Arkansas" />
															<f:selectItem itemValue="AA"
																itemLabel="(AA) Armed Forces Americas" />
															<f:selectItem itemValue="AE"
																itemLabel="(AE) Armed Forces Africa" />
															<f:selectItem itemValue="AE"
																itemLabel="(AE) Armed Forces Canada" />
															<f:selectItem itemValue="AE"
																itemLabel="(AE) Armed Forces Europe" />
															<f:selectItem itemValue="AE"
																itemLabel="(AE) Armed Forces Middle East" />
															<f:selectItem itemValue="AP"
																itemLabel="(AP) Armed Forces Pacific" />
															<f:selectItem itemValue="CA" itemLabel="California" />
															<f:selectItem itemValue="CO" itemLabel="Colorado" />
															<f:selectItem itemValue="CT" itemLabel="Connecticut" />
															<f:selectItem itemValue="DE" itemLabel="Delaware" />
															<f:selectItem itemValue="DC"
																itemLabel="District of Columbia" />
															<f:selectItem itemValue="FL" itemLabel="Florida" />
															<f:selectItem itemValue="GA" itemLabel="Georgia" />
															<f:selectItem itemValue="HI" itemLabel="Hawaii" />
															<f:selectItem itemValue="ID" itemLabel="Idaho" />
															<f:selectItem itemValue="IL" itemLabel="Illinois" />
															<f:selectItem itemValue="IN" itemLabel="Indiana" />
															<f:selectItem itemValue="IA" itemLabel="Iowa" />
															<f:selectItem itemValue="KS" itemLabel="Kansas" />
															<f:selectItem itemValue="KY" itemLabel="Kentucky" />
															<f:selectItem itemValue="LA" itemLabel="Louisiana" />
															<f:selectItem itemValue="ME" itemLabel="Maine" />
															<f:selectItem itemValue="MD" itemLabel="Maryland" />
															<f:selectItem itemValue="MA" itemLabel="Massachusetts" />
															<f:selectItem itemValue="MI" itemLabel="Michigan" />
															<f:selectItem itemValue="MN" itemLabel="Minnesota" />
															<f:selectItem itemValue="MS" itemLabel="Mississippi" />
															<f:selectItem itemValue="MO" itemLabel="Missouri" />
															<f:selectItem itemValue="MT" itemLabel="Montana" />
															<f:selectItem itemValue="NE" itemLabel="Nebraska" />
															<f:selectItem itemValue="NV" itemLabel="Nevada" />
															<f:selectItem itemValue="NH" itemLabel="New Hampshire" />
															<f:selectItem itemValue="NJ" itemLabel="New Jersey" />
															<f:selectItem itemValue="NM" itemLabel="New Mexico" />
															<f:selectItem itemValue="NY" itemLabel="New York" />
															<f:selectItem itemValue="NC" itemLabel="North Carolina" />
															<f:selectItem itemValue="ND" itemLabel="North Dakota" />
															<f:selectItem itemValue="OH" itemLabel="Ohio" />
															<f:selectItem itemValue="OK" itemLabel="Oklahoma" />
															<f:selectItem itemValue="OR" itemLabel="Oregon" />
															<f:selectItem itemValue="PA" itemLabel="Pennsylvania" />
															<f:selectItem itemValue="PR" itemLabel="Puerto Rico" />
															<f:selectItem itemValue="RI" itemLabel="Rhode Island" />
															<f:selectItem itemValue="SC" itemLabel="South Carolina" />
															<f:selectItem itemValue="SD" itemLabel="South Dakota" />
															<f:selectItem itemValue="TN" itemLabel="Tennessee" />
															<f:selectItem itemValue="TX" itemLabel="Texas" />
															<f:selectItem itemValue="UT" itemLabel="Utah" />
															<f:selectItem itemValue="VT" itemLabel="Vermont" />
															<f:selectItem itemValue="VA" itemLabel="Virginia" />
															<f:selectItem itemValue="WA" itemLabel="Washington" />
															<f:selectItem itemValue="WV" itemLabel="West Virginia" />
															<f:selectItem itemValue="WI" itemLabel="Wisconsin" />
															<f:selectItem itemValue="WY" itemLabel="Wyoming" />
														</h:selectOneMenu>
													</hx:formItem>
													<hx:formItem styleClass="formItem" id="formZipLabel"
														label="#{labels.ZipLabel} #{labels.colonLabel} #{labels.requiredLabel}"
														errorText="#{labels.requiredFieldMsg}">
														<h:inputText styleClass="inputText" id="Zip"
															value="#{changeDeliveryAddressHandler.zip}"
															required="true" requiredMessage="Zip Code is Required"
															validatorMessage="#{labels.zipInvalid}"
															onchange="return func_1(this, event);" tabindex="9">
															<f:validateLength maximum="5" minimum="5"></f:validateLength>
														</h:inputText>
													</hx:formItem>
													<hx:formItem styleClass="formItem" id="formHomePhone"
														label="#{labels.homePhoneLabel} #{labels.colonLabel} #{labels.requiredLabel}"
														errorText="#{labels.invalidPhoneFormat}">
														<h:inputText styleClass="inputText" id="HomePhoneAreaCode"
															value="#{changeDeliveryAddressHandler.homePhoneAreaCode}"
															required="true" size="4"
															validatorMessage="#{labels.invalidPhoneFormat}"
															onchange="return func_1(this, event);" tabindex="10"
															requiredMessage="#{labels.phoneRequired}" maxlength="3">
															<f:validateLength minimum="3" maximum="3"></f:validateLength>
															<hx:validateConstraint regex="^[0-9]+$" />
															<hx:inputHelperAssist errorClass="inputText_Error"
																autoTab="true" id="assist5" />
														</h:inputText>
														<h:inputText styleClass="inputText" id="HomePhoneExchange"
															value="#{changeDeliveryAddressHandler.homePhoneExchange}"
															required="true" size="4"
															requiredMessage="Phone Number is Required"
															validatorMessage="#{labels.invalidPhoneFormat}"
															maxlength="3" tabindex="11">
															<f:validateLength minimum="3" maximum="3"></f:validateLength>
															<hx:validateConstraint regex="^[0-9]+$" />
															<hx:inputHelperAssist errorClass="inputText_Error"
																autoTab="true" id="assist8" />
														</h:inputText>
														<h:inputText styleClass="inputText"
															id="HomePhoneExtension" size="5" required="true"
															value="#{changeDeliveryAddressHandler.homePhoneExtension}"
															validatorMessage="#{labels.invalidPhoneFormat}"
															requiredMessage="Phone Number is Required" tabindex="12"
															maxlength="4">
															<f:validateLength minimum="4" maximum="4"></f:validateLength>
															<hx:validateConstraint regex="^[0-9]+$" />
															<hx:inputHelperAssist errorClass="inputText_Error"
																id="assist6" />
														</h:inputText>
													</hx:formItem>
													<hx:formItem styleClass="formItem" id="formBusPhone"
														label="#{labels.workPhoneLabel} #{labels.colonLabel} &nbsp;&nbsp;"
														errorText="#{labels.invalidPhoneFormat}">
														<h:inputText styleClass="inputText" id="BusPhoneAreaCode"
															value="#{changeDeliveryAddressHandler.busPhoneAreaCode}"
															size="4" validatorMessage="#{labels.invalidPhoneFormat}"
															maxlength="3" tabindex="13"
															onchange="return func_1(this, event);">
															<f:validateLength minimum="3" maximum="3"></f:validateLength>
															<hx:validateConstraint regex="^[0-9]+$" />
															<hx:inputHelperAssist errorClass="inputText_Error"
																autoTab="true" id="assist7" />
														</h:inputText>
														<h:inputText styleClass="inputText" id="BusPhoneExchange"
															size="4"
															value="#{changeDeliveryAddressHandler.busPhoneExchange}"
															validatorMessage="#{labels.invalidPhoneFormat}"
															onchange="return func_1(this, event);" tabindex="14"
															maxlength="3">
															<f:validateLength minimum="3" maximum="3"></f:validateLength>
															<hx:validateConstraint regex="^[0-9]+$" />
															<hx:inputHelperAssist errorClass="inputText_Error"
																autoTab="true" id="assist3" />
														</h:inputText>
														<h:inputText styleClass="inputText" id="BusPhoneExtension"
															size="5"
															value="#{changeDeliveryAddressHandler.busPhoneExtension}"
															validatorMessage="#{labels.invalidPhoneFormat}"
															onchange="return func_1(this, event);" tabindex="15"
															maxlength="4">
															<f:validateLength minimum="4" maximum="4"></f:validateLength>
															<hx:validateConstraint regex="^[0-9]+$" />
															<hx:inputHelperAssist errorClass="inputText_Error"
																autoTab="true" id="assist2" />
														</h:inputText>
													</hx:formItem>

													<f:facet name="top"></f:facet>
													<f:facet name="bottom"></f:facet>
												</hx:panelFormBox>

												<h:panelGrid styleClass="panelGrid" id="grid14" columns="1">
													<h:outputText styleClass="outputText" id="RequiredText1"
														value="* Denotes a required field."></h:outputText>
												</h:panelGrid>
											</h:panelGrid>
										</hx:jspPanel>
									</hx:panelSection>
									<hx:jspPanel id="jspPanelFormFields">

										<table border="0" width="600" cellspacing="0" cellpadding="0">
											<tr>
												<td><h:panelGroup styleClass="panelGroup"
														id="AdditionalAddressGroup1"
														rendered="#{changeDeliveryAddressHandler.additionalDeliveryAddressExist}">
														<h:panelGrid styleClass="panelGrid"
															id="AdditionalAddressGrid1" columns="2">
															<h:outputText styleClass="outputText"
																id="AdditionalAddressText1"
																value="We have the following additional address(es) on record:"></h:outputText>
															<br>
															<h:selectOneRadio disabledClass="selectOneRadio_Disabled"
																enabledClass="selectOneRadio_Enabled"
																styleClass="selectOneRadio" id="AdditionalAddressRadio1"
																value="#{changeDeliveryAddressHandler.additionalAddressChoices}"
																layout="pageDirection" tabindex="16" immediate="true">
																<hx:inputHelperAssist errorClass="selectOneRadio_Error"
																	id="assist1" />
																<f:selectItems
																	value="#{changeDeliveryAddressHandler.additionalDeliveryAddressCollection}" />
															</h:selectOneRadio>
															<hx:commandExButton type="submit"
																value="Delete Selected Address"
																styleClass="commandExButton" id="delete"
																confirm="Are you sure you want to delete this address"
																action="#{pc_Changeaddressjsf.doButtonDeleteDeliveryAddressAction}"
																tabindex="18"></hx:commandExButton>
														</h:panelGrid>
													</h:panelGroup>
												</td>
											</tr>
											<br>
											<tr>
												<td align="left"><br><h:panelGrid styleClass="panelGrid"
														id="grid1" style="margin-left: 60px">
														<hx:commandExButton type="submit" value="Continue"
															styleClass="commandExButton" id="Continue"
															action="#{pc_Changeaddressjsf.doButtonSubmitChangeDeliveryAddressAction}"
															tabindex="17"></hx:commandExButton>
													</h:panelGrid> <br> <br> 
												<br><br><br>
												</td>
											</tr>
										</table>
									</hx:jspPanel>
									<hx:behaviorKeyPress key="Enter" id="behaviorKeyPress1"
										behaviorAction="click" targetAction="Continue"></hx:behaviorKeyPress>
									<hx:inputHelperSetFocus id="setFocus1"></hx:inputHelperSetFocus>
								</h:form>
							</hx:scriptCollector>
						<%-- /tpl:put --%>
</DIV>
<%-- /tpl:put --%>
				<DIV id="clear"></DIV>
				<DIV id="bluebar">
					<a href="/feedback/feedback.jsp"
						style="color: white; font-weight: bold; text-decoration: none; font-size: 14px"
						title="Contact Us">Contact Us</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;1-800-872-0001&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a
						href="/faq/utfaq.jsp"
						style="color: white; font-weight: bold; text-decoration: none; font-size: 14px"
						title="Frequently Asked Questions">FAQ</a>
				</DIV>
				<%-- tpl:put name="footerMainArea" --%>
			<%-- tpl:put name="footerArea_1" --%><%-- /tpl:put --%>
			<%=trackingBug.getOmnitureHTML() %>
		<%-- /tpl:put --%>
				<DIV id=graybar>
					Copyright
					<script>
						document.write(getCurrentYear());
					</script>
					USA TODAY, a division of <a href="http://www.gannett.com">Gannett
						Co. Inc.</a> <a href="/privacy/privacy.htm">Privacy
						Notice/California Privacy Notice</a>. <a
						href="/privacy/privacy.htm#adchoice">Ad Choices</a>.<br> By
					using this service, you accept our <a href="/service/service.jsp">Terms
						of Service</a>.
				</DIV>
				<DIV id=copyright>
					<P>Offers available to new subscribers only. Home delivery not
						available in some areas. Not valid with any other offer.
						Prepayment required. After the promotional period ends, the
						subscription will continue and be billed monthly at the then
						regular rate, less any applicable credits, unless USA TODAY is
						otherwise notified. Applicable taxes may not be included in the
						rate. If at any time you decide to cancel your subscription, you
						may contact customer service at 1-800-872-0001 and the full amount
						of any balance over $2.00 will be returned.</P>
				</DIV>
				<!--END OF COPYWRITE-->
				<DIV id=footer></DIV>
			</DIV>
			<!--END OF WHITEBOX-->
		</DIV>
		<!--END OF CONTENT-->
	</DIV>
</BODY>
</HTML><%-- /tpl:insert --%>
</f:view><%-- /tpl:insert --%>
