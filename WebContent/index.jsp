<%@ page import = "com.usatoday.esub.common.*, com.usatoday.esub.account.*" %>
<%
	String loginsessionobject = "";
	// set default values
	session.setAttribute(UTCommon.SESSION_RENEWAL_TRACKING_CODE, UTCommon.DEFAULT_RENEWAL_TRACKING_CODE);	
	//
	// save the pub value
	//
	String pub = request.getParameter(UTCommon.PUB_QUERYSTRING);

	if (pub == null) {
		// default to UT
		pub = UTCommon.PUB_NAMES[0];
	
		try {
	String userEnteredURL = request.getRequestURL().toString();
	if (userEnteredURL != null) {
		if ( userEnteredURL.indexOf("mysportsweekly") >= 0 ) {
		// set publication to SportsWeekly
			pub = UTCommon.PUB_NAMES[1];
		}
	}
		}
		catch (Exception exp) {
	; // ignore
		} 
	}
	
	// Use rtc from URL or default from .ini file  sewrtc
	String renewal_tracking_code = request.getParameter(UTCommon.RENEWAL_TRACKING_CODE_QUERYSTRING);		
	try {
		// RTC
		if (renewal_tracking_code != null) {
	session.removeAttribute(UTCommon.SESSION_RENEWAL_TRACKING_CODE);
	session.setAttribute(UTCommon.SESSION_RENEWAL_TRACKING_CODE, renewal_tracking_code);	
		}
	}
	catch (Exception e) {
		// ignore
		;
	}

	// Check if login information must be removed
	loginsessionobject = request.getParameter(UTCommon.LOGIN_SESSION_OBJECT_QUERYSTRING);
	if (loginsessionobject == null) {
		loginsessionobject = "";
	} else {
		if (loginsessionobject.equals("rem")) {
			session.removeAttribute(UTCommon.SESSION_CUSTOMER_INFO);
			
			// clear the customer handler
			CustomerHandler ch = (CustomerHandler)session.getAttribute("customerHandler");
			if (ch != null) {
				ch.setCustomer(null);
				ch.setAuthenticated(false);
				ch.setCookieAuthenticated(false);
			}
			try {
				// Clear the cancels handler
				CancelsHandler cnh = (CancelsHandler)session.getAttribute("cancelsHandler");
				if (cnh != null) {
					cnh.setCustomer(null);
					cnh.setComment1("");
					cnh.setComment2("");
					cnh.setComment3("");
					cnh.setCancelReason("");
					cnh.setCancelCode("");
					cnh.setOnEZPay("");
				}
			} catch (Exception e) {
			
			}
			// Check and remove ChangeDeliveryAddressHandler
			try {				
				ChangeDeliveryAddressHandler cdah = (ChangeDeliveryAddressHandler)session.getAttribute("changeDeliveryAddressHandler");
				if (cdah != null) {
					session.removeAttribute("changeDeliveryAddressHandler");	
				}
			} catch (Exception e) {
			
			}
			// Check and remove ChangeBillingAddressHandler
			try {				
				ChangeBillingAddressHandler cbah = (ChangeBillingAddressHandler)session.getAttribute("changeBillingAddressHandler");
				if (cbah != null) {
					session.removeAttribute("changeBillingAddressHandler");	
				}
			} catch (Exception e) {
			
			}
			// remove the remember me, auto login, and session key cookies from the response
			AuthenticationCookieProcessor cp = new AuthenticationCookieProcessor();
				
			Cookie c = cp.returnRememberMeCookie(request.getCookies());
			if (c != null) {
				c = cp.setCookieForDeletion(c);
				response.addCookie(c);
			}
			c = cp.returnAutoLoginCookie(request.getCookies());
			if (c != null) {
				c = cp.setCookieForDeletion(c);
				response.addCookie(c);
			}
			c = cp.returnSessionKeyCookie(request.getCookies());
			if (c != null) {
				c = cp.setCookieForDeletion(c);
				response.addCookie(c);
			}
		}
	}		
	int pubIndex = 0;
	for (int i=0; i<UTCommon.PUB_NAMES.length; i++) {
		try {
	if (pub.equalsIgnoreCase(UTCommon.PUB_NAMES[i])) {
		// set updated values
		pubIndex = i;

		break;
	}
		} 
		catch (Exception e) {
	// no pub code!
	String userEnteredURL = request.getRequestURL().toString();
	if (userEnteredURL != null) {
		if ( userEnteredURL.indexOf("mysportsweekly") >= 0 ) {
			// set publication to SportsWeekly
			pub = UTCommon.PUB_NAMES[1];
		}
	}
	break;
		}
	}
	
	try {
		// The .com Point of entry
		String poeValue = request.getParameter("POE");
		if (poeValue != null) {
	session.removeAttribute("POE");
	session.setAttribute("POE", poeValue);
		}
	}
	catch (Exception e) {
		// ignore
		;
	}
	//
	// save the promo code
	//

	try {
		String promo = request.getParameter(UTCommon.KEYCODE_QUERYSTRING);
		
		// if a promotion (keycode) exists
		if (promo != null && promo.length() > 0) {
	
	String forwardURL = "/subscriptions/index.jsp";
	
	if (request.getQueryString() != null && request.getQueryString().length() > 0) {
		forwardURL += "?" + request.getQueryString();
	}
	 
	// redirect to the new subscription  page
	request.getRequestDispatcher(forwardURL).forward(request, response);
		} 
		else {
	if ((pub == null) || (pub.equals(UTCommon.PUB_NAMES[0]))) {
		UTCommon.showUrl(response, UTCommon.BASE_URL + "/welcome.jsp");
	} 
	else {
		UTCommon.showUrl(response, UTCommon.BASE_URL + "/welcome.jsp?pub=BW");
	}
		}
	} catch (Exception e) {
		// no promo code: assume not from a web link so use default
		// code and redirect to the main page
		if ((pub == null) || (pub.equals(UTCommon.PUB_NAMES[0]))) {
	UTCommon.showUrl(response, UTCommon.BASE_URL + "/welcome.jsp");
		} else {
	UTCommon.showUrl(response, UTCommon.BASE_URL + "/welcome.jsp?pub=BW");
		}
	}
%>

<%@page import="com.usatoday.esub.ncs.handlers.CustomerHandler"%>
<%@page import="com.usatoday.esub.ncs.handlers.CancelsHandler"%>
<%@page import="com.usatoday.esub.ncs.handlers.ChangeDeliveryAddressHandler"%>
<%@page import="com.usatoday.esub.ncs.handlers.ChangeBillingAddressHandler"%>
<%@page import="com.usatoday.esub.cookies.AuthenticationCookieProcessor"%><html>
<head>
<title>USA Today Online Subscriptions - Portal</title>
</head>
<body>
<p>This page should never be displayed.</p>
</body>
</html>