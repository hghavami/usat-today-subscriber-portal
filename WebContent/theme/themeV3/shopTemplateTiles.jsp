<!DOCTYPE HTML><%@page language="java"
	contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<html>
<head>
<META HTTP-EQUIV="X-UA-Compatible" CONTENT="IE=EmulateIE8">
<LINK REL="SHORTCUT ICON"
	HREF="${pageContext.request.contextPath}/favicon.ico">
<LINK REL="icon" HREF="${pageContext.request.contextPath}/favicon.ico"
	TYPE="image/ico" />
<SCRIPT SRC="/common/prototype/prototype.js"></SCRIPT>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META HTTP-EQUIV="Content-Style-Type" CONTENT="text/css">
<LINK HREF="/theme/themeV3/css/usatodayBase.css" REL="stylesheet"
	TYPE="text/css">
<LINK HREF="/theme/themeV3/css/usatodayAddOn.css" REL="stylesheet"
	TYPE="text/css">
<LINK HREF="/theme/themeV3/css/smallScreen.css" REL="stylesheet"
	MEDIA="screen and (max-device-width: 550px), handheld" TYPE="text/css">
<SCRIPT SRC="/common/usatCommon.js"></SCRIPT>
<title>
<tiles:getAsString name="documentTitle" />
</title>
<!-- <SCRIPT TYPE="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23158809-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</SCRIPT>
 --><STYLE TYPE="text/css">
#header2 {
	PADDING-LEFT: 10px;
	WIDTH: 940px;
	PADDING-RIGHT: 10px;
	BACKGROUND-REPEAT: no-repeat;
	BACKGROUND-POSITION: center top;
	HEIGHT: 80px;
	MARGIN-LEFT: auto;
	MARGIN-RIGHT: auto;
}

#header2 IMG {
	PADDING-BOTTOM: 0px;
	MARGIN: 0px;
	PADDING-LEFT: 0px;
	PADDING-RIGHT: 0px;
	PADDING-TOP: 0px;
}

#headerLeft2 {
	BACKGROUND-IMAGE: url("/shop/images/shop_header.jpg");
	position: relative;
	left: 0px;
	top: 0px;
	z-index: 10;
	WIDTH: 741px;
	HEIGHT: 80px;
	MARGIN-LEFT: auto;
	MARGIN-RIGHT: auto;
	cursor: pointer
}
</STYLE>

<tiles:insert attribute="headarea"></tiles:insert>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>
	<DIV ID="wrapper">
		<DIV ID="header2">
			<DIV ID="headerLeft2"
				ONCLICK="document.location = 'http://onlinestore.usatoday.com';"></DIV>
		</DIV>
		<DIV ID="content">
			<DIV ID="whitebox">
				<tiles:insert attribute="bodyarea"></tiles:insert>
				<DIV ID="clear">
					<P>&nbsp;</P>
					<P>&nbsp;</P>
				</DIV>
				<DIV ID="bluebar">
					<A HREF="/feedback/feedback.jsp"
						STYLE="color: white; font-weight: bold; text-decoration: none; font-size: 14px"
						TITLE="Contact Us">Contact Us</A>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;1-800-872-0001&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<A
						HREF="/faq/utfaq.jsp"
						STYLE="color: white; font-weight: bold; text-decoration: none; font-size: 14px"
						TITLE="Frequently Asked Questions">FAQ</A>
				</DIV>
				<DIV ID="pageContent2">
					<DIV ID="helpcenter">
						<H3>HELP CENTER</H3>
						<UL>
							<LI><A HREF="/faq/utfaq.jsp">FAQ</A>
							</LI>
							<LI><A HREF="/feedback/feedback.jsp">Contact Us</A>
							</LI>
							<LI><A HREF="http://ourpledge.usatoday.com/">Our Pledge</A>
							</LI>
							<LI><A HREF="/testimonials.jsp">Testimonials</A>
							</LI>
							<LI><A HREF="/sitemap.html">Site Map</A>
							</LI>
						</UL>
					</DIV>
					<!--END OF HELPCENTER-->
					<DIV ID="resource">
						<H3>USA TODAY RESOURCES</H3>
						<UL>
							<LI><A HREF="http://www.usatoday.com">USATODAY.com</A>
							</LI>
							<LI><A HREF="http://www.usaweekend.com/">USA Weekend</A>
							</LI>
							<LI><A HREF="http://www.usatoday.com/educate/homesplash.htm">Education</A>
							</LI>
							<LI><A HREF="/subscriptions/order/checkout.faces?pub=BW">Sports
									Weekly</A>
							</LI>
						</UL>
					</DIV>
					<!--END OF RESOURCE-->
					<DIV ID="connect">
						<H3>CONNECT WITH US</H3>
						<UL>
							<LI CLASS="twitter"><A HREF="http://twitter.usatoday.com">Twitter</A>
							</LI>
							<LI CLASS="facebook"><A
								HREF="http://www.facebook.com/usatoday">Facebook</A>
							</LI>
							<LI CLASS="youtube"><A
								HREF="http://www.youtube.com/user/USATODAY">You Tube</A>
							</LI>
						</UL>
					</DIV>
					<DIV ID="footerAdSpot"></DIV>
				</DIV>
				<!--END OF PAGECONTENT2-->
				<DIV ID="graybar">
					Copyright
					<SCRIPT>document.write(getCurrentYear());</SCRIPT>
					USA TODAY, a division of <A HREF="http://www.gannett.com">Gannett
						Co. Inc.</A> <A HREF="/privacy/privacy.htm">Privacy Policy/Your
						California Privacy Policy</A>. By using this service, you accept our <A
						HREF="/service/service.jsp">Terms of Service</A>.
				</DIV>
				<DIV ID="copyright">
					<P>USA TODAY offers various promotional programs or premium
						incentives in connection with subscription sales. Discounts or
						premium incentives compare the specific offer to the newsstand
						price and not to any other promotional programs or premium
						incentives that may exist. Print edition carrier delivery not
						available in certain areas. Print edition offer available in
						continental USA and Hawaii only. Refunds on all undelivered print
						copies. Introductory offers available to new subscribers only.</P>
				</DIV>
				<!--END OF COPYWRITE-->
				<DIV ID="footer"></DIV>
			</DIV>
		</DIV>
	</DIV>	
</body>
</html>