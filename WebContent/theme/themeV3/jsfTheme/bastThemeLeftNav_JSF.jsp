<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%><%-- jsf:pagecode language="java" location="/JavaSource/pagecode/theme/themeV3/jsfTheme/BastThemeLeftNav_JSF.java" --%><%-- /jsf:pagecode --%><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%><f:view>

	<%@taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%><%-- tpl:insert page="/theme/themeV3/baseTheme/baseThemeHeaderFooter.htpl" --%><!DOCTYPE HTML>
<HTML>
<HEAD>  
<%-- tpl:put name="headerarea1" --%>
			<!-- Header Area For Title and Meta tags -->
			<title>
<tiles:getAsString name="documentTitle" />
</title>
		<%-- /tpl:put --%>
<LINK REL="SHORTCUT ICON" HREF="${pageContext.request.contextPath}/favicon.ico">
<LINK REL="icon" href="${pageContext.request.contextPath}/favicon.ico" TYPE="image/ico" />
<script src="/common/prototype/prototype.js"></script>
<script src="/common/prototype/scriptaculous/scriptaculous.js" type="text/javascript"></script>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="/theme/themeV3/css/usatodayBase.css" rel="stylesheet" type="text/css">
<LINK href="/theme/themeV3/css/usatodayAddOn.css" rel="stylesheet" type="text/css">
<LINK href="/theme/themeV3/css/smallScreen.css" rel="stylesheet"  media="screen and (max-device-width: 550px), handheld" type="text/css">
<script src="/common/usatCommon.js"></script>
<%-- tpl:put name="headarea" --%>
			<!-- Bottom Header area -->
		<%-- /tpl:put --%>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23158809-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</HEAD>
<body>
<DIV id=wrapper>
<DIV id=header>
<div id="headerLeft" onclick="templateLogoClick();"></div>
	<div id="headerRight">
			<%-- tpl:put name="headerbodyarea" --%>
			<f:subview id="subviewBodyHeader1">
				<tiles:insert attribute="bodyHeaderArea" flush="false"></tiles:insert>
			</f:subview>
		<%-- /tpl:put --%>
	</div></DIV>
<DIV id=content>
<DIV id=whitebox>
<%-- tpl:put name="baseBodyarea" --%>
<DIV id=nav>
	<DIV id=leftNavList>
		
		<UL>
		  <LI><A href="/products.html">USA TODAY Products</A></LI>
		  <LI><A href="javascript:_subscribeToPub()">Subscribe</A></LI>
		  <LI><A href="/account/accounthistory/accountSummary.faces">Manage account</A></LI>
		  <LI><A href="javascript:_subscribeToInternationalEdition()">Outside the USA</A></LI>
		  <LI><A href="javascript:_subscribeByMail()">Mail-in order form</A></LI>
		  <LI><A href="javascript:_shopUSAT()">Order back issues</A></LI>
		</UL>
	</DIV>
	<DIV id="navPromo">
	<f:subview id="subviewNavPromo1">
		<tiles:insert attribute="bodyareaNavPromoArea" flush="false"></tiles:insert>
	</f:subview>
	</DIV>
</DIV>
<DIV id=pageContent>
<f:subview id="subviewMainBody1">
	<tiles:insert attribute="bodyareaMain" flush="false"></tiles:insert>
</f:subview>
</DIV>
<%-- /tpl:put --%>
<DIV id=clear>
<P>&nbsp;</P>
<P>&nbsp;</P></DIV>
<DIV id="bluebar"><a href="/feedback/feedback.jsp" style="color: white; font-weight: bold; text-decoration: none; font-size: 14px" title="Contact Us">Contact Us</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;1-800-872-0001&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="/faq/utfaq.jsp" style="color: white; font-weight: bold; text-decoration: none; font-size: 14px" title="Frequently Asked Questions">FAQ</a></DIV>
<DIV id=pageContent2>
<DIV id=helpcenter>
<H3>HELP CENTER</H3>
<UL>
  <LI><A href="/faq/utfaq.jsp">FAQ</A></LI>
  <LI><A href="/feedback/feedback.jsp">Contact Us</A></LI>
  <LI><A href="http://ourpledge.usatoday.com/">Our Pledge</A></LI>
  <LI><A href="/testimonials.jsp">Testimonials</A></LI>
  <LI><A href="/sitemap.html">Site Map</A></LI>
</UL></DIV><!--END OF HELPCENTER-->
<DIV id=resource>
<H3>USA TODAY RESOURCES</H3>
<UL>
  <LI><A href="http://www.usatoday.com">USATODAY.com</A></LI>
  <LI><A href="http://www.usaweekend.com/">USA Weekend</A></LI>
  <LI><A href="http://www.usatoday.com/educate/homesplash.htm">Education</A></LI>
  <LI><A href="/subscriptions/order/checkout.faces?pub=BW">Sports Weekly</A></LI>
</UL>
</DIV><!--END OF RESOURCE-->
<DIV id=connect>
<H3>CONNECT WITH US</H3>
<UL>
  <LI class=twitter><A href="http://twitter.usatoday.com">Twitter</A></LI>
  <LI class=facebook><A href="http://www.facebook.com/usatoday">Facebook</A></LI>
  <LI class=youtube><A href="http://www.youtube.com/user/USATODAY">You Tube</A></LI>
</UL>
</DIV>
 <DIV id="footerAdSpot">
 	<%-- tpl:put name="footerArea1" --%>
			<f:subview id="subviewFooter1">
				<tiles:insert attribute="footerArea1" flush="false"></tiles:insert>
			</f:subview>
		<%-- /tpl:put --%>
 </DIV>
</DIV><!--END OF PAGECONTENT2-->
<DIV id=graybar>  Copyright <script>document.write(getCurrentYear());</script> USA TODAY, a division of <a href="http://www.gannett.com">Gannett Co. Inc.</a> 
<a href="/privacy/privacy.htm">Privacy Policy/Your California Privacy Policy</a>. By using this service, you accept 
our <a href="/service/service.jsp">Terms of Service</a>.</DIV>
<DIV id=copyright>
<P>USA TODAY offers various promotional programs or premium incentives in 
connection with subscription sales. Discounts or premium incentives compare the 
specific offer to the newsstand price and not to any other promotional programs 
or premium incentives that may exist. Print edition carrier delivery not 
available in certain areas. Print edition offer available in continental USA and 
Hawaii only. Refunds on all undelivered print copies. Introductory offers 
available to new subscribers only.</P>
</DIV>
<!--END OF COPYWRITE-->
<DIV id=footer></DIV>
</DIV>
<!--END OF WHITEBOX-->
</DIV>
<!--END OF CONTENT-->
</DIV>
</BODY>
</HTML><%-- /tpl:insert --%>
</f:view>