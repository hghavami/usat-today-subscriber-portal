({
    baseUrl: ".",
    name: "main",
    out: "main.min.js",
    paths: {
        jquery: 'libs/jquery/jquery-1.7.2.min',
        jqueryui: 'libs/jquery/plugins/jquery-ui-1.8.19.custom.min', // to be served via dfp
        underscore: 'libs/underscore/underscore-min',
        backbone: 'libs/backbone/backbone-dev',
        pubsub: 'libs/backbone/plugins/pubsub',
        app: 'models/app',
        baseview: 'views/base',
        cookie: 'libs/jquery/plugins/jquery.cookie',
        easing: 'libs/jquery/plugins/jquery.easing.1.3',
        animatecolors: 'libs/jquery/plugins/jquery.animate-colors-min',
        responsiveimages: 'libs/jquery/plugins/jquery.responsiveImages',
        resizestop: 'libs/jquery/plugins/jquery.resizeStop',
        chosen: 'libs/jquery/plugins/chosen.jquery.min',
        mousewheel: 'libs/jquery/plugins/jquery.mousewheel.min',
        circleplayer: 'libs/jquery/plugins/audio-player/circle.player',
        grab: 'libs/jquery/plugins/audio-player/jquery.grab',
        jplayer: 'libs/jquery/plugins/audio-player/jquery.jplayer',
        transform: 'libs/jquery/plugins/audio-player/jquery.transform',
        twitter: 'libs/twitter/twitter',
        fwinfo: 'fw/models/fwinfo',
        settealium: 'fw/models/settealium',
        facebook: 'fw/facebook/facebook',
        prerollview: 'fw/views/prerollview',
        fitvids: 'libs/jquery/plugins/jquery.fitvids',
        selectorextend: 'libs/jquery/plugins/jquery.selector.extend',
        leaflet: 'libs/leaflet/leaflet',
        tilelayer: 'libs/leaflet/tile-layer',
        waxleaf: 'libs/leaflet/wax.leaf.min'
    },

    /**
     * The use of the order plugin has been discontinued. Define necessary dependencies
     * using shim.
     *
     * https://github.com/jrburke/requirejs/wiki/Upgrading-to-RequireJS-2.0
     */
    shim: {
        'jqueryui': ['jquery'],
        'cookie': ['jquery'],
        'easing': ['jquery'],
        'animatecolors': ['jquery'],
        'responsiveimages': ['jquery'],
        'resizestop': ['jquery'],
        'chosen': ['jquery'],
        'mousewheel': ['jquery'],
        'circleplayer': ['jquery'],
        'grab': ['jquery'],
        'jplayer': ['jquery'],
        'transform': ['jquery'],
        'fitvids': ['jquery'],
        'selectorextend': ['jquery'],
        'tilelayer': ['leaflet'],
        'waxleaf': ['leaflet']
    },

    //If using UglifyJS for script optimization, these config options can be
    //used to pass configuration values to UglifyJS.
    //See https://github.com/mishoo/UglifyJS for the possible values.
    uglify: {
        beautify: false,
        toplevel: true
        //max_line_length: 1000
    }
})