/**
 * @fileoverview Base View Controller providing basic function like render, show, hide
 * @author Jay Merrifield
 */
define(['jquery', 'underscore', 'backbone', 'app', 'pubsub', 'state', 'easing'],
    function($, _, Backbone, App, PubSub, StateManager) {
        var BaseView = Backbone.View.extend({
            // css constants
            transEndEventNames: {
                'WebkitTransition' : 'webkitTransitionEnd',
                'MozTransition'    : 'transitionend',
                'OTransition'      : 'oTransitionEnd',
                'msTransition'     : 'MSTransitionEnd',
                'transition'       : 'transitionend'
            },
            transitionCssName: Modernizr.prefixed('transition'),
            transitionEndName: '',
            perspectiveCssName: Modernizr.prefixed('perspective'),
            useCSSTransitions: false,
            useCSSTransforms3d: false,
            transformCssName: Modernizr.prefixed('transform'),
            backfaceVisibilityCssName: Modernizr.prefixed('backface-visibility'),
            transformCssHyphenName: '',
            pubSub: null,
            currentPath: '',
            options: {animations: {
                fadeIn: {
                    duration: 200,
                    easing: 'easeInQuad'
                },
                fadeOut: {
                    duration: 200,
                    easing: 'easeOutSine'
                },
                slide: {
                    duration: 350,
                    easing: 'ease-in-out'
                },
                useCSSTransitions: true
            }},

            initialize: function(options){
                this.options = $.extend(true, {}, this.options, options);
                this.useCSSTransitions = (this.options.animations.useCSSTransitions &&
                    Modernizr && Modernizr.csstransitions) ? true : false;
                if(this.useCSSTransitions){
                    this.transitionEndName = this.transEndEventNames[this.transitionCssName];
                    this.useCSSTransforms3d = Modernizr.csstransforms3d;
                    this.transformCssHyphenName = this.transformCssName.replace(/([A-Z])/g,
                        function(str,m1){
                            return '-' + m1.toLowerCase();
                        }).replace(/^ms-/,'-ms-');
                }
                this.subviews = {};

                if (this.pubSub){
                    PubSub.attach(this.pubSub, this);
                }
            },

            /**
             * renders the html fragment to the configured 'el'
             * override for more advanced functionality
             * @param htmlFrag {String} the dom to be loaded
             */
            render: function(htmlFrag){
                if (htmlFrag){
                    htmlFrag = $(htmlFrag);
                    if (this.$el.length === 0){
                        this.setElement(htmlFrag);
                        this.$el.insertAfter('#header');
                    }else if (this.$el[0] !== htmlFrag[0]){
                        this.setElement(htmlFrag);
                    }
                }
            },

            animateNewContent: function(htmlFrag){
                this.setElement(htmlFrag);
            },

            transition: function(fromPath, toPath, requestPromise, preload){
                // destroy existing modules so new ones can be instantiated
                // this function is up for debate, when does this get called? who's responsible for calling it?
                this.destroyModules();

                this._setupBeforeReveal(requestPromise, fromPath, toPath, preload);

                this.currentPath = toPath;
                var transition = null;
                if (fromPath !== toPath){
                    transition = this.getTransitionAnimation(fromPath, toPath, requestPromise);
                }
                if (!transition){
                    if (!requestPromise){
                        // no request, no transition, wtf am I supposed to do?
                        this.afterReveal(fromPath, toPath, preload);
                        return $.Deferred().resolve();
                    }else{
                        this._triggerLoader(requestPromise);
                        return this._finishTransition(requestPromise, fromPath, toPath, preload);
                    }
                }else{
                    // getTransitionAnimation returns either a string or a promise object that
                    // resolves when an animation is finished
                    if (_.isString(transition)){
                        transition = this._handleNamedTransition(toPath, transition, requestPromise);
                    }
                    if (requestPromise){
                        transition.done(_.bind(function(){
                            this._triggerLoader(requestPromise);
                        }, this));
                        return this._finishTransition($.when(requestPromise, transition), fromPath, toPath, preload);
                    }else{
                        return this._finishTransition(transition, fromPath, toPath, preload);
                    }
                }
            },
            _triggerLoader: function(requestPromise){
                if (requestPromise.state() === 'pending'){
                    this.activateLoader();
                    requestPromise.always(_.bind(function(){
                        this.deactivateLoader();
                    }, this));
                }
            },
            _finishTransition: function(transitionPromise, fromPath, toPath, preload){
                transitionPromise.done(_.bind(function(htmlFrag){
                    this.animateNewContent(htmlFrag);
                    this.afterReveal(fromPath, toPath, preload);
                }, this));
                return transitionPromise;
            },
            _handleNamedTransition: function(toPath, name, requestPromise){
                if (name === 'left' || name === 'right'){
                    // we need the new content to animate in
                    var deferred = $.Deferred();
                    requestPromise.done(_.bind(function(htmlFrag){
                        this.slide(htmlFrag, name, this.options.animations.slide.duration,
                            this.options.animations.slide.easing).done(function(){
                                deferred.resolve();
                            });
                    }, this));
                    return deferred.promise();
                }else{
                    var deferred = jQuery.Deferred();
                    $.when(requestPromise, this.hide(true))
                        .done(_.bind(function(htmlFrag){
                            this.render(toPath, htmlFrag);
                            this.show(true).done(function(){
                                deferred.resolve();
                            });
                    }));
                    return deferred.promise();
                }
            },
            activateLoader: function(){
                StateManager.loader.show();
            },
            deactivateLoader: function(){
                StateManager.loader.hide();
            },
            startNavigationAnimation: function(deferred){
                StateManager.startNavigationAnimation(deferred);
            },
            /**
             * Clean up view.
             * Removes event handlers and element (optionally).
             * @param {Boolean} removeEl Option to also remove View from DOM.
             */
            destroy: function(removeEl) {
                this.destroyModules();
                this.undelegateEvents();
                if (this.pubSub) PubSub.unattach(this.pubSub, this);
                if (removeEl) this.$el.remove();
            },
            destroyModules: function(){
                // clean up any old constructs cause we're transitioning to something else
                _.each(this.subviews, function(module) {
                    // if we get an array of instances, iterate and destroy
                    if ($.isArray(module)) {
                        _.each(module, function(m) {
                            if (typeof m.destroy !== 'undefined') {
                                m.destroy();
                            }
                        });
                    }
                    else if (typeof module.destroy !== 'undefined') {
                        module.destroy();
                    }
                });
                this.subviews = {};
            },
            pause: function(){
                this.destroyModules();
            },
            /**
             * This is a function that will decide how to animate the reveal of this view
             * @param {String} fromPath path of the view going away
             * @param {String} toPath path of the view we're going to
             * @param {Boolean} preload variable specifying if the view should load up in a paused state
             * @return {jQuery Promise Object} object that will resolve when reveal is complete
             */
            revealView: function(fromPath, toPath, requestPromise, preload){
                // so we are going to find ourselves in a couple of scenarios here,
                // 1: requestPromise is null, cause it's an initial render, very little to do
                // 2: requestPromise has the html we want to render, score!
                // 3: requestPromise isn't ready, but we have a temporary loader, not bad!
                // 4: requestPromise isn't ready, and the view doesn't know what to do, so we wait

                this.currentPath = toPath;
                if (!requestPromise){
                    // scenario 1, initial load, yippy!
                    this.beforeReveal(fromPath, toPath, this.$el, preload);
                    this.afterReveal(fromPath, toPath, preload);
                    return $.Deferred().resolve();
                }else if (requestPromise.state() === 'rejected'){
                    return requestPromise;
                }else{
                    var animationDeferred = null;
                    // make certain beforeReveal gets called when the request finishes
                    this._setupBeforeReveal(requestPromise, fromPath, toPath, preload);
                    if (requestPromise.state() === 'resolved'){
                        // scenario 2, we have html to render immediately, go go captain planet
                        requestPromise.done(function(htmlFrag){
                            this.render(htmlFrag);
                            this.hide();
                            animationDeferred = this.animateRevealView(fromPath, toPath);
                            animationDeferred.done(_.bind(function(){
                                this.afterReveal(fromPath, toPath, preload);

                            }, this));
                        });
                    }else{
                        animationDeferred = $.Deferred();
                        var newContent = this.getTemporaryLoader(toPath);
                        if (newContent){
                            // scenario 3, render temp loader
                            this.render(newContent);
                            this.hide();
                            this.animateRevealView(fromPath, toPath).done(_.bind(function(){
                                requestPromise.done(_.bind(function(htmlFrag){
                                    this.animateNewContent(htmlFrag);
                                    this.afterReveal(fromPath, toPath, preload);
                                }, this));
                                animationDeferred.resolve();
                            }, this));
                        }else{
                            // scenario 4, we have to wait on a generic loader, sad panda!
                            if (requestPromise.state() === 'pending'){
                                // activate generic loader
                                this.activateLoader();
                                requestPromise.always(_.bind(function(){
                                    this.deactivateLoader();
                                }, this));
                            }
                            requestPromise.done(_.bind(function(htmlFrag){
                                this.render(htmlFrag);
                                this.hide();
                                this.animateRevealView(fromPath, toPath).done(_.bind(function(){
                                    this.afterReveal(fromPath, toPath, preload);
                                    animationDeferred.resolve();
                                }, this));
                            }, this)).fail(function(){
                                animationDeferred.reject();
                            });
                        }
                    }
                    return animationDeferred.promise();
                }
            },
            _setupBeforeReveal: function(requestPromise, fromPath, toPath, preload){
                if (requestPromise){
                    requestPromise.done(_.bind(function(htmlFrag){
                        this.beforeReveal(fromPath, toPath, htmlFrag, preload);
                    }, this));
                }else{
                    this.beforeReveal(fromPath, toPath, this.$el, preload);
                }
            },
            /**
             * Overridable function that can change how the current view animates in
             * @param {String} fromPath path we're animating from
             * @param {String} toPath path we're animating to
             * @return {jQuery Promise Object} object that will resolve when reveal animation is complete
             */
            animateRevealView: function(fromPath, toPath){
                return this.show(true);
            },
            /**
             * This is a function that will decide how to animate the removal of this view
             * @param {String} toPath path of the view we're going to
             * @return {jQuery Promise Object} object that will resolve when removal is complete
             */
            removeView: function(toPath){
                try{
                    this.beforeViewRemove(this.currentPath, toPath);
                    var promise = this.animateRemoveView(toPath);
                    promise.always(_.bind(function(){
                        try{
                            this.afterViewRemove(this.currentPath, toPath);
                        }catch(ex){
                            console.error('View threw an exception on afterViewRemove event: ', (ex.stack || ex.stacktrace || ex.message));
                        }
                        this._safeDestroy();
                    }, this));
                    return promise;
                }catch(ex){
                    console.error('View threw an exception trying to removeView: ', (ex.stack || ex.stacktrace || ex.message));
                    this._safeDestroy();
                    return $.Deferred().resolve();
                }
            },
            _safeDestroy: function(){
                try{
                    this.destroy(true);
                }catch(ex){
                    console.error('View threw an exception on destroy: ', (ex.stack || ex.stacktrace || ex.message));
                    this.remove();
                }
            },
            /**
             * Overridable function that can change how the current view animates out
             * @param {String} toPath path we're animating to
             * @return {jQuery Promise Object} object that will resolve when removal animation is complete
             */
            animateRemoveView: function(toPath){
                return this.hide(true);
            },

            /**
             * Overridable function that decides whether a path should be fetched from the server
             * @param {String} toPath path we're possibly going to fetch
             * @return {Boolean}
             */
            shouldFetchData: function(toPath){
                return this.currentPath !== toPath;
            },

            prefetchData: function(toPath){

            },

            /**************************************************************
             * Event Callbacks (no ops by default)
             **************************************************************/

            /**
             * gets an internal transition animation
             * @param fromPath path we're leaving
             * @param toPath path we're about to go to
             * @param requestPromise {jQuery Promise Object} this promise represents the in progress
             *              request that may or may not be done. It's done handler will give you the
             *              successful htmlFrag
             * @return {String or jQuery Promise Object} representing either a predefined
             *              animation named with a string, or a promise object that resolves
             *              when the animation is complete
             */
            getTransitionAnimation: function(fromPath, toPath, requestPromise){
                // no op, defaults to no animation
            },
            /**
             * Gets called before the transition and removal of the current view.
             * This only gets called when the current transition will trigger a removal of this view
             * @param fromPath path we're leaving
             * @param toPath path we're about to go to
             */
            beforeViewRemove: function(fromPath, toPath){
                //no op
            },
            /**
             * Gets called after the transition and before the removal/destruction of the current view.
             * @param fromPath path we're leaving
             * @param toPath path we're about to go to
             */
            afterViewRemove: function(fromPath, toPath){
                //no op
            },
            /**
             * Called before new content is animated in or revealed.
             * This can be called when this view is created, or when transitioning within the view
             * @param fromPath path we're leaving
             * @param toPath path we're about to go to
             * @param {Boolean} paused variable specifying if the view should load up in a paused state
             */
            beforeReveal: function(fromPath, toPath, htmlFrag, paused){
                //no op
            },
            /**
             * Called after new content has been successfully animated in or revealed.
             * This can be called when this view is created, or when transitioning within the view
             * @param fromPath path we're leaving
             * @param toPath path we're about to go to
             * @param {Boolean} paused variable specifying if the view should load up in a paused state
             */
            afterReveal: function(fromPath, toPath, paused){
                //no op
            },

            getTemporaryLoader: function(toPath){

            },

            /**************************************************************
             * Utility Transition & Animation Functions
             **************************************************************/

            slide: function(newContent, direction, timeMs, easing){
                var attribute = null;
                var endPosition = null;
                if (direction === 'left'){
                    attribute = 'left';
                    endPosition = -100;
                }else if (direction === 'right'){
                    attribute = 'left';
                    endPosition = 100;
                } //TODO 'up' and 'down'

                this.$el.css(attribute, '0%');
                newContent = $(newContent);
                newContent.css(attribute, (endPosition * -1) + '%');
                newContent.insertAfter(this.$el);
                var deferred = jQuery.Deferred();
                _.defer(_.bind(function(){
                    // if we have no element, assume someone else is handling the slide out
                    if (this.$el.length !== 0){
                        this.animate(this.$el, attribute, endPosition + '%', timeMs, easing);
                    }
                    var promise = this.animate(newContent, attribute, '0%', timeMs, easing);
                    promise.done(_.bind(function(){
                        this.$el.remove();
                        this.setElement(newContent);
                        deferred.resolve();
                    }, this));
                }, this));
                return deferred.promise();
            },

            /**
             * Utility function that will animate an element using css or jquery animation
             * based on the capabilities of the browser
             * @param el {jQuery Object} element to animate
             * @param property {String} property name to animate
             * @param value {String} property value to animate to
             * @param timeMs {int} time in milliseconds the animation should take
             * @param easing {String} the easing algorithm to use, defaults to 'linear' if absent
             * @param delay {int} time in milliseconds the animation should delay for
             * @return {jQuery Promise Object} that will resolve when the animation finishes
             */
            animate: function(el, property, value, timeMs, easing, delay){
                var easing = easing || 'linear';
                if(this.useCSSTransitions) {
                    if (delay){
                        delay = ' ' + delay + 'ms';
                    }else{
                        delay = '';
                    }
                    if (this.useCSSTransforms3d && property === 'left'){
                        var existingPropertyValue = el[0].style[property];
                        if (existingPropertyValue){
                            el[0].style[this.transformCssName] = 'translate3d(' + existingPropertyValue + ', 0, 0)';
                            el.css(property, 'auto');
                        }
                        return this._transform3d(el, property, value, timeMs, easing, delay);
                    }else{
                        return this._transition(el, property, value, timeMs, easing, delay);
                    }
                } else {
                    if (easing.indexOf('cubic-bezier') != -1){
                        easing = 'swing';
                    }else if (easing === 'ease-in'){
                        easing = 'easeInQuad';
                    }else if (easing === 'ease-out'){
                        easing = 'easeOutQuad';
                    }else if (easing === 'ease-in-out'){
                        easing = 'easeInOutQuad';
                    }
                    var properties = {};
                    properties[property] = value;
                    return el.animate(properties, timeMs, easing);
                }
            },
            _transition: function(el, property, value, timeMs, easing, delay){
                var deferred = $.Deferred();
                el[0].style[this.transitionCssName] = property + ' ' + timeMs + 'ms ' + easing + delay;
                if (timeMs === 0){ // special case 0, we don't want the delay the change
                    el.css(property, value);
                    deferred.resolve();
                }else{
                    // adding a quick delay guarantees that the timing of the function is more accurate
                    // it also improves animation in firefox
                    _.delay(function(){
                        el.css(property, value);
                        _.delay(function(){
                            deferred.resolve();
                        }, timeMs + 50);
                    });
                }
                return deferred.promise();
            },
            _transform3d: function(el, property, value, timeMs, easing, delay){
                var deferred = $.Deferred();
                var transitionValue = this.transformCssHyphenName + ' ' + timeMs + 'ms ' + easing + delay;
                if (timeMs === 0){ // special case 0, we don't want the delay the change
                    el.css(property, value);
                    el[0].style[this.transitionCssName] = transitionValue;
                    el[0].style[this.transformCssName] = 'translate(' + value + ', 0)';
                    deferred.resolve();
                }else{
                    // adding a quick delay guarantees that the timing of the function is more accurate
                    // it also improves animation in firefox
                    _.delay(_.bind(function(){
                        el[0].style[this.transitionCssName] = transitionValue;
                        var translateValue = 'translateX(' + value + ')';
                        if (translateValue === el[0].style[this.transformCssName]){
                            deferred.resolve();
                        }else{
                            el[0].style[this.perspectiveCssName] = '1000';
                            el[0].style[this.backfaceVisibilityCssName] = 'hidden';
                            el[0].style[this.transformCssName] = translateValue;
                            var transitionEndFunc = _.bind(function(){
                                el[0].removeEventListener(this.transitionEndName, transitionEndFunc);
                                deferred.resolve();
                            }, this);
                            el[0].addEventListener(this.transitionEndName, transitionEndFunc);
                        }
                    }, this));
                }
                return deferred.promise();
            },

            /**
             * Show the 'el'. Optionally by fading them in.
             * @param {Boolean} transition Fade out the element.
             * @param {Function} legacyCallback deprecated callback function
             * @return {jQuery Promise Object} that will resolve when the animation finishes
             */
            show: function(transition, legacyCallback) {
                var promise = null;
                if(transition) {
                    promise = this.$el.fadeTo(
                        this.options.animations.fadeIn.duration,
                        1,
                        this.options.animations.fadeIn.easing
                    ).promise();
                } else {
                    //we set the css directly instead of calling jquery to avoid
                    // a css recalculation triggered by jquery
                    promise = $.Deferred().resolve();
                    this.$el.css({display: 'block', opacity: 1});
                }
                promise.done(function(){
                    if (legacyCallback){
                        legacyCallback();
                    }
                });
                return promise;
            },

            /**
             * Hide the cards. Optionally by fading them out.
             * @param {Boolean} transition Fade out the element.
             * @param {Function} legacyCallback deprecated callback function
             * @return {jQuery Promise Object} that will resolve when the animation finishes
             */
            hide: function(transition, legacyCallback) {
                var promise = null;
                if(transition) {
                    promise = this.$el.fadeTo(
                        this.options.animations.fadeOut.duration,
                        0,
                        this.options.animations.fadeOut.easing
                    ).promise();
                } else {
                    //we set the css directly instead of calling jquery to avoid
                    // a css recalculation triggered by jquery
                    promise = $.Deferred().resolve();
                    this.$el.css({display: 'none', opacity: 0});
                }
                promise.done(function(){
                    if (legacyCallback){
                        legacyCallback();
                    }
                });
                return promise;
            }
        });
        return BaseView;
    }
);
