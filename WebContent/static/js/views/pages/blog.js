/**
 * @fileoverview Blog view.
 * @author erik.kallevig@f-i.com (Erik Kallevig)
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'app',
    'pubsub',
    'views/pages/stag-front',
    'views/modules/twitter',
    'views/modules/blogs'
    ],
    function(
        $,
        _,
        Backbone,
        App,
        PubSub,
        StagFront,
        TwitterView,
        Blogs
    ) {


        /**
         * View class.
         */
        var BlogView = StagFront.extend({

            // View element.
            el: '#blog-card',

            /**
             * Instantiate dependencies and render the view.
             */
            _initializeTopic: function(fromPath){
                // Initialize the Twitter Module.
                this.subviews.twitter = new TwitterView({
                    el : '.stag .mod.tweets',
                    num_tweets : 9,
                    columns: 3
                });

                // Initialize the Blogs module
                this.subviews.blogs = new Blogs({
                    el : '.stag .mod.blogs'
                });

                // call base class initialize
                StagFront.prototype._initializeTopic.call(this, fromPath);
            }

        });


        /**
         * Return view class.
         */
        return BlogView;
    }
);
