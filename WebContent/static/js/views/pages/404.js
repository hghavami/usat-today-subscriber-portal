/**
 * @fileoverview 404 / 500 view.
 * @author Robert Huhn
 */
define(['jquery', 'underscore', 'backbone', 'pubsub'],
    function($, _, Backbone, PubSub) {

        /**
         * View class.
         */
        ErrorView = Backbone.View.extend({

            // View element.
            el: 'body',

            // Events.
            events: {
                'click .close': 'close'
            },

            initialize: function(options) {

                this.render();

                this.namespace = this.cid;

                //set footer to open without stealing focus
                $("#footer").removeClass('size-9').find('nav').show();
            },

            render: function(){
                return this;
            },

            /**
             * Close story.
             * @param {Event} e Click event to close overlay.
             */
            close: function(e) {
                PubSub.trigger('close:overlay');
                this.destroy();
                return false;
            }
    });
    return ErrorView;

});