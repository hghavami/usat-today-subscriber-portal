/**
 * @fileoverview Search view.
 * @author stephen.burgess@f-i.com (Stephen Burgess)
 */
define([
    'jquery', 
    'underscore', 
    'backbone', 
    'pubsub', 
    'app',
    'baseview',
    'state'
],
function(
    $, 
    _, 
    Backbone, 
    PubSub, 
    App,
    BaseView,
    StateManager
)
    {

        /**
         * View class.
         */
        var SportsOverlayView = BaseView.extend({

            // View element.
            el: '#overlay',

            // Events.
            events: {
                'click .film': 'close',
                'click .close-wrap': 'close'
            },

            /**
             * Initialize view.
             */
            initialize: function(options) {
                options = $.extend(true, {
                        delay: {
                            playlistAutoClose: 250
                        },
                        animations: {
                            open: {
                                duration: 250,
                                easing: 'ease-in-out'
                            },
                            close: {
                                duration: 250,
                                easing: 'ease-in-out'
                            }
                        }
                    }, options);

                // Cache references to common elements/calculations.
                this.win = App.get('win');
                this.body = App.get('body');
                this.scrollEl = App.get('scrollEl');

                this.init = false;

                // call base class initialize
                BaseView.prototype.initialize.call(this, options);
            },

            afterViewRemove: function(fromPath, toPath){
                //PubSub.trigger('close:searchResults');
            },

            beforeReveal: function(fromPath, toPath, htmlFrag){
                //PubSub.trigger('open:searchResults');
            },

            afterReveal: function(fromPath, toPath){
                if (!this.init){
                    this.init = true;
                }
            },

            close: function(){
                StateManager.navigateToPreloadedUrl();
            }

        });


        /**
         * Return view class.
         */
        return SportsOverlayView;
    }
);
