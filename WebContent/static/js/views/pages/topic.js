/**
 * @fileoverview Topic view.
 * @author kris.hedstrom@f-i.com (Kris Hedstrom)
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'app',
    'pubsub',
    'views/pages/stag-front',
    'views/modules/featured-content',
    'views/modules/previous-bar',
    ],
    function(
        $,
        _,
        Backbone,
        App,
        PubSub,
        StagFront,
        FeaturedContent,
        PreviousBar
    ) {


        /**
         * View class.
         */
        var TopicView = StagFront.extend({

            // View element.
            el: '#topic-card',

            _initializeTopic: function(fromPath){
                if(App.get('contenttype') == 'story') {
                    this.subviews.prevBar = new PreviousBar({
                        url: '/' + fromPath,
                        title: App.get('seotitle')
                    });
                }

                //calculations for size of featured content items / container
                var itemWidth = this.$('.mod.featured-content .paginator ul > li').outerWidth();
                var containerWidth = this.$('.mod.featured-content .paginator').outerWidth();
                var itemsPerView = containerWidth / itemWidth;

                // Initialize the Featured Content module
                this.subviews.featured = new FeaturedContent({
                    el : '.headlines.stag',
                    itemWidth: itemWidth,
                    itemsPerView: itemsPerView,
                    transition : {
                        interval : 15000
                    }
                });

                // call base class initialize
                StagFront.prototype._initializeTopic.call(this, fromPath);
            }
        });


        /**
         * Return view class.
         */
        return TopicView;
    }
);
