/**
 * @fileoverview Story view.
 * @author Jonathan Hensley
 */
define(['jquery', 'underscore', 'backbone', 'pubsub', 'views/modules/footer'],
    function($, _, Backbone, PubSub, Footer) {


        /**
         * View class.
         */
        var StaticPageView = Backbone.View.extend({

            // Events.
            events: {
                'click .close': 'close',
                'click .page-content dl dt': 'toggleQandA'
            },

            /**
             * Initialize view.
             * @param {Object} options View options passed during init.
             */
            initialize: function(options) {
                this.render();

                this.namespace = this.cid;

                this.footer = new Footer();
            },

            /**
             * Load data.
             */
            render: function() {

                // Initialize required module views.
                return this;
            },

            /**
             * Close page.
             * @param {Event} e Click event to close overlay.
             */
            close: function(e) {
                PubSub.trigger('close:overlay');
                this.destroy();
                return false;
            },

            /**
             * Unbind and remove.
             */
            destroy: function() {
                $(window).off("." + this.namespace);
                this.remove();
            },

            /**
             * Toggle the Answer in a Q & A area open and closed
             * @param {Event} e Click event to trigger q & a toggle.
             */
            toggleQandA: function(e) {
                $(e.target).parent().toggleClass('open');
                return false;
            }

        });


        /**
         * Return view class.
         */
        return StaticPageView;
    }
);
