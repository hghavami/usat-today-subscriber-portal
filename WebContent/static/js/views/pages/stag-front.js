/**
 * @fileoverview Topic view.
 * @author kris.hedstrom@f-i.com (Kris Hedstrom)
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'app',
    'pubsub',
    'baseview',
    'views/modules/alert',
    'views/modules/footer',
    'views/modules/utility-bar',
    'views/modules/headline',
    'views/modules/video',
    'views/modules/carousel',
    'views/modules/featured-content',
    'views/modules/form',
    'chosen'
],
function(
    $,
    _,
    Backbone,
    App,
    PubSub,
    BaseView,
    Alert,
    Footer,
    UtilityBar,
    Headline,
    Video,
    Carousel,
    FeaturedContent,
    FormView
) {


        /**
         * View class.
         */
        var StagView = BaseView.extend({

            // Events.
            events: {
                'click .ui-btn': 'toggleHeadlineView'
            },

            /**
             * Initialize view.
             * @param {Object} options View options passed during init.
             */
            initialize: function(options) {
                this.$top = App.get('scrollEl');

                // call base class initialize
                BaseView.prototype.initialize.call(this, options);
            },

            beforeReveal: function(fromPath, toPath, htmlFrag){
                // Skin select element.
                // this.sortSelect = htmlFrag.find('#topic-sort-order');
                // this.sortSelect.chosen({
                //     disable_search_threshold: 30
                // });
            },

            afterReveal: function(fromPath, toPath){
                App.set('currentCardPath', toPath);
                this._initializeTopic(fromPath);
            },

            _initializeTopic: function(fromPath){
                var _this = this;
                this.headlineCollection = this.$('.collection');
                this.toggleList = this.$('.ui-btn.list');
                this.toggleGrid = this.$('.ui-btn.grid');

                this.subviews.video = [];
                // Initialize each video as its own video instance.
                $('.stag.hero .video').each(function() {
                    var video = new Video({
                        // Use .video parent() because hero markup is different.
                        el: $(this).parent().get(0)
                    });
                    _this.subviews.video.push(video);
                });

                // Apply common form logic.
                this.subviews.formview = new FormView({
                    el: this.el
                });

                // Init footer.
                this.subviews.footer = new Footer();

                // Initialize required module views.
                this.subviews.utilityBar = new UtilityBar({
                    noTransition: true
                });

                // Initialize the headline grid.
                this.subviews.headline = new Headline({
                    el : '.stag .mod.headlines'
                });

                // Initialize the gallery carousel.
                this.subviews.gallery = new Carousel({
                    el : '.stag .mod.galleries',
                    fullScreen : true,
                    track : true
                });

                // Defers until the current call stack has cleared.
                _.defer(this.setPage);
            },

            /**
             * Adjust the topic card to the top.
             */
            adjust: function() {
                this.$top.animate({scrollTop: 0}, 200);
            },

            /**
             * Toggle the headline view between grid and list.
             * @param {Event} event Click event.
             */
            toggleHeadlineView: function(event) {
                event.stopPropagation();
                event.preventDefault();

                var currentTarget = $(event.currentTarget);
                if(currentTarget.hasClass('grid')) {
                    //change to grid from list
                    this.headlineCollection.removeClass('listview').addClass('gridview');
                    currentTarget.addClass('active');
                    this.toggleList.removeClass('active');
                }
                else if(currentTarget.hasClass('list')) {
                    //change to list from grid
                    this.headlineCollection.addClass('listview').removeClass('gridview');
                    currentTarget.addClass('active');
                    this.toggleGrid.removeClass('active');
                }
            },

            /**
             * Set page data and trigger page:load event for ads, analytics, etc.
             */
            setPage: function() {
                // Temporary.
                var pageInfoObj = App.getPageInfo(this.$el) || {};
                App.set('pagename', pageInfoObj.topic);
                App.set('ssts', pageInfoObj.ssts);
                App.set('aws', pageInfoObj.aws);
                App.set('url', 'http://www.usatoday.com/' + Backbone.history.fragment);
                App.set('contenttype', pageInfoObj.contenttype);
                App.set('templatetype', pageInfoObj.templatetype);
                var page = {
                    pagename: App.get('pagename'),
                    ssts: App.get('ssts'),
                    aws: App.get('aws'),
                    url: App.get('url'),
                    contenttype: App.get('contenttype'),
                    templatetype: App.get('templatetype'),
                    subviews: this.subviews,
                    asset_collection: pageInfoObj.asset_collection
                };
                // End temporary.
                PubSub.trigger('page:load', page);
            }

        });


        /**
         * Return view class.
         */
        return StagView;
    }
);
