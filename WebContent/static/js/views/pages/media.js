/**
 * @fileoverview Media Gallery.
 * @author jmerrifiel@gannett.com (Jay Merrifield)
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'pubsub',
    'app',
    'baseview',
    'views/modules/carousel',
    'views/modules/fullscreen',
    'views/modules/vertical-scroll',
    'views/modules/video',
    'views/modules/utility-bar'
],
function(
    $,
    _,
    Backbone,
    PubSub,
    App,
    BaseView,
    Carousel,
    FullScreen,
    VerticalScroll,
    Video,
    UtilityBar
)
    {
        /**
         * View class.
         */
        var MediaView = BaseView.extend({
            el: '#media-view',
            events: {
                'click .media-tabs': 'mediaTabClick',
                'mouseout .media-playlist': 'mediaPlaylistMouseOut',
                'mouseover .media-playlist': 'mediaPlaylistMouseIn',
                'click .filter-group li label': 'filterSelect',
                'click .search-toggle': 'searchToggle',
                'click .media-playlist a': 'changeMediaClick',
                'click .image-view div.fullscreen': 'imageFullScreen',
                'click .sidenav > li': 'sectionHighlight',
                'click .tabgroup a': 'onClickTabgroupLinks'
            },
            initialize: function(options){
                options = $.extend(true, {
                        delay: {
                            playlistAutoClose: 250
                        },
                        animations: {
                            cinematic: {
                                change: {
                                    fadeOut: {
                                        duration: 300,
                                        easing: 'easeOutSine'
                                    },
                                    fadeIn: {
                                        duration: 300,
                                        easing: 'easeInQuad'
                                    }
                                }
                            }
                        }
                    }, options);

                this.win = App.get('win');
                this.scrollEl = App.get('scrollEl');
                this.breakingBar = App.get('breakingEl');

                this.win.on('resize.' + this.cid, _.throttle(_.bind(this.handleResizeWindow, this), 50));

                // base class will attach and detach this
                this.pubSub = {
                    'breakingbar:before:open': _.bind(this.adjustTop, this, true),
                    'breakingbar:before:close': _.bind(this.adjustTop, this, false)
                };

                this.playlistTimer = null;
                this.init = false;

                //$('.sidenav li.active div, .sidenav li.sub-active div').show();

                // call base class initialize
                BaseView.prototype.initialize.call(this, options);

                var section = $('.sidenav > li.sub-active');
                this.lastSectionName = section.attr('data-section');
            },
            handleResizeWindow: function(){
                this._setContentHeight(this.$el, this.breakingBar.height());
                if(this.browse) this.columnize(this.$el);
            },
            _setContentHeight: function(html, offset, animate){
                var height = $(window).height();
                var navHeight = 40 +139; //$('#header .fixed-wrap').height();
                offset = offset || 0;
                var h = height - (navHeight + offset);
            },
            beforeReveal: function(fromPath, toPath, htmlFrag){
                if (!this.isCinematicPath(toPath)){
                    this.columnize(htmlFrag);
                }
            },
            afterReveal: function(fromPath, toPath){
                if (this.isCinematicPath(toPath)){
                    this.subviews.utilityBar = new UtilityBar();
                    if (toPath.indexOf('/gallery') !== -1){
                        this._initializeGallery();
                    }else if (toPath.indexOf('/photo') !== -1){
                        this._initializeImage(this.$el);
                    }else if (toPath.indexOf('/video') !== -1){
                        this._initializeVideo(this.$el);
                    }
                    this.browse = false;
                    if (!this.isCinematicPath(fromPath)){
                        this.playlistStuckOpen = false;
                        this.delayClosePlaylist();
                        if (fromPath){
                            // set the back url if we have one, otherwise it defaults to '/media'
                            // fromPath doesn't start with '/'
                            this.$('.back-to-media').attr('href', '/' + fromPath);
                        }
                    }
                } else {
                    // Browse view.
                    this._initializeBrowse();
                }
                _.defer(this.setPage);
            },
            afterViewRemove: function(fromPath, toPath){

            },
            getTransitionAnimation: function(fromPath, toPath, htmlFrag){
                if (this.isCinematicPath(toPath)){
                    if (!this.isCinematicPath(fromPath)){
                        // transition to cinematic view
                        return 'left';
                    }else{
                        // transitioning from cinematic to cinematic
                        return this._fadeInNewCinematicContent(toPath, htmlFrag);
                    }
                }else{ // this isn't a cinematic path
                    if (this.isCinematicPath(fromPath)){
                        // but we're on cinematic mode, slide out cinematic to the right
                        return 'right';
                    }
                }
            },
            isCinematicPath: function(path){
                return path && path.indexOf('/cinematic') !== -1;
            },
            _fadeInNewCinematicContent: function(toPath, htmlFrag){
                // this deferred only resolves when the mainViewfinishes
                var deferred = jQuery.Deferred();
                this.$('.related-media').fadeTo(
                    this.options.animations.cinematic.change.fadeOut.duration,
                    0,
                    this.options.animations.cinematic.change.fadeOut.easing,
                    _.bind(function() {
                        var newView = $(htmlFrag).find('.media-main-view');
                        newView.css('opacity', 0); // hide the new so it fades in
                        this.$('.related-media').replaceWith(newView);
                        newView.fadeTo(
                            this.options.animations.cinematic.change.fadeIn.duration,
                            1,
                            this.options.animations.cinematic.change.fadeIn.easing
                        );
                    }, this)
                );
                this.$('.media-main-view').fadeTo(
                    this.options.animations.cinematic.change.fadeOut.duration,
                    0,
                    this.options.animations.cinematic.change.fadeOut.easing,
                    _.bind(function() {
                        var newView = $(htmlFrag).find('.media-main-view');
                        newView.css('opacity', 0); // hide the new so it fades in
                        this.$('.media-main-view').replaceWith(newView);
                        if (!newView.length){
                            // something went wrong, resolve the animation cause fadeTo doesn't
                            // work on empty elements
                            deferred.resolve();
                        }else{
                            newView.fadeTo(
                                this.options.animations.cinematic.change.fadeIn.duration,
                                1,
                                this.options.animations.cinematic.change.fadeIn.easing,
                                _.bind(function(){
                                    deferred.resolve();
                                }, this)
                            );
                        }
                    }, this)
                );
                return deferred.promise();
            },
            _initializeGallery: function(){
                var html = this.$('.galleries');
                this.subviews.carousel = new Carousel({
                    el: html[0],
                    fullScreen: true,
                    color: 'black'
                });
                // this isn't needed because we start with large images from the server
                // this._flipToLargeImages(html);
            },
            _initializeVideo: function(html){
                this.subviews.video = new Video({
                    el: html,
                    autostart: true
                });
            },
            _initializeImage: function(html){
                this._flipToLargeImages(html);
            },
            _flipToLargeImages: function(html){
                $(html).find('.viewport img').each(function() {
                    var t = $(this);
                    if (t.attr('data-src-large')) {
                        var lg = t.attr('data-src-large');
                        t.attr('src', lg);
                    }
                });
            },

            /**
             * Handle click events on the tabgroup links.
             */
            onClickTabgroupLinks: function(e) {
                this.$('.tabgroup a').removeClass('active');
                $(e.currentTarget).addClass('active');
            },
            destroy: function(removeEl){
                this.win.off('.' + this.cid);
                // call base class destroy
                BaseView.prototype.destroy.call(this, removeEl);
            },
            imageFullScreen: function(e){
                e.preventDefault();
                var fullScreenView = new FullScreen({
                    parent: this.$('.viewport')[0]
                });
                return false;
            },
            changeMediaClick: function(e){
                // this function only changes the highlight state
                // actual navigation is handled by the routes
                this.$('.playlist li.active').removeClass('active');
                $(e.target).closest('li').addClass('active');
            },
            mediaTabClick: function(e){
                var $playlist = this.$('.media-playlist');
                var $target = $(e.target);
                if ($target.hasClass('expando')){
                    if ($playlist.hasClass('open')){
                        this.playlistStuckOpen = false;
                        $playlist.removeClass('open');
                    }else{
                        this.playlistStuckOpen = true;
                        $playlist.addClass('open');
                    }
                }else if ($target.hasClass('tab')){
                    $playlist.addClass('open');
                    if (!$target.hasClass('active')){
                        $playlist.find('.media-tabs .active').removeClass('active');
                        $target.addClass('active');
                        $playlist.find('.playlist').addClass('hidden');
                        $playlist.find('.' + $target.attr('for')).removeClass('hidden');
                    }
                }
            },
            mediaPlaylistMouseOut: function(){
                if (!this.playlistStuckOpen){
                    this.delayClosePlaylist();
                }
            },
            mediaPlaylistMouseIn: function(){
                clearTimeout(this.playlistTimer);
            },
            delayClosePlaylist: function(){
                this.playlistTimer = setTimeout($.proxy(function(){
                    this.$('.media-playlist').removeClass('open');
                }, this), this.options.delay.playlistAutoClose);
            },

            /**
             * Initialize browse / overview page.
             */
            _initializeBrowse: function() {
                this.navContainer = $('.sidenav-container', this.$el);

                this.gridWrapper = this.$('.grid-wrapper');

                this.browse = true;

                if (this.imageFullScreen) { this.$('.close').trigger('click'); }


            },

            /**
             * Create columns for the browse view, based on page width.
             */
            columnize: function(htmlFrag) {
                var columnSize = 360;
                var gridItems = htmlFrag.find('.grid > li');
                var pageWidth = $(window).width();
                var containerWidth = pageWidth - 120 - 30;
                var columns = Math.floor(containerWidth / columnSize);
                if (columns < 1){
                    columns = 1;
                }
                this.browseContent = htmlFrag.find('.content');
                this.browseContent.width(columns * columnSize + 30);
                gridItems.removeClass('featured');
                var t = gridItems.slice(0, columns);
                t.addClass('featured');
                if (t.attr('data-src-large')) {
                    var lg = t.attr('data-src-large');
                    t.attr('src', lg);
                }

                //this.adjustMediaHeight(htmlFrag);
            },

            /**
             * Handle filter selections.
             * @param {Event} e Click event
             */
            filterSelect: function(e) {
                var label = $(e.currentTarget);
                var filterGroup = label.parents('.filter-group').find('label');
                filterGroup.removeClass('active');
                label.addClass('active');
                // @TODO: do the actual filtering.
            },

            /**
             * Adjust the top offset of the media wrapper
             * @param {Boolean} open Open or closed.
             */
            adjustTop: function(open) {
                if(this.isBreakingBarOpen === open) {
                    this.$el.addClass('no-transition');
                    if(open) {
                        this.$el.addClass('offset');
                    }
                    else {
                        this.$el.removeClass('offset');
                    }
                    return;
                }
                else {
                    this.$el.removeClass('no-transition');
                }
                this.isBreakingBarOpen = open;

                var timeout = 250;
                if(open) {
                    this.$el.addClass('offset');
                    setTimeout(_.bind(function() {
                        this._setContentHeight(this.$el, this.breakingBar.height());
                        if (!this.isCinematicPath(this.currentPath)){
                            this.adjustMediaHeight(this.$el);
                        }
                    }, this), timeout);
                } else {
                    this._setContentHeight(this.$el);
                    this.$el.removeClass('offset');
                    if (!this.isCinematicPath(this.currentPath)){
                        setTimeout(_.bind(function() {
                            this.adjustMediaHeight(this.$el);
                        }, this), timeout);
                    }
                }

            },

            /**
             * Adjust the top offset of the media wrapper
             */
            adjustMediaHeight: function(htmlFrag) {
                this.adjustTop(this.isBreakingBarOpen);
            },


            /**
             * Toggle search panel.
             * @param {Event} e Click event
             */
            searchToggle: function(e) {
                if(e) {
                    e.stopPropagation();
                    e.preventDefault();
                }

                var toggle = $(e.currentTarget);

                if(toggle.hasClass('active')) {
                    toggle.removeClass('active');
                    this.browseContent.removeClass('search');
                    this.searchOpen = false;
                } else {
                    toggle.addClass('active');
                    this.browseContent.addClass('search');
                    this.searchOpen = true;
                }
                this.adjustMediaHeight(this.$el);
            },

            /**
             * Toggle subsections.
             * @param {Event} e Click event
             */
            sectionHighlight: function(e) {

                var target = $(e.currentTarget);
                var section = $('.sidenav > li');
                var subsection = $('.sidenav li div');
                var activeSub = target.children('div');
                    // activeSubHeight = activeSub.outerHeight();
                var href = target.find('a').attr('href');

                if(!target.hasClass('active') && this.lastSectionName !== target.attr('data-section')) {

                    this.lastSectionName = target.attr('data-section');
                    this.startNavigationAnimation(
                        $.when(subsection.slideUp(250), activeSub.slideDown(250))
                    );
                    section.removeClass('active');
                    target.addClass('active');

                    // setTimeout(function() {window.location = href}, 250);
                    // return false;
                }
            },

             /**
             * Set page data and trigger page:load event for ads, analytics, etc.
             */
            setPage: function() {
                // Temporary.
                var pageInfoObj = App.getPageInfo(this.$el) || {};
                var section = pageInfoObj.section_name,
                    page,
                    ssts;
                    ssts = pageInfoObj.ssts;
                App.set('pagename', section);
                App.set('ssts', ssts);
                App.set('aws', pageInfoObj.aws);
                App.set('url', 'http://www.usatoday.com/' + Backbone.history.fragment);
                App.set('contenttype', pageInfoObj.media_type);
                App.set('contentId', pageInfoObj.contentid);
                App.set('seotitle', ssts);
                App.set('templatetype', pageInfoObj.base_page_type);

                 page = {
                    pagename: App.get('pagename'),
                    ssts: App.get('ssts'),
                    aws: App.get('aws'),
                    url: App.get('url'),
                    contenttype: App.get('contenttype'),
                    contentId: App.get('contentId'),
                    sectionPath: section,
                    seotitle: App.get('seotitle'),
                    templatetype: App.get('templatetype')
                };
                // End temporary.
                PubSub.trigger('page:load', page);
            }

        });
        /**
         * Return view class.
         */
        return MediaView;
    }
);
