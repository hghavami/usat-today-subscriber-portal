/**
 * @fileoverview Search view.
 * @author stephen.burgess@f-i.com (Stephen Burgess)
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'pubsub',
    'app',
    'baseview',
    'state'
],
function(
    $,
    _,
    Backbone,
    PubSub,
    App,
    BaseView,
    StateManager
)
    {

        /**
         * View class.
         */
        var SearchView = BaseView.extend({

            // View element.
            el: '.search-results',

            // Events.
            events: {
                'click .grid': 'gridView',
                'click .list': 'listView',
                'click .close': 'close',
                'click .pager a': 'getMoreResults'
            },

            /**
             * Initialize view.
             */
            initialize: function(options) {
                options = $.extend(true, {
                        delay: {
                            playlistAutoClose: 250
                        },
                        animations: {
                            open: {
                                duration: 250,
                                easing: 'ease-in-out'
                            },
                            close: {
                                duration: 250,
                                easing: 'ease-in-out'
                            }
                        }
                    }, options);

                // Cache references to common elements/calculations.
                this.win = App.get('win');
                this.body = App.get('body');
                this.scrollEl = App.get('scrollEl');

                // Set initial view state.
                this.currentView = 'view-grid';

                this.init = false;

                // call base class initialize
                BaseView.prototype.initialize.call(this, options);
            },

            animateRevealView: function(fromPath, toPath){
                if (fromPath == null){
                    return $.Deferred().resolve();
                }else{
                    this.body.addClass('show-search');
                    this.$el.css({opacity: 1, display: 'block', top: '-100%', height: '100%'});
                    var promise = this.animate(this.$el, 'top', '0%',
                                                    this.options.animations.open.duration,
                                                    this.options.animations.open.easing);
                    promise.done(_.bind(function(){
                        this.$el.css('height', 'auto');
                        PubSub.trigger('open:searchResults');
                    }, this));
                    return promise;
                }
            },

            animateRemoveView: function(toPath){
                var scrollPosition = App.getScrollPosition();
                this.body.removeClass('show-search');
                this.$el.css({marginTop: -1 * scrollPosition,
                                height: this.win.height() + scrollPosition});
                this.scrollEl.scrollTop(0);
                return this.animate(this.$el, 'top', '-100%',
                                                    this.options.animations.close.duration,
                                                    this.options.animations.close.easing);
            },

            afterViewRemove: function(fromPath, toPath){
                PubSub.trigger('close:searchResults');
            },

            beforeReveal: function(fromPath, toPath, htmlFrag){
                PubSub.trigger('open:searchResults');
            },

            afterReveal: function(fromPath, toPath){
                if (!this.init){
                    this.init = true;
                    // stash the original path so we know where we came from
                    this.originalPath = fromPath;
                }
                this.searchWrap = this.$('#search-form');
                this.searchTextInput = this.searchWrap.find('.text-input');
                this.setup();
                this.setupTextAd();
                _.defer(this.setPage);
            },

            /**
             * Get more search results
             */
            getMoreResults: function(e) {
                var $pager = $(e.currentTarget),
                    fetchUrl = $pager.attr('href');
                StateManager.fetchHtml(fetchUrl).done(function(htmlFrag){
                    $pager.parent().replaceWith(htmlFrag);
                });
                return false;
            },

            getTerm: function() {
                return $('.search-term .term', this.$el).attr('data-term');
            },

            /**
             * Setup
             */
            setup: function() {
                this.results = this.$('.results');
                this.summary = this.$('.summary');

                this.sidebar = this.$('.sidebar');
                this.viewGrid = this.$('.grid');
                this.viewList = this.$('.list');

                if(this.currentView === 'view-list') {this.listView();}
            },

            setupTextAd : function() {
                // grab term
                var term = this.getTerm();
                // grab element
                var id = 'partner_search_text_ads' + new Date().getTime();
                $('.tile.ad', this.$el).attr('id', id);
                var pageOptions = { 
                  'pubId': 'partner-gannett',
                  'query': term,
                  'hl': 'en'
                };

                var adblock1 = { 
                  'container': id,
                  //'number': '3', // Let Google decide
                  'width': '200px',
                  'lines': '3',
                  'fontFamily': 'arial',
                  'fontSizeTitle': '13px',
                  'fontSizeDescription': '12px',
                  'fontSizeDomainLink': '12px',
                  'colorTitleLink': '1EA3FF',
                  'colorText': 'FFFFFF',
                  'colorDomainLink': '00C641',
                  'colorBackground': '2C2C2C'
                };

                new google.ads.search.Ads(pageOptions, adblock1);
            },

            /**
             * Grid view
             */
            gridView: function() {
                this.viewGrid.addClass('active');
                this.viewList.removeClass('active');
                this.summary.removeClass('list').addClass('grid');
                this.results.removeClass('view-list').addClass('view-grid');
                this.currentView = 'view-grid';
                return false;
            },


            /**
             * List view
             */
            listView: function() {
                this.viewList.addClass('active');
                this.viewGrid.removeClass('active');
                this.summary.removeClass('grid').addClass('list');
                this.results.removeClass('view-grid').addClass('view-list');
                this.currentView = 'view-list';

                return false;
            },

            close: function(){
                StateManager.navigateToPreloadedUrl(this.originalPath);
            },

            /**
             * Set page data and trigger page:load event for ads, analytics, etc.
             */
            setPage: function() {
                var pageInfoObj = App.getPageInfo(this.$el) || {},
                    templatetype = this.currentView,
                    page;
                 if(templatetype === undefined){
                    templatetype = 'view-grid';
                }
                if(pageInfoObj){
                    App.set('keywords', pageInfoObj.keywords);
                }
                App.set('url', 'http://www.usatoday.com/' + Backbone.history.fragment);
                App.set('contenttype', 'search');
                App.set('templatetype', templatetype);
                App.set('ssts', 'search');
                App.set('cst', 'search');
                page = {
                    url: App.get('url'),
                    contenttype: App.get('contenttype'),
                    templatetype: App.get('templatetype'),
                    searchkeywords: App.get('keywords'),
                    ssts: App.get('ssts'),
                    cst: App.get('cst')
                };
                PubSub.trigger('page:load', page);
            }

        });


        /**
         * Return view class.
         */
        return SearchView;
    }
);
