/**
 * @fileoverview Homepage view.
 * @author erik.kallevig@f-i.com (Erik Kallevig)
 *
 * HomeView extends BasePage (view/pages/base-page.js).
 * If you require to override any of BasePage's methods please
 * ensure you called it's super method using the same technique
 * demonstrated in the initialize method:
 * BasePage.prototype.[overridden method name].apply(this, arguments);
 *
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'pubsub',
    'app',
    'views/pages/base-page',
    'views/modules/ajax-widget'
],
function(
    $,
    _,
    Backbone,
    PubSub,
    App,
    BasePage,
    AjaxWidget
)
    {

        /**
         * View class.
         */
        var HomeView = BasePage.extend({

            // View element.
            el: 'body',

            /**
             * Initialize view.
             */
            initialize: function(path, sectionPath) {
                //calls super method
                BasePage.prototype.initialize.apply(this, arguments);
            },

            /**
             * Load data.
             */
            render: function() {
                //calls super method
                BasePage.prototype.render.apply(this, arguments);

                // Check for markets, if found - instantiate Ajax Widget
                if($('.load-markets').length){
                    this.subviews.markets = new AjaxWidget({
                        el : '.load-markets',
                        url : '/services/markets/home-widget/',
                        refresh : 60000,
                        contents : ".lottery-wrapper"
                    });
                }

            }

        });


        /**
         * Return view class.
         */
        return HomeView;
    }
);
