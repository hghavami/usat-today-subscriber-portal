/**
 * @fileoverview User Auth view.
 * @author jheiner@usatoday.com (John Heiner)
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'app',
    'pubsub',
    'baseview',
    'facebook',
    'fwinfo',
    '//connect.facebook.net/en_US/all.js'
],
function(
    $,
     _,
     Backbone,
     App,
     PubSub,
     BaseView,
     Facebook,
     FwInfo
)
{


        /**
         * View class.
         */
        var UserAuthView = BaseView.extend({

            // View element.
            el: '#userauth-card',

            /**
             * Initialize view.
             * @param {Object} options View options passed during init.
             */
            initialize: function(options) {
                this.$top = App.get('scrollEl');

                // call base class initialize
                BaseView.prototype.initialize.call(this, options);

            },
            afterReveal: function(fromPath, toPath){
                App.set('currentCardPath', toPath);

                Facebook.init(function(){return;},true);

                // Defers until the current call stack has cleared.
                _.defer(this.setPage);
                $('.fb-login-button').attr( 'scope', Facebook.permissions() );
                Facebook.parse();
            },
            /**
             * Adjust the topic card to the top.
             */
            adjust: function() {
                this.$top.animate({scrollTop: 0}, 200);
            },

            /**
             * Set page data and trigger page:load event for ads, analytics, etc.
             */
            setPage: function() {
                // Temporary will be hardcoded values till we get requirements from analytics team 
                App.set('pagename', 'miscpage');
                App.set('aws','staticpage');
                App.set('ssts', 'staticpage');
                App.set('url', 'http://www.usatoday.com/' + Backbone.history.fragment);

                App.set('contenttype', 'sectionfrontpage');
                App.set('contentParentId', 'card');
                App.set('seotitle', 'miscpage');
                App.set('templatetype', 'templatetype');
                var page = {
                    pagename: App.get('pagename'),
                    aws:App.get('aws'),
                    ssts: App.get('ssts'),
                    url: App.get('url'),
                    contenttype: App.get('contenttype'),
                    contentParentId: App.get('contentParentId'),
                    sectionPath : "",
                    seotitle: App.get('seotitle'),
                    templatetype: App.get('templatetype'),
                    subviews: this.subviews
                };
                // End temporary.
                PubSub.trigger('page:load', page);
            }
        });


        /**
         * Return view class.
         */
        return UserAuthView;
    }
);
