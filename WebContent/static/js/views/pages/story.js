/**
 * @fileoverview Story view.
 * @author Chris Manning
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'pubsub',
    'app',
    'baseview',
    'views/modules/utility-bar',
    'views/modules/video',
    'views/modules/audio',
    'views/modules/carousel',
    'views/modules/map',
    'views/modules/interactives/before-after',
    'views/modules/interactives/timeline',
    'views/modules/interactives/chronology',
    'views/modules/interactives/sequencer',
    'views/modules/sponsored-series-bar',
    'fitvids'
],
function(
    $,
    _,
    Backbone,
    PubSub,
    App,
    BaseView,
    UtilityBar,
    Video,
    Audio,
    Carousel,
    Map,
    iBeforeAfter,
    iTimeline,
    iChronology,
    iSequencer,
    SponsoredSeriesBar
)
    {

        /**
         * View class.
         */
        var StoryView = BaseView.extend({

            // Events.
            events: {
                'click .credits .comments': 'loadComments',
                'click .credits .share': 'loadShare'
            },

            /**
             * Initialize view.
             * @param {Object} options View options passed during init.
             */
            initialize: function(options) {
                this.el = options.el;
                this.win = App.get('win');
                _.bindAll(this, 'setPage');

                // call base class initialize
                BaseView.prototype.initialize.call(this, {});
                this.render();

                // this.updatePrevInfo();
                // PubSub.trigger('shouldDisplayPrevBar:overlay', this.setPrevStoryVisiblity);

                // Temporary. Triggers page:load for Framework to use.
            },

            /**
             * Check for inline third party video and images.
             * adds open in new window fallback and handler to override app.js triggeroute event
             */
            checkForInlineContent: function() {
                var story_content =this.$el.find('div.double-wide');

                //parses story view for inline videos for fitfits.js
                story_content.fitVids();

                //parses inline imgs for orientation, needs to be replaced with template handeling if possible
                story_content.find('img').each(function(){
                    if($(this).is('div.double-wide > img, div.double-wide > p > img')){
                        $(this).addClass(this.width > this.height ? 'landscape' : 'portrait').addClass('clearFix');
                    }
                });

                //parses inline links for uotrack, needs to be replaced with template handeling if possible
                story_content.find('a').each(function(){
                    if($(this).is('div.double-wide > a, div.double-wide > p > a')){
                        $(this).attr('data-uotrack', 'storyinlinelink');
                    }
                });

            },

            /**
             * Open 'comment' flyout.
             */
            loadComments: function() {
                $('.comment-module .btn').trigger('click');
            },

            /**
             * Open 'share' flyout.
             */
            loadShare: function() {
                $('.share-module .btn').trigger('click');
            },

            /**
             * Load data.
             */
            render: function() {
                var _this = this;
                this.setHeight();
                this.win.on('resizestop.' + this.cid, _.bind(this.setHeight, this));

                // Initialize the subviews module.
                this.subviews.utilitybar = new UtilityBar();

                // this.subviews.fbconnect = new FacebookConnectView();

                // Initialize required module views.
                this.setupSub('timeline', '.interactive.timeline', iTimeline);

                this.setupSub('chron', '.interactive.chronology', iChronology);

                this.setupSub('sequencer', '.interactive.sequencer', iSequencer);

                this.setupSub('before-after', '.before-after', iBeforeAfter);

                this.subviews.galleries = [];
                $.each(this.$el.find('.galleries'), function(i) {
                    var gallery = new Carousel({
                        el: $(this).get(0),
                        fullScreen: true,
                        track: true
                    });
                    _this.subviews.galleries.push(gallery);
                });

                this.subviews.video = [];
                $.each(this.$el.find('.video'), function(i) {
                    var video = new Video({
                        el: $(this).get(0)
                    });
                    _this.subviews.video.push(video);
                });

                this.subviews.maps = [];
                $.each(this.$el.find('.map .mapboxWrap'), function(i) {
                    var map = new Map({
                        el: $(this)
                    });
                    _this.subviews.maps.push(map);
                });

                this.subviews.audio = [];
                $.each(this.$el.find('.cp-container'), function(i) {
                    var audio = new Audio({
                        el: '#'+$(this).attr('id')
                    });
                    _this.subviews.audio.push(audio);
                });

                // check for sponsored series bar
                var $ssbar = $('.sponsored-series-bar', this.$el);
                if ($ssbar.length > 0) {
                    this.setupSub('sponsored-series-bar', '.sponsored-series-bar', SponsoredSeriesBar);
                }

                this.checkForInlineContent();

                // Defers until the current call stack has cleared.
                _.defer(this.setPage);
            },

            /**
             * Set min-height on the asset element.
             */
            setHeight: function() {
                this.$el.find('article.asset').css("min-height", this.win.height() - 80);
            },

            /**
             * Set page data and trigger page:load event for ads, analytics, etc.
             */
            setPage: function() {
                // Temporary.
                var story_name = $('article h1', this.$el).text(),
                    content_id =$('article.asset', '#collection').attr('data-assetid'),
                    pageInfoObj = App.getPageInfo(this.$el) || {
                        ss: 'undefined',
                        aws: 'undefined'
                    },
                    texttype =$('article.asset', '#collection').attr('data-texttype'),
                    page;
                App.set('pagename', story_name);                
                App.set('ssts', pageInfoObj.ss);
                App.set('aws', pageInfoObj.aws);                
                App.set('url', 'http://www.usatoday.com/' + Backbone.history.fragment);
                App.set('contenttype', texttype);
                App.set('contentParentId', 'asset');
                App.set('contentId', content_id);
                App.set('seotitle', story_name);
                App.set('templatetype', texttype);
                page = {
                    pagename: App.get('pagename'),
                    ssts: App.get('ssts'),
                    aws: App.get('aws'),
                    url: App.get('url'),
                    contenttype: App.get('contenttype'),
                    contentId: App.get('contentId'),
                    contentParentId: App.get('contentParentId'),
                    seotitle: App.get('seotitle'),
                    templatetype: App.get('templatetype'),
                    asset_collection: App.get('collection'),
                    subviews: this.subviews
                };
                // End Temporary.
                PubSub.trigger('page:load', page);
            },

            /**
             * Helper to create subviews against jQuery selectors.
             * Loops over instances and adds to array for eventual destruction.
             *
             * @param {string} name. Name to give to subview object.
             * @param {string} selector. CSS selector to search against with jQuery.
             * @param {object} View. Backbone View.
             * @param {object} options. Options to pass to View.
             */
            setupSub: function(name, selector, View, options) {
                if (!options) options = {};

                this.subviews[name] = [];
                var _this = this;
                
                $.each(this.$el.find(selector), function(i) {
                    options.el = $(this).get(0);
                    var v = new View(options);
                    _this.subviews[name].push(v);
                });
            }

        });


        /**
         * Return view class.
         */
        return StoryView;
    }
);
