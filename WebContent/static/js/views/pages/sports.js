/**
 * @fileoverview Sports view.
 * @author webninjataylor@gmail.com (Taylor Johnson)
 *
 * SportsView extends BasePage (view/pages/base-page.js).
 * If you require to override any of BasePage's methods please
 * ensure you called it's super method using the same technique
 * demonstrated in the initialize method:
 * BasePage.prototype.[overridden method name].apply(this, arguments);
 *
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'app',
    'pubsub',
    'views/pages/base-page',
    'views/modules/sports/back-to-top',
    'views/modules/sports/scores',
    'views/modules/sports/scoresPageFiltering',
    'views/modules/sports/SportsSubnavSpacer',
    'views/modules/sports/leagues',
    'views/modules/sports/teamLeaders',
    'views/modules/sports/matchups',
    'views/modules/sports/filterSeason',
    'views/modules/sports/team-chooser',
    'views/modules/sports/teamStatistics',
    'views/modules/sports/golf',
    'views/modules/twitter',
    'chosen',
    'libs/jquery/plugins/jquery.dataTables'
],
function(
    $,
    _,
    Backbone,
    App,
    PubSub,
    BasePage,
    BackToTop,
    Scores,
    scoresPageFiltering,
    SportsSubnavSpacer,
    LeaguesView,
    TeamLeaders,
    Matchups,
    FilterSeason,
    TeamChooser,
    TeamStatistics,
    Golf,
    TwitterView)
    {
        //  View class.
        var SportsView = BasePage.extend({

            // View element.
            el: '#section_sports',

            //  Initialize view.
            initialize: function(path, sectionPath) {
                //calls super method
                BasePage.prototype.initialize.apply(this, arguments);

            },

            //  Render view.
            render: function(){
                this.renderSports();
                BasePage.prototype.render.apply(this, arguments);

            },
            /**
             * Named renderSports to avoid conflict with parent's render
             * which is called on initialize. We want this to run in addition
             * to BasePage.render.
             */
            renderSports: function() {
                 // Initialize sportsNav.
                this.subviews.backtotop = new BackToTop();
                this.subviews.scores = new Scores();
                this.subviews.scoresPageFiltering = new scoresPageFiltering();
                this.subviews.sportsNav = new SportsSubnavSpacer();
                this.subviews.leagues = new LeaguesView();
                this.subviews.teamLeaders = new TeamLeaders();
                this.subviews.matchups = new Matchups();
                this.subviews.filterSeason = new FilterSeason();
                this.subviews.teamChooser = new TeamChooser();
                this.subviews.teamStatistics = new TeamStatistics();
                this.subviews.golf = new Golf();
                this.divSportsSelect = this.$el.find('#page_type fieldset select, #scores select');
                this.divSportsSelect.chosen({disable_search_threshold: 30});
                   // Initalize the data table sorter.
                this.$('.sort').dataTable({
                    "bFilter": false, "bDestroy": true, "bPaginate": false, "bInfo": false, "aaSorting": []
                });
                   // Initalize the Twitter Module.
                this.subviews.twitter = new TwitterView({
                    el : 'section[data-section-id="sports"]',
                    num_tweets : 4,
                    columns: 2
                });

            },

            destroy: function() {
                this.undelegateEvents();
            }

        });

        //  Return view class.
        return SportsView;
    }
);
