/**
 * @fileoverview Homepage view.
 * @author erik.kallevig@f-i.com (Erik Kallevig)
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'app',
    'pubsub',
    'baseview',
    'views/modules/live-feed-view',
    'views/modules/footer',
    'views/modules/hero',
    'views/modules/video',
    'views/modules/carousel',
    'views/modules/gallery-carousel',
    'views/modules/tooltip',
    'views/modules/headline'
],
function(
    $,
    _,
    Backbone,
    App,
    PubSub,
    BaseView,
    LiveFeedView,
    Footer,
    Hero,
    Video,
    Carousel,
    GalleryCarousel,
    ToolTip,
    Headline
)
    {

        /**
         * View class.
         */
        var BasePageView = BaseView.extend({

            // View element.
            el: 'body',

            /**
             * Initialize view.
             */
            initialize: function(path, sectionPath) {
                this.setElement('section[data-section-id="'+sectionPath+'"]');

                if (path.substr(-1) == '/') {
                    path = path.substr(0, path.length - 1);
                }
                this.path = path;
                this.sectionPath = sectionPath;
                this.pubSub = {
                    'open:overlay': this.destroy
                };
                _.bindAll(this, 'setPage');

                // call base class initialize
                BaseView.prototype.initialize.call(this, {});
            },


            /**
             * Load data.
             */
            render: function() {
                var sectionPath = this.sectionPath;
                var _this = this;

                // Initialize the hero module.
                this.subviews.hero = new Hero({
                    el : 'section[data-section-id="'+sectionPath+'"] .mod.hero:first'
                });

                // Initialize the hero module's carousel when needed.
                if ($(this.subviews.hero.el).hasClass('carousel')) {
                    this.subviews.heroCarousel = new Carousel({
                        el : this.subviews.hero.el,
                        hoverTransition: 200,
                        transition : {
                            interval : 4000
                        }
                    });
                }

                // Initialize each video as its own video instance.
                this.subviews.video = [];
                $('section[data-section-id="'+sectionPath+'"] .video').each(function() {
                     var video = new Video({
                        // Use .video parent() because hero markup is different.
                        el: $(this).parent().get(0)
                    });
                    _this.subviews.video.push(video);
                });

                // Initialize the live feed.
                this.subviews.livefeed = new LiveFeedView({
                    el : 'section[data-section-id="'+sectionPath+'"] .mod.live-feed',
                    section: this.path
                });

                // Initialize the gallery carousel.
                this.subviews.gallery = new Carousel({
                    ads : true,
                    el : 'section[data-section-id="'+sectionPath+'"] .mod.galleries',
                    fullScreen : true
                });

                // Initialize the gallery list.
                this.subviews.galleryList = new GalleryCarousel({
                    el : 'section[data-section-id="'+sectionPath+'"] .front-galleries'
                });

                // Initialize the tooltip.
                this.subviews.tooltip = new ToolTip({
                    el : '.add-tooltip',
                    offset : {x:-3, y:3}
                });

                // Initialize the headline.
                this.subviews.headline = new Headline({
                    el : 'section[data-section-id="'+sectionPath+'"] .mod.headlines',
                    section : sectionPath //for tracking
                });

                // Initialize footer.
                this.subviews.footer = new Footer();

                // Initialize the FacebookConnect Module.
                // this.subviews.fbconnect = new FacebookConnectView();

                // Defers until the current call stack has cleared.
                _.defer(this.setPage);
            },

            /**
             * Set page data and trigger page:load event for ads, analytics, etc.
             */
            setPage: function() {
                // Temporary.
                var sectionPath = this.sectionPath,
                    pageInfoObj = App.getPageInfo(this.$el) || {},
                    templatetype =this.$el.attr('data-template'),
                    page;
                App.set('pagename', sectionPath);
                var ssts = this.path;
                if (this.path.indexOf('/') == this.path.length - 1) {
                    ssts = this.path.substring(0, this.path.length - 1);
                }
                App.set('ssts', ssts);
                App.set('aws', pageInfoObj.aws);
                App.set('url', 'http://www.usatoday.com/' + Backbone.history.fragment);
                App.set('contenttype', sectionPath);
                App.set('contentParentId', 'card');
                App.set('seotitle', sectionPath);
                App.set('templatetype', templatetype);

                page = {
                    pagename: App.get('pagename'),
                    ssts: App.get('ssts'),
                    aws: App.get('aws'),
                    url: App.get('url'),
                    contenttype: App.get('contenttype'),
                    contentParentId: App.get('contentParentId'),
                    sectionPath: sectionPath,
                    seotitle: App.get('seotitle'),
                    templatetype: App.get('templatetype'),
                    subviews: this.subviews,
                    asset_collection: pageInfoObj.asset_collection
                };
                // End temporary.
                PubSub.trigger('page:load', page);
            }

        });


        /**
         * Return view class.
         */
        return BasePageView;
    }
);
