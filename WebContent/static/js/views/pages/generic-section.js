/**
 * @fileoverview Homepage view.
 * @author erik.kallevig@f-i.com (Erik Kallevig)
 *
 * GenericSectionView extends BasePage (view/pages/base-page.js).
 * If you require to override any of BasePage's methods please
 * ensure you called it's super method using the same technique
 * demonstrated in the initialize method:
 * BasePage.prototype.[overridden method name].apply(this, arguments);
 *
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'app',
    'pubsub',
    'views/pages/base-page',
    'views/modules/twitter',
    'views/modules/featured-content',
    'views/modules/blogs',
    'views/modules/ajax-widget'
],
function(
    $,
    _,
    Backbone,
    App,
    PubSub,
    BasePage,
    TwitterView,
    FeaturedContent,
    Blogs,
    AjaxWidget
)
    {

        /**
         * View class.
         */
        var GenericSectionView = BasePage.extend({

            // View element.
            el: 'body',

            /**
             * Initialize view.
             */
            initialize: function(path, sectionPath) {
                //calls super method
                BasePage.prototype.initialize.apply(this, arguments);
                this.renderGeneric(path, sectionPath);
            },

            renderGeneric: function(path, sectionPath) {
                
                // Initalize the Twitter Module.
                this.subviews.twitter = new TwitterView({
                    el : 'section[data-section-id="'+sectionPath+'"]',
                    num_tweets : 4,
                    columns: 2
                });

                // Initalize the Lottery Module.
                if($('.mod.lottery').length){
                    this.subviews.markets = new AjaxWidget({
                        el : '.mod.lottery',
                        url : '/lottery/front-module/',
                        refresh : 60000,
                        contents : ""
                    });
                }

                // Intialize the Featured Content module
                this.subviews.featured = new FeaturedContent({
                    el : 'section[data-section-id="'+sectionPath+'"]',
                    transition : {
                        interval : 15000
                    }
                });

                // Initalize the Blogs module
                this.subviews.blogs = new Blogs({
                    el : 'section[data-section-id='+this.path+'] .mod.blogs'
                });
            }

        });


        /**
         * Return view class.
         */
        return GenericSectionView;
    }
);
