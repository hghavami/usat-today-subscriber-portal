/**
 * @fileoverview Weather view.
 * @author stwilson@gannett.com (Stan Wilson)
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'app',
    'pubsub',
    'views/modules/weather/weather-map',
    'views/modules/weather/national-settings',
    'views/modules/weather/local-settings',
    'views/modules/weather/forecast-sidebar',
    'views/modules/weather/weather-autocomplete',
    'views/modules/footer',
    'views/modules/carousel',
    'views/modules/gallery-carousel',
    'views/modules/tooltip',
    'views/modules/twitter',
    'cookie'
],
function(
    $,
    _,
    Backbone,
    App,
    PubSub,
    WeatherMap,
    natSettings,
    localSettings,
    Forecast,
    WeatherAutocomplete,
    Footer,
    Carousel,
    GalleryCarousel,
    ToolTip,
    TwitterView,
    ForecastSidebar

)
    {

        /**
         * View class.
         */
        var WeatherView = Backbone.View.extend({
            // View element.
            el: '#section_weather',

            /**
             * Initialize view.
             */
            initialize: function(path, sectionPath) {
                this.win = $(window);
                this.body = $('body');
                this.$el = $(this.el);
                this.sizeBar = this.body.find('section[data-section-id="weather"] .sizer.size-bar');
                this.isOpen = false;
                this.buttonFadeIn = 200;
                this.buttonFadeOut = 200;
                this.viewAnimateIn = 250;
                this.viewAnimateOut = 250;
                this.updateDelay = 30; //seconds
                this.setElement('section[data-section-id="'+sectionPath+'"]');
                this.subviews = {};
                this.path = path;
                this.sectionPath = sectionPath;
                this.pubSub = {
                    'open:overlay': this.destroy,
                    'after:animate:cards': this.render
                };
                PubSub.attach(this.pubSub, this);
                _.bindAll(this, 'setPage');
             },

            /**
             * Load data.
             */
            render: function() {
                this.activeLocation =  {
                        name: 'Mclean VA',
                        city: 'Mclean',
                        countryCode: 'US',
                        latitude: '38.93245690006358',
                        longitude: '-77.2189736366272',
                        state: 'VA'
                };
                this.openButton = this.body.find('section[data-section-id="weather"] .open-live-feed');
                this.closeButton = this.body.find('section[data-section-id="weather"] .close-live-feed');
                this.openButton.bind('click', $.proxy(this.openOpenForecast, this));
                this.closeButton.bind('click', $.proxy(this.closeOpenForecast, this));
                sectionPath = this.sectionPath;
                // Intialize the gallery carousel.
                this.subviews.gallery = new Carousel({
                    ads : true,
                    el : 'section[data-section-id="'+sectionPath+'"] .mod.galleries',
                    fullScreen : true
                });

                // Intialize the gallery list.
                this.subviews.galleryList = new GalleryCarousel({
                    el : 'section[data-section-id="'+sectionPath+'"] .front-galleries'
                });

                // Intialize the tooltip.
                this.subviews.tooltip = new ToolTip({
                    el : '.add-tooltip',
                    offset : {x:-3, y:3}
                });
                // Initalize the Twitter Module.
                this.subviews.twitter = new TwitterView({
                    el : '.size-bar .mod.twitter',
                    num_tweets: 10,
                    columns: 1
                });
                // Initialize footer.
                this.subviews.footer = new Footer();
                // Initialize the FacebookConnect Module
                // this.subviews.fbconnect = new FacebookConnectView();
                // setup weather interactions
                this.subviews.weatherAutocomplete = new WeatherAutocomplete({
                                locationField : ".location-autocomplete",
                                loadButton: ".location-load"
                });
                this.setup();



                // Defers until the current call stack has cleared.
                _.defer(this.setPage);
            },

            /**
             * Clean up registered subviews.
             */
            destroy: function(removeEl) {
                this.undelegateEvents();
                PubSub.unattach(this.pubSub, this);
                _.each(this.subviews, function(view){
                    if (typeof view.destroy !== 'undefined') {
                        view.destroy(removeEl);
                    }
                });
            },

            setup: function(){
                var self = this,
                // get location
                location = $.cookie('weatherLocation') || '22102';
                self.subviews.weatherMap = new WeatherMap();
                // prevent infinite looping
                self.hadError = false;
                // get location data
                self.getLocationData(location);
            },

            getLocationData: function(location) {
                var self = this;
                // get weather data for location
                $.ajax({
                    url: '/services/weather/forecast/json/' + location.replace(/,/g, '') + '/',
                    dataType: 'json',
                    cache: false,
                    success: function(data) {
                        if(data[0] !== undefined && data[0] !== null) {
                            self.subviews.weatherMap.setup([
                                {
                                    $element: $('.wmap-local'),
                                    params: localSettings,
                                    active: true
                                },
                                {
                                    $element: $('.wmap-national'),
                                    params: natSettings
                                }
                            ], data);
                            if(!self.subviews.forecast){
                                self.subviews.forecast = new Forecast( data );
                            }
                        }
                        else if (!self.hadError) {
                            console.warn('ERROR: Unable to find data for ' + location.name);
                            self.hadError = true;
                            self.getLocationData('22102');
                        }
                        else {
                            console.warn('ERROR: Experienced issue with data service')
                        }
                    }
                });
            },

            /**
             * Set page data and trigger page:load event for ads, analytics, etc.
             */
            setPage: function() {
                // Temporary.
                var sectionPath = this.sectionPath,
                    pageInfoObj = App.getPageInfo(this.$el),
                    templatetype =this.$el.attr('data-template'),
                    page;
                App.set('pagename', sectionPath);
                var ssts = this.path;
                if (this.path.indexOf('/') == this.path.length - 1) {
                    ssts = this.path.substring(0, this.path.length - 1);
                }
                App.set('ssts', ssts);
                if(pageInfoObj){
                    App.set('aws', pageInfoObj.aws);
                }
                App.set('url', 'http://www.usatoday.com/' + Backbone.history.fragment);
                App.set('contenttype', sectionPath);
                App.set('contentParentId', 'card');
                App.set('seotitle', sectionPath);
                App.set('templatetype', templatetype);

                page = {
                    pagename: App.get('pagename'),
                    ssts: App.get('ssts'),
                    aws: App.get('aws'),
                    url: App.get('url'),
                    contenttype: App.get('contenttype'),
                    contentParentId: App.get('contentParentId'),
                    sectionPath: sectionPath,
                    seotitle: App.get('seotitle'),
                    templatetype: App.get('templatetype'),
                    subviews: this.subviews
                };
                // End temporary.

                PubSub.trigger('page:load', page);
            }


        });


        /**
         * Return view class.
         */
        return WeatherView;
    }
);
