/**
 * @fileoverview Tooltip view module.
 * @author kris.hedstrom@f-i.com (Kris Hedstrom)
 *         stephen.burgess@f-i.com (Stephen Burgess)
 */
 define(['jquery', 'underscore', 'backbone', 'pubsub'],
    function($, _, Backbone, PubSub) {

        /**
         * View class.
         */
        var TooltipView = Backbone.View.extend({

            // Events.
            events: {
                'mouseenter': 'show',
                'mouseleave': 'hide'
            },

            /**
             * Initialize view.
             */
            initialize: function(options) {

                this.options = $.extend({
                    template: '<div class="tooltip"><div class="tooltip-indicator"></div><div class="tooltip-inner"></div></div>',
                    animation: {
                        fadeIn: 0,
                        fadeOut: 200,
                        opacity: 0.9
                    },
                    position: 'top',
                    offset: {
                        x: 0,
                        y: 0
                    }
                }, options || {});

                this.el = $(this.options.el);
                _.each(this.el, function(child) {
                    $(child).data('title', $(child).attr('title')).removeAttr('title');
                });

                this.tip().addClass(this.options.position);
            },


            /**
             * Clean up view.
             * Removes event handlers and element (optionally).
             * @param {boolean} removeEl option to also remove View from DOM.
             */
            destroy: function(removeEl) {
                this.undelegateEvents();
                this.tip().remove();
                if (removeEl) {this.tip().remove();}
            },

            /**
             * Create or return an existing tooltip.
             */
            tip: function() {
                return this.tooltip = this.tooltip || $(this.options.template);
            },

            /**
             * Display the tooltip.
             */
            show: function(e) {

                var $target = $(e.currentTarget);
                $(document.body).append(this.tip());

                this.tip().children('.tooltip-inner').html($target.data('title'));
                this.tip().addClass('active');

                switch(this.options.position) {

                    case "left" :
                        var px = $target.offset().left - this.tip().outerWidth();
                        var py = $target.offset().top + (($target.outerHeight() - this.tip().outerHeight()) * 0.5);
                        break;

                    case "bottom" :
                        var px = $target.offset().left + (($target.outerWidth() - this.tip().outerWidth()) * 0.5);
                        var py = $target.offset().top + this.tip().outerHeight();
                        break;

                    case "right" :
                        var px = $target.offset().left + $target.outerWidth();
                        var py = $target.offset().top + (($target.outerHeight() - this.tip().outerHeight()) * 0.5);
                        break;

                    case "top" :
                        var px = $target.offset().left + (($target.outerWidth() - this.tip().outerWidth()) * 0.5);
                        var py = $target.offset().top - this.tip().outerHeight();
                        break;
                }

                py += this.options.offset.x;
                py += this.options.offset.y;

                this.tip().css({
                    left: px+'px',
                    top: py+'px'
                });

                this.tip().stop(true, false).fadeTo(this.options.animation.fadeIn, this.options.animation.opacity, function() {
                    // Animation complete.
                });
            },

            /**
             * Hide the tooltip.
             */
             hide: function() {
                this.tip().removeClass('active');

                this.tip().stop(true, false).fadeTo(this.options.animation.fadeOut, 0, function() {
                    // Animation complete.
                });
             },


        });


        /**
         * Return view class.
         */
        return TooltipView;
    }
);
