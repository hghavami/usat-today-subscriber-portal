/**
 * @fileoverview Full Screen module view.
 * @author Chris Manning
 */
define(['jquery', 'underscore', 'backbone', 'app', 'pubsub', 'state',
    'views/modules/gallery-thumb-scroll', 'views/modules/horizontal-scroll'],
       function($, _, Backbone, App, PubSub, StateManager, GalleryThumbScroll) {

    /**
     * View class.
     */
    var FullScreenView = Backbone.View.extend({

        // View element.
        el: '.gallery.fullscreen',

        // Events.
        events: {
            'click .close': 'close'
        },

        /**
         * Initialize view.
         * @param {Object} options View options passed in during init.
         */
        initialize: function(options) {
            this.asset = true;
            if ($('article.asset').length === 0) {
                this.asset = false;
            }
            this.win = App.get('win');
            this.scrollEl = App.get('scrollEl');
            this.carousel = options.carousel;
            this.parent = options.parent;
            this.setElement(this.parent);
            StateManager.registerFullScreenView(this);

            this.launch();
        },

        /**
         * Close Full Screen Experience.
         * @param {Event} e Browser event.
         */
        close: function(e) {

            // Cache the fullscreen's last slide position
            var updater = this.cacheSlide();

            this.destroy(true);
            if (this.asset) {
                $('article.asset').removeClass('fixed');
            } else {
                $('#cards').css('margin-top', '0').removeClass('fixed');
                this.scrollEl.scrollTop(this.scrollTop);
            }

            // Cache the selectors.
            var $thumbs = $('.mod.galleries .thumbs li', this.parent.el),
                $slides = $('.mod.galleries .slide', this.parent.el);

            // Remove the active class from all elements.
            $thumbs.removeClass('active');
            $slides.removeClass('active');

            // Add the active class to the currently selected
            // jQuery is forcing the double $
            $($thumbs[updater]).addClass('active');
            $($slides[updater]).addClass('active');

            this.toggleVisible();
            StateManager.clearFullScreenView();

            $('.film.fullscreen').remove();

            return false;

        },

        /**
         * Store the active slide from a given state (usually full screen) *
         */
        cacheSlide: function(e) {
            // Cache the selectors.
            var $active = this.$('.slide.active');

            // Insert return the slide to the close function before the destroy event.
            return $active.index();
        },

        /**
         * Clean up view.
         * Removes event handlers and element (optionally).
         * @param {boolean} removeEl option to also remove View from DOM.
         */
        destroy: function(removeEl) {
            $(document).off("." + this.cid);
            $(window).off("." + this.cid);
            if (removeEl) {this.remove();}

            if (this.carousel) {
                // Reset the carousel element back to original.
                this.carousel.cacheSelectors(this.parent);
                this.carousel.stoptimer();
                this.carousel.delegateEvents();
            }
        },

        /**
         * Launch Full Screen Experience.
         */
        launch: function() {
            if (this.asset) {
                $(this.el).closest('article.asset').addClass('fixed');
            } else {
                this.setCardsFixed();
            }
            this.toggleVisible();

            var html = '<article class="gallery fullscreen">';
            html += '<a class="close" href="#"></a>';
            html += $(this.el).clone().wrap('<div>').parent().html();
            html += '</article><div class="film fullscreen"></div>';

            html = this.swapImages(html);
            $('body').append(html);

            // Manually set the fullscreen view element since it was just appended.
            this.setElement('.gallery.fullscreen');
            this.delegateEvents();

            // Initialize the horizontal scroll.
            this.hscrollbar = new GalleryThumbScroll({
                color: 'black',
                el: '.gallery.fullscreen .thumbs'
            });

            // Update the carousel element to use its events in full screen view.
            if (this.carousel) {
                this.carousel.cacheSelectors('.gallery.fullscreen .galleries');
                this.carousel.delegateEvents();
            }
            this.namespace = this.cid;
            this.setHandlers();

            return false;
        },

        /**
         * Update gallery thumbnail positions.
         * @param {Event} event Browser event.
         */
        onUpdated: function(event) {
            event.stopPropagation();
            this.hscrollbar.updateThumbsPosition();
        },

        /**
         * Fix the background cards to prevent scrolling.
         * @param {Element} cards Cards wrap to fix. Sometimes we need to
         *     pass a reference to this if the cards have not been attached
         *     to the DOM yet.
         */
        setCardsFixed: function(cards) {
            this.scrollTop = cards ? '80' : this.win.scrollTop();
            cards = cards || $('#cards');
            cards.addClass('fixed');
            cards.css('margin-top', '-' + this.scrollTop + 'px');
            this.scrollEl.scrollTop(0);
        },

        /**
         * Set up handlers beyond this.el scope.
         */
        setHandlers: function() {
            var _this = this;
            $(document).on('keydown.' + this.cid, function(e) {
                if (e.keyCode == 37) {
                    _this.$el.find('.prev').trigger('click');
                    return false;
                }
                if (e.keyCode == 39) {
                    _this.$el.find('.next').trigger('click');
                    return false;
                }
                if (e.keyCode == 27) {
                    _this.close();
                    return false;
                }
            });
            $(window).on('resize.' + this.cid, $.proxy(this.hscrollbar.refresh, this.hscrollbar));
            $(this.el).bind('updateScroller', $.proxy(this.onUpdated, this));
        },

        /**
         * Use large image paths for fullscreen gallery.
         * @param {String} txt Gallery markup to modify
         */
        swapImages: function(txt) {
            var html = $(txt);
            html.find('img').each(function() {
                var t = $(this);
                if (t.attr('data-src-large')) {
                    var lg = t.attr('data-src-large');
                    t.attr('src', lg);
                }
            });
            return html;
        },

        /**
         * Show/hide certain items for fullscreen presentation.
         */
        toggleVisible: function() {
            $('body > .partner, #breaking, footer').toggle();
        }

    });


    /**
     * Return view class.
     */
    return FullScreenView;
});