/**
 * @fileoverview HorizontalScroll view module.
 * 
 * When instantiating the class pass and object that defines:
 * el: The container of the scrollable content and where the scrollbar will be placed
 * contentClass: The class name that the scrollable content div has (this class must sit
 * within the container)
 * template: markup for the actual scroller
 * padding: left/right padding amount for the scroller
 * To use outside gallery thumbs set an alternate el and contentClass when instantiating
 * the class. Any issues / questions don't hesitate to contact me.
 *
 * SEE BASE-SCROLL.JS FOR TROUBLESHOOTING ADVICE
 * 
 * @author stephen.burgess@f-i.com (Stephen Burgess)
 */
 define([
    'jquery', 
    'underscore', 
    'views/modules/base-scroll'
],
function(
    $, 
    _, 
    BaseScroll
)
    {

        /**
         * View class.
         */
        var HorizontalScrollView = BaseScroll.extend({

            /**
             * Initialize view.
             */
            initialize: function(options) {

                this.options = $.extend({
                    color: 'white', // white, black
                    contentClass: '.scrollable-content',
                    padding: 20
            	}, options || {});

                // Set up html template based on color.
                var compiled = _.template('<div class="horizontal-scroll <%= color %>"><div class="scrolldragger <%= color %>"><div class="over-icon <%= color %>"></div></div></div>');
                this.options.template = compiled({color: this.options.color});

                this.win = App.get('win');
                this.body = App.get('body');
                this.el = $(this.options.el);

                // Remove any pre-existing instances just in case.
                this.el.find('.horizontal-scroll').remove();

                BaseScroll.prototype.initialize.apply(this, arguments);
            },

            /**
             * Refresh / setup the scrollbar
             */
            refresh: function() {
                if(this.content.length === 0) {
                    //there is no content for the scroller return and do nothing
                    return;
                }

                var width = this.el.outerWidth() - (this.options.padding * 2);
                this.bar.css({width: width+'px', left: this.options.padding+'px'});

                var contentWidth = this.content.outerWidth();
                var percent = width / contentWidth;
                this.dragger.css({'width': (percent * width)+'px'});

                //store re-usable calculations
                this.overflow = contentWidth - this.el.width();
                this.draggerSpace = this.bar.width() - this.dragger.width();

                if(percent < 1) {
                    this.el.append(this.bar);
                    this.bindEvents();
                    this.active = true;
                }
                else {
                    this.bar.remove();
                    this.unbindEvents();
                    this.active = false;
                    this.currentPosition = 0;
                }

                this.position(this.currentPosition, true);
            },

            /**
             * Position content.
             */
            position: function(percent, positionDragger) {
                var x = -(this.overflow * percent);
                this.content.css({marginLeft: x+'px'});
                this.currentPosition = percent;

                if(positionDragger) {
                    var draggerX = this.draggerSpace * percent;
                    this.dragger.css({marginLeft: draggerX+'px'});
                }

                this.el.trigger('scrollBarUpdate');
            },

            /**
             * Bar clicked.
             */
            onBarClicked: function(event) {
                var barMouseX = event.pageX - this.bar.offset().left;
                var percent = barMouseX / this.bar.width();
                this.position(percent);

                //position dragger based on where bar was clicked
                var pos = this.draggerSpace * percent;
                this.dragger.css({marginLeft: pos+'px'});

                this.addActiveClass();
            },

            /**
             * Mouse move.
             */
            onMouseMove: function(event) {
                if(!this.isMouseDown) return;

                //calculate the dragger position
                var barMouseX = event.pageX - this.bar.offset().left;
                var draggerX = barMouseX - this.draggerOffset;
                draggerX = Math.max(0, Math.min(draggerX, this.draggerSpace));
                this.dragger.css({marginLeft: draggerX+'px'});

                //calculate % to position content
                var percent = draggerX / this.draggerSpace;
                this.position(percent);
            },

            /**
             * Mouse down.
             */
            onMouseDown: function(event) {
                //call onMouseDown on super
                BaseScroll.prototype.onMouseDown.apply(this, arguments);

                //calculate the offset from where the dragger was clicked
                var barMouseX = event.pageX - this.bar.offset().left;
                var draggerX = parseInt(this.dragger.css('marginLeft'));
                this.draggerOffset = barMouseX - draggerX;

                this.bar.addClass('active');
            },

            /**
             * Mouse wheel scroll.
             */
            onMouseWheel: function(event, delta) {
                if(this.isMouseDown) return;

                var deltaX = event.originalEvent.wheelDeltaX * 0.1;
                //Firefox doesn't support wheelDeltaX so we'll have to use detail
                if(isNaN(deltaX)) deltaX = event.originalEvent.detail * -1;
                //IE doesn't support wheelDeltaX and returns 0 for detail, it does support wheelDelta though
                if(isNaN(deltaX) || deltaX === 0) deltaX = event.originalEvent.wheelDelta * 0.1;

                deltaX = deltaX < 0 ? Math.floor(deltaX) : Math.ceil(deltaX);
                var draggerX = parseInt(this.dragger.css('marginLeft'));
                draggerX -= deltaX;
                draggerX = Math.max(0, Math.min(draggerX, this.draggerSpace));
                this.dragger.css({marginLeft: draggerX+'px'});

                //calculate % to position content
                var percent = draggerX / this.draggerSpace;
                this.position(percent);

                if(percent > 0 && percent < 1 && deltaX != 0) {
                    //don't bubble event if scrolling content
                    event.preventDefault();
                    this.addActiveClass();
                }
            }

        });


        /**
         * Return view class.
         */
        return HorizontalScrollView;
    }
);
