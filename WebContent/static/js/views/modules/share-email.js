/**
 * @fileoverview Share on Facebook view.
 * @author Jonathan Hensley <jhensley@gannett.com>
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'pubsub'
],
function(
    $,
    _,
    Backbone,
    PubSub
)
    {

        /**
         * View class.
         */
        var ShareViaEmailView = Backbone.View.extend({

            el: '.utility-modal.share-story .share.email',

            // Events.
            events: {
                'submit form': 'post',
                'click .social-links a': 'switchServices'
            },


            /**
             * Post the email message
             */
            post: function(e){

                var form_url = $('#share-email-form').attr('action');

                $(e.currentTarget).ajaxSend(function(event, xhr, settings) {
                    function getCookie(name) {
                        var cookieValue = null,
                            cookies,
                            i,
                            cookie;
                        if (document.cookie && document.cookie != '') {
                            cookies = document.cookie.split(';');
                            for (i = 0; i < cookies.length; i++) {
                                cookie = jQuery.trim(cookies[i]);
                                // Does this cookie string begin with the name we want?
                                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                                    break;
                                }
                            }
                        }
                        return cookieValue;
                    }
                    if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                        // Only send the token to relative URLs i.e. locally.
                        xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                    }
                });

                $.ajax({
                    url: form_url,
                    type: 'POST',
                    data: {
                        'To' : $('#share-email-form #to').val(),
                        'From' : $('#share-email-form #from').val(),
                        'Message' : $('#share-email-form #message').val()
                    },
                    success: function(data){
                        alert(data);
                    }
                });
                PubSub.trigger('uotrack', 'textsharingemail');
                return false;
            },


            /**
             * Switch to a different sharing mechanism
             * Param {object} e is the event that triggered the function
             */
            switchServices: function(e){
                var service = $(e.currentTarget).attr('class');

                switch (service){
                    case 'fb':
                        $(this.el).hide();
                        $('.share.facebook').fadeIn();
                        break;
                }

                return false;
            },


            /**
             * Clean up view.
             * Removes event handlers and element (optionally).
             * @param {boolean} removeEl option to also remove View from DOM.
             */
            destroy: function(removeEl) {
                this.undelegateEvents();
                if (removeEl) {this.remove();}
            }

        });

        /**
         * Return view class.
         */
        return ShareViaEmailView;
    }
);
