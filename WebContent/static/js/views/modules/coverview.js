/**
 * @fileoverview Cover View.
 * @author Chris Manning
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'pubsub',
    'app',
    'baseview'
],
function(
    $,
    _,
    Backbone,
    PubSub,
    App,
    BaseView
)
    {
        /**
         * View class.
         */
        var CoverView = BaseView.extend({

            el: '#cover-view',

            events: {
                'click .prev': 'prevSlide',
                'click .next': 'nextSlide',
                'click .front': 'nextSlide',
                'click .back': 'prevSlide'
            },

            initialize: function(options){

                this.$body = App.get('body');
                this.$top = App.get('scrollEl');
                this.$doc = App.get('doc');
                this.$window = App.get('win');

                // var throttledResize = _.throttle(_.bind(this.handleResizeWindow, this), 50);
                // this.$window.on('resize.' + this.cid, throttledResize);
                // this.handleResizeWindow();

                // call base class initialize
                BaseView.prototype.initialize.call(this, options);
            },

            buildSlides: function() {

            },

            goToSlide: function(index) {
            },

            prevSlide: function(e) {
                if (e) e.preventDefault();

                // First page check.
                var $targetPage = this.$activePage.prev('.page');
                if ($targetPage.hasClass('first')) return;
                if ($targetPage.prev('.page').hasClass('first')) {
                    this.$prev.hide();
                } else {
                    this.$next.show();
                }

                // Busy check.
                if (this.busy) return;
                this.busy = true;

                // Z-index update.
                var $zIndexTargetPage = this.$activePage.next('.page');
                var activePageZIndex = $zIndexTargetPage.length ? parseFloat(this.$activePage.next('.page').css('z-index')) + 1 : 1;
                this.$activePage.css('z-index', activePageZIndex);
                
                // End transition handler.
                $targetPage.on(this.transitionEndName, 
                    _.bind(function(e){
                        $(e.currentTarget).off(this.transitionEndName);
                        this.busy = false;
                    }, this)
                );

                // Start transition.
                this.$activePage.removeClass('active');
                this.$activePage = $targetPage.removeClass('flipped').addClass('active');
            },

            nextSlide: function(e) {
                if (e) e.preventDefault();

                // Last page check.
                var $targetPage = this.$activePage.next('.page');
                if (!$targetPage.length) return;
                if (!$targetPage.next('.page').length) {
                    this.$next.hide();
                } else {
                    this.$prev.show();
                }

                // Busy check.
                if (this.busy) return;
                this.busy = true;
                
                // Z-index calculation.
                var targetPageZIndex = parseFloat(this.$activePage.css('z-index')) + 1;

                // End transition handler.
                this.$activePage.on(this.transitionEndName, 
                    _.bind(function(e){
                        $targetPage.css('z-index', targetPageZIndex);
                        $(e.currentTarget).off(this.transitionEndName);
                        this.busy = false;
                    }, this)
                );

                // Start transition.
                this.$activePage.addClass('flipped').removeClass('active');
                this.$activePage = $targetPage.addClass('active');
            },

            keyboardShortcuts: function(e) {
                if (e.which === 37) {
                    this.prevSlide(e);
                } else if (e.which === 39) {
                    this.nextSlide();
                }
            },

            afterReveal: function(fromPath, toPath, paused) {
                _.bindAll(this, 'keyboardShortcuts');
                this.setElement('#cover-view');
                this.$prev = this.$el.find('.prev');
                this.$next = this.$el.find('.next');
                this.$activePage = this.$('.page.active');
                this.buildSlides();
                this.$doc.on('keyup', this.keyboardShortcuts);
            },

            handleResizeWindow: function() {

            },

            destroy: function() {
                this.$doc.off('keyup', this.keyboardShortcuts);
                BaseView.prototype.destroy.call(this);
            }

        });

        /**
         * Return view class.
         */
        return CoverView;
    }
);