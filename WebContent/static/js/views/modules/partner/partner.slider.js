/**
 * @fileoverview Pushdown Ad 
 * @author Chad Shryock <dshryock@gannett.com>
 */

 define(['jquery', 'underscore', 'backbone', 'pubsub'],
    function($, _, Backbone, PubSub) {

        /**
         * View class.
         */
        var SliderView = Backbone.View.extend({

            /**
             * Events
             */
            events: {
                'click a.close-partner' : 'onClickCloseButton'
            },

            refreshTime : 60000,
            refreshTimer : null,

            /**
             * Clean up the view (no argument).
             */
            destroy: function(removeEl) {
                this.onClickCloseButton();
                this.undelegateEvents();
                PubSub.unattach(this.pubSub, this);
                if (this.refreshTimer != null) {
                    clearTimeout(this.refreshTimer);
                }
            },

            /**
             * Initialize view.
             */
            initialize: function(options) {
                this.options = $.extend({}, options || {});
                this.el = this.options.el;
                this.$adContainer = $('.partner-bundle', this.$el);
                this.$el.fadeIn(500, 'easeInOutCubic', $.proxy(this.onShown, this.$el));
                this.pubSub = {
                    'breakingbar:before:close': this.shiftTop,
                    'breakingbar:before:open': this.shiftTop
                };
                PubSub.attach(this.pubSub, this);
                this.setupTimer();
            },

            onClickCloseButton: function() {
                this.$el.fadeOut(500, 'easeInOutCubic', $.proxy(this.onHidden, this.$el));
                _.delay(_.bind(function() {
                    this.$adContainer.empty();
                }, this), 200);
                return false;
            },

            onHidden: function() {
                this.removeClass('show').removeAttr('style');
            },

            onShown: function() {
                this.addClass('show').removeAttr('style');
            },

            refreshAd : function() {
                this.options.adModel.refreshAd(this.options.adSlot);
                clearTimeout(this.refreshTimer);
                this.setupTimer();
            },

            setupTimer: function() {
                this.refreshTime = site_config.ADS.SLIDER.refreshTime;
                this.refreshTimer = setTimeout(_.bind(function() {
                    this.refreshAd();
                }, this), this.refreshTime);
            },

            /**
             * Shift top of ad unit -- depending on nav height.
             * @param {number} top Top value to set/animate to (usually header outerheight).
             * @param {number=} slideSpeed Speed of slide animation.
             */
            shiftTop: function(top, slideSpeed) {
                this.$el.stop().animate({'top': top}, slideSpeed);
            }

        });

        /**
         * Return view class.
         */
        return SliderView;
    }
);
