/**
 * @fileoverview Pushdown Ad 
 * @author Chad Shryock <dshryock@gannett.com>
 */

 define([
    'jquery', 
    'underscore', 
    'backbone', 
    'app',
    'pubsub',
    'fwinfo'
],
function(
    $, 
    _, 
    Backbone, 
    App,
    PubSub,
    FwInfo
) {

        /**
         * View class.
         */
        var PushdownView = Backbone.View.extend({

            isTakeover: false,

            /**
             * Events
             */
            events: {
                'click a.close-partner' : 'onClickCloseButton',
                'click .film' : 'onClickCloseButton'
            },

            /**
             * Partner Info object used to pass around in events
             */
            partnerInfo : {
                '$placeholder' : null,
                'adType' : 'elastic',
                'codeIdentifier' : null
            },

            /**
             * Clean up the view (no argument).
             */
            destroy: function(removeEl) {
                PubSub.trigger('partner:' + this.partnerInfo.codeIdentifier + ':destroy');
                this.teardown();
                PubSub.unattach(this.pubSub, this);
                this.undelegateEvents();
                
                this.$el.remove();
            },

            findSizeForAd: function() {
                // get visible area height and width
                var wh = document.documentElement.clientHeight - App.get('headerEl').outerHeight();
                var ww = document.documentElement.clientWidth;

                // check size
                if (ww > 1080 && wh > 810) {
                    return 'size-xl';
                } else if (ww > 936 && wh > 700) {
                    return 'size-l';
                } else if (ww > 768 && wh > 576) {
                    return 'size-m';
                }
                // default to small
                return 'size-s';
            },

            /**
             * Initialize view.
             */
            initialize: function(options) {
                this.options = $.extend({}, options || {});

                this.el = this.options.el;

                this.partnerInfo.codeIdentifier = this.options.codeIdentifier;
                this.$placeholder = $('.partner-placeholder', this.$el);
                this.partnerInfo.codeIdentifier = this.options.codeIdentifier;
                this.partnerInfo.$placehoder = this.$el;

                this.$leaveBehind = $('article.cards .card-wrap .card[data-section-id="' + this.options.page.sectionPath + '"]').siblings('.partner-leavebehind');

                if (this.options.page.contentParentId == 'card') {
                    this.isTakeover = true;
                }

                // Setup the ad
                this.setup(this.options);

                // Listen for the events
                this.listen();
            },

            listen: function() {
                this.pubSub = {};
                this.pubSub['card:position:leftMargin'] = this.onRecenterLeaveBehind;
                this.pubSub['resize:app'] = this.onResize;
                this.pubSub['breakingbar:after:close'] = this.onResize;
                this.pubSub['breakingbar:after:open'] = this.onResize;
                this.pubSub['partner:' + this.options.codeIdentifier + ':ready'] = this.onPartnerReady;
                this.pubSub['partner:' + this.options.codeIdentifier + ':close'] = this.onPartnerClose;
                this.pubSub['partner:' + this.options.codeIdentifier + ':finish'] = this.onPartnerFinish;
                this.pubSub['open:overlay'] = this.onPartnerClose;
                PubSub.attach(this.pubSub, this);
            },

            onClickLeavebehind: function(ev) {
                if (!this.$leaveBehind.hasClass('show')) {
                    return;
                }
                this.$leaveBehind.removeClass('show');
                this.onPartnerReady({'adType':'pushdown', 'isNoDelay':true, 'resume': true});
            },

            onClickCloseButton: function(ev) {
                PubSub.trigger('partner:' + this.partnerInfo.codeIdentifier + ':stopToClose', this.partnerInfo);
                this.onPartnerClose();
                return false;
            },

            onPartnerClose: function() {
                // show leavebehind
                if (this.isTakeover) {
                    if (Modernizr.csstransitions) {
                        this.$leaveBehind.addClass('show');    
                    } else {
                        setTimeout(_.bind(function() {
                            this.$leaveBehind.animate({
                                'opacity' : 1
                            }, 200);
                        }, this), 200);
                    }
                    this.$el.fadeOut(500);
                } else {
                    this.$el.fadeOut(500, _.bind(function() {
                        this.destroy();
                    }, this));
                }

            },

            onPartnerFinish: function() {
                // elastic ignores - no auto close
            },

            onPartnerReady: function(params) {
                this.$el.fadeIn(500, _.bind(function() {
                    // Play
                    PubSub.trigger('partner:' + this.partnerInfo.codeIdentifier + ':play');

                    // track
                    this.track();
                }, this));

            },

            onRecenterLeaveBehind: function(left) {
                this.$leaveBehind.animate({
                    'left': left + 'px'
                }, 200);
            },

            onResize: function() {
                // determine size
                this.$placeholder.removeClass('size-s').removeClass('size-m').removeClass('size-l').removeClass('size-xl').addClass(this.findSizeForAd());
            },

            setup: function(obj) {
                // Grab codeIdentifier
                this.partnerInfo.codeIdentifier = obj.codeIdentifier;

                // leavebehind
                if (this.isTakeover) {
                    this.setupLeavebehind(obj.leaveBehindImageUrl, obj.leaveBehindImageAltTxt);
                }

                // determine size
                this.onResize();

                // creative
                this.setupCreative(obj.creative);

                // handle css
                this.setupCSS(obj.stylesheet);

                // handle scripts
                this.setupScripts(obj.creativeScripts);
            },

            setupCreative: function(creativeHtml) {
                if (creativeHtml != null && creativeHtml != '') {
                    var variables = {
                        adType : 'elastic',
                        codeIdentifier : this.partnerInfo.codeIdentifier,
                        id : 'partner_html_' + this.partnerInfo.codeIdentifier,
                        html : creativeHtml
                    }
                    var template = _.template($("#partner_container_template").html(), variables);
                    $(template).appendTo(this.$placeholder);
                }
            },

            setupCSS: function(stylesheet) {
                if (stylesheet != null && stylesheet != '') {
                    var link = document.createElement('link');
                    link.rel = 'stylesheet';
                    link.type = 'text/css';
                    link.href = stylesheet;
                    this.$el.append(link);
                }
            },

            setupLeavebehind: function(imageUrl, altText) {
                if (imageUrl != null && imageUrl != '') {
                    var variables = {
                        ad_type : 'elastic',
                        image_url : imageUrl,
                        image_alt : altText
                    };
                    var template = _.template($("#partner_leave_behind_template").html(), variables);
                    var $template = $(template).appendTo(this.$leaveBehind);
                    var me = this;
                    $template.bind('click',function(ev) {
                        me.onClickLeavebehind(ev);
                    });
                }

            },

            setupScripts: function(scripts) {
                for (var s = 1; s < 6; s++) {
                    if (scripts['script0' + s] == null || scripts['script0' + s] == '') {
                        continue;
                    }

                    var script = document.createElement('script');
                    script.src = scripts['script0' + s];
                    script.type = 'text/javascript';
                    script.defer = false;
                 
                    if (typeof callback != "undefined" && callback != null) {
                 
                        // IE only, connect to event, which fires when JavaScript is loaded
                        script.onreadystatechange = function() {
                            if (this.readyState == 'complete' || this.readyState == 'loaded') {
                                this.onreadystatechange = this.onload = null; // prevent duplicate calls
                            }
                        }
                 
                        // FireFox and others, connect to event, which fires when JavaScript is loaded
                        script.onload = function() {
                            this.onreadystatechange = this.onload = null; // prevent duplicate calls
                        };
                    }
                    
                    this.$el.append(script);
                }
            },

            teardown: function() {
                $('.leave-behind.elastic').remove();
            },

            track: function() {
                if (this.options.viewUrl == null) {
                    return;
                }
                var html = '<img src="' + this.options.viewUrl + 'http://assets-dev.usatoday.com/media/dfp/pixel-transparent.png" class="hide">';
                this.$el.append(html);
            }

        });

        /**
         * Return view class.
         */
        return PushdownView;
    }
);
