/**
 * @fileoverview Pushdown Ad 
 * @author Chad Shryock <dshryock@gannett.com>
 */

 define([
    'jquery', 
    'underscore', 
    'backbone', 
    'pubsub',
    'fwinfo'
],
function(
    $, 
    _, 
    Backbone, 
    PubSub,
    FwInfo
    ) {

        /**
         * View class.
         */
        var InlineElasticView = Backbone.View.extend({

            isReady: false,

            /**
             * Partner Info object used to pass around in events
             */
            partnerInfo : {
                '$placeholder' : null,
                'adType' : 'elastic',
                'codeIdentifier' : null
            },

            /**
             * Clean up the view (no argument).
             */
            destroy: function(removeEl) {
                PubSub.trigger('partner:' + this.partnerInfo.codeIdentifier + ':destroy');
                this.teardown();
                PubSub.unattach(this.pubSub, this)
                this.undelegateEvents();
            },

            /**
             * Initialize view.
             */
            initialize: function(options) {
                this.options = $.extend({}, options || {});

                this.el = this.options.el;

                this.partnerInfo.codeIdentifier = this.options.codeIdentifier;

                // Setup the ad
                this.setup(this.options);

                // Listen for the events
                this.listen();
            },

            listen: function() {
                this.pubSub = {};
                this.pubSub['partner:' + this.options.codeIdentifier + ':ready'] = this.onPartnerReady;
                this.pubSub['partner:' + this.options.codeIdentifier + ':show'] = this.onPartnerShow;
                this.pubSub['partner:' + this.options.codeIdentifier + ':close'] = this.onPartnerClose;
                this.pubSub['partner:' + this.options.codeIdentifier + ':finish'] = this.onPartnerFinish;
                PubSub.attach(this.pubSub, this);
            },

            onPartnerClose: function(arg) {
                PubSub.trigger('partner:' + this.partnerInfo.codeIdentifier + ':stopToClose');
                this.destroy();
                if (arg) {
                    arg();
                }
            },

            onPartnerFinish: function() {
                // elastic ignores - no auto close
            },

            onPartnerReady: function(params) {
                this.isReady = true;
            },

            onPartnerShow: function() {
                if (!this.isReady) {
                    // TODO what to do if the Ad isn't ready?
                    return;
                }

                // Play
                PubSub.trigger('partner:' + this.partnerInfo.codeIdentifier + ':play');

                // track
                this.track();
            },

            setup: function(obj) {
                // creative
                this.setupCreative(obj.creative);

                // handle css
                this.setupCSS(obj.stylesheet);

                // handle scripts
                this.setupScripts(obj.creativeScripts);
            },

            setupCreative: function(creativeHtml) {
                if (creativeHtml != null && creativeHtml != '') {
                    var variables = {
                        adType : 'elastic',
                        codeIdentifier : this.partnerInfo.codeIdentifier,
                        id : 'partner_html_' + this.partnerInfo.codeIdentifier,
                        html : creativeHtml
                    }
                    var template = _.template($("#partner_container_template").html(), variables);
                    var $template = $(template);
                    $('.close-partner', $template).remove();
                    var $t = $template.appendTo(this.$el);
                    $t.wrap('<div/>');
                }
            },

            setupCSS: function(stylesheet) {
                if (stylesheet != null && stylesheet != '') {
                    var link = document.createElement('link');
                    link.rel = 'stylesheet';
                    link.type = 'text/css';
                    link.href = stylesheet;
                    this.$el.append(link);
                }
            },

            setupScripts: function(scripts) {
                for (var s = 1; s < 6; s++) {
                    if (scripts['script0' + s] == null || scripts['script0' + s] == '') {
                        continue;
                    }

                    var script = document.createElement('script');
                    script.src = scripts['script0' + s];
                    script.type = 'text/javascript';
                    script.defer = false;
                 
                    if (typeof callback != "undefined" && callback != null) {
                 
                        // IE only, connect to event, which fires when JavaScript is loaded
                        script.onreadystatechange = function() {
                            if (this.readyState == 'complete' || this.readyState == 'loaded') {
                                this.onreadystatechange = this.onload = null; // prevent duplicate calls
                            }
                        }
                 
                        // FireFox and others, connect to event, which fires when JavaScript is loaded
                        script.onload = function() {
                            this.onreadystatechange = this.onload = null; // prevent duplicate calls
                        };
                    }
                    
                    this.$el.append(script);
                }
            },

            teardown: function() {
                this.$el.attr('data-is-active','false').removeAttr('data-code-id').removeAttr('id').empty();
            },

            track: function() {
                if (this.options.viewUrl == null) {
                    return;
                }
                // TODO fix this URL
                var html = '<img src="' + this.options.viewUrl + 'http://assets-dev.usatoday.com/media/dfp/pixel-transparent.png" class="hide">';
                this.$el.append(html);

                // if vCE - track it
                if (this.options.vceTrack) {
                    this.bannerview.trackVCE('1080x810', this, this.$el);
                }
            }

        });

        /**
         * Return view class.
         */
        return InlineElasticView;
    }
);
