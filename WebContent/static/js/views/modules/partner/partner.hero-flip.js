/**
 * @fileoverview Pushdown Ad 
 * @author Chad Shryock <dshryock@gannett.com>
 */

 define(['jquery', 'underscore', 'backbone', 'pubsub'],
    function($, _, Backbone, PubSub) {

        /**
         * View class.
         */
        var PushdownView = Backbone.View.extend({

            /**
             * Events
             */
            events: {
                'click a.close-partner' : 'onClickCloseButton'
            },

            /**
             * Partner Info object used to pass around in events
             */
            partnerInfo : {
                '$placeholder' : null,
                'adType' : 'heroflip',
                'codeIdentifier' : null
            },

            /**
             * Clean up the view (no argument).
             */
            destroy: function(removeEl) {
                PubSub.trigger('partner:' + this.partnerInfo.codeIdentifier + ':destroy');
                this.onPartnerClose();
                this.teardown();
                PubSub.unattach(this.pubSub, this);
                this.undelegateEvents();
                
                this.$el.empty();
            },

            /**
             * Initialize view.
             */
            initialize: function(options) {
                this.options = $.extend({}, options || {});

                this.el = this.options.el;
                this.$theatre = this.$el.parents('.theatre');
                var $card = this.$el.parents('.card-wrap');
                this.$leaveBehind = $('.partner-leavebehind', $card);

                this.partnerInfo.codeIdentifier = this.options.codeIdentifier;
                this.partnerInfo.$placehoder = this.$el;

                // Setup the ad
                this.setup(this.options);

                // Listen for the events
                this.listen();

                // track
                this.track();
            },

            listen: function() {
                PubSub.on('card:position:leftMargin', this.onRecenterLeaveBehind, this);
                PubSub.on('partner:' + this.options.codeIdentifier + ':ready', this.onPartnerReady, this);
                PubSub.on('partner:' + this.options.codeIdentifier + ':close', this.onPartnerClose, this);
                PubSub.on('partner:' + this.options.codeIdentifier + ':finish', this.onPartnerFinish, this);
                PubSub.on('open:overlay', this.onPartnerClose, this);
            },

            onClickLeavebehind: function(ev) {
                if (!this.$leaveBehind.hasClass('show')) {
                    return;
                }
                this.$leaveBehind.removeClass('show');
                this.onPartnerReady({'adType':'pushdown', 'isNoDelay':true, 'resume': true});
            },

            onClickCloseButton: function(ev) {
                PubSub.trigger('partner:' + this.partnerInfo.codeIdentifier + ':stopToClose', this.partnerInfo);
                this.onPartnerClose();
                return false;
            },

            onPartnerClose: function() {
                this.$theatre.removeClass('flip');
                PubSub.trigger('partner:heroflip:close');
                if (Modernizr.csstransitions) {
                    this.$leaveBehind.addClass('show');    
                } else {
                    var me = this;
                    setTimeout(_.bind(function() {
                        this.$leaveBehind.animate({
                            'opacity' : 1
                        }, 200);
                    }, this), 200);
                }

                setTimeout(function() {
                    PubSub.trigger('carousel:starttimer');
                }, 1000);
            },

            onPartnerFinish: function() {
                // hero flip ignores - no auto close
            },

            onPartnerReady: function(params) {
                var delay = 1000;
                if (params.isNoDelay) {
                    delay = 0;
                }
                PubSub.trigger('carousel:stoptimer');
                this.$theatre.addClass('flip');
                PubSub.trigger('partner:heroflip:open');
                _.delay(_.bind(function() {
                    PubSub.trigger('partner:' + this.partnerInfo.codeIdentifier + ':play');
                }, this), delay);
            },

            onRecenterLeaveBehind: function(left) {
                this.$leaveBehind.animate({
                    'left': left + 'px'
                }, 200);
            },

            setup: function(obj) {
                // Grab codeIdentifier
                this.partnerInfo.codeIdentifier = obj.codeIdentifier;

                // leavebehind
                this.setupLeavebehind(obj.leaveBehindImageUrl, obj.leaveBehindImageAltTxt);

                // creative
                this.setupCreative(obj.creative);

                // handle css
                this.setupCSS(obj.stylesheet);

                // handle scripts
                this.setupScripts(obj.creativeScripts);
            },

            setupCreative: function(creativeHtml) {
                if (creativeHtml != null && creativeHtml != '') {
                    var variables = {
                        adType : 'heroflip',
                        codeIdentifier : this.partnerInfo.codeIdentifier,
                        id : 'partner_html_' + this.partnerInfo.codeIdentifier,
                        html : creativeHtml
                    }
                    var template = _.template($("#partner_container_template").html(), variables);
                    $(template).appendTo(this.$el);
                }
            },

            setupCSS: function(stylesheet) {
                if (stylesheet != null && stylesheet != '') {
                    var link = document.createElement('link');
                    link.rel = 'stylesheet';
                    link.type = 'text/css';
                    link.href = stylesheet;
                    this.$el.append(link);
                }
            },

            setupLeavebehind: function(imageUrl, altText) {
                if (imageUrl != null && imageUrl != '') {
                    var variables = {
                        ad_type : 'heroflip',
                        image_url : imageUrl,
                        image_alt : altText
                    };
                    var template = _.template($("#partner_leave_behind_template").html(), variables);
                    var $template = $(template).appendTo(this.$leaveBehind);
                    var me = this;
                    $template.bind('click',function(ev) {
                        me.onClickLeavebehind(ev);
                    });
                }

            },

            setupScripts: function(scripts) {
                for (var s = 1; s < 6; s++) {
                    if (scripts['script0' + s] == null || scripts['script0' + s] == '') {
                        continue;
                    }

                    var script = document.createElement('script');
                    script.src = scripts['script0' + s];
                    script.type = 'text/javascript';
                    script.defer = false;
                 
                    if (typeof callback != "undefined" && callback != null) {
                 
                        // IE only, connect to event, which fires when JavaScript is loaded
                        script.onreadystatechange = function() {
                            if (this.readyState == 'complete' || this.readyState == 'loaded') {
                                this.onreadystatechange = this.onload = null; // prevent duplicate calls
                            }
                        }
                 
                        // FireFox and others, connect to event, which fires when JavaScript is loaded
                        script.onload = function() {
                            this.onreadystatechange = this.onload = null; // prevent duplicate calls
                        };
                    }

                    this.$el.append(script);
                }
            },

            teardown: function() {
                this.$leaveBehind.empty();
            },

            track: function() {
                if (this.options.viewUrl == null) {
                    return;
                }
                var html = '<img src="' + this.options.viewUrl + 'http://assets-dev.usatoday.com/media/dfp/pixel-transparent.png" class="hide">';
                this.$el.append(html);
            }

        });

        /**
         * Return view class.
         */
        return PushdownView;
    }
);
