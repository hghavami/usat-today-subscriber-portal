/**
 * @fileoverview Pushdown Ad 
 * @author Chad Shryock <dshryock@gannett.com>
 */

 define(['jquery', 'underscore', 'backbone', 'pubsub'],
    function($, _, Backbone, PubSub) {

        /**
         * View class.
         */
        var PushdownView = Backbone.View.extend({

            /**
             * Events
             */
            events: {
                'click a.close-partner' : 'onClickCloseButton'
            },

            /**
             * Partner Info object used to pass around in events
             */
            partnerInfo : {
                '$placeholder' : null,
                'adType' : 'pushdown',
                'codeIdentifier' : null
            },

            /**
             * Clean up the view (no argument).
             */
            destroy: function(removeEl) {
                PubSub.trigger('partner:' + this.partnerInfo.codeIdentifier + ':destroy');
                this.teardown();
                PubSub.unattach(this.pubSub, this);
                this.undelegateEvents();
                
                this.$el.empty();
            },

            /**
             * Initialize view.
             */
            initialize: function(options) {
                this.options = $.extend({}, options || {});

                this.el = this.options.el;

                this.partnerInfo.codeIdentifier = this.options.codeIdentifier;
                this.partnerInfo.$placehoder = this.$el;

                this.$cardWrap = this.$el.parents('.card-wrap');
                this.$leaveBehind = $('.partner-leavebehind', this.$cardWrap);

                // Setup the ad
                this.setup(this.options);

                // Listen for the events
                this.listen();

                // track
                this.track();
            },

            listen: function() {
                this.pubSub = {};
                this.pubSub['card:position:leftMargin'] = this.onRecenterLeaveBehind;
                this.pubSub['partner:' + this.options.codeIdentifier + ':ready'] = this.onPartnerReady;
                this.pubSub['partner:' + this.options.codeIdentifier + ':close'] = this.onPartnerClose;
                this.pubSub['partner:' + this.options.codeIdentifier + ':finish'] = this.onPartnerFinish;
                this.pubSub['open:overlay'] = this.onPartnerClose;
                PubSub.attach(this.pubSub, this);
            },

            onCardsPushdownComplete: function() {
                this.$el.addClass('open');
                PubSub.off('complete:adjsttopmargin:cards', this.onCardsPushdownComplete, this);
                PubSub.trigger('partner:' + this.partnerInfo.codeIdentifier + ':play');
            },

            onClickLeavebehind: function(ev) {
                if (!this.$leaveBehind.hasClass('show')) {
                    return;
                }
                this.$leaveBehind.removeClass('show');
                this.onPartnerReady({'adType':'pushdown', 'isNoDelay':true, 'resume': true});
            },

            onClickCloseButton: function(ev) {
                PubSub.trigger('partner:' + this.partnerInfo.codeIdentifier + ':stopToClose', this.partnerInfo);
                this.onPartnerClose();
                return false;
            },

            onPartnerClose: function() {
                if (Modernizr.csstransitions) {
                    this.$leaveBehind.addClass('show');    
                } else {
                    setTimeout(_.bind(function() {
                        this.$leaveBehind.animate({
                            'opacity' : 1
                        }, 200);
                    }, this), 200);
                }

                this.$el.removeClass('open');

                this.removeBackground();

                PubSub.trigger('pullup:cards');
            },

            onPartnerFinish: function() {
                // pushdown ignores
            },

            onPartnerReady: function(params) {
                var me = this;
                var delay = 1000;
                if (params.isNoDelay) {
                    delay = 0;
                }

                PubSub.on('complete:adjsttopmargin:cards', this.onCardsPushdownComplete, this);

                setTimeout(function() {
                    PubSub.trigger('pushdown:cards');
                    me.setupBackground();
                }, delay);
            },

            onRecenterLeaveBehind: function(left) {
                this.$leaveBehind.animate({
                    'left': left + 'px'
                }, 200);
            },

            onResize: function() {
                if (Modernizr.mq('(min-width: 1440px)') && this.options.backgroundImageLarge != null && this.options.backgroundImageLarge != '') {
                    this.$cardWrap.css('background-image', 'url(\'' + this.options.backgroundImageLarge + '\')');
                } else if (Modernizr.mq('(min-width: 1280px)') && this.options.backgroundImageMedium != null && this.options.backgroundImageMedium != '') {
                    this.$cardWrap.css('background-image', 'url(\'' + this.options.backgroundImageMedium + '\')');
                } else if (this.options.backgroundImageSmall != null && this.options.backgroundImageSmall != '') {
                    this.$cardWrap.css('background-image', 'url(\'' + this.options.backgroundImageSmall + '\')');
                } else {
                    this.$cardWrap.css('background-image', '');
                }
            },

            removeBackground: function() {
                $('body').css('background','').css('background-image','').css('background-color','');
                this.$cardWrap.css('background-image', '');
            },

            setup: function(obj) {
                // Grab codeIdentifier
                this.partnerInfo.codeIdentifier = obj.codeIdentifier;

                // leavebehind
                this.setupLeavebehind(obj.leaveBehindImageUrl, obj.leaveBehindImageAltTxt);

                // creative
                this.setupCreative(obj.creative);

                // handle css
                this.setupCSS(obj.stylesheet);

                // handle scripts
                this.setupScripts(obj.creativeScripts);
            },

            setupBackground: function() {
                if (this.options.solidBackgroundColor != 'transparent' && (this.options.repeatingBackground == null || this.options.repeatingBackground == '')) {
                    // background color, no repeating
                    $('body').css('background', this.options.solidBackgroundColor);

                } else if (this.options.solidBackgroundColor != 'transparent' && (this.options.repeatingBackground != null && this.options.repeatingBackground != '')) {
                    // background color and repeating image
                    $('body').css({
                        'background-color': this.options.solidBackgroundColor,
                        'background-image': 'url(\'' + this.options.repeatingBackground + '\')'
                    });

                } else if (this.options.solidBackgroundColor == 'transparent' && (this.options.repeatingBackground != null && this.options.repeatingBackground != '')) {
                    // no background-color, repeating image
                    $('body').css({
                        'background-color': 'transparent',
                        'background-image': 'url(\'' + this.options.repeatingBackground + '\')'
                    });
                }

                // background image
                this.onResize();
                PubSub.on('resize:app', this.onResize, this);

            },

            setupCreative: function(creativeHtml) {
                if (creativeHtml != null && creativeHtml != '') {
                    var variables = {
                        adType : 'pushdown',
                        codeIdentifier : this.partnerInfo.codeIdentifier,
                        id : 'partner_html_' + this.partnerInfo.codeIdentifier,
                        html : creativeHtml
                    }
                    var template = _.template($("#partner_container_template").html(), variables);
                    $(template).appendTo(this.$el);
                }
            },

            setupCSS: function(stylesheet) {
                if (stylesheet != null && stylesheet != '') {
                    var link = document.createElement('link');
                    link.rel = 'stylesheet';
                    link.type = 'text/css';
                    link.href = stylesheet;
                    this.$el.append(link);
                }
            },

            setupLeavebehind: function(imageUrl, altText) {
                if (imageUrl != null && imageUrl != '') {
                    var variables = {
                        ad_type : 'pushdown',
                        image_url : imageUrl,
                        image_alt : altText
                    };
                    var template = _.template($("#partner_leave_behind_template").html(), variables);
                    var $template = $(template).appendTo(this.$leaveBehind);
                    var me = this;
                    $template.bind('click',function(ev) {
                        me.onClickLeavebehind(ev);
                    });
                }

            },

            setupScripts: function(scripts) {
                for (var s = 1; s < 6; s++) {
                    if (scripts['script0' + s] == null || scripts['script0' + s] == '') {
                        continue;
                    }

                    var script = document.createElement('script');
                    script.src = scripts['script0' + s];
                    script.type = 'text/javascript';
                    script.defer = false;
                 
                    if (typeof callback != "undefined" && callback != null) {
                 
                        // IE only, connect to event, which fires when JavaScript is loaded
                        script.onreadystatechange = function() {
                            if (this.readyState == 'complete' || this.readyState == 'loaded') {
                                this.onreadystatechange = this.onload = null; // prevent duplicate calls
                            }
                        }
                 
                        // FireFox and others, connect to event, which fires when JavaScript is loaded
                        script.onload = function() {
                            this.onreadystatechange = this.onload = null; // prevent duplicate calls
                        };
                    }
                    
                    this.$el.append(script);
                }
            },

            teardown: function() {
                this.removeBackground();
                this.$leaveBehind.empty();
            },

            track: function() {
                if (this.options.viewUrl == null) {
                    return;
                }
                var html = '<img src="' + this.options.viewUrl + 'http://assets-dev.usatoday.com/media/dfp/pixel-transparent.png" class="hide">';
                this.$el.append(html);
            }

        });

        /**
         * Return view class.
         */
        return PushdownView;
    }
);
