/**
 * @fileoverview Asset Sponsorship Ad 
 * @author Chad Shryock <dshryock@gannett.com>
 */

 define([
    'jquery', 
    'underscore', 
    'backbone', 
    'pubsub'
],
function(
    $, 
    _, 
    Backbone, 
    PubSub
    ) {

        /**
         * View class.
         */
        var SponsorshipView = Backbone.View.extend({

            /**
             * Events
             */
            events: {
            },

            /**
             * Clean up the view (no argument).
             */
            destroy: function(removeEl) {
                this.teardown();
                this.undelegateEvents();
                // no el remove because it is the asset view, should be removed by parent view
            },

            /**
             * Initialize view.
             */
            initialize: function(options) {
                this.options = $.extend({}, options || {});

                this.el = this.options.el;

                this.setup();

            },

            setup: function() {
                // Check if needed
                if (this.options.page.contentParentId != 'asset') {
                    return;
                }

                this.setupBackground();
                this.setupLogo();
            },

            setupBackground: function() {
                if (this.options.skinLeft == null || this.options.skinRight == null) {
                    return;
                }

                var html = '<img src="<%= srcLeft %>" class="asset-sponsorship-skin asset-sponsorship-skin-left" /><img src="<%= srcRight %>" class="asset-sponsorship-skin asset-sponsorship-skin-right" />';

                var v = {
                    srcLeft : this.options.skinLeft,
                    srcRight : this.options.skinRight
                }
                var template = _.template(html, v);
                
                this.$el.append(template);
            },

            setupLogo: function() {
                // TODO once topic bar is completed
            },

            teardown: function() {
                $('.asset-sponsorship-skin', this.$el).remove();
            }

        });

        /**
         * Return view class.
         */
        return SponsorshipView;
    }
);
