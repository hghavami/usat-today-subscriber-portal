/**
 * @fileoverview Favorite  view.
 * @author Jonathan Hensley <jhensley@gannett.com>
 * modified by Paul Kane <pkane@gannett.com> 7/19/2012
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'pubsub',
    'fwinfo',
    'facebook'
],
function(
    $,
    _,
    Backbone,
    PubSub,
    FwInfo,
    Facebook
)
    {

        /**
         * View class.
         */
        var FavoriteView = Backbone.View.extend({

            el: '.dyn.favorite',

            // Events.
            events: {
                'click' : 'isFavorite' 
            },

            /**
             * Initialize view.
             */
            initialize: function() {
                // Grab the initial contents of the button - prepended with "Un-"
                // for alternate state

                this.$button = $(this.el).find('a');

                this.buttonContents = this.$button.html();

                this.assetId = $('.asset').attr('data-assetid');

                this.doFav = false;

                this.checkfav();
                       
                PubSub.on('user:authenticated', _.bind(function(loggedin){
                    if (loggedin) {
                        if (this.dofav == true){
                            this.fav();
                        } else {
                            this.checkfav();
                        }
                    }
                    else
                    {
                         this.toggleFavState('unfavorite');
                    }
                }, this));
                 $('.fb-sign-in .btn').unbind('click').bind('click', _.bind(function() {
                         this.connect();
                    }, this));
          
            },

            /**
             * Check to see if the current status is favorited or unfavorited to trigger the right
             * function - favorite vs. unfavorite
             * Param {object} is the event that generated this function call
             */
            isFavorite: function(e){
                var $button = $(e.currentTarget);
                if($button.hasClass('favorite')){
                    this.unfav();
                } else {
                    this.favorite();
                }
            },

            /**
             * Favorite the object to user account.
             */
            favorite: function() {

                var time = new Date().getTime();

                if (Facebook.status() == 'connected') {
                    this.getUserInfo();
                    var url = '/services/set/fav/' + this.assetId + '/' + this.userId + '/'+time;

                    $.ajax({
                        url: url,
                        success: _.bind(function(data){
                            if(data.IsArticlePinned == 1){
                                this.toggleFavState('favorite');
                            }
                        }, this)
                    });
                }
                else
                {
                    if($('.favorite').hasClass('saved')){
                        $('.favorite').removeClass('saved').html('<a href="#"><span>Favorite<span class="icon"></span></span></a>');
                    } else {
                        $('.favorite').addClass('saved').html('<a href="#"><span>Unfavorite<span class="icon"></span></span></a>');
                    }
                    $('.utility-modal.favorite-story').toggle(); 
                }

            },

            /**
             * Un-Favorite the object from user account.
             */
            unfav: function() {

                var self = this,
                    time = new Date().getTime(),
                    url;

                this.getUserInfo();

                if (Facebook.status() == 'connected') {
                    url = '/services/unset/fav/' + this.assetId + '/' + this.userId + '/'+time;

                    $.ajax({
                        url: url,
                        success: _.bind(function(data){
                            if(data.IsArticlePinned == 0){
                                self.toggleFavState('unfavorite');
                            }
                        }, this)
                    });
                }


            },

            /**
             * Perform a check to see if the current user has
             * already favorited the object
             */
            checkfav: function() {

                var self = this,
                    time = new Date().getTime(),
                    url;

                this.getUserInfo();

                if (Facebook.status() == 'connected') {
                    url = '/services/get/fav/' + this.assetId + '/' + this.userId + '/'+time;

                    $.ajax({
                        url: url,
                        success: _.bind(function(data){
                            if(data.IsArticlePinned == 1){
                                self.toggleFavState('favorite');
                            }
                        }, this)
                    });
                }

            },

            /**
             * Perform a check to see if the current user is logged
             * in, and what their user_id is
             */
            getUserInfo: function() {
                if (Facebook.status() == 'connected') {
                    this.userId = FwInfo.getuseruniqueid();
                } else {
                    return false;
                }
            },

            /**
             * Toggle favorite class
             * Cycles through .favorite or .unfavorite
             * Param {string} favStatus details which state to show, favorite vs. unfavorite
             */
            toggleFavState: function(favStatus) {
                if (favStatus == 'favorite') {
                    $('.unfavorite').removeClass('unfavorite').addClass('favorite');
                    this.$button.html('Un-'+this.buttonContents);
                    PubSub.trigger('uotrack', 'UtilityBarFav');
                } else {
                    $('.favorite').removeClass('favorite').addClass('unfavorite');
                    this.$button.html(this.buttonContents);
                    PubSub.trigger('uotrack', 'UtilityBarUnfav');
                }

            },

            /**
             * Clean up view.
             * Removes event handlers and element (optionally).
             * @param {boolean} removeEl option to also remove View from DOM.
             */
            destroy: function(removeEl) {
                this.undelegateEvents();
                if (removeEl) {this.remove();}
            },

    	    connect: function() {
              $('.utility-modal.fav-story').toggle(); 
    	    	Facebook.login();
    	    }

        });

        /**
         * Return view class.
         */
        return FavoriteView;
    }
);
