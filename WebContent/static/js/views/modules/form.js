/**
 * @fileoverview Generic forms view for common site-wide form element handling.
 * @author erik.kallevig@f-i.com (Erik Kallevig)
 */
define(['jquery', 'underscore', 'backbone', 'pubsub', 'app'],
    function($, _, Backbone, PubSub, App) {


        /**
         * View class.
         */
        var FormView = Backbone.View.extend({

            // Events.
            events: {
                'focus .search-input': 'onFocusBlurSearchField',
                'blur .search-input': 'onFocusBlurSearchField'
            },

            /**
             * Initialize the view.
             */
            initialize: function() {
            },

            /**
             * Focus/blur handler for search input field.
             * @param {Event} e Focus/blur event.
             */
            onFocusBlurSearchField: function(e) {
                var input = $(e.currentTarget);
                var defaultText = input.data('default-text');
                if (!defaultText) input.data('default-text', input.val());
                if (e.type === 'focusin' &&
                    input.val() === input.data('default-text')) {
                        input.val('');
                } else if (e.type === 'focusout' && input.val() === '') {
                    input.val(defaultText);
                }
            },

            /**
             * Clean up view.
             * Removes event handlers and element (optionally).
             * @param {boolean} removeEl option to also remove View from DOM.
             */
            destroy: function(removeEl) {
                this.undelegateEvents();
                if (removeEl) this.remove();
            }
        });

        /**
         * Return view class.
         */
        return FormView;
    }
);
