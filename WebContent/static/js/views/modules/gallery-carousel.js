/**
 * @fileoverview Gallery Carousel module view.
 * @author pkane, cwmanning
 */
define(['jquery', 'underscore', 'backbone', 'pubsub', 'app', 'views/modules/carousel',
    'views/modules/loader', 'state'],
    function($, _, Backbone, PubSub, App, Carousel, Loader, StateManager) {

        /**
         * View class.
         */
        var GalleryCarouselView = Backbone.View.extend({

            /**
             * Events
             */
            events: {
                'click #CList-galleries li' : 'select'
            },

            /**
             * Initialize view.
             * @param {Object} options View options passed in during init.
             */
            initialize: function(options) {
                this.el = options.el;
                this.subviews = [];
                this.$galleries = this.$el.find('.front-gallery');
                this.$navItems = this.$el.find('.galleries li');

                _.bindAll(this, 'switchGallery');

                this.loader = new Loader({
                        el: this.$galleries.parent(),
                        msg: 'Loading...'
                    });
            },

            /**
             * Clean up view.
             * Removes event handlers and element (optionally).
             * @param {boolean} removeEl option to also remove View from DOM.
             */
            destroy: function(removeEl) {
                this.undelegateEvents();
                _.each(this.subviews, function(view){
                    if (typeof view.destroy !== 'undefined') {
                        view.destroy();
                    }
                });
                this.loader.destroy(removeEl);
                if (removeEl) this.remove();
            },

            /**
             * Fetch data from server via AJAX. Takes a path to fetch and a
             * callback to parse the data and initialize views.
             * @param {String} path The path to the ajax endpoint.
             * @param {Number} index The Gallery list item position.
             * @param {Function} callback Function to call when data is returned
             *      from server.
             */
            fetchData: function(path, index, callback) {
                var _this = this;

                _this.loading(true);
                StateManager.fetchHtml(path, {data: {type: 'front'}})
                    .done(function(html){
                        callback(html, index);
                        _this.loading(false);
                    });
            },

            /**
             * Toggle loading presentation
             * @param {boolean} showLoading Whether or not to display loading.
             */
            loading: function(showLoading) {
                if (showLoading) {
                    this.loader.show();
                } else {
                    this.loader.hide();
                }
            },

            /**
             * Toggle selected class and load gallery data.
             * @param {Event} event View click event.
             */
            select: function(event) {
                var $item = $(event.currentTarget);
                var index = $item.index();
                var $gallery = this.$galleries.eq(index);


                var targetLink = $item.find('a');
                var path = targetLink.attr('href');

                //heattracking
                App.setHeatTrack(targetLink);

                if ($gallery.find('.galleries').length > 0) {
                    this.switchGallery(null, index);
                } else {
                    this.fetchData(path, index, this.switchGallery);
                }

                // Highlight clicked navigation item.
                this.$navItems.removeClass('selected');
                $item.addClass('selected');
                return false;
            },

            /**
             * Switch to selected gallery.
             * @param {String} html Returned from fetchData.
             * @param {Number} index The Gallery list item position.
             */
            switchGallery: function(html, index) {
                var $item = this.$navItems.eq(index);
                var $gallery = this.$galleries.eq(index);

                // Show chosen gallery.
                this.$galleries.removeClass('selected');
                $gallery.addClass('selected');

                if (html) {
                    $gallery.html(html);


                    var _this = this;
                    setTimeout(function() {
                        // Create a new carousel instance.
                        // Timeout allows for appended html to paint before
                        // creating scroll bars in carousel init.
                        _this.subviews['gallery' + index] = new Carousel({
                             ads: true,
                             el: $gallery.find('.mod.galleries'),
                             fullScreen: true
                        });
                    }, 100);
                }
            }

        });


        /**
         * Return view class.
         */
        return GalleryCarouselView;
    }
);
