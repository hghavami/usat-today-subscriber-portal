/**
 * @fileoverview Headlines module view.
 * @author Chad Shryock <cdshryock@gannett.com>
 */
define(['jquery', 'underscore', 'backbone', 'pubsub'],
    function($, _, Backbone, PubSub) {


    /**
     * View class.
     */
    var HeadlineView = Backbone.View.extend({

        // Events.
        events: {
            'click .button-add-content': 'showMore'
        },

        /**
         * Initialize view.
         * @param {Object} options View options passed in during init.
         */
        initialize: function(options) {

            this.$mod = $(options.el, document);
            this.itemsPerPage = this.$mod.data('itemsPerPage');
            this.$button = $('.button-add-content', this.$mod);
            this.section = options.section + 'front';
            _.bindAll(this, 'transitionMore');

        },

        /**
         * Clean up view.
         * Removes event handlers and element (optionally).
         * @param {boolean} removeEl option to also remove View from DOM.
         */
        destroy: function(removeEl) {
            this.undelegateEvents();
            if (removeEl) {this.remove();}
        },

        getMoreHeadlines: function(nextPageNum, callback) {
            var mod = this.$mod;
            $.ajax({
                url: nextPageNum + '/',
                data: {
                    ajax: true
                },
                success: function(htmlFrag) {
                    var $page = $(htmlFrag).find('.page').removeClass('active').addClass('inactive');
                    var $button = $(htmlFrag).find('.button-add-content');
                    mod.find('.collection').append($page);
                    if ($button.length > 0) {
                        mod.find('.show-more').html($button);
                    } else {
                        mod.find('.button-add-content').addClass('inactive');
                    }
                    callback($page);
                }
            });
        },

        /**
         * Shows the next block of headlines.
         * @param {Event} event View click event.
         */
        showMore: function(event) {
            if ($(event.target).hasClass('inactive')) return false;

            this.$button.addClass('loading').text(this.$button.attr('data-loading'));
            var $nextPage;

            if (this.$el.hasClass('stag')) {
                var nextPageNum = parseInt($(event.target).data('next'));
                this.getMoreHeadlines(nextPageNum, this.transitionMore);
            } else {
                // On a typical front - headlines already in DOM.
                $nextPage = $('.page.active:last', this.$mod).next();
                $('img', $nextPage).each(function() {
                    var $me = $(this);
                    $me.attr('src', $me.attr('data-src'));
                });
                this.transitionMore($nextPage);
            }

            return false;
        },

        transitionMore: function($nextPage) {
            if (Modernizr.csstransitions) {
                $nextPage.removeClass('inactive').addClass('active');
            } else {
                $nextPage.animate({
                    'max-height': '5000px'
                }, 200, function() {
                    $(this).removeClass('inactive').addClass('active').css('max-height', '');
                });
            }
            if ($('.page.inactive', this.$mod).length <= 0) {
                // no more items to show, inactivate the show more button
                this.$button.addClass('inactive');
            }
            this.$button.removeClass('loading').text(this.$button.attr('data-default'));
            PubSub.trigger('showmore:headlines');
        }

    });

    /**
     * Return view class.
     */
    return HeadlineView;
});