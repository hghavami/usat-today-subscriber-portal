/**
 * @fileoverview Single Video module view.
 * @author Chris Manning
 */
define(['jquery', 'underscore', 'backbone','pubsub'],
    function($, _, Backbone, PubSub) {


        /**
         * View class.
         */
        var VideoView = Backbone.View.extend({

            // Events.
            events: {
                'click .text, .videoStillPlay': 'swapImageForVideo',
                'click .videoCloseButton': 'hidePlayer'
            },

            /**
             * Fired on new VideoView
             */
            initialize: function(options) {
                if (!options || !options.el) {return;}
                this.el = options.el;

                this.scriptsLoaded = false;
                this.liveHidden = false;
                this.$videoObj = this.$el.find('.videoObject');
                _.bindAll(this, 'createPlayer');

                this.pubSub = {
                    'carousel:switchSlide': this.hidePlayer
                };
                PubSub.attach(this.pubSub, this);

                if (this.options.autostart || this.options.autocreate) {
                    this.createPlayer();
                }

                window.BCLoad = function(playerID){
                    //loading indicator for the scripts on our end
                    $('.temp-loader').fadeOut();
                } 
            },

            /**
             * Render Brightcove Player
             */
            createPlayer: function() {
                if (window.brightcove) {
                    console.log('creating player for ' + this.cid);

                    var brightcoveId = this.$el.find('.BrightcoveExperience').attr('id');
                    if ($('.open-live-feed').is(':visible') && $('#overlay').length === 0) {
                        $('.open-live-feed').hide();
                        this.liveHidden = true;
                    }

                    window.brightcove.createExperiences(null, brightcoveId);

                    this.$videoObj.show();

                    // Trigger the PubSub video:load event
                    PubSub.trigger('video:load', {
                        "videoname" : this.$videoObj.attr('data-videoname')
                    });
                } else {
                    this.loadScripts(this.createPlayer);
                }
            },

            /**
             * Clean up view.
             * Removes event handlers and element (optionally).
             * @param {boolean} removeEl option to also remove View from DOM.
             */
            destroy: function(removeEl) {
                this.undelegateEvents();
                PubSub.unattach(this.pubSub, this);
                // Let's cleanup that ugly Brightcove API callback
                delete window.BCLoad;
                if (removeEl) {this.remove();}
            },

            /**
             * Hide Brightcove Player
             */
            hidePlayer: function() {
                if (this.liveHidden) {
                    $('.open-live-feed').show();
                    liveHidden = false;
                }
                if (this.$videoObj) {
                    this.$videoObj.hide();
                }
            },

            /**
             * Load Brightcove Player script dependencies
             */
            loadScripts: function(callback) {
                require(['http://admin.brightcove.com/js/BrightcoveExperiences.js'], function() {
                    // Script creates window.brightcove object we need to create video flash players.
                    if (typeof window.brightcove !== 'undefined') {
                        callback();
                    } else {
                        console.log('Error loading Brightcove');
                    }
                });
            },

            /* Replace the video still with an actual video player
             * @param {Event} e Browser event
             */
            swapImageForVideo: function(e){
                if (this.$el.parents('.hero') && $(e.target).is("a")) {return;}

                this.createPlayer();
                
                //loading indicator for the scripts on our end
                $('.temp-loader').show();

                PubSub.trigger('video:playClick');
            }


        });


        /**
         * Return view class.
         */
        return VideoView;
    }
);
