/**
 * @fileoverview Global header (nav) module view.
 * @author erik.kallevig@f-i.com (Erik Kallevig)
 *         stephen.burgess@f-i.com (Stephen Burgess)
 */
define(['jquery', 'underscore', 'backbone', 'pubsub', 'app', 'state', 'facebook',
    'easing', 'animatecolors', 'cookie'],
    function($, _, Backbone, PubSub, App, StateManager, Facebook) {


        /**
         * View class.
         */
        var HeaderView = Backbone.View.extend({

            // View element.
            el: '#header',

            // Events.
            events: {
                'click #nav .search': 'onClickNavSearch',
                'click #breaking .close-btn': 'onClickCloseBreakingBtn',
                'hover #navbar .weather': 'onHoverWeatherNav',
                'hover #navbar .media': 'onHoverMediaNav',
                'click #navbar .weather .dropdown': 'onClickWeatherDropdown',
                'click #navbar .weather .settings-btn': 'openWeatherDropSettings',
                'click #navbar .weather .cancel-x': 'closeWeatherDropSettings',
                'click #navbar .weather .cancel-btn': 'closeWeatherDropSettings',
                'click #navbar .weather .submit': 'onClickWeatherSubmit',
                'focus #navbar .weather .query': 'onFocusWeatherQueryField',
                'blur #navbar .weather .query': 'onFocusWeatherQueryField',
                'submit #weather-dropdown-form': 'onSubmitWeatherForm',
                'hover #navbar .media': 'onHoverMediaNav',
                'hover #navbar .user': 'onHoverUserNav',
                'click #navbar .user .login': 'onClickLogin',
                'click #navbar .user .logout': 'onClickLogout'
            },

            /**
             * Initialize the view.
             */
            initialize: function() {

                // Cache references to common elements/calculations.
                this.win = App.get('win');
                this.body = App.get('body');
                this.scrollEl = App.get('scrollEl');
                this.fixedWrap = this.$('.fixed-wrap');
                this.header = this.body.find("#header");
                this.logo = this.fixedWrap.find('.logo');
                this.searchWrap = $('#search-form');
                this.nav = App.get('navEl');
                this.navSearchBtn = this.nav.find('.search');
                this.breaking = App.get('breakingEl');
                this.breakingWrap = App.get('breakingWrapEl');

                // Values.
                // TODO: May want to replace these with fetched values from DOM.
                this.navHeight = this.nav.outerHeight();
                this.mastheadHeight = 80;

                // Breaking news animate colors.
                this.breakingColors = {
                    breaking : {
                        normal : 'rgb(247,0,0)',
                        highlight : 'rgb(191,49,49)'
                    },
                    live : {
                        normal : 'rgb(0,170,255)',
                        highlight : 'rgb(0,145,218)'
                    },
                    developing : {
                        normal : 'rgb(0,170,255)',
                        highlight : 'rgb(0,145,218)'
                    }
                };

                // Keep 'this' reference to view in scroll.
                _.bindAll(this, 'onScrollWindow', 'updateHeaderMinHeight',
                    'openBreakingBar', 'onCompleteOpenBreakingBar',
                    'updateBreakingBar', 'parseBreakingNews',
                    'parseMediaDropdown', 'updateMediaDropdown',
                    'toggleMediaDropdown', 'toggleUserDropdown',
                    'parseWeatherDropdown', 'updateWeatherDropdown',
                    'toggleWeatherDropdown');

                // Scroll event flag.
                this.preventScrollEvent = false;

                // Initialize header states.
                this.isMastheadExpanded = this.isSearchResultsOpen = this.body.hasClass('show-search');

                // Show/hide masthead speeds.
                this.slideSpeed = 200;
                App.set('headerSlideSpeed', this.slideSpeed);
                this.slideEasing = 'easeOutCubic';

                // Publish events.
                PubSub.on('created:cards', this.shiftSideCards, this);
                PubSub.on('toggle:weatherdropdown', this.toggleWeatherDropdown, this);
                PubSub.on('toggle:mediadropdown', this.toggleMediaDropdown, this);
                PubSub.on('open:searchResults', this.openSearchResults, this);
                PubSub.on('close:searchResults', this.closeSearchResults, this);

                // Breaking news poll.
                this.breakingLastUpdateClosed =
                    parseFloat($.cookie('breakingLastUpdateClosed'));
                this.breakingLastUpdate = this.breaking.data('lastupdate');
                // TODO: Re-enable this check once we are loading breaking news
                // data with the first page load.
                // if (this.shouldOpenBreakingBar()) {

                // Disabling breaking news - uncomment to re-instantiate
                setTimeout(this.updateBreakingBar, 1000);

                // }

                // Breaking news poll
                this.breakingNewsPoll = setInterval(this.updateBreakingBar,
                91000);

                // Weather poll (every 5 minutes).
                setTimeout(this.updateWeatherDropdown, 1200);
                this.weatherDropPoll = setInterval(this.updateWeatherDropdown,
                300000);

                // Media poll (every 15 minutes).
                setTimeout(this.updateMediaDropdown, 1200);
                this.mediaDropPoll = setInterval(this.updateMediaDropdown,
                900000);
            },

            /**
             * Window scroll handler for header elements.
             * @param {Event} e Browser event triggering toggle.
             */
            onScrollWindow: function(e) {

                // Hide masthead when scrolling.
                if (!this.preventScrollEvent && !this.isSearchResultsOpen) {
                    this.collapseMasthead(true);
                }
            },

            /**
             * Click handler for nav weather settings button.
             * @param {Event} e Click event.
             */
            onClickWeatherDropdown: function(e) {
                e.stopPropagation();
            },

            /**
             * Hover handler for weather nav button.
             * @param {Event} e Hover event.
             */
            onHoverWeatherNav: function(e) {
                clearTimeout(this.weatherNavTimer);
                    this.weatherNavTimer = _.delay(this.toggleWeatherDropdown,
                        300, undefined, e.type === 'mouseenter');
            },

            /**
             * Hover handler for media nav button.
             * @param {Event} e Hover event.
             */
            onHoverMediaNav: function(e) {
                clearTimeout(this.mediaNavTimer);
                    this.mediaNavTimer = _.delay(this.toggleMediaDropdown,
                        300, undefined, e.type === 'mouseenter');
            },

            /**
             * Hover handler for user nav button.
             * @param {Event} e Hover event.
             */
            onHoverUserNav: function(e) {
                clearTimeout(this.userNavTimer);
                    this.userNavTimer = _.delay(this.toggleUserDropdown,
                        300, undefined, e.type === 'mouseenter');
            },

            /**
             * Focus handler for nav weather query field.
             * @param {Event} e Focus event.
             */
            onFocusWeatherQueryField: function(e) {
                var input = $(e.currentTarget);
                var defaultText = input.data('default-text');
                if (!defaultText) input.data('default-text', input.val());
                if (e.type === 'focusin' &&
                    input.val() === input.data('default-text')) {
                        input.val('');
                } else if (e.type === 'focusout' && input.val() === '') {
                    input.val(defaultText);
                }
            },

            /**
             * Click handler for weather dropdown form submit button.
             * @param {Event} e Click event.
             */
            onClickWeatherSubmit: function(e) {
                $(e.currentTarget).closest('form').submit();
            },

            /**
             * Submit handler for weather dropdown form.
             * @param {Event} e Submit event.
             */
            onSubmitWeatherForm: function(e) {
                e.preventDefault();
                var location = $(e.currentTarget).find('.query').val();
                $.cookie('weatherLocation', location);
                this.updateWeatherDropdown(location);
                this.closeWeatherDropSettings();
            },

            /**
             * Close user dropdown.
             * @param {boolean=} open Whether to force open the dropdown.
             */
            toggleUserDropdown: function(e, open) {
                if (this.preventScrollEvent) return;
                if (!this.userNavBtn) {
                    this.userNavBtn = this.nav.find('.user');
                }
                this.userNavBtn.toggleClass('active', open);
                if (this.userNavBtn.hasClass('active')) {
                    this.win.on('scroll', this.toggleUserDropdown);
                } else {
                    this.win.off('scroll', this.toggleUserDropdown);
                }
                return false;
            },

            /**
             * Close media dropdown.
             * @param {boolean=} open Whether to force open the dropdown.
             */
            toggleMediaDropdown: function(e, open) {
                if (this.preventScrollEvent) return;
                if (!this.mediaNavBtn) {
                    this.mediaNavBtn = this.nav.find('.media');
                }
                this.mediaNavBtn.toggleClass('active', open);
                if (this.mediaNavBtn.hasClass('active')) {
                    this.win.on('scroll', this.toggleMediaDropdown);
                } else {
                    this.win.off('scroll', this.toggleMediaDropdown);
                }
                return false;
            },

            /**
             * Close weather dropdown.
             * @param {boolean=} open Whether to force open the dropdown.
             */
            toggleWeatherDropdown: function(e, open) {
                if (this.preventScrollEvent) return;
                if (!this.weatherNavBtn) {
                    this.weatherNavBtn = this.nav.find('.weather');
                    this.weatherDropFront = this.weatherNavBtn.find('.front-side');
                    this.weatherDropBack = this.weatherNavBtn.find('.back-side');
                }
                this.weatherNavBtn.toggleClass('active', open);
                if (this.weatherNavBtn.hasClass('active')) {
                    this.nav.find('.query').blur();
                    this.win.on('scroll', this.toggleWeatherDropdown);
                } else {
                    this.win.off('scroll', this.toggleWeatherDropdown);
                }
                this.closeWeatherDropSettings();
                return false;
            },

            /**
             * Open weather settings view.
             */
            openWeatherDropSettings: function() {
                this.weatherDropFront.animate({
                    'left': -240
                }, 200, this.slideEasing);
                this.weatherDropBack.animate({
                    'left': 0
                }, 200, this.slideEasing);

                var _this = this;
                require(['libs/leaflet/jquery-ui-1.8.21.custom.min' ],function(){ 
                    $("#navbar li.weather").find( ".query").autocomplete( _this.weatherAutoCompleteSettings ); 
                });



                return false;
            },
 
              /// settings for weather auto complete
             weatherAutoCompleteSettings : {
                appendTo: ".weather #weather-dropdown-form",
                open: function(){
              
                    var frontSide =  $("li.weather .front-side"),
                        activeMenu = $(this),
                        frontSideHeight = 400,
                        oldHeight = ( !!frontSide.attr("data-height" ) )? frontSide.attr("data-height" ) : frontSide.height();

                    frontSide.attr("data-height", oldHeight ).height( frontSideHeight  );  
                    activeMenu.autocomplete('widget').css('position', "relative");
                    activeMenu.autocomplete('widget').find("li").css("float","none");
    

                    return false;
                },
                close : function(){
                    var heightReset = $("li.weather .front-side").attr("data-height" );
                    $("li.weather .front-side").height( heightReset ).removeAttr("data-height");
 
                },
                // Dynamic Source
                source :  function(request, response) {
                    $.ajax({
                        url: '/services/city/autocomplete/json/'+request.term,
                        dataType: 'json' ,
                        success: function(data) {
                            data = data.splice(0,5);   
                            
                            response( $.map( data, function( item ) {
                                return {
                                    label: item.City +  ", "+item.State,
                                    value: item.City+" "+item.State
                                }
                            }));
                        }
                    });
                },
                minLength: 3,
                select: function( event, ui ) {
                    this.value =  ui.item.value;
                    $("#navbar .weather .submit").trigger("click");
                } 
             },
 
            /**
             * Close weather settings view.
             */
            closeWeatherDropSettings: function() {
                this.weatherDropFront.animate({
                    'left': 0
                }, 200, this.slideEasing);
                this.weatherDropBack.animate({
                    'left': 240
                }, 200, this.slideEasing);
            },

            /**
             * Update weather dropdown data.
             * @param {string} location Location string.
             */
            updateMediaDropdown: function(location) {
                if (this.busyCheckingMedia) return;
                this.busyCheckingMedia = true;
                StateManager.fetchHtml('/services/media/dropdown/')
                    .done(this.parseMediaDropdown);
            },

            /**
             * Parse weather dropdown response and append/show/hide.
             * @param {string} htmlFrag HTML response text from XHR.
             */
            parseMediaDropdown: function(htmlFrag) {
                var fetchedMedia = $(htmlFrag);
                var dropdown = this.nav.find('.media .dropdown');
                dropdown.html(fetchedMedia.html());
                this.busyCheckingMedia = false;
            },

            /**
             * Update weather dropdown data.
             * @param {string} location Location string.
             */
            updateWeatherDropdown: function(location) {
                if (this.busyCheckingWeather) return;
                this.busyCheckingWeather = true;
                location = location || $.cookie('weatherLocation') || '22102';
                StateManager.fetchHtml('/services/weather/dropdown/' + location + '/')
                    .done(this.parseWeatherDropdown);
            },

            /**
             * Parse weather dropdown response and append/show/hide.
             * @param {string} htmlFrag HTML response text from XHR.
             */
            parseWeatherDropdown: function(htmlFrag) {
                var fetchedWeather = $(htmlFrag);
                var dropdown = this.nav.find('.weather .dropdown');
                var front = dropdown.find('.front-side');
                var navBtnTemp = this.nav.find('.weather .btn-temp');
                if (front.length) {
                    front.html(fetchedWeather.find('.front-side').html());
                } else {
                    dropdown.html(fetchedWeather.html());
                }
                var weatherIcon = fetchedWeather.find('.drop-icon').data('icon');
                navBtnTemp.attr('class', 'btn-temp ' + weatherIcon);
                navBtnTemp.html(fetchedWeather.find('.temp').html());
                navBtnTemp.parents('.weather').addClass('has-data');
                this.busyCheckingWeather = false;
            },

            /**
             * Click handler for nav search button.
             * @param {Event} e Click event.
             */
            onClickNavSearch: function(e) {
                if (this.isMastheadExpanded) {
                    if(this.isSearchResultsOpen && !this.body.hasClass('search-results')) { }
                    else {
                        this.collapseMasthead(true);
                    }
                } else {
                    this.expandMasthead(true);
                }
                return false;
            },

            /**
             * Click handler for close button in breaking news bar.
             * @param {Event} e Click event object.
             */
            onClickCloseBreakingBtn: function(e) {
                this.closeBreakingBar();
                PubSub.trigger('uotrack', 'breakingnewsbarexit');
            },

            /**
             * Open search results view
             */
            openSearchResults: function() {

                // Deeplinked to search results; leave header as is.
                //if(this.body.hasClass('search-results')) return;
                this.isSearchResultsOpen = true;

                // Fix nav.
                //this.expandMasthead(true);
                this.expandMasthead(!this.body.hasClass('search-results'));
            },

            /**
             * Close search results view
             */
            closeSearchResults: function() {
                this.isSearchResultsOpen = false;
                this.collapseMasthead(true);
                this.body.removeClass('show-search');
            },

            /**
             * Hide masthead (with slide animation).
             * @param {boolean=} animate Whether to animate closing.
             */
            collapseMasthead: function(animate) {
                if (this.isMastheadExpanded) {
                    this.isMastheadExpanded = false;
                    var props = {'height': '0'};
                    if (animate) {
                        this.searchWrap.stop().animate(props, this.slideSpeed,
                            this.slideEasing);
                    } else {
                        this.searchWrap.stop(false, true).css(props);
                    }
                    this.toggleLogo(animate, false);
                    this.body.removeClass('search-open');

                    // Scroll listener.
                    this.win.off('scroll', this.onScrollWindow);
                }
            },

            /**
             * Show masthead (with slide animation).
             * @param {boolean=} animate Whether to animate closing.
             */
            expandMasthead: function(animate) {
                if (!this.isMastheadExpanded) {
                    this.isMastheadExpanded = true;
                    var props = {
                        'height': this.mastheadHeight
                    };
                    if (animate) {
                        this.searchWrap.stop().animate(
                            props,
                            this.slideSpeed,
                            this.slideEasing,
                            function() {
                                $('.search .text-input').focus();
                            }
                        );
                    } else {
                        this.searchWrap.stop(false, true).css(props);
                    }
                    this.body.addClass('search-open');
                    this.toggleLogo(animate, true);

                    // Scroll listener.
                    this.win.on('scroll', this.onScrollWindow);
                }
            },

            /**
             * Toggle logo (with slide animation).
             * @param {boolean=} animate Whether to animate closing.
             * @param {boolean=} collapsed Whether to force a logo to be
             *     collapsed.
             */
            toggleLogo: function(animate, largeLogo) {
                console.log('toggle logo')
                if (animate) {
                    this.logo.stop().fadeOut(150, _.bind(function() {
                        this.logo.toggleClass('large', largeLogo)
                            .fadeIn(150);
                    }, this));
                } else {
                    this.logo.stop(false, true).toggleClass('large',
                        largeLogo);
                }
            },

            /**
             * Whether header height should animate.
             */
            shouldHeaderAnimate: function() {
                return App.getScrollPosition() <= 40;
            },

            /**
             * Close breaking news bar.
             */
            closeBreakingBar: function() {
                PubSub.trigger('breakingbar:before:close', this.navHeight, this.slideSpeed);
                this.isBreakingBarOpen = false;
                this.breakingLastUpdateClosed = this.breakingLastUpdate;
                $.cookie('breakingLastUpdateClosed', this.breakingLastUpdate);
                this.breakingHeight = -this.breaking.height();
                this.breaking.css('height', -this.breakingHeight);
                this.breakingWrap.css('position', 'absolute');
                this.breaking.stop().animate({'height': 0}, this.slideSpeed,
                    _.bind(function() {
                        if (!this.shouldHeaderAnimate()) this.updateHeaderMinHeight();
                        this.breaking.css('background-color', '');
                        PubSub.trigger('breakingbar:after:close');
                    }, this));
                if (this.shouldHeaderAnimate()) this.updateHeaderMinHeight(true);
                this.shiftSideCards(this.navHeight, true);
            },

            /**
             * Open breaking news bar.
             */
            openBreakingBar: function() {
                this.isBreakingBarOpen = true;
                this.breakingHeight = this.breakingWrap.height();
                PubSub.trigger('breakingbar:before:open', this.breakingHeight + this.navHeight, this.slideSpeed);
                var props = { 'height': this.breakingHeight };
                this.breaking.stop().animate(props, this.slideSpeed,
                    this.onCompleteOpenBreakingBar);
                if (this.shouldHeaderAnimate()) this.updateHeaderMinHeight(true);
                this.shiftSideCards(this.breakingHeight + this.navHeight, true);
            },

            /**
             * Update breaking bar after opening it.
             */
            onCompleteOpenBreakingBar: function() {
                this.breaking.css('height', 'auto');
                this.breakingWrap.css('position', 'static');
                if (!this.shouldHeaderAnimate()) this.updateHeaderMinHeight();
                PubSub.trigger('breakingbar:after:open');
            },

            /**
             * Set header min-height.
             * @param {boolean=} animate Whether to animate height change.
             */
            updateHeaderMinHeight: function(animate) {
                var breakingOffset = this.isBreakingBarOpen ?
                    this.breakingWrap.height() :
                    0;
                var height = this.navHeight + breakingOffset;
                if (animate) {
                    this.header.stop().animate({ 'min-height': height }, this.slideSpeed);
                } else {
                    this.header.css('min-height', height);
                }
                if (!this.shouldHeaderAnimate()) {
                    this.setScrollTop(this.win.scrollTop() +
                        this.breakingHeight);
                }
                this.busyCheckingBreakingNews = false;
            },

            /**
             * Shift side cards vertical postions.
             * @param {number} top Target top position of cards.
             * @param {boolean=} animate Whether to animate shift.
             */
            shiftSideCards: function(top, animate) {
                top = top || (this.navHeight + this.breaking.height());
                var cards = $('.card-fixed-container');
                if (animate) {
                    cards.stop().animate({'top': top}, this.slideSpeed);
                } else {
                    cards.css({'top': top});
                }
            },

            /**
             * Update breaking news bar.
             */
            updateBreakingBar: function() {
                if (this.busyCheckingBreakingNews) return;
                this.busyCheckingBreakingNews = true;
                StateManager.fetchHtml('/services/breaking-news/nav-bar/', {cache: false})
                    .done(this.parseBreakingNews);
            },

            /**
             * Check 'breakingLastUpdateClosed' cookie to determine whether to
             * open the breaking news bar.  This will be re-enabled once the
             * breaking news data is returned with the full page load later on.
             */
            // shouldOpenBreakingBar: function() {
            //     return this.breakingLastUpdate && this.breakingLastUpdateClosed
            //         !== this.breakingLastUpdate
            // },

            /**
             * Parse breaking news response and append/show/hide.
             * @param {string} htmlFrag HTML response text from XHR.
             */
            parseBreakingNews: function(htmlFrag) {
                var fetchedBreaking = $(htmlFrag);
                var fetchedBreakingLastUpdate =
                    fetchedBreaking.data('lastupdate');
                var fetchedBreakingType =
                    fetchedBreaking.data('type');
                if (!fetchedBreakingLastUpdate) {
                    this.closeBreakingBar();
                    this.busyCheckingBreakingNews = false;
                } else if (this.breakingLastUpdate !== fetchedBreakingLastUpdate) {
                    this.breakingLastUpdate = fetchedBreakingLastUpdate;
                    if (this.isBreakingBarOpen) {
                        this.breakingWrap.html(fetchedBreaking.find('p')).stop();
                        var highlightColor = this.breakingColors[fetchedBreakingType].highlight;
                        var normalColor = this.breakingColors[fetchedBreakingType].normal;
                        this.breaking
                            .animate({'backgroundColor': highlightColor})
                            .animate({'backgroundColor': normalColor});
                        this.busyCheckingBreakingNews = false;
                    } else {
                        this.breakingWrap.html(fetchedBreaking.find('p'));
                        this.breaking.attr('class', '').addClass(fetchedBreakingType);
                        this.openBreakingBar();
                    }
                } else {
                    this.busyCheckingBreakingNews = false;
                }
            },

            /**
             * Set scrollTop without triggering scroll event.
             * @param {number} top Scroll top setting.
             */
            setScrollTop: function(top) {
                this.preventScrollEvent = true;
                this.scrollEl.scrollTop(top);
                setTimeout(_.bind(function() {
                        this.preventScrollEvent = false;
                    }, this), 100);
            },

            /**
             * Click logout in the user auth dropdown.
             */
            onClickLogout: function() {
                this.toggleUserDropdown();
                Facebook.logout();
            },

            /**
             * Click handler for user auth settings button.
             */
            onClickLogin: function() {
                this.toggleUserDropdown();
                Facebook.login();
                PubSub.trigger('uotrack', 'headerprofile');
            }
        });

        /**
         * Return view class.
         */
        return HeaderView;
    }
);
