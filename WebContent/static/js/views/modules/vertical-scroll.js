/**
 * @fileoverview VerticalScroll view module.
 * 
 * When instantiating the class pass and object that defines:
 * el: The container of the scrollable content and where the scrollbar will be placed
 * contentClass: The class name that the scrollable content div has (this class must sit
 * within the container)
 * template: markup for the actual scroller
 * padding: left/right padding amount for the scroller
 * To use outside gallery thumbs set an alternate el and contentClass when instantiating
 * the class. Any issues / questions don't hesitate to contact me.
 *
 * SEE BASE-SCROLL.JS FOR TROUBLESHOOTING ADVICE
 * 
 * @author stephen.burgess@f-i.com (Stephen Burgess)
 */
 define([
    'jquery', 
    'underscore', 
    'views/modules/base-scroll'
],
function(
    $, 
    _, 
    BaseScroll
)
    {

        /**
         * View class.
         */
        var VerticalScrollView = BaseScroll.extend({

            /**
             * Initialize view.
             */
            initialize: function(options) {

                this.options = $.extend({
                    template: '<div class="vertical-scroll white"><div class="scrolldragger white"><div class="over-icon white"></div></div></div>',
                    contentClass: '.scrollable-content',
                    padding: 5
            	}, options || {});

                this.win = App.get('win');
                this.body = App.get('body');
                this.el = $(this.options.el);

                // Remove any pre-existing instances just in case.
                this.el.find('.vertical-scroll').remove();

                BaseScroll.prototype.initialize.apply(this, arguments);
            },

            /**
             * Refresh / setup the scrollbar
             */
            refresh: function() {

                if (this.content.length === 0) {
                    // There is no content for the scroller return and do nothing
                    return;
                }

                var wrapHeight = this.el.height();
                var barHeight = wrapHeight - (this.options.padding * 2);
                this.bar.css({height: barHeight, top: this.options.padding});
                
                var contentHeight = this.content.outerHeight();
                var percent = wrapHeight / contentHeight;
                var draggerHeight = percent * barHeight;
                this.dragger.css({height: draggerHeight});

                // Store re-usable calculations.
                this.overflow = contentHeight - wrapHeight;
                this.draggerSpace = barHeight - draggerHeight;

                if (percent < 1) {
                    this.el.append(this.bar);
                    this.bindEvents();
                    this.active = true;
                } else {
                    this.bar.remove();
                    this.unbindEvents();
                    this.active = false;
                    this.currentPosition = 0;
                }

                this.position(this.currentPosition, true);
            },

            /**
             * Position content.
             */
            position: function(percent, positionDragger) {
                var y = -(this.overflow * percent);
                this.content.css({marginTop: y+'px'});
                this.currentPosition = percent;

                if(positionDragger) {
                    var draggerY = this.draggerSpace * percent;
                    this.dragger.css({marginTop: draggerY+'px'});
                }

                this.el.trigger('scrollBarUpdate');
            },

            /**
             * Bar clicked.
             */
            onBarClicked: function(event) {
                var barMouseY = event.pageY - this.bar.offset().top;
                var percent = barMouseY / this.bar.height();
                this.position(percent);

                // Position dragger based on where bar was clicked.
                var pos = this.draggerSpace * percent;
                this.dragger.css({marginTop: pos+'px'});

                this.addActiveClass();
            },

            /**
             * Mouse move.
             */
            onMouseMove: function(event) {
                if(!this.isMouseDown) return;

                // Calculate the dragger position.
                var barMouseY = event.pageY - this.bar.offset().top;
                var draggerY = barMouseY - this.draggerOffset;
                draggerY = Math.max(0, Math.min(draggerY, this.draggerSpace));
                this.dragger.css({marginTop: draggerY+'px'});

                // Calculate % to position content.
                var percent = draggerY / this.draggerSpace;
                this.position(percent);
            },

            /**
             * Mouse down.
             */
            onMouseDown: function(event) {
                // Call onMouseDown on super.
                BaseScroll.prototype.onMouseDown.apply(this, arguments);

                // Calculate the offset from where the dragger was clicked.
                var barMouseY = event.pageY - this.bar.offset().top;
                var draggerY = parseInt(this.dragger.css('marginTop'));
                this.draggerOffset = barMouseY - draggerY;

                this.bar.addClass('active');
            },

            /**
             * Mouse wheel scroll.
             */
            onMouseWheel: function(event, delta) {
                if(this.isMouseDown) return;

                var deltaY = event.originalEvent.wheelDeltaY * 0.1;

                // Firefox doesn't support wheelDeltaY so we'll have to use detail.
                if(isNaN(deltaY)) deltaY = event.originalEvent.detail * -1;

                // IE doesn't support wheelDeltaY and returns 0 for detail,
                // it does support wheelDelta though.
                if(isNaN(deltaY) || deltaY === 0) deltaY = event.originalEvent.wheelDelta * 0.1;

                deltaY = deltaY < 0 ? Math.floor(deltaY) : Math.ceil(deltaY);
                var draggerY = parseInt(this.dragger.css('marginTop'));
                draggerY -= deltaY;
                draggerY = Math.max(0, Math.min(draggerY, this.draggerSpace));
                this.dragger.css({marginTop: draggerY+'px'});

                // Calculate % to position content.
                var percent = draggerY / this.draggerSpace;
                this.position(percent);

                if(percent > 0 && percent < 1 && deltaY != 0) {
                    // Don't bubble event if scrolling content.
                    event.preventDefault();
                    this.addActiveClass();
                }
            }

        });


        /**
         * Return view class.
         */
        return VerticalScrollView;
    }
);
