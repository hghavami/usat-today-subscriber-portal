/**
 * @fileoverview Share Tools view.
 * @author Jonathan Hensley <jhensley@gannett.com>
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'pubsub',
    'app',
    'views/modules/vertical-scroll',
    '//connect.facebook.net/en_US/all.js'
],
function(
    $,
    _,
    Backbone,
    PubSub,
    App,
    VerticalScroll
) {


    /**
     * View class.
     */
    var CommentView = Backbone.View.extend({

        el: '.utility-bar .comment-module',

        /**
         * Initialize view.
         */
        initialize: function() {
            _.bindAll(this, 'toggle');
            this.subviews = {};
            this.$btn = this.$('.btn');
            this.$flyout = this.$('.flyout-comments');
            this.$scrollWrap = this.$('.scroll-wrap');
            this.$scrollable = this.$('.scrollable-content');
            this.$utilWrap = this.$el.closest('.utility-wrap');
            this.$header = $('#header');
            this.$win = App.get('win');
            this.url = window.location.href;
            this.pubSub = {
                'breakingbar:after:open': this.scalePanel
            };
            PubSub.attach(this.pubSub, this);
            this.toggle();
        },

        /**
         * Listen for new comment to update count.
         */
        attachCommentListener: function() {
            FB.Event.subscribe('comment.create',
                _.bind(function() {
                    PubSub.trigger('comment:created');
                }, this)
            );
            FB.Event.subscribe('comment.remove',
                _.bind(function() {
                    PubSub.trigger('comment:removed');
                }, this)
            );
        },

        /**
         * Stop listening for new comments.
         */
        unattachCommentListener: function() {
            FB.Event.unsubscribe('comment.create');
            FB.Event.unsubscribe('comment.remove');
        },

        /**
         * Load Facebook Comments social plugin.
         */
        loadComments: function() {
            this.$scrollable
                .html('<div class="fb-comments " data-href="' + this.url +
                    '" data-num-posts="18" data-width="590"></div>');
            FB.XFBML.parse(this.$scrollable[0]);
            this.scrollableHeight = this.$scrollable.height();
            this.heightPoll = setInterval(_.bind(function(){
                if (this.scrollableHeight !== this.$scrollable.height()) {
                    this.scrollableHeight = this.$scrollable.height();
                    this.scalePanel(this.scrollableHeight);
                }
            }, this), 200);

            // Initialize the Vertical Scroll
            this.subviews.vscroll = new VerticalScroll({
                el: this.$scrollWrap,
                padding: 20
            });
        },

        /**
         * Sets up the proper height of the panel.
         */
        scalePanel: function(scrollableHeight) {
            if (!scrollableHeight) scrollableHeight = this.scrollableHeight;
            var availHeight = this.$win.height() - this.$header.outerHeight() -
                this.$utilWrap.outerHeight();
            var height = scrollableHeight > availHeight ? availHeight - 20 : scrollableHeight;
            this.$scrollWrap.css('height', height);
            if (this.subviews.vscroll) this.subviews.vscroll.refresh();
        },

        /**
         * Toggle the share panel open/closed.
         * @param {Event} e Browser event.
         * @param {boolean=} close Force close.
         */
        toggle: function(e, close) {
            close = close || this.$btn.hasClass('open');
            if (e) {
                if ($(e.target).closest(this.$scrollWrap).length > 0) return false;
            }
            if (close) {
                this.$btn.removeClass('open');
                this.$flyout.removeClass('open');
                clearInterval(this.heightPoll);
                PubSub.trigger('uotrack', 'UtilityBarFacebookOpen');
                this.$win.off('click.' + this.cid);
                this.unattachCommentListener();
                this.destroy();
            } else if (close === false) {
                this.$btn.addClass('open');
                this.$flyout.addClass('open');
                this.loadComments();
                this.attachCommentListener();
                PubSub.trigger('uotrack', 'UtilityBarFacebookClose');

                // Without this delay, the window click handler gets attached
                // before the event finishes propagation, so it gets triggered
                // from the initial click, effectively canceling the first call
                // to 'toggle'.
                _.delay(_.bind(function(){this.$win.on('click.' + this.cid, this.toggle);}, this), 100);
            }
        },

        /**
         * Clean up view.
         * Removes event handlers and element (optionally).
         * @param {boolean} removeEl option to also remove View from DOM.
         */
        destroy: function(removeEl) {
            this.undelegateEvents();
            _.each(this.subviews, function(view){
                if (typeof view.destroy !== 'undefined') {
                    view.destroy(removeEl);
                }
            });
            if (this.pubSub){
                PubSub.unattach(this.pubSub, this);
            }
            if (removeEl) {this.remove();}
        }
    });

    /**
     * Return view class.
     */
    return CommentView;
});
