/**
 * @fileoverview GalleryThumbScroll view module.
 * 
 * @author stephen.burgess@f-i.com (Stephen Burgess)
 */
 define(['jquery', 'underscore', 'views/modules/horizontal-scroll'],
    function($, _, HorizontalScroll) {

        /**
         * View class.
         */
        var GalleryThumbScroll = HorizontalScroll.extend({
            
            /**
             * Called from a carousel when next/previous buttons clicked.
             */
            updateThumbsPosition: function() {
                //if scroller isn't active do nothing
                if(!this.active) return;
                //otherwise find active thumb
                var thumbs = this.content.children();
                var i = thumbs.length;
                var thumb;
                while(--i > -1) {
                    thumb = $(thumbs[i]);
                    if(thumb.hasClass('active')) {
                        //this.centerThumb(thumb);
                        this.checkThumbPosition(thumb);
                        break;
                    }
                }
            },

            /**
             * Checks if the selected thumb is in full view, i.e. isn't offscreen.
             * If offscreen slide it across so it's in full view.
             */
            checkThumbPosition: function(thumb) {
                //calculations and vars
                var x = thumb.position().left;
                var pos = x + thumb.outerWidth();
                var margin;
                var offscreen;
                if(x < 0) {
                    //thumb off screen to left
                    margin = parseInt(this.content.css('marginLeft'), 10);
                    x = thumb.position().left - margin;
                    pos = x + thumb.outerWidth();
                    x = (-pos) + this.el.width();
                    x = Math.min(0, x);
                    offscreen = true;
                }
                else if(pos > this.el.width()) {
                    //thumb off screen to right
                    margin = parseInt(this.content.css('marginLeft'), 10);
                    x = -(thumb.position().left - margin);
                    x = Math.max(x, -this.overflow);
                    offscreen = true;
                }
                //if offscreen ease to new position
                if(offscreen) {
                    //if not sliding get current position
                    if(!this.slidingThumbs) {
                        this.slidingThumbs = true;
                        this.currentPercent = margin / -this.overflow;
                    }
                    //work out as percent and ease to it
                    this.targetPercent = x / -this.overflow;
                    if(this.slidingInterval) clearInterval(this.slidingInterval);
                    this.slidingInterval = setInterval($.proxy(this.updatePercent, this), 50);
                }
            },

            /**
             * Works out the position of the specified thumb and starts
             * an interval for animating the sliding.
             */
            centerThumb: function(thumb) {
                //content margin (how much it's scrolled)
                var margin = parseInt(this.content.css('marginLeft'), 10);
                //if not sliding get current position
                if(!this.slidingThumbs) {
                    this.slidingThumbs = true;
                    this.currentPercent = margin / -this.overflow;
                }
                //selected thumb x position
                var x = thumb.position().left - margin;
                //position to move scrollable content to
                var pos = ((this.el.width() * 0.5) - x) - (thumb.outerWidth() * 0.5);
                //ensure that end items stay on edge
                pos = Math.max(Math.min(pos, 0), -this.overflow);
                //work out as percent and ease to it
                this.targetPercent = pos / -this.overflow;
                if(this.slidingInterval) clearInterval(this.slidingInterval);
                this.slidingInterval = setInterval($.proxy(this.updatePercent, this), 50);
            },

            /**
             * Called by timer to ease the position of the content so
             * selected thumb is centered.
             */
            updatePercent: function() {
                //if we're more or less there cancel the update loop
                if(Math.abs(this.currentPercent - this.targetPercent) < 0.0001) {
                    this.currentPercent = this.targetPercent;
                    this.killSliding();
                }
                var vx = (this.targetPercent - this.currentPercent) * 0.4;
                this.currentPercent += vx;
                this.position(this.currentPercent, true);
            },

            /**
             * Stops the interval.
             */
            killSliding: function() {
                this.slidingThumbs = false;
                if(this.slidingInterval) clearInterval(this.slidingInterval);
            }

        });


        /**
         * Return view class.
         */
        return GalleryThumbScroll;
    }
);
