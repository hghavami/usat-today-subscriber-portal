/**
 * @fileoverview Blogs module view.
 * @author Chad Shryock <cdshryock@gannett.com>
 */
define(['jquery', 'underscore', 'backbone', 'pubsub'],
    function($, _, Backbone, PubSub) {


        /**
         * View class.
         */
        var BlogsView = Backbone.View.extend({

        	// Events.
            events: {
                'click .scroller' : 'scroll'
            },

            /**
             * Initialize view.
             * @param {Object} options View options passed in during init.
             */
            initialize: function(options) {
                this.$mod = $(options.el, document);
                this.$moveMe = $('.stage ul', this.$mod);
                this.totalItems = $('li', this.$moveMe).length;
                this.$scrollerLeft = $('.scroller.left', this.$mod);
                this.$scrollerRight = $('.scroller.right', this.$mod);

            },

            /**
             * Clean up view.
             * Removes event handlers and element (optionally).
             * @param {boolean} removeEl option to also remove View from DOM.
             */
            destroy: function(removeEl) {
                this.undelegateEvents();
                if (removeEl) this.remove();
            },

            scroll: function(event) {

                // pressed scroller
                var $scroller = $(event.target);

                // ignore if disabled
                if ($scroller.hasClass('disabled')) {
                    return false;
                }

                // get the vars to do the math
                var isScrollRight = $scroller.hasClass('right');
                var currentItemOffset = this.$moveMe.data("item-offset");
                var byNoItems = 4;
                if (window.matchMedia("screen and (min-width: 1440px)").matches) {
                    byNoItems = 6;
                } else if (window.matchMedia("screen and (min-width: 1280px)").matches) {
                    byNoItems = 5;
                }

                // Item Offset Count
                if (isScrollRight) {
                    byNoItems = byNoItems * -1;
                }
                var newItemOffset = byNoItems + currentItemOffset;
                if (newItemOffset > 0) {
                    newItemOffset = 0;
                }
                this.$moveMe.data("item-offset", newItemOffset);

                // Item Offset Pixels
                var currentPixelOffset = (currentItemOffset * 190) - 1;
                var changePixelOffset = byNoItems * 190;
                var newPixelOffset = changePixelOffset + currentPixelOffset;
                newPixelOffset = (newItemOffset * 190) - 1;

                // Scroller Left enable/disable
                if (newItemOffset == 0) {
                    this.$scrollerLeft.addClass('disabled');
                } else {
                    this.$scrollerLeft.removeClass('disabled');
                }

                // Scroller Right enable/disable
                if (Math.abs(newItemOffset) + Math.abs(byNoItems) > this.totalItems) {
                    this.$scrollerRight.addClass('disabled');
                } else {
                    this.$scrollerRight.removeClass('disabled');
                }

                // do the move
                this.$moveMe.css('left', newPixelOffset);

                return false;

            }

        });

        /**
         * Return view class.
         */
        return BlogsView;
    }
);
