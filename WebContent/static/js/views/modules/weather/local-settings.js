/**
* @fileoverview USAToday weather local settings view
* @author plinders@gannett.com (Pim Linders)
*/

define([],
    function(){
        return {
            target: "#weather-maps",
            maps: [
                {
                    id: "continental",
                    url: "http://a.tiles.mapbox.com/v3/usatoday.map-m28xx5xa.jsonp", 
                    scrollTo: true,
                    zoom: true,
                    options: {
                        zoom: 9,
                        dragging: true,
                        doubleClickZoom: true,
                        scrollWheelZoom: true
                    },
                    data: {
                        radar: true,
                        satellite: true,
                        worldSatellite: false
                    }
                }
            ]
        };
});