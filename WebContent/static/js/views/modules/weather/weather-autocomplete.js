/**
 * @fileoverview Weather  autocomplete view.
 * @author stwilson@gannett.com (Stan Wilson)
 */

define(['jquery' , 'underscore', 'backbone','pubsub', 'state'],
    function($, _, Backbone, PubSub, StateManager) {

        var weatherAutoComplete = Backbone.View.extend({

            el : "#weather-forecast-sidebar",

            events : {
                  'click .location-load': 'loadLocation'
            },

             destroy : function(){

                 this.undelegateEvents();
                PubSub.unattach(this.pubSub, this);

            },
            initialize: function(options) {
                    this.locationField  = options.locationField;
                    this.loadButton = options.loadButton ;
                    this.addAutocomplete();

                    this.delegateEvents();
            },
            addAutocomplete : function(){

                var _this = this;

                require(['libs/leaflet/jquery-ui-1.8.21.custom.min' ],function(){ 
                    $( _this.locationField ).addClass('ui-light');
                    $(_this.locationField).autocomplete( _this.autoCompleteSettings );
                    $('.ui-autocomplete').addClass('ui-light');

                });
            },
            loadLocation : function(e){
                e.preventDefault();

                var  locationValue = $( this.locationField ).val();

                if( locationValue == "" ){
                     $(".change-loc").addClass("not-zip");
                     return false
                }else{
                    $(".change-loc").removeClass("not-zip").removeClass("not-found");
                }


                var publishData = function(data){
                    if( data[0] !=   null ){

                        $(".change-loc").removeClass("not-found").removeClass("not-zip");
                        PubSub.trigger('weather:newLocation', data);
                        $('#weather-forecast-sidebar').removeClass("settings-active");
                    } else {
                         $(".change-loc").addClass("not-found")

                    }
                };
                StateManager.fetchData('/services/weather/forecast/json/' + locationValue + '/')
                    .done(publishData);
            },
            autoCompleteSettings : {

                open: function(){
                        $(this).autocomplete('widget').css('z-index', 1);
                        return false;
                    },
                // Dynamic Source

                source :  function(request, response) {

                    StateManager.fetchData('/services/city/autocomplete/json/'+request.term)
                        .done(function(data) {
                            data = data.splice(0,10);

                            response( $.map( data, function( item ) {
                               return {
                                    label: item.City +  ", "+item.State,
                                    value: item.City+" "+item.State
                                }
                            }));
                        });
                },

                minLength: 3,
                select: function( event, ui ) {

                     this.value = ui.item.value;

                   $( ".location-load" ).trigger("click");



                }
            }

        });

    return weatherAutoComplete;

});





