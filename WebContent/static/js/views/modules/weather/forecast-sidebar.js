/**
 * @fileoverview Weather  forecast sidebar view.
 * @author stwilson@gannett.com (Stan Wilson), mdkennedy@gannett.com (Mark Kennedy)
 */

 define(['jquery' , 'underscore', 'backbone','pubsub','views/modules/sidebar-scroll','chosen'],
    function($, _, Backbone, PubSub, SidebarScroll) {

 
        var ForecastSidebar = Backbone.View.extend({

            el : "#weather-forecast-sidebar",

            // declare global variables
            scrollbar: null,
            timer: null,

            events : {
              "click .sub.top": 'toggleForecastData',
              "click .icon.settings" : 'showSettings',
              "click .icon.close" : 'hideSettings'
            },

            destroy : function(){
                this.undelegateEvents();
                PubSub.unattach(this.pubSub, this);
                scrollbar.destroy();
            },

            initialize: function(data) {
                this.getLocationForecast(data);

                // PubSub events.
                this.pubSub = {
                    'weather:newLocation': this.getLocationForecast,

                };
                //$("#forecast-content").delegate(,"click",this.toggleMoreData);
                PubSub.attach(this.pubSub, this);
            },

            /**
             * Handles collapsing/expanding elements in forecast area
             */
            toggleForecastData : function(e){
                 e.preventDefault();
                 e.stopPropagation();
            
                var $target = $(e.currentTarget).closest('.sub'),            
                $expandableDiv = $target.children('div').first(),
                isExpanded = $target.hasClass("active"),    
                animTime

                if ($expandableDiv.css('-webkit-transition-duration')) {
                    animTime = $expandableDiv.css('-webkit-transition-duration');
                } else if($expandableDiv.css('-moz-transition-duration')) {
                    animTime = $expandableDiv.css('-moz-transition-duration');
                } else if($expandableDiv.css('-o-transition-duration')) {
                    animTime = $expandableDiv.css('-o-transition-duration');
                } else if($expandableDiv.css('transition-duration')) {
                    animTime = $expandableDiv.css('transition-duration');
                }

                //convert seconds to milliseconds for setTimeout delay
                var delay = parseFloat(animTime.replace(/[^0-9\.]+/g, ''))* 1000;

                if (animTime) {
                    if( isExpanded ){
                        $target.removeClass('active');
                        $expandableDiv.css('height','0px');
                     } else {
                        $('#weather-forecast-sidebar .sub').removeClass('active').children('div').css('height','0px');
                        $target.addClass('active');
                        $expandableDiv.css('height',$expandableDiv[0].scrollHeight+'px');
                     };

                     var _this = this;
                     this.timer = setTimeout(function(){
                            _this.refreshScrollbar();
                        }, delay);
                 }
                
             },
            /**
             * Load 
             * @param {string} location Location string.
             */

            getLocationForecast : function(data) {
                 
                var currtmpl = $("script.forecast-current-tmpl").html(),
                    hourexttmpl = $("script.forecast-hourext-tmpl").html(),
                    currentContent = _.template( currtmpl , { loc : data }),
                    hourlyExtendedContent = _.template( hourexttmpl , { loc : data });

                    this.$el.children('.forecast-content').children('#forecast-current').html( currentContent );
                    this.$el.children('.forecast-content').children('#forecast-scroll-content').children('.scrollable-content').html( hourlyExtendedContent );

                    _.defer(this.addScrollbar);

            },

            /**
             * Adds scrollbar.
             */
            addScrollbar : function(){

               // Add scrollbar
               scrollbar = new SidebarScroll({
                   el: '#forecast-scroll-content',
                   template: '<div class="vertical-scroll ui-light"><div class="scrolldragger"></div></div>',
                   padding: 2
               });
               scrollbar.addHideClass();
            },
            
             /**
              * Refreshes the scrollbar.
              * @param {Object} scrollbar to refresh
              */
             refreshScrollbar : function(){
                scrollbar.refresh();
                clearTimeout(this.timer);
             },
             
             /**
              * Shows settings panel.
              */
             showSettings : function(){
                if (!this.$el.hasClass('settings-active')) {
                    this.$el.addClass('settings-active');
                }
             },

             /**
              * Hides settings panel.
              */
             hideSettings : function(){
                if (this.$el.hasClass('settings-active')) {
                    this.$el.removeClass('settings-active');
                }
             }

        });


        return ForecastSidebar;

})