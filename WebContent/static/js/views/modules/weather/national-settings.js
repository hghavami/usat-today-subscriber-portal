/**
* @fileoverview USAToday weather national settings view 
* @author plinders@gannett.com (Pim Linders)
*/

define([],
    function(){
        return {
            target: "#weather-maps",
            maps: [
                {
                    id: "continental",
                    url: "http://a.tiles.mapbox.com/v3/usatoday.national-weather.jsonp",
                    scrollTo: false,
                    zoom: false,
                    options: {
                        lat: 36.73888412439431,
                        lon: -95.888671875,
                        zoom: 4
                    },
                    data: {
                        temp: [
                            "Bangor ME",
                            "New York NY",
                            "Cleveland OH",
                            "Boston MA",
                            "Washington DC",
                            "Raleigh NC",
                            "Myrtle Beach SC",
                            "Atlanta GA",
                            "Jacksonville FL",
                            "Tampa FL",
                            "Miami FL",
                            "Rochester NY",
                            "Chicago IL",
                            "Cincinnati OH",
                            "Somerset KY",
                            "Memphis TN",
                            "Jackson MS",
                            "New Orleans LA",
                            "St. Louis MO",
                            "Wichita KS",
                            "Salt Lake City UT",
                            "Bismarck ND",
                            "Pierre SD",
                            "Lincoln NE",
                            "Sioux City IA",
                            "Billings MT",
                            "Kalispell MT",
                            "Casper WY",
                            "Minneapolis MN",
                            "Denver CO",
                            "Durango CO",
                            "Oklahoma City OK",
                            "Dallas TX",
                            "San Antonio TX",
                            "Lubbock TX",
                            "Albuquerque NM",
                            "Phoenix AZ",
                            "Las Vegas NV",
                            "Reno NV",
                            "Boise ID",
                            "Bend OR",
                            "Seattle WA",
                            "Portland OR",
                            "Crescent City CA",
                            "San Francisco CA",
                            "Los Angeles CA"
                        ],
                        radar: true,
                        satellite: true
                    }
                },
                {
                    id: "hawaii",
                    url: "http://a.tiles.mapbox.com/v3/usatoday.national-weather.jsonp",
                    scrollTo: false,
                    zoom: false,
                    options: {
                        lat: 20.591652120829167,
                        lon: -157.2802734375,
                        zoom: 4
                    },
                    data: {
                        temp: [
                            "Honolulu HI"
                        ],
                        radar: false,
                        satellite: false
                    }
                },
                {
                    id: "alaska",
                    url: "http://a.tiles.mapbox.com/v3/usatoday.national-weather.jsonp",
                    scrollTo: false,
                    zoom: false,
                    options: {
                        lat: 62.75472592723178, 
                        lon: -155.390625,
                        zoom: 2
                    },
                    data: {
                        temp: [
                            "Anchorage AK"   
                        ],
                        radar: false,
                        satellite: false
                    }
                }
            ]
        };
});