/**
* @fileoverview USAToday weather map view
* @author plinders@gannett.com (Pim Linders)
*/
define([
    'jquery',
    'underscore', 
    'backbone',
    'app',
    'pubsub',
    'views/modules/loader',
    'leaflet',
    'tilelayer',
    'waxleaf'
],
function($, _, Backbone, App, PubSub, Loader) {

    var WeatherMap = Backbone.View.extend({

        el : "#weather-maps-wrapper",
        events:{
                    'click #weather-maps-wrapper a[data-heattrack|="weather"]': 'setTrack'
        },

        initialize: function(location, data, el) {},

        /*heat track events*/
        setTrack: function(e){
            if(e.type == 'click' && e.hasOwnProperty('originalEvent')){
                var targetLink = $(e.currentTarget).find('a');
                //heattracking
                App.setHeatTrack(targetLink);
            }
        },

        /**
         * Clean up view.
         * Removes event handlers and element (optionally).
         * @param {boolean} removeEl option to also remove View from DOM.
         */
        destroy: function(removeEl) {
            this.undelegateEvents();
            this.destroyMap();
            if (this.switchControls) {
                this.destroySwitchControls(this.switchControls);
            }
            if (removeEl) {
                this.remove();
            }
        },

        destroySwitchControls: function(controls) {
            var self = this;
            // set intial selected to first control element
            $.each(controls, function(index, control){
                control.$element.unbind('click').parent('li').removeClass('selected');
            });
        },

        createSwitchControls: function(controls, data) {
            var self = this,
            params;
            // set menu selected
            $.each(controls, function(index, control) {
                // set menu selected
                if(typeof control.active !== 'undefined' && control.active === true){
                    control.$element.parent('li').addClass('selected');
                    params = control.params;
                }
                control.$element.bind('click', function(e){
                    e.preventDefault();
                    //setTrack(e);
                    var $this = $(this);
                    // remove selected class
                    $this.parent('li').parent('ul').children().removeClass('selected');
                    // add selected class
                    $this.parent('li').addClass('selected');
                    self.destroyMap();
                    // add class to weather-maps container
                    $('#weather-maps').removeClass().addClass('map-' + $this.text().toLowerCase());

                    self.weatherMap = self.createMap(control.params, self.data);
                });
            });
            return params;
        },

        setup: function(controls, data) {
            var self = this
            self.data = data;
            var params = self.createSwitchControls(controls, data);
            self.weatherMap = self.createMap(params, data);
        },

        destroyMap: function(object) {
            var self = this;
            // Reset DOM elements
            $('#weather-maps-wrapper').empty().append($('<div>').attr('id','weather-maps'));
            // quick fix for killing events
            self.weatherMap.killEvents = true;
            self.weatherMap = null;
            PubSub.unattach(self.pubSub, self);
            // delete weahter map
            delete self.weatherMap;
        },

        createMap: function(params, data) {
            var self = this,
            weatherMap = new self.wMap();
            weatherMap.init(params, data);
            self.pubSub = {
                'weather:newLocation': function(data){
                    self.data = data;
                    weatherMap.setLocationDetails(data);
                }
            };
            PubSub.attach(self.pubSub, self);
            return weatherMap;
        },

        wMap: function() {
            this.debug = false;
            this.popups = [];
            this.overlays = {
                radarLayer: {
                    text: 'Radar',
                    type: 'l2radary',
                    maxZoom: 11,
                    maps: [],
                    labels: {
                        interval: 2,
                        max: 19
                    }
                },
                satLayer: {
                    text: 'Satellite',
                    type: 'satconus',
                    maxZoom: 9,
                    maps: [],
                    labels: {
                        interval: 2,
                        max: 19
                    }
                }
            };
            this.options = null;
            this.type = null;
            this.killEvents = false;
            this.location = {};
            this.setType = function(type) {
                $target = $(this.options.target);
                this.type = type;
                // friendly named classes for Mark the Great
                type = (type === 'l2radary') ? 'radar' : type;
                type = (type === 'satconus') ? 'satellite' : type;
                // get all classes and remove type class
                var classes = $target[0].className.replace(/(type-.[a-zA-Z0-9]*)/g, '');
                // add new type class
                $target.removeClass().addClass(classes).addClass('type-' + type);
            };
            this.icon = L.Icon.extend({
                options: {
                    iconUrl: '/static/images/weather/marker-combined.png',
                    iconSize: new L.Point(26, 23),
                    iconAnchor: new L.Point(7, 22)
                }
            });
            this.loader = {
                queue: [],
                create: function(target) {
                    this.$loader = new Loader({
                        el: target,
                        msg: 'Loading'
                    });
                    this.$target = $(target);
                    return this;
                },
                addToQ: function(item) {
                    if (item !== undefined) {
                        // push item to queue
                        if (this.queue.length === 0){
                            this.show();
                        }
                        this.queue.push(item);
                    }
                },
                removeFromQ: function(item) {
                    var self = this;
                    // queue is not empty
                    if (self.queue.length) {
                        // remove item from queue
                        self.queue = jQuery.grep(self.queue, function(value) {
                            return value !== item;
                        });
                        if (self.queue.length === 0) {
                            self.hide();
                        }
                    }
                },
                hide: function() {
                    var self = this;
                    clearTimeout(this.loaderTimer);
                    self.$target.removeClass('loading').addClass('loaded');
                    self.$loader.hide();
                },
                show: function() {
                    var self = this;
                    clearTimeout(this.loaderTimer);
                    this.loaderTimer = setTimeout(function(){
                        self.$target.removeClass('loaded').addClass('loading');
                        self.$loader.show();
                    }, 300);
                }
            };

            // create maps
            this.createMaps = function(target, mapsParams, data) {
                var self = this,
                maps = [],
                lat,
                lon,
                pubSub,
                loaded = 0;
                // loop through each map
                $.each(mapsParams, function(index, mapParams) {
                    // create markup for maps
                    $(target).append($('<div>').attr('id', mapParams.id).addClass('weather-map'));
                    // default map options
                    var mapOptions = {
                        zoomControl: false,
                        dragging: false,
                        doubleClickZoom: false,
                        scrollWheelZoom: false,
                        maxZoom: 11,
                        attributionControl: ''
                    };
                    // lat lon not specified, use location lat lon
                    if (typeof mapParams.options.lat === 'undefined' && typeof mapParams.options.lon === 'undefined') {
                        lat = data[0].local.lat;
                        lon = data[0].local.lon;
                    }
                    // use map param lat lon
                    else {
                        lat = mapParams.options.lat;
                        lon = mapParams.options.lon;
                    }
                    // extend map options
                    $.extend(
                        // default options
                        mapOptions,
                        // passed in options
                        mapParams.options,
                        // create center from lat and lon, convert to string for IE
                        {
                            center: new L.LatLng(String(lat), String(lon))
                        }
                    );
                    // create maps
                    var map = new L.Map(mapParams.id, mapOptions);
                    // add scroll param to map
                    map.scrollTo = mapParams.scrollTo;
                    // add zoom param to map
                    map.hasZoomControls = mapParams.zoom;
                    // get maptiles from mapbox
                    wax.tilejson(mapParams.url, function(tilejson) {
                        // create mapbox tiles
                        var mapbox = new wax.leaf.connector(tilejson);
                        map.addLayer(mapbox);
                    });
                    map.on('zoomend', function(e) {
                        self.reloadOverlay(this);
                    });
                    map.on('dragend', function(e) {
                        $('.leaflet-tile-pane > .leaflet-layer:not(.map-layer) img').css('opacity', self.opacity.getVal());
                    });
                    map.id = mapParams.id;
                    // prep map for overlay data
                    map.overlays = {};
                    if (mapParams.data.radar) {
                        self.overlays.radarLayer.maps.push(map);
                        map.overlays[self.overlays.radarLayer.type] = {
                            layers: [],
                            urls: []
                        };
                    }
                    if (mapParams.data.satellite) {
                        self.overlays.satLayer.maps.push(map);
                        map.overlays[self.overlays.satLayer.type] = {
                            layers: [],
                            urls: []
                        };
                    }
                    maps.push(map);
                    // check for temperature data
                    if (typeof mapParams.data.temp === 'object') {
                        // get temperature data
                        self.getTempData(mapParams.data.temp, map);
                    }
                });
                pubSub = {
                    'gotTempData': function() {
                        loaded += 1;
                        if(maps.length === loaded && !self.killEvents) {
                            // remove temp from loader
                            self.loader.removeFromQ('temp');
                            // allow the player to play
                            self.player.loaded();
                            // remove pubsub
                            PubSub.unattach(pubSub, self);
                        }
                    }
                };
                PubSub.attach(pubSub, self);
                return maps;
            };

            this.addOverlay = function(overlay, map, data) {
                var self = this,
                frames = [],
                frameCnt = 0,
                i = 0,
                d = new Date(Date.now()),
                dateTimes = [];
                // get frames
                data.frames = data.frames.reverse();
                for(i=0; i<data.frames.length; i+=overlay.labels.interval) {
                    frames.push(data.frames[i]);
                    frameCnt += 1;
                    if(frameCnt === overlay.labels.max){
                        break;
                    }
                }
                $.each(frames, function(index, frame) {
                    // add labels to slider
                    if (map === self.maps[0]) {
                        if (self.player.labels[overlay.type] === undefined) {
                            self.player.labels[overlay.type] = {
                                dateTimes: [],
                                hours: true,
                                shortHours: false,
                                dates: true,
                                days: false
                            };
                        }
                        var dateTime = new Date(Date.UTC(d.getUTCFullYear(), d.getUTCMonth(), d.getUTCDate(), frame.substring(0, frame.length/2),  frame.substring(frame.length/2, frame.length), 0, 0));
                        // adjust date appropriately, i.e. previous date = jul 4 0012 and current date = jul 4 11:55
                        if(index > 0 && dateTime.getTime() > dateTimes[index-1].getTime()){
                            // current date is more than previous date, change dateTime to previous day i.e. july 3 11:55
                            while(dateTime.getTime() > dateTimes[index-1].getTime()){
                                // set date to previous date
                                dateTime.setDate(d.getDate()-1);
                            }
                        }
                        dateTimes.push(dateTime);
                    }
                    url = data.url + '/' + frame + '/{z}/{x}_{y}.png';

                    map.overlays[overlay.type].urls.push(url);
                });
                // set labels and reverse frame order, frames set to oldest to newest
                self.player.labels[overlay.type].dateTimes = dateTimes.reverse();
                // reverse overlay urls
                map.overlays[overlay.type].urls.reverse();
                // set initial slider options
                self.player.setMax(dateTimes.length-1);
                self.player.createLabels(overlay.type, 2);
                self.loadOverlay(map);
            };

            this.reloadOverlay = function(map) {

                var self = this,
                // get layers
                overlay = self.type,
                layers = map.overlays[overlay].layers,
                key;
                // remove other layers from the map
                $.each(map.overlays, function(index, overlay) {
                    $.each(overlay.layers, function(index, layer) {
                        map.removeLayer(layer);
                    });
                   overlay.layers = [];
                });
                self.loadOverlay(map);
            };

            this.loadOverlay = function(map){
                var self = this,
                // get overlay type
                overlay = self.type,
                // tile load counter
                loadCnt = 0,
                // event timestamp
                d = new Date();

                time = d.getTime();
                // recreate layers from urls
                $.each(map.overlays[overlay].urls, function(index, url) {
                    // create new layer
                    var layer = new L.TileLayer(url, {maxZoom: overlay.maxZoom});
                    layer.url = url;
                    // add layers and containers to map
                    map.overlays[overlay].layers.push(layer);
                    layer.addEventListener('tileloading', function() {
                        // add tiles to be loaded
                        loadCnt += 1;
                        if (loadCnt === 1 && !self.killEvents) {
                            // set player into wait mode
                            self.player.wait();
                            if (self.debug) {
                               console.log('starting ' + time);
                            }
                        }
                        layer.hasLoaded = false;
                    });
                    layer.addEventListener('tileload', function() {
                        // subtract loaded tiles
                        loadCnt -= 1;
                        layer.hasLoaded = true;
                        if (loadCnt === 0 && !self.killEvents) {
                            // set player loaded mode
                            self.player.loaded();
                            if (self.debug) {
                               console.log('loaded ' + time);
                            }
                        }
                    });
                    // add layer to map
                    map.addLayer(layer);
                    // hide all but the current selected overlay
                    if (index !== self.player.getVal()) {
                        $(layer._container).hide();
                    }
                    $(layer._container).children().css('opacity', self.opacity.getVal());
                });
                // hide loader
                self.loader.removeFromQ('overlay');
            };

            // get overlay data
            this.getOverlayData = function(overlay, map){
                var self = this;
                $.ajax({
                    url: '/services/weather/overlay/' + overlay.type,
                    dataType: 'json',
                    success: function(data) {
                        self.addOverlay(overlay, map, data);
                    }
                });
            };

            // get temperature data from urls
            this.getTempData = function(locations, map) {
                var self = this,
                // set intial type
                initState = 'hourly';
                self.setType(initState);
                // create popup layer group
                map.popupLayer = new L.LayerGroup();
                $.ajax({
                    url: '/services/weather/forecast/json/' + locations.join(',') + '/',
                    dataType: 'json',
                    cache: false,
                    success: function(data) {
                        if(data[0] !== null) {
                            $.each(data, function(index, location) {
                                if(location !== null){
                                    map.popupLayer.addLayer(self.createTempPopup(location, map));
                                    // setup intial state
                                    if(index === 0 && self.maps[0] === map) {
                                        // bind slider events
                                        self.player.$container.bind('slide slidechange', function(e, ui) {
                                            self.player.setActiveLabel(ui.value);
                                            // update popup info
                                            $.each(self.popups, function(index, popup) {
                                                self.updateTempPopup(popup, initState, ui.value);
                                            });
                                        });
                                    }
                                }
                                else {
                                    console.warn('WMAP ERROR: Unable to find data for ' + locations[index]);
                                }

                            });
                        }
                        else {
                            console.warn('WMAP ERROR: Unable to find data for ' + locations.join(','));
                        }
                        PubSub.trigger('gotTempData');
                    }
                });
            };

            // create popup
            this.createTempPopup = function(data, map) {
                var self = this,
                popup = new L.Popup({
                    className: ' weathertile-map',
                    closeButton: false
                }),
                temp = data.forecast.hourly[0].temperature,
                tempClass = self.getTempClass(temp),
                // stores infobox
                infobox = null;
                // over ride popup close
                popup._close = function(){};
                popup.setLatLng(new L.LatLng(String(data.local.lat), String(data.local.lon)));
                var restorePopups = function(){
                    // unbind click event
                    $('body').unbind('click.wmap-clicked');
                    // restore opacity
                    $.each(self.popups, function(index, popup){
                        popup.$div.parent().css('opacity', '1');
                    });
                };
                var timer;
                var $div = $('<div>').addClass('weathertile weathertile-sm ' + tempClass + ' city-' + data.local.cityId)
                    .append($('<span>').addClass('city').text(data.local.city))
                    .append($('<span>').addClass('condition wicon wicon-' + data.forecast.hourly[0].weathericon).text(data.forecast.hourly[0].txtshort))
                    .append($('<span>').addClass('temp').html(temp + '&deg;'))
                // show infobox
                .click(function(e){
                    e.preventDefault();
                    //setTrack(e);
                    $this = $(this);
                        var $wrapper = $this.parent();
                        var animate = function(){
                            clearTimeout(timer);
                            $wrapper.addClass('animating');
                            timer = setTimeout(function(){
                                $wrapper.removeClass('animating');
                                clearTimeout(timer);
                            }, 350)
                        };
                        var close = function(){
                            animate();
                            $wrapper.removeClass('clicked').removeClass('bottom');
                            // restore opacity

                            restorePopups();
                        };
                        // open marker if its not the active marker
                        if(!$wrapper.hasClass('clicked')){
                            // remove all clicked classes
                            $('.clicked').removeClass('clicked');
                        $.each(self.popups, function(index, popup) {
                            if($this[0] !== popup.$div[0]){
                                popup.$div.parent().css('opacity', '0.6');
                            }
                            else {
                                $wrapper.css('opacity', '1');
                            }
                        });
                        if(parseInt($wrapper.css('bottom'), 10) > -70) {
                                $wrapper.addClass('bottom')
                        }
                        if(self.debug) {
                            console.log(data);
                        }
                        $wrapper.addClass('clicked');
                        animate();
                        $('body').bind('click.wmap-clicked', function(event) {
                            if (!$(event.target).closest('.clicked').length) {
                                close();
                            }

                        });
                    }
                    // close marker if clicked
                    else {
                        close();
                    }
                });
                // add popup to map
                map.addLayer(popup);
                //get rid of tip container and wrapper --dont need
                $(popup._tipContainer).remove();
                $(popup._wrapper).remove();
                //add content -- try not to use setContent method because it adds unnecessary markup that throws off click action effects
                $(popup._container).append($div);
                self.popups.push({
                    data: data,
                    popup: popup,
                    $div: $div
                });
                return popup;
            };

            this.updateTempPopup = function (popup, type, index) {
                var self = this,
                temp = null,
                lowTemp = null;
                if (type === 'hourly') {
                    temp = popup.data.forecast.hourly[index].temperature + '&deg;';
                }
                else if (type === 'extended') {
                    temp = popup.data.forecast.day[index].daytime.hightemperature + '&deg;';
                    lowTemp = $('<span>').addClass('low').html(' / ' + popup.data.forecast.day[index].daytime.lowtemperature + '&deg;');
                }
                popup.data.forecast.hourly[index].temperature;
                var tempClass = self.getTempClass(temp),
                // remove temp from classs
                classes = popup.$div[0].className.replace(/\btemp.*?\b/g, '');

                popup.$div.removeClass().addClass(classes + ' ' + tempClass);
                popup.$div.children('.temp').html(temp).append(lowTemp);
                popup.$div.children('.wicon').removeClass().addClass('condition wicon wicon-' + popup.data.forecast.hourly[index].weathericon).text(popup.data.forecast.hourly[index].txtshort);
            };

            this.createControls = function(maps, target, mapsParams) {

                 var self = this,
                $zoomControl,
                cnt = 0,
                firstOverlay,
                $wrapper = $('<div>').attr('id', 'weathermap-controls').addClass('clearfix ui-light'),
                $controls = $('<ul>').addClass('map-control mapview-control ui-tabgroup'),
                createOpacityControl = function(target) {
                    var self = this,
                    opacity = {
                        $wrapper: $('<div>').addClass('map-control opacity-control opacity-slider ui-slider ui-light'),
                        $value: $('<span>').addClass('ui-label ui-label-right opacity-control-value'),
                        $container: $('<div>').addClass('opacity-slider-bar ui-slider-bar').slider({
                            min: 0,
                            max: 1,
                            step: 0.1,
                            animate: true,
                            value: 1,
                            slide: function(e, ui){
                                opacity.setOpacity(e, ui);
                            },
                            change: function(e, ui){
                                opacity.setOpacity(e, ui);
                            },
                            create: function() {
                                $(this).removeClass('ui-slider');
                            }
                        }),
                        show: function() {
                            opacity.$wrapper.show();
                        },
                        hide: function() {
                            opacity.$wrapper.hide();
                        },
                        getVal: function() {
                            return opacity.$container.slider('opacity-control-value');
                        },
                        setVal: function(value) {
                            return opacity.$container.slider('opacity-control-value', value);
                        },
                        setOpacity: function(e, ui) {
                            $('.leaflet-tile-pane > .leaflet-layer:not(.map-layer) img').css('opacity', ui.value);
                            opacity.$value.text(ui.value * 100 + '%');
                        },
                        init: function(target) {
                            opacity.$wrapper
                                .append($('<span>').addClass('map-control-label opacity-label ui-label ui-label-left').text('Opacity'))
                                .append(opacity.$container)
                                .append(opacity.$value.text('100%'));
                            opacity.$container.children('a').addClass('opacity-slider-handle ui-btn');
                            $(target).append(opacity.$wrapper.hide());
                            return opacity;
                        }
                    };
                    return opacity.init(target);
                },
                // add active class to control
                setActive = function(target) {
                    // already active, return false to prevent any actions from occuring
                    if(!$(target).hasClass('active')) {
                        // stop slider if playing
                        self.player.stop();
                        $controls.children('li').children('a').removeClass('active');
                        $(target).addClass('active');
                        return true;
                    }
                    return false;
                },
                // radar and sat click
                overlayClick = function(e, overlay, element) {
                    e.preventDefault();
                    //setTrack(e);
                    var getContainers = function(map, overlay) {
                        var layers = map.overlays[overlay.type].layers,
                        containers = [];
                        $.each(layers, function(index, layer) {
                            containers.push(layer._container);
                        });
                        return containers;
                    };
                    if(setActive(element)) {
                        // show loader
                        self.loader.addToQ('overlay');
                        // reset slider
                        self.player.setVal(0);
                        // set slider max, set to 0 in case of error
                        self.player.setMax(0);
                        // remove all markers
                        $('.marker').remove();
                        // remove all labels
                        $('.label').remove();
                        // hide popups
                        $('.leaflet-popup-pane').hide();
                        // reset opacity
                        self.opacity.show();
                        self.opacity.setVal(1);
                        //set type on slider
                        self.setType(overlay.type);
                        // set play speed
                        self.player.playSpeed = 300;
                        // hide all overlays
                        $('.leaflet-tile-pane > .leaflet-layer:not(.map-layer)').addClass('hidden');
                        // hide all but the main map
                        for(i=1; i<self.maps.length; i += 1) {
                            $(self.maps[i]._container).hide();
                        }
                        $.each(overlay.maps, function(index, map) {
                            if (map.scrollTo) {
                                // set max zoom
                                map.options.maxZoom = overlay.maxZoom;
                            }
                            // reload overlay
                            if (map.overlays[overlay.type].started) {
                                // zoom out map to overlay max zoom
                                if(map.scrollTo && map.getZoom() > overlay.maxZoom) {
                                     map.setZoom(overlay.maxZoom);
                                }
                                self.reloadOverlay(map);
                                // set slider max
                                self.player.setMax(self.player.labels[overlay.type].dateTimes.length-1);
                                self.player.createLabels(overlay.type, 2);
                            }
                            // first time
                            else {
                                if (map.scrollTo) {
                                    // zoom map to overlay max zoom
                                    map.setZoom(overlay.maxZoom);
                                }
                                map.overlays[overlay.type].started = true;
                                self.getOverlayData(overlay, map);
                            }
                        });
                        // unbind events
                        self.player.$container.unbind('slidechange slide');
                        // reset slider
                        self.player.$container.slider('value', 0);
                        self.player.$container.bind('slide slidechange', function(e, ui) {
                            self.player.setActiveLabel(ui.value);
                            $.each(overlay.maps, function(index, map) {
                                var containers = getContainers(map, overlay);
                                $(containers).hide();
                                $(containers[ui.value]).show();
                            });
                        });
                        // hide temperature tiles
                        $(target + ' .leaflet-popup-pane').hide();
                    }
                },
                // temperature click
                tempClick = function (e, type, target) {
                    e.preventDefault();
                    //setTrack(e);
                    if (setActive(target)) {
                        // hide all overlays
                        $('.leaflet-tile-pane > .leaflet-layer:not(.map-layer)').addClass('hidden');
                        // show all maps
                        for(i=1; i<self.maps.length; i += 1) {
                            $(self.maps[i]._container).show();
                        }
                        // show popups
                        $('.leaflet-popup-pane').show();
                        // unbind events
                        self.player.$container.unbind('slidechange slide');
                        // reset slider
                        self.player.setVal(0);
                        //set type on slider
                        self.setType(type);
                        if (type === 'hourly') {
                            // set slider max
                            self.player.setMax(self.player.labels.hourly.dateTimes.length-1);
                            self.player.createLabels(type, 1);
                        }
                        else if (type === 'extended') {
                            // set slider max
                            self.player.setMax(self.player.labels.extended.dateTimes.length-1);
                            self.player.createLabels(type);
                        }
                        // open first set of popups
                        $.each(self.popups, function(index, popup) {
                            self.updateTempPopup(popup, type, 0);
                        });
                        // reset opacity
                        self.opacity.hide();
                        self.opacity.setVal(1);
                        // set play speed
                        self.player.playSpeed = 750;
                        // bind slider events
                        self.player.$container.unbind('slide slidechange').bind('slide slidechange', function(e, ui) {
                            self.player.setActiveLabel(ui.value);
                            // update popup info
                            $.each(self.popups, function(index, popup) {
                                self.updateTempPopup(popup, type, ui.value);
                            });
                        });
                    }
                };
                // check for temp controls
                self.hasTempControls = false;
                $.each(mapsParams, function(index, mapParams) {
                    if(typeof mapParams.data.temp === 'object') {
                        self.hasTempControls = true;
                        return false;
                    }
                });
                if(self.hasTempControls) {
                    // add temp load to q
                    self.loader.addToQ('temp');
                    // ensure the player waits for tiles to load
                    self.player.wait();
                    // create extended temperature controls
                    $controls.prepend(
                        $('<li>').addClass('ui-tab-item').append(
                           $('<a>').addClass('ui-btn')
                               .attr('href','#')
                               .attr('data-heattrack', 'weather-extended')
                               .text('Extended')
                               .click(function(e){
                                   tempClick(e, 'extended', this);
                               })
                        )
                    );
                    // create hourly temperature controls
                    $controls.prepend(
                        // mark temperature as active
                       $('<li>').addClass('ui-tab-item').append(
                            $('<a>').addClass('ui-btn active')
                               .attr('href','#')
                               .attr('data-heattrack', 'weather-hourly')
                               .text('Hourly')
                               .click(function(e){
                                   tempClick(e, 'hourly', this);
                               })
                        )
                    );
                }
                if(maps[0].hasZoomControls) {
                    $zoomControl = $('<div>').addClass('map-control zoom-control')
                            .append($('<span>').addClass('map-control-label zoom-label ui-label ui-label-left').text('Zoom'))
                            .append($('<div>').addClass('ui-zoom')
                                .append($('<a class="ui-btn minus-icon">')
                                    .attr('href','#')
                                    .attr('data-heattrack', 'weather-zoomout')
                                    .text('Zoom Out')
                                    .click(function(e){
                                        e.preventDefault();
                                        maps[0].zoomOut();
                                    })
                                )
                                .append($('<a class="ui-btn plus-icon">')
                                    .attr('href','#')
                                    .attr('data-heattrack', 'weather-zoomin')
                                    .text('Zoom In')
                                    .click(function(e){
                                        e.preventDefault();
                                        maps[0].zoomIn();
                                    })
                                )
                            );
                        }
                        // create overlay controls
                        $.each(self.overlays, function (index, overlay) {
                            if(overlay.maps.length) {
                                var link =  $('<a>').addClass('ui-btn')
                                   .attr('href','#')
                                   .attr('data-heattrack', 'weather-overlaybutton'+index)
                                   .text(overlay.text)
                                   .click(function(e) {
                                       e.preventDefault();
                                       overlayClick(e, overlay, this);
                                   });
                        $controls.append(
                            $('<li>').addClass('ui-tab-item').append(link)
                        );
                        // set initial state to the first overlay if there are no temperature layers
                        if(cnt === 0){
                            firstOverlay = link;
                        }
                        cnt += 1;
                   }
                });
                // create opacity controls
                self.opacity = createOpacityControl($wrapper);
                // add controls to DOM
                $(target).append(
                    $wrapper
                        .append($controls)
                        .append($zoomControl)
                );
                if (!self.hasTempControls) {
                    firstOverlay.click();
                }
                return $controls;
            };

            this.createSlider = function(target) {
 
                    var self = this,
                    player = {
                        labels: {},
                        playSpeed: 750,
                        isPlaying: false,
                        isWaiting: false,
                        // create labels and markers
                        createLabels: function(type, interval) {
                            // remove all markers
                            $('.marker').remove();
                            // remove all labels
                            $('.label').remove();
                            // get slider max
                            var max = player.getMax(),
                            label;
                            // create individual markers
                            for (i=0; i<=max; i += 1) {
                                var position = (i/max * 100),
                                markerTop = $('<span>').addClass('marker marker-top').css('left', position +'%'),
                                markerBottom = $('<span>').addClass('marker marker-bottom').css('left', position +'%');
                                // get printable labels
                                label = player.getPrintLabel(
                                    player.labels[type].dateTimes[i],
                                    player.labels[type].days,
                                    player.labels[type].hours,
                                    player.labels[type].shortHours,
                                    player.labels[type].dates
                                );
                                // add position to label
                                label.css('left', position +'%');
                                // add label per interval
                                if(interval !== undefined && i%interval !== 0) {
                                    label = null;
                                }
                                player.$container.append(label).append(markerTop).append(markerBottom);
                                // set active label
                                if(i === 1){
                                    player.setActiveLabel(0);
                                }
                            }
                            return player.$container;
                        },
                        getPrintLabel: function(dateTime, days, hours, shortHours, dates) {
                            var dateStrings = {
                                weekday: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                                month: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                                getWeekday: function(day) {
                                    return this.weekday[day];
                                },
                                getMonth: function(month) {
                                    return this.month[month];
                                },
                                getShortWeekday: function(day) {
                                    return this.getWeekday(day).substring(0, 3);
                                },
                                getShortMonth: function(month) {
                                    return this.getMonth(month).substring(0, 3);
                                }
                            };
                            var label = $('<div>').addClass('label'),
                            hourText = '';
                            dayText = '';
                            labelCnt = 0;
                            // print day
                            if (days) {
                                labelCnt += 1;
                                dayText  = dateStrings.getShortWeekday(dateTime.getDay());
                                label.append($('<span>').addClass('day').text(dayText));
                            }
                            // print hour
                            if (hours) {
                                labelCnt += 1;
                                // print time stamps
                                minutes = (dateTime.getMinutes() < 10 ? '0' : '') + dateTime.getMinutes();
                                if (dateTime.getHours() > 12) {
                                    hourText = (dateTime.getHours() - 12) + ':' + minutes + ' PM';
                                }
                                else if (dateTime.getHours() === 0) {
                                    hourText = "12:" + minutes + ' AM';
                                }
                                else {
                                    hourText = dateTime.getHours() + ':' + minutes + ' PM';
                                }
                                label.append($('<span>').addClass('hour').text(hourText));
                            }
                            dateText = '';
                            // print only hour
                            if (shortHours) {
                                labelCnt += 1;
                                // print time stamps
                                if (dateTime.getHours() > 12) {
                                    hourText = (dateTime.getHours() - 12) + ' PM';
                                }
                                else if (dateTime.getHours() === 0) {
                                    hourText = '12 AM';
                                }
                                else {
                                    hourText = dateTime.getHours() + ' AM';
                                }
                                label.append($('<span>').addClass('hour').text(hourText));
                            }
                            // print date
                            if (dates) {
                                labelCnt += 1;
                                dateText = dateStrings.getShortMonth(dateTime.getMonth()) + ' ' + dateTime.getDate();
                                label.append($('<span>').addClass('date').text(dateText));
                            }
                            label.addClass('label-' + labelCnt + 'up');
                            return label;
                        },
                        $container: $('<div>').addClass('timeline-belt time').slider({
                            min: 0,
                            max: 0,
                            step: 1,
                            animate: true
                        }),
                        $activeLabel: $('<div>').addClass('inner'),
                        $play: $('<a>').addClass('timeline-play-btn').attr('href','#').attr('data-heattrack', 'weather-timelineplay').click(function(e) {
                            e.preventDefault();
                            player.toggle();
                        }).append($('<span>').addClass('play').text('Play')),
                        setActiveLabel: function(value) {
                            label = player.getPrintLabel(
                                player.labels[self.type].dateTimes[value],
                                player.labels[self.type].days,
                                player.labels[self.type].hours,
                                player.labels[self.type].shortHours,
                                player.labels[self.type].dates
                            );
                            player.$activeLabel.empty().append(label);
                        },
                        moveUp: function() {
                            if(player.isPlaying && !player.isWaiting) {
                                player.setVal(player.getVal() + 1);
                                // on last slider value stop interval
                                if(player.getVal() === player.getMax()) {
    
                                    player.stop();
                                }
                                else {
                                    setTimeout(function() {
                                        player.moveUp();
                                    }, player.playSpeed);
                                }
                            }
                        },
                        wait: function() {
                            player.stop();
                            player.isWaiting = true;
                        },
                        loaded: function() {
                            self.loader.removeFromQ('player');
                            player.isWaiting = false;
                            if(player.isPlaying) {
                                player.moveUp();
                            }
                        },
                        stop: function() {
                            player.$play.empty().append($('<span>').addClass('play').text('Play'));
                            player.isPlaying = false;
                        },
                        play: function() {
                            player.$play.empty().append($('<span>').addClass('pause').text('Pause'));
                            player.isPlaying = true;
                            if (player.getVal() === player.getMax()) {
                                player.setVal(0);
                            }
                            if (player.isWaiting) {
                                self.loader.addToQ('player');
                            }
                            else {
                               player.moveUp();
                            }
                        },
                        toggle: function() {
                            if(player.isPlaying){
                                player.stop();
                            }
                            else {
                                player.play();
                            }
                        },
                        getMax: function() {
                            return player.$container.slider('option', 'max');
                        },
                        setMax: function(max) {
                            return player.$container.slider('option', 'max', max);
                        },
                        getVal: function() {
                            return player.$container.slider('value');
                        },
                        setVal: function(value) {
                            return player.$container.slider('value', value);
                        },
                        init: function(target) {
                            player.$container.children('a').addClass('timeline-buckle').append(player.$activeLabel);
                            $(target)
                                .before($('<div>').attr('id','weathermap-timeline').addClass('clearfix')
                                    .append(player.$play).append(player.$container));
                            return player;
                        }
                    };
                    return player.init(target);

            };

            this.getTempClass = function(temp) {
                temp = Math.floor(parseInt(temp, 10) / 10) * 10;
                if(temp > 100) {
                    temp = 100;
                }
                if(temp < 0) {
                    temp = 0;
                }
                return "temp" + temp + "s";
            };

            this.setLocationDetails = function(data, maps) {
                var self = this;
                if(maps === undefined) {
                    maps = self.maps;
                }
                data = data[0];
                var placeLocationOnMap = function(map) {
                    if (typeof self.location.marker !== 'undefined'){
                        self.location.map.removeLayer(self.location.marker);
                    }
                    var icon = new self.icon();
                    self.location.map = map;
                    self.location.marker = new L.Marker(new L.LatLng(self.location.latitude, self.location.longitude), {icon: icon});
                    map.addLayer(self.location.marker);
                },
                findMap = function(maps, id) {
                    var myMap;
                    // find a spacific map based on ID
                    $.each(maps, function(index, map){
                        if(map.id === id) {
                            myMap = map;
                            return false;
                        }
                    });
                    return myMap;
                },
                setMapCenter = function(map, lat, lon) {
                    map.setView(new L.LatLng(lat, lon), map.getZoom(), false);
                    // reload overlay
                    self.reloadOverlay(map);
                },
                setTempLabels = function(){
                    // get labels
                    self.player.labels.extended = {
                        dateTimes: [],
                        hours: false,
                        shortHours: false,
                        dates: true,
                        days: true
                    };
                    self.player.labels.hourly = {
                        dateTimes: [],
                        hours: false,
                        shortHours: true,
                        dates: false,
                        days: false
                    };
                    // create timestamps and labels for extended
                    $.each(data.forecast.day, function(index, day) {
                        var dateTime = new Date(day.obsdate);
                        self.player.labels.extended.dateTimes.push(dateTime);
                    });
                    // create timestamps and labels for hourly
                    $.each(data.forecast.hourly, function(index, hour) {
                        var hourStr = hour.time.toString(),
                        hourInt = hour.time;
                        // AM
                        if (hourStr.indexOf("AM") !== -1) {
                            hourInt === 12 ? hourInt += 12 : hourInt;
                        }
                        // PM
                        if (hourStr.indexOf("PM") !== -1) {
                            hourInt !== 12 ? hourInt += 12 : hourInt;
                        }
                        var d = new Date(),
                        dateTime = new Date(d.getFullYear(), (d.getMonth()+1), d.getDate(), hourInt, 0, 0, 0);
                        self.player.labels.hourly.dateTimes.push(dateTime);
                    });
                    self.player.setMax(self.player.labels[self.type].dateTimes.length-1);
                    self.player.createLabels(self.type);
                },
                setLocation = function() {
                    self.location.data = data;
                    // set coordinates
                    self.location.latitude = data.local.lat;
                    self.location.longitude = data.local.lon;
                    self.location.city = data.local.city;
                    self.location.state = data.local.adminArea;
                    var map;
                    // more then 1 map
                    if (maps.length > 1) {
                        var state = data.local.adminArea.Text[0].toLowerCase();
                        // location is in hawaii
                        if (state === 'hawaii') {
                            map = findMap(maps, 'hawaii');
                        }
                        // location is alaska
                        else if (state === 'alaska') {
                            map = findMap(maps, 'alaska');
                        }
                        // location is continental
                        else {
                            map = findMap(maps, 'continental');
                        }
                    }
                    // only one map, place marker on the first map
                    else {
                        map = maps[0];
                    }
                    placeLocationOnMap(map);
                    if (map.scrollTo) {
                        setMapCenter(map, data.local.lat, data.local.lon);
                    }
                };
                // get weather for location
                if (data !== null && data.failure === undefined) {
                    if(self.hasTempControls) {
                        setTempLabels();
                    }
                    setLocation();
                }
                else {
                    if(self.debug) {
                        console.warn("WMAP ERROR: Unable to find weather for " + location.name);
                    }
                }
            };

            // create map legends
            this.createLegends = function(target){
                // temperature legend
                var $list = $('<ul>'),
                text = '';
                for (i=0; i<=100; i+=10) {
                    text = (i===0) ? '-' + i : i+'s';
                    $list.append($('<li>').addClass('temp' + i + 's').text(text));
                }
                $(target)
                    .append($('<div>').addClass('key key-forecast')
                        .append($list)
                    )
                    .append($('<div>').addClass('key key-radar'))
                    .append($('<div>').addClass('key key-satilite'))
                    .append($('<div>').addClass('weathermap-credits')
                        .append($('<span>').addClass('credit').html('<a href="http://www.accuweather.com" target="_blank" class="logo">Accuweather</a>'))
                    );
            };

            this.init = function(params, data) {


                 var self = this;
                require(['libs/leaflet/jquery-ui-1.8.21.custom.min' ],function(){ 

                    if ($(params.target).children().length) {
                        throw new Error(params.target + " is already initialized.");
                    } else {

                        // set global params
                        self.options = params;
                        // create loader
                        self.loader.create(params.target);
                        // create slider
                        self.player = self.createSlider(params.target);
                        // create maps
                        self.maps = self.createMaps(params.target, params.maps, data);
                        // create map controls
                        self.createControls(self.maps, params.target, params.maps);
                        // create legends
                        self.createLegends(params.target);
                        // get users location and details
                        self.setLocationDetails(data, self.maps);
                        return self;
                    }
                }) // End of require function
            };
        }
   });
   return WeatherMap;
})
