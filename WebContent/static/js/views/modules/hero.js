/**
 * @fileoverview Hero module view.
 * @author Chad Shryock <cdshryock@gannett.com>
 */
define(['jquery', 'underscore', 'backbone', 'views/modules/carousel', 'views/modules/video'],
    function($, _, Backbone, Carousel, Video) {


        /**
         * View class.
         */
        var HeroView = Backbone.View.extend({

            // Events.
            events: {
                'click .slide' : 'open',
                'click .contents li' : 'openTile'
            },

            /**
             * Clean up view.
             * Removes event handlers and element (optionally).
             * @param {boolean} removeEl option to also remove View from DOM.
             */
            destroy: function(removeEl) {
                this.undelegateEvents();
                if (removeEl) {this.remove();}
            },

            /**
             * Open anchor.
             * @param {Event} event View click event.
             */
            open: function(event) {
                var $currentTarget = $(event.currentTarget);
                if ($currentTarget.find('.text.video').length == 0 && !$(event.target).is('a')) {
                    $currentTarget.find('.story > h1 > a').trigger('click');
                }
            },

            openTile: function(event){
                // ignore click events that weren't user generated
                if(event.type == 'click' && event.hasOwnProperty('originalEvent')){
                    var index = $(event.currentTarget).index();
                    this.$('.slide:eq(' + index + ') .story > h1 > a').click();
                }
            },

            /**
             * Clean up the view (no argument).
             */
            destroy: function() {
                this.undelegateEvents();
            }

        });

        /**
         * Return view class.
         */
        return HeroView;
    }
);
