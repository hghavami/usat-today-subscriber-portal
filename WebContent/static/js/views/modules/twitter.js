/**
 * @fileoverview Twitter view module.
 * @author jheiner@usatoday.com (John Heiner)
 */
 define(['jquery', 'underscore', 'backbone', 'twitter', 'pubsub', 'views/modules/sidebar-scroll', ],
    function($, _, Backbone, Twitter, PubSub, SidebarScroll ) {

        /**
         * View class.
         */
        TwitterView = Backbone.View.extend({

            scrollbar: null,
            twitter_el:null,

            /**
             * Initialize view.
             * @param {Object} options
             *     @param el {String} container element.
             *     @param num_tweets {number} the number of tweets to show.
             *     @param columns {number} Set how many columns. Defaults to 1.
             */
            initialize: function(options) {
              
                this.options = $.extend({
                    num_tweets: 6,
                    columns: 1
                }, options || {});

                this.columns = this.options.columns;
                this.num_tweets = this.options.num_tweets;
                this.twitterHandle = this.options.username;

                _.bindAll(this);

                twitter_el = $('.tweet-list', options.el);
                // check if the JSON user attr is there otherwise use current section feed
                var twitter_user,
                    _this_el = this.$el,
                    userID = twitter_el.attr('data-user-id'),
                    sectionID = "USATODAY",
                    section_from_page = twitter_el.attr('data-section-id'),
                    blog_name =$('div.stag-masthead h1').text();
                    blog_name = blog_name.replace(/\s+/g, '');
                if(section_from_page){
                  sectionID = sectionID +  section_from_page;
                }
                //Logic and fallbacks for twitter user, will try json user, than section, than USAToday users
                if (userID){
                    twitter_user = userID;
                } else if(blog_name !=""){
                    twitter_user = blog_name; //temp for blogs
                }else if (sectionID) {
                    twitter_user = sectionID;
                } else {
                    twitter_user = "USAToday";
                }

                this.twitter = new Twitter({
                    user:twitter_user,
                    tweetCount: this.num_tweets,
                    appendTo: this.el
                });
                this.twitter.loadTweets(this.createTweetsHTML);

            },

            /**
             * Clean up view.
             * Removes event handlers and element (optionally).
             * @param {boolean} removeEl option to also remove View from DOM.
             */
            destroy: function(removeEl) {
                this.undelegateEvents();
                if (removeEl) {this.remove();}
            },

            /**
             * calls the template file and fills it with the tweets
             */
            createTweetsHTML: function(tweets) {
                // Check for existence of tweet list template, and set tweet-list to current card
                var tweet_templates = $("#tweet-list-template", this.el),
                    compiled;
                if(tweet_templates.length) {
                    compiled = _.template($("#tweet-list-template", this.el).html(), {data: tweets});
                    twitter_el.html(compiled);
                    twitter_el.find('li').slice(0, this.columns).addClass('no-border');
                }
                this.addScrollbar();
            },
            /**
             * Adds scrollbar.
             */
            addScrollbar : function(){
               scrollbar = new SidebarScroll({
                   el: '.size-bar .mod.twitter .twitter-scroll-content',
                   template: '<div class="vertical-scroll ui-light"><div class="scrolldragger"></div></div>',
                   padding: 2
               });
               scrollbar.addHideClass();
            }

        });

        /**
         * Return view class.
         */
        return TwitterView;
    }
);
