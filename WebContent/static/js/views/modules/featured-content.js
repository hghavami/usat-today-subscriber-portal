/**
 * @fileoverview Featured Content module view.
 * @author Robert Huhn <rhuhn@usatoday.com>  and John Heiner <jheiner@usatoday.com>
 */
define(['jquery', 'underscore', 'backbone', 'pubsub'],
    function($, _, Backbone, PubSub) {


        /**
         * View class.
         */
        var FeaturedContentView = Backbone.View.extend({

            // Events.
            events: {
                'click .indicator span' : 'scroll'
            },

            /**
             * Initialize view.
             * @param {Object} options View options passed in during init.
             */
            initialize: function(options) {
                _.bindAll(this, 'starttimer', 'stoptimer');

                this.options = $.extend({
                    itemWidth: 180,
                    itemsPerView: 4,
                    transition : {
                        interval : 15000
                    }
                }, options || {});

                this.$mod = $(options.el, document);
                FeaturedContentView.moveMe = $('.featured-content ul', this.$mod);
                this.$paginator = this.$('.featured-content .paginator');
                this.$paginator.on('mouseenter.' + this.cid, this.stoptimer);
                this.$paginator.on('mouseleave.' + this.cid, this.starttimer);

                if (typeof options.transition !== 'undefined') {
                    var trans = options.transition;
                    this.interval = (trans.interval) ? trans.interval : false;
                    this.timer = null;

                    // Bind 'this' to automate function calls.
                    // Useful for context when calling with setInterval.
                    _.bindAll(this, 'automate', 'starttimer', 'stoptimer');

                    if (this.interval) {this.starttimer();}
                }
            },

            /*
             * Function to handle automated rotation of the carousel.
             */
            automate: function() {
                // Cache the selectors.
                var $active = $('.indicator .selected', this.el),
                    currentIndex = $active.index(),
                    pages = $('.indicator span', this.el),
                    numberOfItems = pages.length;

                // Triger the event on the next item in the list.
                // If on the last, trigger the first.
                if(currentIndex < numberOfItems -1){
                    pages.eq(currentIndex+1).click();
                } else {
                    pages.eq(0).click();
                }

            },

            /**
             * Clean up view.
             * Removes event handlers and element (optionally).
             * @param {boolean} removeEl option to also remove View from DOM.
             */
            destroy: function(removeEl) {
                this.undelegateEvents();
                this.$paginator.off('.' + this.cid);
                this.stoptimer();
                if (removeEl) {this.remove();}
            },

            scroll: function(event) {
                var index = $(event.currentTarget).index(),
                    offsetBy = this.options.itemWidth * this.options.itemsPerView,
                    targetOffset = offsetBy*index,
                    cssObj = {'left': '-'+targetOffset+'px'};
                $(event.currentTarget).siblings().removeClass('selected');
                $(event.currentTarget).addClass('selected');
                FeaturedContentView.moveMe.css(cssObj);

                return false;

            },
            /*
             * Start the interval that automates slide switching.
             */
            starttimer: function() {
                if (this.interval && !this.timer){
                    this.timer = setInterval(this.automate, this.interval);
                }
            },

            /*
             * Stop the interval - primarily used when hovering the module.
             */
            stoptimer: function() {
                if (this.timer){
                    clearInterval(this.timer);
                    this.timer = null;
                }
            }

        });

        /**
         * Return view class.
         */
        return FeaturedContentView;
    }
);
