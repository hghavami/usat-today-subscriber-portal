/**
 * @fileoverview Global alert view. Display loading messages, prompts,
 * confirmation messages, etc.
 * @author kris.hedstrom@f-i.com (Kris Hedstrom)
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'pubsub',
    'app',
    'easing'
],
function(
    $,
    _,
    Backbone,
    PubSub,
    App
) {

    /**
     * View class.
     */
    var Alert = Backbone.View.extend({

        el: 'body',

        // Events.
        events: {},

        /**
         * Initialize the view.
         */
        initialize: function(options) {
            
            this.options = $.extend({
                templateID: 'alert'
            }, options || {});
   
            this.render();
        },

        /**
         * Create markup for alert.
         * @TODO: separate this out to a underscore template.
         */
        render: function() {
            this.$alert = $('<div class="ui-alert"><span class="text"></span><span class="close-btn">Close</span></div>')
                            .appendTo(this.$el);
            this.$alertText = this.$alert.find('.text');
            this.$alert.on('click', '.close-btn', _.bind(function(){
                this.hidePrompt();
            }, this));
        },

        /**
         * Display alert.
         * @param {String} msg The HTML to display.
         */
        show: function(msg) {
            PubSub.trigger('show:alert');
            this.$alertText.html(msg);
            this.$alert.addClass('active').removeClass('error');
        },

        /**
         * Hide the alert.
         */
        hide: function() {
            PubSub.trigger('hide:alert');
            this.$alert.removeClass('active').removeClass('error');
        },

        /**
         * Display error message.
         * @param {String} msg The HTML to display.
         */
        showError: function(msg) {
            PubSub.trigger('show:error:alert');
            this.$alertText.html(msg);
            this.$alert.addClass('active').addClass('error');
        },

        showPrompt: function(msg, confirmCallback, cancelCallback) {

            /**
             * Confirmation event handler. Stops default events and calls
             * confirmation callback.
             * @private
             */
            function _confirm(e) {
                e.preventDefault();
                e.stopPropagation();
                if(typeof confirmCallback === 'function') confirmCallback();
            }
            
            /**
             * Cancel handler. Stops default events and calls cancel callback.
             * @private
             */
            function _cancel (e) {
                e.preventDefault();
                e.stopPropagation();
                if(typeof cancelCallback === 'function') cancelCallback();
            }
            
            // @TODO: make this more flexible.
            var confirm = $('<a/>', {
                'html': 'Okay'
            });

            var cancel = $('<a/>', {
                'html': 'Cancel'
            });

            var txt = $('<div/>', {
                'class': 'prompt',
                'html': msg
            });

            txt.append(confirm).append(cancel);
            this.$alert.addClass('active').html(txt);

            confirm.on('click', _confirm);
            cancel.on('click', _cancel);

        },

        /**
         * Hide the confirmation prompt.
         */
        hidePrompt: function() {
            this.$alert.removeClass('active');
        }

    });

    /**
     * Return singleton.
     */
    return new Alert();
});
