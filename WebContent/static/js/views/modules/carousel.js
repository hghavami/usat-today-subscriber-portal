/**
 * @fileoverview Global carousel module view.
 * @author jhensley@gannett.com (Jonathan Hensley)
 * @author cwmanning@usatoday.com (Chris Manning)
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'pubsub',
    'app',
    'fwinfo',
    'views/modules/fullscreen',
    'views/modules/vertical-scroll',
    'views/modules/gallery-thumb-scroll',
    'fw/views/bannerview'
    ],
    function(
        $,
        _,
        Backbone,
        PubSub,
        App,
        FwInfo,
        FullScreen,
        VerticalScroll,
        GalleryThumbScroll,
        BannerView
    ) {

        /**
         * View class.
         */
        var CarouselView = Backbone.View.extend({

            // Events.
            events: {
                'click .fullscreen': 'fullscreen',
                'click .prev' : 'prevSlide',
                'click .next' : 'nextSlide',
                'click .slide img' : 'nextSlide',
                'click .contents li' : 'switchSlide',
                'click .thumbs li' : 'switchSlide',
                'click .replay': 'nextSlide',
                'click .autoplay': 'autoplay',
                'click .captions': 'captionsToggle',
                'click .thumbnails': 'thumbsToggle',
                'mouseenter #media-view .viewport' : 'thumbsToggle',
                'mouseenter #media-view .thumbs' : 'checkThumbs',
                'mouseleave #media-view .thumbs' : 'thumbsToggle',
                'mouseleave #media-view .viewport' : 'thumbsToggle',
                'mouseenter' : 'stoptimer',
                'mouseover .viewport, .slide-nav, .videoObject, .hero .media' : 'mouseover',
                'mouseout .viewport, .slide-nav, .videoObject, .hero .media' : 'mouseout',
                'mouseenter .contents li' : 'thumbMouseover',
                'mouseleave .contents li' : 'thumbMouseout'
            },

            /**
             * Initialize view.
             * @param {Object} options View options passed in during init.
             */
            initialize: function(options) {
                this.options = $.extend({
                    ads : false,
                    color: 'white' // white, black
                }, options || {});

                this.index = 0;

                this.cacheSelectors(options.el);
                this.fullScreen = (options.fullScreen) ? options.fullScreen : false;
                //Tracking on hold for FW
                //this.tracking = (options.track) ? options.track : false;

                this.isRotator = $(this.el).hasClass('rotator');

                this.timer = null;
                if (typeof options.transition !== 'undefined') {
                    var trans = options.transition;
                    this.interval = (trans.interval) ? trans.interval : false;

                    // Bind 'this' to automate function calls.
                    // Useful for context when calling with setInterval.
                    _.bindAll(this, 'automate', 'starttimer', 'stoptimer');

                    if (this.interval) {this.starttimer();}
                }

                this.showNav();
                this.setTicker();

                // Initialize the vertical scroll
                var vscrollbar = new VerticalScroll({
                    el: this.$el.find('.thumbs')
                });

                // Initialize the horizontal scroll
                this.hscrollbar = new GalleryThumbScroll({
                    el: this.$el.find('.thumbs'),
                    color: options.color
                });

                // Listen for pause from hero flip ad
                this.pubSub = {
                    'carousel:stoptimer': this.stoptimer,
                    'carousel:starttimer': this.starttimer
                };
                PubSub.attach(this.pubSub, this);

                if (this.options.ads) {
                    this.loadAd();
                }
            },

            /*
             * Function to handle automated rotation of the carousel.
             */
            automate: function() {
                // Cache the selectors.
                var $active = $('.slide.active', this.el);

                // Get the details of what we're automating.
                var currentIndex = $active.index(),
                    numberOfItems = this.$slides.length - 1;

                // Trigger the event on the next item in the list.
                // If on the last, trigger the first.
                if ($('.gallery').hasClass('fullscreen')) {
                    if(currentIndex < numberOfItems - 1){
                        this.$thumbs.eq(currentIndex+1).click();
                    } else {
                        if (currentIndex === numberOfItems - 1) {
                            this.$('.pause').click();
                        }
                        this.$('.next').click();
                    }
                } else {
                    if (currentIndex < numberOfItems) {
                        this.$thumbs.eq(currentIndex+1).click();
                    } else {
                        if (currentIndex === numberOfItems) {
                            this.$('.pause').click();
                        }
                        this.$('.next').click();
                    }
                }
            },

            /*
             * Function to cache high usage css selectors.
             * @param {string} element View el selector
             */
            cacheSelectors: function(element) {
                this.setElement(element);
                this.$adSlide = $('.ad-slide', this.el);
                this.$thumbs = $('.contents li, .thumbs li', this.el);
                this.$slides = $('.slide', this.el);
            },

            /**
             * Clean up view.
             * Removes event handlers and element (optionally).
             * @param {boolean} removeEl option to also remove View from DOM.
             */
            destroy: function(removeEl) {
                this.undelegateEvents();
                this.stoptimer();
                this.hscrollbar.killSliding();
                this.hscrollbar.destroy();
                PubSub.unattach(this.pubSub, this);
                if (removeEl) {this.remove();}
            },

            /*
             * Launch fullscreen view
             * @param {Event} event View click event.
             */
            fullscreen: function() {
                if (this.fullScreen) {
                    var _new = $.extend(true, {}, this);
                    var fullScreenView = new FullScreen({
                        carousel: _new,
                        parent: this.el,
                        track: this.tracking
                    });
                    this.fullscreen = true;
                }
                return false;
            },

            /*
             * Navigate to previous slide.
             * @param {Event} event View click event.
             */
            prevSlide: function(event) {
                this.switchSlide(event, -1);
                this.hscrollbar.updateThumbsPosition();
                if(this.fullScreen) {$(this.el).trigger('updateScroller');}
                return false;
            },

            /*
             * Toggle hover classes.
             * @param {Event} event View element mouseover event.
             */
            mouseover: function(event) {
                if (!$(event.currentTarget).hasClass('slide-nav')) {
                    this.$el.find('.viewport').addClass('hover');
                }
                this.$el.addClass('hover');
            },

            /*
             * Toggle hover classes.
             * @param {Event} event View element mouseout event.
             */
            mouseout: function(event) {
                if (!$(event.currentTarget).hasClass('slide-nav')) {
                    this.$el.find('.viewport').removeClass('hover');
                }
                this.$el.removeClass('hover');
            },

            /*
             * Navigate to next slide.
             * @param {Event} event View click event.
             */
            nextSlide: function(event) {

                this.switchSlide(event, 1);
                this.hscrollbar.updateThumbsPosition();

                if(this.fullScreen) {$(this.el).trigger('updateScroller');}
                return false;
            },

            /**
             * Update slide ticker (Image 1 of 9).
             */
            setTicker: function() {
                // Cache the selectors.
                var $active = $('.slide.active', this.el);

                // Get the details of what we're setting.
                var currentIndex = $active.index(),
                    numItems;

                    if (this.$el.find('.endslate')) {
                        numItems = (this.$slides.length - 1);
                    } else {
                        numItems = this.$slides.length;
                    }

                // Iterate the html for the ticker full screen element (current slide / total slides)
                this.$el.find('.ticker:first-child').html('<b>' + (currentIndex + 1) + '</b> of <b>' + numItems + '</b>');
            },

            /**
             * Initially expose next/prev navigation.
             */
            showNav: function() {
                var _this = this;
                setTimeout(function() {
                    _this.$el.find('.slide-nav').trigger('mouseover');
                }, 600);

                setTimeout(function() {
                    _this.$el.find('.slide-nav').trigger('mouseout');
                }, 3600);
            },

            /*
             * Start the interval that automates slide switching.
             */
            starttimer: function() {
                if (this.interval) {
                    this.timer = setInterval(this.automate, this.interval);
                }
            },

            /*
             * Stop the interval - primarily used when hovering the module and hero flip
             */
            stoptimer: function() {
                clearInterval(this.timer);
            },

            /*
             * Switch from one slide to another.
             * @param {Event} event View click event.
             * @param {number} slideOffset View slide position.
             */
            switchSlide: function(event, slideOffset) {
                var index = $(event.currentTarget).index(),
                    $contents = $('.contents', this.el),
                    newIndex;

                if (slideOffset) {
                    newIndex = this.index + slideOffset;
                    index = (newIndex > 0 && newIndex < this.$slides.length) ? newIndex : 0;
                }

                // Older browser transition fall-back
                // Set opacity for slides - fade out

                /*
                if (!Modernizr.csstransitions) {
                    $active.animate({
                        opacity: 0
                    }, 200);
                    // Set opacity for new slide - fade in
                    $($slides[index]).animate({
                        opacity: 1
                    }, 200);
                }
                */

                // clean up the ad
                if (this.$adSlide.hasClass('active')) {
                    this.$adSlide.removeClass('active');
                    PubSub.trigger('partner:' + this.$adSlide.attr('data-code-id') + ':close',_.bind(function() {
                        this.loadAd();
                    }, this));
                }

                // Remove the active class from all elements.
                this.$slides.removeClass('active');
                this.$thumbs.removeClass('active');

                // Reset the next/prev arrows and the ticker
                this.$el.find('.slide-nav.prev').css('display','block');
                this.$el.find('.slide-nav.next').css('display','block');
                this.$el.find('.ticker').css('display','block');

                if (this.checkAndProcessAd()) {
                    return false;
                }

                // Add the active class to the currently selected
                // jQuery is forcing the double $
                $(this.$thumbs[index]).addClass('active');
                $(this.$slides[index]).addClass('active');

                if (this.fullScreen) {
                    this.setTicker();
                }

                // Move the external indicator if it is a rotator
                if (this.isRotator) {
                    $contents.removeClass('position-1').removeClass('position-2').removeClass('position-3').removeClass('position-4').removeClass('position-5').removeClass('position-6').addClass('position-' + (index + 1));
                }

                if (this.interval) {
                    PubSub.trigger('carousel:switchSlide');
                }

                this.index = index;

                // Cache the selectors.
                var $active = $('.slide.active', this.el);

                // Check for active slide.
                if ($active.index()===0) {
                    this.$el.find('.slide-nav.prev').css('display','none');
                }

                // Endslate check to hide ticker.
                else if ($active.hasClass('endslate')) {
                    this.$el.find('.ticker, .slide-nav.next').css('display','none');
                }

                // If none of the above, show ticker and the next/previous arrows.
                else {
                    this.$el.find('.ticker, .slide-nav').css('display','block');
                }
                this.slide = $(this.$slides[index]);
                this.trackSlideChange(event, this.slide);

                PubSub.trigger('livefeed:hide');

                return false;
            },

            loadAd: function() {
                var cst = this.$el.data('cst') || '';
                var $div = $('.ad-slide', this.$el);
                BannerView.preloadAd($div, cst);
            },

            checkAndProcessAd: function() {
                if (this.options.ads && FwInfo.slideTransition()) {
                    // Need an ad shown
                    if (this.$adSlide.attr('data-is-active') == 'true') {
                        this.$adSlide.addClass('active');
                        BannerView.showPreloadedAd(this.$adSlide.attr('data-code-id'));
                        return true;
                    }
                }
                return false;
            },

            trackSlideChange: function(event, slide){
                //switch slide gallery info object
                this.galleryInfObj={};
                this.galleryID = $(this.el).attr('data-gallery-id');
                this.galleryTitle = $(this.el).attr('data-title');
                this.slideID = slide.find('img, div.media img').attr('data-id');
                this.ssts = $(this.el).attr('data-ssts');
                //gallery info object, hero galleries get their layout name instead of gallery id as they do not have an id
                this.galleryInfObj = {
                    'gallery_id': this.galleryID,
                    'slide_id': this.slideID,
                    'ssts': this.ssts,
                    'contenttype': App.get('contenttype'),
                    'fullscreen' : this.fullscreen
                };

                //checks if event was user generated and sends galleryInfoObj to slide:change
                if(event.type == 'click' && event.hasOwnProperty('originalEvent')){
                    _.debounce(PubSub.trigger('slide:change', this.galleryInfObj), 100);
                }
            },

            thumbMouseover: function(event) {
                if (this.options.hoverTransition){
                    this.hoverTimeout = setTimeout(_.bind(function(){
                        this.hoverTimeout = null;
                        $(event.target).closest('li').click();
                    }, this), this.options.hoverTransition);
                }
                if (this.isRotator) {
                    var $item = $(event.currentTarget),
                        $activeIndicator = $('.active-indicator', this.el);

                    $('li:eq(' + $item.index() + ')', $activeIndicator).addClass('hover');
                }
            },

            thumbMouseout: function(event) {
                if (this.options.hoverTransition){
                    if (this.hoverTimeout){
                        clearTimeout(this.hoverTimeout);
                    }
                }
                if (this.isRotator) {
                    var $item = $(event.currentTarget),
                        $activeIndicator = $('.active-indicator', this.el);

                    $('li:eq(' + $item.index() + ')', $activeIndicator).removeClass('hover');
                }
            },

            /**
             * Start autoplay.
             * @param {Event} e Browser event.
             */
            autoplay: function(event) {
                var playBtn = this.$el.find('.playbtn');
                if (playBtn.hasClass('pause')) {
                    playBtn.removeClass('pause').html('Autoplay');
                    this.autostop();
                }
                else {
                    var auto = _.bind(this.automate, this);
                    this.timer = setInterval(auto, 5000);
                    playBtn.addClass('pause').html('Pause');
                    // this.showEndSlate();
                }
                return false;
            },

            /**
             * Stop autoplay.
             * @param {Event} e Browser event.
             */
            autostop: function(event) {
                if (this.timer) clearInterval(this.timer);
                return false;
            },

            /**
             * Toggle on/off captions
             * @param {Event} e Browser event.
             */
            captionsToggle: function(event) {
                var meta = this.$el.find('.meta');
                var label = this.$el.find('.captionLabel');
                var thumbs = this.$el.find('.thumbs');
                if (meta.hasClass('on')) {
                    meta.removeClass('on');
                    label.html('Show Captions');
                    if (thumbs.hasClass('on')) meta.removeClass('thumbsoff');
                }
                else {
                    meta.addClass('on');
                    label.html('Hide Captions');
                    if (thumbs.hasClass('on')) {
                        meta.removeClass('thumbsoff');
                    } else {
                        meta.addClass('thumbsoff');
                    }
                }
                return false;
            },

            /**
             * Toggle on/off thumbnails.
             * @param {Event} e Browser event.
             */
            checkThumbs: function(event) {
                var thumbs = this.$el.find('.thumbs');
                    thumbs.addClass('on');
            },

            /**
             * Toggle on/off thumbnails.
             * @param {Event} e Browser event.
             */
            thumbsToggle: function(event) {
                var meta = this.$el.find('.meta'),
                    thumbs = this.$el.find('.thumbs'),
                    thumbLabel = this.$el.find('.thumbLabel');
                if (thumbs.hasClass('on') || event.type === 'mouseleave') {
                    thumbs.removeClass('on');
                    thumbLabel.html('Show Thumbnails');
                    if (meta.hasClass('on')) meta.addClass('thumbsoff');
                }
                else {
                    thumbs.addClass('on');
                    thumbLabel.html('Hide Thumbnails');
                    if (meta.hasClass('on')) meta.removeClass('thumbsoff');
                }
                return false;
            }

        });


        /**
         * Return view class.
         */
        return CarouselView;
    }
);
