/**
 * @fileoverview Cards View.
 * @author jmerrifiel@gannett.com (Jay Merrifield)
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'pubsub',
    'app',
    'baseview',
    'views/pages/home',
    'views/pages/generic-section',
    'views/pages/weather',
    'views/pages/sports',
    'views/modules/alert',
    'state'
],
function(
    $,
    _,
    Backbone,
    PubSub,
    App,
    BaseView,
    HomeView,
    GenericSectionView,
    WeatherView,
    SportsView,
    Alert,
    StateManager
)
    {
        /**
         * View class.
         */
        var CardView = BaseView.extend({
            // View element.
            el: '#cards',

            events: {'click .open-sidebar': 'openSidebar',
                'click .close-sidebar': 'closeSidebar'
            },

            // Instance variables
            cards: [
                'home',
                'news',
                'sports',
                'life',
                'money',
                'tech',
                'travel',
                'opinion',
                'weather'
            ],

            initialize: function(options){
                options = $.extend(true, {
                    animations: {
                        horizontal: {
                            duration: 350,
                            easing: 'cubic-bezier(0.645, 0.045, 0.355, 1.000)'
                        },
                        vertical: {
                            duration: 400,
                            easing: 'cubic-bezier(0.645, 0.045, 0.355, 1.000)'
                        },
                        fadeIn: {
                            duration: 200
                        },
                        sidebar: {
                            button: {
                                fadeIn: 200,
                                fadeOut: 200
                            },
                            slideIn: 250,
                            slideOut: 250
                        }
                    },
                    UPDATE_FREQUENCY: 900000, // Time in ms before checking for new data.
                    keyboard: false,
                    peekWidth: 40,
                    minCardWrapWidth: 1024 - 120,
                    template:
                        '<section class="card-wrap offscreen">' +
                            '<div class="partner-placeholder pushdown" data-position="pushdown"></div>' +
                            '<section class="card <%= section %>" data-section-id="<%= section %>" id="section_<%= section %>">' +
                            '<div class="card-loading size-suspender"><div class="loadingText">Loading...</div></div>' +
                            '<div class="sh_bottom"></div>' +
                            '</section>' +
                            '</section>'
                }, options);

                _.bindAll(this);

                this.$body = App.get('body');
                this.$top = App.get('scrollEl');
                this.$doc = App.get('doc');
                this.$window = App.get('win');
                this.template = _.template(options.template);

                this.isNarrowScreen = this.$window.width() <= 1280;

                this.pubSub = {
                    'pushdown:cards': _.bind(this.adjustTopMargin, this, '615px'),
                    'pullup:cards': _.bind(this.adjustTopMargin, this, '0'),
                    'timer:tick:app':_.bind(this.onTimeTick, this),
                    'partner:heroflip:open': this.hideSidebarOpenButton,
                    'partner:heroflip:close': this.delayShowSidebarOpenButton,
                    'sidebar:hide': this.closeSidebar,
                    'sidebar:show': this.openSidebar
                };
                var throttledResize = _.throttle(_.bind(this.handleResizeWindow, this), 50);
                this.$window.on('resize.' + this.cid, throttledResize);
                if(this.options.keyboard) {
                    // Keyboard navigation.
                    this.$doc.on('keydown.' + this.cid, $.proxy(this.keyNavigation, this));
                }
                this.topAdjusted = false;
                this.domInitialized = false;

                // call base class initialize
                BaseView.prototype.initialize.call(this, options);

            },

            destroy: function(removeEl){
                this.$window.off('.' + this.cid);
                this.$doc.off('.' + this.cid);

                // call base class destroy
                BaseView.prototype.destroy.call(this, removeEl);
            },

            destroyModules: function(){
                if (this.topAdjusted){
                    this.adjustTopMargin(0);
                }
                BaseView.prototype.destroyModules.call(this);
            },

            getTransitionAnimation: function(fromPath, toPath, requestPromise){
                var sectionName = this._getSectionPath(toPath);
                return this.goTo(sectionName);
            },
            beforeReveal: function(fromPath, toPath, htmlFrag, paused){
                var sectionName = this._getSectionPath(toPath);
                this.updateNavigation(sectionName);
                if (paused){ // we're being preloaded, we need the asset collection
                    // Update asset collection nav.
                    var $card = htmlFrag.find('section#section_' + toPath);
                    App.setCollection($card);
                }
                App.set('currentCardPath', toPath);
            },
            afterReveal: function(fromPath, toPath, paused){
                if (!this.domInitialized){
                    this.initializeDom();
                    this.domInitialized = true;
                    this.handleResizeWindow();
                }
                this.isSidebarOpen = undefined;
                // add sidebar to new card
                this.addSidebar();

                if (!paused){
                    this.subviews.view = this._getViewMethod(toPath);
                    this.subviews.view.render();
                }

                PubSub.trigger('cards:loaded');

                //after card has loaded, resize sidebar dom elements
                this.resetSidebar();
            },
            beforeViewRemove: function(fromPath, toPath){
                $('#nav > ul .text').removeClass('active');
            },
            animateNewContent: function(htmlFrag){
                if (this.domInitialized){
                    htmlFrag = htmlFrag.find('section.card-wrap').children('.card');
                    this.swapCards(this.$currentCard, htmlFrag);
                    this.$currentCard = htmlFrag;
                }
            },

            swapCards: function(fadeOut, fadeIn){
                fadeIn.css({'z-index': 0, margin: 0, 'left': fadeOut[0].style['left']});
                fadeOut.css({'position': 'absolute', 'z-index': 1, opacity: 1});
                fadeIn.insertAfter(fadeOut);
                this.animate(fadeOut, 'opacity', 0, this.options.animations.fadeIn.duration).done(function(){
                    fadeOut.remove();
                });
            },

            initializeDom: function(){
                // Cache selectors
                this.$container = this.$('.card-container');
                this.$currentWrapper = this.$container.find('.card-wrap');
                this.$currentCard = this.$currentWrapper.find('.card');
                this.$prevBtn = this.$('#prevSection');
                this.$nextBtn = this.$('#nextSection');

                // Get the current id.
                this.currentId = this.$currentCard.data('section-id') || 'home';

                // Get current index.
                this.currentIndex = $.inArray(this.currentId, this.cards);

                // Create cards.
                this.createCards();
                this.reorder(true);

                this.addSidebar();
            },

            handleResizeWindow: function(){
                if (this.domInitialized){
                    var windowWidth = this.$window.width();
                    this.cardWidth = this.$currentCard.width();
                    this.cardWrapWidth = this.cardWidth + (windowWidth - 2 * this.options.peekWidth - this.cardWidth) / 2;
                    if (this.cardWrapWidth < this.options.minCardWrapWidth){
                        this.cardWrapWidth = this.options.minCardWrapWidth;
                    }
                    this.leftMargin = ((windowWidth - this.cardWrapWidth) / 2) + 0.5 >> 0; // round up
                    PubSub.trigger('card:position:leftMargin', this.leftMargin);
                    this.$cards.css({'width': this.cardWrapWidth});
                    this.$cards.children('.card').css({'left': (this.cardWrapWidth - this.cardWidth) / 2,
                                                        'margin': 0});
                    this.reorder();
                    this.resetSidebar();
                }
            },

            /**
             * Create the markup for the cards / sections.
             * @param {Function} callback Call this function when the cards
             *      have been created.
             */
            createCards: function() {
                this.$cards = $([]);
                for(var i = 0, l = this.cards.length; i < l; i++) {
                    // Don't need to create the current index card.
                    if(i === this.currentIndex) {
                        this.$currentWrapper.attr('data-order', 0);
                        this.$cards.push(this.$currentWrapper[0]);
                    } else {
                        // Create card.
                        var $card = this.createAndInitializeEmptyCard(this.cards[i]);
                        this.$cards.push($card[0]);
                        this.$container.append($card);

                        $card.attr('data-order', i - this.currentIndex);
                    }
                }
            },

            /**
             * Handler called on global timer tick event.
             */
            onTimeTick: function() {
                // Check if we need to update.
                this.checkForNewData();
            },

            checkForNewData: function(){
                // Get the timestamp on the current card.
                var currentCard = this.$currentCard;
                var lastUpdate = currentCard.attr('data-last-update');
                if(this.compareTime(lastUpdate, this.options.UPDATE_FREQUENCY)) {
                    var path = currentCard.attr('data-section-id');

                    // If this card is a subsection, update the path.
                    if (currentCard.attr('data-subsection-id').length > 0 &&
                        currentCard.attr('data-subsection-id') != 'None') {
                        path = path + '/' + currentCard.attr('data-subsection-id');
                    }

                    var xhr = StateManager.fetchHtml(path);
                    // this request will get canceled if the user navigates to a new section
                    xhr.done(_.bind(function(htmlFrag){
                        var section = htmlFrag.find('section.card-wrap section');
                        var newTimeStamp = section.attr('data-last-update');
                        if(newTimeStamp > lastUpdate) {
                            this.promptPageReload(path, htmlFrag);
                        }
                    }, this));
                }
            },

            /**
             * Compare a time string with the current datetime. If it
             * @param {String} timeStr The time string to compare with.
             * @param {String} compareWith Timestamp to compare with.
             * @return {Boolean} Returns true if timeStr is new.
             */
            compareTime: function(timeStr, compareWith) {
                var timeNow = (new Date()).getTime();
                var timeThen = parseInt(timeStr, 10) * 1000; // get it to ms.
                return ((timeNow - timeThen) > compareWith);
            },

            /**
             * Display a reload message if user has been idle and there's new
             * content to display.
             * @param {String} path The path / section id.
             */
            promptPageReload: function(path, htmlFrag) {
                Alert.showPrompt(
                    'Hello. There is new content available. ' +
                        'Would you like to load it? ',
                    $.proxy(function() {
                        Alert.hidePrompt();
                        this.destroyModules();
                        this.render(path, htmlFrag);
                        this.afterReveal(null, path);
                    }, this),
                    $.proxy(function() {
                        Alert.hidePrompt();
                    }, this)
                );
            },

            /**
             * Update the top navigation and jump nav states.
             * @param {String} path Path of the currently active section.
             */
            updateNavigation: function(path) {
                var jumpNav = $('nav.quick ul li');
                var mainNav = $('#nav > ul .text');

                if(path === '') {
                    path = 'home';
                }
                // Update jump nav.
                jumpNav.removeClass('active');
                jumpNav.filter('.' + path).addClass('active');

                // Update main nav.
                mainNav.removeClass('active');
                mainNav.filter('.' + path).addClass('active');
            },

            _getViewMethod: function(toPath){
                var sectionPath = this._getSectionPath(toPath);
                switch(sectionPath) {
                    case '':
                    case 'home':
                        return new HomeView('home', 'home');
                    case 'weather':
                        return new WeatherView(toPath, sectionPath);
                    case 'sports':
                        return new SportsView(toPath, sectionPath);
                    default:
                        return new GenericSectionView(toPath, sectionPath);
                }
            },

            /**
             * Update previous and next buttons.
             * @param {String} previous Path to previous card.
             * @param {String} next Path to the next card.
             */
            updateBtns: function(previous, next) {
                var previous = (previous === 'home') ? '/' : '/' + previous + '/';
                var next = (next === 'home') ? '/' : '/' + next + '/';
                this.$prevBtn.attr('href', previous);
                this.$nextBtn.attr('href', next);
            },

            /**
             * Reorder the cards based on the current index.
             * @param {boolean=} onResize Whether this is being called on resize
             *     so we know not to trigger the main.js onreorder call.
             */
            reorder: function(fadeIn) {
                var numCards = this.$cards.length;
                var cardSplit = (numCards / 2) >> 0;
                var $previous;
                var $next;
                for(var i = 0; i < numCards; i++) {
                    var $card = this.$cards.eq(i);
                    var position = i - this.currentIndex;
                    if (Math.abs(position) > cardSplit){
                        if (position > 0){
                            position = position - numCards;
                        }else{
                            position = position + numCards;
                        }
                    }
                    $card.attr('data-order', position);
                    this._positionCard($card, position);

                    if (i === this.currentIndex){
                        this.$currentWrapper = $card.removeClass('offscreen');
                        this.$currentCard = this.$currentWrapper.children('.card');
                    }else{
                        $card.show();
                        $card.addClass('offscreen');
                        if (position === -1){
                            $previous = $card;
                        }else if (position === 1){
                            $next = $card;
                        }
                    }
                }
                if (fadeIn){
                    $previous.hide().fadeIn(500);
                    $next.hide().fadeIn(500);
                }

                // kill the animation by setting duration to 0
                // so we can readjust the containers
                this.$container.css("margin-top", 0);
                this.animate(this.$container, 'left', '0px', 0);

                this.updateBtns(
                    $previous.children('.card').data('section-id'),
                    $next.children('.card').data('section-id')
                );

                this.resetSidebar();
            },


            /**
             * Keyboard navigation (left / right arrows).
             * @param {Event} e Keyboard event.
             */
            keyNavigation: function(e) {
                switch(e.keyCode) {
                    // Left arrow.
                    case 37:
                        this.previous();
                        break;
                    // Right arrow.
                    case 39:
                        this.next();
                        break;
                    default:
                        break;
                }
            },

            /**
             * Check if index is a) numeric and b) within bounds.
             * @param {Number} n Number to compare.
             * @param {Number} l Max length.
             */
            inBounds: function(n, l) {
                return (!isNaN(parseFloat(n, 10)) &&
                    isFinite(n) && n >= 0 && n <= (l - 1));
            },

            /**
             * Animate to a card.
             * @param {String} targetId The id of the target card.
             */
            goTo: function(targetId) {
                // Target index.
                var targetIndex = $.inArray(targetId, this.cards);

                // Check the bounds.
                if(!this.inBounds(targetIndex, this.cards.length)) return;
                if (targetIndex === this.currentIndex) {
                    return;
                }

                // Ready to animate.
                var targetPosition = this.prepareOffscreenCardsForAnimation(targetIndex);
                var originalIndex = this.currentIndex;
                this.currentIndex = targetIndex;

                var time = this.options.animations.horizontal.duration;
                var easing = this.options.animations.horizontal.easing;
                var containerPosition = ((-1 * targetPosition * this.cardWrapWidth) + 'px');

                var promise = this.animate(this.$container, 'left', containerPosition, time, easing);
                promise.done($.proxy(function(){
                    this.$top.scrollTop(0);

                    // destroy the html of the original card to save memory and speed up the browser
                    var $blankCard = this.createAndInitializeEmptyCard(this.cards[originalIndex]);
                    $blankCard = $blankCard.children('.card');
                    var fadeOut = this.$cards.eq(originalIndex).children('.card');
                    this.swapCards(fadeOut, $blankCard);

                    // set everything to it's correct value
                    this.reorder();
                }, this));
                return promise;
            },

            createAndInitializeEmptyCard: function(section){
                var $blankCard = $(this.template({section: section}));
                if (this.domInitialized){
                    $blankCard.css({'width': this.cardWrapWidth});
                    $blankCard.children('.card').css({'margin': 0, 'left': (this.cardWrapWidth - this.cardWidth) / 2});
                }
                return $blankCard;
            },

            prepareOffscreenCardsForAnimation: function(targetIndex){
                var targetCard = this.$cards.eq(targetIndex);
                var targetPosition = parseInt(targetCard.attr('data-order'), 10);
                // we need to text the card on the edge to see if the user has picked the last card
                // in the carousel and we need to rotate the carousel a little behind the scenes
                if (targetPosition > 0){
                    var nextCard = this._getNextCard(targetPosition);
                    /* A diagram is necessary to explain the following code
                     we want to end up with the following transition
                     Card Peek Peek Target
                     if the target position is > 3, we have something like this
                     Card RPeek Extra...Extra LPeek Target
                     So we want to hide the extra cards and push the LPeek over
                     */
                    if (targetPosition > 3){
                        // move right peeking card it's optimal position
                        this._positionCard(nextCard, 4);
                        // move the target card to optimal position
                        this._positionCard(targetCard, 3);
                        // move left peeking card it's optimal position
                        var prevCard = this._getPreviousCard(targetPosition);
                        this._positionCard(prevCard, 2);
                        // iterate over each intermediate cards until we reach the right peek card (1)
                        var i = targetPosition - 1;
                        do
                        {
                            prevCard = this._getPreviousCard(i).hide();
                            i--;
                        }while (i > 2);
                        targetPosition = 3; // this is the ideal target position
                    }else if (parseInt(nextCard.attr('data-order'), 10) < 0){
                        // the right peeking card is in the wrong position
                        this._positionCard(nextCard, targetPosition + 1);
                    }
                }else{
                    var prevCard = this._getPreviousCard(targetPosition);
                    // See comments above for explanation of this section of code
                    if (targetPosition < -3){
                        // move the left peeking card to it's optimal position
                        this._positionCard(prevCard, -4);
                        // move the target card to it's optimal position
                        this._positionCard(targetCard, -3);
                        // move the right peeking card to it's optimal position
                        var nextCard = this._getNextCard(targetPosition);
                        this._positionCard(nextCard, -2);
                        // iterate over each intermediate cards until we reach the left peek card (-1)
                        var i = targetPosition + 1;
                        do
                        {
                            nextCard = this._getNextCard(i).hide();
                            i++;
                        }while (i < -2);
                        targetPosition = -3; // this is the ideal target position
                    }else if (parseInt(prevCard.attr('data-order'), 10) > 0){
                        // the right peeking card is in the wrong position
                        this._positionCard(prevCard, targetPosition - 1);
                    }
                }
                //return target position
                return targetPosition;
            },

            /**
             * Get the next card in dom order, handling looping around the carousel
             * @param targetOrder {Integer} order position we want the next order for
             * @return the next card in the carousel
             * @private
             */
            _getNextCard: function(targetOrder){
                var item = this.$cards.filter('[data-order=' + (targetOrder + 1) + ']');
                if (!item.length){
                    item = this.$cards.filter('[data-order=' + (targetOrder * -1) + ']');
                }
                return item;
            },

            /**
             * Get the previous card in dom order, handling looping around the carousel
             * @param targetOrder {Integer} order position we want the previous order for
             * @return the previous card in the carousel
             * @private
             */
            _getPreviousCard: function(targetOrder){
                var item = this.$cards.filter('[data-order=' + (targetOrder - 1) + ']');
                if (!item.length){
                    item = this.$cards.filter('[data-order=' + (targetOrder * -1) + ']');
                }
                return item;
            },

            _positionCard: function(targetDom, targetPosition){
                targetDom.css('margin-left', ((this.leftMargin + (targetPosition) * this.cardWrapWidth)) + 'px');
            },

            /**
             * Adjust top margins, for ads.
             * @param {String} top The css value to adjust the top margin to.
             */
            adjustTopMargin: function(top, duration) {
                var duration = duration || this.options.animations.vertical.duration;
                if (top){
                    this.topAdjusted = true;
                }else{
                    this.topAdjusted = false;
                }
                this.animate(this.$currentCard, 'margin-top', top, duration).done(
                    _.bind(function() {
                        PubSub.trigger('complete:adjsttopmargin:cards');
                    }, this)
                );
            },

            _getSectionPath: function(path){
                if (path){
                    var slashIndex = path.indexOf('/');
                    if (slashIndex == -1){
                        return path;
                    }else{
                        return path.substring(0, slashIndex);
                    }
                }else{
                    return 'home';
                }
            },
            /**
             * Add sidebar
             */
            addSidebar: function() {
                this.sidebar = this.$('.sizer.size-bar');
                this.sidebarOpenButton = this.$('.open-sidebar');
                this.sidebarCloseButton = this.$('.close-sidebar');
            },
            /**
             * Slides the panel out from behind the page card.
             */
            openSidebar: function(event) {
                if (event){
                    event.preventDefault();
                    event.stopPropagation();
                }

                if(this.isSidebarOpen) return;
                this.isSidebarOpen = true;

                this.sidebarOpenButton.fadeOut(this.sidebarButtonFadeOut, 'easeInOutCubic', _.bind(this.onSidebarHidden, this.sidebarOpenButton));

                var w = parseInt(this.sidebar.css('width'), 10);

                this.sidebar.stop(true, false);
                if (this.isNarrowScreen){
                    this.sidebar.animate({right: -(w)+'px'}, this.options.animations.sidebar.slideOut, $.proxy(function() {
                        this.sidebar.addClass('top');
                        this.sidebar.animate({right: 0}, this.options.animations.sidebar.slideIn, 'easeInOutCubic', this.showSidebarCloseButton);
                    }, this));
                }else{
                    this.$currentCard.animate({width: this.cardWidth}, this.options.animations.sidebar.slideIn, 'easeInOutCubic');
                    this.sidebar.animate({right: 0}, this.options.animations.sidebar.slideIn,
                        'easeInOutCubic', _.bind(function(){
                            this.sidebar.addClass('top');
                        }, this));
                }
            },

            /**
             * Slides the sidebar behind the page card.
             */
            closeSidebar: function(event) {
                if (event){
                    event.preventDefault();
                    event.stopPropagation();
                }

                if((!this.isSidebarOpen && this.isSidebarOpen !== undefined)) return;
                this.isSidebarOpen = false;

                this.sidebarCloseButton.fadeOut(this.sidebarButtonFadeOut, 'easeInOutCubic', _.bind(this.onSidebarHidden, this.sidebarCloseButton));
                var w = parseInt(this.sidebar.css('width'), 10);

                this.sidebar.stop(true, false);

                if (this.isNarrowScreen){
                    this.sidebar.animate({right: -(w)+'px'}, this.options.animations.sidebar.slideOut, _.bind(function() {
                        this.sidebar.removeClass('top');
                        this.sidebar.animate({right: 0}, this.options.animations.sidebar.slideIn, 'easeInOutCubic', this.showSidebarOpenButton);
                    }, this));
                }else{
                    this.sidebar.removeClass('top');
                    this.$currentCard.animate({width: '840px'}, this.options.animations.sidebar.slideIn, 'easeInOutCubic');
                    this.sidebar.animate({right: w + 'px'}, this.options.animations.sidebar.slideIn, 'easeInOutCubic');
                }
            },
            /**
             * Recalculate and reset screen width for sidebar
             */
            resetSidebar: function() {
                this.isNarrowScreen = this.$window.width() <= 1280;
            },
            /**
             * Show the sidebar open button.
             */
            showSidebarOpenButton: function() {
                this.sidebarOpenButton.fadeIn(this.sidebarButtonFadeIn, 'easeInOutCubic', _.bind(this.onSidebarShown, this.sidebarOpenButton));
            },

            /**
             * Show the sidebar close button.
             */
            showSidebarCloseButton: function() {
                this.sidebarCloseButton.fadeIn(this.sidebarButtonFadeIn, 'easeInOutCubic', _.bind(this.onSidebarShown, this.sidebarCloseButton));
            },

            delayShowSidebarOpenButton: function() {
                _.delay(_.bind(function() {
                    this.showSidebarOpenButton();
                }, this), 1000);
            },

            hideSidebarOpenButton: function() {
                this.sidebarOpenButton.fadeOut(this.sidebarButtonFadeOut, 'easeInOutCubic', $.proxy(this.onSidebarHidden, this.sidebarOpenButton));
            },

            /**
             * Called when the sidebar panel is revealed from behind card.
             */
            onSidebarShown: function() {
                this.sidebar.removeClass('hide');
                this.sidebar.removeAttr('style');
            },

            /**
             * Called when the sidebar panel is hidden behind card.
             */
            onSidebarHidden: function() {
                this.sidebar.addClass('hide');
                this.sidebar.removeAttr('style');
            }



        });

        /**
         * Return view class.
         */
        return CardView;
    }
);