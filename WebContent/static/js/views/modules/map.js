/**
 * @fileoverview Asset map module view.
 * @author Chris Manning, Paul Kane
 */
define(['jquery', 'underscore', 'backbone'],
    function($, _, Backbone) {


        /**
         * View class.
         */
        var MapView = Backbone.View.extend({

            /**
             * Fired on new MapView
             */
            initialize: function(options) {
                this.setElement(options.el);
                this.loadScripts();
            },

            /**
             * Render MapBox map
             */
            createMap: function(e) {
                var mapData = this.$el.data();

                var url = mapData.attrMapurl,
                    lat = parseFloat(mapData.attrLat),
                    lon = parseFloat(mapData.attrLon),
                    name = mapData.attrName,
                    size = mapData.attrSize,
                    zoom = parseInt(mapData.attrZoom)
                    zoomControl = false;

                if (size == "L") zoomControl = true;

                var map = new L.Map(this.$el.get(0), {zoomControl: zoomControl});
                map.setView(new L.LatLng(lat, lon), zoom);

                wax.tilejson(url, function(tilejson) {
                    map.addLayer(new wax.leaf.connector(tilejson));
                });

                var MyIcon = L.Icon.extend({
                    options: {
                        iconUrl: '/static/images/modules/maps/marker.png',
                        shadowUrl: '/static/images/modules/maps/marker-shadow.png'
                    }
                });
                var icon = new MyIcon();

                var marker1 = new L.Marker(new L.LatLng(lat, lon), {icon: icon});
                marker1.bindPopup(name);
                map.addLayer(marker1);
                if (size == "L") marker1.openPopup();
            },

            /**
             * Load MapBox dependencies Leaflet and Wax.
             */
            loadScripts: function() {
                if (typeof L === 'undefined' || typeof wax === 'undefined') {
                    require(['/static/js/libs/leaflet/leaflet.js'], _.bind(function() {

                        if (typeof L !== 'undefined') {
                            // wax is dependent on leaflet.
                            require(['/static/js/libs/wax/wax.leaf.min.js'], _.bind(function() {
                                if (typeof wax !== 'undefined') {
                                    this.createMap();
                                } else {
                                    if (window.console && window.console.log) {
                                        console.log('Error loading leaflet');
                                    }
                                }
                            }, this));
                        } else {
                            if (window.console && window.console.log) {
                                console.log('Error loading leaflet');
                            }
                        }

                    }, this));
                }
                else {
                    this.createMap();
                }
            }

        });


        /**
         * Return view class.
         */
        return MapView;
    }
);
