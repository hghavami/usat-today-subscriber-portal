/**
 * @fileoverview SidebarScroll.
 * Extends Vertical Scroll for treatment on Sidebars on cards
 * @author stephen.burgess@f-i.com (Stephen Burgess), mdkennedy@gannett.com (Mark Kennedy)
 */
 define([
    'jquery', 
    'underscore', 
    'views/modules/vertical-scroll'
],
function(
    $, 
    _, 
    VerticalScroll
)
    {

        /**
         * View class.
         */
        var SidebarScroll = VerticalScroll.extend({

            removeActiveDelay: 600,

        	/**
             * Mouse over. This is here to override the
             * base-scroll to prevent standard actions
             */
            onMouseOver: function(event) {
                this.isMouseOver = true;
                
            },

            /**
             * Mouse out. This is here to override the
             * base-scroll to prevent standard actions.
             */
            onMouseOut: function(event) {
                this.isMouseOver = false;
            },

            addTopShadow: function() {
                if(!this.topShadow) {
                    this.topShadow = $('<div class="shadow top-shadow"></div>');
                }
                if(this.el.find('.top-shadow').length === 0) {
                    this.el.append(this.topShadow);
                }
            },

            removeTopShadow: function() {
                if(this.topShadow) {
                    this.topShadow.remove();
                }
            },

            /**
             * Add shadow to bottom of scroll content
             */
            addBottomShadow: function() {
                if(!this.bottomShadow) {
                    this.bottomShadow = $('<div class="shadow bottom-shadow"></div>');
                }
                if(this.el.find('.bottom-shadow').length === 0) {
                    this.el.append(this.bottomShadow);
                }

                console.log('addbottom shadow')
            },

            /**
             * Removes shadow from bottom of scroll content
             */
            removeBottomShadow: function() {
                if(this.bottomShadow) {
                    this.bottomShadow.remove();
                }
            },

            /**
             * Remove active class.
             */
            removeActiveClass: function() {
                //call removeActiveClass on super
                VerticalScroll.prototype.removeActiveClass.apply(this, arguments);
                //this is to hide the scroller when not active as it gets in way of rollovers
                setTimeout(_.bind(function() {
                    if(!this.bar.hasClass('active')) {
                        this.bar.addClass('hide');
                    }
                }, this), 1000);
            },

            /**
             * Add active class.
             */
            addActiveClass: function(delay) {
                this.bar.removeClass('hide');
                //call addActiveClass on super
                VerticalScroll.prototype.addActiveClass.apply(this, arguments);
            },

            addHideClass:function() {
                this.bar.addClass('hide');
            },
             /**
             * Add active scroll.
             */
             addActiveScroll : function(){ 

                if(scrollbar) 
                    scrollbar.addActiveClass(1000);
             },

            /**
             * When the scrollbar position is updated.
             */
            onScrollUpdated: function(event) {

                if (scrollbar) {
                    if (scrollbar.getCurrentPosition() == 0) {
                        scrollbar.removeTopShadow();
                    }
                    else {
                        scrollbar.addTopShadow();
                    }

                    if(scrollbar.getCurrentPosition() == 1) {
                        scrollbar.removeBottomShadow();
                    }
                    else if(scrollbar.active) {
                        scrollbar.addBottomShadow();
                    }
                }
                 
            },

            movePosition:function(pos) {
                var y = parseInt(this.content.css('marginTop'));
                this.content.css({marginTop: (y-pos)+'px'});
                // set transition time based on size but with upper/lower limits
                var time = Math.max(500, Math.min(1300, pos * 1.6));

                setTimeout(_.bind(function() {
                    this.content.animate({
                        'marginTop': y
                    }, time, _.bind(function() {
                        // Animation complete
                        this.refresh();
                    }, this));
                }, this), 350);
            }

        });


        /**
         * Return view class.
         */
        return SidebarScroll;
    }
);