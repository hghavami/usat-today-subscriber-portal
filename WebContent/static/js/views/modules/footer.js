/**
 * @fileoverview Global footer module view.
 * @author erik.kallevig@f-i.com (Erik Kallevig)
 * @author kris.hedstrom@f-i.com (Kris Hedstrom)
 */
define(['jquery', 'underscore', 'backbone', 'easing', 'pubsub', 'app'],
    function($, _, Backbone, Easing, PubSub, App) {


        /**
         * View class.
         */
        var FooterView = Backbone.View.extend({

            // View element.
            el: '.footer',

            // Events.
            events: {
                'click p.site-index': 'toggle'
            },

            /**
             * Initialize class view.
             */
            initialize: function() {
                // Cache selectors.
                this.$scrollEl = App.get('scrollEl');
                this.$nav = $('nav', this.$el);
                this.$doc = App.get('doc');
                this.$win = App.get('win');
                this.$cardContainer = $('.card-container');
                this.$toggle = $('p.site-index');

                // Subscribe to events.
                //PubSub.on('before:animate:cards', this.close, this);
                //PubSub.on('reordering:cards', this.adjust, this);
                //PubSub.on('resize:app', this.adjust, this);

                // @TODO: make this dynamic
                // Height of expanded nav + bottom margin of cards + height of
                // collapsed footer.
                this.footerHeight = 383;
            },

            /**
             * Subscribe to multiple events at once.
             * @param {Object} events Key/value pair of events to subscribe to.
             */
            subscribers: function(events) {
                var evt;
                for(evt in events) {
                    this.subscribe(evt, events[evt]);
                }
            },

            /**
             * Subscribe to event
             * @param {String} evt Name of event.
             * @param {String} callback Name of callback function to call.
             */
            subscribe: function(evt, callback) {
                if(typeof this[callback] === 'function') {
                    PubSub.on(evt, $.proxy(function() {
                        this[callback].apply(this, arguments);
                    }, this));
                }
            },

            /**
             * Show/hide toggle for footer.
             * @param {Event} e Browser event triggering toggle.
             */
            toggle: function(e) {
                if(e) {e.preventDefault();}
                var scrollTop = this.$el.offset().top -
                    $('#nav').height() - $('#breaking').height() -
                    $('.social-wrap .wrap').height();
                this.$toggle.toggleClass('open');
                this.$scrollEl.animate(
                    {'scrollTop': scrollTop},
                    500,
                    'easeInOutCubic');
                this.$nav.slideToggle(500, 'easeInOutCubic');
            },

            /**
             * Collapse / close the footer.
             */
            close: function() {
                this.$nav.hide();
                this.$toggle.removeClass('open');
            },

            /**
             * Adjust the position of the footer to always be at bottom
             * of the page.
             */
            adjust: function() {
                var winHeight = this.$win.height();
                var newHeight = winHeight - this.footerHeight + 'px';
                this.$cardContainer.css({'min-height': newHeight});
            },

            /**
             * Clean up view.
             * Removes event handlers and element (optionally).
             * @param {Boolean} removeEl Option to also remove View from DOM.
             */
            destroy: function(removeEl) {
                this.undelegateEvents();
                if (removeEl) {this.remove();}
            }
        });


        /**
         * Return view class.
         */
        return FooterView;
    }
);
