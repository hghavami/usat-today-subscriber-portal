/**
 * @fileoverview Previous bar view.
 * @author stephen.burgess@f-i.com (Stephen Burgess)
 */
define([
    'jquery',
    'underscore',
    'backbone'
    ],
    function(
        $,
        _,
        Backbone
    ) {


        /**
         * View class.
         */
        var PreviousBarView = Backbone.View.extend({

            // View element.
            el: '.previous-bar',

            /**
             * Initialize view.
             * @param {Object} options View options passed during init.
             */
            initialize: function(options) {
                this.options = $.extend({}, options || {});
                var previousBar = $('.previous-bar').addClass('active');
                previousBar.find('.title').text(options.title);
                previousBar.attr('href', options.url);
            }

        });


        /**
         * Return view class.
         */
        return PreviousBarView;
    }
);
