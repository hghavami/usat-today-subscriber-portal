/**
 * @fileoverview Utility Bar module view. (Bottom fixed blue bar)
 * @author Chris Manning
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'pubsub',
    'views/modules/share-tools',
    'views/modules/comment',
    'fwinfo'
],
function (
    $,
    _,
    Backbone,
    PubSub,
    ShareToolsView,
    CommentView,
    FwInfo
)
    {

        /**
         * View class.
         */
        var UtilityBarView = Backbone.View.extend({

            // View element.
            el: '.utility-wrap',

            // Events.
            events: {
                'click .print-module .btn': 'printWindow',
                'click .comment-module .btn': 'onClickCommentBtn',
                'click .share-module .btn': 'onClickShareBtn',
            },

            /**
             * Initialize view.
             * @param {Object} options View options passed during init.
             */
            initialize: function(options) {

                _.bindAll(this, 'fetchCommentCount', 'parseCommentCount');

                this.options = $.extend({
                    noTransition: false
                }, options || {});

                this.subviews = {};

                this.pubSub = {
                    'hide:utilitybar': this.hide,
                    'show:utilitybar': this.show,
                    'comment:created': this.fetchCommentCount,
                    'comment:removed': this.fetchCommentCount
                };
                PubSub.attach(this.pubSub, this);

                this.show();

                if (this.options.noTransition) {
                    this.$el.addClass('show').addClass('no-transition');
                }

                this.url = window.location.href;

                this.fetchCommentCount();

                // PubSub.on('user:authenticated', _.bind(function(loggedin){ 
                //     if (loggedin && FwInfo.getUserStatus()=="banned") 
                //         $('.btn.contribute').hide();
                //     else
                //         $('.btn.contribute').show();
                // }, this));
            },

            /**
             * Initalize comments module.
             */
            onClickCommentBtn: function(e) {
                if (!$(e.currentTarget).hasClass('open')) this.subviews.comments = new CommentView();
            },

            /**
             * Initalize share module.
             */
            onClickShareBtn: function(e) {
                if (!$(e.currentTarget).hasClass('open')) this.subviews.share = new ShareToolsView();
            },

            /**
             * Open print dialog.
             */
            printWindow: function(e) {
                PubSub.trigger('uotrack', 'UtilityBarPrint');
                window.print();                
                return false;
            },

            /**
             * Fetch comment count and update on page.
             */
            fetchCommentCount: function() {
                FB.api('/?ids=' + this.url, this.parseCommentCount);
            },

            /**
             * Parse FB comment response.
             * @param {Object} response FB comment api call respone object.
             */
            parseCommentCount: function(response) {
                var commentCount = 0;
                var responseObj = response[this.url];
                if (responseObj && responseObj.comments){
                    commentCount = responseObj.comments;
                }
                this.updateCommentCount(commentCount);
            },

            /**
             * Update the two places comment counts are displayed: utility bar
             *     and the byline bar.
             * @param {number} count Number of comments.
             */
            updateCommentCount: function(count) {
                var $creditsCommentsLabel = $('.credits .comments .label');
                var $creditsCommentsNum = $creditsCommentsLabel.siblings('.num');
                if (count > 1) $creditsCommentsLabel.text('Comment');
                $creditsCommentsNum.text(count + ' ');
                this.$('.comment-module .count').text(count);
            },

            /**
             * Adds show class to el.
             */
            show: function() {
                if (this.options.noTransition) return;
                this.$el.addClass('show');
            },

            /**
             * Removes show class from el.
             */
            hide: function() {
                if (this.options.noTransition) return;
                this.$el.removeClass('show');
            },

            /*
             * Destroy view.
             */
            destroy: function(removeEl){
                this.undelegateEvents();
                PubSub.unattach(this.pubSub, this);
                _.each(this.subviews, function(view){
                    if (typeof view.toggle !== 'undefined') view.toggle(undefined, true);
                    if (typeof view.destroy !== 'undefined') view.destroy(removeEl);
                });
                if (removeEl) this.remove();
            }
        });

        /**
         * Return view class.
         */
        return UtilityBarView;
    }
);
