/**
 * @fileoverview Global preloaders
 * @author mdkennedy@gannett.com (Mark Kennedy)
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'pubsub',
    'app'
],
function(
    $,
    _,
    Backbone,
    PubSub,
    App
) {

    /**
     * View class.
     */
    var Loader = Backbone.View.extend({

        /**
         * Initialize the view.
         */
        initialize: function(options) {
            
            this.options = $.extend({
                msg: ''
            }, options || {});

            this.win = App.get('win');
            this.body = App.get('body');
            this.el = $(this.options.el);
            this.msg = this.options.msg;

            this.render();
        },

        /**
         * Create markup for loader.
         */
        render: function() {

            var $circDiv = $('<div>').addClass('loading-icon'),
                i;

            this.$loader = $('<div>').addClass('ui-loader');
            this.$target = this.$el;
            this.$target.append(this.$loader.append($circDiv).append($('<span>').html(this.msg)));
            this.$el = this.$loader;

        },

        destroy: function(removeEl){
            this.hide();
            if (removeEl){
                this.remove();
            }
        },

        /**
         * Display loader.
         */
        show: function() {
            this.$loader.addClass('active');
        },

        /**
         * Hide the loader.
         */
        hide: function() {
            this.$loader.removeClass('active');
        }

    });

    /**
     * Return view class.
     */
    return Loader;
});
