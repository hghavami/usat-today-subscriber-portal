/**
 * @fileoverview Global overlay (nav) module view.
 * @author erik.kallevig@f-i.com (Erik Kallevig)
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'pubsub',
    'app',
    'baseview',
    'state',
    'views/pages/story',
    'views/pages/static-page',
    'views/modules/cards2',
    'easing'
],
function(
    $,
    _,
    Backbone,
    PubSub,
    App,
    BaseView,
    StateManager,
    StoryView,
    StaticPageView,
    CardView
) {


        /**
         * View class.
         */
        var OverlayView = BaseView.extend({

            // View element.
            el: '#overlay',

            // Events.
            events: {
                'click .film': 'close',
                'click .close-overlay': 'close',
                'click .arrows .previous, .arrows .next': 'onArrowClick'
            },

            /**
             * Initialize the view.
             * @param {Object=} options Init options.
             */
            initialize: function(options) {
                options = $.extend(true, {
                    template: '<div id="overlay" class="no-transition">' +
                                '<div class="content">' +
                                  '<div id="collection">' +
                                    '<div class="content-wrap active">' +
                                      '<div class="transition-wrap show">' +
                                      '</div>' +
                                    '</div>' +
                                  '</div>' +
                                '</div>' +
                                '<div class="film show"></div>' +
                              '</div>'
                });
                this.init = false;

                _.bindAll(this, 'close', 'loadStaged', 'updateArrowLinks',
                    'setCloseFixed');

                // Cached queries.
                this.win = App.get('win');
                this.scrollEl = App.get('scrollEl');
                this.nav = App.get('navEl');
                this.breaking = App.get('breakingEl');
                this.breakingWrap = App.get('breakingWrapEl');

                // PubSub events.
                this.pubSub = {
                    'close:overlay': this.close,
                    'updateArrowLinks:overlay': this.updateArrowLinks
                };

                // PubSub.on('getPrevInfo:overlay', this.getPrevInfo);
                // PubSub.on('setPrevInfo:overlay', this.setPrevInfo);
                // PubSub.on('shouldDisplayPrevBar:overlay', this.shouldDisplayPrevBar);

                // CSS transforms browser events.
                // var transEndEventNames = {
                //     'WebkitTransition' : 'webkitTransitionEnd',
                //     'MozTransition'    : 'transitionend',
                //     'OTransition'      : 'oTransitionEnd',
                //     'msTransition'     : 'MSTransitionEnd',
                //     'transition'       : 'transitionend'
                // };
                // this.transEndEventName = transEndEventNames[ Modernizr.prefixed('transition') ];

                // if(options && options.doTransition) {
                //     var y = Math.min(this.win.height(), this.transitionWrap.height()) * 0.25;
                //     var origin = '50% ' + y+'px';
                //     this.transitionWrap.css({
                //         '-webkit-transform-origin': origin,
                //            '-moz-transform-origin': origin,
                //             '-ms-transform-origin': origin,
                //              '-o-transform-origin': origin,
                //                 'transform-origin': origin
                //     });
                //     this.$el.removeClass('no-transition');
                // }

                /*
                 * The below is there to resolve a strange issue. When you have an element with fixed
                 * position inside an item that you are applying CSS transforms to strange positioning
                 * things happen. In theory the below should be able to go in the about setTimeout
                 * but it seems to take a second before the removed transforms actually get ignored
                 * when positioning elements fixed. It's been tested in Chrome, Safari and FF but please
                 * edit the time (currently 1850ms) if you find the bug still happens for you.
                 */
                // setTimeout(_.bind(function() {
                //     this.transitioning = false;
                //     this.onScrollWindow();
                // }, this), 1850);

                // Call base class initialize
                BaseView.prototype.initialize.call(this, options);
            },

            animateNewContent: function(htmlFrag){
                // scroll in the content at the top
                this.scrollEl.scrollTop(0);

//                this.$el.replaceWith(htmlFrag);
//                this.setElement(htmlFrag);
            },

//            getTemporaryLoader: function(toPath){
//                return this.options.template;
//            },

            beforeReveal: function(fromPath, toPath, htmlFrag, paused){
                this.article = htmlFrag.find('article.asset');
                this.activeContentWrap = htmlFrag.find('.content-wrap.active');
                this.transitionWrap = htmlFrag.find('.transition-wrap');
                this.film = htmlFrag.find('.film');
            },

            afterReveal: function(fromPath, toPath){
                // Initial page load, no ajax.
                if (fromPath === null) {
                    // Initialize overlay.
                    var story = this.$('.content-wrap'),
                        section = this.parseSection(story);

                    // Determine which section front should load
                    // behind depending on the story.
                    StateManager.preloadView(CardView, null, section);
                }
                // Populate arrow links.
                this.updateArrowLinks(true);

                var story = this.$('.content-wrap');
                this.closeWrap = this.activeContentWrap.find('.close-wrap');

                this.subviews.storyView = new StoryView({
                    el: story
                });

                if (!this.init){
                    PubSub.trigger('open:overlay');
                    this.init = true;
                }

                // bugfix for chrome, need to let the other fixed elements animate in before we can
                // switch to fixed closed button, otherwise it positions itself incorrectly.
                _.delay(_.bind(function(){
                    this.setCloseFixed(true);
                }, this), 1000);
            },

            animateRevealView: function(fromPath, toPath){
                this.$el.show().css({opacity: 1, display: 'block'});
                this.film.removeClass('show');

                // cache this for the life of the app
                this.collection = this.$('#collection');

                var transitionWrap = this.$('.transition-wrap');
                transitionWrap.removeClass('show').addClass('zoomed-out');
                var y = Math.min(this.win.height(), transitionWrap.height()) * 0.25,
                    origin = '50% ' + y+'px';
                transitionWrap.css({
                    '-webkit-transform-origin': origin,
                       '-moz-transform-origin': origin,
                        '-ms-transform-origin': origin,
                         '-o-transform-origin': origin,
                            'transform-origin': origin
                });
                this.$el.removeClass('no-transition');

                transitionWrap.addClass('show').addClass('zoomed-in');
                this.film.addClass('show');

                this.transitioning = true;

                var deferred = jQuery.Deferred();
                setTimeout(_.bind(function() {
                    transitionWrap.removeClass('zoomed-in zoomed-out');
                    this.transitioning = false;
                    deferred.resolve();
                }, this), 850);
                return deferred.promise();
            },

            animateRemoveView: function(toPath){
                this.$el.removeClass('no-transition');

                var transitionWrap = this.$('.transition-wrap');
                var y = Math.min(this.win.height(), transitionWrap.height()) * 0.25,
                    origin = '50% ' + y+'px';
                transitionWrap.css({
                    '-webkit-transform-origin': origin,
                       '-moz-transform-origin': origin,
                        '-ms-transform-origin': origin,
                         '-o-transform-origin': origin,
                            'transform-origin': origin
                });
                transitionWrap.addClass('zoomed-out').removeClass('show');
                this.film.removeClass('show');
                this.$('.overlay-arrows').removeClass('show');
                PubSub.trigger('hide:utilitybar');
                var deferred = jQuery.Deferred();
                setTimeout(_.bind(function() {
                    deferred.resolve();
                }, this), 600);
                return deferred.promise();
            },

            /**
             * Load staged story and social bar.
             */
            loadStaged: function() {

                // Query active/staged elements.
                this.activeContentWrap = this.$('.content-wrap.active');
                this.stagedContentWrap = this.$('.content-wrap.staged');
                var activeSocialWrap = this.$('.utility-wrap.active'),
                    stagedSocialWrap = this.$('.utility-wrap.staged'),

                // Position staged story.
                    stagedClass = this.slideDirection ? this.slideDirection :
                    'center';
                this.stagedContentWrap.addClass(stagedClass);

                // Unfix close button so it can slide with card.
                this.setCloseFixed(false);

                // Swap new social bar for old.
                activeSocialWrap.remove();
                stagedSocialWrap.removeClass('staged').addClass('active');

                // Slide direction specified, so slide-in animation required.
                if (this.slideDirection) {

                    this.activeContentWrap.css(this.slideDirection, '0%');
                    this.stagedContentWrap.css(this.slideDirection, '-100%');

                    var promise = $.when(this.animate(this.activeContentWrap, this.slideDirection,
                        '100%', 400, 'ease-in-out'),
                        this.animate(this.stagedContentWrap, this.slideDirection,
                            '0%', 400, 'ease-in-out'));
                    promise.done(_.bind(function(){
                        this.activeContentWrap.remove();
                        this.onCompleteLoadStaged();
                        this.slideDirection = false;
                    }, this));
                    return promise;

                // No slideDirection specified, so fade/prev-story load required.
                } else {

                    // Remove active story.
                    this.activeContentWrap.remove();

                    // Show staged story.
                    this.onCompleteLoadStaged();

                    var deferred = $.Deferred();
                    return deferred.resolve().promise();
                }
            },

            /**
             * Complete load staged.
             */
            onCompleteLoadStaged: function() {
                PubSub.trigger('loadStagedComplete:overlay');
                this.article = this.stagedContentWrap.attr('style', '')
                        .removeClass('staged left right center')
                        .addClass('active')
                        .find('article.asset');
                this.updateArrowLinks();
            },

            /**
             * Determine whether prev bar should be displayed.
             */
            // shouldDisplayPrevBar: function(callback) {
            //     if (callback) callback(this.slideDirection === false);
            // },

            /**
             * Retrieve stored 'previous story' data from overlay
             * @param {Object} callback Callback to pass the data to.
             */
            // getPrevInfo: function(callback) {
            //     var prevInfo = {};
            //     prevInfo.prevLink = this.$el.data('prevLink');
            //     prevInfo.prevTitle = this.$el.data('prevTitle');
            //     if (callback) callback(prevInfo);
            //     return prevInfo;
            // },

            /**
             * Store data for 'previous story' on overlay element.
             * @param {Object} prevInfo Previous story info.
             */
            // setPrevInfo: function(prevInfo) {
            //     this.$el.data('prevLink', prevInfo.prevLink);
            //     this.$el.data('prevTitle', prevInfo.prevTitle);
            // },

            /**
             * Update arrow links and toggle disabled state as necessary.
             * @param {boolean=} fadeIn Whether to fade in arrows.
             */
            updateArrowLinks: function(fadeIn) {

                // Get template.
                if (!this.arrowWrapTemplate) {
                    this.arrowWrapTemplate = this.$('#overlay-arrow-wrap');
                }

                // Cache container query.
                if (!this.arrowContainer) {
                    this.arrowContainer = this.$('.overlay-arrows');
                }

                // Get next/prev items.
                var currentPath = window.location.pathname,
                    currentIndex,
                    collection = App.get('collection');

                if (!collection) {return;}
                collection = _.filter(collection, function(item){
                    return item.headline && item.links && item.links.html;
                });

                _.each(collection, function(item, index) {
                    if (item.links.html === currentPath) {
                        currentIndex = index;
                    }
                });
                if (currentIndex === undefined) {return;}
                var adjacentAssets = [
                    collection[currentIndex - 1],
                    collection[currentIndex + 1]
                ];

                // Populate arrow links.
                var arrowMarkup = '';
                _.each(adjacentAssets, _.bind(function(asset, index) {
                    if (!asset) {
                        asset = {
                            disabled: 'disabled'
                        };
                    } else {
                        asset.disabled = '';
                    }
                    asset.ss = collection.ss;
                    asset.dir = index < 1 ? 'previous' : 'next';

                    // Check for square photo.
                    if (asset.photo && asset.photo.crops) {
                        if (asset.photo.crops['1_1'] && asset.photo.crops['1_1'].indexOf('.jpg') != -1) {
                            asset.image = asset.photo.crops['1_1'];
                            // Alter path to resize square to 64px.
                            transmogEnd = '_r64.jpg?3aa4db7cc8ccd5d587e4ef60ee4f4bfbf4814675';
                            asset.image = asset.image.replace('.jpg', transmogEnd);
                        }
                    }

                    arrowMarkup += _.template(this.arrowWrapTemplate.html(), asset);
                }, this));
                this.arrowContainer.html(arrowMarkup);
                if (fadeIn) {this.arrowContainer.addClass('show');}
            },

            /**
             * Arrow click handler (to control slide direction).
             * @param {Event} e Click event.
             */
            onArrowClick: function(e) {
                this.slideDirection = $(e.currentTarget).hasClass('next') ?
                    'right' : 'left';
            },

            /**
             * Set position of close button to fixed.
             * @param {boolean=} fix Whether to fix or unfix.
             */
            setCloseFixed: function(fix) {
                if (fix){
                    this.closeWrap.css('top', this.article.offset().top + 10);
                }else{
                    this.closeWrap.css('top', 10);
                }
                this.closeWrap.toggleClass('fixed', fix);
            },

            /**
             * Close the overlay.
             * @param {Event} e Click event.
             */
            close: function(e) {
                this.setCloseFixed(false);
                PubSub.trigger('uotrack','articleclose');
                if (this.liveHidden) {
                    $('.open-live-feed').show();
                    liveHidden = false;
                }
                StateManager.navigateToPreloadedUrl();
                if (e) return false;
            },

            /**
             * Clean up view.
             * Removes event handlers and element (optionally).
             * @param {Boolean} removeEl Option to also remove View from DOM.
             */
            destroy: function(removeEl) {
                this.undelegateEvents();
                PubSub.unattach(this.pubSub, this);
                if (removeEl){ this.remove();}
            },

            parseSection: function(el){
                var section = '';
                var pageInfoObj = App.getPageInfo(el);
                if (pageInfoObj && pageInfoObj.ss) {
                    App.set('ss', pageInfoObj.ss);
                    path = pageInfoObj.ss;
                    section = path;
                    if (path.indexOf('/') > 0){
                        section = path.substring(0, path.indexOf('/'));}

                    if (section == '/') {
                        section = '';
                    } else {
                        section = section + '/';
                    }
                }
                return section;
            },

            getTransitionAnimation: function(fromPath, toPath, requestPromise){
                var deferred = $.Deferred();
                requestPromise.done(_.bind(function(htmlFrag){
                    var story = htmlFrag.find('.content-wrap');
                    story.removeClass('active').addClass('staged');
                    this.collection.append(story);
                    var socialBar = htmlFrag.find('.utility-wrap');
                    socialBar.removeClass('active').addClass('staged').addClass('show');
                    this.collection.append(socialBar);
                    this.loadStaged().done(function(){
                        deferred.resolve();
                    });
                }, this));
                return deferred;
            }
        });


        /**
         * Return view class.
         */
        return OverlayView;
    }
);
