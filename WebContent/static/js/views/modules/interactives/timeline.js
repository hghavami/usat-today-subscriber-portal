/**
 * @fileoverview Timeline (interactive) view.
 * @author Chris Manning
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'pubsub',
    'app',
    'views/modules/video'
],
function(
    $,
    _,
    Backbone,
    PubSub,
    App,
    Video
)
    {

        /**
         * View class.
         */
        var TimelineView = Backbone.View.extend({

            el: '.interactive.timeline',

            // Events.
            events: {
                'click .tlEventMarker': 'renderButtonClick',
                'click .tlLeftArrow, .tlRightArrow': 'renderArrowClick'
            },

            /**
             * Initialize view.
             * @param {Object} options View options passed during init.
             */
            initialize: function(options) {
                this.el = options.el;
                this.subviews = {};
                this.subviews.video = [];

                this.numLabels = 13;
                this.arrEventPositions = [];
                this.maxEvents = 0;
                this.speed = 500;
                this.arrayPanelMax = 956;
                this.numPanelMinLefts = 19;
                this.numMarkerWidth = 9;
                this.strDateBegin = "";
                this.strDateEnd = "";
                this.blnFirstClickDone = false;
                this.$tlLeftArrow = this.$el.find(".tlLeftArrow");
                this.$tlRightArrow = this.$el.find(".tlRightArrow");
                this.$tlEvent = this.$el.find('.tlEvent');
                this.$tlContainer = this.$el.find('#tlContainer');

                require(["http://i.usatoday.net/_common/_scripts/_j/jquery-ui-1.8.13.custom.min.js"], _.bind(function() {
                    this.loadData();

                    $(".tlEventScrubber").draggable({containment: ".tlEventScrubberBox", cursor: "move", stop: _.bind(this.snapToMarker, this) });
                    /* Touch support - for later.
                    $(".tlContentBox").swipe({ triggerOnTouchEnd : true, swipeStatus : status , allowPageScroll:"none", threshold:100 });
                    */
                }, this));

            },

            loadData: function() {
                var eventBoxContent = "";

                this.strDateBegin = this.$tlContainer.data().graphstart;
                this.strDateEnd = this.$tlContainer.data().graphend;

                var _this = this;
                this.$tlEvent.each(function(index) {
                    var percent = $(this).find('.tlFloatingContentBox').data().tlpercent;
                    numLeftPosition = Math.round((percent * 911) + 27);
                    _this.arrEventPositions[index] = numLeftPosition;
                    eventBoxContent += "<div class=\"tlEventMarker\" style=\"left:" + numLeftPosition + "px;\"></div>";
                });

                this.$el.find(".tlEventBox").append(eventBoxContent);
                this.maxEvents = this.arrEventPositions.length;
                this.goTo(0);
                this.setUpLabels();
            },

            setUpLabels: function() {
                var dateStart, dateEnd, numDifference, numFactor, numLeftPosition, numTimeSpan;

                dateStart = this.strDateBegin;
                dateEnd = this.strDateEnd;
                numTimeSpan = dateEnd - dateStart;
                numDifference = this.$tlContainer.data().unitcount;
                numFactor = Math.ceil(numDifference / this.numLabels);

                this.$el.find('.tlLabel').each(function(index) {
                    labelDate = $(this).data().labelms;
                    numLeftPosition = Math.round((((labelDate - dateStart) / numTimeSpan) * (911)));
                    $(this).css('left', numLeftPosition);
                    // Also position "content back" in lne with labels.
                    if ($(this).find('img').hasClass('label') && numFactor > 0) {
                        $('.tlContentBack').eq(index / numFactor).css('left', numLeftPosition);
                    }
                });
                this.$el.find('.tlLabelBox, .tlContentBackBox').removeClass('invisible');
            },

            renderButtonClick: function(e) {
                var target = $(e.target);
                var _parent = target.parent().find(".tlEventMarker");
                this.goTo(_parent.index(target));
                var tracking = "timeline" + document.location.pathname + "button_position" + _parent.index(target).toString();
                PubSub.trigger('uotrack', {'prop41':tracking});
            },

            addEventListeners: function() {
                /* Touch support - for later.
                if(Modernizr.touch){
                    this.$tlLeftArrow.touch(this.renderArrowClick);
                    this.$tlRightArrow.touch(this.renderArrowClick);
                }
                */
            },

            renderArrowClick: function(e) {
                var target = $(e.target);
                var strArrowType;
                if(target.hasClass("tlLeftArrow")) {
                    this.previousImage();
                    strArrowType = "Previous";
                } else {
                    this.nextImage();
                    strArrowType = "Next";
                }
                var tracking = "timeline" + document.location.pathname + "arrow" + strArrowType;
                console.log(tracking);
                PubSub.trigger('uotrack', {'prop41':tracking});
            },

            goTo: function(intIndex){
                currentEvent = intIndex;
                if (currentEvent == 0) {
                    this.$tlLeftArrow.addClass('invisible');
                    this.$tlRightArrow.removeClass('invisible');
                } else if (currentEvent == this.maxEvents-1) {
                    this.$tlRightArrow.addClass('invisible');
                    this.$tlLeftArrow.removeClass('invisible');
                } else {
                    this.$tlLeftArrow.removeClass('invisible');
                    this.$tlRightArrow.removeClass('invisible');
                }
                this.renderEvent(currentEvent, this.speed);
            },

            previousImage: function() {
                currentEvent = Math.max(currentEvent-1, 0);
                this.$tlRightArrow.removeClass('invisible');
                if (currentEvent == 0) {
                    this.$tlLeftArrow.addClass('invisible');
                } else {
                    this.$tlLeftArrow.removeClass('invisible');
                }
                this.renderEvent(currentEvent, this.speed);
            },

            nextImage: function() {
                currentEvent = Math.min(currentEvent+1, this.maxEvents-1);
                this.$tlLeftArrow.removeClass('invisible');
                if (currentEvent == this.maxEvents-1) {
                    this.$tlRightArrow.addClass('invisible');
                } else {
                    this.$tlRightArrow.removeClass('invisible');
                }
                this.renderEvent(currentEvent, this.speed);
            },

            snapToMarker: function (e) {
                var numClosest = 980, numIndex, i, objScrubber = $(e.target);
                var numScrubberPosition = Number(objScrubber.css("left").replace("px", ""));
                for (i = 0; i < this.arrEventPositions.length; i++) {
                    if (Math.abs((numScrubberPosition + 19) - this.arrEventPositions[i]) < numClosest) {
                        numIndex = i;
                        numClosest = Math.abs((numScrubberPosition + 19) - this.arrEventPositions[i]);
                    }
                }

                var tracking = "timeline" + document.location.pathname + "drag_button_position" + numIndex.toString();
                PubSub.trigger('uotrack', {'prop41':tracking});
                this.goTo(numIndex, this.speed);
            },

            renderEvent: function(currentEvent, duration) {
                // Destroy current video instances.
                _.each(this.subviews.video, function(v){
                    v.destroy();
                });

                var numLeftPosition, numPanel;
                this.$el.find(".tliPadSlideVideo").removeClass("active");

                this.$el.find(".tlEventScrubber").css("left", (this.arrEventPositions[currentEvent] - 19).toString() + "px");
                this.$el.find(".tlBlueArrow").css("left", (this.arrEventPositions[currentEvent] - 9).toString() + "px");

                currentPanelWidth = parseInt(this.$el.find('.tlFloatingContentBox').eq(currentEvent).width());
                maxLeft = this.arrayPanelMax - currentPanelWidth;

                numLeftPosition = Math.round(this.arrEventPositions[currentEvent] + (this.numMarkerWidth / 2) - currentPanelWidth / 2);
                if (numLeftPosition > maxLeft) {
                    numLeftPosition = maxLeft;
                } else if (numLeftPosition < this.numPanelMinLefts) {
                    numLeftPosition = this.numPanelMinLefts;
                } //if

                this.$tlEvent.eq(currentEvent)
                    .find('.tlBlueArrow')
                    .css("left", this.arrEventPositions[currentEvent] - 9 - numLeftPosition.toString() + "px")

                this.$tlEvent.addClass('hidden')
                    .eq(currentEvent)
                    .css("left", numLeftPosition.toString() + "px")
                    .removeClass('hidden');

                this.$el.find(".tlTextChatter a").attr("target", "_blank");

                var _this = this;
                // Process video(s) within.
                $.each(this.$tlEvent.eq(currentEvent).find('.tlVideo'), function() {
                    var video = new Video({
                        el: $(this).get(0),
                        autocreate: true    // We do not have a video still for a 'click to create' embed.
                    });
                    _this.subviews.video.push(video);
                });
            },

            status: function(event, phase, direction, distance) {
                if( phase=="move" && (direction=="left" || direction=="right") ) {
                    var duration=0;
                    if (direction == "left") {
                        this.renderEvent(( currentEvent) + distance, duration);
                    } else if (direction == "right") {
                        this.renderEvent(( currentEvent) - distance, duration);
                    }
                } else if ( phase == "cancel") {
                    this.renderEvent( currentEvent, this.speed);
                } else if ( phase =="end" ) {
                    if (direction == "right") {
                        this.previousImage();
                    } else if (direction == "left") {
                        this.nextImage();
                    }
                }

            }

        });


        /**
         * Return view class.
         */
        return TimelineView;
    }
);
