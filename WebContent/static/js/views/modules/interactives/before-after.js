 /**
 * @fileoverview Before After (interactive) view.
 * @author Stan Wilson Jr
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'pubsub',
    'app'
],
function(
    $,
    _,
    Backbone,
    PubSub,
    App
 )
    {

        /**
         * View class.
         */
        var BeforeAfterView = Backbone.View.extend({

            el: '.before-after',
            currentSlide : 0,

            // Events.
            events: {
                "click   .BABar .bullets ul li" : "activateContent",
                "click   .caption" : "captionToggle",
                "click   .closeBtn" : "captionToggle",
                "click   #split" : "splitToggle"
            },

            /**
             * Initialize view.
             * @param {Object} options View options passed during init.
             */
            initialize: function(options) {

                 this.setUpSlider();

            },
            render : function(){




            },

            bulletLengthCheck : function(){
               if ($(".BABar .bullets ul li").length <= 1){
                 $(".BABar .bullets").css({"display": "none"});
               }
            },

            setUpSlider : function(){

                require([
                    '/static/js/libs/jquery/plugins/jquery-ui-1.8.13.custom.min.js',
                    '/static/js/libs/jquery/plugins/jquery.beforeafter-1.4-modified.js'
                    ],function(){

                    $("div.imageContainer").each( function() {
                            $(this).beforeAfter({
                            animateIntro:true,
                                imagePath:'/static/images/interactives/before-after/',
                                introDuration : 2000,
                                introPosition : 0.50,
                                showFullLinks : true,
                                beforeLinkText: 'Show before image',
                                afterLinkText: 'Show after image',
                                cursor: 'e-resize',
                                dividerColor: 'none',
                                dividerWidth: 2,
                                handleWidth: 51,
                                handleHeight: 541,
                                handleImageName: "handle.png",
                                handleLeftOffset: -25,
                                dragAreaWidth: 920,
                                onReady: function () { $("div.BABar").addClass("show");   }
                            });
                    });
                    $(".BABar .bullets ul li:eq(0)").trigger("click");

                });


            },
            captionToggle : function(){

                var container = this.$el;
                var button = container.find('.captionButton');
                var caption = container.find('.BAImagesContainer .captionText');
                var closeBtn = container.find('.closeBtn');
                var onText = "caption on";
                var offText = "caption off",
                tracking;

                if( caption.css("display") == 'block'){
                    button.toggleClass("on");
                    button.html(onText);
                    tracking = "beforeandafter_caption_off";
                    caption.css("display", "none");
                } else {
                    button.toggleClass("on");
                    button.html(offText);
                    tracking = "beforeandafter_caption_on";
                    caption.css("display", "block");
                }
                PubSub.trigger('uotrack', {'prop41':tracking});

            },


            splitToggle : function(){


                        var _this = $(this);
                        var _parent = _this.parent().find("#split"),
                        tracking;

                        if ($("#split").is(".active")) {


                            $("#split").attr({"class": "inactive"});
                            $(".splitText").html("split");
                            $(".label1 , .label2").css({"top": "3px"});
                            $(".ui-draggable").css({"left": "482px"}).removeClass("hidden");
                            $(".beforeIMG img , .afterIMG img").css({"width": "965px"});
                            $(".beforeIMG img , .afterIMG img").css({"height": "100%"});
                            $(".beforeIMG img , .afterIMG img").css({"margin-top": "0px"});
                            tracking = "beforeandafter_splitview_inactive";

                        } else{
                            $("#split").attr({"class": "active"});
                            $(".splitText").html("merge");
                            $(".label1 , .label2").css({"top": "134px"});
                            $(".ui-draggable").addClass("hidden").css({"left": "1955px"});
                            $(".beforeIMG img").css({"width": "482px"});

                            $(".beforeIMG").css({"width": "482px"});
                            $(".afterIMG img").css({"width": "482px"});
                            $(".beforeIMG img , .afterIMG img").css({"height": "50%"});
                            $(".beforeIMG img , .afterIMG img").css({"margin-top": "130px"});
                            tracking = "beforeandafter_splitview_active";
                        }

                        PubSub.trigger('uotrack', {'prop41':tracking});


            },
            activateContent : function(event){
                var activeItem = $(event.currentTarget),
                    intIndex = $(".BABar .bullets ul li").index(activeItem),
                    list = $(".BABar .bullets ul li"),
                    contentBlocks = $("div.BAImagesContainer div.contentBlock"),
                    tracking;


                list.removeClass("active").filter('li:eq('+intIndex+')').addClass("active");
                contentBlocks.removeClass("active").filter('div.contentBlock:eq('+intIndex+')').addClass("active");
                $("#split").attr({"class": "inactive"});
                this.currentSlide = intIndex;
               if(event.type == 'click' && event.hasOwnProperty('originalEvent')){
                    tracking = "beforeandaftercurrentslide" + this.currentSlide;
                    PubSub.trigger('uotrack', {'prop41':tracking});
                }
            }





        });


        /**
         * Return view class.
         */
        return BeforeAfterView;
});