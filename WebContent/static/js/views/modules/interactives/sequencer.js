/**
 * @fileoverview Sequencer (interactive) view.
 * @author Chris Manning
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'pubsub',
    'app',
    'views/modules/video'
],
function(
    $,
    _,
    Backbone,
    PubSub,
    App,
    Video
)
    {

        /**
         * View class.
         */
        var SequencerView = Backbone.View.extend({

            el: '.interactive.sequencer',

            // Events.
            events: {
                'click .sqButton': 'renderButtonClick',
                'click .sqPanelHide': 'showHidePanel'
            },

            /**
             * Initialize view.
             * @param {Object} options View options passed during init.
             */
            initialize: function(options) {
                this.el = options.el;
                this.subviews = {};
                this.subviews.video = [];

                this.blnFirstClickDone = false;
                this.$tlEvent = this.$el.find('.sqImageSliderBox .sqImage');
                this.$clipContainer = this.$el.find('.sqImageSliderBox');
                this.maxImages = this.$tlEvent.length;
                this.clipTotal = this.$clipContainer.width() * this.maxImages;
                this.slideWidth = this.$clipContainer.width();
                this.currentSlide = 0;
                this.speed = 500;

                this.setup();
            },

            setup: function() {
                if (this.maxImages > 1) {
                    this.$el.find(".sqPanelHide").css("display", "block");
                    this.$el.find(".sqButtonPanelContainer").css("display", "block");
                }
                this.$clipContainer.width(this.clipTotal);
                this.goTo(0);

                /* Touch support for later
                if ((this.$el.find("#sqContainer").hasClass("slide")) && (Modernizr.touch)) {
                    this.$clipContainer.swipe({ triggerOnTouchEnd : true, swipeStatus : status , allowPageScroll:"none", threshold:100 });
                }
                */
            },

            renderButtonClick: function(e) {
                var target = $(e.currentTarget);
                var parent = target.parent().find(".sqButton"),
                    tracking;
                this.goTo(parent.index(target));

                tracking = "sequencer" + document.location.pathname + "position" + parent.index(target).toString();
                PubSub.trigger('uotrack', {'prop41':tracking});
            },

            goTo: function(index) {
                this.currentSlide = index;
                if (this.$el.find("#sqContainer").hasClass("slide")) {
                    this.scrollImages();
                } else {
                    this.fadeImages();
                }
            },

            renderEvent: function(index) {
                /* Touch support for later
                this.$el.find(".sqiPadSlideVideo").addClass("active");
                if (Modernizr.touch) {
                    this.$el.find(".blankCover").addClass("active");
                }
                */

                // Do not try to process non-existant videos.
                if (this.$tlEvent.eq(index).find('.tlVideo').length < 1) return;

                // Destroy current video instances.
                _.each(this.subviews.video, function(v){
                    v.$el.addClass('hidden');
                    v.destroy();
                });

                var _this = this;
                // Process video(s) within.
                $.each(this.$tlEvent.eq(index).find('.tlVideo'), function() {
                    var video = new Video({
                        el: $(this).get(0),
                        autocreate: true    // We do not have a video still for a 'click to create' embed.
                    });
                    video.$el.removeClass('hidden');
                    _this.subviews.video.push(video);
                });
            },

            scrollImages: function(duration) {
                if (!duration) var duration = this.speed;
                var distance = this.slideWidth * this.currentSlide;

                this.$clipContainer.animate({"left":-1 * distance}, duration, _.bind(this.renderEvent, this, this.currentSlide));
                this.$el.find(".sqButton").removeClass("active").eq(this.currentSlide).addClass("active");
            },

            fadeImages: function(duration) {
                if (!duration) var duration = this.speed;

                this.$tlEvent.filter(".active").animate({"opacity":0}, duration).removeClass("active");
                this.$tlEvent.eq(this.currentSlide).animate({"opacity":1}, duration, _.bind(this.renderEvent, this, this.currentSlide)).addClass("active");
                this.$el.find(".sqButton").removeClass("active").eq(this.currentSlide).addClass("active");
            },

            animatePanel: function(distance) {
                if (!duration) var duration = this.speed;

                /* Touch support for later
                if (Modernizr.touch) {
                    this.$el.find(".sqButtonPanelContainer").css("-webkit-transition-duration", (duration/1000).toFixed(1) + "s");
                    var value = (distance<0 ? "" : "-") + Math.abs(distance).toString();
                    this.$el.find(".sqButtonPanelContainer").css("-webkit-transform", "translate3d(0px,"+value +"px,0px)");
                } else {
                */
                    this.$el.find(".sqButtonPanelContainer").animate({"top":-1 * distance}, duration);
                //}
            },

            showHidePanel: function(e) {
                var target = $(e.target),
                    tracking;
                target.toggleClass('show');

                if (target.is(".show")) {
                    this.animatePanel(430);
                    tracking = "sequencer" + document.location.pathname + "showpanel";
                } else {
                    /* Touch support later
                    var isiPad = navigator.userAgent.match(/iPad/i) != null;
                    if (isiPad) {
                        this.animatePanel(0);
                    } else {
                    */
                        this.animatePanel(-46);
                    //}
                    tracking = "sequencer" + document.location.pathname + "hidepanel";
                }
                PubSub.trigger('uotrack', {'prop41':tracking});
            },

            status: function(event, phase, direction, distance) {
                var tracking;
                if( phase=="move" && (direction=="left" || direction=="right") ) {
                    var duration=0;
                    if (direction == "left") {
                        this.scrollImages(this.currentSlide - 1, duration);
                    } else if (direction == "right") {
                        this.scrollImages(this.currentSlide + 1, duration);
                    }
                } else if (phase == "cancel") {
                    this.scrollImages(this.currentSlide, this.speed);
                } else if (phase =="end") {
                    if (direction == "right") {
                        this.previousImage();
                    } else if (direction == "left") {
                        this.nextImage();
                    }
                }
                tracking = "sequencer" + document.location.pathname + "swipe";
                PubSub.trigger('uotrack', {'prop41':tracking});
            }

        });


        /**
         * Return view class.
         */
        return SequencerView;
    }
);
