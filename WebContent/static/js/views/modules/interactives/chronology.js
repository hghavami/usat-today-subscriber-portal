/**
 * @fileoverview Chronology (interactive) view.
 * @author Chris Manning
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'pubsub',
    'app',
    'views/modules/video'
],
function(
    $,
    _,
    Backbone,
    PubSub,
    App,
    Video
)
    {

        /**
         * View class.
         */
        var ChronologyView = Backbone.View.extend({

            el: '.interactive.chronology',

            // Events.
            events: {
                'click .topBullet': 'renderButtonClick',
                'click .tlLeftArrow, .tlRightArrow': 'renderArrowClick'
            },

            /**
             * Initialize view.
             * @param {Object} options View options passed during init.
             */
            initialize: function(options) {
                this.el = options.el;
                this.subviews = {};
                this.subviews.video = [];

                this.blnFirstClickDone = false;
                this.$tlLeftArrow = this.$el.find(".tlLeftArrow");
                this.$tlRightArrow = this.$el.find(".tlRightArrow");
                this.$tlEvent = this.$el.find('.clipContainer .slideLI');
                this.$clipContainer = this.$el.find(".clipContainer");
                this.slideWidth = this.$tlEvent.outerWidth();
                this.maxEvents = this.$tlEvent.length;
                this.currentSlide = 0;
                this.clipTotal = parseInt(this.$clipContainer.width()) * this.maxEvents;
                this.speed = 500;

                this.setup();
            },

            setup: function() {
                this.$clipContainer.width(this.clipTotal);
                this.renderEvent(0);

                /* Touch support for later
                this.$clipContainer.swipe({ triggerOnTouchEnd : true, swipeStatus : status , allowPageScroll:"none", threshold:100 });
                */
            },

            renderButtonClick: function(e) {
                var target = $(e.target);
                var _parent = target.parents('.eventBulletList').find(".topBullet");    //this is the buttons on top, not the slides
                this.goTo(_parent.index(target.closest('.topBullet')));
                var tracking ="chronology" + document.location.pathname + "button_position" + target.closest('.topBullet').attr("data-index");
                PubSub.trigger('uotrack', {'prop41':tracking});
            },

            renderArrowClick: function(e) {
                var target = $(e.target);
                var strArrowType;
                if (target.is(this.$tlLeftArrow)) {
                    this.previousImage();
                    strArrowType = "Previous";
                } else {
                    this.nextImage();
                    strArrowType = "Next";
                }
                var tracking ="chronology" + document.location.pathname + "arrow" + strArrowType;
                PubSub.trigger('uotrack', {'prop41':tracking});
            },

            goTo: function(index) {
                this.currentSlide = index;
                if (index == 0) {
                    this.$tlLeftArrow.addClass('invisible');
                    this.$tlRightArrow.removeClass('invisible');
                } else if (index == this.maxEvents-1) {
                    this.$tlLeftArrow.removeClass('invisible');
                    this.$tlRightArrow.addClass('invisible');
                } else {
                    this.$tlLeftArrow.removeClass('invisible');
                    this.$tlRightArrow.removeClass('invisible');
                }
                this.scrollImages(index, this.speed);
            },

            previousImage: function() {
                this.goTo(Math.max(this.currentSlide-1, 0));
            },

            nextImage: function() {
                this.goTo(Math.min(this.currentSlide+1, this.maxEvents-1));
            },

            renderEvent: function(index) {
                /* Touch support for later
                $(".iPadSlideVideo").addClass("active");
                if (Modernizr.touch) {
                    $(".blankCover").addClass("active");
                }
                */

                // Destroy current video instances.
                _.each(this.subviews.video, function(v){
                    v.$el.addClass('hidden');
                    v.destroy();
                });

                var _this = this;
                // Process video(s) within.
                $.each(this.$tlEvent.eq(index).find('.tlVideo'), function() {
                    var video = new Video({
                        el: $(this).get(0),
                        autocreate: true    // We do not have a video still for a 'click to create' embed.
                    });
                    video.$el.removeClass('hidden');
                    _this.subviews.video.push(video);
                });
            },

            scrollImages: function(index, duration) {
                /* Touch support for later
                $(".iPadSlideVideo").removeClass("active");
                */
                var distance = this.slideWidth * index;
                this.$clipContainer.animate({"left":-1 * distance}, duration, _.bind(this.renderEvent, this, this.currentSlide));
                this.$el.find(".topBullet").removeClass("active").eq(this.currentSlide).addClass("active");
            },

            status: function(event, phase, direction, distance) {
                if( phase=="move" && (direction=="left" || direction=="right") ) {
                    var duration=0;
                    if (direction == "left") {
                        this.scrollImages(this.currentSlide - 1, duration);
                    } else if (direction == "right") {
                        this.scrollImages(this.currentSlide + 1, duration);
                    }
                } else if (phase == "cancel") {
                    this.scrollImages(this.currentSlide, this.speed);
                } else if (phase =="end") {
                    if (direction == "right") {
                        this.previousImage();
                    } else if (direction == "left") {
                        this.nextImage();
                    }
                }
                var tracking = "chronology" + document.location.pathname + "swipe";
                PubSub.trigger('uotrack', {'prop41':tracking});
            }

        });


        /**
         * Return view class.
         */
        return ChronologyView;
    }
);
