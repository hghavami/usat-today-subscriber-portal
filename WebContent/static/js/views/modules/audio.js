/**
 * @fileoverview AudioView.
 * @author stephen.burgess@f-i.com (Stephen Burgess)
 */
define(['jquery', 'underscore', 'backbone', 'circleplayer', 'grab', 'jplayer', 'transform'],
    function($, _, Backbone) {

        /**
         * View class.
         */
        var AudioView = Backbone.View.extend({

        	/**
             * Initialize view.
             */
            initialize: function(options) {

                this.options = $.extend({}, options || {});

                this.circlePlayer = new CirclePlayer(this.options.el+' .cp-jplayer', {
                    m4a: $(this.el).attr('data-m4a'),
                    oga: $(this.el).attr('data-ogg')
                }, {
                    cssSelectorAncestor: this.options.el,
                    swfPath: "js",
                    wmode: "window"
                });
            },

            destroy: function() {
                this.circlePlayer.destroy();
            }

        });


        /**
         * Return view class.
         */
        return AudioView;
    }
);