/**
 * @fileoverview Ajax Widget module view. Dynamically loads an HTML snippet,
 * appending it to this.el
 * @author jhensley
 */
define(['jquery', 'underscore', 'backbone', 'state'],
    function($, _, Backbone, StateManager) {

        /**
         * View class.
         */
        var AjaxWidgetView = Backbone.View.extend({

            /**
             * Initialize view.
             * @param {Object} options View options passed in during init.
             */
            initialize: function(options) {

                var replace = $(this.options.contents).length > 0;
                this.loadData(this.options.url, replace);

                if (this.options.refresh && this.options.contents) {
                    this.refresh = setInterval(_.bind(function() {
                        this.loadData(this.options.url, true)
                    }, this), this.options.refresh)
                };

            },


            /**
             * Load ajax contents.
             * @param {string} url is the string of the url to parse.
             * @param {Boolean} replace is a flag to determine whether or not to
             * replace the existing contents/module
             */
            loadData: function(url, replace) {

                if ($(this.options.contents).hasClass('paused')) {
                    clearInterval(this.refresh);
                    return;
                };

                this.ajax = StateManager.fetchHtml(url).done(_.bind(function(htmlFrag){
                    this.ajax = null;
                    if (replace) {
                        $(this.options.contents).remove();
                    }
                    this.$el.append(htmlFrag);
                }, this));

            },

            /**
             * Clean up view.
             * Removes event handlers and element (optionally).
             * @param {boolean} removeEl option to also remove View from DOM.
             */
            destroy: function(removeEl) {
                this.undelegateEvents();
                if (this.ajax){
                    this.ajax.abort();
                }
                clearInterval(this.refresh);
                if (removeEl) this.remove();
            }

        });


        /**
         * Return view class.
         */
        return AjaxWidgetView;
    }
);
