/**
 * @fileoverview Live feed view module.
 * @author stephen.burgess@f-i.com (Stephen Burgess)
 */
 define([
    'jquery',
    'underscore',
    'backbone',
    'pubsub',
    'state',
    'app',
    'views/modules/sidebar-scroll',
    'models/modules/live-feed-model',
     'views/modules/loader'
],
function(
    $,
    _,
    Backbone,
    PubSub,
    StateManager,
    App,
    SidebarScroll,
    LiveFeedModel,
    Loader
)
    {

        /**
         * View class.
         */
        var LiveFeedView = Backbone.View.extend({

            // Events.
            events: {
                'scrollBarUpdate .sidebar-content': 'onScrollUpdated'
            },

            /**
             * Initialize view.
             */
            initialize: function(options) {
                this.options = $.extend({}, options || {});

                _.bindAll(this,
                    'dataLoaded',
                    'updateLoaded',
                    'refreshScrollBar',
                    'onQueueUpdated',
                    'onMarkupUpdated',
                    'checkForUpdates'
                );

                this.xhr = null;
                this.win = App.get('win');
                this.body = App.get('body');
                this.sizeBar = this.$el.parents('.sizer.size-bar');
                this.ul = this.$el.find('ul');

                this.loader = new Loader({
                        el: this.$el,
                        msg: 'Loading...'
                    });

                //set loader to show initially
                this.loader.show();

                this.updateDelay = 30; //seconds
                //this is when the highlights to new content gets removed - should be less than updateDelay
                this.removeNewHighlight = 20; //seconds

                // Initialize the live feed model.
                this.model = new LiveFeedModel();
                this.fetchData('/feeds/live/'+this.options.section+'?count=40', this.dataLoaded);

                this.newItemsArray = [];
                _.bindAll(this, 'onQueueUpdated', 'onMarkupUpdated', 'checkForUpdates');

                // Initialize the vertical scroll
                this.vscrollbar = new SidebarScroll({
                    el: $('.live-feed-content', this.$el),
                    template: '<div class="vertical-scroll ui-light"><div class="scrolldragger"></div></div>',
                    padding: 2
                });
                this.vscrollbar.addHideClass();

                //for refreshing scroll bar on window resize
                var throttledResize = _.throttle(this.refreshScrollBar, 500);
                this.win.on('resize.' + this.cid, throttledResize);

                this.pubSub = {
                    'showmore:headlines': this.onHeadlinesUpdated
                };
                PubSub.attach(this.pubSub, this);
                this.model.bind('change:queue-'+this.options.section, this.onQueueUpdated);

                if(this.ul.find('li').length) {
                    this.$el.removeClass('shadow');
                    this.vscrollbar.position(0, true);
                    this.model.set('markup-'+this.options.section, $(this.ul.find('li')));
                    this.checkForUpdates();
                    this.startUpdateCheck();
                }
                else {
                    var markup = this.model.get('markup-'+this.options.section);
                    if(markup && markup.length) {
                        this.onMarkupUpdated(this.model, markup);
                    }
                    else {
                        this.model.bind('change:markup-'+this.options.section, this.onMarkupUpdated);
                    }
                }
            },

            /**
             * Checks for updated data for specified section.
             * @param {String} the section to load data for.
             */
            fetchUpdate: function(section) {
                this.cancelAjax();

                var queue = this.model.get('queue-' + this.options.section);
                var timestamp;

                if(queue && queue.length) {
                    //if there's a queue use last items timestamp
                    timestamp = $(queue[0]).attr('data-timestamp');
                }
                else {
                    //if no queue use timestamp from last markup item
                    var o = this.model.get('markup-' + this.options.section);
                    timestamp = $(o).attr('data-timestamp');
                }

                //uncomment for testing livefeed - remove before launch
                /*if(this.debug) {
                 //this.debug = false;
                 timestamp = '2012-07-19T03:32:33';
                 }
                 else {
                 return;
                 }*/

                var url = '/feeds/live/'+section+'?date='+timestamp;
                this.fetchData(url, this.updateLoaded);
            },

            /**
             * Loads initial live feed data for specified section.
             * @param {String} the section to load data for.
             */
            fetchData: function(url, callback) {
                this.cancelAjax();
                this.xhr = StateManager.fetchHtml(url)
                    .done(callback)
                    .always(_.bind(function(){
                        //hide loader
                        if (this.xhr){
                            this.xhr = null;
                        }
                        this.loader.hide();
                    }, this));
            },

            /**
             * Cancels the current Ajax request if there is one.
             */
            cancelAjax: function() {
                if(this.xhr) {
                    this.xhr.abort();
                }
            },

            /**
             * Handler for when initial data is loaded.
             * @param {String} html markup from ajax request.
             */
            updateLoaded: function(markup) {
                if(markup.length === 0) return;

                var queue = this.model.get('queue-' + this.options.section);
                //if we don't have a queue create one
                if(queue === undefined) queue = $();

                //last item from new markup to compare with current
                var last = $(markup[markup.length-1]);
                var prev;

                if(queue.length) {
                    //if there's items in queue use them
                    prev = $(queue[0]);
                }
                else {
                    //otherwise use markup
                    prev = $(this.model.get('markup-' + this.options.section)[0]);
                }

                var counter = markup.length;
                //checks to ensure last item and new items are dups
                if(prev && last.attr('data-timestamp') === prev.attr('data-timestamp') &&
                    last.find('.headline a strong').text() === prev.find('.headline a strong').text()) {
                    //the last item is a duplicate so don't include it
                    --counter;
                }

                if(counter === 0) return;
                queue = $.makeArray(queue);

                //add new items to the new items array
                while(--counter > -1) {
                    queue.unshift(markup[counter]);
                }
                this.model.set('queue-' + this.options.section, queue);
            },

            /**
             * Handler for when initial data is loaded.
             * @param {String} html markup from ajax request.
             */
            dataLoaded: function(markup) {
                this.model.set('markup-' + this.options.section, markup);
            },

            /**
             * Handler initial data loaded.
             * @param {Object} model that dispatched event.
             * @param {Object} jquery object of <li> items.
             */
            onMarkupUpdated: function(model, markup) {
                this.model.unbind('change:markup-' + this.options.section, this.onMarkupUpdated);

                this.ul.find('li').remove();
                this.ul.append(markup);

                //remove the bottom border from last item
                var li = this.ul.find('li');
                $(li[li.length-1]).addClass('last-item');

                this.refreshScrollBar();
                this.startUpdateCheck();
            },

            /**
             * Handler for queue data updated.
             * @param {Object} model that dispatched event.
             * @param {Object} jquery object of <li> items.
             */
            onQueueUpdated: function(model, markup) {
                this.updateItemsToLoad(this.model.get('queue-'+this.options.section).length);
            },

            /**
             * When the scrollbar position is updated.
             */
            onScrollUpdated: function(event) {
            },

            /**
             * Called when the page resizes as when the media query changes
             * the width of the panel we need to refresh the scrollbar.
             */
            refreshScrollBar: function(event) {
                this.isNarrowScreen = this.win.width() <= 1280;
                if(!this.vscrollbar) return;
                this.vscrollbar.refresh();
                this.onScrollUpdated();
            },

            /**
             * Called when the page height changes due to more stories loading
             */
            onHeadlinesUpdated: function() {
                //wait 1 second then refresh scrollbar
                setTimeout(this.refreshScrollBar, 1000);
            },

            /**
             * Updates the loadMoreButton with indication of how many items to be loaded.
             */
            updateItemsToLoad: function(newItems) {
                if(newItems === 0) return;
                this.showNewItems();
            },

            /**
             * Called when new items are available and user clicks the
             * loadMoreButton this function adds them to the panel
             */
            showNewItems: function(event) {
                if(event) {
                    event.stopPropagation();
                    event.preventDefault();
                }

                this.newcontent = $(this.model.get('queue-'+this.options.section));

                if(this.newcontent === undefined || this.newcontent.length === 0) return;

                this.newcontent.addClass('newcontent');
                $(this.newcontent[0]).append('<div class="shadow"></div>').addClass('top');
                $(this.newcontent[this.newcontent.length-1]).append('<div class="shadow"></div>').addClass('bottom');
                this.ul.prepend(this.newcontent);

                var h = _.reduce(this.newcontent, function(memo, item) {return memo + $(item).outerHeight();}, 0);
                this.vscrollbar.movePosition(h);

                //if there's over 60 items remove oldest
                var li = this.ul.find('li');
                if(li.length > 60) {
                    li.slice(60).remove();
                }

                this.model.merge(this.options.section);

                if(this.newBubble === undefined) {
                    var header = $('h6', this.el);
                    header.append('<div class="new-bubble">'+this.newcontent.length+'</div>');
                    this.newBubble = header.find('.new-bubble');
                    this.newBubble.fadeOut(0);
                }
                else {
                    this.newBubble.html(this.newcontent.length);
                }

                this.newBubble.fadeIn(300);

                setTimeout(_.bind(function() {
                    this.newcontent.removeClass('newcontent');
                    this.newBubble.fadeOut(300);
                }, this), this.removeNewHighlight * 1000);
            },

            /**
             * Starts the update check.
             */
            startUpdateCheck: function() {
                clearInterval(this.interval);
                this.interval = setInterval(this.checkForUpdates, this.updateDelay * 1000);
            },

            /**
             * Stops the update check.
             */
            stopUpdateCheck: function() {
                clearInterval(this.interval);
            },

            /**
             * Checks if any new stories are available.
             */
            checkForUpdates: function() {
                //tell model to get an update
                this.fetchUpdate(this.options.section);
            },

            /**
             * Clean up the view (no argument).
             */
            destroy: function() {
                this.undelegateEvents();
                PubSub.unattach(this.pubSub, this);
                this.win.off('.' + this.cid);

                this.stopUpdateCheck();
                
                if(this.vscrollbar) {
                    this.vscrollbar.destroy();
                }
                this.loader.destroy(true);
                this.model.unbind('change:markup-'+this.options.section, this.onMarkupUpdated);
                this.model.unbind('change:queue-'+this.options.section, this.onQueueUpdated);
                this.cancelAjax();
            }

        });


        /**
         * Return view class.
         */
        return LiveFeedView;
    }
);
