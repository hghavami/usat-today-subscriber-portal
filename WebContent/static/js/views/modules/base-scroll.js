/**
 * @fileoverview BaseScroll view module.
 * 
 * Defines methods that are shared by both the vertical
 * and horizontal scroll components
 *
 * TROUBLESHOOTING:
 * The most common reason for the scroller not working is the divs for the
 * container and scrollable content not being setup correctly. Please ensure 
 * that the container is passed as the el (and not as a Jquery object) and 
 * the content resides within the container and either has the 
 * 'scrollable-content' class on it, or you pass through a new contentClass 
 * within your options.
 *
 * If you are still having issues console.log the height and contentHeight 
 * (or width / contentWidth) variables in the refresh method and ensure they 
 * are as you would expect them to be. The key is to ensure that the content 
 * doesn't shrink to be the same width/height as it's container. The 
 * contentHeight should be larger than the height for a scroll bar to be 
 * required.
 * 
 * @author stephen.burgess@f-i.com (Stephen Burgess)
 */
 define([
    'jquery', 
    'underscore', 
    'backbone', 
    'pubsub', 
    'app', 
    'mousewheel'
],
function(
    $, 
    _, 
    Backbone, 
    PubSub, 
    App, 
    MouseWheel
)
    {

        /**
         * View class.
         */
        var BaseScrollView = Backbone.View.extend({

            // Events.
            events: {
                'mousewheel': 'onMouseWheel'
            },

            removeActiveDelay: 300,
            currentPosition: 0,

            /**
             * Initialize view.
             */
            initialize: function() {
            	_.bindAll(this, 
                    'onBarClicked',
                    'onMouseDown',
                    'onMouseOver',
                    'onMouseOut',
                    'onMouseMove',
                    'onMouseUp',
                    'removeActiveClass'
                );

                this.content = this.el.find(this.options.contentClass);
                this.bar = $(this.options.template);
                this.dragger = this.bar.find('.scrolldragger');

                //vars
                this.isMouseDown = false;
                this.isMouseOver = false;
                this.draggerOffset = 0;

                this.refresh();
            },

            /**
             * Bind events.
             */
            bindEvents: function() {
                this.delegateEvents();
                this.bar.on('mousedown.' + this.cid, this.onBarClicked);
                this.dragger.on('mousedown.' + this.cid, this.onMouseDown);
                this.dragger.on('mouseover.' + this.cid, this.onMouseOver);
                this.dragger.on('mouseout.' + this.cid, this.onMouseOut);
            },

            /**
             * Unbind events.
             */
            unbindEvents: function() {
                this.undelegateEvents();
                this.bar.off('.' + this.cid);
                this.dragger.off('.' + this.cid);
                this.body.off('.' + this.cid);
            },

            /**
             * Mouse down.
             */
            onMouseDown: function(event) {
                event.stopPropagation();
                event.preventDefault();

                this.isMouseDown = true;
                this.body.on('mousemove.' + this.cid, this.onMouseMove);
                this.body.on('mouseup.' + this.cid, this.onMouseUp);
            },

            /**
             * Mouse up.
             */
            onMouseUp: function(event) {
                this.isMouseDown = false;
                this.body.off('.' + this.cid);
                if(!this.isMouseOver) {
                    this.dragger.removeClass('hover');
                    this.bar.removeClass('active');
                }
            },

            /**
             * Mouse over.
             */
            onMouseOver: function(event) {
                this.isMouseOver = true;
                this.dragger.addClass('hover');
                this.bar.addClass('active');
            },

            /**
             * Mouse out.
             */
            onMouseOut: function(event) {
                this.isMouseOver = false;
                if(!this.isMouseDown) {
                    this.dragger.removeClass('hover');
                    this.startTimeout(this.removeActiveDelay);
                }
            },

            /**
             * Mouse wheel scroll.
             */
            onMouseWheel: function(event, delta) {
            },

            /**
             * Add active class.
             */
            addActiveClass: function(delay) {
                this.bar.addClass('active');
                this.startTimeout(delay || this.removeActiveDelay);
            },

            /**
             * Remove active class.
             */
            removeActiveClass: function() {
                if(this.isMouseDown || this.isMouseOver) return;
                this.bar.removeClass('active');
                clearInterval(this.timeInterval);
            },

            /**
             * Add active class.
             */
            startTimeout: function(delay) {
                clearInterval(this.timeInterval);
                this.timeInterval = setInterval(this.removeActiveClass, delay);
            },

            /**
             * Returns the current position of the scroll bar
             * as a percent between 0-1
             */
            getCurrentPosition: function() {
                return isNaN(this.currentPosition) ? 0 : this.currentPosition;
            },

            destroy: function() {
                this.unbindEvents();
                this.bar.remove();
            }

        });


        /**
         * Return view class.
         */
        return BaseScrollView;
    }
);