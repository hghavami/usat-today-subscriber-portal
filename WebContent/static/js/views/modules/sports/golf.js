/**
* @fileoverview JS functions for golf.
 * inspired by gallery-carousel.js
* @author kris.steigrwald@gmail.com
**/
define(['jquery', 'underscore', 'backbone','pubsub', 'app', 'views/modules/loader', 'state'],
function($, _,Backbone,PubSub, App,  Loader, StateManager) {
    var Golf = Backbone.View.extend({
        el: '#section_sports',

          /**
             * Events
             */
            events: {
                'change #page_type select#golf-tour' : 'select'
            },

            /**
             * Initialize view.
             * @param {Object} options View options passed in during init.
             */
            initialize: function(options) {
                this.$content = this.$el.find('article.golf')
                this.$tour = tour_list = this.$el.find('#golf-tour')
                this.tour = (function() {
                    for (var i=0, item; item = tour_list.children()[i]; i++){
                        if(App.attributes.currentCardPath.match(item.value)) return item.value
                    }
                })();    
                this.loader = new Loader({
                    el: this.$content,
                    msg: 'Loading...'
                });
            },

            /**
             * Clean up view.
             * Removes event handlers and element (optionally).
             * @param {boolean} removeEl option to also remove View from DOM.
             */
            destroy: function(removeEl) {
                this.undelegateEvents();
                this.loader.destroy(removeEl);
                if (removeEl) this.remove();
            },
            /**
             * Toggle between tours
             * @param {Event} event View change event.
             */
            select: function(event) {
                var _this = this,
                curr_path = App.attributes.currentCardPath,
                   $index = $(event.currentTarget),
                     path = curr_path.replace(curr_path.match(this.tour), $index.val())
                     
                if (Modernizr.history){
                    Backbone.history.navigate(path, {trigger: true});
                } else {
                    window.location = path ;
                }

                return false;
            }
        });
    return Golf;
});
