/**
* @fileoverview JS functions for sports scores.
* @author webninjataylor@gmail.com (Taylor Johnson)
*/
define(['jquery', 'underscore', 'backbone'],
    function($, _, Backbone) {

        /**
         * View class.
         */
        var ScoresView = Backbone.View.extend({

        	// DOM element.
        	el: $('#scores'),

        	// Events.
            events: {
                'click': 'scoresSuspender',
                'change': 'scoresSuspender'
            },

            initialize: function() {
                this.$pairings = this.$el.find('#pairings');
            },

            scoresSuspender: function(event) {
                if(event.target.id == 'upbar'){
                    var scroll = this.$pairings.scrollTop();
                    var h = this.$pairings.height()+18;
                    this.$pairings.animate({'scrollTop': scroll-h},1000);

                };
                if(event.target.id == 'downbar'){
                    var scroll = this.$pairings.scrollTop();
                    var h = this.$pairings.height()+18;
                    this.$pairings.animate({'scrollTop': scroll+h},1000);
                };
                if(event.target.nodeName == 'SELECT'){
                    var league = this.$el.find('.filter').val();
                    this.$el.find('.league').hide();
                    this.$el.find('.'+league).show();
                };
            },

            destroy: function() {
              this.undelegateEvents();
            }
        });

        /**
         * Create object and return it.
         */
        var league = $('#scores').find('.filter').val();
        $('#scores').find('.league').hide();
        $('#scores').find('.'+league).show();
        return ScoresView;
    }
);