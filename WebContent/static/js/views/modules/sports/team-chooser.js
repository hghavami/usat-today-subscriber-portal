/**
* @fileoverview JS function for scrolling sports pages back to the top.
* @author nolawi.petros@gmail.com (Nolawi)
*/
define(['jquery', 'underscore', 'backbone'],
    function($, _, Backbone) {

        /**
         * View class.
         */
        var TeamChooser = Backbone.View.extend({

        	// DOM element.
        	el: '#section_sports',

        	// Events.
            events: {
                'click #team-chooser': 'onTeamPickerOpen',
                'click span.close': 'onTeamPickerClose',
            },


            initialize: function() {
                this.$teamPicker = this.$el.find('#team-picker, #popupfilmlayer');
            },

            onTeamPickerOpen: function(e) {
                e.preventDefault();
                this.$teamPicker.show();
            },

            onTeamPickerClose: function(e) {
                e.preventDefault();
                this.$teamPicker.hide();
            },


            destroy: function() {
                this.undelegateEvents();
            }

        });

        /**
         * Create object and return it.
         */
        return TeamChooser;
    }
);
