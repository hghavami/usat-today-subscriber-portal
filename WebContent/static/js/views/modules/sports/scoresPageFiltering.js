/**
* @fileoverview JS functions for scores page filtering.
* @author webninjataylor@gmail.com
*/
define(['jquery', 'underscore', 'backbone'],
    function($, _, Backbone) {
        /**
         * View class.
         */
        var scoresPageFilteringView = Backbone.View.extend({
            // DOM element.
            el: '#section_sports',

            // Events.
            events: {
                'change select.filterweek': 'filter'
            },
            /**
            * Initialize view.
            */
            initialize: function() {
                this.$filter = this.$el.find('select.filterweek');
            },

            filter: function(e) {
                var league = $('#section_sports').data('subsection-id');
                var timeframe = this.$filter.val();
                document.location.replace('/sports/'+league+'/scores/'+timeframe+'/');
            },

            destroy: function() {
                this.undelegateEvents();
            }
        });
        /**
         * Return view class.
         */
        return scoresPageFilteringView;
    }
);
