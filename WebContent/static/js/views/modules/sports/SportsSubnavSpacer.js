/**
* @fileoverview JS functions for sports navigation.
* @author  nolawi.petros@gmail.com (Nolawi)
*/
define(['jquery', 'underscore', 'backbone'],
    function($, _, Backbone) {

        /**
         * View class.
         */
        var Subnav = Backbone.View.extend({

             el: '#sports_nav_cont',

            // Events.
            events: {
             'mouseover .navItem' : 'navItemHover',
             'mouseout .navItem' : 'navItemHoverRemove',
                       
            },

            // Initialize view.
            initialize:  function() {
            },

            // Space hover effect. 
            navItemHover: function() {
                $('.spacer').addClass("showing");
            },

            // Remover Space hover effect on mouseout. 
            navItemHoverRemove: function() {
                $('.spacer').removeClass("showing");
            },
            
            destroy: function() {
              this.undelegateEvents();
            }

        });

        /**
         * Create object and return it.
         */

        return Subnav;
    }
);

 