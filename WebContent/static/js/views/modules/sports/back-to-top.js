/**
* @fileoverview JS function for scrolling sports pages back to the top.
* @author webninjataylor@gmail.com (Taylor Johnson)
*/
define(['jquery', 'underscore', 'backbone'],
    function($, _, Backbone) {

        /**
         * View class.
         */
        var BackToTopView = Backbone.View.extend({

        	// DOM element.
        	el: '#section_sports',

        	// Events.
            events: {
                'click .backtotop': 'backToTop'
            },

            initialize: function() {
            },

            backToTop: function(event) {
                $('body,html').animate({'scrollTop': 0},1000);
            }, 

            destroy: function() {
                this.undelegateEvents();
            }

        });

        /**
         * Create object and return it.
         */
        return BackToTopView;
    }
);
