/**
* @fileoverview JS functions for league indexes.
* @author de.lorie@gmail.com
*/
define(['jquery', 'underscore', 'backbone'],
    function($, _, Backbone) {

        var TeamScheduleView = Backbone.View.extend({
            el: '#section_sports',

            events: {
                'change select#year': 'onSeasonalFilter',
                'change select#week': 'onWeekFilter',
                'click label[for="topplayers"]': 'onSeasonalFilter',
                'click label[for="topteams"]': 'onSeasonalFilter',
             },

            initialize: function(path, sectionPath) {
                this.$league = this.$el.data('subsection-id');
                this.$pageType = this.$el.find('> article').data('page');
            },


            onSeasonalFilter: function(e) {
                e.preventDefault();
                var $teamNow = this.$el.find('> article').data('team'),
                    $year = this.$el.find('#year').val(),
                    $pageType = this.$pageType;

                var $teamNew = $(e.currentTarget).data('team') !== undefined ?
                        $(e.currentTarget).data('team') : $teamNow;

                var $season = $year !== "" ? $year.split('|')[0] : "";
                var $subseason = $year.split('|').length > 1 ?
                    $year.split('|')[1] : "";
  
                if ($(e.currentTarget).attr('for') == "topplayers" || ($('#topplayers').is(':checked') && $teamNew != $teamNow)){
                    $pageType = ["player",$pageType].join('/');
                }

                var urlArgs = ['/sports', this.$league, $teamNew, $pageType, $season, $subseason];

                //Filters out the null,undefined,empty strings, and join for url path
                urlArgs = urlArgs.filter(function(e){return e;});
                var url = urlArgs.join('/');

                if (Modernizr.history){
                    Backbone.history.navigate(url, {trigger: true});
                } else {
                    window.location = url;
                }
            },

            onWeekFilter: function(e) {
                var toggle = this.$el.find('#week').val(),
                    $weeks = this.$el.find('div[data-tags^="WEEK-"]'),
                    $week = this.$el.find('div[data-tags="'+toggle+'"]');

                if(toggle === '')
                    $weeks.show();
                else {
                    $weeks.hide();
                    $week.show();
                }
            },

            destroy: function() {
              this.undelegateEvents();
            }
            
        });

        return TeamScheduleView;
    }
);