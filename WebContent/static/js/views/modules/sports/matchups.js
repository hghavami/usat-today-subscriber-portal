/**
* @fileoverview JS functions for team matchups.
* @author tdljackson@usatoday.com
**/
define(['jquery', 'underscore', 'backbone','pubsub','chosen'],
function($, _,Backbone,PubSub) {
    var Matchups = Backbone.View.extend({
        el: '#matchup',

        initialize: function(path, sectionPath) {
                this.matchupStatGroup = this.$el.find('div.matchupStatGroup');
                this.matchupFooterGraph = this.$el.find('div#matchupFooterGraphDiv');
                this.buildGraph();
        },
        // TODO: DRY this...
        buildGraph: function(e) {
            var templateResult = "";
            var fillerCodeArray=[this.matchupStatGroup.size()];
            this.matchupStatGroup.each(function(key, value) { 
                var awayStat = parseFloat($(value).find('.matchupAway-stat').html());
                var homeStat = parseFloat($(value).find('.matchupHome-stat').html());
                var statMax = $(value).find('.matchupStat-max').html();
                var statLabel = $(value).find('.matchupStat-label').html();      
                var leftPercent = awayStat / statMax * 100;
                var rightPercent = homeStat / statMax * 100;
                var matchupAwayBarColor = "rgb(255,0,0)";
                var matchupHomeBarColor = "rgb(0,0,255)";
                fillerCodeArray[key] = {"leftPercent": leftPercent, "rightPercent": rightPercent, "matchupAwayBarColor": matchupAwayBarColor, "matchupHomeBarColor": matchupHomeBarColor, "statLabel": statLabel};
                var matchTemplate = _.template($("#matchupStatBarTemplate").text());
                templateResult = templateResult + matchTemplate(fillerCodeArray[key]);

            });

            this.matchupFooterGraph.html(templateResult);
        },

        destroy: function() {
            this.undelegateEvents();
        }

    });

    return Matchups;
});
