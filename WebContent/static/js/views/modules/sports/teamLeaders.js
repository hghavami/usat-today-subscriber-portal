/**
* @fileoverview JS functions for sports team statistics.
* @author kklee@usatoday.com (Kent Lee)
*/
define([
    'jquery',
    'underscore', 
    'backbone',
    'app',
    'pubsub',
    'views/modules/loader'
    ],
    function($, _, Backbone, App, PubSub, Loader) {

        var TeamLeadersView = Backbone.View.extend({

            // DOM element.
            el: $('#leaders-view li'),

            // Events.
            events: {
                'click': 'leadersView'
            },

            initialize: function() {
                $('#leaders-view li:first').addClass('selected');
                $("#leaderContainer > div").hide();
                $("#leaderContainer div:first").show();
            },

            leadersView: function(event) {
                var $item = $(event.currentTarget);
                var index = $item.index();
                var currentClass = '#'+$item.find('a').attr('class');
                this.$el.removeClass('selected');
                $item.addClass('selected');
                $('.leaders-container').hide();
                $(currentClass).show();
                $("#leaderContainer > div").hide();
                $(currentClass + "-info").show();
            },

            destroy: function() {
                this.undelegateEvents();
            }
        });

        return TeamLeadersView;
    }
);
