/**
* @fileoverview JS functions for league index of teams.
* @author kris.steigerwald@gmail.com
**/
define(['jquery', 'underscore', 'backbone','pubsub','chosen'],
function($, _,Backbone,PubSub) {
    var LeaguesView = Backbone.View.extend({
        el: '#section_sports',

        events: {
            'change select#conference_selector': 'onSelectConference',
            'change select#division_selector': 'onSelectDivision',
            'change select#ncaaf_div_selector': 'onNCAAFChange'
        },

        initialize: function(path, sectionPath) {
            this.$teams = this.$el.find('div.teams');
            this.$ncaaf = this.$el.find('#ncaaf_div_selector');
            
        },
        // TODO: DRY this...
        onSelectConference: function(e) {

            var toggle = $(e.currentTarget).val(),
                $conf = $('div[data-conf="'+toggle+'"]');

            if(toggle == 'all')
                this.$teams.show();
            else {
                this.$teams.hide();
                $conf.show();
            }
            
        },
        onSelectDivision: function(e) {
            var toggle = $(e.currentTarget).val(), 
                $div = $('div[data-div="'+toggle+'"]');
            if(toggle == 'all')
              this.$teams.show()
            else {
              this.$teams.hide()
              $div.show()
            }
        },
        destroy: function() {
            this.undelegateEvents();
        },
        onNCAAFChange: function(event) {
            var _this = this,
             curr_path = App.attributes.currentCardPath,
                $index = $(event.currentTarget),
                  path = _.template('sports/<%= division %>/teams/',{ division: $index.val() });
                  
             if (Modernizr.history){
                 Backbone.history.navigate(path, {trigger: true});
             } else {
                 window.location = path ;
             }

             return false;
        }

    });

    return LeaguesView;
});
