/**
* @fileoverview JS functions for sports team statistics.
* @author kklee@usatoday.com (Kent Lee)
*/
define([
    'jquery',
    'underscore', 
    'backbone',
    'app',
    'pubsub',
    'views/modules/loader',
    'libs/flot/jquery.flot',
    'libs/flot/styleAssets'
    ],
    function($, _, Backbone, App, PubSub, Loader) {

        var TeamStatsView = Backbone.View.extend({

        	el: $('#section_sports #stats-view li'),

            events: {
                'click': 'statsView',
            },

            initialize: function() {
                var self = this;
                self.box = $('.chart-wrapper');
                
                self.box.hide();
                self.box.first().show();
                self.createBar('#' + self.box.first().attr('id'));

                // if not li:selected, classify the first
                if (self.$el.parent().find('li.selected').length == 0)
                    self.$el.first().addClass('selected');
            },

            statsView: function(event) {
                var $item = $(event.currentTarget);
                var index = $item.index();
                this.$el.removeClass('selected');
                $item.addClass('selected');

                this.box.hide();    // hide works fine on event
                //this.box.css(this.boxcss);

                activeBox = '#' + $item.find('a').attr('class'); 
                $(activeBox).show();
                this.createBar(activeBox);
            },

            createTicks: function(tickData) {
                var labelArray = tickData.split(","),
                newArray = [];
                $.each(labelArray,function (index, label) {
                    newArray.push(label)
                });
                return newArray ;
            },

            createBar: function(el) {
                var self = this,
                    _graph = $(el+' .chart-graph');

                if (_graph.find('table.data').length == 0)
                    return; // do not plot is already drawn

                require([
                        'libs/flot/jquery.flot.multibars',
                        'libs/flot/jquery.flot.usat.style'
                        ], function () {
                    var data = [];
                    colorId = $(el).data('section');
                    colors = chartAssets.color[colorId];
                    tickLabels = ($(el).data("xaxis") != "") ? self.createTicks($(el).data("xaxis")) : null;

                    $(el+" .data tr").each(function (index) {
                        var row = $(el+" tr:eq(" + index + ")"),
                        label = row.find("td:eq(0)").text(),    //find the label, which is first td
                        innerData = [];
                        // find each td.coord within each row and split into an x and y axis
                        row.find("td.coord").each(function () {
                            var coordsSrc = ($(this).text()).split(',');
                            innerData.push([ coordsSrc[0], coordsSrc[1]] );           
                        });

                        data.push({
                            label: label,
                            data: innerData,
                            color:  "rgba(255,255,255,1)"  ,
                            bars: { borderWidth: 30,  fillColor: colors[index]  }  
                        })
                    });
 
                    var p = $.plot($(_graph), data, {
                        multiplebars: true,
                        legend: {
                            show: false,
                        },
                        xaxis: {
                            ticks: tickLabels,
                            tickLength: 0,
                            tickDecimals: 0 
                        },
                        yaxis: {
                            position: "left",
                            labelHeight: 25
                        },
                        series: {
                            bars: {
                                show: true,
                                barWidth: 0.4,
                                align: "center"
                            }
                        },
                        grid: {
                            borderWidth: 0,
                            labelMargin: 5,
                            axisMargin: 0,
                            clickable: true,
                            hoverable: true,
                            autoHighlight: true
                        },
                        colors: colors,
                        pattern: chartAssets.createPattern()
                    });
                });
                _graph.bind("plothover", self.barHover);
            },

            barHover: function(event, pos, obj) {
                var chartArea = $(this);
                var chartBox = chartArea.parent().parent().parent().attr('id');
                
                var chartHover = $('#' + chartBox + ' .chart-hover'),
                    chartWidth = $('#' + chartBox).width(),
                    chartLeft = $('#' + chartBox).offset().left,
                    chartTop = $('#'+ chartBox).offset().top

                if (!obj){ 
                    chartHover.hide();
                    return
                }else{
                    chartHover.show();
                }

                var percent = parseFloat(obj.series.percent).toFixed(2),
                    top = chartArea.position().top + (chartArea.height() / 3) + chartTop,
                    areaLeft = chartLeft + chartHover.width(),
                    areaRight = chartLeft + chartWidth - (chartHover.width() / 2),
                    bottom =  chartArea.position().top + ((chartArea.height() / 3) * 2) + chartTop,
                    hoverClass = "",
                    chartHoverHeight = chartHover.height(),
                    chartHoverWidth = chartHover.width();

                if (pos.pageX < areaLeft) {
                    hoverClass = "middleleft-hover";
                    pointerClass = "pointer-left";
                    hoverCss = {
                        top: pos.pageY - (chartHoverHeight / 2) - chartTop,
                        left: pos.pageX - chartLeft + 20
                    };
                } else if (pos.pageX > areaRight) {
                    hoverClass = "middleright-hover";
                    pointerClass = "pointer-right";
                    hoverCss = {
                        top: pos.pageY - (chartHoverHeight / 2) - chartTop,
                        left: pos.pageX - chartHoverWidth - chartLeft - 20
                    };
                } else if (pos.pageY < top) {
                    hoverClass = "top-hover";
                    pointerClass = "pointer-top";
                    hoverCss = {
                        top: pos.pageY - chartTop + 20,
                        left: pos.pageX - (chartHoverWidth / 2) - chartLeft
                    };
                } else if (pos.pageY > top) {
                    hoverClass = "bottom-hover";
                    pointerClass = "pointer-bottom";
                    hoverCss = {
                        top: pos.pageY - chartHoverHeight - chartTop - 20,
                        left: pos.pageX - (chartHoverWidth / 2) - chartLeft
                    };
                } else {
                    chartHover.hide();
                }
                content = "<div class='" + pointerClass + "'></div><h3>" + obj.series.label +
                    "</h3><h4 class='sports'>" + obj.datapoint[1] + "</h4><p></p>";
                chartHover.css(hoverCss).html(content).attr("class", hoverClass + " chart-hover");
            },

            destroy: function() {
                this.undelegateEvents();
            }
        });

        return TeamStatsView;
    }
);

