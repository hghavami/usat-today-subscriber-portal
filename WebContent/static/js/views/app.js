/**
 * @fileoverview App view.
 * @author Chris Manning
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'app',
    'pubsub',
    'responsiveimages',
    'resizestop',
    'views/modules/header',
    'views/modules/footer'
],
function(
    $,
    _,
    Backbone,
    App,
    PubSub,
    ResponsiveImages,
    ResizeStop,
    Header,
    Footer
)
    {

        /**
         * View class.
         * Global event listener. Not intended to render an actual view.
         */
        var AppView = Backbone.View.extend({

            // View element.
            el: 'body',

            // Event listeners.
            events: {
                'click': 'triggerRoute',
                'submit form.search': 'triggerSearch'
            },

            /**
             * Initialize view.
             * @param {Object} options View options passed in during init.
             */
            initialize: function(options) {
                // Avoid console errors.
                var console = {};
                if(typeof window.console === 'undefined' || typeof window.console.log === 'undefined') {
                    console.log = console.error = console.info = console.debug = console.warn = console.trace = console.dir = console.dirxml = console.group = console.groupEnd = console.time = console.timeEnd = console.assert = console.profile = function() {};
                    window.console = console;
                } else {
                    console = window.console;
                }

                // Event Proxy
                window.appViewEventProxy = PubSub;

                // Set global properties/lookups.
                this.DEBUG = App.get('DEBUG');

                // Router reference.
                this.router = options.router;

                // Initialize global header.
                var header = window.Header = new Header();

                // Responsive images plugin.
                $(window).bind('resizestop', function(event) {
                    // Fire resize event.
                    PubSub.trigger('resize:app');
                    $.responsiveImages();
                }).trigger('resizestop');

                // Start global timer.
                this.globalTimer();
            },

            /**
             * Initialize a global timer.
             */
            globalTimer: function() {
                if(this.timer) {
                    clearInterval(this.timer);
                }

                this.timer = setInterval(function() {
                    PubSub.trigger('timer:tick:app');
                }, this.options.idleTimer || 1000000);

            },

            /**
             * Check if fragment matches defined 'Router' routes.
             * NOTE: using internal Backbone.history functions -- when upgrading
             * Backbone, must ensure these functions stil exist.
             * @param {string} fragment Url fragment to test.
             */
            isDefinedRoute: function(fragment) {
                if (fragment === '#') return false;
                var fragment = Backbone.history.getFragment(fragment);
                var matched = _.any(Backbone.history.handlers, function(handler) {
                    if (handler.route.test(fragment)) return true;
                });
                return matched;
            },

            /**
             * Handle global links.
             * @param {Event} e Link click event.
             */
            triggerRoute: function(e){
                var $targetLink = $(e.target).closest('a'),
                    href;
                if (!$targetLink.length) return;
                href = $.trim($targetLink.attr('href'));

                App.setHeatTrack($targetLink);
                if (href){
                    if (href[0] === '#'){
                        // anchor hrefs!
                        if (href.length === 1){
                            // disable jump to top ('#' only in href)
                            return false;
                        }
                        // let the browser deal with it
                        return;
                    }
                    if (href.indexOf('../') !== -1 || (href[0] !== '/' && href.indexOf('://') === -1)){
                        console.error('Attempting to load a relative url, bad code monkey! (' + href + ')');
                        return false;
                    }
                    if(Modernizr.history && this.isDefinedRoute(href)){
                        this.router.navigate(href, true);
                        return false;
                    } else if(!$targetLink.attr('target')) {
                        window.location = href;
                    }
                }
            },

            /**
             * Handle global search form submits.
             * @param {Event} e Link click event.
             */
            triggerSearch: function(e){
                var href = '/search/' + encodeURI($(e.currentTarget['q']).val()) + '/';
                if(Modernizr.history && this.isDefinedRoute(href)){
                    e.preventDefault();
                    this.router.navigate(href, {trigger: true});
                }
            }
        });

        /**
         * Return view class.
         */
        return AppView;
    }
);
