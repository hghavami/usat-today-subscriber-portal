/**
 * Library to handle calls to/from Facebook for various service interactions.
 */
define([
    'jquery',
    'underscore',
    'pubsub',
    'fwinfo'
],
function(
    $,
    _,
    PubSub,
    FwInfo
)
    {
        var _config= site_config.AUTH.FACEBOOK,
            _appid= _config.APPID,
            _channelUrl= _config.CHANNELURL,
            _authResponse= undefined,
            _user= {},
            _perms= _config.PERMISSIONS;
        require(['//connect.facebook.net/en_US/all.js'], function() {
            FB.init({
                appId      : _appid,
                channelUrl : _channelUrl,
                status     : true,  // facebook will check if logged in
                cookie     : true,  // yes we want to use cookies
                xfbml      : true  // yes we want to parse XFBML
            });
            FB.Event.subscribe('auth.authResponseChange', function(response) {
                _authResponse= response;
                PubSub.trigger('auth.authResponseChange', response);
            });
        });
        PubSub.on('auth.authResponseChange', facebookAuthResponseChange);
        PubSub.on('auth:user', facebookHeaderUpdate);
        PubSub.on('page:load', facebookCheckLoginStatus);
        function facebookAuthResponseChange(response) {
            if (response.status == "connected" && response.authResponse != null) 
                facebookLogin(response.authResponse);
            else 
                facebookLogout(response.authResponse);
        }
        function facebookLogout(authResponse) {
            // DOM manip
            _authResponse= undefined;
            _user = {};
            if($('body').hasClass('fbauth')) 
                $('body').removeClass('fbauth');
        }
        function facebookLogin(authResponse) {
            _authResponse= authResponse;
            // DOM manip
            if(!$('body').hasClass('fbauth')) 
                $('body').addClass('fbauth');
            //Query the FB photo from the FB API
            FB.api({
                    method: 'fql.query',
                    query: 'SELECT name, pic_square FROM user WHERE uid= '+authResponse.userID
                }, function(apiResponse) {
                    _user.name = apiResponse[0].name;
                    _user.photoSmall = apiResponse[0].pic_square;
                    PubSub.trigger('auth:user', apiResponse[0]);
            });
            $.ajax({
                    url: '/facebook/tokens/'+authResponse.accessToken,
                    type: 'put',
                    data: {
                        accessExpires: authResponse.expiresIn,
                        accessToken: authResponse.accessToken,
                        userID: authResponse.userID
                    },
                    success: function(r){
                        // FIXME -- rememeber useful stuff from r.success if we need it
                    },
                    error: function(){}
            })
            //if user is logging in on the User Auth Page, redirect him or her back to content
            if(document.location.pathname.toLowerCase() == '/userauth/default/') {
                console.log("user auth'd! about to go back!");
                history.back();
            }
        }
        function facebookHeaderUpdate() {
                $('.fb-avatar').attr( { 
                        src: _user.photoSmall,
                        height: 22,
                        width: 22
                });
        }
        function facebookCheckLoginStatus() {
                FB.getLoginStatus(function(response) { facebookAuthResponseChange(response); }, true);
        }
        var Facebook= {
            init: function() {
            },
            login: function(cb) {
                FB.login(cb, { scope : _perms } );
            },
            logout: function() {
                FB.logout(function(response) {
                    // user is now logged out
                });
            },
            parse: function() {
                FB.XFBML.parse();
            },
            permissions: function() {
                return _perms;
            },
            status: function() {
                if(!_.isUndefined(_authResponse)) {
                    return _authResponse.status;
                }
                else {
                    return false;
                }
            }
        };
        _.extend(Facebook, Backbone.Events);
        return Facebook;
    }
)
