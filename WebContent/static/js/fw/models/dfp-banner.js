/**
 * @DFPADModel .
 * Ad model specific to DFP
 * @author Shyam Sedamkar
 */
define([
	'jquery',
	'backbone', 
	'pubsub', 
	'app', 
	'fwinfo'
], 
function(
	jQuery,
	Backbone, 
	PubSub, 
	App, 
	FwInfo
) {
	
	var _ready = false;
	
	var _enabled = false;
	
	var _accountid = FwInfo.adconfig('accountid');
	

	var _accountname = FwInfo.adconfig('accountname');
	var _fwinfo =FwInfo;
	 
	var _calls = [];
	
	var _slots = [];
	
	var _valid = { // stuff we are willing to tell google about
		zip: 1,
		yob: 1,
		gender: 1,
		segments: 1
	};
	
	var _bannerDfpClass= Backbone.Model.extend({
		
		checkForAssetTransition : 0,

		destroy: function() {
			this.undelegateEvents();
		},

		enablePubService: function() {
			// Setup the DFP call
			if (!_enabled) {
				_enabled = true;
				var pubService = googletag.pubads();
				// Collapse Empty Divs is important for hiding of ads
				pubService.collapseEmptyDivs();
				googletag.enableServices();
			}
		},

		/**
		 * Worker function that searches for partner placeholders and calls Google
		 */
		go: function(controller, page) {
			// Grab targets
			var $targets = [];

			switch (page.contentParentId) {
				case 'card':
					var $cards = $('article.cards', document);
					$targets = $('.partner-bundle', $cards);
					break;
				case 'asset':
					$targets = $('.content-wrap.active article.asset .partner.poster', document);
					this.checkForAssetTransition = FwInfo.assetTransition();
					break;
			}

			if ($targets.length == 0 && !this.checkForAssetTransition) {
				// console.log('No partner positions on page');
				return false;
			}

			// reset
			_calls = [];
			_slots = [];

			// User targeting
			var kw = document.cookie.match(/\bkw=([^;]*)/) && RegExp.$1 || '';
			this.setUserTargeting({kw: kw});
			this.setUserTargeting({segments: _fwinfo.getrevscisegements()});

			this.enablePubService();

			// process the ads
			var me = this;
			$targets.each(function() {
				me.makeCall(controller, $(this), page);
			});

			// asset transition
			if (this.checkForAssetTransition) {
				// ad call
				me.makeOutOfPageCall(controller, page, 'asset_transition');
				// reset
				this.checkForAssetTransition = 0;
			}

			return true;
		},

		initialize: function() {
			
		},

		/**
		 * Does the individual ad call for Google
		 */
		makeCall: function(controller, $el, page) {
			
			// reset the $el
			var html = '';
			if (page.contentParentId == 'asset') {
				html = '<span>Advertisement</span>';
			}
			$el.html(html);

			// Grab the position from the $el
			var position = $el.data('position');

			//Get the AD cst value from fwinfo
			var cst = _fwinfo.getadcst();

			// Determine Ad Unit
			var adunit = _accountid + '/' + _accountname + '/' + position + '/' + cst;

			// Set an ID for Google
			var id = 'partner_' + cst.replace(/\//g,'') + '_' + position + '_' + new Date().getTime();
			$el.attr('id', id);

			// Grab the sizes
			var sizes = controller.getSizesForPosition(position);

			if (page.sectionPath == 'home') {
				sizes = controller.getSizesForPosition('home_high_impact');
			}

			// Save the call
			_calls.push([adunit, sizes, id]);
			
			// Talk to Google
			googletag.cmd.push(function() {
				// get the Slot
				var slot = googletag.defineSlot(adunit, sizes, id).addService(googletag.pubads());

				// Tell the partner banner view about ad if is on the front, because it is a slider
				if ($el.data('position').indexOf('high_impact') != -1) {
					controller.listenForAd($el, slot);
				}

				// document and display
				_slots.push([slot, adunit, sizes, id]);
				googletag.display(id);
			});

		},

		makeOutOfPageCall: function(controller, page, position) {
			//Get the AD cst value from fwinfo
			var cst = _fwinfo.getadcst();
			// Determine Ad Unit
			var adunit = _accountid + '/' + _accountname + '/' + position + '/' + cst;

			// Set an ID for Google
			var id = 'partner_' + cst.replace(/\//g,'') + '_' + position + '_' + new Date().getTime();

			// Put div on page
			var variables = {
	            'id' : id,
	            'position' : position
	        }
	        var template = _.template($("#partner_overlay_template").html(), variables);
	        $(template).appendTo(App.get('body'));

	        // Grab the sizes
			var sizes = controller.getSizesForPosition('elastic');

			// Save the call
			_calls.push([adunit, sizes, id]);

			// Talk to Google
			googletag.cmd.push(function() {
				slot = googletag.defineOutOfPageSlot(adunit, id).addService(googletag.pubads());
				_slots.push([slot, adunit, sizes, id]);
				googletag.display(id);
			});
			
		},

		preloadGalleryTransition: function(controller, $div, cst) {
			this.enablePubService();
            cst = cst || '';
			var adunit = _accountid + '/' + _accountname + '/transition/' + cst;
			var id = 'partner_' + cst.replace(/\//g,'') + '_transition_' + new Date().getTime();
			$div.attr('id', id);
			var sizes = controller.getSizesForPosition('elastic');
			_calls.push([adunit, sizes, id]);
			googletag.cmd.push(function() {
				slot = googletag.defineOutOfPageSlot(adunit, id).addService(googletag.pubads());
				_slots.push([slot, adunit, sizes, id]);
				googletag.display(id);
			});
		},

		refreshAd: function(slot) {
			googletag.pubads().refresh(slot);
		},

		/**
		 * this function refreshes the ad for the provided slot
		 * @param{slot} 
		 */ 
		refreshAds: function() {
			var slots = _slots;
			googletag.cmd.push(function() {
				for (var j= 0; slots < slots.length; j++) {
					googletag.pubads().refresh(slots[j][0]);
				}
			});
		},

		/**
		 * this function sets key values related to user info to googletags
		 * @param{keyvalues} 
		 */
		setUserTargeting: function(data) {
			var me = this;
			if (!data) {
				return;
			}
			var pubAds = googletag.pubads();
			for (var key in data) {
				if (_valid[key]) {
					me.setTargeting(pubAds, key, me.splitBar(data[key]));
				}
			}
		},

		setTargeting: function(pubads, name, value) {
			googletag.cmd.push(function() {
				pubads.setTargeting(name, value);
			});
		},

		/**
		 * treat | separated value as an array of values
		 * @param(value)
		 */
		splitBar: function(item) {
			if (item.split) {
				var r= item.split('|');
				if (r.length) return r;
			}
			return item;
		}
	});

	var bannerDfpView = new _bannerDfpClass();
	
	return bannerDfpView;
});
