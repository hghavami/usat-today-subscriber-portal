/**
 * @GenericADModel .
  * this model will help us with testing banner model without any adprovider.
 * @author Shyam sedamkar
 */
define(['jquery','backbone'], function(jQuery,Backbone) {
 	var _bannerGenericClass=   Backbone.Model.extend({
			  

              initialize: function() {
                    },
              
            
             /*
             * this is generic ad call 
               * @param{Genericmodel,element} 
              */ 
              makeADcall:function(globalmodel,elem){
 
                  var randomcolor = ["red","blue","black","green","pink"],
                      randomnumber=Math.floor(Math.random()*4),
                       parentele =$(elem).parent(),
                      sizearr;

                      $(parentele).css("background-color",randomcolor[randomnumber]);
                      $(parentele).html("<span class='red'>AD <b>Again</b></span>");
                        
                          
                      sizearr = globalmodel.sizelist[0].split('x');
                      $(parentele).css('width', function() {
                            return  sizearr[0];
                      });
                      $(parentele).css('height', function(index) {
                            return sizearr[1];
                      });
                      // console.log("globalmodel.sizelist"+globalmodel.sizelist)
                   return true;
              }
			 
	});
	var _bannerGeneric= new _bannerGenericClass();
	return _bannerGeneric;
});
