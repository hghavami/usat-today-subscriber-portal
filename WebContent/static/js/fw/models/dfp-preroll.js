/**
 * @DFPADModel .
 * Ad model specific to DFP
 * @author Shyam sedamkar
 */
define(['jquery','backbone', 'pubsub', 'fwinfo'], function(jQuery,Backbone, PubSub, FwInfo) {
	var _ready= false;
	var _accountid= FwInfo.adconfig('accountid');
	var _accountname= FwInfo.adconfig('accountname');
	var _cst = FwInfo.getadcst();

	var _prerollDfpClass= Backbone.Model.extend({
		initialize: function() {
			// console.log('dfp-preroll initialize');
		},

		setVideoAdurl:function($el, page){
			/*var adServerURL = "http://pubads.g.doubleclick.net/gampad/ads?env=vp&gdfp_req=1&impl=s&output=xml_vast2&iu=/"+_accountid+"/"+_accountname+"/Relaunch_Preroll_Temp/"+_cst+"&sz=640x480&unviewed_position_start=1&url=";*/
// console.log("adServerUrl"+adServerURL);
			/*$el.each(function(){
				$(this).prepend('<param name="adServerURL" value="'+adServerURL+'">');
			})*/
		}
	});
	var THIS= new _prerollDfpClass();
	return THIS;
});
