define(['jquery', 'underscore', 'backbone', 'pubsub'], function($, _, Backbone, PubSub) {
    var _currentPage = {},
        _cookieRECdelim = '~',
        _cookieNAMdelim = ':',
        _localcache = null,
        _cstdata = 'undefined',
	// _fbobj,
	_localsegs = [],
        _lastClickDate= new Date(),
        _recordClick= function() {
            _lastClickDate= new Date();        
        },
        _asset = {
            isTransition: 0,
            here: 'unknown', // needed only for bandage
            n: 0, /* n: how many pages have we seen? */
            card: 'unknown',
            j: 0, /* j: what's our current threshold index? */
            t: site_config.ADS.thresholds.asset[0], /* t: whats our next threshold total? */
            thresholds: site_config.ADS.thresholds.asset
        },
        _slide = {
            isTransition: 0,
            here: 'none',
            gallery_id: 'none',
            slide_id: 'none' // needed only for bandage
        };

    function _unpack_fwutil() {
        if (_localcache) {
            return;
        }
        _localcache = {};
        if (!document.cookie.match(/\bfwutil=([^\;]*)/)) {
            return;
        }
        var pairs = RegExp.$1.split(_cookieRECdelim),
            j, pair, pairs_length = pairs.length,
            pair_length;
        for (j = 0; j < pairs_length; j++) {
            pair = pairs[j].split(_cookieNAMdelim);
            pair_length = pair.length;
            if (2 == pair_length) {
                _localcache[pair[0]] = decodeURIComponent(pair[1]);
            }
        }
    }

    function _repack_fwutil() {
        var c = 'fwutil=', /* cookie string */
            d = '', /* current delimiter */
            p; /* current property name */
        for (p in _localcache) {
            if (_localcache.hasOwnProperty(p)) {
                c += d + p + _cookieNAMdelim + encodeURIComponent(_localcache[p]);
                d = _cookieRECdelim;
            }
        }
        document.cookie = c+';path=/';
    }

    function _advance(state, thresholdEvent, targeting) {
        if (state.t <= ++state.n) {
            j = ++state.j;
            delta = state.thresholds[j];
            if (0 > delta) {
                j = state.j += delta;
                delta = state.thresholds[j];
            }
            state.t += state.thresholds[j];
            state.isTransition= 1;
            PubSub.trigger(thresholdEvent, targeting, _lastClickDate);
        }
    }


    var _fwInfo = new(Backbone.Model.extend({
        initialize: function() {
            PubSub.on('page:load', this.page_changed);
            PubSub.on('slide:change', this.slide_changed);
            PubSub.on('heattrack', _recordClick);
            PubSub.on('uotrack', _recordClick);
        },
        page_changed: function(page) {
            /* begin bandage for bogus duplicate page:load events */
            var here = document.location.href;
            if (here == _asset.here) {
                console.log('ignoring duplicate page:load event in fwinfo (PLEASE FIX)');
                return;
            }
            /* end bandage */
             _cstdata= (page.aws+'').replace(/\/\/*$/,'');
            if ('card' == page.contentParentId) {
                _asset = {
                    isTransition: 0,
                    n: 0, // n: how many pags have we seen?
                    j: 0, // j: what's our current threshold index? 
                    t: site_config.ADS.thresholds.asset[0], // t: what's our next threshold total? 
                    thresholds: site_config.ADS.thresholds.asset
                };
            } else if ('asset' == page.contentParentId) {
                _advance(_asset, 'asset:needtransition', page);
            }
            _asset.here= here; // needed only for bandage
        },
        assetTransition: function() {
            var t= _asset.isTransition;
            _asset.isTransition= 0;
            return t;
        },
        slide_changed: function(slide) {
            /* begin bandage for bogus duplicate slide:change events */
            if (slide.slide_id == _slide.slide_id) {
                console.log('ignoring duplicate slide:change event in fwinfo (PLEASE FIX)'); 
                return;
            }
            /* end bandage */
            var here = document.location.href, j, delta;
            if (slide.gallery_id != _slide.gallery_id || here != _slide.here) {
                _slide = {
                    isTransition: 0,
                    here: here,
                    n: 0, /* n: how many slides have we seen? */
                    gallery_id: slide.gallery_id,
                    j: 0, /* j: what's our current threshold index? */
                    t: site_config.ADS.thresholds.slide[0], /* t: what's our next threshold total? */
                    thresholds: site_config.ADS.thresholds.slide
                };
            } else {
                _advance(_slide, 'slide:needtransition', slide);
            }
            _slide.slide_id = slide.slide_id; // needed only for bandage
        },
        slideTransition: function() {
            var t= _slide.isTransition;
            _slide.isTransition= 0;
            return t;
        },
        adprovider: function() {
            return site_config.ADS.PROVIDER;
        },
        admodel: function(model) {
            return 'fw/models/' + _fwInfo.adprovider().toLowerCase() + '-' + model;
        },
        adconfig: function(key) {
            var provider = site_config.ADS.PROVIDER;
            return site_config.ADS[provider][key.toUpperCase()];
        },
        analyticsconfig: function() {
            return site_config.ANALYTICS;
        },
        setcurrentpage: function(page) {
            _currentPage = page;
        },
        getcurrentpage: function() {
            return _currentPage;
        },
	/*
        setfbuserobject: function(fb) {
            _fbobj = fb;
        },
        getloginstatus: function() {
            if (typeof _fbobj != 'undefined') {
                return _fbobj.loggedIn;
            } else {
                return "";
            }
        },
        getuserfbid: function() {
            if (typeof _fbobj != 'undefined' ) {
                return _fbobj.userID;
            } else {
                return "";
            }
        },
	*/
        getuseruniqueid: function() {
            var _uniqueid = _fwInfo.getsessionvalue("uniqueid");
            if (_uniqueid != null && _uniqueid != "") {
                return _uniqueid;
            } else {
                return "";
            }
        },
        setfmUserInfo:function(fmuser){
            var status,sessiontoken; 
            status=(fmuser.statuses.length>0)?fmuser.statuses.join():"";
            sessiontoken=(fmuser.session_token!="")?fmuser.session_token:null;
             _fwInfo.setsessionvalue("userstatus",status);
             _fwInfo.setsessionvalue("usersessiontoken",sessiontoken);

        },
        getUserStatus:function(){
            return _fwInfo.getsessionvalue("userstatus");

        },
        getUserSessionToken:function(){
            return _fwInfo.getsessionvalue("usersessiontoken");

        },
        getadcst: function() {
            return _cstdata;
        },
        getcsid: function() {
            return "J06575";
        },
        getrevscisegements: function() {
            var _segscookie = "";
            if (_fwInfo._localsegs == undefined) {
                _segscookie = $.cookie("rsi_segs");
                if (_segscookie == null) {
                    return "";
                } else {
                    _fwInfo.segmentsrecieved(_segscookie.split("|"));
                    return _fwInfo._localsegs;
                }
            } else {
                return _fwInfo._localsegs;
            }
        },
        setrevscisegements: function(segs) {
            _fwInfo._localsegs = segs;
        },
        segmentsrecieved: function(rsinetsegs) {
            var _segRe = new RegExp('^' + _fwInfo.getcsid() + '_', 'i'),
                _segs = [],
                i, rsinetsegs_length = rsinetsegs.length,
                a;
            for (i = 0; i < rsinetsegs_length; i++) {
                a = rsinetsegs[i];
                _segs.push(a.replace(_segRe, ''));
            }
            PubSub.trigger('revsegmentsreceived', _segs);
            _fwInfo.setrevscisegements(_segs);
        },

        //Retrieving  session value either from JS cache or cookie
        setsessionvalue: function(key, value) {
            if (!_localcache) {
                _unpack_fwutil();
            }
            _localcache[key] = value;
            _repack_fwutil();
        },
        getsessionvalue: function(key) {
            if (!_localcache) {
                _unpack_fwutil();
            }
            return _localcache[key];
        },
        getandclearsessionvalue: function(key) {
            var r= _fwInfo.getsessionvalue(key);
            if (!r) return null;
            _fwInfo.setsessionvalue(key, '');
            return r;
        }

    }))();
    // preload ad delivery support if no one else has done that yet
    return _fwInfo;
});
