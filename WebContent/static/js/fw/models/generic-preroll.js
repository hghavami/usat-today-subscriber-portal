/**
 * @Preroll Model global.
 * @author Shyam sedamkar
 */
define(['jquery','backbone', 'fwinfo'], function(jQuery,Backbone, FwInfo) {
	var _ready= false;
	var _$el= null;
	var _page= null;
	var _admodel= FwInfo.admodel('preroll');
	var _prerollclass= Backbone.Model.extend({
		//Initialize model
		initialize: function() {
			THIS= this;
			// load 
			require([_admodel], function() {
				_ready= true;
				if (_$el) {
					THIS.setVideoAdurl(_$el, _page);
					_$el= _page= 0;
				}
			});
		},
		  
		/*
		 * sets ad url in video players on page so they can call for preroll ads
		 */ 
		setVideoAdurl: function($el, page){
			// console.log('preroll makeADcall', $el, page);
			if (!_ready) {
				_$el= $el;
				_page= page;
				return false;
			}
			var admodel= FwInfo.admodel('preroll'); 
			require([admodel], function(admodel) {
				// console.log('preroll makeADcall (inner)', admodel);
				admodel.setVideoAdurl($el, page);
			});
			return true;
		}
	});
	var THIS= new _prerollclass();
	return THIS;
});
