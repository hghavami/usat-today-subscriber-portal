/**
 * @Preroll Model global.
 * @author Shyam sedamkar
 */
define(['jquery','backbone', 'fwinfo'], function(jQuery,Backbone, FwInfo) {
	var _ready= false;
	var _$el_and_page= [];
	var _admodel= FwInfo.admodel('preroll');
	var _prerollclass= Backbone.Model.extend({
		//Initialize model
		initialize: function() {
			THIS= this;
			// load 
			require([_admodel], function() {
				_ready= true;
				_(_$el_and_page).each(function(stuff){
					THIS.setVideoAdurl(stuff.el, stuff.pg);
				})
				_$el_and_page= [];
			});
		},
		  
		/*
		 * sets ad url in video players on page so they can call for preroll ads
		 */ 
		setVideoAdurl: function($el, page){
			// console.log('preroll setVideoAdurl', $el, page);
			if (!_ready) {
				_$el_and_page.push({
					el: $el,
					pg: page
				});
				return false;
			}
			var admodel= FwInfo.admodel('preroll'); 
			require([admodel], function(admodel) {
				// console.log('preroll setVideoAdurl (inner)', admodel);
				admodel.setVideoAdurl($el, page);
			});
			return true;
		}
	});
	var THIS= new _prerollclass();
	return THIS;
});
