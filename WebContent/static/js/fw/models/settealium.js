define(['pubsub','fwinfo','underscore','backbone'], function(PubSub,FwInfo,_) {
      var _curPage= {}
        , _queue= []
        , _utag= function(method, data) {
            ; _queue.push([method, data])
        }
    ; var _setTealiumClass= Backbone.Model.extend({
        initialize: function() {
              PubSub.on('page:load', function(e) {_setTealium.page_load(e)})
            ; PubSub.on('uotrack', function(e) {_setTealium.uotrack(e)})
            ; PubSub.on('heattrack', function(e) {_setTealium.heattrack(e)})
            ; PubSub.on('slide:change', function(e) {_setTealium.slide_change(e)})
            ; if (self.utag) this.ready()
        }
        , ready: function() {
              _.defer(function() {
                ; for (var j= 0; j < _queue.length; j++)
                    utag.track.apply(utag, _queue[j])
                ; _queue= [];
                ; _utag= function(method, data) {
                    ; utag.track(method, data)
                }
            })
        }
        , relevant_data: function(pageinfo, eventtype) {
            ; var cstlist= ['undefined']
                , halfHour= new Date(1800000*parseInt(new Date().getTime()/1800000))
                , h= halfHour.getHours()
                , sstslist= ['undefined']
                , hashvalue=location.hash.match(/\bht=([^&]*)/) && RegExp.$1
            ; try {
                  cstlist= (pageinfo.aws || FwInfo.getadcst()).replace(/\/\/*$/,'').split('/')
                ; sstslist= pageinfo.ssts.split('/')
            } catch (er) {
                console.error('Invalid page', er.stack || er.stacktrace || er.message);
            }
            ; return {
                  atyponuserid: FwInfo.getsessionvalue("atyponid")
                , category: cstlist[0]
                , clickName: (hashvalue)?hashvalue:FwInfo.getandclearsessionvalue('clickName')
                , clickPage: FwInfo.getandclearsessionvalue('clickPage')
                , cst: cstlist.slice(0,3).join(':')
                , contenttype: pageinfo.contenttype
                , assetid: pageinfo.contentId
                , dayofweek: 'sunday monday tuesday wednesday thursday friday saturday'.split(' ')[new Date().getDay()]
                , eventtype: eventtype
                , gcionid: document.cookie.match(/\bGCIONID=([^;]*)/) && RegExp.$1
                , halfhour: (1+(h+11)%12)+':'+('0'+halfHour.getMinutes()).substr(-2)+(h>11 ?' pm' :' am')
                , linkTrackVars: 'prop1'
                , searchkeywords: pageinfo.searchkeywords
                , section: sstslist[0]
                , seotitle: pageinfo.seotitle
                , ssts: pageinfo.ssts
                , subcategory: cstlist.slice(0,2).join(':')
                , subsection: sstslist.slice(0,2).join('/')
                , templatetype: pageinfo.templatetype
                , topic: sstslist.slice(0,3).join('/')
                , typeofday: ((new Date().getDay()+8)%7>1) ?'weekday' :'weekend'
                , uniqueuserid: FwInfo.getsessionvalue("uniqueid")
                , referrerhost:(hashvalue)?document.referrer.split('/')[2]:""
            };
        }
        , page_load: function(detail) {
              var data= _curPage= _setTealium.relevant_data(detail, 'page:load')
            ; _utag('view', data);
        }
        , uotrack: function(detail) {
            ; if ('string' != typeof detail) {
                console.error('uotrack has been given a non-string event argument', detail);
                detail= (detail||{}).prop41 || 'somethingbad'
            }
            ; var data= {clickName: detail, eventtype: 'uotrack', linkTrackEvents: 'None', linkTrackVars: 'prop41,prop1'}
            ; _utag('link', data)
        }
        , heattrack: function(clickName) {
              FwInfo.setsessionvalue('clickName', clickName)
            ; FwInfo.setsessionvalue('clickPage', location.href)
        }
        , slide_change: function(detail) {
              var data= _curPage
            ; data.ssts= detail.ssts
            ; data.contenttype= detail.contenttype
            ; data.eventtype= 'slide:change'
            ; data.templatetype= ''
            ; _utag('view', data)
        }

    })
    ; var _setTealium= new _setTealiumClass()
    ; return _setTealium;
});
