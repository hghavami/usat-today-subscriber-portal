/**
 * @Preroll App view.
 * @author Shyam sedamkar
 */
define(['jquery', 'underscore', 'backbone', 'pubsub', 'fwinfo', 'fw/models/preroll'],
	function($, _, Backbone, PubSub, FwInfo, PrerollModel) {
		// console.log('prerollview require fn');
		/**
		 * View class.
		 * Global event listener. Not intended to render an actual view.
		 */
		var PrerollView = Backbone.View.extend({
			// Initialize view.
			initialize: function(options) {
				// console.log('PrerollView initialize', options);
				PubSub.on('page:load', this.render);
			},

			// Render wrapper divs for preroll.
			render: function(page) {
				// console.log('PrerollView render', page);
				var $el= $('object'); // apparently, backbone helpfully strips off the jquery wrapper...
				THIS.model.setVideoAdurl($el, page);
			}

		});
		var THIS= new PrerollView({model: PrerollModel});

		/**
		* Return view class.
		*/
		return THIS;
	}
);
