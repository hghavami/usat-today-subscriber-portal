/**
 * Manages all display advertising
 * @Banner App view.
 * @author Shyam Sedamkar
 */
define(
	[
		'jquery',
		'underscore',
		'backbone',
		'pubsub',
		'fwinfo',
		'views/modules/partner/partner.pushdown',
		'views/modules/partner/partner.slider',
		'views/modules/partner/partner.hero-flip',
		'views/modules/partner/partner.elastic',
		'views/modules/partner/partner.inline-elastic',
		'views/modules/partner/partner.asset-sponsorship'
	],
function(
	$, 
	_, 
	Backbone, 
	PubSub, 
	FwInfo, 
	PushdownAd,
	SliderAd,
	HeroFlipAd,
	ElasticAd,
	InlineElasticAd,
	AssetSponsorship
) {

		var _admodelReference = FwInfo.admodel('banner');

		var _admodelInstance = null;

		var _sizeMap = {
			// Front Cards
			'high_impact': [
				[1200,615],
				[1080,810],
				[720,524],
				[300,250],
				[300,600]
			],
			'home_high_impact': [
				[1200,615],
				[1080,810],
				[720,524],
				[300,250],
				[300,600]
			],
			// Stag Fronts
			'stag_high_impact': [
				[1080,810],
				[300,250],
				[300,600]
			],
			// Assets
			'poster': [
				[300,250],
				[300,600]
			],
			// Transition/TakeOver/Gallery Inline
			'elastic': [
				[1080,810]
			],
		};

		var _earlyPage = null;

		var _page = null;

		var _sliderCheckCounter = [];
		var _sliderCheckTimer = null;

		var BannerView = Backbone.View.extend({

			/**
			 * Called by the creative to put on the page
			 */
			doSetup: function(obj) {
				obj.iframe = $('iframe[name="' + obj.windowName + '"]');
				obj.page = _page;
				var vceTrack = true;
				var size = '1x1';

				// grab page and setup
				if (obj.adType == 'pushdown') {
					obj.el = $('article.cards .card-wrap .card[data-section-id="' + _page.sectionPath + '"]').siblings('.partner-placeholder[data-position="pushdown"]');
					_page.subviews['partnerPushdown'] = new PushdownAd(obj);
					size = '1200x615';
				} else if (obj.adType == 'heroflip') {
					obj.el = $('article.cards .card-wrap .card[data-section-id="' + _page.sectionPath + '"] .partner-placeholder[data-position="hero-flip"]');
					_page.subviews['partnerHeroFlip'] = new HeroFlipAd(obj);
					size = '720x524';
				} else if (obj.adType == 'elastic' && obj.iframe.parents('.gallery,.galleries').length > 0) {
					obj.bannerview = this;
					obj.el = obj.iframe.parents('.ad-slide');
					obj.el.attr('data-code-id', obj.codeIdentifier).attr('data-is-active',true);
					_page.subviews['partnerInlineElastic'] = new InlineElasticAd(obj);
					// do not vCE track - It will be done on view
					vceTrack = false;
				} else if (obj.adType == 'elastic' && _page.contentParentId == 'card') {
					// Put div on page
					var variables = {
			            'id' : 'partner_html_' + obj.codeIdentifier,
			            'position' : 'elastic'
			        }
			        var template = _.template($("#partner_overlay_template").html(), variables);
			        $(template).appendTo(App.get('body'));

			        obj.el = '#partner-overlay';
					_page.subviews['partnerElastic'] = new ElasticAd(obj);
					size = '1080x810';
				} else if (obj.adType == 'elastic') {
					obj.el = '#partner-overlay';
					_page.subviews['partnerElastic'] = new ElasticAd(obj);
					size = '1080x810';
				} else if (obj.adType == 'asset-sponsorship') {
					obj.el = '.content-wrap.active article.asset';
					_page.subviews['partnerSponsorship'] = new AssetSponsorship(obj);
					// do not vCE track - It is built into the template
					vceTrack = false;
				}

				if (vceTrack && obj.vceTrack) {
					this.trackVCE(size, obj, $(obj.el));
				}
			},

			destroy: function() {
				this.undelegateEvents();
				clearTimeout(_sliderCheckTimer);
			},

			getSizesForPosition: function(position) {
				return _sizeMap[position];
			},

			/**
			 * Initalizes the view
			 */
			initialize: function(options) {
				var me = this;

				// Per Page Ad turn off
				if (decodeURI((RegExp('no_ads' + '=' + '(.+?)(&|$)').exec(location.search)||[,null])[1]) == 'true') {
					return;
				}

				// Listen for page load event and start the chain of ad events
				PubSub.on('page:load', function(page) {
					_page = page;
					this.renderPage(page);
				}, this);

				// Event triggering from creative
				// self is $(window)
				self.partner_station = {
					setup: this.doSetup,
					trackVCE: this.trackVCE
				}; 
				// Load the ad processing script
				require([_admodelReference,"http://www.googletagservices.com/tag/js/gpt.js"], function(admodel) {
					_admodelInstance = admodel;
					me.renderPage = me.renderPageImplementation;
					if (_earlyPage) {
						me.renderPage(_earlyPage);
						_earlyPage = 0;
					}
				});

			},

			listenForAd: function($el, slot) {
				var adLoaded = false;

				if (!_.isUndefined($el.attr('style')) && $el.attr('style').indexOf('display: none;') != -1) {
					clearTimeout(_sliderCheckTimer);
					return;
				}

				// check for iframe
				if ($('iframe', $el).attr('width') == 300) {
					adLoaded = true;
					var options = {
						'adModel' : _admodelInstance,
						'adSlot' : slot,
						'el' : $el.parents('.partner.slider')
					};
					_page.subviews['slider'] = new SliderAd(options);
				}

				// count out the number of tires
				if (_.isUndefined(_sliderCheckCounter[$el.attr('id')])) {
					_sliderCheckCounter[$el.attr('id')] = 0;
				} else if (_sliderCheckCounter[$el.attr('id')] > 10 || adLoaded) {
					// give up and stop checking
					clearTimeout(_sliderCheckTimer);
					return;
				}

				// check again
				_sliderCheckTimer = setTimeout(_.bind(function() {
					_sliderCheckCounter[$el.attr('id')]++;
					this.listenForAd($el, slot);
				}, this), 100);

			},

			/**
			 * Preloads an out-of-page ad into a supplied div with a supplied cst.
			 *
			 * @param jQuery reference to an element where Google will place the ad
			 * @param cst value for ad targeting
			 */
			preloadAd: function($div, cst) {
				if(_admodelInstance == null) {
					// not created yet. set timeout
					setTimeout(_.bind(function() {
						this.preloadAd($div, cst);
					}, this), 200);
					return;
				}
				_admodelInstance.preloadGalleryTransition(this, $div, cst);
			},

			// this will be replaced with a method to render ads on a page
			// for now, though, just save the page for when we are ready to deliver them
			renderPage: function(page) {
				_earlyPage = page;
			},

			// render ads to a page
			renderPageImplementation: function(page) {
				_admodelInstance.go(this, page);
			},

			showPreloadedAd: function(codeIdentifier) {
				PubSub.trigger('partner:' + codeIdentifier + ':show');
			},

			trackVCE: function(size, partnerTemplate, $container) {
				//<script src="http://b.voicefive.com/c2/6035223/rs.js#c1=3&amp;c3=%ebuy!&amp;c4=%ecid!&amp;c5=%epid!&amp;c6=&amp;c10=1&amp;c11=%esid!&amp;c13=%psz=!;&amp;c16=dfa&amp;ax_i=&amp;ax_g=&amp;"></script>
				var vceSrc = 'http://b.voicefive.com/c2/6035223/rs.js#c1=3&amp;c3=' + partnerTemplate.dfp.ebuy + '&amp;c4=' + partnerTemplate.dfp.ecid + '&amp;c5=' + partnerTemplate.dfp.epid + '&amp;c6=&amp;c10=1&amp;c11=' + partnerTemplate.dfp.esid + '&amp;c13=' + size + ';&amp;c16=dfp';
				var script = document.createElement('script');
                script.src = vceSrc;
                script.type = 'text/javascript';
                script.defer = false;
             
                if (typeof callback != "undefined" && callback != null) {
             
                    // IE only, connect to event, which fires when JavaScript is loaded
                    script.onreadystatechange = function() {
                        if (this.readyState == 'complete' || this.readyState == 'loaded') {
                            this.onreadystatechange = this.onload = null; // prevent duplicate calls
                        }
                    }
             
                    // FireFox and others, connect to event, which fires when JavaScript is loaded
                    script.onload = function() {
                        this.onreadystatechange = this.onload = null; // prevent duplicate calls
                    };
                }

                $container.append(script);
			}

		});

		var adView = new BannerView();

		/**
		* Return view class.
		*/
		return adView;
	}
);
