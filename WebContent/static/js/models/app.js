/**
 * @fileoverview Global App "model" object to be used for any globally required
 * references -- cached DOM lookups, global properties, etc.
 * @author erik.kallevig@f-i.com (Erik Kallevig)
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'pubsub'
],
function(
    $,
    _,
    Backbone,
    PubSub
)
    {

        var AppModel = Backbone.Model.extend({

            initialize: function() {
                this.set('DEBUG', true);

                // Set global properties/lookups.
                this.set('document', $(document));
                this.set('win', $(window));
                this.set('body', $('body'));
                this.set('scrollEl', $('html,body'));
                this.set('doc', $(document));
                this.set('navEl', $('#nav'));
                this.set('headerEl', $('#header'));
                this.set('breakingEl', $('#breaking'));
                this.set('breakingWrapEl', $('#breaking-wrap'));
                this.pubSub = {
                    'page:load': this.onPageLoad
                };
                PubSub.attach(this.pubSub, this);
            },

            /*
             * Get pageinfo text data and return as JSON.
             * @param {jQuery selector} $sel Selector to find pageinfo with.
             */
            getPageInfo: function($sel) {
                if (!$sel) {$sel = $('body');}
                var pageInfoTxt = $sel.find('.pageinfo.data').text();
                return $.parseJSON(pageInfoTxt);
            },

            onPageLoad: function(pageInfoObj){
                if (pageInfoObj && pageInfoObj.asset_collection) {
                    this.set('collection', pageInfoObj.asset_collection);
                    PubSub.trigger('updateArrowLinks:overlay', true);
                }else{ // clear out the model since we have no asset collection
                    this.set('collection', null);
                }
            },

            /**
             * Set asset collection for story next/previous.
             * @param {jQuery selector} $sel Selector to find pageinfo with.
             */
            setCollection: function($sel) {
                var pageInfoObj = this.getPageInfo($sel);
                if (pageInfoObj && pageInfoObj.asset_collection) {
                    this.set('collection', pageInfoObj.asset_collection);
                    PubSub.trigger('updateArrowLinks:overlay', true);
                }
            },

            /*
             * Get element and call heattracking with it.
             * @param {jQuery selector} $sel Selector to heattrack.
             */
            setHeatTrack: function($sel) {
                var tracking;
                if($sel.attr('data-heattrack')){
                    tracking = $sel.attr('data-heattrack');
                    PubSub.trigger('heattrack', tracking);
                } else if ($sel.attr("data-uotrack")){
                    tracking = $sel.attr("data-uotrack");
                    PubSub.trigger('uotrack', tracking);
                } else{
                    return;
                }
            },

            getScrollPosition: function(){
                return window.pageYOffset || document.body.scrollTop;
            },

            /**
             * Load externally hosted script (SDK, library, etc) on-demand.
             * @param {string} scriptUrl URL of script file.
             * @param {string} symbol Unique window property from external
             *     script to validate the script has been successfully loaded.
             * @param {Function} callback Function to call when script has loaded.
             */
            loadScript: function(scriptUrl, symbol, callback) {
                if (window[symbol]) {
                    callback();
                } else {
                    require([scriptUrl], function(){
                        if (!window[symbol]) {
                            console.log('Error loading ' + scriptUrl);
                        } else {
                            callback();
                        }
                    });
                }
            },

            /**
             * Open a centered popup window.
             * @param {string} url URL to load in the popup.
             * @param {number=} width Width of popup.
             * @param {number=} height Height of popup.
             */
            openPopup: function(url, width, height) {
                width = width || 600;
                height = height || 400;
                var winCoords = this.popupCoords(width, height);
                window.open(
                    url,
                    '',
                    'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,' +
                    'height='+ height +',width='+ width +',top='+ winCoords.top +
                    ',left='+ winCoords.left
                );
            },

            /**
             * Calculate and return coordinates for centering a popup window to
             *     the outerWidth/outerHeight of the browser window.
             * @param {number} w Width of popup window.
             * @param {number} h Height of popup window.
             */
            popupCoords: function(w, h) {
                var wLeft = window.screenLeft ? window.screenLeft : window.screenX;
                var wTop = window.screenTop ? window.screenTop : window.screenY;
                var wWidth = window.outerWidth ? window.outerWidth : document.documentElement.clientWidth;
                var wHeight = window.outerHeight ? window.outerHeight : document.documentElement.clientHeight;

                // Subtract 25 pixels to top to approximately compensate for chrome
                // on top of popup window, since we can't calculate that before
                // opening it.
                return {
                    left: wLeft + (wWidth / 2) - (w / 2),
                    top: wTop + (wHeight / 2) - (h / 2) - 25
                };
            }

        });

        /**
         * Extend Backbone.Model to a global object.
         */
        return new AppModel;
    }
);
