/**
 * @fileoverview Live feed model.
 * @author stephen.burgess@f-i.com (Stephen Burgess)
 */
define([
    'jquery', 
    'underscore', 
    'backbone'
],
function(
    $, 
    _, 
    Backbone
)
    {

        /**
         * Model class.
         */
        var LiveFeedModel = Backbone.Model.extend({

            /**
             * Merges current markup with markup from queue and resets queue.
             * @param {String} the section to merge data for.
             */
            merge: function(section) {
                var m = $.makeArray(this.get('markup-' + section));
                var q = this.get('queue-' + section);
                this.set('markup-' + section, $(q.concat(m)));
                this.set('queue-' + section, []);
            }
        });


        /**
         * Return model class.
         */
        return LiveFeedModel;
    }
);
