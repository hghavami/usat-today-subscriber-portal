/**
 * @fileoverview jquery extended custom selectors.
 * @author rhuhn@usatoday.com (Robert Huhn)
 */

//finds external non usat links
$.extend(
    $.expr[':'].external = function(obj){
        if(obj.hostname == location.hostname || obj.hostname == 'usatoday'){
            return false;
        }
        return true;
});