/*global jQuery */
/*!
 * FitVids 1.0
 *
 * Copyright 2011, Chris Coyier - http://css-tricks.com + Dave Rupert - http://daverupert.com
 * Credit to Thierry Koblentz - http://www.alistapart.com/articles/creating-intrinsic-ratios-for-video/
 * Released under the WTFPL license - http://sam.zoy.org/wtfpl/
 *
 * Date: Thu Sept 01 18:00:00 2011 -0500
 */

(function($) {

    $.fn.fitVids = function(options) {
        var settings = {
            customSelector: null,
            maxWidth: false,
            maxWidthDefault: 574
        },
            div = document.createElement('div'),
            ref = document.getElementsByTagName('base')[0] || document.getElementsByTagName('script')[0];

        div.className = 'fit-vids-style';
        div.style.display = 'none';

        ref.parentNode.insertBefore(div, ref);

        if (options) {
            $.extend(settings, options);
        }

        return this.each(function() {
            var selectors = [
            "iframe[src*='player.vimeo.com']",
            "iframe[src*='www.youtube.com']",
            "iframe[src*='www.kickstarter.com']",
            "iframe[src^='http://www.kickstarter.com']",
            "iframe[src^='http://www.funnyordie.com']",
            "iframe[src^='http://media.mtvnservices.com']",
            "iframe[src^='http://trailers.apple.com']",
            "iframe[src^='http://www.brightcove.com']",
            "iframe[src^='http://blip.tv']",
            "iframe[src^='http://break.com']",
            "iframe[src^='http://www.traileraddict.com']",
            "iframe[src^='http://d.yimg.com']",
            "iframe[src^='http://movies.yahoo.com']",
            "iframe[src^='http://www.dailymotion.com']",
            "iframe[src^='http://s.mcstatic.com']",
            "iframe[src*='mlb.mlb.com']",
            "iframe[src*='www.nfl.com']",
            "iframe object",
            // "object",
            // "object[data]",
            // "object[classid]:not(:has(object[data]))",
            "object embed[src*='www.youtube.com']",
            // "embed"
            ];

            if (settings.customSelector) {
                selectors.push(settings.customSelector);
            }

            var $allVideos = $(this).find(selectors.join(','));

            $allVideos.each(function() {
                var $this = $(this);
                if (this.tagName.toLowerCase() == 'embed' && $this.parent('object').length || $this.parent('.fluid-width-video-wrapper').length) {
                    return;
                }
                var height = this.tagName.toLowerCase() == 'object' ? $this.attr('height') : $this.height(),
                    width = this.tagName.toLowerCase() == 'object' ? $this.attr('width') : $this.width(),
                    aspectRatio, originalWidth = $this.attr('width');
                if (!height || !width) {
                    height = parseInt($this.find('embed').css('height'), 10);
                    width = parseInt($this.find('embed').css('width'), 10);
                }
                aspectRatio = height / width;
                if (!$this.attr('id')) {
                    var videoID = 'fitvid' + Math.floor(Math.random() * 999999);
                    $this.attr('id', videoID);
                }
                $this.wrap('<div class="fluid-width-video-wrapper" style="padding-top:' + (aspectRatio * 100) + '%;"></div>').removeAttr('height').removeAttr('width');
                $this.removeAttr('height').removeAttr('width');
                $this.parent('.fluid-width-video-wrapper').parent('object').removeAttr('height').removeAttr('width');
                $this.parent('.fluid-width-video-wrapper').parent().removeAttr('style').parent().removeAttr('style'); //gametrailers
                $this.find('embed').removeAttr('height').removeAttr('width').removeAttr('style').removeAttr('style').parent().removeAttr('style'); //youtube object embed
            });
        });

    };
})(jQuery);