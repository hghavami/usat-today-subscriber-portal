/**
 * @fileoverview A jQuery plugin that dynamically
 * scales images for use in a responsive design
 * @author jhensley@gannett.com (Jonathan Hensley)
 */

(function( $ ) {
	$.responsiveImages = function(options){

		// Defaults
		// Create some defaults, extending them with any options that were provided
    	var settings = $.extend( {
      		'small' 			: 	'data-href-small',
      		'medium'			: 	'data-href-medium',
      		'large'		 		: 	'data-href-large',
      		'selector'		 	: 	'.mq-swap',
      		'mqEl'				:   'div[role="main"]'
    	}, options);

    	var $swap = $(settings.selector),
    		$mqEl = $(settings.mqEl),
    		mqWidth = parseFloat($mqEl.width());

		$swap.each(function(){
			if(mqWidth>=1080){
				$(this).attr('src',$(this).attr('data-image-medium'));
			} else {
				$(this).attr('src',$(this).attr('data-image-small'));
			}
		});

	}
})( jQuery );