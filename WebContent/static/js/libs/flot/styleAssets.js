var chartAssets = {
	
	createPattern : function(color){
 		

		var pattern = document.createElement('canvas');
		pattern.width = 6;
		pattern.height = 6;
		pattern.setAttribute("id","chart-pattern");


		var pctx = pattern.getContext('2d');

		pctx.moveTo(0, 0);
		pctx.lineTo(200, 200);
		pctx.strokeStyle = (color)? color : "rgba(256,256,256,0.5)";
		pctx.stroke();
		return pattern;
	},
	color :  {

		news : [ 
			'rgb(14,64,86)',
			'rgb(23,98,132)',
			'rgb(27,116,180)',
			'rgb(40,158,252)',
			'rgb(119,202,252)',
 			'rgb(173,222,253)',
			'rgb(204,238,255)' 
		],
		money : [
 			'rgb(11,53,22)',
 			'rgb(17,91,38)',
 			'rgb(23,124,55)',
 			'rgb(24,169,67)',
 			'rgb(118,208,150)',
 			'rgb(173,226,192)',
			'rgb(209,237,218)'
 		],
 		sports : [
 			'rgb(73,14,13)', 
 			'rgb(124,24,19)',
 			'rgb(167,33,22)',
 			'rgb(232,35,25)',
 			'rgb(242,132,118)',
 			'rgb(247,181,172)',
			'rgb(249,229,228)'
 		],
 		life : [
 			'rgb(60,12,71)',
 			'rgb(86,18,102)',
 			'rgb(118,23,137)',
 			'rgb(159,26,188)',
 			'rgb(202,118,218)',
 			'rgb(223,173,232)',
 			'rgb(236,211,242)'
 		],
 		tech : [
			'rgb(71,29,12)',
			'rgb(127,53,20)',
			'rgb(181,79,27)',
			'rgb(252,102,33)',
			'rgb(253,171,120)',
			'rgb(254,205,173)',
 			'rgb(255,232,220)'
 		],
 		weather : [
			'rgb(68,52,13)',	 
			'rgb(117,90,23)',
			'rgb(182,146,35)',
			'rgb(254,203,46)',
			'rgb(254,226,123)',
			'rgb(254,237,175)',
 			'rgb(252,242,214)'
 		]
 
	}

 

}