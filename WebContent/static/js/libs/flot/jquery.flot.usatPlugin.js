 /*
Flot plugin for rendering pie charts. The plugin assumes the data is 
coming is as a single data value for each series, and each of those 
values is a positive value or zero (negative numbers don't make 
any sense and will cause strange effects). The data values do 
NOT need to be passed in as percentage values because it 
internally calculates the total and percentages.

* Created by Brian Medendorp, June 2009
* Updated November 2009 with contributions from: btburnett3, Anthony Aragues and Xavi Ivars
 

More detail and specific examples can be found in the included HTML file.

*/

(function ($) {
	function init(plot){ // this is the "body" of the plugin
	 	console.log( plot );
		
	} // end init (plugin body)
	
	// define pie specific options and their default values
	var options = {
		 
	};
    
	$.plot.plugins.push({
		init: init,
		options: options,
		name: "pie",
		version: "1.0"
	});
})(jQuery);