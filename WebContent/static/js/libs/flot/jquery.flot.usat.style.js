/*
Flot plugin for customizing USA Today Bar Charts 

* Built to accept canvas patterns

* Created by Brian Stan Wilson Jr, August 2012
 

*/

(function ($) 
{

 	function init(plot) // this is the "body" of the plugin
	{
 
 
 
		// add hook to determine if pie plugin in enabled, and then perform necessary operations
		plot.hooks.draw.push(addStyle);
		//plot.hooks.bindEvents.push(bindEvents);	

		// check to see if the pie plugin is enabled
		function addStyle(plot){

 		 		

  				 var ctx = plot.getCanvas().getContext("2d"),
 				 	canvasWidth =  plot.width()+ 100,
 					canvasHeight  =  plot.height()+50;

 				ctx.save();


  		 		var patternSrc = plot.getOptions().pattern;
 				var pattern = ctx.createPattern(patternSrc, "repeat");


 				ctx.globalCompositeOperation = 'source-atop';
 				//ctx.translate( 0, 0); 
				ctx.moveTo(20, 20);
				ctx.beginPath();
 				ctx.rect(0,0, canvasWidth, canvasHeight  );
 				ctx.fillStyle = pattern;
				ctx.fill();
 				ctx.restore();
		}
	 
	 
	} // end init (plugin body)
	
	// define pie specific options and their default values
	var options = {  };
    
	$.plot.plugins.push({
		init: init,
		options: options,
		name: "style ",
		version: "0.05"
	});
})(jQuery);
