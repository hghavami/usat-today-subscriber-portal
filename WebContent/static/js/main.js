/**
 * @fileoverview Bootstrap file for USAToday.com.
 * @author erik.kallevig@f-i.com (Erik Kallevig)
 * @author kris.hedstrom@f-i.com (Kris Hedstrom)
 */


/**
 * Base require.js config.
 * 
 * IMPORTANT
 * This configuration is for development only.
 * Any updates here should likely also be made for production use in build.config.js.
 */
require.config({
    paths: {
        jquery: 'libs/jquery/jquery-1.7.2',
        jqueryui: 'libs/jquery/plugins/jquery-ui-1.8.19.custom.min', // to be served via dfp
        underscore: 'libs/underscore/underscore-min',
        backbone: 'libs/backbone/backbone-dev',
        pubsub: 'libs/backbone/plugins/pubsub',
        app: 'models/app',
        baseview: 'views/base',
        cookie: 'libs/jquery/plugins/jquery.cookie',
        easing: 'libs/jquery/plugins/jquery.easing.1.3',
        animatecolors: 'libs/jquery/plugins/jquery.animate-colors-min',
        responsiveimages: 'libs/jquery/plugins/jquery.responsiveImages',
        resizestop: 'libs/jquery/plugins/jquery.resizeStop',
        chosen: 'libs/jquery/plugins/chosen.jquery.min',
        mousewheel: 'libs/jquery/plugins/jquery.mousewheel.min',
        circleplayer: 'libs/jquery/plugins/audio-player/circle.player',
        grab: 'libs/jquery/plugins/audio-player/jquery.grab',
        jplayer: 'libs/jquery/plugins/audio-player/jquery.jplayer',
        transform: 'libs/jquery/plugins/audio-player/jquery.transform',
        twitter: 'libs/twitter/twitter',
        fwinfo: 'fw/models/fwinfo',
	    settealium: 'fw/models/settealium',
        facebook: 'fw/facebook/facebook',
        prerollview: 'fw/views/prerollview',
        fitvids: 'libs/jquery/plugins/jquery.fitvids',
        selectorextend: 'libs/jquery/plugins/jquery.selector.extend',
        leaflet: 'libs/leaflet/leaflet',
        tilelayer: 'libs/leaflet/tile-layer',
        waxleaf: 'libs/leaflet/wax.leaf.min'
    },
    
    /**
     * The use of the order plugin has been discontinued. Define necessary dependencies
     * using shim.
     *
     * https://github.com/jrburke/requirejs/wiki/Upgrading-to-RequireJS-2.0
     */
    shim: {
        'jqueryui': ['jquery'],
        'cookie': ['jquery'],
        'easing': ['jquery'],
        'animatecolors': ['jquery'],
        'responsiveimages': ['jquery'],
        'resizestop': ['jquery'],
        'chosen': ['jquery'],
        'mousewheel': ['jquery'],
        'circleplayer': ['jquery'],
        'grab': ['jquery'],
        'jplayer': ['jquery'],
        'transform': ['jquery'],
        'fitvids': ['jquery'],
        'selectorextend': ['jquery'],
        'tilelayer': ['leaflet'],
        'waxleaf': ['leaflet']
    }
});


/**
 * Base 'App' initializaiton.
 */
require([
    'jquery',
    'underscore',
    'backbone',
    'pubsub',
    'app',
    'state',
    'views/app',
    'views/pages/home',
    'views/pages/generic-section',
    'views/pages/weather',
    'views/pages/story',
    'views/pages/404',
    'views/pages/search',
    'views/pages/static-page',
    'views/pages/media',
    'views/pages/topic',
    'views/pages/blog',
    'views/pages/sports',
    'views/pages/sports-overlay',
    'views/pages/userauth',
    'views/modules/overlay',
    'views/modules/cards2',
    'views/modules/coverview',
    'views/modules/alert',
    'facebook',
    'settealium',
    'fw/views/bannerview',
    'jqueryui',
    'fwinfo',
    'prerollview'
],
function(
    $,
    _,
    Backbone,
    PubSub,
    App,
    StateManager,
    AppView,
    HomeView,
    GenericSectionView,
    WeatherView,
    StoryView,
    ErrorView,
    SearchView,
    StaticPageView,
    MediaView,
    TopicView,
    BlogView,
    SportsView,
    SportsOverlayView,
    UserAuthView,
    Overlay,
    CardView,
    CoverView,
    Alert
) {

    /**
     * App router.
     */
    var AppRouter = Backbone.Router.extend({

        /**
         * Constants.
         */
        DEBUG: false, // Display console.log messages.
        XHR_REMIND_TIME: 30000, // Time in ms before prompting slow xhr.
        LOADING_MESSAGE: 'Loading... (temporary loader)',
        SLOW_CONNECTION_MESSAGE: 'Oops. Connection is taking ' +
            'a long time... (temporary loader)',
        ERROR_MESSAGE: 'Connection Error... (temporary message)',
        UPDATE_FREQUENCY: 900000, // Time in ms before checking for new data.

        /**
         * Initialize.
         */
        initialize: function() {
            // Flags
            this.init = true;
            window.App = App;

            // Set this boolean to true after initial full server page load.
            // The 'all' event is fired AFTER every router match and
            // subsequent callback, so the boolean will only be true after
            // the app's first page load.
            this.shouldFetchData = false;
            this.on('all', this.setShouldFetchData);

            // Define routes (and pass full route to callback);
            this.route(/^()$/, 'loadCardView');
            this.route(/^([a-zA-Z0-9\-\/]*)$/, 'loadCardView');
            this.route(/^(index.html)$/, 'loadCardView');
            this.route(/^(cover-view\/)$/, 'loadCoverView');
            this.route(/^(cover-view\/.*\/)$/, 'loadCoverView');
            this.route(/^(photos\/.*\/)$/, 'loadOverlay');
            this.route(/^(video\/.*\/)$/, 'loadOverlay');
            this.route(/^(story\/.*\/)$/, 'loadOverlay');
            this.route(/^(p\/.*)$/, 'loadStaticPage');
            this.route(/^(search\/.*)$/, 'loadSearch');
            this.route(/^(sports\/[a-zA-Z0-9]+\/scores\/preview\/.*)$/, 'loadSportsOverlay');
            this.route(/^(sports\/[a-zA-Z0-9]+\/scores\/recap\/.*)$/, 'loadSportsOverlay');
            this.route(/^(topic\/.*)$/, 'loadTopicPage');
            this.route(/^(media\/.*)$/, 'loadMediaView');
            this.route(/^(blog\/.*)$/, 'loadBlogPage');
            this.route(/^(userauth\/default\/.*)$/, 'loadUserAuthPage');
            this.route(/^(404.html)$/, 'load404');
            this.route(/^(500.html)$/, 'load500');
            //this.route(/^(sports\/.*)$/, 'loadSportsCard');

            // Currently running ajax request.
            this.xhr = null;
            this.xhrTimer = null;
            this.DEBUG = App.get('DEBUG');

            // Listen for global timer ticks.
            PubSub.on('timer:tick:app', this.onTimeTick, this);

            PubSub.trigger('initialize:router');

        },

        /**
         * After first route is loaded (from full server pageload),
         * set 'shouldFetchData' to true so subsequent routes will fetch
         * data via XHR.
         */
        setShouldFetchData: function() {
            this.shouldFetchData = true;
            this.off('all', this.setShouldFetchData);
        },

        /**
         * Fetch data from server via AJAX. Takes a path to fetch and a
         * callback to parse the data and initialize views.
         * @param {String} path The path to the ajax endpoint.
         * @param {Function} callback Function to call when data is returned
         *      from server.
         * @param {Boolean} isStatic tells the ajax request whether to add
         *      the pjax headers or not
         * @param {Function|undefined} errorCallback Callback function that
         *      gets called if ajax fetch fails.
         */
        fetchData: function(path, callback, isStatic, errorCallback) {
            if(this.xhr) {
                // Kill any pending xhr requests.
                // @TODO: figure out if anything needs to load in parallel,
                // in that case this needs to change.
                this.xhr.abort();
                clearInterval(this.xhrTimer);
                Alert.hideLoader();
            }
            if (this.shouldFetchData) {
                PubSub.trigger('data:fetching:router');
                var extra_headers = {};

                if (isStatic) {
                    extra_headers['X-PJAX'] = "true";
                    extra_headers['X-PJAX-Container'] = "#overlay";
                }

                this.xhrTimer = setInterval($.proxy(function() {
                    if(this.xhr) {
                        Alert.showLoader(this.SLOW_CONNECTION_MESSAGE);
                    }
                }, this), this.XHR_REMIND_TIME);
                this.xhr = $.ajax({
                    url: '/' + path,
                    data: {
                        ajax: true
                    },
                    beforeSend: function(xhr){
                        $.each(extra_headers, function(k, v){
                            xhr.setRequestHeader(k, v);
                        });
                    },
                    success: callback,
                    error: $.proxy(function(e) {
                        if(typeof errorCallback === 'function') {
                            errorCallback(e);
                        } else {
                            if (this.DEBUG) {
                                console.log('fetchData Error: ',
                                e.responseText);
                            }
                        }
                    }, this),
                    complete: $.proxy(function() {
                        PubSub.trigger('data:fetched:router');
                        clearInterval(this.xhrTimer);
                        this.xhr = this.xhrTimer = null;
                    }, this)
                });
            } else {
                callback();
            }
        },
        /**
         * Load a static page -- simple pass thru function to call
         *      loadOverlay with additional parameter.
         * @param {string} path Full path of the page to load.
         */
        loadStaticPage: function(path) {
            if (this.DEBUG) {console.log('Router: Load static page: ' + path);}

            // Trigger loadOverlay function
            this.loadOverlay(path, true);
        },

        /**
         * Load 'topic' card (layer 0).
         * @param {String} path Path of topic to load.
         */
        loadTopicPage: function(path) {
            StateManager.loadView(TopicView, null, path);
        },

        /**
         * Load media view (layer 0).
         */
        loadCardView: function(path) {
            StateManager.loadView(CardView, null, path);
        },

        /**
         * Load cover view (layer 0).
         */
        loadCoverView: function(path) {
            StateManager.loadView(CoverView, null, path);
        },

        /**
         * Load media view (layer 0).
         */
        loadMediaView: function(path) {
            StateManager.loadView(MediaView, null, path);
        },

        /**
         * Load 'blog' card (layer 0).
         */
        loadBlogPage: function(path) {
            StateManager.loadView(BlogView, null, path);
        },

        /**
         * Load an overlay -- story, static page, etc (layer 1).
         */
        loadOverlay: function(path) {
            StateManager.loadView(Overlay, null, path, true);
        },

        /**
         * Load 404 (layer 1).
         */
        load404: function(path) {
            if (this.DEBUG) {console.log('Router: Load 404: ', path);}
            this.fetchData(path, function(htmlFrag) {
                var view = new ErrorView();
            });
        },

        /**
         * Load 500 (layer 1).
         */
        load500: function(path) {
            if (this.DEBUG) {console.log('Router: Load 500: ', path);}
            this.fetchData(path, function(htmlFrag) {
                var view = new ErrorView();
            });
        },

         /**
         * LoadUserAuthPage
         */
        loadUserAuthPage: function(path) {
            StateManager.loadView(UserAuthView, null, path);
        },

        /**
         * Load search front.
         */
        loadSearch: function(path) {
            StateManager.loadView(SearchView, null, path);
        },

        /**
         * Load sports overlay.
         */
        loadSportsOverlay: function(path) {
            StateManager.loadView(SportsOverlayView, null, path, true);
        }

    });


    /**
     * Initialize app router.
     */
    var router = window.Router = new AppRouter();


    /**
     * Initialize app view.
     */
    var view = new AppView({
        router: router,
        idleTimer: 900000 // 15 minutes in milliseconds.
    });


    /**
     * Start history manager.
     */
    Backbone.history.start({pushState: true, hashChange: false});

});
