/**
 * @fileoverview State Manager that handles the application state. Maintains the active view and
 * manages routing and transitioning between the views. Ajax calls and animation calls
 * should be registered with the state manager to guarentee that animation isn't interrupted
 * and that stale or requests that are being thrown out.
 * @author jmerrifiel@gannett.com (Jay Merrifield)
 */
define(['jquery', 'underscore', 'backbone', 'pubsub', 'app', 'views/modules/alert', 'views/modules/loader'],
    function($, _, Backbone, PubSub, App, Alert, Loader ) {
        var StateManager = Backbone.View.extend({
            DEBUG: true,
            TYPE_OVERLAY: 'overlay',
            TYPE_PRELOAD: 'preload',
            TYPE_NORMAL: 'normal',
            XHR_REMIND_TIME: 30000, // Time in ms before prompting slow xhr.
            SLOW_CONNECTION_MESSAGE: 'Oops. Connection is taking ' +
                'a long time... (temporary loader)',

            initialize: function(options){
                // this variable tracks an extended request in progress
                this.deferredTransaction = null;
                this.navRequest = null;
                this.activeView = null;
                this.activeViewType = null;
                this.preloadedView = null;
                this.xhrList = [];
                this.loader = new Loader({
                        el: 'body',
                        msg: 'Loading...'
                    });

                this.fullscreenView = null;

                this.loader.$el.addClass('global-loader');
                this.scrollEl = App.get('scrollEl');
                this.nav = App.get('navEl');
                this.breaking = App.get('breakingEl');
                this.headerSlideSpeed = App.get('headerSlideSpeed');
            },

            navigateToPreloadedUrl: function(defaultPath){
                var navPath = (this.preloadedView && this.preloadedView.currentPath) ||
                                defaultPath || App.get('currentCardPath');
                if (Modernizr.history){
                    Backbone.history.navigate(navPath, {trigger: true});
                } else if (navPath) {
                    window.location = '/' + navPath;
                } else {
                    window.history.back();
                }
            },

            getActiveView: function(){
                return this.activeView;
            },

            /**
             * Registers a full screen view with the state manager. This is necessary because
             * the full screen view doesn't have a unique url and lives outside the knowledge
             * of the state manager
             * @param fullscreenView {FullScreenView}
             */
            registerFullScreenView: function(fullscreenView){
                this.fullscreenView = fullscreenView;
            },

            /**
             * Clears out the full screen view from the state manager
             */
            clearFullScreenView: function(){
                this.fullscreenView = null;
            },

            /**
             * This is the main state manager call, this will compare what the current state
             * of the universe is, and make any ajax calls, and transition to the current state
             * with the correct view information
             *
             * @param ViewType {Class} the javascript class that is taking over the view
             * @param initOptions {Map} any initialization options for the view class
             * @param newPath {String} path being loaded
             * @param overlay {Boolean} overlay the view on top of the existing view
             */
            loadView: function(ViewType, initOptions, toPath, overlay){
                // full screen views live outside of the state manager because they don't
                // modify the url. If that assumption ever changes, we should switch
                // full screen view to being state managed and this hack can go away
                if (this.fullscreenView){
                    this.fullscreenView.close();
                    this.fullscreenView = null;
                }
                initOptions = initOptions || {};
                var type = (overlay ? this.TYPE_OVERLAY : this.TYPE_NORMAL);
                if (!this.activeView){
                    // initial page load, html is already loaded
                    if(this.DEBUG) {console.log('Router: Initializing: ', toPath);}
                    this.activeView = new ViewType(initOptions);
                    this.activeViewType = type;
                    this.activeView.revealView(null, toPath);
                }else{
                    return this._triggerTransitionEvents(this.activeView, ViewType, initOptions, type, toPath);
                }
            },
            preloadView: function(ViewType, initOptions, toPath){
                if (Modernizr.history){
                    if(this.DEBUG) console.log('Router: Preloading: ', toPath);
                    return this._triggerTransitionEvents(this.preloadedView, ViewType, initOptions,
                                                this.TYPE_PRELOAD, toPath);
                }
            },
            _triggerTransitionEvents: function(activeView, NewViewType, initOptions, type, toPath){
                // the goal of this function is to trigger both an animation and a ajax request
                // at the same time. We use startNavigationAnimation to make certain both events
                // finish at the same time before proceeding to the final phase
                var fromPath = activeView && activeView.currentPath;
                var preload = type === this.TYPE_PRELOAD;
                // have to do this first, cause fetchData needs to be pinned to this deferred
                var animationPromise = this._removeCurrentView(NewViewType, type, toPath);
                if (!animationPromise){
                    // we're not removing the current view for a couple of reasons,
                    // 1: it's an internal transition
                    // 2: it's an overlay on top of the current view (stashing current view)
                    var requestPromise = this._fetchNextViewData(activeView, NewViewType, toPath, preload);
                    if (type === this.TYPE_OVERLAY && this.activeViewType === this.TYPE_NORMAL){
                        animationPromise = this._revealOverlay(new NewViewType(initOptions), fromPath, toPath, requestPromise);
                    }else if (type === this.TYPE_PRELOAD && !this.preloadedView){
                        animationPromise = this._revealNewView(new NewViewType(initOptions), type, fromPath, toPath, requestPromise);
                    }else{
                        // if we're not removing the view, let the view transition internally
                        animationPromise = activeView.transition(fromPath, toPath, requestPromise, preload);
                        this.startNavigationAnimation(animationPromise);
                    }
                }else{
                    this.startNavigationAnimation(animationPromise);
                    if (type === this.TYPE_NORMAL &&
                        this.preloadedView && this.preloadedView.__proto__ === NewViewType.prototype){
                        // we're going to resume to a preloaded view, we need to change the pointers
                        // so fetch knows whether we want to fetch new data
                        activeView = this.activeView = this.preloadedView;
                        this.activeViewType = type;
                        this.preloadedView = null;
                    }
                    var requestPromise = this._fetchNextViewData(activeView, NewViewType, toPath, preload);
                    animationPromise.done(_.bind(function(){
                        this.scrollEl.scrollTop(0);
                        if (this.activeView.__proto__ === NewViewType.prototype){
                            // we hit this if statement when transitioning from an overlay to a preloaded
                            // view that's can handle the transition
                            var transition = this.activeView.transition(fromPath, toPath, requestPromise)
                            this.startNavigationAnimation(transition);
                            return transition.done(_.bind(function(){
                                this._transitionFromOverlayScroll(this.activeView);
                            }, this));
                        }else{
                            this._revealNewView(NewViewType, initOptions, type, fromPath, toPath, requestPromise);
                        }
                    }, this));
                }
                return animationPromise;
            },
            _revealOverlay: function(nextView, fromPath, toPath, requestPromise){
                this.activeView.pause();
                this._transitionToOverlayScroll(this.activeView);
                this.preloadedView = this.activeView;
                return this._revealNewView(nextView, this.TYPE_OVERLAY, fromPath, toPath, requestPromise);
            },
            _revealNewView: function(nextView, type, fromPath, toPath, requestPromise){
                // So when we find ourselves here under the following circumstances
                // 1: transitioning from one normal/overlay view to different one
                // 2: transitioning from one preloaded view to another

                // all new views come in scrolled top
                var revealPromise = nextView.revealView(fromPath, toPath, requestPromise);
                this.startNavigationAnimation(revealPromise);

                // save the variables
                if (type === this.TYPE_PRELOAD){
                    this.preloadedView = nextView;
                    requestPromise.done(function(){
                        nextView.$el.addClass('fixed').css('top', this._getBreakingNewsOffset());
                    });
                }else{
                    this.activeView = nextView;
                    this.activeViewType = type;
                }
                return revealPromise;
            },
            _removeCurrentView: function(newViewType, type, newPath){
                var removalPromise = null;
                // switching from a normal view to a normal view, or an overlay to another overlay
                if ((type === this.TYPE_OVERLAY && this.activeViewType === this.TYPE_OVERLAY) ||
                    (type === this.TYPE_NORMAL && this.activeView)){
                    if (this.activeView.__proto__ !== newViewType.prototype){
                        removalPromise = this.activeView.removeView(newPath);
                        var preloadedView = this.preloadedView;

                        if (this.activeViewType === this.TYPE_OVERLAY){
                            removalPromise.done(_.bind(function(){
                                this._transitionFromOverlayScroll(preloadedView);
                            }, this));
                        }
                    }
                // switching from a preloaded view to another preloaded view
                }else if (type === this.TYPE_PRELOAD && this.preloadedView){
                    if (this.preloadedView.__proto__ !== newViewType.prototype){
                        removalPromise = this.preloadedView.removeView(newPath);
                    }
                }
                // so we're transitioning away to a normal view (not an overlay or preloaded view)
                // but the preloadedView isn't right, so trigger both to transition out at the same time
                if (removalPromise && type === this.TYPE_NORMAL &&
                    this.preloadedView && this.preloadedView.__proto__ !== newViewType.prototype){
                    removalPromise = $.when(removalPromise, this.preloadedView.removeView(newPath));
                    this.preloadedView = null;
                }
                return removalPromise;
            },
            _fetchNextViewData: function(activeView, NextViewType, toPath, preload){
                if (activeView && activeView.__proto__ === NextViewType.prototype && !activeView.shouldFetchData(toPath)){
                    if(this.DEBUG) console.log('Router: fetching data skipped for path: ', toPath);
                    return;
                }
                if(this.DEBUG) console.log('Router: Ajax Load Path: ', toPath);
                var requestPromise = this.fetchHtml(toPath, null, !preload);
                requestPromise.fail(_.bind(function(e){
                    if (e){
                        if (e.status === 500){
                            Alert.showError('Connection Error... (INTERNAL SERVER ERROR)');
                        }else if (e.status === 404){
                            Alert.showError('Connection Error... (FILE NOT FOUND)');
                        }else if (e.status !== 200 && e.status){
                            // if the status is 200, we just got canceled, otherwise we show some error message
                            Alert.showError('Connection Error... (' + (e.statusText || 'Unknown Error') + ')');
                        }
                        if (e.status !== 200 && e.status){
                            // if the status is 200, means the request succeeded, but the promise was rejected/aborted
                            // status of 0 means the connection itself was aborted
                            window.history.back();
                        }
                    }
                }, this));
                return requestPromise;
            },
            _transitionFromOverlayScroll: function(activeView){
                if (activeView){
                    activeView.$el.removeClass('fixed').css('top', '');
                    var navBreakingOffset = this._getBreakingNewsOffset();
                    this.scrollEl.scrollTop(Math.abs(this.scrollTop - navBreakingOffset));
                }
            },
            _transitionToOverlayScroll: function(activeView){
                if (activeView){
                    this.scrollTop = this._getBreakingNewsOffset() - App.getScrollPosition();
                    activeView.$el.stop().addClass('fixed').css('top', this.scrollTop);
                    this.scrollEl.scrollTop(0);
                }
            },
            _getBreakingNewsOffset: function(){
                return this.nav.outerHeight() + this.breaking.outerHeight();
            },
            /**
             * Registers a navigation animation that should defer all incoming ajax requests
             * @param deferred
             */
            startNavigationAnimation: function(deferred){
                if (this.deferredTransaction){
                    console.warn('registering a duplicate navigation animation, is this intentional?');
                    this.deferredTransaction = $.when(this.deferredTransaction, deferred);
                }else{
                    this.deferredTransaction = deferred;
                }
                this.deferredTransaction.always(_.bind(function(){
                    this.deferredTransaction = null;
                }, this));
                return this.deferredTransaction;
            },
            /**
             * Helper function that auto populates fetchData with the isHtml flag being true
             * @param path
             * @param options
             * @param isNavigation
             * @param isStatic
             * @return {jQuery}
             */
            fetchHtml: function(path, options, isNavigation, isStatic){
                return this.fetchData(path, options, isNavigation, isStatic, true);
            },
            /**
             * Fetch data from server via AJAX. Takes a path to fetch and a
             * callback to parse the data and initialize views.
             * @param {String} path The path to the ajax endpoint.
             * @param {Boolean} isNavigation specifies if this request is a navigation request
             *                  or a background loading request
             * @param {Boolean} isStatic tells the ajax request whether to add
             *      the pjax headers or not
             * @param {Boolean} isHtml will return a quickly built jQuery dom object
             * @return {jQuery Promise Object}
             */
            fetchData: function(path, options, isNavigation, isStatic, isHtml) {
                var proceed = this._initializeFetchData(path, isNavigation);
                if (!proceed){
                    var promise = $.Deferred().reject();
                    promise.abort = function(){};
                    return promise;
                }
                var extra_headers = {};

                if (isStatic) {
                    extra_headers['X-PJAX'] = "true";
                    extra_headers['X-PJAX-Container'] = "#overlay";
                }
                if (!path){
                    path = '/';
                }else if (path.indexOf('/') !== 0){
                    path = '/' + path;
                }
                options = $.extend({
                    url: path,
                    data: {ajax: true},
                    beforeSend: function(xhr){
                        $.each(extra_headers, function(k, v){
                            xhr.setRequestHeader(k, v);
                        });
                    }
                }, options);

                var ajaxPromise = $.ajax(options);
                if (this.DEBUG){
                    ajaxPromise.fail(function(e){
                        if (e.statusText !== 'abort'){
                            console.log('fetchData Error: ', e.statusText);
                        }
                    });
                }
                var deferred = $.Deferred();
                this._registerNavDoneHandlers(ajaxPromise, deferred, isHtml);
                this._registerNavRequests(ajaxPromise, deferred, isNavigation);
                var returnPromise = deferred.promise();
                returnPromise.abort = _.bind(function(){
                    ajaxPromise.abort();
                }, this);
                return returnPromise;
            },
            _registerNavDoneHandlers: function(ajaxPromise, deferred, isHtml){
                if (this.deferredTransaction){
                    // stall all callbacks until the main transaction finishes
                    $.when(ajaxPromise, this.deferredTransaction).done(_.bind(function(h){
                        if (isHtml){
                            h[0] = this._quickGenerateJQueryObject(h[0]);
                        }
                        deferred.resolveWith(this, [h[0]]);
                    }, this)).fail(function(e){
                        deferred.rejectWith(this, [e]);
                    });
                }else{
                    ajaxPromise.done($.proxy(function(h){
                        if (isHtml){
                            h = this._quickGenerateJQueryObject(h);
                        }
                        deferred.resolveWith(this, [h]);
                    }, this)).fail(function(e){
                        deferred.rejectWith(this, [e]);
                    });
                }
            },
            _registerNavRequests: function(ajaxPromise, deferred, isNavigation){
                if (isNavigation){
                    this.navRequest.xhr = ajaxPromise;
                    this.navRequest.deferred = deferred;
                    // clear the timer on ajax complete to avoid harassing the user
                    ajaxPromise.always(
                        $.proxy(function() {
                            if (this.navRequest){
                                clearInterval(this.navRequest.xhrTimer);
                            }
                        }, this)
                    );
                    // only clear out the nav request when the promise is done
                    deferred.always(
                        $.proxy(function() {
                            this.navRequest = null;
                        }, this)
                    );
                }else{
                    this.xhrList.push(ajaxPromise);
                    ajaxPromise.always($.proxy(function(){
                        var index = $.inArray(ajaxPromise, this.xhrList);
                        if (index != -1){
                            this.xhrList.splice(index, 1);
                        }else{
                            console.warn('fetchData: ajaxPromise not found in xhrList');
                        }
                    }, this));
                }
            },
            /**
             * This function avoids all the cleanup functions of jQuery and improves performance greatly
             * @param htmlFrag
             * @return {jQuery Object}
             * @private
             */
            _quickGenerateJQueryObject: function(htmlFrag){
                var div = document.createElement('div');
                div.innerHTML = htmlFrag;
                return $(div).children();
            },
            /**
             * This private function validates whether or not this fetch data should proceed
             * as well as setting up the internal structures necessary to track the request
             * @param {String} path The path being requested
             * @param {Boolean} isNavigation Boolean marking whether this is a nav or background request
             * @return {Boolean} true or false depending on whether the request should proceed
             * @private
             */
            _initializeFetchData: function(path, isNavigation){
                if (!isNavigation){
                    if (this.navRequest){
                        // reject any background requests when we're changing navigation
                        return false;
                    }
                }else{
                    if (this.navRequest){
                        // we have an existing nav request, what should we do?
                        if (this.navRequest.path === path){
                            // double clicking a link, reject it
                            return false;
                        }
                        // Kill any pending xhr requests.
                        if (this.navRequest.xhr){
                            this.navRequest.xhr.abort();
                        }
                        // aborting the xhr might have rejected the deferred already and cleared it out
                        if (this.navRequest && this.navRequest.deferred){
                            this.navRequest.deferred.reject();
                        }
                    }
                    // abort all background requests
                    jQuery.each(this.xhrList, function(i, item){
                        item.abort();
                    });
                    this.xhrList = [];
                    this.navRequest = {path: path,
                        xhrTimer: setInterval($.proxy(function() {
                            //* COMMENTED OUT BY Mark Kennedy (mdkennedy@gannett.com)
                            // * Now that we have a new Loader class (loader.js), we need to figure out whether the following code should use the "Alert" or "Loader" class or whether we even need it at all. As a user, I'm not sure if showing me a 'your connection is slow message' is all that helpful unless it has some sort of option to continue to wait or browse something else.

                            // if(this.navRequest && this.navRequest.path === path) {
                            //     Alert.showLoader(this.SLOW_CONNECTION_MESSAGE);
                            // }
                        }, this), this.XHR_REMIND_TIME)};
                }
                return true;
            }
        });
        /**
         * return singleton
         */
        return new StateManager();
    }
);