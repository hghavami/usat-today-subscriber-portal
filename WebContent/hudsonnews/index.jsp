<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/hudsonnews/Index.java" --%><%-- /jsf:pagecode --%>
<%-- %@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"% --%>
<%-- tpl:insert page="/theme/themeV3/jsfTheme/subscriberPortalNoNav_JSF_1.jtpl" --%><!DOCTYPE HTML><%@page import="com.usatoday.business.interfaces.products.ProductIntf"%>
<%@page import="com.usatoday.businessObjects.products.ProductBO"%>
<%@page import="com.usatoday.util.MobileDeviceDetectionUtil"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%><%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%><%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%><f:view><f:loadBundle basename="resourceFile" var="labels" />
	<%-- tpl:insert page="/theme/subscriptionPortalRoot_1.htpl" --%><HTML>
<HEAD>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8">
<%-- tpl:put name="headerareaTop" --%>
<%
	String docProtocol = "https:";
	if (!request.isSecure()) {
		docProtocol = "http:"; 
	}
	
	// change this to any javascript you want called on page load
	String initFunction = "";
	
	com.usatoday.esub.common.WebBug trackingBug = com.usatoday.esub.common.UTCommon.getWebAnalyticsBug();
	try {
		trackingBug.deriveValuesFromRequest(request);
	}
	catch (Exception e) { 
		; // ignore
	}
	
	String pubCode = "";
	String productName = "USA TODAY";
	
    boolean isEEditionProduct = true;

	com.usatoday.businessObjects.products.promotions.PromotionSet defaultPromotionSet = null;
	com.usatoday.businessObjects.products.promotions.PromotionSet currentOfferPromotionSet = null;
	com.usatoday.business.interfaces.customer.CustomerIntf customer = null;
   	com.usatoday.businessObjects.products.promotions.PromotionSet currentAccountPromotionSet = null;
   		
	com.usatoday.business.interfaces.products.OfferIntf offer = null;   		
	try {
		offer = com.usatoday.esub.common.UTCommon.getCurrentOfferVersion2(request);
		pubCode = com.usatoday.util.constants.UsaTodayConstants.UT_PUBCODE;
		if (offer != null) {
			pubCode = offer.getPubCode();
			try {
				String brandingPubCode = offer.getProduct().getBrandingPubCode();
				ProductIntf bp = ProductBO.fetchProduct(brandingPubCode);
				productName = bp.getName();
			}
			catch (Exception teee) {
			}							
		}
		// get the default promotion set for the publication		
		defaultPromotionSet = com.usatoday.esub.common.UTCommon.getDefaultPromotionSetForPub(pubCode);
		
		// get the keycode specific promotion set
		
		if (offer != null && offer instanceof com.usatoday.business.interfaces.products.SubscriptionOfferIntf) {
			currentOfferPromotionSet = ((com.usatoday.business.interfaces.products.SubscriptionOfferIntf)offer).getPromotionSet();
		}
		boolean isSignedIn = false;

		customer = (com.usatoday.business.interfaces.customer.CustomerIntf)session.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_CUSTOMER_INFO);
		
		if (customer != null) {
			isSignedIn = true;
			com.usatoday.business.interfaces.products.SubscriptionProductIntf product = com.usatoday.businessObjects.products.SubscriptionProductBO.getSubscriptionProduct(customer.getCurrentEmailRecord().getPubCode());
			isEEditionProduct = product.isElectronicDelivery(); 
			// This promotion set is based on pub and keycode on the customer account
			currentAccountPromotionSet = com.usatoday.businessObjects.products.promotions.PromotionManager.getInstance().getPromotionsForOffer(customer.getCurrentAccount().getPubCode(), customer.getCurrentAccount().getKeyCode());
			
		}
	} catch (Exception e) {
	
	}

	String mobileCSS = "";
	if (MobileDeviceDetectionUtil.isSmallScreenDevice(request)) {
		mobileCSS = "<LINK href=\"/theme/themeV3/css/smallScreen.css\" rel=\"stylesheet\" type=\"text/css\">";
	}
%>
<script src="/common/prototype/prototype.js"></script>
<script src="/common/prototype/scriptaculous/scriptaculous.js" type="text/javascript"></script>
<link rel="stylesheet"	href="/theme/themeV3/css/stylesheet_jsfcomponents.css"	type="text/css">
<%-- tpl:put name="headerarea_Top" --%>
		<title>e-newspaper</title>
	<%-- /tpl:put --%>
<%-- /tpl:put --%>
<LINK REL="SHORTCUT ICON" HREF="/favicon.ico">
<LINK REL="icon" href="/favicon.ico" TYPE="image/ico" />
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META http-equiv="Content-Style-Type" content="text/css">
<meta name="viewport" content="initial-scale=1.0">
<LINK href="/theme/themeV3/css/usatodayBase_1.css" rel="stylesheet"
	type="text/css">
<LINK href="/theme/themeV3/css/usatodayAddOn_1.css" rel="stylesheet"
	type="text/css">
<script src="/common/usatCommon.js"></script>
<%-- tpl:put name="headareaBottom" --%>
<%=mobileCSS %>
<%-- tpl:put name="headarea_Bottom" --%>
		<!-- Bottom Header area -->
	<%-- /tpl:put --%>
<script>
// method called on page load
function initPage(thisObj, thisEvent) {
	// insert site-wide code here
	
	// for product messageing in popUpOverlay.js
	usat_productName = "<%=productName %>";
	
	<%=initFunction %>
}
</script>
<%-- /tpl:put --%>
<script type="text/javascript">
	/*   var _gaq = _gaq || [];
	 _gaq.push(['_setAccount', 'UA-23158809-1']);
	 _gaq.push(['_trackPageview']);

	 (function() {
	 var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	 ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	 var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	 })();
	 */
</script>
</HEAD>
<body>
<!-- 	<DIV id="wrapper"> -->
		<DIV id="header_1">
<!-- 		<div id=contactUsText> -->
<!-- 			CONTACT US -->
<!-- 		</div> -->
<!-- 		<div id=contactUsNumber> -->
<!-- 			1-800-872-0001 -->
<!-- 		</div>				 -->
<!-- 			<div id="headerLeft" onclick="templateLogoClick();"></div> -->
<!-- 			<div id="headerRight"> -->
				<%-- tpl:put name="headerbodyarea" --%>

		<%-- /tpl:put --%>
<!-- 			</div> -->
		</DIV>
		<!-- end header -->
		<DIV id="content">
			<DIV id="bluebox">
				<%-- tpl:put name="bodyAreaMainWhiteBox" --%>
<DIV id="pageContentFullWidth_1">
			<%-- tpl:put name="mainBodyArea" --%>
							<%
			try {
				// Check for already accessed cookie and if so forward to e-newspaper
				String eEditionLink = com.usatoday.olive.partners.HudsonNewsProcessor.checkEnewspaperAvailable(request);
				if (eEditionLink != null && !eEditionLink.trim().equals("")) {
					response.sendRedirect(eEditionLink);
				}
			} catch (Exception e) {
				System.out.println ("Could not process Hudson News access cookie, because: " + e);			
			}
			%>
							<hx:scriptCollector id="scriptCollector1"
								preRender="#{pc_Index3.onPageLoadBegin}">
								<h:form styleClass="form" id="form1">
									<br>
								<h:messages styleClass="messages_1" id="messages1"></h:messages>
								<h:panelGroup styleClass="panelGroup" id="group1">
										<h:panelGrid styleClass="panelGrid" id="grid1">
 										<hx:panelFormBox id="formBox1" styleClass="panelFormBox" 
											helpPosition="over" labelPosition="left">
											<hx:graphicImageEx styleClass="formImageIndent" id="imageEx1" value="../theme/themeV3/themeImages/USATODAY_SubHeader_940x36.jpg"></hx:graphicImageEx>
											<hx:formItem styleClass="formItemIndentWhite" id="accessCodeFormItem"
												label="#{labels.AccessCode}" releaseMargin="label">
												<h:inputText styleClass="inputText" id="accesscode"
													value="#{hudsonNewsHandler.accesscode}" required="true"
													requiredMessage="Access code is required.">
													<f:validateLength minimum="6" maximum="6"></f:validateLength>
												</h:inputText>
											</hx:formItem>
											<h:outputText styleClass="outputText" id="text1"></h:outputText>
											<hx:commandExButton type="submit" value="Submit"
												styleClass="formItemIndent" id="authAccessCodeSubmitButton"
												action="#{pc_Index3.doAuthAccessCodeSubmitButtonAction}">
											</hx:commandExButton>
											<h:outputText styleClass="outputText" id="text3"></h:outputText>
											<h:outputText styleClass="outputText" id="text4"></h:outputText>
										</hx:panelFormBox>
									</h:panelGrid>
									</h:panelGroup>
								</h:form>
							</hx:scriptCollector>
						<%-- /tpl:put --%>
</DIV>
<%-- /tpl:put --%>
				<DIV id="clear"></DIV>
				<%-- tpl:put name="footerMainArea" --%>
	<%-- tpl:put name="footerArea_1" --%><%-- /tpl:put --%>
	<%=trackingBug.getOmnitureHTML() %>
		<%-- /tpl:put --%>
				<DIV id=graybar_1>
				<DIV id=graybar_1_text>
					<br>&copy;
					<script>
						document.write(getCurrentYear());
					</script>
					USA TODAY, a <a href="http://www.gannett.com">Gannett Company</a> <br>
					<a href="http://www.usatoday.com/legal/privacynotice.html">Privacy
						Notice/California Privacy Notice</a>| <a
						href="http://www.usatoday.com/legal/privacynotice.html#adchoices">Ad
						Choices</a>.<br> By using this service, you accept our <a
						href="http://www.usatoday.com/legal/tos.html">Terms of Agreement</a>.
				</DIV>
			</DIV>
				<!--END OF COPYWRITE-->
			</DIV>
			<!--END OF WHITEBOX-->
		</DIV>
		<!--END OF CONTENT-->
<!-- 	</DIV> -->
</BODY>
</HTML><%-- /tpl:insert --%>
</f:view> <%-- /tpl:insert --%>