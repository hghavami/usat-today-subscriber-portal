<%-- tpl:insert page="/theme/subscriptionPortalJSPPage.jtpl" --%><!DOCTYPE HTML><%@page import="com.usatoday.businessObjects.products.ProductBO"%>
<%@page import="com.usatoday.business.interfaces.products.ProductIntf"%>
<%@page import="com.usatoday.util.MobileDeviceDetectionUtil"%>
<%-- tpl:insert page="/theme/subscriptionPortalRoot.htpl" --%><HTML>
<HEAD>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8">
<%-- tpl:put name="headerareaTop" --%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%
	String docProtocol = "https:";
	if (!request.isSecure()) { 
		docProtocol = "http:";
	} 
	// following work around to get around problem with RAD 8.0.2 automated links in templates - APE
	String subcribeJS = "javascript:_subscribeToPub();"; 
	String subcribeIntJS = "javascript:_subscribeToInternationalEdition();"; 
	String backIssueJS = "javascript:_shopUSAT();";
	 
	String navigationSubscribeLinkValue = "/subscriptions/index.jsp";
	String navigationGiveGiftLinkValue = "/subscriptions/order/checkout.faces";
	String electronicEditionLink = "/subscriptions/order/checkout.faces?pub=EE";
	String internationalEditionLink = "/international/welcomeint.jsp";
	String FAQLink = "/faq/utfaq.jsp";
	String subscribeByMail = "/subscriptions/subscribebymail.html";
	String dynamicNavigation = "";
	
	String closingHREF = "";
	String printEditionCSClass = "showMe";
	String eEditionCSClass = "hideMe";
	String ourPledgeCSClass = "showMe";
	
	// Following 4 attributes are only modified in the sub-template for Order Entry
	String navigationPromoImg = "/images/shim.gif";
	String navigationPromoImgAltText = "";
	String navigationPromoImgHREF = "";
	String navClosingHref = "";
	String navigationPromoFlash = "";
	String navigationPromoImgClassName = "promoImages";
	
	String navPubImg = "/theme/images/nav_top.jpg";
	String initFunction = "";  // pages using this template can change this value to be the name of a javascript function that will
							   // be called when the page loads.
	String unloadFunction = "";
	String onBeforeUnloadFunction = "";

	boolean isSignedIn = false;
	String pubCode = "";
	String productName = "USA TODAY";
    boolean isEEditionProduct = false;
   	com.usatoday.businessObjects.products.promotions.PromotionSet currentAccountPromotionSet = null;
	
	com.usatoday.business.interfaces.customer.CustomerIntf customer = null;
	// check if logged in and/or log in from cookie
	try {

		customer = (com.usatoday.business.interfaces.customer.CustomerIntf)session.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_CUSTOMER_INFO);

		if (customer != null) {
			isSignedIn = true;
			com.usatoday.business.interfaces.products.SubscriptionProductIntf product = com.usatoday.businessObjects.products.SubscriptionProductBO.getSubscriptionProduct(customer.getCurrentEmailRecord().getPubCode());
			isEEditionProduct = product.isElectronicDelivery(); 
			if (isEEditionProduct) {
				// hide the Buttons for vaca holds and complaints
				printEditionCSClass = "hideMe";
				// This promotion set is based on pub and keycode on the customer account
				currentAccountPromotionSet = com.usatoday.businessObjects.products.promotions.PromotionManager.getInstance().getPromotionsForOffer(customer.getCurrentAccount().getPubCode(), customer.getCurrentAccount().getKeyCode());
			}
			
		}
	}
	catch (Exception e){ 
		;
	}
		
	// Promotional Stuff
	com.usatoday.businessObjects.products.promotions.PromotionSet defaultPromotionSet = null;
	com.usatoday.businessObjects.products.promotions.PromotionSet currentOfferPromotionSet = null;
	
	// Omniture tracking
	com.usatoday.esub.common.WebBug trackingBug = com.usatoday.esub.common.UTCommon.getWebAnalyticsBug();
	try {
		
		com.usatoday.business.interfaces.products.OfferIntf offer = com.usatoday.esub.common.UTCommon.getCurrentOffer(request);
		
		pubCode = com.usatoday.util.constants.UsaTodayConstants.UT_PUBCODE;
		if (offer != null) {
			pubCode = offer.getPubCode();
			try {
				String brandingPubCode = offer.getProduct().getBrandingPubCode();
				ProductIntf bp = ProductBO.fetchProduct(brandingPubCode);
				productName = bp.getName();
			}
			catch (Exception teee) {
			}				
		}
		
		
		if (pubCode.equalsIgnoreCase(com.usatoday.util.constants.UsaTodayConstants.SW_PUBCODE)) {
				navigationSubscribeLinkValue = "/subscriptions/order/checkout.faces";
				navPubImg = "/theme/images/nav_top_sw2.jpg";
				electronicEditionLink = "http://www.usatoday.com/marketing/brand_mkt/splash/spw_electronic_edition/index.html";
				internationalEditionLink = "/international/welcomeswint.jsp";
				FAQLink = "/faq/bwfaq.jsp";
				subscribeByMail = "/subscriptions/subscribebymail_sw.html";
		}

		// get the default promotion set for the publication		
		defaultPromotionSet = com.usatoday.esub.common.UTCommon.getDefaultPromotionSetForPub(pubCode);
		
		// get the keycode specific promotion set
		
		if (offer != null && offer instanceof com.usatoday.business.interfaces.products.SubscriptionOfferIntf) {
			currentOfferPromotionSet = ((com.usatoday.business.interfaces.products.SubscriptionOfferIntf)offer).getPromotionSet();
		}

		if (currentOfferPromotionSet != null) {
			com.usatoday.business.interfaces.products.promotions.HTMLPromotionIntf dPromo = currentOfferPromotionSet.getDynamicNavigationOrderEntryHTML();
			if (dPromo != null) {
				dynamicNavigation = dPromo.getPromotionalHTML();
			}
		}
		
		com.usatoday.business.interfaces.products.promotions.ImagePromotionIntf imagePromo = null;
		

		// Check for EE cancels allowed flag and keycode		
		if (com.usatoday.util.constants.UsaTodayConstants.EE_CANCELS_ALLOWED_PROP && isEEditionProduct && currentAccountPromotionSet != null) {						// Is it enabled in the properties file
			eEditionCSClass = "showMe";		
			if (currentAccountPromotionSet.getEECancelsAllowed() != null) {
				com.usatoday.util.constants.UsaTodayConstants.EE_CANCELS_ALLOWED = false;				// Flag to be used through our the app
				eEditionCSClass = "HideMe";
			}
		}
 
 
		if (!pubCode.equals(com.usatoday.util.constants.UsaTodayConstants.UT_PUBCODE)) {
			ourPledgeCSClass = "HideMe";
		}

		// set up navigation pane promotional graphic
		imagePromo = null;
		
		if (currentOfferPromotionSet != null && currentOfferPromotionSet.getTemplateNavigationPromoImage() != null) {
			imagePromo = currentOfferPromotionSet.getTemplateNavigationPromoImage();
		}
		else {
			imagePromo = defaultPromotionSet.getTemplateNavigationPromoImage();
		}
		
		if (imagePromo != null) {
			navigationPromoImg = imagePromo.getImagePathString();

			if (navigationPromoImg.endsWith(".swf")) {
				navigationPromoImgClassName = "hideMe";
				navigationPromoFlash = "<object classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" codebase=\"https://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0\" height=\"90\" width=\"150\"> " +
						"<param name=\"movie\" value=\"" + navigationPromoImg + "\" /> " +
						"<param name=\"quality\" value=\"best\" />" +
						"<param name=\"play\" value=\"true\" /> " +
						"<embed height=\"90\" pluginspage=\"https://www.macromedia.com/go/getflashplayer\" src=\"" + navigationPromoImg + "\" type=\"application/x-shockwave-flash\" width=\"150\" quality=\"best\" play=\"true\"></embed> " +
					    "</object>";
				navigationPromoImg = "/images/shim.gif";
			}			
			else {
				navigationPromoImgAltText = (imagePromo.getImageAltText() == null) ? "" : "alt=\"" + imagePromo.getImageAltText() + "\"";
				navigationPromoImgHREF = (imagePromo.getImageLinkToURL() == null || imagePromo.getImageLinkToURL().length()==0) ? "" : "<a href=\"" + imagePromo.getImageLinkToURL() + "\">";
				if (navigationPromoImgHREF.length() > 0) {
					navClosingHref = "</a>";
				}
			}
		}

		// figure out tracking
		trackingBug.deriveValuesFromRequest(request);
	} 
 	catch (Exception e) {	
		System.out.println("Exception building Page Template Inside Pages: " + e.getMessage());
		request.getRequestDispatcher("/err/err.html").forward(request, response);
		return;
	}    
	
	String mobileCSS = "";
	if (MobileDeviceDetectionUtil.isSmallScreenDevice(request)) {
		mobileCSS = "<LINK href=\"/theme/themeV3/css/smallScreen.css\" rel=\"stylesheet\" type=\"text/css\">";
	}
			
%>
<script src="/common/prototype/prototype.js"></script>
<script src="/common/prototype/scriptaculous/scriptaculous.js" type="text/javascript"></script>
	<%-- tpl:put name="headerarea_Top" --%>
		<TITLE>USA Today Online Subscriptions - First Time Account
			Setup</TITLE>
		<META HTTP-EQUIV="expires" CONTENT="0">
	<%-- /tpl:put --%>
	<%-- /tpl:put --%>
<LINK REL="SHORTCUT ICON" HREF="/favicon.ico">
<LINK REL="icon" href="/favicon.ico" TYPE="image/ico" />
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META http-equiv="Content-Style-Type" content="text/css">
<meta name="viewport" content="initial-scale=1.0">
<LINK href="/theme/themeV3/css/usatodayBase.css" rel="stylesheet"
	type="text/css">
<LINK href="/theme/themeV3/css/usatodayAddOn.css" rel="stylesheet"
	type="text/css">
<script src="/common/usatCommon.js"></script>
<%-- tpl:put name="headareaBottom" --%>
<%=mobileCSS %>
		<%-- tpl:put name="headarea_Bottom" --%>

		<%
			String phoneNum1 = "";
			String phoneNum2 = "";
			String phoneNum3 = "";
			String emailAddress = "";
			String zipCode = "";
			String password = "";
			String errorMsg = "";
			int errorID = 0;
			String proceedButton = "Continue";
			String faq = "";

			try {

				String pubcode = (String) session
						.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_PUB_NAME);
				if (pubcode
						.equals(com.usatoday.esub.common.UTCommon.PUB_NAMES[0])) { // USATODAY publication
					faq = "utfaq.jsp";
				} else {
					faq = "bwfaq.jsp";
				}
			} catch (NullPointerException e) {
			}
			try {
				com.usatoday.esub.common.UTFirstTime data = null;
				data = com.usatoday.esub.common.UTCommon
						.getFirstTimeSessionDefaultDataObject(request);
				if (data != null) {
					phoneNum1 = "";
					phoneNum2 = "";
					phoneNum3 = "";
					//zipCode = data.getDeliveryZip();  sew
					zipCode = "";
					emailAddress = data.getEmailAddress();
					password = data.getPassword();
					errorMsg = data.getErrorMsg();
					errorID = data.getErrorID();

				}
			} catch (NullPointerException e) {
				//System.out.println( "Null Pointer Exception in /subscriptions/newsubscriptions/index.jsp");
			}
			String accountPhoneReset = "account_phone_reset($('firsttimeForm'));";
			initFunction += "; checkErrors( $('firsttimeForm'), errorMsg, errorID); account_phone_clear($('firsttimeForm'));";
		%>
		<SCRIPT language="JavaScript" src="/common/pwdMaxLengthValidator.js"></SCRIPT>
		<SCRIPT language="JavaScript" src="/common/trimString.js"></SCRIPT>
		<SCRIPT language="JavaScript" src="/common/autotab.js"></SCRIPT>
		<SCRIPT language="JavaScript">
<!--
var errorMsg = "<%=errorMsg%>";
var errorID = "<%=errorID%>";
function FrontPage_Form1_Validator(theForm)
{ 
	var accnt_ID = trim(theForm.ACCNT_ID.value);
	var phone_Num1 = trim(theForm.PHONE_NUM1.value);
	var phone_Num2 = trim(theForm.PHONE_NUM2.value);
	var phone_Num3 = trim(theForm.PHONE_NUM3.value);	
	if (phone_Num1 == "" && phone_Num2 == "" && phone_Num3 == "") {
	  with (theForm.ACCNT_ID)
	  {
	  	var charsNotAllowed = new Array("%",".","?","//","\\","^",":","|","<",">",'"',"+","\n");
	 	for ( var i = 0; i < charsNotAllowed.length; i++ ) {
			if (accnt_ID.indexOf(charsNotAllowed[i]) > -1) {
				var alertMsg = "Please enter a new account number without \"%\", \".\", \"?\", \"\/\", \"<\", \">\", \"(\", \")\", '\"', or \"&\" characters.";
				alert(alertMsg);
	            theForm.ACCNT_ID.focus();
	            return (false);
			}
		}
	 	var newValue = (value);
	 	var newLength = (value.length);
	 	var newFlag = 0;
	
		if (value.length != 7)
		{ 
	 	 newFlag = 1; 	
		} 
		
		for(var i = 0; i != newLength; i++) 
	 	{ 	
	  	 aChar = newValue.substring(i,i+1);
	  	 if(aChar < "0" || aChar > "9") 
	  	 {
	  	   newFlag = 1;  	 
	     }
	    } 
	    if ((newLength < 7 && newLength > 0) || (newLength == 7 && newFlag == 1)) {
	      alert("The \"Account Number\" entered is not valid.  Please retry or call the Customer Service number below.  ");  
	      theForm.ACCNT_ID.focus();	  
	      return (false);
	    }
	    if (newLength == 0) {
	      alert("Please enter a value for the  \"Account Number\" option.");  
	      theForm.ACCNT_ID.focus();	  
	      return (false);
	    }
	  }
	}
	if (accnt_ID == "") {
	// Check phone number field 1	
	 with (theForm.PHONE_NUM1)
	  {
	 	var newValue = (value);
	 	var newLength = (value.length);
	 	var newFlag = 0;
	
		if (value.length != 3)
		{ 
	 	 newFlag = 1; 	
		} 
		
		for(var i = 0; i != newLength; i++) 
	 	{ 	
	  	 aChar = newValue.substring(i,i+1);
	  	 if(aChar < "0" || aChar > "9") 
	  	 {
	  	   newFlag = 1;  	 
	     }
	    } 
	    if ((newLength < 3 && newLength > 0) || (newLength == 3 && newFlag == 1)) {
	      alert("The \"Phone Number\" entered is not valid.  Please retry or call the Customer Service number below.  ");  
	      theForm.PHONE_NUM1.focus();	  
	      return (false);
	    }
	    if (newLength == 0) {
	      alert("Please enter a value for the  \"Phone Number\" option.");  
	      theForm.PHONE_NUM1.focus();	  
	      return (false);
	    }
	  }
	// Check phone number field 2
	 with (theForm.PHONE_NUM2)
	  {
	 	var newValue = (value);
	 	var newLength = (value.length);
	 	var newFlag = 0;
	
		if (value.length != 3)
		{ 
	 	 newFlag = 1; 	
		} 
		
		for(var i = 0; i != newLength; i++) 
	 	{ 	
	  	 aChar = newValue.substring(i,i+1);
	  	 if(aChar < "0" || aChar > "9") 
	  	 {
	  	   newFlag = 1;  	 
	     }
	    } 
	    if ((newLength < 3 && newLength > 0) || (newLength == 3 && newFlag == 1)) {
	      alert("The \"Phone Number\" entered is not valid.  Please retry or call the Customer Service number below.  ");  
	      theForm.PHONE_NUM2.focus();	  
	      return (false);
	    }
	    if (newLength == 0) {
	      alert("Please enter a value for the  \"Phone Number\" option.");  
	      theForm.PHONE_NUM2.focus();	  
	      return (false);
	    }
	  }
	// Check phone number field 3	
	 with (theForm.PHONE_NUM3)
	  {
	 	var newValue = (value);
	 	var newLength = (value.length);
	 	var newFlag = 0;
	
		if (value.length != 4)
		{ 
	 	 newFlag = 1; 	
		} 
		
		for(var i = 0; i != newLength; i++) 
	 	{ 	
	  	 aChar = newValue.substring(i,i+1);
	  	 if(aChar < "0" || aChar > "9") 
	  	 {
	  	   newFlag = 1;  	 
	     }
	    } 
	    if ((newLength < 4 && newLength > 0) || (newLength == 4 && newFlag == 1)) {
	      alert("The \"Phone Number\" entered is not valid.  Please retry or call the Customer Service number below.  ");  
	      theForm.PHONE_NUM3.focus();	  
	      return (false);
	    }
	    if (newLength == 0) {
	      alert("Please enter a value for the  \"Phone Number\" option.");  
	      theForm.PHONE_NUM3.focus();	  
	      return (false);
	    }
	  }
	}

  var emailAddress = trim(theForm.EMAIL.value);	
  if ( emailAddress != "") 
  	{
	apos=emailAddress.indexOf("@");
	dotpos=emailAddress.lastIndexOf(".");
	lastpos=emailAddress.length-1;
	if (apos<1 || dotpos-apos<2 || lastpos-dotpos>3 || lastpos-dotpos<2) 
	{
		alert("The \"E-mail Address\" entered is not valid.  Please retry or call the Customer Service number below.");
  		theForm.EMAIL.focus();
   		return (false);
    }
  } else 
  {
	alert("Please enter a value for the \"E-mail Address\" option.");
	theForm.EMAIL.focus();
	return (false);
  }	

  with (theForm.ZIPCODE)
  {
 	var newValue = (value);
 	var newLength = (value.length);
 	var newFlag = 0;

	for(var i = 0; i != newLength; i++) 
 	{ 	
  	 aChar = newValue.substring(i,i+1);
  	 if(aChar < "0" || aChar > "9") 
  	 {
  	   newFlag = 1;  	 
     }
    } 

    if (newLength == 0) {
      alert("Please enter a value for the \"ZipCode\" option.");  
      theForm.ZIPCODE.focus();	  
      return (false);
    }
    
    if ((newLength < 5 && newLength > 0) || (newLength == 5 && newFlag == 1)) {
      alert("The \"ZipCode\" entered is not valid.  Please retry or call the Customer Service number below.");  
      theForm.ZIPCODE.focus();	  
      return (false);
    }
  }


  if (theForm.PASSWORD.value == "")
  {
    alert("Please enter a value for the \"Password\" option.");
    theForm.PASSWORD.focus();
    return (false);
  }

  var pattern1 = /^[a-zA-Z0-9]+$/;
  with (theForm.PASSWORD)
  {
 	    var newValue = (value);
 	    var newLength = (value.length);
 	    var newFlag = 0;
		   
		if(newLength < 5 || newLength > 30 || newValue.indexOf(' ') >= 0) 
  	    {
        	alert("Please enter a password between 5 and 30 alphanumeric with no spaces or special characters.");  
   		   	theForm.PASSWORD.focus();	  
	       	return (false); 
        }
   }

/*  with (theForm.PASSWORD)
	{
 		var charsNotAllowed = new Array("%",".","?","//","\\","^",":","|","<",">",'"',"+","\n");
	 	for ( var i = 0; i < charsNotAllowed.length; i++ ) {
			if (theForm.PASSWORD.value.indexOf(charsNotAllowed[i]) > -1) {
				var alertMsg = "Please enter a new password without \"%\", \".\", \"?\", \"\/\", \"<\", \">\", \"(\", \")\", '\"', or \"&\" characters.";
 */		
		if (!theForm.PASSWORD.value.match(pattern1)) 
		{
			var alertMsg = "Please enter a password between 5 and 30 alphanumeric with no spaces or special characters.";	
			alert(alertMsg);
            theForm.PASSWORD.focus();
		    return (false);
		}
//	}

  if (theForm.VERIFY_PASSWORD.value == "")
  {
    alert("Please enter a value for the \"Verify Password\" option.");
    theForm.VERIFY_PASSWORD.focus();
    return (false);
  }
  if (theForm.PASSWORD.value != theForm.VERIFY_PASSWORD.value)
  {
    alert("The \"Password\" option and the \"Verify Password\" option are not equal.");
    theForm.PASSWORD.focus();
    return (false);
  }
  	
  return (true);
}

function checkErrors(theForm, errorMsg, errorID)
{
  if (errorID > "0")
  {
      alert(errorMsg);   	  
 	  switch (errorID) {
	 	  case "1" :
			theForm.PASSWORD.focus();
			return (false);
			break;

	 	  case "2" :
			theForm.EMAIL.focus();
			return (false);
			break;

	 	  case "3" :
			theForm.EMAIL.focus();
			return (false);
			break;

	 	  case "4" :
			theForm.ACCNT_ID.focus();
			return (false);
			break;

	 	  case "5" :
			theForm.ACCNT_ID.focus();
			return (false);
			break;

	 	  case "6" :
			theForm.ACCNT_ID.focus();
			return (false);
			break;

	 	  case "7" :
			theForm.PHONE_NUM1.focus();
			return (false);
			break;
			
		  default:
			theForm.ACCNT_ID.focus();
			return (false);
			break;
 	}
  }  
  return (true);
}

function account_phone_clear(theForm)
{
 theForm.ACCNT_ID.disabled = false;
 theForm.PHONE_NUM1.disabled = false;
 theForm.PHONE_NUM2.disabled = false;
 theForm.PHONE_NUM3.disabled = false;
 var accnt_ID = trim(theForm.ACCNT_ID.value);
 var phone_Num1 = trim(theForm.PHONE_NUM1.value);
 var phone_Num2 = trim(theForm.PHONE_NUM2.value);
 var phone_Num3 = trim(theForm.PHONE_NUM3.value);
 if (accnt_ID != "")
 {
  theForm.PHONE_NUM1.disabled = true;
  theForm.PHONE_NUM1.value = "";
  theForm.PHONE_NUM2.disabled = true;
  theForm.PHONE_NUM2.value = "";
  theForm.PHONE_NUM3.disabled = true;
  theForm.PHONE_NUM3.value = "";
 } else {
	 if (phone_Num1 != "" || phone_Num2 != "" || phone_Num3 != "") 
	 {
	  theForm.ACCNT_ID.disabled = true;
	  theForm.ACCNT_ID.value = "";
	 }
 }
 return (true); 
}

function account_phone_reset(theForm)
{
 theForm.ACCNT_ID.disabled = false;
 theForm.ACCNT_ID.value = "";
 theForm.PHONE_NUM1.disabled = false;
 theForm.PHONE_NUM1.value = "";
 theForm.PHONE_NUM2.disabled = false;
 theForm.PHONE_NUM2.value = "";
 theForm.PHONE_NUM3.disabled = false;
 theForm.PHONE_NUM3.value = "";
 theForm.EMAIL.value = "";
 theForm.ZIPCODE.value = "";
 theForm.PASSWORD.value = "";
 theForm.VERIFY_PASSWORD.value = ""; 
 return (true); 
}

//-->

function func_1(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
/* 	var charsNotAllowed = new Array("%",".","?","//","\\","^",":","|","<",">",'"',"+","\n");
	for ( var i = 0; i < charsNotAllowed.length; i++ ) {
		if (thisObj.value.indexOf(charsNotAllowed[i]) > -1) {
			var alertMsg = "Please enter a new password without \"%\", \".\", \"?\", \"\/\", \"<\", \">\", \"(\", \")\", '\"', or \"&\" characters.";
 */		var pattern = /^[a-zA-Z0-9]+$/;
		if (!thisObj.value.match(pattern)) 
		{
			var alertMsg = "Please enter a password between 5 and 30 alphanumeric with no spaces or special characters.";	
			alert(alertMsg);
		    thisObj.focus();
		    return (false);
		}
}
function func_2(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	var charsNotAllowed = new Array("%",".","?","//","\\","^",":","|","<",">",'"',"+","\n");
	for ( var i = 0; i < charsNotAllowed.length; i++ ) {
		if (thisObj.value.indexOf(charsNotAllowed[i]) > -1) {
			var alertMsg = "Please enter a new account number without \"%\", \".\", \"?\", \"\/\", \"<\", \">\", \"(\", \")\", '\"', or \"&\" characters.";
			alert(alertMsg);
		    thisObj.focus();
		    return (false);
		}
	}
}

</SCRIPT>
	<%-- /tpl:put --%>
<SCRIPT>			
	var currentPub = "<%=pubCode %>";
	
	function initPage(thisObj, thisEvent) {
		try {
			document.getElementById('navPromoImage').src = "<%=navigationPromoImg%>";	
			document.getElementById('navPromoImage').className = "<%=navigationPromoImgClassName%>";
		}
		catch (err){}
		
		// for product messageing in popUpOverlay.js
		usat_productName = "<%=productName %>";
	
		<%=initFunction %>
	}	
			
	function unloadPage(thisObj, thisEvent) {		
		<%=unloadFunction %>
	}
				
	function onBeforeUnloadPage(thisObj, thisEvent) {		 
		<%=onBeforeUnloadFunction %>
	}
	
	function subscribeToPub() {
		document.location = "<%=navigationSubscribeLinkValue%>";
	}	
	
	function giveAGift() {
		document.location = "<%=navigationGiveGiftLinkValue%>";
	}
	
	function subscribeToElectronicEdition() {
		document.location = "<%=electronicEditionLink %>";
	}
	
	function subscribeToElectronicEditionOrderPage() {
		document.location = "<%=electronicEditionLink %>";
	}
	
	function subscribeToInternationalEdition() {
		document.location = "<%=internationalEditionLink%>";
	}

	function loadFAQ() {
		window.open('<%=FAQLink%>','FAQ', 'width=825,height=800,menubar=no,titlebar=yes,scrollbars=yes,status=no,location=no,directories=no');
	}

	function subscribeByMail() {
		document.location = "<%=subscribeByMail%>";
	}
			
</SCRIPT>
	<%-- /tpl:put --%>
<script type="text/javascript">
	/*   var _gaq = _gaq || [];
	 _gaq.push(['_setAccount', 'UA-23158809-1']);
	 _gaq.push(['_trackPageview']);

	 (function() {
	 var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	 ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	 var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	 })();
	 */
</script>
</HEAD>
<body>
	<DIV id="wrapper">
		<DIV id="header">
			<div id="headerLeft" onclick="templateLogoClick();"></div>
			<div id="headerRight">
				<%-- tpl:put name="headerbodyarea" --%>
					<%-- tpl:insert page="/login/loginform.jspf" --%><div style="float: none">
	<%
		String uri = request.getRequestURI();
		String pageName = uri.substring(uri.lastIndexOf("/") + 1);
		String jScript = "";
		if (uri.indexOf("subscriptions") > 0 && uri.indexOf("thankyou.jsp") > 0) {
			try {
				com.usatoday.esub.handlers.ShoppingCartHandler shoppingCartHandler = (com.usatoday.esub.handlers.ShoppingCartHandler) session
						.getAttribute("shoppingCartHandler");
				if (shoppingCartHandler != null) {
					jScript = shoppingCartHandler.getOnePageThankYouJavaScriptTealiumText();
				}
			} catch (Exception E) {

			}
		} else {
			com.usatoday.businessObjects.products.promotions.PromotionSet currentOfferPromotionSetTealium = null;
			com.usatoday.business.interfaces.products.OfferIntf offerTealium = com.usatoday.esub.common.UTCommon
					.getCurrentOffer(request);
			if (offerTealium != null && offerTealium instanceof com.usatoday.business.interfaces.products.SubscriptionOfferIntf) {
				currentOfferPromotionSetTealium = ((com.usatoday.business.interfaces.products.SubscriptionOfferIntf) offerTealium)
						.getPromotionSet();
				jScript = currentOfferPromotionSetTealium.getPromoOnePageJavaScriptTealiumText();
				jScript = org.apache.commons.lang3.StringUtils.replace(jScript, "page_name:\"\"", "page_name:\"" + uri + "\"");
			}
		}
	%>
	<%=jScript%>
</div>
<%-- 			<f:verbatim>
				<h:outputText styleClass="outputText" id="textThankYouJS13"	escape="false" value="#{shoppingCartHandler.tealiumOnePageThankYouJavaScriptText}"></h:outputText>
			</f:verbatim>
 --%>
<%
	String isLoginDisplay = "true";
	com.usatoday.business.interfaces.customer.CustomerIntf customer2 = null;
	String loggedinDivClass = "hideMe";
	String notLoggedinDivClass = "showMeInline";
	String firstName = "";
	String lastName = "";
	String firmInfo = "";
	String accountNum = "";
	String loginErrorMessage2 = "";
	String logOut = "";
	String publication = "";
	String changeAccountLink = "";
	String earlyAlertDisplay = "display:none";

	String jScriptNoOp = "javascript:;";

	try {
		publication = (String) session.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_PUB_NAME);
		if (publication == null) {
			publication = request.getParameter("pub");
			if (publication == null) {
				publication = com.usatoday.util.constants.UsaTodayConstants.UT_PUBCODE;
			}
		}
		if (publication.equals(com.usatoday.esub.common.UTCommon.PUB_NAMES[0])) { // USATODAY publication
			logOut = com.usatoday.esub.common.UTCommon.BASE_URL + "/index.jsp?pub=UT&logsessobj=rem";
		} else {
			logOut = com.usatoday.esub.common.UTCommon.BASE_URL + "/index.jsp?pub=BW&logsessobj=rem";
		}

		// If customer is logged in remove login page, otherwise display it
		customer2 = (com.usatoday.business.interfaces.customer.CustomerIntf) session
				.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_CUSTOMER_INFO);
		if (customer2 == null) {
			// showMeInling and hideMe are defined in style sheets
			loggedinDivClass = "showMeInline";
			notLoggedinDivClass = "hideMe";
		} else {

			if (customer2.getCurrentAccount() != null) {
				firstName = customer2.getCurrentAccount().getDeliveryContact().getFirstName();
				if (firstName != null && !firstName.trim().equals("")) {
					firstName = com.usatoday.esub.common.UTCommon.formatNameCapitalization(firstName); // Format it
				}
				lastName = customer2.getCurrentAccount().getDeliveryContact().getLastName();
				if (lastName != null && !lastName.trim().equals("")) {
					lastName = com.usatoday.esub.common.UTCommon.formatNameCapitalization(lastName); // Format it
				}
				firmInfo = customer2.getCurrentAccount().getDeliveryContact().getFirmName();
				if (customer2.getNumberOfAccounts() > 1) {
					changeAccountLink = "[&nbsp;<a href=\"/account/select_accnt.jsp\">Change</a>&nbsp;]";
				}
				try {
					accountNum = customer2.getCurrentAccount().getAccountNumber();
					//				if (accountNum == null || accountNum.length() < 4) {
					if (accountNum == null) {
						accountNum = "";
					}
					//				else {
					/*
					if (accountNum.equalsIgnoreCase("340000000")) {
						accountNum = "Pending Setup";
					}
					 */
					//				}
				} catch (Exception eeee) {
					eeee.printStackTrace();
				}

				//EarlyAlert - Check to see if their paper delivery has been affected by a delay
				// HD-CONS109 SEW
				/*
				if (customer2.getCurrentAccount().isShowDeliveryAlert()) {
				earlyAlertDisplay = "display:inline";
				}
				 */

			} else {
				if (customer2.getCurrentEmailRecord() != null
						&& customer2.getCurrentEmailRecord().getEmailAddress() != null) {
					firstName = customer2.getCurrentEmailRecord().getEmailAddress();
					accountNum = "Pending Setup";
				} else {
					firstName = "";
				}
			}
		}
		loginErrorMessage2 = (String) session.getAttribute("LOGIN_FAILED_MESSAGE");
		if (loginErrorMessage2 == null || loginErrorMessage2.trim().equals("")) {
			loginErrorMessage2 = "";
		} else {
			session.removeAttribute("LOGIN_FAILED_MESSAGE");
		}
	} catch (Exception e) {
		e.printStackTrace();
		if (firstName == null) {
			firstName = "";
		}
		if (lastName == null) {
			lastName = "";
		}
	}
	String nameInfo = firstName + " " + lastName;
	String loginInfo = null;
	if (!nameInfo.trim().equals("")) {
		loginInfo = nameInfo;
	} else {
		if (firmInfo != null && !firmInfo.trim().equals("")) {
			loginInfo = firmInfo;
		}
	}
	// if no login info, then hide data
	if (loginInfo == null) {
		loggedinDivClass = "showMeInline";
		notLoggedinDivClass = "hideMe";
		loginInfo = "";
	}
%>

<%@page import="com.usatoday.util.constants.UsaTodayConstants"%><div
	id="loginDisplayDiv" class="<%=loggedinDivClass%>">
	<table border="0" width="100%" height="60px">
		<tr>
			<td>
				<TABLE class="loginFormTable">
					<TBODY>
						<TR>
							<TD class="loginFormColumn"><br /> <a
								href="/login/auth.faces">Login to Manage Account</a></TD>
						</TR>
						<tr>
							<td class="loginFormColumn" align="right"><img
								style="cursor: pointer; margin-top: 10px;"
								onclick="javascript: templateClickToChatClickedGuest(this, event);"
								alt="Click for Assistance" title="Click for Assistance"
								src="${pageContext.request.contextPath}/theme/themeV3/themeImages/chatIcon136.png"
								hspace="0" vspace="5"></td>
						</tr>
					</TBODY>
				</TABLE></td>
		</tr>
	</table>
</div>
<div id="loginHideDiv" class="<%=notLoggedinDivClass%>">
	<table width="100%" border="0" class="loginFormTable">
		<tr>
			<td nowrap align="right">
				<TABLE class="loginFormTable">
					<TBODY>
						<TR>
							<TD class="loginFormColumn" nowrap><span
								class="loginNormalPrint">Hello, <%=loginInfo%>&nbsp;&nbsp;[&nbsp;<a
									href="<%=jScriptNoOp%>"
									onclick="javascript:document.location='<%=logOut%>';_unloadPage(this, event);">Logout</a>&nbsp;]</span>
							</TD>
						</TR>
					</TBODY>
				</TABLE></TD>
		</tr>
		<tr>
			<td class="loginFormColumn" align="right"><span
				class="loginfineprint">Account #:&nbsp;<%=accountNum%>&nbsp;<%=changeAccountLink%></span>
			</td>
		</tr>
		<tr>
			<td class="loginFormColumn" align="right"><div
					id="chatIconRequestDiv">
					<img onclick="javascript: templateClickToChatClicked(this, event);"
						style="cursor: pointer; margin-top: 10px;"
						alt="Click for Assistance" title="Click for Assistance"
						src="${pageContext.request.contextPath}/theme/themeV3/themeImages/chatIcon136.png"
						hspace="0" vspace="5">
				</div>
			</td>
		</tr>
	</table>
</div>
<div id="templateChatWindowDiv" style="display: none;">
	<div id="templateChatTopDiv">
		<span onclick="templateClickToChatHide();" style="cursor: pointer;">No
			Thanks<img src="/theme/themeV3/themeImages/close_14x14.png"
			width="14" height="14" border="0"
			style="margin-left: 5px; margin-right: 5px" /> </span>
	</div>
<!-- 	<div id="tempateChatImageDiv">
		<a
			onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
			href=""><img border="0"
			src="https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY" />
		</a>
	</div>
 -->	<div id="templateChatAccountLinksDiv">
		<ul>
			<li>To report a problem with delivery, click <a
				href="/account/cp/delissue.faces"> here</a>.</li>
			<li>To place your delivery on a temporary hold, click <a
				href="/account/vacationholds/holds.faces"> here</a>.</li>
			<li>Forgot your password or login? Click <a
				href="/idpassword/forgotpassword.jsp"> here</a>.
			<li>To contact us <a href="/feedback/feedback.jsp"> click here</a>.</li>				
<!-- 			<li>To cancel your subscription, please call 1-800-872-0001 and -->
<!-- 				select option 6.</li> -->
		</ul>
	</div>

	<div id="templateChatLinksDiv">
		<ul>

			<li><a
				onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
				href="">Click to Chat</a>
			</li>
		</ul>
	</div>
	<div id="templateChatBottomDiv">
		<span class="outputText">Or, call us at:<br> <b>Call
				1-877-713-6241</b><br>(M-F 8:00 a.m. to 7 p.m. E.T.)</span>
	</div>
</div>
<div id="templateChatWindowGuestDiv" style="display: none;">
	<div id="templateChatTopDiv">
		<span onclick="templateClickToChatHideGuest();"
			style="cursor: pointer;">No Thanks<img
			src="/theme/themeV3/themeImages/close_14x14.png" width="14"
			height="14" border="0" style="margin-left: 5px; margin-right: 5px" />
		</span>
	</div>
<!-- 	<div id="tempateChatImageDiv">
		<a
			onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
			href=""><img border="0"
			src="https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY" />
		</a>
	</div>
 -->	<div id="templateChatAccountLinksDiv">
		<ul>
			<li>To manage your account online - including reporting a
				problem with delivery or placing a temporary hold on your account -
				log in by entering your e-mail address and password. Click <a
				href="/login/auth.faces"> here</a> to access the login page now.</li>
			<li>Forgot your password or login? <a
				href="/idpassword/forgotpassword.jsp"> click here</a>.</li>
			<li>To contact us <a href="/feedback/feedback.jsp"> click here</a>.</li>
			<!-- 			<li>To cancel your subscription, please call 1-800-872-0001 and -->
			<!-- 				select option 6.</li> -->
		</ul>

	</div>

	<div id="templateChatLinksDiv">
		<ul>
			<li><a
				onclick="javascript:window.open('https://chat.gannett.com/HtmlChat.jsp?PublicationBasename=usatoday&PublicationId=USAT&PublicationName=USA+TODAY','wochat','width=484,height=361');templateClickToChatHide();return false;"
				href="">Click to Chat</a>
			</li>
		</ul>
	</div>
	<div id="templateChatBottomDiv">
		<span class="outputText">Or, call us at:<br> <b>Call
				1-877-713-6241</b><br>(M-F 8:00 a.m. to 7 p.m. E.T.)</span>
	</div>
</div>
<%-- /tpl:insert --%>			
				<%-- /tpl:put --%>
			</div>
		</DIV>
		<!-- end header -->
		<DIV id="content">
			<DIV id="whitebox">
				<%-- tpl:put name="bodyAreaMainWhiteBox" --%>
<DIV id=nav>
	<DIV id=leftNavList>
		<UL>
		  <LI><A href="/products.html">USA TODAY Products</A></LI>
		  <LI><A href="<%=subcribeJS%>">Subscribe</A></LI>
		  <LI><A href="/account/accounthistory/accountSummary.faces">Manage account</A></LI>
		  <LI><A href="<%=subcribeIntJS%>">Outside the USA</A></LI>
		  <LI><A href="<%=backIssueJS%>">Order back issues</A></LI>
		  <%=dynamicNavigation %>
		</UL>
	</DIV>
	<DIV id="navPromo">
		<%-- tpl:put name="navigationArea" --%>
			
	    <%-- /tpl:put --%>
	</div>
</DIV>
	<DIV id=pageContent>
		<%-- tpl:put name="MainBodyArea" --%>
<H1>Online Account Set Up</H1>
<FORM method="POST" action="/FirstTime" name="firsttimeForm" id="firsttimeForm" onsubmit="return FrontPage_Form1_Validator(this)" onreset="<%=accountPhoneReset%>" enctype="application/x-www-form-urlencoded">
<TABLE border="0" width="100%" cellspacing="0" cellpadding="0">
<TR>

		<TD colspan="3"><a href="https://usatoday.com">Maintain profile</a> <IMG
											border="0" src="/images/shim.gif" width="1" height="1"
											vspace="5"></TD>
	</TR>
<TR>

		<TD colspan="3"><FONT face="Arial" size="2">
			 In order to manage your account online, please complete the two steps below.  <BR></FONT></TD>
	</TR>
	<TR>
		<TD><IMG border="0" src="/images/shim.gif" width="1"
									height="1" vspace="5"></TD>
		<TD></TD>
	</TR>
	<TR>
		<TD> <SPAN style="color: #4268b4; font-family: Arial; font-size: 10pt"><B>&nbsp;&nbsp;:: Step 1* ::</B></SPAN>
		</TD>
		<TD><INPUT type="hidden" name="ERRORMSG" value="<%=errorMsg%>"> <INPUT type="hidden" name="ERRORID" value="<%=errorID%>"> <FONT face="Arial"><B></B></FONT></TD>
	</TR>
</TABLE>
<TABLE border="0" width="100%" cellspacing="0" cellpadding="0">
	<TR>
		<TD></TD>
		<TD align="right" colspan="2"></TD>
		<TD ></TD>
		<TD colspan="2"></TD>
	</TR>
	<TR>
		<TD colspan="6" align="left" width="100%">
			<table style="margin-left: 3px;">
				<tr>
					<td align="right">Publication:
					</td>
					<td align="left"><select name="PUB_NAMES"><option value="UT">USA TODAY - Print Edition</option>
															<option value="EE">USA TODAY - Electronic Edition</option>
															<option value="BW">SPORTS WEEKLY</option>
													</select>
													</td>
				</tr>
				<tr>
					<td align="right">Account Number:
					</td>
					<td align="left"><INPUT type="number" maxlength="9" name="ACCNT_ID" size="10" value="<%=accountNum%>" onfocus="account_phone_clear(firsttimeForm);" onchange="account_phone_clear(firsttimeForm);func_2(this, event);" tabindex="1"></td>
				</tr>
				<tr>
					<td>
					</td>
					<td align="left"><A href="javascript:;" onclick="javascript:window.open('/faq/<%=faq%>#section28','FAQ', 'width=800,height=400,menubar=no,titlebar=yes,scrollbars=yes,status=no,location=no,directories=no');"><FONT face="Arial" size="1">Where can I find this?</FONT></A></td>
				</tr>
				<tr>
					<td>
					</td>
					<td align="left"><br><B>OR</B><br><br></td>
				</tr>
				<tr>
					<td align="right">Phone Number:
					</td>
					<td align="left" nowrap="nowrap"><INPUT type="number" maxlength="3" name="PHONE_NUM1" size="3" value="<%=phoneNum1%>" onkeyup="return autoTab(this, 3, event)" onfocus="account_phone_clear(firsttimeForm);" onchange="account_phone_clear(firsttimeForm);" tabindex="2">&nbsp;<INPUT type="number" maxlength="3" name="PHONE_NUM2" size="3" value="<%=phoneNum2%>" onkeyup="return autoTab(this, 3, event)" onfocus="account_phone_clear(firsttimeForm);" onchange="account_phone_clear(firsttimeForm);" tabindex="2"> <INPUT type="number" maxlength="4" name="PHONE_NUM3" size="4" value="<%=phoneNum3%>" onkeyup="return autoTab(this, 4, event)" onfocus="account_phone_clear(firsttimeForm);" onchange="account_phone_clear(firsttimeForm);" tabindex="2"></td>
				</tr>
			</table>
		</TD>
	</TR>
	<TR>
		<td></td>
		<TD colspan="5" align="center">
			<br>
			<HR width="95%"><br>
		</TD>
	</TR>
	<TR>
		<TD></TD>
		<TD align="left" colspan="5"><SPAN style="color: #4268b4; font-family: Arial; font-size: 10pt"><B>:: Step 2* ::</B></SPAN></TD>
	</TR>
</TABLE>
<TABLE border="0" cellspacing="0" cellpadding="0">
	<TR>
		<TD>&nbsp;</TD>
		<TD align="left" colspan="2"></TD>
		<TD ></TD>
		<TD ></TD>
	</TR>
	<tr>
		<td colspan="5" align="left" width="100%">
			<table style="margin-left: 3px;">
				<tr>
					<td align="right">E-mail Address:
					</td>
					<td align="left"><INPUT type="email" name="EMAIL" size="20" maxlength="60" value="<%=emailAddress%>" tabindex="3">
					</td>
				</tr>
				<tr>
					<td align="right">Delivery	Zipcode:
					</td>
					<td align="left"><INPUT type="number" name="ZIPCODE" maxlength="5" size="20" value="<%=zipCode%>" tabindex="4">
					</td>
				</tr>
				<tr>
					<td align="right">Password:
					</td>
					<td align="left"><INPUT type="password" name="PASSWORD" maxlength="30" size="30" onkeyup="maxPwdChars(this, 30)" value="<%=password%>" tabindex="5" onchange="return func_1(this, event);"><FONT face="Arial" size="2">&nbsp;(Min
								5, Max 30 Alphanumeric)</FONT>
					</td>
				</tr>
				<tr>
					<td align="right">Retype Password:
					</td>
					<td align="left"><INPUT type="password" name="VERIFY_PASSWORD" maxlength="30" size="30" onkeyup="maxConfirmPwdChars(this, 30)" value="<%=password%>" tabindex="6">
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
	<td>
	</td>
	<td>&nbsp;
	</td>
	</tr>
	<TR>
		<TD></TD>
		<TD colspan="4" nowrap="nowrap">	</TD>
	</TR>
	<TR>
		<TD align="center" valign="top" nowrap="nowrap" >
		<P style="margin-top: 15"><FONT size="2"></FONT></P>
		</TD>
		<TD align="center" valign="top" nowrap="nowrap"><INPUT type="submit" value="<%=proceedButton%>" name="B1"></TD>
	</TR>
	<TR><TD height="27"></TD>
		<TD colspan="4" height="27"><FONT color="#0000FF">*</FONT> <FONT face="Arial" size="2">Required</FONT></TD>
	</TR>
</TABLE>
<INPUT type="hidden" name="STEP" value="1">
</FORM>
<%-- /tpl:put --%>
	</DIV>
	<p>&nbsp;</p><%-- /tpl:put --%>
				<DIV id="clear"></DIV>
				<DIV id="bluebar">
					<a href="/feedback/feedback.jsp"
						style="color: white; font-weight: bold; text-decoration: none; font-size: 14px"
						title="Contact Us">Contact Us</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;1-800-872-0001&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a
						href="/faq/utfaq.jsp"
						style="color: white; font-weight: bold; text-decoration: none; font-size: 14px"
						title="Frequently Asked Questions">FAQ</a>
				</DIV>
				<%-- tpl:put name="footerMainArea" --%>
		<%-- tpl:put name="footerArea_1" --%><!-- Insert Footer Ad Here --><%-- /tpl:put --%>
		<%=trackingBug.getOmnitureHTML() %>
	<%-- /tpl:put --%>
				<DIV id=graybar>
					Copyright
					<script>
						document.write(getCurrentYear());
					</script>
					USA TODAY, a division of <a href="http://www.gannett.com">Gannett
						Co. Inc.</a> <a href="/privacy/privacy.htm">Privacy
						Notice/California Privacy Notice</a>. <a
						href="/privacy/privacy.htm#adchoice">Ad Choices</a>.<br> By
					using this service, you accept our <a href="/service/service.jsp">Terms
						of Service</a>.
				</DIV>
				<DIV id=copyright>
					<P>Offers available to new subscribers only. Home delivery not
						available in some areas. Not valid with any other offer.
						Prepayment required. After the promotional period ends, the
						subscription will continue and be billed monthly at the then
						regular rate, less any applicable credits, unless USA TODAY is
						otherwise notified. Applicable taxes may not be included in the
						rate. If at any time you decide to cancel your subscription, you
						may contact customer service at 1-800-872-0001 and the full amount
						of any balance over $2.00 will be returned.</P>
				</DIV>
				<!--END OF COPYWRITE-->
				<DIV id=footer></DIV>
			</DIV>
			<!--END OF WHITEBOX-->
		</DIV>
		<!--END OF CONTENT-->
	</DIV>
</BODY>
</HTML><%-- /tpl:insert --%>
<%-- /tpl:insert --%>