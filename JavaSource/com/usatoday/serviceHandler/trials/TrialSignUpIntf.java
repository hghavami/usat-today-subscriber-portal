package com.usatoday.serviceHandler.trials;

public interface TrialSignUpIntf {

	public String addTrialSubscriber(String partnerID, String partnerIP, String clubNumber, String firstName, String lastName,
			String address1, String apartmenttSuite, String city, String state, String zip, String phone, String emailAddress,
			String password, String hash) throws Exception;

	public boolean isLastAddSuccessful();

}
