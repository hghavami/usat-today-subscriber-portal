package com.usatoday.serviceHandler.trials;

import org.joda.time.DateTime;

import com.usatoday.business.interfaces.partners.TrialPartnerIntf;
import com.usatoday.businessObjects.customer.ContactBO;
import com.usatoday.businessObjects.customer.CustomerBO;
import com.usatoday.businessObjects.customer.TrialCustomerBO;
import com.usatoday.businessObjects.customer.TrialInstanceBO;
import com.usatoday.businessObjects.customer.UIAddressBO;
import com.usatoday.businessObjects.partners.TrialPartnerBO;
import com.usatoday.businessObjects.util.MD5OneWayHash;

public class TrialSignUp implements TrialSignUpIntf {

	private boolean lastAddSuccessful = false;

	@Override
	public String addTrialSubscriber(String partnerID, String partnerIP, String clubNumber, String firstName, String lastName,
			String address1, String apartmentSuite, String city, String state, String zip, String phone, String emailAddress,
			String password, String hash) throws Exception {

		StringBuilder returnMessage = new StringBuilder();

		this.lastAddSuccessful = false;

		try {
			TrialPartnerIntf partner = TrialPartnerBO.fetchPartner(partnerID, true);

			if (partner == null) {
				throw new Exception("Invalid Partner ID: " + partnerID);
			}
			if (!partner.getIsOfferCurrentlyRunning()) {
				throw new Exception("Invalid Partner ID: " + partnerID + ". Program ended on "
						+ partner.getEndDate().toString("M/d/yyyy"));
			}

			String hashPassword = "";

			if (password == null || password.trim().length() == 0) {
				// generate password
				password = CustomerBO.generateNewPassword();
			} else {
				// set hash password for hash computation
				hashPassword = password;
			}

			String partnerSecretKey = partner.getAPISecretHashKey();
			if (partnerSecretKey != null && partnerSecretKey.length() > 0) {
				// requires a hash
				if (hash == null) {
					throw new Exception("Hash Does Not Compute");
				} else {
					StringBuilder hashValue = new StringBuilder(partnerID.trim());
					hashValue.append(partnerIP.trim());
					if (clubNumber != null && clubNumber.length() > 0) {
						hashValue.append(clubNumber);
					}
					if (firstName != null && firstName.length() > 0) {
						hashValue.append(firstName);
					}
					if (lastName != null && lastName.length() > 0) {
						hashValue.append(lastName);
					}
					if (address1 != null && address1.length() > 0) {
						hashValue.append(address1);
					}
					if (apartmentSuite != null && apartmentSuite.length() > 0) {
						hashValue.append(apartmentSuite);
					}
					if (city != null && city.length() > 0) {
						hashValue.append(city);
					}
					if (state != null && state.length() > 0) {
						hashValue.append(state);
					}
					if (zip != null && zip.length() > 0) {
						hashValue.append(zip);
					}
					if (phone != null && phone.length() > 0) {
						hashValue.append(phone);
					}
					if (emailAddress != null && emailAddress.length() > 0) {
						hashValue.append(emailAddress);
					}
					hashValue.append(hashPassword);
					hashValue.append(partnerSecretKey);

					String myHash = MD5OneWayHash.getMD5Hash(hashValue.toString());

					if (!myHash.equalsIgnoreCase(hash)) {
						throw new Exception("Hash Does Not Compute");
					}

				}
			}

			TrialInstanceBO newTrialCustomer = new TrialInstanceBO();

			newTrialCustomer.setClubNumber(clubNumber);
			newTrialCustomer.setNcsRepID("Web_API");

			ContactBO contact = null;
			contact = new ContactBO();
			contact.setBusinessPhone("");

			if (phone != null && phone.length() > 10) {
				try {
					phone = phone.trim();
					StringBuilder newPhone = new StringBuilder();

					char[] phoneNumArray = phone.toCharArray();
					for (char c : phoneNumArray) {
						if (Character.isDigit(c)) {
							newPhone.append(c);
						}
					}

					phone = newPhone.toString();
				} catch (Exception e) {
					; // ignore
				}
				if (phone.length() > 10) {
					// unsupported phone number so clear it
					phone = "";
				}
			}
			contact.setHomePhone(phone);

			contact.setEmailAddress(emailAddress);
			contact.setFirstName(firstName);
			contact.setLastName(lastName);
			if (password == null || password.trim().length() == 0) {
				password = CustomerBO.generateNewPassword();
			}
			contact.setPassword(password.trim());

			UIAddressBO address = new UIAddressBO();
			address.setAddress1(address1);
			address.setAddress2("");
			address.setAptSuite(apartmentSuite);
			address.setCity(city);
			address.setState(state);
			address.setZip(zip);
			address.setValidated(true);

			contact.setUIAddress(address);

			newTrialCustomer.setContactInformation(contact);

			newTrialCustomer.setPubCode(partner.getPubCode());
			newTrialCustomer.setKeyCode(partner.getKeyCode());

			newTrialCustomer.setPartner(partner);

			DateTime start = new DateTime();
			DateTime end = new DateTime();
			end = end.plusDays(partner.getDurationInDays());

			newTrialCustomer.setStartDate(start);
			newTrialCustomer.setEndDate(end);

			// validate instance. will throw exception if not valid
			TrialCustomerBO.validateNewTrialInstance(newTrialCustomer);

			newTrialCustomer.setClientIP(partnerIP);

			if (partner.getIsSendConfirmationOnCreation()) {
				newTrialCustomer.setConfirmationEmailSent("Y");
			} else {
				newTrialCustomer.setConfirmationEmailSent("N");
			}

			newTrialCustomer.save();

			// if sending confirmation emails immediately
			if (partner.getIsSendConfirmationOnCreation()) {
				boolean emailSent = newTrialCustomer.sendConfirmationEmail();
				if (!emailSent) {
					// update customer email sent status to N instead of Y
					newTrialCustomer.setConfirmationEmailSent("N");
					newTrialCustomer.save();

					throw new Exception("Customer Added but Confirmation Email Send Failed.");
				}
			}

			returnMessage.append("Y");
			this.lastAddSuccessful = true;

		} catch (Exception e) {
			returnMessage.append("USA TODAY Trial - Failed to process new start: " + e.getMessage());
		}

		return returnMessage.toString();
	}

	@Override
	public boolean isLastAddSuccessful() {
		return lastAddSuccessful;
	}

	public void setLastAddSuccessful(boolean lastAddSuccessful) {
		this.lastAddSuccessful = lastAddSuccessful;
	}

}
