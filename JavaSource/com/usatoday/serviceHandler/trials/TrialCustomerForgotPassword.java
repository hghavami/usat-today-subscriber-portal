package com.usatoday.serviceHandler.trials;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.customer.TrialCustomerIntf;
import com.usatoday.business.interfaces.products.SubscriptionProductIntf;
import com.usatoday.businessObjects.products.SubscriptionProductBO;
import com.usatoday.esub.common.SmtpMailSender;
import com.usatoday.esub.common.UTCommon;
import com.usatoday.util.constants.UsaTodayConstants;

public class TrialCustomerForgotPassword {

	public static void sendPassword(TrialCustomerIntf cust) throws UsatException {

		if (cust != null) {
			String password = cust.getPassword();
			String email = cust.getEmailAddress();

			// Build messsage text
			String prodName = null;
			// String phone = "";
			String pubCode = null;
			SubscriptionProductIntf prod = null;
			SubscriptionProductIntf brandingProd = null;
			prod = SubscriptionProductBO.getSubscriptionProduct(cust.getCurrentInstance().getPubCode());
			brandingProd = SubscriptionProductBO.getSubscriptionProduct(prod.getBrandingPubCode());

			prodName = brandingProd.getName();
			pubCode = brandingProd.getProductCode();

			StringBuilder forgotPasswordEmailText = new StringBuilder(prodName);
			forgotPasswordEmailText.append(" Trial Subscription Forgot Password Request\n\n");
			forgotPasswordEmailText.append("\n\nOur records indicate your email address and password are as follows: \n\n");
			forgotPasswordEmailText.append("Email Address: ").append(email);
			forgotPasswordEmailText.append("\nPassword: ").append(password);
			forgotPasswordEmailText.append("\n\n");
			forgotPasswordEmailText.append("Login at https://service.usatoday.com/login/trialLogin.faces \n\n");
			String footer = "";
			if (pubCode == null || pubCode.equalsIgnoreCase(UsaTodayConstants.UT_PUBCODE)) {
				footer = "USA TODAY, 7950 Jones Branch Dr, McLean, Virginia, 22108";
			} else {
				footer = "USA TODAY Sports Weekly, 7950 Jones Branch Dr, McLean, Virginia, 22108";
			}

			forgotPasswordEmailText.append(footer);

			String emailSubject = UTCommon.EMAIL_SUBJECT;
			if (pubCode.equalsIgnoreCase(UTCommon.BWPUBCODE)) {
				emailSubject = emailSubject.replaceAll("USA TODAY", "Sports Weekly");
			}

			String message = forgotPasswordEmailText.toString();

			if ((email == null) || (emailSubject == null) || (message == null)) {
				throw new UsatException("Failed to send password. null data.");
			}

			SmtpMailSender mailSender = new SmtpMailSender();
			mailSender.setMailServerHost(UTCommon.SMTP_SERVER);
			mailSender.setMessageSubject(emailSubject);
			mailSender.setMessageText(message);

			if (brandingProd.getProductCode() == null || brandingProd.getProductCode().equals(UTCommon.UTPUBCODE)) {
				mailSender.setSender(UTCommon.UTFROM_EMAIL);
			} else {
				mailSender.setSender(UTCommon.BWFROM_EMAIL);
			}
			mailSender.addTORecipient(email);
			try {
				mailSender.sendMessage();
			} catch (Exception e) {

				throw new UsatException(e.getMessage());
			}

		} else {
			throw new UsatException("No email address.");
		}

	}
}
