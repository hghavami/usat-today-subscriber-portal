package com.usatoday.esub.account;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.usatoday.business.interfaces.customer.CustomerIntf;
import com.usatoday.esub.common.UTCommon;
import com.usatoday.esub.ncs.handlers.CustomerHandler;

public class Authenticate implements Filter {
	/*
	 * (non-Java-doc)
	 * 
	 * @see java.lang.Object#Object()
	 */
	public Authenticate() {
		super();
	}

	/*
	 * (non-Java-doc)
	 * 
	 * @see javax.servlet.Filter#init(FilterConfig arg0)
	 */
	public void init(FilterConfig arg0) throws ServletException {

	}

	/*
	 * (non-Java-doc)
	 * 
	 * @see javax.servlet.Filter#doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain arg2)
	 */

	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {

		CustomerIntf customer = null;
		CustomerHandler ch = null;
		String accountPath = "";
		HttpServletRequest request = (HttpServletRequest) req;

		// make sure a session is created.
		HttpSession session = request.getSession();
		HttpServletResponse response = (HttpServletResponse) resp;

		try {
			customer = (CustomerIntf) session.getAttribute(UTCommon.SESSION_CUSTOMER_INFO);

			ch = (CustomerHandler) session.getAttribute("customerHandler");

		} catch (Exception e) {

		}
		String reqURI = request.getRequestURI().toString();
		accountPath = (String) session.getAttribute(UTCommon.SESSION_ACCOUNT_PATH);

		// Check if customer is already logged in
		if (customer != null && ch != null) {

			if (ch.isAuthenticated()) {
				if (customer.getCurrentAccount() == null && customer.getCurrentEmailRecord() != null) {
					// signed in but only has access to eEdtion
					// forward to eEdition Access Page
					request.getRequestDispatcher("/eAccount/eEditionNewAccountIndex.faces").forward(request, response);
				} else {
					if (accountPath == null || accountPath.trim().equals("")) {
						session.setAttribute(UTCommon.SESSION_ACCOUNT_PATH, "/account/accounthistory/accountSummary.faces");
					}
					chain.doFilter(req, resp);
				}
				return;
			} else {
				// not properly logged in
				response.sendRedirect("/login/auth.faces");
				return;
			}
		} else {

			// check for remember me cookie
			// check for cookie

			customer = UTCommon.authenticateViaRememberMeCookie(request, response);

			if (customer != null) {
				if (customer.getNumberOfAccounts() > 1 && customer.getCurrentAccount() == null) {
					// select Account page
					request.getRequestDispatcher("/account/select_accnt.jsp").forward(request, response);
				} else {
					// let them move on
					chain.doFilter(req, resp);
				}
				return;
			} else { // no auth cookie

				// Store for retrieval by JSPs.
				if (accountPath == null || accountPath.trim().equals("")) {
					session.setAttribute(UTCommon.SESSION_ACCOUNT_PATH, reqURI);
				}
				// sew auth
				response.sendRedirect("/login/auth.faces");

				return;
			}
		}
	}

	/*
	 * (non-Java-doc)
	 * 
	 * @see javax.servlet.Filter#destroy()
	 */
	public void destroy() {

	}

}