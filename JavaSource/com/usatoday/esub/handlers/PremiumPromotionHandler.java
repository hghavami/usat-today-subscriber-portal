package com.usatoday.esub.handlers;

import com.usatoday.businessObjects.products.promotions.premiums.PremiumPromotion;
import com.usatoday.esub.subscriptions.MPFOnePageHelperBean;

public class PremiumPromotionHandler {

	private PremiumPromotion promotion = null;
	private MPFOnePageHelperBean mpfHelper = null;
	private String termAsString = null;

	public MPFOnePageHelperBean getMpfHelper() {
		return mpfHelper;
	}

	public void setMpfHelper(MPFOnePageHelperBean mpfHelper) {
		this.mpfHelper = mpfHelper;
	}

	public PremiumPromotion getPromotion() {
		return promotion;
	}

	public void setPromotion(PremiumPromotion promotion) {
		this.promotion = promotion;
	}

	public boolean getOrderHasPremiumPromotion() {
		if (this.promotion != null && this.mpfHelper != null) {
			return true;
		}
		return false;
	}

	public void reset() {
		this.promotion = null;
		this.mpfHelper = null;
	}

	public String getPromotionDescription() {
		String description = "";
		try {
			if (this.promotion != null) {
				description = this.promotion.getDescriptionText();
			}
		} catch (Exception e) {
			description = "";
		}
		return description;
	}

	public String getTermAsString() {
		return termAsString;
	}

	public void setTermAsString(String termAsString) {
		this.termAsString = termAsString;
	}
}
