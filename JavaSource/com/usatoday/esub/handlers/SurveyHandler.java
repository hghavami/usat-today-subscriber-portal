package com.usatoday.esub.handlers;

import com.usatoday.business.interfaces.customer.CustomerIntf;
import com.usatoday.businessObjects.customer.CustomerBO;

public class SurveyHandler {

	private String accountNum = null;
	private String pubCode = com.usatoday.util.constants.UsaTodayConstants.UT_PUBCODE;
	private CustomerIntf customer = null;
	private String emailAddress = null;
	private String question1Answer = null;
	private String question2Answer = null;
	private String question3Answer = null;
	private String question4Answer = null;

	public String getQuestion4Answer() {
		return question4Answer;
	}

	public void setQuestion4Answer(String question4Answer) {
		this.question4Answer = question4Answer;
	}

	private String comment1 = null;

	public String getComment1() {
		return comment1;
	}

	public void setComment1(String comment1) {
		this.comment1 = comment1;
	}

	public String getAccountNum() {
		if (getCustomer() != null) {
			accountNum = getCustomer().getCurrentAccount().getAccountNumber();
		}
		return accountNum;
	}

	public CustomerIntf getCustomer() {
		try {
			customer = CustomerBO.loginCustomerEmailAccountPub(emailAddress, accountNum, pubCode);
		} catch (Exception e) {

		}
		return customer;
	}

	public String getEmailAddress() {
		if (getCustomer() != null) {
			emailAddress = getCustomer().getCurrentEmailRecord().getEmailAddress();
		}
		return emailAddress;
	}

	public String getQuestion1Answer() {
		return question1Answer;
	}

	public void setAccountNum(String accountNum) {
		this.accountNum = accountNum;
	}

	public void setCustomer(CustomerIntf customer) {
		this.customer = customer;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public void setQuestion1Answer(String question1Answer) {
		this.question1Answer = question1Answer;
	}

	public String getQuestion2Answer() {
		return question2Answer;
	}

	public void setQuestion2Answer(String question2Answer) {
		this.question2Answer = question2Answer;
	}

	public String getQuestion3Answer() {
		return question3Answer;
	}

	public void setQuestion3Answer(String question3Answer) {
		this.question3Answer = question3Answer;
	}

}
