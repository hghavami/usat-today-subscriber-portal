package com.usatoday.esub.handlers;

public class ReferHandler {
	private String subFirstName = null;
	private String subLastName = null;
	private String subEmailAddress = null;
	private String subAccount = null;

	private String refFirstName = null;
	private String refLastName = null;
	private String refEmailAddress = null;

	public ReferHandler() {
		super();
	}

	public String getSubFirstName() {
		return subFirstName;
	}

	public void setSubFirstName(String subFirstName) {
		this.subFirstName = subFirstName;
	}

	public String getSubLastName() {
		return subLastName;
	}

	public void setSubLastName(String subLastName) {
		this.subLastName = subLastName;
	}

	public String getSubEmailAddress() {
		return subEmailAddress;
	}

	public void setSubEmailAddress(String subEmailAddress) {
		this.subEmailAddress = subEmailAddress;
	}

	public String getSubAccount() {
		return subAccount;
	}

	public void setSubAccount(String subAccount) {
		this.subAccount = subAccount;
	}

	public String getRefFirstName() {
		return refFirstName;
	}

	public void setRefFirstName(String refFirstName) {
		this.refFirstName = refFirstName;
	}

	public String getRefLastName() {
		return refLastName;
	}

	public void setRefLastName(String refLastName) {
		this.refLastName = refLastName;
	}

	public String getRefEmailAddress() {
		return refEmailAddress;
	}

	public void setRefEmailAddress(String refEmailAddress) {
		this.refEmailAddress = refEmailAddress;
	}

}
