package com.usatoday.esub.handlers;

public class GiftCardHandler {
	private String subFirstName = null;
	private String subLastName = null;
	private String subEmailAddress = null;
	private String subAccount = null;
	private String giftMessage = null;
	private String refFirstName = null;
	private String refLastName = null;
	private String refEmailAddress = null;
	private String term = null;
	private String pub = null;
	private String startDate = null;

	public GiftCardHandler() {
		super();
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getPub() {
		return pub;
	}

	public void setPub(String pub) {
		this.pub = pub;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public String getSubFirstName() {
		return subFirstName;
	}

	public void setSubFirstName(String subFirstName) {
		this.subFirstName = subFirstName;
	}

	public String getGiftMessage() {
		return giftMessage;
	}

	public void setGiftMessage(String giftMessage) {
		this.giftMessage = giftMessage;
	}

	public String getSubLastName() {
		return subLastName;
	}

	public void setSubLastName(String subLastName) {
		this.subLastName = subLastName;
	}

	public String getSubEmailAddress() {
		return subEmailAddress;
	}

	public void setSubEmailAddress(String subEmailAddress) {
		this.subEmailAddress = subEmailAddress;
	}

	public String getSubAccount() {
		return subAccount;
	}

	public void setSubAccount(String subAccount) {
		this.subAccount = subAccount;
	}

	public String getRefFirstName() {
		return refFirstName;
	}

	public void setRefFirstName(String refFirstName) {
		this.refFirstName = refFirstName;
	}

	public String getRefLastName() {
		return refLastName;
	}

	public void setRefLastName(String refLastName) {
		this.refLastName = refLastName;
	}

	public String getRefEmailAddress() {
		return refEmailAddress;
	}

	public void setRefEmailAddress(String refEmailAddress) {
		this.refEmailAddress = refEmailAddress;
	}

}
