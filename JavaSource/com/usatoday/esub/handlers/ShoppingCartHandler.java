package com.usatoday.esub.handlers;

import java.util.Date;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.joda.time.DateTime;

import com.usatoday.business.interfaces.products.SubscriptionProductIntf;
import com.usatoday.business.interfaces.products.SubscriptionTermsIntf;
import com.usatoday.business.interfaces.products.promotions.JavaScriptPromotionIntf;
import com.usatoday.business.interfaces.shopping.OrderIntf;
import com.usatoday.business.interfaces.shopping.OrderItemIntf;
import com.usatoday.business.interfaces.shopping.ShoppingCartIntf;
import com.usatoday.business.interfaces.shopping.SubscriptionOrderItemIntf;
import com.usatoday.business.interfaces.shopping.payment.CreditCardPaymentMethodIntf;
import com.usatoday.businessObjects.products.SubscriptionProductBO;
import com.usatoday.businessObjects.products.promotions.PromotionSet;
import com.usatoday.businessObjects.products.utils.ElectronicEditionFulfillmentHelper;
import com.usatoday.businessObjects.shopping.ShoppingCart;
import com.usatoday.businessObjects.shopping.SubscriptionOrderItemBO;
import com.usatoday.esub.common.UTCommon;
import com.usatoday.util.MobileDeviceDetectionUtil;
import com.usatoday.util.constants.UsaTodayConstants;

public class ShoppingCartHandler {

	private ShoppingCartIntf cart = null;
	private OrderIntf lastOrder = null;
	private boolean isCheckoutStarted = false;

	public boolean isCheckoutStarted() {
		return isCheckoutStarted;
	}

	public void setCheckoutStarted(boolean isCheckoutStarted) {
		this.isCheckoutStarted = isCheckoutStarted;
	}

	private boolean isZeroDollarAmount = false;
	private boolean isNotZeroDollar = true;
	private boolean isListServicesEEKeycode = false;
	private boolean isListServicesUTKeycode = false;

	public ShoppingCartHandler() {
		super();
		// initialize the cart. The order will not be set until an order is
		// placed and the cart is cleared
		this.cart = new ShoppingCart();
	}

	public boolean getIsZeroDollarAmount() {
		return this.isZeroDollarAmount;
	}

	public void setIsZeroDollarAmount(boolean isZeroDollarAmount) {
		this.isZeroDollarAmount = isZeroDollarAmount;
	}

	public void setIsNotZeroDollar(boolean isNotZeroDollar) {
		this.isNotZeroDollar = isNotZeroDollar;
	}

	public boolean getIsNotZeroDollar() {
		return this.isNotZeroDollar;
	}

	public ShoppingCartIntf getCart() {
		return cart;
	}

	public void setCart(ShoppingCartIntf cart) {
		this.cart = cart;
	}

	public OrderIntf getLastOrder() {
		return lastOrder;
	}

	public void setLastOrder(OrderIntf lastOrder) {
		this.lastOrder = lastOrder;
	}

	public String getNewDefaultUTeEditionLink() {
		String link = "#";

		HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();

		if (this.lastOrder != null) {
			for (OrderItemIntf item : this.lastOrder.getOrderedItems()) {
				if (item.getProduct().isElectronicDelivery()
						|| (item.getProduct().getProductCode().equalsIgnoreCase(UsaTodayConstants.UT_PUBCODE) && UsaTodayConstants.UT_EE_COMPANION_ACTIVE)) {
					try {
						boolean useAlternate = MobileDeviceDetectionUtil.isMobileDevice(req);

						String email = this.lastOrder.getDeliveryContact().getEmailAddress();
						SubscriptionProductIntf p = SubscriptionProductBO
								.getSubscriptionProduct(UsaTodayConstants.DEFAULT_UT_EE_PUBCODE);
						link = ElectronicEditionFulfillmentHelper.buildOliveFulfillmentURL(p, email, null, null, useAlternate);
					} catch (Exception e) {
						e.printStackTrace();
					}
					break;
				}
			}
		}
		return link;
	}

	public String getNewUTeEditionLink() {
		String link = "#";
		if (this.lastOrder != null) {
			for (OrderItemIntf item : this.lastOrder.getOrderedItems()) {
				if (item.getProduct().isElectronicDelivery()
						|| (item.getProduct().getProductCode().equalsIgnoreCase(UsaTodayConstants.UT_PUBCODE) && UsaTodayConstants.UT_EE_COMPANION_ACTIVE)) {
					try {

						boolean useAlternate = false;
						String email = this.lastOrder.getDeliveryContact().getEmailAddress();
						SubscriptionProductIntf p = SubscriptionProductBO
								.getSubscriptionProduct(UsaTodayConstants.DEFAULT_UT_EE_PUBCODE);
						link = ElectronicEditionFulfillmentHelper.buildOliveFulfillmentURL(p, email, null, null, useAlternate);
						p = SubscriptionProductBO.getSubscriptionProduct(UsaTodayConstants.DEFAULT_UT_EE_PUBCODE);
						link = ElectronicEditionFulfillmentHelper.buildOliveFulfillmentURL(p, email, null, null, useAlternate);
					} catch (Exception e) {
						e.printStackTrace();
					}
					break;
				}
			}
		}
		return link;
	}

	public String getAlternateUTeEditionLink() {
		String link = "#";

		if (this.lastOrder != null) {
			for (OrderItemIntf item : this.lastOrder.getOrderedItems()) {
				if (item.getProduct().isElectronicDelivery()
						|| (item.getProduct().getProductCode().equalsIgnoreCase(UsaTodayConstants.UT_PUBCODE) && UsaTodayConstants.UT_EE_COMPANION_ACTIVE)) {
					try {
						boolean useAlternate = true;

						String email = this.lastOrder.getDeliveryContact().getEmailAddress();
						SubscriptionProductIntf p = SubscriptionProductBO
								.getSubscriptionProduct(UsaTodayConstants.DEFAULT_UT_EE_PUBCODE);
						link = ElectronicEditionFulfillmentHelper.buildOliveFulfillmentURL(p, email, null, null, useAlternate);
					} catch (Exception e) {
						e.printStackTrace();
					}
					break;
				}
			}
		}
		return link;
	}

	public String getNewUTTutorialLink() {
		String link = "#";
		if (this.lastOrder != null) {
			for (OrderItemIntf item : this.lastOrder.getOrderedItems()) {
				if (item.getProduct().isElectronicDelivery()) {
					try {
						link = item.getProduct().getSupplier().getTutorialURL();
					} catch (Exception e) {
						;
					}
				}
			}
		}
		return link;
	}

	public SubscriptionOrderItemIntf getOrderedSubscriptionItem() {
		SubscriptionOrderItemIntf item = null;
		try {
			if (this.lastOrder != null) {
				for (OrderItemIntf tempItem : this.lastOrder.getOrderedItems()) {
					if (tempItem instanceof SubscriptionOrderItemIntf) {
						item = (SubscriptionOrderItemIntf) tempItem;
						break;
					}
				}
			}
		} catch (Exception e) {

		}
		return item;
	}

	public String getSubTotalWithNoPunctuation() {
		String subTotal = "";
		if (this.lastOrder != null) {
			try {
				double subTotalD = this.lastOrder.getSubTotal() * 100;
				String temp = String.valueOf(subTotalD);
				subTotal = temp.substring(0, temp.indexOf("."));
			} catch (Exception e) {
				; // ignore
			}
		}
		return subTotal;
	}

	public boolean getPaidByCreditCard() {
		boolean paidByCredit = true;
		try {
			if (!this.cart.getPaymentMethod().getPaymentType().requiresPaymentProcessing()) {
				paidByCredit = false;
			}
		} catch (Exception e) {
			;
		}
		return paidByCredit;
	}

	public String getCreditCardExpirationString() {
		String expString = "";
		try {
			if (this.cart.getPaymentMethod().getPaymentType().requiresPaymentProcessing()) {
				CreditCardPaymentMethodIntf ccMethod = (CreditCardPaymentMethodIntf) this.cart.getPaymentMethod();
				expString = ccMethod.getExpirationMonth() + "/" + ccMethod.getExpirationYear();
			}
		} catch (Exception e) {
			;
		}

		return expString;
	}

	public Date getDateOrderPlaced() {
		try {
			return this.lastOrder.getDateTimePlaced().toDate();
		} catch (Exception e) {
			DateTime dt = new DateTime();
			return dt.toDate();
		}

	}

	public Date getStartDateOfSub() {
		try {
			SubscriptionOrderItemIntf item = this.getOrderedSubscriptionItem();

			return item.getStartDateObj().toDate();
		} catch (Exception e) {

		}
		return null;
	}

	public String getSelectedEZPayString() {
		String choseEZPay = "No";
		try {

			for (OrderItemIntf item : this.lastOrder.getOrderedItems()) {
				if (item instanceof SubscriptionOrderItemIntf) {
					SubscriptionOrderItemIntf sItem = (SubscriptionOrderItemIntf) item;
					if (sItem.isChoosingEZPay()) {
						choseEZPay = "Yes";
						break;
					}
				}
			}
		} catch (Exception e) {
		}
		return choseEZPay;
	}

	/**
	 * @param isLinkServicesKeycode
	 *            the isLinkServicesKeycode to set
	 */
	public void setListServicesEEKeycode(boolean isLinkServicesKeycode) {
		this.isListServicesEEKeycode = isLinkServicesKeycode;
	}

	/**
	 * @return the isLinkServicesKeycode
	 */
	public boolean isListServicesEEKeycode() {
		isListServicesEEKeycode = false;
		if (UsaTodayConstants.LIST_SERVICES_EE_KEYCODES.indexOf(this.getOrderedSubscriptionItem().getKeyCode()) > -1) {
			isListServicesEEKeycode = true;
		}
		return isListServicesEEKeycode;
	}

	/**
	 * @param isLinkServicesUTKeycode
	 *            the isLinkServicesUTKeycode to set
	 */
	public void setListServicesUTKeycode(boolean isLinkServicesUTKeycode) {
		this.isListServicesUTKeycode = isLinkServicesUTKeycode;
	}

	/**
	 * @return the isLinkServicesUTKeycode
	 */
	public boolean isListServicesUTKeycode() {
		isListServicesUTKeycode = false;
		if (UsaTodayConstants.LIST_SERVICES_UT_KEYCODES.indexOf(this.getOrderedSubscriptionItem().getKeyCode()) > -1) {
			isListServicesUTKeycode = true;
		}
		return isListServicesUTKeycode;
	}

	public boolean isShowElectronicLinks() {
		boolean showLinks = false;

		if (this.lastOrder != null) {
			SubscriptionOrderItemIntf item = this.getOrderedSubscriptionItem();
			if (item.getProduct().isElectronicDelivery()) {
				showLinks = true;
			} else if (item.getProduct().getBrandingPubCode().equalsIgnoreCase(UsaTodayConstants.UT_PUBCODE)
					&& com.usatoday.util.constants.UsaTodayConstants.UT_EE_COMPANION_ACTIVE) {
				showLinks = true;
			}
		}

		return showLinks;
	}

	public boolean isShowGiftPanel() {
		boolean showPanel = false;

		if (this.lastOrder != null) {
			SubscriptionOrderItemIntf item = this.getOrderedSubscriptionItem();
			if (item.isGiftItem()) {
				showPanel = true;
			}
		}

		return showPanel;
	}

	public boolean isShowSWGiftPanel() {
		boolean showSWPanel = false;

		if (this.lastOrder != null) {
			SubscriptionOrderItemIntf item = this.getOrderedSubscriptionItem();
			if (item.getProduct().getBrandingPubCode().equalsIgnoreCase("BW")) {

				showSWPanel = true;
			}
		}

		return showSWPanel;
	}

	public boolean isShowPremiumPromotionGrid() {
		// HDCONS - 41
		boolean showGrid = false;

		/*
		 * if (this.lastOrder != null) { SubscriptionOrderItemIntf item = this.getOrderedSubscriptionItem(); if
		 * (item.getPromotionalItems() != null) { showGrid = true; } }
		 */
		return showGrid;
	}

	public boolean isShowEEWelcomePanel() {
		boolean showPanel = false;

		if (this.lastOrder != null) {
			SubscriptionOrderItemIntf item = this.getOrderedSubscriptionItem();
			if (item.getProduct().getProductCode().equalsIgnoreCase(UsaTodayConstants.DEFAULT_UT_EE_PUBCODE)) {
				showPanel = true;
			}
		}

		return showPanel;
	}

	@SuppressWarnings("deprecation")
	public String getLastSubscriptionItemTermDuration() {
		String termDes = "";
		String durationType = "";
		if (this.lastOrder != null) {
			SubscriptionOrderItemIntf item = this.getOrderedSubscriptionItem();
			if (item.getSelectedTerm().getDurationType().equals("M")) {
				durationType = " Months";
			} else {
				durationType = " Weeks";
			}
			SubscriptionTermsIntf term = item.getSelectedTerm();
			termDes = term.getDuration();
			termDes = term.getDurationInWeeks();
			int firstIndex = termDes.indexOf('0');
			if (firstIndex > -1) {
				int index = 0;
				boolean trimmed = false;
				while (index < termDes.length() && !trimmed) {
					if (termDes.charAt(index) == '0') {
						index++;
					} else {
						termDes = termDes.substring(index);
						trimmed = true;
					}
				}
			}

		}
		return termDes + durationType;
	}

	/**
	 * 
	 * @return The commission junction tracking pixel.. this method intentionally hard coded due to complexity
	 */
	public String getCommissionJunctionTrackingPixel() {
		String pixel = null;

		if (this.lastOrder != null && UTCommon.COMM_JUNCTION_ACTIVE) {
			StringBuilder sb = new StringBuilder();
			try {

				SubscriptionOrderItemIntf lastItem = this.getOrderedSubscriptionItem();
				if (lastItem.getProduct().getProductCode().equalsIgnoreCase(UsaTodayConstants.UT_PUBCODE)) {
					// UT
					sb.append("<img src=\"https://www.commission-junction.com/track/receive.dll?AMOUNT=");
					sb.append(UTCommon.COMM_AMOUNT);
					sb.append("&CID=");
					sb.append(UTCommon.CID_NUMBER);
					sb.append("&OID=");
					sb.append(this.lastOrder.getOrderID());
					sb.append("&TYPE=");
					sb.append(UTCommon.COMM_JUNCTION_UT_TYPE);
					sb.append("&KEEP=YES&METHOD=IMG\" height=\"1\" width=\"1\">");
				} else if (lastItem.getProduct().getProductCode().equalsIgnoreCase(UsaTodayConstants.BW_PUBCODE)) {
					// BW
					sb.append("<img src=\"https://www.commission-junction.com/track/receive.dll?AMOUNT=");
					sb.append(UTCommon.COMM_AMOUNT);
					sb.append("&CID=");
					sb.append(UTCommon.CID_NUMBER);
					sb.append("&OID=");
					sb.append(this.lastOrder.getOrderID());
					sb.append("&TYPE=");
					sb.append(UTCommon.COMM_JUNCTION_SW_TYPE);
					sb.append("&KEEP=YES&METHOD=IMG\" height=\"1\" width=\"1\">");
				} else if (lastItem.getProduct().getProductCode().equalsIgnoreCase(UsaTodayConstants.DEFAULT_UT_EE_PUBCODE)) {
					// EE
					if (this.lastOrder.getTotal() == 0.0) {
						sb.append("<img src=\"https://www.emjcd.com/u?AMOUNT=0.00&CID=741590&OID=");
						sb.append(this.lastOrder.getOrderID());
						sb.append("&TYPE=330529&CURRENCY=USD&METHOD=IMG\" height=\"1\" width=\"1\" >");
					} else {
						sb.append("<img src=\"https://www.emjcd.com/u?AMOUNT=9.95&CID=741590&OID=");
						sb.append(this.lastOrder.getOrderID());
						sb.append("&TYPE=330530&CURRENCY=USD&METHOD=IMG\" height=\"1\" width=\"1\">");
					}
				}
			} catch (Exception e) {
				e.printStackTrace();

			}

			pixel = sb.toString();
		}

		return pixel;
	}

	public String getOnePageThankYouJavaScript1Text() {
		String jScript = null;

		try {
			if (this.lastOrder != null) {
				SubscriptionOrderItemIntf item = this.getOrderedSubscriptionItem();

				PromotionSet pSet = item.getOffer().getPromotionSet();

				JavaScriptPromotionIntf jPromo = pSet.getPromoOnePageThankYouJavaScript1Text();
				if (jPromo != null) {
					jScript = jPromo.getDynamicJavaScript(this.lastOrder.getOrderID(), this.lastOrder.getTotal(),
							this.lastOrder.getSubTotal(), item.getKeyCode(), item.getProduct().getProductCode(),
							item.getDeliveryEmailAddress());
				}
			}
		} catch (Exception e) {
			// e.printStackTrace();
		}
		return jScript;
	}

	public String getOnePageThankYouJavaScript2Text() {
		String jScript = null;

		try {
			if (this.lastOrder != null) {
				SubscriptionOrderItemIntf item = this.getOrderedSubscriptionItem();

				PromotionSet pSet = item.getOffer().getPromotionSet();

				JavaScriptPromotionIntf jPromo = pSet.getPromoOnePageThankYouJavaScript2Text();
				if (jPromo != null) {
					jScript = jPromo.getDynamicJavaScript(this.lastOrder.getOrderID(), this.lastOrder.getTotal(),
							this.lastOrder.getSubTotal(), item.getKeyCode(), item.getProduct().getProductCode(),
							item.getDeliveryEmailAddress());
				}
			}
		} catch (Exception e) {
			// e.printStackTrace();
		}
		return jScript;
	}

	public String getOnePageThankYouJavaScript3Text() {
		String jScript = null;

		try {
			if (this.lastOrder != null) {
				SubscriptionOrderItemIntf item = this.getOrderedSubscriptionItem();

				PromotionSet pSet = item.getOffer().getPromotionSet();

				JavaScriptPromotionIntf jPromo = pSet.getPromoOnePageThankYouJavaScript3Text();
				if (jPromo != null) {
					jScript = jPromo.getDynamicJavaScript(this.lastOrder.getOrderID(), this.lastOrder.getTotal(),
							this.lastOrder.getSubTotal(), item.getKeyCode(), item.getProduct().getProductCode(),
							item.getDeliveryEmailAddress());
				}
			}
		} catch (Exception e) {
			// e.printStackTrace();
		}
		return jScript;
	}

	public String getOnePageThankYouJavaScript4Text() {
		String jScript = null;

		try {
			if (this.lastOrder != null) {
				SubscriptionOrderItemIntf item = this.getOrderedSubscriptionItem();

				PromotionSet pSet = item.getOffer().getPromotionSet();

				JavaScriptPromotionIntf jPromo = pSet.getPromoOnePageThankYouJavaScript4Text();
				if (jPromo != null) {
					jScript = jPromo.getDynamicJavaScript(this.lastOrder.getOrderID(), this.lastOrder.getTotal(),
							this.lastOrder.getSubTotal(), item.getKeyCode(), item.getProduct().getProductCode(),
							item.getDeliveryEmailAddress());
				}
			}
		} catch (Exception e) {
			// e.printStackTrace();
		}
		return jScript;
	}

	public String getOnePageThankYouJavaScript5Text() {
		String jScript = null;

		try {
			if (this.lastOrder != null) {
				SubscriptionOrderItemIntf item = this.getOrderedSubscriptionItem();

				PromotionSet pSet = item.getOffer().getPromotionSet();

				JavaScriptPromotionIntf jPromo = pSet.getPromoOnePageThankYouJavaScript5Text();
				if (jPromo != null) {
					jScript = jPromo.getDynamicJavaScript(this.lastOrder.getOrderID(), this.lastOrder.getTotal(),
							this.lastOrder.getSubTotal(), item.getKeyCode(), item.getProduct().getProductCode(),
							item.getDeliveryEmailAddress());
				}
			}
		} catch (Exception e) {
			// e.printStackTrace();
		}
		return jScript;
	}

	public String getOnePageThankYouJavaScript6Text() {
		String jScript = null;

		try {
			if (this.lastOrder != null) {
				SubscriptionOrderItemIntf item = this.getOrderedSubscriptionItem();

				PromotionSet pSet = item.getOffer().getPromotionSet();

				JavaScriptPromotionIntf jPromo = pSet.getPromoOnePageThankYouJavaScript6Text();
				if (jPromo != null) {
					jScript = jPromo.getDynamicJavaScript(this.lastOrder.getOrderID(), this.lastOrder.getTotal(),
							this.lastOrder.getSubTotal(), item.getKeyCode(), item.getProduct().getProductCode(),
							item.getDeliveryEmailAddress());
				}
			}
		} catch (Exception e) {
			// e.printStackTrace();
		}
		return jScript;
	}

	public String getOnePageThankYouJavaScript7Text() {
		String jScript = null;

		try {
			if (this.lastOrder != null) {
				SubscriptionOrderItemIntf item = this.getOrderedSubscriptionItem();

				PromotionSet pSet = item.getOffer().getPromotionSet();

				JavaScriptPromotionIntf jPromo = pSet.getPromoOnePageThankYouJavaScript7Text();
				if (jPromo != null) {
					jScript = jPromo.getDynamicJavaScript(this.lastOrder.getOrderID(), this.lastOrder.getTotal(),
							this.lastOrder.getSubTotal(), item.getKeyCode(), item.getProduct().getProductCode(),
							item.getDeliveryEmailAddress());
				}
			}
		} catch (Exception e) {
			// e.printStackTrace();
		}
		return jScript;
	}

	public String getOnePageThankYouJavaScript8Text() {
		String jScript = null;

		try {
			if (this.lastOrder != null) {
				SubscriptionOrderItemIntf item = this.getOrderedSubscriptionItem();

				PromotionSet pSet = item.getOffer().getPromotionSet();

				JavaScriptPromotionIntf jPromo = pSet.getPromoOnePageThankYouJavaScript8Text();
				if (jPromo != null) {
					jScript = jPromo.getDynamicJavaScript(this.lastOrder.getOrderID(), this.lastOrder.getTotal(),
							this.lastOrder.getSubTotal(), item.getKeyCode(), item.getProduct().getProductCode(),
							item.getDeliveryEmailAddress());
				}
			}
		} catch (Exception e) {
			// e.printStackTrace();
		}
		return jScript;
	}

	public String getOnePageThankYouJavaScript9Text() {
		String jScript = null;

		try {
			if (this.lastOrder != null) {
				SubscriptionOrderItemIntf item = this.getOrderedSubscriptionItem();

				PromotionSet pSet = item.getOffer().getPromotionSet();

				JavaScriptPromotionIntf jPromo = pSet.getPromoOnePageThankYouJavaScript9Text();
				if (jPromo != null) {
					jScript = jPromo.getDynamicJavaScript(this.lastOrder.getOrderID(), this.lastOrder.getTotal(),
							this.lastOrder.getSubTotal(), item.getKeyCode(), item.getProduct().getProductCode(),
							item.getDeliveryEmailAddress());
				}
			}
		} catch (Exception e) {
			// e.printStackTrace();
		}
		return jScript;
	}

	public String getOnePageThankYouJavaScript10Text() {
		String jScript = null;

		try {
			if (this.lastOrder != null) {
				SubscriptionOrderItemIntf item = this.getOrderedSubscriptionItem();

				PromotionSet pSet = item.getOffer().getPromotionSet();

				JavaScriptPromotionIntf jPromo = pSet.getPromoOnePageThankYouJavaScript10Text();
				if (jPromo != null) {
					jScript = jPromo.getDynamicJavaScript(this.lastOrder.getOrderID(), this.lastOrder.getTotal(),
							this.lastOrder.getSubTotal(), item.getKeyCode(), item.getProduct().getProductCode(),
							item.getDeliveryEmailAddress());
				}
			}
		} catch (Exception e) {
			// e.printStackTrace();
		}
		return jScript;
	}

	public String getDeliveryMethodDescription() {

		String deliveryMethod = "";
		try {
			if (this.lastOrder != null) {
				SubscriptionOrderItemIntf item = this.getOrderedSubscriptionItem();

				String method = item.getDeliveryMethod();
				if (method != null) {
					if (item.getProduct().isElectronicDelivery()) {
						deliveryMethod = "Electronic Delivery";
					} else if (method.equalsIgnoreCase("M")) {
						deliveryMethod = "Mail Delivery";
					} else if (method.equalsIgnoreCase("C")) {
						deliveryMethod = "Morning Delivery";
					}

				}
			}
		} catch (Exception e) {
		}
		return deliveryMethod;
	}

	public boolean isShowDeliveryMethod() {

		boolean showIt = false;

		try {
			if (this.lastOrder != null) {
				SubscriptionOrderItemIntf item = this.getOrderedSubscriptionItem();

				if (item.getProduct().isElectronicDelivery()) {
					showIt = true;
				} else {

					if (item instanceof SubscriptionOrderItemBO) {
						SubscriptionOrderItemBO itemBO = (SubscriptionOrderItemBO) item;
						if (itemBO.isDeliveryMethodCheck()) {
							showIt = true;
						}
					}
				}
			}
		} catch (Exception e) {
		}

		return showIt;
	}

	public String getOnePageThankYouJavaScriptTealiumText() {
		String jScript = null;
		HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		String uri = req.getRequestURI();
		// In JSF, the above returns the same URL address as the order page, not the actual thankyou.jsp, so need to hardcode 
		String pageName = uri.substring(0,uri.lastIndexOf("/")) + "/thankyou.jsp";
		try {
			if (this.lastOrder != null) {
				SubscriptionOrderItemIntf item = this.getOrderedSubscriptionItem();
	
				PromotionSet pSet = item.getOffer().getPromotionSet();
	
				JavaScriptPromotionIntf jPromo = pSet.getPromoOnePageThankYouJavaScriptTealiumText();
				if (jPromo != null) {
					jScript = jPromo.getTealiumDynamicJavaScript(this.lastOrder.getOrderID(), this.lastOrder.getTotal(),
							this.lastOrder.getSubTotal(), item.getKeyCode(), item.getProduct().getProductCode(), item
									.getDeliveryEmailAddress(), pageName, item.getSelectedTerm().getPiaRateCode(), item
									.getSelectedTerm().getDescription(), item.getSelectedTerm().requiresEZPAY(), item
									.getSelectedTerm().getDuration());
				}
			}
		} catch (Exception e) {
			// e.printStackTrace();
		}
		return jScript;
	}

	public String getOnePageThankYouJavaScript11Text() {
		String jScript = null;

		try {
			if (this.lastOrder != null) {
				SubscriptionOrderItemIntf item = this.getOrderedSubscriptionItem();

				PromotionSet pSet = item.getOffer().getPromotionSet();

				JavaScriptPromotionIntf jPromo = pSet.getPromoOnePageThankYouJavaScript11Text();
				if (jPromo != null) {
					jScript = jPromo.getDynamicJavaScript(this.lastOrder.getOrderID(), this.lastOrder.getTotal(),
							this.lastOrder.getSubTotal(), item.getKeyCode(), item.getProduct().getProductCode(),
							item.getDeliveryEmailAddress());
				}
			}
		} catch (Exception e) {
			// e.printStackTrace();
		}
		return jScript;
	}

	public String getOnePageThankYouJavaScript12Text() {
		String jScript = null;

		try {
			if (this.lastOrder != null) {
				SubscriptionOrderItemIntf item = this.getOrderedSubscriptionItem();

				PromotionSet pSet = item.getOffer().getPromotionSet();

				JavaScriptPromotionIntf jPromo = pSet.getPromoOnePageThankYouJavaScript12Text();
				if (jPromo != null) {
					jScript = jPromo.getDynamicJavaScript(this.lastOrder.getOrderID(), this.lastOrder.getTotal(),
							this.lastOrder.getSubTotal(), item.getKeyCode(), item.getProduct().getProductCode(),
							item.getDeliveryEmailAddress());
				}
			}
		} catch (Exception e) {
			// e.printStackTrace();
		}
		return jScript;
	}

	public String getOnePageThankYouJavaScript13Text() {
		String jScript = null;

		try {
			if (this.lastOrder != null) {
				SubscriptionOrderItemIntf item = this.getOrderedSubscriptionItem();

				PromotionSet pSet = item.getOffer().getPromotionSet();

				JavaScriptPromotionIntf jPromo = pSet.getPromoOnePageThankYouJavaScript13Text();
				if (jPromo != null) {
					jScript = jPromo.getDynamicJavaScript(this.lastOrder.getOrderID(), this.lastOrder.getTotal(),
							this.lastOrder.getSubTotal(), item.getKeyCode(), item.getProduct().getProductCode(),
							item.getDeliveryEmailAddress());
				}
			}
		} catch (Exception e) {
			// e.printStackTrace();
		}
		return jScript;
	}

	public String getOnePageThankYouJavaScript14Text() {
		String jScript = null;

		try {
			if (this.lastOrder != null) {
				SubscriptionOrderItemIntf item = this.getOrderedSubscriptionItem();

				PromotionSet pSet = item.getOffer().getPromotionSet();

				JavaScriptPromotionIntf jPromo = pSet.getPromoOnePageThankYouJavaScript14Text();
				if (jPromo != null) {
					jScript = jPromo.getDynamicJavaScript(this.lastOrder.getOrderID(), this.lastOrder.getTotal(),
							this.lastOrder.getSubTotal(), item.getKeyCode(), item.getProduct().getProductCode(),
							item.getDeliveryEmailAddress());
				}
			}
		} catch (Exception e) {
			// e.printStackTrace();
		}
		return jScript;
	}

	public String getOnePageThankYouJavaScript15Text() {
		String jScript = null;

		try {
			if (this.lastOrder != null) {
				SubscriptionOrderItemIntf item = this.getOrderedSubscriptionItem();

				PromotionSet pSet = item.getOffer().getPromotionSet();

				JavaScriptPromotionIntf jPromo = pSet.getPromoOnePageThankYouJavaScript15Text();
				if (jPromo != null) {
					jScript = jPromo.getDynamicJavaScript(this.lastOrder.getOrderID(), this.lastOrder.getTotal(),
							this.lastOrder.getSubTotal(), item.getKeyCode(), item.getProduct().getProductCode(),
							item.getDeliveryEmailAddress());
				}
			}
		} catch (Exception e) {
			// e.printStackTrace();
		}
		return jScript;
	}

	public String getOnePageThankYouJavaScript16Text() {
		String jScript = null;

		try {
			if (this.lastOrder != null) {
				SubscriptionOrderItemIntf item = this.getOrderedSubscriptionItem();

				PromotionSet pSet = item.getOffer().getPromotionSet();

				JavaScriptPromotionIntf jPromo = pSet.getPromoOnePageThankYouJavaScript16Text();
				if (jPromo != null) {
					jScript = jPromo.getDynamicJavaScript(this.lastOrder.getOrderID(), this.lastOrder.getTotal(),
							this.lastOrder.getSubTotal(), item.getKeyCode(), item.getProduct().getProductCode(),
							item.getDeliveryEmailAddress());
				}
			}
		} catch (Exception e) {
			// e.printStackTrace();
		}
		return jScript;
	}

	public String getOnePageThankYouJavaScript17Text() {
		String jScript = null;

		try {
			if (this.lastOrder != null) {
				SubscriptionOrderItemIntf item = this.getOrderedSubscriptionItem();

				PromotionSet pSet = item.getOffer().getPromotionSet();

				JavaScriptPromotionIntf jPromo = pSet.getPromoOnePageThankYouJavaScript17Text();
				if (jPromo != null) {
					jScript = jPromo.getDynamicJavaScript(this.lastOrder.getOrderID(), this.lastOrder.getTotal(),
							this.lastOrder.getSubTotal(), item.getKeyCode(), item.getProduct().getProductCode(),
							item.getDeliveryEmailAddress());
				}
			}
		} catch (Exception e) {
			// e.printStackTrace();
		}
		return jScript;
	}

	public String getOnePageThankYouJavaScript18Text() {
		String jScript = null;

		try {
			if (this.lastOrder != null) {
				SubscriptionOrderItemIntf item = this.getOrderedSubscriptionItem();

				PromotionSet pSet = item.getOffer().getPromotionSet();

				JavaScriptPromotionIntf jPromo = pSet.getPromoOnePageThankYouJavaScript18Text();
				if (jPromo != null) {
					jScript = jPromo.getDynamicJavaScript(this.lastOrder.getOrderID(), this.lastOrder.getTotal(),
							this.lastOrder.getSubTotal(), item.getKeyCode(), item.getProduct().getProductCode(),
							item.getDeliveryEmailAddress());
				}
			}
		} catch (Exception e) {
			// e.printStackTrace();
		}
		return jScript;
	}

	public String getOnePageThankYouJavaScript19Text() {
		String jScript = null;

		try {
			if (this.lastOrder != null) {
				SubscriptionOrderItemIntf item = this.getOrderedSubscriptionItem();

				PromotionSet pSet = item.getOffer().getPromotionSet();

				JavaScriptPromotionIntf jPromo = pSet.getPromoOnePageThankYouJavaScript19Text();
				if (jPromo != null) {
					jScript = jPromo.getDynamicJavaScript(this.lastOrder.getOrderID(), this.lastOrder.getTotal(),
							this.lastOrder.getSubTotal(), item.getKeyCode(), item.getProduct().getProductCode(),
							item.getDeliveryEmailAddress());
				}
			}
		} catch (Exception e) {
			// e.printStackTrace();
		}
		return jScript;
	}

	public String getOnePageThankYouJavaScript20Text() {
		String jScript = null;

		try {
			if (this.lastOrder != null) {
				SubscriptionOrderItemIntf item = this.getOrderedSubscriptionItem();

				PromotionSet pSet = item.getOffer().getPromotionSet();

				JavaScriptPromotionIntf jPromo = pSet.getPromoOnePageThankYouJavaScript20Text();
				if (jPromo != null) {
					jScript = jPromo.getDynamicJavaScript(this.lastOrder.getOrderID(), this.lastOrder.getTotal(),
							this.lastOrder.getSubTotal(), item.getKeyCode(), item.getProduct().getProductCode(),
							item.getDeliveryEmailAddress());
				}
			}
		} catch (Exception e) {
			// e.printStackTrace();
		}
		return jScript;
	}
}
