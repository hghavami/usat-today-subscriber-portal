package com.usatoday.esub.handlers;

import java.util.ArrayList;
import java.util.Date;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.joda.time.DateTime;

import com.usatoday.business.interfaces.products.SubscriptionOfferIntf;
import com.usatoday.business.interfaces.products.SubscriptionProductIntf;
import com.usatoday.business.interfaces.products.SubscriptionTermsIntf;
import com.usatoday.business.interfaces.products.promotions.CreditCardPromotionIntf;
import com.usatoday.business.interfaces.products.promotions.HTMLPromotionIntf;
import com.usatoday.business.interfaces.products.promotions.ImagePromotionIntf;
import com.usatoday.business.interfaces.products.promotions.PromotionIntf;
import com.usatoday.businessObjects.products.SubscriptionOfferManager;
import com.usatoday.businessObjects.products.SubscriptionProductBO;
import com.usatoday.businessObjects.products.promotions.PromotionSet;
import com.usatoday.businessObjects.util.AS400CurrentStatus;
import com.usatoday.esub.common.UTCommon;
import com.usatoday.util.constants.UsaTodayConstants;

public class SubscriptionOfferHandler {

	public static String DEFAULT_UT_BRAND_NUM_COPIES_LEARN_MORE_HTML = "&nbsp;<a onclick=\"javascript:window.open('/faq/utfaq.jsp#section29','FAQ', 'width=800,height=800,menubar=no,titlebar=yes,scrollbars=yes,status=no,location=no,directories=no')\" href=\"javascript:;\"><span style=\"font-family: Arial; font-size: 7pt;\">Learn More...</span></a>";
	public static String DEFAULT_BW_BRAND_NUM_COPIES_LEARN_MORE_HTML = "&nbsp;<a onclick=\"javascript:window.open('/faq/bwfaq.jsp#section29','FAQ', 'width=800,height=800,menubar=no,titlebar=yes,scrollbars=yes,status=no,location=no,directories=no')\" href=\"javascript:;\"><span style=\"font-family: Arial; font-size: 7pt;\">Learn More...</span></a>";

	public static String ADDRESS_LEARN_MORE_HTML = "<span style=\"font-family: Arial; font-size: 7pt;\">Learn More...</span>";
	public static String WHAT_THIS_HTML = "<span style=\"font-family: Arial; font-size: 7pt;\">What's this?</span>";

	public static String NUM_COPIES_LEARN_MORE = "<span style=\"font-family: Arial; font-size: 7pt;\">Learn More...</span>";

	private SubscriptionOfferIntf currentOffer = null;

	private SubscriptionOfferIntf previousOffer = null;

	private SubscriptionProductIntf product = null;

	public String getAddressLearnMoreHTML() {
		String learnMoreAddress = SubscriptionOfferHandler.ADDRESS_LEARN_MORE_HTML;

		return learnMoreAddress;
	}

	public ArrayList<SelectItem> getCreditCardExpirationYears() {

		ArrayList<SelectItem> items = new ArrayList<SelectItem>();

		try {
			SelectItem si = new SelectItem();
			si.setDescription("Year");
			si.setLabel("Year");
			si.setValue("-1");
			items.add(si);

			DateTime today = new DateTime();

			for (int c = 0; c < 11; c++) {
				String year = today.toString("yyyy");

				si = new SelectItem();

				si.setDescription(year);
				si.setLabel(year);
				si.setValue(year);

				items.add(si);

				today = today.plusYears(1);
			}
		} catch (Exception e) {
			System.out.println("SubscriptionOfferHandler:  Failed to build terms items: " + e.getMessage());
		}
		return items;
	}

	public SubscriptionOfferIntf getCurrentOffer() {
		return currentOffer;
	}

	public String getCustomerServicePhone() {
		String custServicePhone = "";
		if (this.currentOffer != null) {

			try {
				PromotionIntf p = this.currentOffer.getPromotionSet().getCustomerServicePhoneCheck();

				if (p != null) {
					custServicePhone = p.getFulfillText();
				}

			} catch (Exception e) {
			}
		}
		return custServicePhone;
	}

	public String getDualPagePromotionText() {
		String html = "";
		try {
			if (this.currentOffer != null) {
				html = this.currentOffer.getPromotionSet().getPromoLandingPageText().getPromotionalHTML();
			} else {
				SubscriptionOfferIntf o = SubscriptionOfferManager.getInstance().getDefaultOffer(UsaTodayConstants.UT_PUBCODE);
				html = o.getPromotionSet().getPromoLandingPageText().getPromotionalHTML();
			}
		} catch (Exception e) {
			html = "";
		}
		return html;
	}

	public Date getEarliestPossibleStartDate() {
		Date start = null;

		// this is for calendar validation only

		DateTime today = new DateTime();
		start = today.minusDays(1).toDate();

		return start;
	}

	public String getEarliestPossibleStartDateString() {

		DateTime start = null;
		if (this.product != null) {
			start = this.product.getEarliestPossibleStartDate();
		}

		if (start == null) {
			start = new DateTime();
		}

		return start.toString("MM/dd/yyyy");
	}

	public SubscriptionOfferHandler getElectronicEditionOffer() {
		SubscriptionOfferHandler soh = null;

		try {
			if (this.product.getBrandingPubCode().equalsIgnoreCase(UsaTodayConstants.UT_PUBCODE)) {

				if (this.product.isElectronicDelivery()) {
					soh = this;
				} else {
					SubscriptionOfferIntf offer = null;
					try {
						offer = SubscriptionOfferManager.getInstance().getEEOffer(this.currentOffer.getKeyCode());
					} catch (Exception e) {
					}

					if (offer == null) {
						try {
							offer = SubscriptionOfferManager.getInstance().getEEOffer(this.product.getDefaultKeycode());
						} catch (Exception e) {
						}
					}
					soh = new SubscriptionOfferHandler();
					soh.setCurrentOffer(offer);
				}
			}
			/* will need to add SW later */
		} catch (Exception e) {
			soh = this;
		}

		return soh;
	}

	public String getForceEZPayCustomTermsText() {
		String text = "";
		try {
			if (this.currentOffer != null) {
				text = this.currentOffer.getPromotionSet().getPromoOnePageForceEZPayCustomTermsText().getPromotionalHTML();
			}
		} catch (Exception e) {
			text = "";
		}

		return text;
	}

	public boolean getIsBillMeAllowed() {
		boolean isAllowed = false;
		if (this.currentOffer != null) {
			isAllowed = this.currentOffer.isBillMeAllowed();
		}
		return isAllowed;
	}

	public boolean getIsClubNumberRequired() {
		boolean isClubNumberRequired = false;
		if (this.currentOffer != null) {
			isClubNumberRequired = this.currentOffer.isClubNumberRequired();
		}
		return isClubNumberRequired;
	}

	public boolean getIsForceBillMe() {
		boolean isForced = false;
		if (this.currentOffer != null) {
			isForced = this.currentOffer.isForceBillMe();
		}
		return isForced;
	}

	public boolean getIsForceEZPAY() {
		boolean isForceEzPAY = false;
		if (this.currentOffer != null) {
			isForceEzPAY = this.currentOffer.isForceEZPay();
		}
		return isForceEzPAY;
	}

	public boolean getIsForceEZPAYRate() {
		boolean isForceEzPayRate = false;
		try {
			if (this.currentOffer != null) {
				for (SubscriptionTermsIntf aTerm : this.currentOffer.getTerms()) {
					if (this.currentOffer.isForceEZPay() || aTerm.requiresEZPAY()) {
						isForceEzPayRate = true;
					}
				}
			}
		} catch (Exception e) {
			System.out.println("SubscriptionOfferHandler:  Failed to derive Force EZPay terms: " + e.getMessage());
		}

		return isForceEzPayRate;
	}

	public boolean getIsNonEZPayRate() {
		boolean isNonEzPayRate = false;

		try {
			if (this.currentOffer != null) {
				for (SubscriptionTermsIntf aTerm : this.currentOffer.getTerms()) {
					if (this.currentOffer.isForceEZPay() || aTerm.requiresEZPAY()) {
						// Do nothing
					} else {
						isNonEzPayRate = true;
					}
				}
			}
		} catch (Exception e) {
			System.out.println("SubscriptionOfferHandler:  Failed to derive non-EZPay terms: " + e.getMessage());
		}

		return isNonEzPayRate;
	}

	public String getEZPayDisclaimer() {
		String disclaimerText = "";
		try {
			if (this.currentOffer != null) {
				for (SubscriptionTermsIntf aTerm : this.currentOffer.getTerms()) {
					// if (this.currentOffer.isForceEZPay() || aTerm.requiresEZPAY()) {
					disclaimerText = aTerm.getDisclaimerText();
					if (disclaimerText != null && !disclaimerText.trim().equals("")) {
						break;
					}
					// }
				}
			}
		} catch (Exception e) {
			System.out.println("SubscriptionOfferHandler:  Failed to derive EZPay disclaimer text: " + e.getMessage());
		}
		return disclaimerText;
	}

	public String getNonEZPayDisclaimer() {
		String disclaimerText = "";
		try {
			if (this.currentOffer != null) {
				for (SubscriptionTermsIntf aTerm : this.currentOffer.getTerms()) {
					if (this.currentOffer.isForceEZPay() || aTerm.requiresEZPAY()) {
						// Do nothing
					} else {
						disclaimerText = aTerm.getDisclaimerText();
					}
				}
			}
		} catch (Exception e) {
			System.out.println("SubscriptionOfferHandler:  Failed to derive EZPay disclaimer text: " + e.getMessage());
		}
		return disclaimerText;
	}

	public boolean getIsShowChangeOrderQtySection() {
		boolean showSection = false;

		if (this.product != null && this.product.getMaxQuantityPerOrder() > 1) {
			showSection = true;
		}
		return showSection;
	}

	public boolean getIsShowCreditCardPaymentOptionData() {
		boolean showOptions = true;
		try {
			if (this.currentOffer != null) {
				if (this.currentOffer.isForceEZPay() || !this.currentOffer.isForceBillMe()) {
					// force ezpay or not a force bill me, then show CC info
					showOptions = true;
				} else {
					showOptions = false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return showOptions;
	}

	public boolean getIsShowOfferDisclaimerText() {
		boolean showDisclaimer = false;
		try {
			if (this.currentOffer != null) {
				HTMLPromotionIntf disclaimerTextPromo = this.currentOffer.getPromotionSet().getPromoOnePageDisclaimerText();
				if (disclaimerTextPromo == null || disclaimerTextPromo.getPromotionalHTML().length() == 0) {
					showDisclaimer = false;
				} else {
					showDisclaimer = true;
				}
			}
		} catch (Exception e) {
			showDisclaimer = false;
		}
		return showDisclaimer;
	}

	public boolean getIsShowOfferOverride() {
		boolean showOverride = false;
		try {
			if (this.currentOffer != null) {
				PromotionIntf promo = this.currentOffer.getPromotionSet().getAllowOfferOverrides();
				if (promo != null && promo.getFulfillText() != null && promo.getFulfillText().equalsIgnoreCase("Y")) {
					showOverride = true;
				}
			}

		} catch (Exception e) {
			showOverride = false;
		}
		return showOverride;
	}

	public boolean getIsShowOrderPathOverlayPopUp() {
		boolean showKeyCodeOverlay = false;
		try {
			if (this.currentOffer != null) {
				// first check offer for this pub/keycode
				ImagePromotionIntf imagePromo = this.currentOffer.getPromotionSet().getOrderPathPopOverlayPromoImage();

				if (imagePromo != null) {
					String fulfillText = imagePromo.getFulfillText();
					if (fulfillText != null && !fulfillText.trim().equalsIgnoreCase("HIDE")) {
						showKeyCodeOverlay = true;
					}
				}

			}

		} catch (Exception e) {
			showKeyCodeOverlay = false;
		}
		return showKeyCodeOverlay;
	}

	public boolean getIsShowPaymentOptionSelector() {
		boolean showOptions = false;
		try {
			if (this.currentOffer != null) {
				if (this.currentOffer.isBillMeAllowed() && !this.currentOffer.isForceBillMe()) {
					// if bill me is allowed, but not forced then show options
					showOptions = true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return showOptions;
	}

	public boolean getIsTakesAMEX() {
		// default to true
		boolean takesCard = true;
		if (this.currentOffer != null) {
			CreditCardPromotionIntf ccPromo = this.currentOffer.getPromotionSet().getPromoCreditCard();
			if (ccPromo != null) {
				takesCard = ccPromo.acceptsAmEx();
			}
		}
		return takesCard;
	}

	public boolean getIsTakesDiners() {
		// default to true
		boolean takesCard = true;
		if (this.currentOffer != null) {
			CreditCardPromotionIntf ccPromo = this.currentOffer.getPromotionSet().getPromoCreditCard();
			if (ccPromo != null) {
				takesCard = ccPromo.acceptsDiners();
			}
		}
		return takesCard;
	}

	public boolean getIsTakesDiscover() {
		// default to true
		boolean takesCard = true;
		if (this.currentOffer != null) {
			CreditCardPromotionIntf ccPromo = this.currentOffer.getPromotionSet().getPromoCreditCard();
			if (ccPromo != null) {
				takesCard = ccPromo.acceptsDiscovery();
			}
		}
		return takesCard;
	}

	public boolean getIsTakesMasterCard() {
		// default to true
		boolean takesCard = true;
		if (this.currentOffer != null) {
			CreditCardPromotionIntf ccPromo = this.currentOffer.getPromotionSet().getPromoCreditCard();
			if (ccPromo != null) {
				takesCard = ccPromo.acceptsMasterCard();
			}
		}
		return takesCard;
	}

	public boolean getIsTakesVisa() {
		// default to true
		boolean takesCard = true;
		if (this.currentOffer != null) {
			CreditCardPromotionIntf ccPromo = this.currentOffer.getPromotionSet().getPromoCreditCard();
			if (ccPromo != null) {
				takesCard = ccPromo.acceptsVisa();
			}
		}
		return takesCard;
	}

	public boolean getIsShowAccountInfoPromoSection() {
		// default to true
		boolean showSection = false;
		if (this.currentOffer != null) {
			if (this.currentOffer.getPromotionSet().getPromoAccountInfoImageSpot1() != null
					|| this.currentOffer.getPromotionSet().getPromoAccountInfoImageSpot2() != null
					|| this.currentOffer.getPromotionSet().getPromoAccountInfoImageSpot3() != null) {
				showSection = true;
			}
		}
		return showSection;
	}

	public Date getLatestPossibleStartDate() {
		Date end = null;

		if (this.product != null) {
			end = this.product.getLatestPossibleStartDate().toDate();
		}

		if (end == null) {
			DateTime now = new DateTime();
			end = now.plusDays(90).toDate();
		}
		return end;
	}

	public String getLatestPossibleStartDateString() {
		DateTime end = null;
		if (this.product != null) {
			end = this.product.getLatestPossibleStartDate();
		}

		if (end == null) {
			DateTime now = new DateTime();
			end = now.plusDays(90);
		}

		return end.toString("MM/dd/yyyy");
	}

	public String getMaximumQuantityPerOrder() {
		int max = 10;

		if (this.product != null && this.product.getMaxQuantityPerOrder() > 1) {
			max = this.product.getMaxQuantityPerOrder();
		}
		return String.valueOf(max);
	}

	public ArrayList<SelectItem> getOfferTermsSelectItems() {

		ArrayList<SelectItem> items = new ArrayList<SelectItem>();
		// INC652627 - remove rate per copy
		// java.text.NumberFormat currencyFormat = java.text.NumberFormat.getCurrencyInstance();

		try {
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
			HttpSession session = (HttpSession) externalContext.getSession(false);
			HttpServletRequest req = (HttpServletRequest) externalContext.getRequest();
			String urlRateCode = req.getParameter("ratecode");
			if (urlRateCode != null) {
				session.setAttribute(UTCommon.SESSION_URL_RATECODE, urlRateCode);
			}
			urlRateCode = (String) session.getAttribute(UTCommon.SESSION_URL_RATECODE);
			boolean urlRateCodeFound = false;
			if (this.currentOffer != null) {
				// If rate code is specified in the URL. If so, then check if it was valid
				if (urlRateCode != null && !urlRateCode.equals("")) {
					for (SubscriptionTermsIntf aTerm : this.currentOffer.getTerms()) {
						if (urlRateCode.equals(aTerm.getPiaRateCode())) {
							urlRateCodeFound = true;
							break;
						}
					}
				}
				for (SubscriptionTermsIntf aTerm : this.currentOffer.getTerms()) {
					SelectItem si = new SelectItem();
					// set to render the HTML
					si.setEscape(false);
					// If rate code specified in the URL and was valid
					if (urlRateCodeFound) {
						if (urlRateCode.equals(aTerm.getPiaRateCode())) {
							StringBuilder sb = new StringBuilder("<strong>First ");
							sb.append(aTerm.getDescription());
							sb.append("</strong>");
							if (this.currentOffer.isForceEZPay() || aTerm.requiresEZPAY()) {
								sb.append("<font color=\"navy\">&nbsp;<sup>*</sup></font>");
								sb.append("<strong>");
								sb.append(this.getTermsDescriptionTextEzpayHTML(aTerm.getPiaRateCode()));
								sb.append("</strong>");
							} else {
								sb.append("<strong>");
								sb.append(this.getTermsDescriptionTextHTML(aTerm.getPiaRateCode()));
								sb.append("</strong>");
							}
							si.setDescription(sb.toString());
							si.setLabel(sb.toString());
							si.setValue(aTerm.getTermAsString());
							items.add(si);						
//							break;
						}
					} else {
						StringBuilder sb = new StringBuilder("<strong>First ");
						sb.append(aTerm.getDescription());
						sb.append("</strong>");
						if (this.currentOffer.isForceEZPay() || aTerm.requiresEZPAY()) {
							sb.append("<font color=\"navy\">&nbsp;<sup>*</sup></font>");
							sb.append("<strong>");
							sb.append(this.getTermsDescriptionTextEzpayHTML(aTerm.getPiaRateCode()));
							sb.append("</strong>");
						} else {
							sb.append("<strong>");
							sb.append(this.getTermsDescriptionTextHTML(aTerm.getPiaRateCode()));
							sb.append("</strong>");
						}
						si.setDescription(sb.toString());
						si.setLabel(sb.toString());
						si.setValue(aTerm.getTermAsString());
						items.add(si);
					}

					// INC652627 - remove rate per copy
					// sb.append(" - ").append(currencyFormat.format(aTerm.getDailyRate())).append(" per issue");
				}
			}
		} catch (Exception e) {
			System.out.println("SubscriptionOfferHandler:  Failed to build terms items: " + e.getMessage());
		}
		return items;
	}

	public String getOnePageDisclaimerText() {
		String html = "";
		try {
			if (this.currentOffer != null) {
				html = this.currentOffer.getPromotionSet().getPromoOnePageDisclaimerText().getPromotionalHTML();
			} else {
				SubscriptionOfferIntf o = SubscriptionOfferManager.getInstance().getDefaultOffer(this.product.getBrandingPubCode());
				html = o.getPromotionSet().getPromoOnePageDisclaimerText().getPromotionalHTML();
			}
		} catch (Exception e) {
			html = "";
		}
		return html;
	}

	public String getOnePageJavaScriptText() {
		String html = "";
		try {
			if (this.currentOffer != null) {
				html = this.currentOffer.getPromotionSet().getPromoOnePageJavaScript1Text().getJavaScript();
			}
		} catch (Exception e) {
			html = "";
		}
		return html;
	}

	public String getOnePageJavaScript2Text() {
		String html = "";
		try {
			if (this.currentOffer != null) {
				html = this.currentOffer.getPromotionSet().getPromoOnePageJavaScript2Text().getJavaScript();
			}
		} catch (Exception e) {
			html = "";
		}
		return html;
	}

	public String getOnePageJavaScript3Text() {
		String html = "";
		try {
			if (this.currentOffer != null) {
				html = this.currentOffer.getPromotionSet().getPromoOnePageJavaScript3Text().getJavaScript();
			}
		} catch (Exception e) {
			html = "";
		}
		return html;
	}

	public String getOnePageJavaScript4Text() {
		String html = "";
		try {
			if (this.currentOffer != null) {
				html = this.currentOffer.getPromotionSet().getPromoOnePageJavaScript4Text().getJavaScript();
			}
		} catch (Exception e) {
			html = "";
		}
		return html;
	}

	public String getOnePageJavaScript5Text() {
		String html = "";
		try {
			if (this.currentOffer != null) {
				html = this.currentOffer.getPromotionSet().getPromoOnePageJavaScript5Text().getJavaScript();
			}
		} catch (Exception e) {
			html = "";
		}
		return html;
	}

	public String getOnePageLeftColumnImageSpot1ImageAltText() {
		String path = "";
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoOnePageLeftColSpot1Image();
				if (image.getImageAltText() != null && image.getImageAltText().length() > 0) {
					path = image.getImageAltText();
				}
			}

		} catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getOnePageLeftColumnImageSpot1ImageOnClickURL() {
		String path = "";
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoOnePageLeftColSpot1Image();
				if (image.getImageLinkToURL() != null && image.getImageLinkToURL().length() > 0) {
					path = image.getImageLinkToURL();
				}
			}

		} catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getOnePageLeftColumnImageSpot1ImagePath() {
		String path = "";
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoOnePageLeftColSpot1Image();
				if (image.getImagePathString() != null && image.getImagePathString().length() > 0) {
					path = image.getImagePathString();
				}
			}

		} catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getOnePageLeftColumnImageSpot2ImageAltText() {
		String path = "";
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoOnePageLeftColSpot2Image();
				if (image.getImageAltText() != null && image.getImageAltText().length() > 0) {
					path = image.getImageAltText();
				}
			}

		} catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getOnePageLeftColumnImageSpot2ImageOnClickURL() {
		String path = "";
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoOnePageLeftColSpot2Image();
				if (image.getImageLinkToURL() != null && image.getImageLinkToURL().length() > 0) {
					path = image.getImageLinkToURL();
				}
			}

		} catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getOnePageLeftColumnImageSpot2ImagePath() {
		String path = "";
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoOnePageLeftColSpot2Image();
				if (image.getImagePathString() != null && image.getImagePathString().length() > 0) {
					path = image.getImagePathString();
				}
			}

		} catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getOnePageLeftColumnVideoSpot1ImageAltText() {
		String path = "";
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoOnePageLeftColVideoSpot1Image();
				if (image.getImageAltText() != null && image.getImageAltText().length() > 0) {
					path = image.getImageAltText();
				}
			}

		} catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getOnePageLeftColumnVideoSpot1ImageOnClickURL() {
		String path = "";
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoOnePageLeftColVideoSpot1Image();
				if (image.getImageLinkToURL() != null && image.getImageLinkToURL().length() > 0) {
					path = image.getImageLinkToURL();
				}
			}

		} catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getOnePageLeftColumnVideoSpot1ImagePath() {
		String path = "";
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoOnePageLeftColVideoSpot1Image();
				if (image.getImagePathString() != null && image.getImagePathString().length() > 0) {
					path = image.getImagePathString();
				}
			}

		} catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getOnePagePromotionText1() {
		String html = "&nbsp;";
		try {
			if (this.currentOffer != null) {
				html = this.currentOffer.getPromotionSet().getPromoOnePageText1().getPromotionalHTML();
			} else {
				SubscriptionOfferIntf o = SubscriptionOfferManager.getInstance().getDefaultOffer(this.product.getBrandingPubCode());
				html = o.getPromotionSet().getPromoOnePageText1().getPromotionalHTML();
			}

		} catch (Exception e) {
			html = "&nbsp;";
		}
		return html;
	}

	public String getOnePageRightColumnHTMLSpot1HTML() {
		String html = "";
		try {
			if (this.currentOffer != null) {
				HTMLPromotionIntf htmlPromo = this.currentOffer.getPromotionSet().getPromoOnePageRightColSpot1HTML();
				html = htmlPromo.getPromotionalHTML();
			}

		} catch (Exception e) {
			html = "";
		}
		return html;
	}

	public String getOnePageRightColumnHTMLSpot2HTML() {
		String html = "";
		try {
			if (this.currentOffer != null) {
				HTMLPromotionIntf htmlPromo = this.currentOffer.getPromotionSet().getPromoOnePageRightColSpot2HTML();
				html = htmlPromo.getPromotionalHTML();
			}

		} catch (Exception e) {
			html = "";
		}
		return html;
	}

	public String getOnePageRightColumnImageSpot1ImageAltText() {
		String path = "";
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoOnePageRightColSpot1Image();
				if (image.getImageAltText() != null && image.getImageAltText().length() > 0) {
					path = image.getImageAltText();
				}
			}

		} catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getOnePageRightColumnImageSpot1ImageOnClickURL() {
		String path = "";
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoOnePageRightColSpot1Image();
				if (image.getImageLinkToURL() != null && image.getImageLinkToURL().length() > 0) {
					path = image.getImageLinkToURL();
				}
			}

		} catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getOnePageRightColumnImageSpot1ImagePath() {
		String path = "";
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoOnePageRightColSpot1Image();
				if (image.getImagePathString() != null && image.getImagePathString().length() > 0) {
					path = image.getImagePathString();
				}
			}

		} catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getOnePageRightColumnImageSpot2ImageAltText() {
		String path = "";
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoOnePageRightColSpot2Image();
				if (image.getImageAltText() != null && image.getImageAltText().length() > 0) {
					path = image.getImageAltText();
				}
			}

		} catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getOnePageRightColumnImageSpot2ImageOnClickURL() {
		String path = "";
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoOnePageRightColSpot2Image();
				if (image.getImageLinkToURL() != null && image.getImageLinkToURL().length() > 0) {
					path = image.getImageLinkToURL();
				}
			}

		} catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getOnePageRightColumnImageSpot2ImagePath() {
		String path = "";
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoOnePageRightColSpot2Image();
				if (image.getImagePathString() != null && image.getImagePathString().length() > 0) {
					path = image.getImagePathString();
				}
			}

		} catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getOnePageRightColumnImageSpot3ImageAltText() {
		String path = "";
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoOnePageRightColSpot3Image();
				if (image.getImageAltText() != null && image.getImageAltText().length() > 0) {
					path = image.getImageAltText();
				}
			}

		} catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getOnePageRightColumnImageSpot3ImageOnClickURL() {
		String path = "";
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoOnePageRightColSpot3Image();
				if (image.getImageLinkToURL() != null && image.getImageLinkToURL().length() > 0) {
					path = image.getImageLinkToURL();
				}
			}

		} catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getOnePageRightColumnImageSpot3ImagePath() {
		String path = "";
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoOnePageRightColSpot3Image();
				if (image.getImagePathString() != null && image.getImagePathString().length() > 0) {
					path = image.getImagePathString();
				}
			}

		} catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getOnePageRightColumnVideoSpot1HTML() {
		String html = "";
		try {
			if (this.currentOffer != null) {
				HTMLPromotionIntf htmlPromo = this.currentOffer.getPromotionSet().getPromoOnePageRightColVideoSpot1HTML();
				html = htmlPromo.getPromotionalHTML();
			}

		} catch (Exception e) {
			html = "";
		}
		return html;
	}

	public String getOnePageRightColumnVideoSpot1ImageAltText() {
		String path = "";
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoOnePageRightColVideoSpot1Image();
				if (image.getImageAltText() != null && image.getImageAltText().length() > 0) {
					path = image.getImageAltText();
				}
			}

		} catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getOnePageRightColumnVideoSpot1ImageOnClickURL() {
		String path = "";
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoOnePageRightColVideoSpot1Image();
				if (image.getImageLinkToURL() != null && image.getImageLinkToURL().length() > 0) {
					path = image.getImageLinkToURL();
				}
			}

		} catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getOnePageRightColumnVideoSpot1ImagePath() {
		String path = "";
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoOnePageRightColVideoSpot1Image();
				if (image.getImagePathString() != null && image.getImagePathString().length() > 0) {
					path = image.getImagePathString();
				}
			}

		} catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getOnePageThankYouLeftColHTMLSpot1() {
		String html = "";
		try {
			if (this.currentOffer != null) {
				html = this.currentOffer.getPromotionSet().getPromoOnePageThankYouLeftColSpot1HTML().getPromotionalHTML();
			}
		} catch (Exception e) {
			html = "";
		}
		return html;
	}

	public String getOnePageThankYouRightColHTMLSpot2() {
		String html = "";
		try {
			if (this.currentOffer != null) {
				html = this.currentOffer.getPromotionSet().getPromoOnePageThankYouRightColSpot2HTML().getPromotionalHTML();
			}
		} catch (Exception e) {
			html = "";
		}
		return html;
	}

	public String getOnePageThankYouLeftColumnImageSpot1ImageAltText() {
		String path = "";
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoOnePageThankYouLeftColSpot1Image();
				if (image.getImageAltText() != null && image.getImageAltText().length() > 0) {
					path = image.getImageAltText();
				}
			}

		} catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getOnePageThankYouLeftColumnImageSpot1ImageOnClickURL() {
		String path = "";
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoOnePageThankYouLeftColSpot1Image();
				if (image.getImageLinkToURL() != null && image.getImageLinkToURL().length() > 0) {
					path = image.getImageLinkToURL();
				}
			}

		} catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getOnePageThankYouLeftColumnImageSpot1ImagePath() {
		String path = "";
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoOnePageThankYouLeftColSpot1Image();
				if (image.getImagePathString() != null && image.getImagePathString().length() > 0) {
					path = image.getImagePathString();
				}
			}

		} catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getOnePageThankYouRightColumnImageSpot2ImageAltText() {
		String path = "";
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoOnePageThankYouRightColSpot2Image();
				if (image.getImageAltText() != null && image.getImageAltText().length() > 0) {
					path = image.getImageAltText();
				}
			}

		} catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getOnePageThankYouRightColumnImageSpot2ImageOnClickURL() {
		String path = "";
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoOnePageThankYouRightColSpot2Image();
				if (image.getImageLinkToURL() != null && image.getImageLinkToURL().length() > 0) {
					path = image.getImageLinkToURL();
				}
			}

		} catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getOnePageThankYouRightColumnImageSpot2ImagePath() {
		String path = "";
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoOnePageThankYouRightColSpot2Image();
				if (image.getImagePathString() != null && image.getImagePathString().length() > 0) {
					path = image.getImagePathString();
				}
			}

		} catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getAccountInfoPageImageSpot1ImageAltText() {
		String path = "";
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoAccountInfoImageSpot1();
				if (image.getImageAltText() != null && image.getImageAltText().length() > 0) {
					path = image.getImageAltText();
				}
			}

		} catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getAccountInfoPageImageSpot1ImageOnClickURL() {
		String path = "";
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoAccountInfoImageSpot1();
				if (image.getImageLinkToURL() != null && image.getImageLinkToURL().length() > 0) {
					path = image.getImageLinkToURL();
				}
			}

		} catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getAccountInfoPageImageSpot1ImagePath() {
		String path = "/images/shim.gif";
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoAccountInfoImageSpot1();
				if (image.getImagePathString() != null && image.getImagePathString().length() > 0) {
					path = image.getImagePathString();
				}
			}

		} catch (Exception e) {
			path = "/images/shim.gif";
		}
		return path;
	}

	public String getAccountInfoPageImageSpot2ImageAltText() {
		String path = "";
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoAccountInfoImageSpot2();
				if (image.getImageAltText() != null && image.getImageAltText().length() > 0) {
					path = image.getImageAltText();
				}
			}

		} catch (Exception e) {
			path = "/images/shim.gif";
		}
		return path;
	}

	public String getAccountInfoPageImageSpot2ImageOnClickURL() {
		String path = "";
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoAccountInfoImageSpot2();
				if (image.getImageLinkToURL() != null && image.getImageLinkToURL().length() > 0) {
					path = image.getImageLinkToURL();
				}
			}

		} catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getAccountInfoPageImageSpot2ImagePath() {
		String path = "/images/shim.gif";
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoAccountInfoImageSpot2();
				if (image.getImagePathString() != null && image.getImagePathString().length() > 0) {
					path = image.getImagePathString();
				}
			}

		} catch (Exception e) {
			path = "/images/shim.gif";
		}
		return path;
	}

	public String getAccountInfoPageImageSpot3ImageAltText() {
		String path = "";
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoAccountInfoImageSpot3();
				if (image.getImageAltText() != null && image.getImageAltText().length() > 0) {
					path = image.getImageAltText();
				}
			}

		} catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getAccountInfoPageImageSpot3ImageOnClickURL() {
		String path = "";
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoAccountInfoImageSpot3();
				if (image.getImageLinkToURL() != null && image.getImageLinkToURL().length() > 0) {
					path = image.getImageLinkToURL();
				}
			}

		} catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getAccountInfoPageImageSpot3ImagePath() {
		String path = "/images/shim.gif";
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoAccountInfoImageSpot3();
				if (image.getImagePathString() != null && image.getImagePathString().length() > 0) {
					path = image.getImagePathString();
				}
			}

		} catch (Exception e) {
			path = "/images/shim.gif";
		}
		return path;
	}

	public String getOnePgLftClSpt1HTM() {
		String html = "";
		try {
			if (this.currentOffer != null) {
				HTMLPromotionIntf htmlPromo = this.currentOffer.getPromotionSet().getPromoOnePageLeftColSpot1HTML();
				html = htmlPromo.getPromotionalHTML();
			}

		} catch (Exception e) {
			html = "";
		}
		return html;
	}

	/**
	 * 
	 * @return Left column video spot HTML
	 */
	public String getonePgLtClVdSpt1HT() {
		String html = "";
		try {
			if (this.currentOffer != null) {
				HTMLPromotionIntf htmlPromo = this.currentOffer.getPromotionSet().getPromoOnePageLeftColVideoSpot1HTML();
				html = htmlPromo.getPromotionalHTML();
			}

		} catch (Exception e) {
			html = "";
		}
		return html;
	}

	public String getOrderPathOverlayPopUpImageLinkURL() {
		String imageURL = null;
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf imagePromo = this.currentOffer.getPromotionSet().getOrderPathPopOverlayPromoImage();

				if (imagePromo != null) {
					imageURL = imagePromo.getImageLinkToURL();
				}

			}

		} catch (Exception e) {
			imageURL = null;
		}
		return imageURL;
	}

	public String getOrderPathOverlayPopUpImagePath() {
		String imagePath = null;
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf imagePromo = this.currentOffer.getPromotionSet().getOrderPathPopOverlayPromoImage();

				if (imagePromo != null) {
					imagePath = imagePromo.getImagePathString();
				}

			}

		} catch (Exception e) {
			imagePath = null;
		}
		return imagePath;
	}

	public ArrayList<SelectItem> getOrderQtySelectItems() {

		ArrayList<SelectItem> items = new ArrayList<SelectItem>();

		try {
			if (this.product != null) {
				for (int i = 1; i <= this.product.getMaxQuantityPerOrder(); i++) {
					SelectItem si = new SelectItem();
					String strVal = String.valueOf(i);

					si.setValue(strVal);
					si.setLabel(strVal);
					items.add(si);
				}
			} else {
				// limit to one
				SelectItem si = new SelectItem();
				si.setDescription("1");
				si.setLabel("1");
				si.setValue("1");
				items.add(si);
			}
		} catch (Exception e) {
			System.out.println("SubscriptionOfferHandler:  Failed to build quantity items: " + e.getMessage());
		}
		return items;
	}

	public SubscriptionOfferIntf getPreviousOffer() {
		return previousOffer;
	}

	public String getProductDescription() {
		String prodDes = "";
		if (product != null) {
			prodDes = product.getDescription();
		}
		return prodDes;
	}

	public String getProductName() {
		String prodName = "None Selected";
		if (product != null) {
			prodName = product.getName();
		}
		return prodName;
	}

	public String getProductSampleURL() {
		String url = "";
		try {
			url = this.product.getSupplier().getSampleURL();
			if (url == null) {
				url = "";
			}
		} catch (Exception e) {
			; // ignore

		}
		return url;
	}

	public String getQuantityLearnMoreHTML() {
		String learnMore = SubscriptionOfferHandler.DEFAULT_UT_BRAND_NUM_COPIES_LEARN_MORE_HTML;
		if (this.product != null && this.product.getMaxQuantityPerOrder() > 1) {

			if (this.product.getBrandingPubCode().equalsIgnoreCase(UsaTodayConstants.BW_PUBCODE)) {
				learnMore = SubscriptionOfferHandler.DEFAULT_BW_BRAND_NUM_COPIES_LEARN_MORE_HTML;
			}

		}
		return learnMore;
	}

	public boolean isRenderLeftColmnImageSpot1ImageAsClickable() {
		boolean clickable = false;
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoOnePageLeftColSpot1Image();
				if (image.getImageLinkToURL() != null && image.getImageLinkToURL().length() > 0) {
					clickable = true;
				}
			}

		} catch (Exception e) {
			clickable = false;
		}

		return clickable;
	}

	public boolean isRenderAccountInfoImageSpot1ImageAsClickable() {
		boolean clickable = false;
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoAccountInfoImageSpot1();
				if (image.getImageLinkToURL() != null && image.getImageLinkToURL().length() > 0) {
					clickable = true;
				}
			}

		} catch (Exception e) {
			clickable = false;
		}

		return clickable;
	}

	public boolean isRenderAccountInfoImageSpot2ImageAsClickable() {
		boolean clickable = false;
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoAccountInfoImageSpot2();
				if (image.getImageLinkToURL() != null && image.getImageLinkToURL().length() > 0) {
					clickable = true;
				}
			}

		} catch (Exception e) {
			clickable = false;
		}

		return clickable;
	}

	public boolean isRenderAccountInfoImageSpot3ImageAsClickable() {
		boolean clickable = false;
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoAccountInfoImageSpot3();
				if (image.getImageLinkToURL() != null && image.getImageLinkToURL().length() > 0) {
					clickable = true;
				}
			}

		} catch (Exception e) {
			clickable = false;
		}

		return clickable;
	}

	public boolean isRenderLeftColmnImageSpot2ImageAsClickable() {
		boolean clickable = false;
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoOnePageLeftColSpot2Image();
				if (image.getImageLinkToURL() != null && image.getImageLinkToURL().length() > 0) {
					clickable = true;
				}
			}

		} catch (Exception e) {
			clickable = false;
		}

		return clickable;
	}

	public boolean isRenderLeftColumnImageSpot1() {
		boolean render = false;
		try {
			if (this.currentOffer != null && this.currentOffer.getPromotionSet().getPromoOnePageLeftColSpot1Image() != null) {
				ImagePromotionIntf p = this.currentOffer.getPromotionSet().getPromoOnePageLeftColSpot1Image();
				if (p.getImagePathString() != null && p.getImagePathString().length() > 0) {
					render = true;
				}
			}

		} catch (Exception e) {
			render = false;
		}
		return render;
	}

	public boolean isRenderLeftColumnImageSpot2() {
		boolean render = false;
		try {
			if (this.currentOffer != null && this.currentOffer.getPromotionSet().getPromoOnePageLeftColSpot2Image() != null) {
				ImagePromotionIntf p = this.currentOffer.getPromotionSet().getPromoOnePageLeftColSpot2Image();
				if (p.getImagePathString() != null && p.getImagePathString().length() > 0) {
					render = true;
				}
			}

		} catch (Exception e) {
			render = false;
		}
		return render;
	}

	public boolean isRenderLeftColumnVideoSpot1() {
		boolean render = false;
		try {
			if (this.currentOffer != null && this.currentOffer.getPromotionSet().getPromoOnePageLeftColVideoSpot1Image() != null) {
				ImagePromotionIntf p = this.currentOffer.getPromotionSet().getPromoOnePageLeftColVideoSpot1Image();
				if (p.getImagePathString() != null && p.getImagePathString().length() > 0) {
					render = true;
				}
			}

		} catch (Exception e) {
			render = false;
		}
		return render;
	}

	public boolean isRenderOnePageThankYouRightColmnImageSpot2ImageAsClickable() {
		boolean clickable = false;
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoOnePageThankYouRightColSpot2Image();
				if (image.getImageLinkToURL() != null && image.getImageLinkToURL().length() > 0) {
					clickable = true;
				}
			}

		} catch (Exception e) {
			clickable = false;
		}

		return clickable;
	}

	public boolean isRenderOnePaqeThankYouRightColumnImageSpot2() {
		boolean render = false;
		try {
			if (this.currentOffer != null
					&& this.currentOffer.getPromotionSet().getPromoOnePageThankYouRightColSpot2Image() != null) {
				ImagePromotionIntf p = this.currentOffer.getPromotionSet().getPromoOnePageThankYouRightColSpot2Image();
				if (p.getImagePathString() != null && p.getImagePathString().length() > 0) {
					render = true;
				}
			}

		} catch (Exception e) {
			render = false;
		}
		return render;
	}

	public boolean isRenderRightColmnImageSpot1ImageAsClickable() {
		boolean clickable = false;
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoOnePageRightColSpot1Image();
				if (image.getImageLinkToURL() != null && image.getImageLinkToURL().length() > 0) {
					clickable = true;
				}
			}

		} catch (Exception e) {
			clickable = false;
		}

		return clickable;
	}

	public boolean isRenderRightColmnImageSpot2ImageAsClickable() {
		boolean clickable = false;
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoOnePageRightColSpot2Image();
				if (image.getImageLinkToURL() != null && image.getImageLinkToURL().length() > 0) {
					clickable = true;
				}
			}

		} catch (Exception e) {
			clickable = false;
		}

		return clickable;
	}

	public boolean isRenderRightColmnImageSpot3ImageAsClickable() {
		boolean clickable = false;
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoOnePageRightColSpot3Image();
				if (image.getImageLinkToURL() != null && image.getImageLinkToURL().length() > 0) {
					clickable = true;
				}
			}

		} catch (Exception e) {
			clickable = false;
		}

		return clickable;
	}

	public boolean isRenderRightColumnImageSpot1() {
		boolean render = false;
		try {
			if (this.currentOffer != null && this.currentOffer.getPromotionSet().getPromoOnePageRightColSpot1Image() != null) {
				ImagePromotionIntf p = this.currentOffer.getPromotionSet().getPromoOnePageRightColSpot1Image();
				if (p.getImagePathString() != null && p.getImagePathString().length() > 0) {
					render = true;
				}
			}

		} catch (Exception e) {
			render = false;
		}
		return render;
	}

	public boolean isRenderRightColumnImageSpot2() {
		boolean render = false;
		try {
			if (this.currentOffer != null && this.currentOffer.getPromotionSet().getPromoOnePageRightColSpot2Image() != null) {
				ImagePromotionIntf p = this.currentOffer.getPromotionSet().getPromoOnePageRightColSpot2Image();
				if (p.getImagePathString() != null && p.getImagePathString().length() > 0) {
					render = true;
				}
			}

		} catch (Exception e) {
			render = false;
		}
		return render;
	}

	public boolean isRenderRightColumnImageSpot3() {
		boolean render = false;
		try {

			if (this.currentOffer != null && this.currentOffer.getPromotionSet().getPromoOnePageRightColSpot3Image() != null) {
				ImagePromotionIntf p = this.currentOffer.getPromotionSet().getPromoOnePageRightColSpot3Image();
				if (p.getImagePathString() != null && p.getImagePathString().length() > 0) {
					render = true;
				}
			}

		} catch (Exception e) {
			render = false;
		}
		return render;
	}

	public boolean isRenderRightColumnVideoSpot1() {
		boolean render = false;
		try {
			if (this.currentOffer != null && this.currentOffer.getPromotionSet().getPromoOnePageRightColVideoSpot1Image() != null) {
				ImagePromotionIntf p = this.currentOffer.getPromotionSet().getPromoOnePageRightColVideoSpot1Image();
				if (p.getImagePathString() != null && p.getImagePathString().length() > 0) {
					render = true;
				}
			}

		} catch (Exception e) {
			render = false;
		}
		return render;
	}

	public boolean isRenderAccountInformationSpot1() {
		boolean render = false;
		try {
			if (this.currentOffer != null && this.currentOffer.getPromotionSet().getPromoAccountInfoImageSpot1() != null) {
				ImagePromotionIntf p = this.currentOffer.getPromotionSet().getPromoAccountInfoImageSpot1();
				if (p.getImagePathString() != null && p.getImagePathString().length() > 0) {
					render = true;
				}
			}

		} catch (Exception e) {
			render = false;
		}
		return render;
	}

	// TODO: Change this
	public boolean isRenderAccountInformationSpot2() {
		boolean render = false;
		try {
			if (this.currentOffer != null && this.currentOffer.getPromotionSet().getPromoAccountInfoImageSpot2() != null) {
				ImagePromotionIntf p = this.currentOffer.getPromotionSet().getPromoAccountInfoImageSpot2();
				if (p.getImagePathString() != null && p.getImagePathString().length() > 0) {
					render = true;
				}
			}

		} catch (Exception e) {
			render = false;
		}
		return render;
	}

	// TODO: change this
	public boolean isRenderAccountInformationSpot3() {
		boolean render = false;
		try {
			if (this.currentOffer != null && this.currentOffer.getPromotionSet().getPromoAccountInfoImageSpot3() != null) {
				ImagePromotionIntf p = this.currentOffer.getPromotionSet().getPromoAccountInfoImageSpot3();
				if (p.getImagePathString() != null && p.getImagePathString().length() > 0) {
					render = true;
				}
			}

		} catch (Exception e) {
			render = false;
		}
		return render;
	}

	public boolean isShowDeliveryMethodCheck() {
		boolean showDelCheck = false;

		if (AS400CurrentStatus.getJdbcActive() && this.currentOffer != null
				&& this.currentOffer.getSubscriptionProduct().getProductCode().equalsIgnoreCase("UT")) {

			// default to show it if not EE
			showDelCheck = true;

			PromotionSet currentOfferPromotionSet = currentOffer.getPromotionSet();

			if (currentOfferPromotionSet != null && currentOfferPromotionSet.getDeliveryNotification() != null
					&& currentOfferPromotionSet.getDeliveryNotification().getFulfillText() != null
					&& !currentOfferPromotionSet.getDeliveryNotification().getFulfillText().trim().equals("")) {
				if (currentOfferPromotionSet.getDeliveryNotification().getFulfillText().equals("ON")) {
					showDelCheck = true;
				} else {
					showDelCheck = false;
				}
			}

		}
		return showDelCheck;
	}

	public boolean isShowPreviousOfferRevertLink() {
		if (this.previousOffer != null) {
			return true;
		}
		return false;
	}

	public void setCurrentOffer(SubscriptionOfferIntf cOffer) {

		// hdcons-153 change rates

		/*
		 * old call before Genesys if (cOffer != null && this.currentOffer != null &&
		 * !cOffer.getKeyCode().equalsIgnoreCase(this.currentOffer.getKeyCode())) { this.previousOffer = this.currentOffer; }
		 */

		if (cOffer != null && this.currentOffer != null && !cOffer.getKeyCode().equalsIgnoreCase(this.currentOffer.getKeyCode())) {
			this.previousOffer = this.currentOffer;
		}

		this.currentOffer = cOffer;
		if (cOffer != null) {
			try {
				// if (product == null || !product.getProductCode().equalsIgnoreCase(cOffer.getPubCode())){
				product = SubscriptionProductBO.getSubscriptionProduct(cOffer.getPubCode());
				// }
			} catch (Exception e) {
				product = null;
			}
		} else {
			product = null;
		}

	}

	public void setEarliestPossibleStartDate(Date d) {
		;// ignore
	}

	public void setIsForceBillMe(boolean value) {
		; // ignore, only here to appease JSF
	}

	public void setIsShowOrderPathOverlayPopUp(boolean newValue) {
		; // ignore, no update allowed
	}

	public void setLatestPossibleStartDate(Date d) {
		;// ignore
	}

	public void setPreviousOffer(SubscriptionOfferIntf previousOffer) {
		this.previousOffer = previousOffer;
	}

	public boolean isRenderOnePageThankYouLeftColmnImageSpot1ImageAsClickable() {
		boolean clickable = false;
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoOnePageThankYouLeftColSpot1Image();
				if (image.getImageLinkToURL() != null && image.getImageLinkToURL().length() > 0) {
					clickable = true;
				}
			}

		} catch (Exception e) {
			clickable = false;
		}

		return clickable;
	}

	public boolean isRenderOnePaqeThankYouLeftColumnImageSpot1() {
		boolean render = false;
		try {
			if (this.currentOffer != null && this.currentOffer.getPromotionSet().getPromoOnePageThankYouLeftColSpot1Image() != null) {
				ImagePromotionIntf p = this.currentOffer.getPromotionSet().getPromoOnePageThankYouLeftColSpot1Image();
				if (p.getImagePathString() != null && p.getImagePathString().length() > 0) {
					render = true;
				}
			}

		} catch (Exception e) {
			render = false;
		}
		return render;
	}

	public String getOnePageThankYouRightColHTMLSpot1() {
		String html = "";
		try {
			if (this.currentOffer != null) {
				html = this.currentOffer.getPromotionSet().getPromoOnePageThankYouRightColSpot1HTML().getPromotionalHTML();
			}
		} catch (Exception e) {
			html = "";
		}
		return html;
	}

	public String getOnePageThankYouRightColumnImageSpot1ImageAltText() {
		String path = "";
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoOnePageThankYouRightColSpot1Image();
				if (image.getImageAltText() != null && image.getImageAltText().length() > 0) {
					path = image.getImageAltText();
				}
			}

		} catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getOnePageThankYouRightColumnImageSpot1ImageOnClickURL() {
		String path = "";
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoOnePageThankYouRightColSpot1Image();
				if (image.getImageLinkToURL() != null && image.getImageLinkToURL().length() > 0) {
					path = image.getImageLinkToURL();
				}
			}

		} catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getOnePageThankYouRightColumnImageSpot1ImagePath() {
		String path = "";
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoOnePageThankYouRightColSpot1Image();
				if (image.getImagePathString() != null && image.getImagePathString().length() > 0) {
					path = image.getImagePathString();
				}
			}

		} catch (Exception e) {
			path = "";
		}
		return path;
	}

	public boolean isRenderOnePageThankYouRightColmnImageSpot1ImageAsClickable() {
		boolean clickable = false;
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf image = this.currentOffer.getPromotionSet().getPromoOnePageThankYouRightColSpot1Image();
				if (image.getImageLinkToURL() != null && image.getImageLinkToURL().length() > 0) {
					clickable = true;
				}
			}

		} catch (Exception e) {
			clickable = false;
		}

		return clickable;
	}

	public boolean isRenderOnePaqeThankYouRightColumnImageSpot1() {
		boolean render = false;
		try {
			if (this.currentOffer != null
					&& this.currentOffer.getPromotionSet().getPromoOnePageThankYouRightColSpot1Image() != null) {
				ImagePromotionIntf p = this.currentOffer.getPromotionSet().getPromoOnePageThankYouRightColSpot1Image();
				if (p.getImagePathString() != null && p.getImagePathString().length() > 0) {
					render = true;
				}
			}

		} catch (Exception e) {
			render = false;
		}
		return render;
	}

	public String getWhatsThisHTML() {
		String whatsThis = SubscriptionOfferHandler.WHAT_THIS_HTML;

		return whatsThis;
	}

	public String getEZPayFreeWeeksOffer() {
		String numWeeks = "0";
		try {
			if (this.currentOffer != null) {
				PromotionIntf promo = this.currentOffer.getPromotionSet().getEZPayFreeWeeks();

				if (promo != null && promo.getFulfillText().length() > 0) {
					numWeeks = promo.getFulfillText().trim();
				}
			}

		} catch (Exception e) {
			numWeeks = "0";
		}
		return numWeeks;
	}

	public void setEZPayFreeWeeksOffer(String weeks) {
		; // unsettable
	}

	public String getEZPayFreeWeeksOfferText() {
		String ezpayText = " ";
		try {
			if (this.currentOffer != null) {
				PromotionIntf promo = this.currentOffer.getPromotionSet().getEZPayFreeWeeks();

				if (promo != null && promo.getFulfillText().length() > 0) {
					ezpayText = promo.getFulfillText().trim();
//					ezpayText = "I will get an additional " + promo.getFulfillText().trim()
//							+ "FREE weeks included in the price of my subscription!";
				}
			}
		} catch (Exception e) {
			ezpayText = " ";
		}
		return ezpayText;
	}

	public boolean isEzPayFreeWeeks() {
		boolean freeWeeks = false;
		try {
			if (this.currentOffer != null) {
				PromotionIntf promo = this.currentOffer.getPromotionSet().getEZPayFreeWeeks();

				if (promo != null && promo.getFulfillText().length() > 0) {
					freeWeeks = true;
				}
			}
		} catch (Exception e) {
			freeWeeks = false;
		}
		return freeWeeks;
	}

	public String getTermsDescriptionTextEzpayHTML(String piaRateCode) {
		String html = "";
		try {
			if (this.currentOffer != null) {
				HTMLPromotionIntf htmlPromo = this.currentOffer.getPromotionSet().getTermsDescriptionTextEzpay();
				// Check for populated or blanks rate code
				// if (htmlPromo.getFulfillUrl().trim().equals(piaRateCode) || htmlPromo.getFulfillUrl() == null ||
				// htmlPromo.getFulfillUrl().trim().equals("")) {
				html = htmlPromo.getPromotionalHTML();
				// }
			}

		} catch (Exception e) {
			html = "";
		}
		return html;
	}

	public String getTermsDescriptionTextHTML(String piaRateCode) {
		String html = "";
		try {
			if (this.currentOffer != null) {
				HTMLPromotionIntf htmlPromo = this.currentOffer.getPromotionSet().getTermsDescriptionText();
				// Check for populated or blanks rate code
				// if (htmlPromo.getFulfillUrl().trim().equals(piaRateCode) || htmlPromo.getFulfillUrl() == null ||
				// htmlPromo.getFulfillUrl().trim().equals("")) {
				html = htmlPromo.getPromotionalHTML();
				// }
			}

		} catch (Exception e) {
			html = "";
		}
		return html;
	}
}
