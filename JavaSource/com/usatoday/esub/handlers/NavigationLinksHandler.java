package com.usatoday.esub.handlers;

public class NavigationLinksHandler {

	private boolean showNavHeaderArea1 = false;
	private boolean showNavHeaderArea2 = false;

	private boolean showUpperNavGrid = false;
	private boolean showLowerNavGrid = false;

	public boolean isShowNavHeaderArea1() {
		return showNavHeaderArea1;
	}

	public boolean getShowNavHeaderArea1() {
		return showNavHeaderArea1;
	}

	public void setShowNavHeaderArea1(boolean showNavHeaderArea1) {
		this.showNavHeaderArea1 = showNavHeaderArea1;
	}

	public boolean isShowNavHeaderArea2() {
		return showNavHeaderArea2;
	}

	public boolean getShowNavHeaderArea2() {
		return showNavHeaderArea2;
	}

	public void setShowNavHeaderArea2(boolean showNavHeaderArea2) {
		this.showNavHeaderArea2 = showNavHeaderArea2;
	}

	public boolean isShowUpperNavGrid() {
		return showUpperNavGrid;
	}

	public void setShowUpperNavGrid(boolean showUpperNavGrid) {
		this.showUpperNavGrid = showUpperNavGrid;
	}

	public boolean isShowLowerNavGrid() {
		return showLowerNavGrid;
	}

	public void setShowLowerNavGrid(boolean showLowerNavGrid) {
		this.showLowerNavGrid = showLowerNavGrid;
	}

}
