package com.usatoday.esub.common;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.*;
import com.usatoday.UsatException;

import com.usatoday.util.constants.UsaTodayConstants;

public class BWPubDates extends PublicationDates {

	/**
	 * UTLogin constructor comment.
	 */
	public BWPubDates() {
		super();
		this.setPubCode(UTCommon.BWPUBCODE);
	}

	/**
	 * UTLogin constructor comment.
	 */
	public BWPubDates(String publication, String begin, String end) {
		super(publication, begin, end);
	}

	public String addStartDate(String pub, String begin_date) {
		String date = begin_date;

		HashMap<String, String> datemap = PublicationDates.getDateFields(date);

		String year = (String) datemap.get("YEAR");
		String month = (String) datemap.get("MONTH");
		String day = (String) datemap.get("DAY");

		// set calendar
		GregorianCalendar startingDate = new GregorianCalendar();

		startingDate.set(Integer.parseInt(year), (Integer.parseInt(month) - 1), Integer.parseInt(day));

		startingDate.add(Calendar.DATE, 1);

		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		String startPubDateNew = sdf.format(startingDate.getTime());

		return startPubDateNew;

	}

	public String calcEndDate() throws com.usatoday.UsatException {
		boolean foundFlag = false;
		String endPubCode = this.getPubCode();
		String endPubDate = this.getEnd_date();
		String newEndDate = null;

		// cycle backwards through dates until valid pubdate found
		if (!this.pastMaxDate(endPubDate)) {
			while (!foundFlag) {
				newEndDate = getEndDate(endPubDate);

				if (validPubDate(endPubCode, newEndDate)) {
					foundFlag = true;
				} else {
					endPubDate = newEndDate;
				}
			}
		} else {
			// throw an exception
			throw new UsatException(
					"BWPubDates::calcEndDate() - Cannot calculate end date because it is past the maximum supported date. Date requested: "
							+ this.getEnd_date());
		}

		return newEndDate;

	}

	public String calcNextStartDate() {

		String next_start_date = null;
		String days_out = getDaysOut();

		Connection connection = null;
		try {
			// get a connection to the database
			connection = DataSourceConnectionFactory.getDBConnection();

			// create a prepared statement and substitute the parameters
			PreparedStatement statement = connection.prepareStatement(JDBC_PUBCALC);
			statement.setString(1, this.getPubCode());
			statement.setString(2, days_out);

			statement.execute();

			// get the result
			ResultSet resultSet = statement.getResultSet();
			boolean resultFound = false;
			while (resultSet.next()) {
				resultFound = true;

				String date = resultSet.getString("PubDate");
				HashMap<String, String> datemap = UTPubDates.getDateFields(date);

				String year = (String) datemap.get("YEAR");
				String month = (String) datemap.get("MONTH");
				String day = (String) datemap.get("DAY");

				next_start_date = month + "/" + day + "/" + year;
				break;

			}

			statement.close();

			// inspect the result
			if (!resultFound) {
				// PubCode/PubDate not valid
				return next_start_date;
			} else {
				return next_start_date;
			}
		} catch (SQLException e) {
			return next_start_date;
		} catch (UsatDBConnectionException e) {
			return next_start_date;
		} finally {
			// close the connection resource
			DataSourceConnectionFactory.closeConnection(connection);
		}
	}

	public String calcStartDate() throws UsatException {
		boolean foundFlag = false;
		boolean searchFlag = false;
		String startPubCode = this.getPubCode();
		String startPubDate = this.getPubDate();

		String newStartDate = null;

		String date = this.getPubDate(); // given pubdate

		HashMap<String, String> datemap = PublicationDates.getDateFields(date);

		String year = (String) datemap.get("YEAR");
		String month = (String) datemap.get("MONTH");
		String day = (String) datemap.get("DAY");

		// set calendar
		GregorianCalendar oldStartingDate = new GregorianCalendar();

		oldStartingDate.set(Integer.parseInt(year), (Integer.parseInt(month) - 1), Integer.parseInt(day));

		String date_new = this.getNew_pubdate(); // calculated pubdate

		HashMap<String, String> datemap_new = UTPubDates.getDateFields(date_new);

		String year_new = (String) datemap_new.get("YEAR");
		String month_new = (String) datemap_new.get("MONTH");
		String day_new = (String) datemap_new.get("DAY");

		// set calendar
		GregorianCalendar newStartingDate = new GregorianCalendar();

		newStartingDate.set(Integer.parseInt(year_new), (Integer.parseInt(month_new) - 1), Integer.parseInt(day_new));

		if (newStartingDate.after(oldStartingDate)) {

			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			newStartDate = sdf.format(newStartingDate.getTime());
		} else {
			if (validPubDate(startPubCode, startPubDate)) {
				newStartDate = startPubDate;
			} else {
				// cycle forward through dates until valid pubdate found
				while (!foundFlag) {
					newStartDate = startPubDate;
					if (validPubDate(startPubCode, newStartDate)) {
						foundFlag = true;
					} else {
						if (!this.pastMaxDate(newStartDate)) {
							while (!searchFlag) {
								startPubDate = addStartDate(startPubCode, newStartDate);
								if (validPubDate(startPubCode, startPubDate)) {
									newStartDate = startPubDate;
									searchFlag = true;
									foundFlag = true;
								} else {
									newStartDate = startPubDate;
								}
							}
						} else {
							// invalid date specified
							newStartDate = null;
							foundFlag = true;
							throw new UsatException(
									"BWPubDates::calcStartDate() - Cannot calculate start date because it is past the maximum supported date. Date requested: "
											+ newStartDate);
						}
					}
				}
			}
		}
		return newStartDate;

	}

	public String getDaysOut() {

		String nextStartDate = null;
		GregorianCalendar gcDate = new GregorianCalendar();

		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		String currentDate = sdf.format(gcDate.getTime());

		Connection connection = null;
		try {
			// get a connection to the database
			connection = DataSourceConnectionFactory.getDBConnection();

			// create a prepared statement and substitute the parameters
			PreparedStatement statement = connection.prepareStatement(JDBC_PUBCALC);
			statement.setString(1, UTCommon.UTPUBCODE);
			statement.setString(2, currentDate);

			// execute the SQL
			statement.execute();

			// get the result
			ResultSet resultSet = statement.getResultSet();
			boolean resultFound = false;

			// Calculate Start Date using PUB_DAYS_OUT SEW
			int count = 1;

			while (resultSet.next()) {

				if (count == UTCommon.PUB_DAYS_OUT) {

					do {
						resultFound = true;

						String date = resultSet.getString("PubDate");
						HashMap<String, String> datemap = UTPubDates.getDateFields(date);

						String year = (String) datemap.get("YEAR");
						String month = (String) datemap.get("MONTH");
						String day = (String) datemap.get("DAY");

						nextStartDate = month + "/" + day + "/" + year;
						break;
					} while (!validPubDate());
					break;
				}

				count = count + 1;
			}

			statement.close();

			// inspect the result
			if (!resultFound) {
				return nextStartDate;
			} else {
				return nextStartDate;
			}

		} catch (SQLException e) {
			return nextStartDate;

		} catch (UsatDBConnectionException e) {
			return nextStartDate;
		} finally {
			// close the connection resource
			DataSourceConnectionFactory.closeConnection(connection);
		}

	}

	public String getStartDate(String pub, String start_date) {
		String date = start_date;

		HashMap<String, String> datemap = PublicationDates.getDateFields(date);

		String year = (String) datemap.get("YEAR");
		String month = (String) datemap.get("MONTH");
		String day = (String) datemap.get("DAY");

		// set calendar
		GregorianCalendar startingDate = new GregorianCalendar();

		startingDate.set(Integer.parseInt(year), (Integer.parseInt(month) - 1), Integer.parseInt(day));

		int count = 0;

		while (count != UTCommon.PUB_DAYS_OUT) {
			int dayOfWeek = startingDate.get(Calendar.DAY_OF_WEEK);

			if (dayOfWeek == Calendar.SATURDAY) {
				startingDate.add(Calendar.DATE, 2);
			} else if (dayOfWeek == Calendar.SUNDAY) {
				startingDate.add(Calendar.DATE, 1);
			} else {
				startingDate.add(Calendar.DATE, 1);
				count = count + 1;
			}
		}
		int startPubDay = startingDate.get(Calendar.DATE);
		int startPubMth = startingDate.get(Calendar.MONTH);
		int startPubYear = startingDate.get(Calendar.YEAR);

		String startPubDateNew = ((startPubMth + 1) + "/" + startPubDay + "/" + startPubYear);

		return startPubDateNew;

	}

	public String calcNextVacStartDate() {

		String nextStartDate = null;
		// Set current date
		GregorianCalendar gcDate = new GregorianCalendar();

		int startPubDay = gcDate.get(Calendar.DATE);
		int startPubMth = gcDate.get(Calendar.MONTH);
		int startPubYear = gcDate.get(Calendar.YEAR);

		String currentDate = ((startPubMth + 1) + "/" + startPubDay + "/" + startPubYear);

		Connection connection = null;
		try {
			// get a connection to the database
			connection = DataSourceConnectionFactory.getDBConnection();

			// create a prepared statement and substitute the parameters
			PreparedStatement statement = connection.prepareStatement(JDBC_PUBCALC);
			statement.setString(1, this.getPubCode());
			statement.setString(2, currentDate);

			// execute the SQL
			statement.execute();

			// get the result
			ResultSet resultSet = statement.getResultSet();
			boolean resultFound = false;

			// Calculate Start Date using PUB_DAYS_OUT SEW
			int count = 1;
			while (resultSet.next()) {
				if (count == UTCommon.PUB_BW_VAC_DAYS_OUT) {
					resultFound = true;

					String date = resultSet.getString("PubDate");
					HashMap<String, String> datemap = UTPubDates.getDateFields(date);

					String year = (String) datemap.get("YEAR");
					String month = (String) datemap.get("MONTH");
					String day = (String) datemap.get("DAY");

					nextStartDate = month + "/" + day + "/" + year;
					break;

				}

				count = count + 1;
			}

			// close the database connection
			statement.close();
			connection.close();

			// inspect the result
			if (!resultFound) {
				return nextStartDate;
			} else {
				return nextStartDate;
			}

		} catch (SQLException e) {
			return nextStartDate;

		} catch (UsatDBConnectionException e) {
			return nextStartDate;
		} finally {
			DataSourceConnectionFactory.closeConnection(connection);
		}
	}

	@Override
	public int getMaxDaysInFuture() {
		return UsaTodayConstants.MAX_DAYS_IN_FUTURE_FOR_SW_BRAND_SUBSCRIPTION_START;
	}

}
