package com.usatoday.esub.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;

import com.usatoday.UsatException;
import com.usatoday.util.constants.UsaTodayConstants;

public class UTPubDates extends PublicationDates {

	/**
	 * UTLogin constructor comment.
	 */
	public UTPubDates() {
		super();
		this.setPubCode(UTCommon.UTPUBCODE);
	}

	/**
	 * UTLogin constructor comment.
	 */
	public UTPubDates(String publication, String begin, String end) {
		super(publication, begin, end);
	}

	public String calcEndDate() throws UsatException {
		boolean foundFlag = false;
		String endPubCode = this.getPubCode();
		String endPubDate = this.getEnd_date();
		String newEndDate = null;

		// cycle backwards through dates until valid pubdate found
		if (!this.pastMaxDate(endPubDate)) {
			while (!foundFlag) {
				newEndDate = getEndDate(endPubDate);

				if (validPubDate(endPubCode, newEndDate)) {
					foundFlag = true;
				} else {
					endPubDate = newEndDate;
				}
			}
		} else {
			// throw an exception
			throw new UsatException(
					"UTPubDates::calcEndDate() - Cannot calculate end date because it is past the maximum supported date. Date requested: "
							+ this.getEnd_date());
		}

		return newEndDate;

	}

	public String calcNextVacStartDate() {

		String nextStartDate = null;
		// Set current date
		GregorianCalendar gcDate = new GregorianCalendar();

		int startPubDay = gcDate.get(Calendar.DATE);
		int startPubMth = gcDate.get(Calendar.MONTH);
		int startPubYear = gcDate.get(Calendar.YEAR);

		String currentDate = ((startPubMth + 1) + "/" + startPubDay + "/" + startPubYear);

		Connection connection = null;
		try {
			// get a connection to the database
			connection = DataSourceConnectionFactory.getDBConnection();

			// create a prepared statement and substitute the parameters
			PreparedStatement statement = connection.prepareStatement(JDBC_PUBCALC);
			statement.setString(1, this.getPubCode());
			statement.setString(2, currentDate);

			// execute the SQL
			statement.execute();

			// get the result
			ResultSet resultSet = statement.getResultSet();
			boolean resultFound = false;

			// Calculate Start Date using PUB_DAYS_OUT SEW
			int count = 1;
			while (resultSet.next()) {

				if (count == UTCommon.PUB_UT_VAC_DAYS_OUT) {
					resultFound = true;

					String date = resultSet.getString("PubDate");
					HashMap<String, String> datemap = UTPubDates.getDateFields(date);

					String year = (String) datemap.get("YEAR");
					String month = (String) datemap.get("MONTH");
					String day = (String) datemap.get("DAY");

					nextStartDate = month + "/" + day + "/" + year;
					break;

				}

				count = count + 1;
			}

			// close the database connection
			statement.close();
			connection.close();

			// inspect the result
			if (!resultFound) {
				return nextStartDate;
			} else {
				return nextStartDate;
			}

		} catch (SQLException e) {
			return nextStartDate;

		} catch (UsatDBConnectionException e) {
			return nextStartDate;
		} finally {
			DataSourceConnectionFactory.closeConnection(connection);
		}
	}

	public String calcStartDate() throws UsatException {
		// get parms
		String publication = this.getPubCode();
		String oldPubDate = this.getPubDate();
		String startPubDate = null;
		String newStartDate = null;
		boolean foundFlag = false;
		boolean searchFlag = false;

		String date = this.getPubDate();

		HashMap<String, String> datemap = PublicationDates.getDateFields(date);

		String year = (String) datemap.get("YEAR");
		String month = (String) datemap.get("MONTH");
		String day = (String) datemap.get("DAY");

		// set calendar
		GregorianCalendar oldStartingDate = new GregorianCalendar();

		oldStartingDate.set(Integer.parseInt(year), (Integer.parseInt(month) - 1), Integer.parseInt(day));

		String date_new = this.getNew_pubdate();

		HashMap<String, String> datemap_new = PublicationDates.getDateFields(date_new);

		String year_new = (String) datemap_new.get("YEAR");
		String month_new = (String) datemap_new.get("MONTH");
		String day_new = (String) datemap_new.get("DAY");

		// set calendar
		GregorianCalendar newStartingDate = new GregorianCalendar();

		newStartingDate.set(Integer.parseInt(year_new), (Integer.parseInt(month_new) - 1), Integer.parseInt(day_new));

		if (newStartingDate.after(oldStartingDate)) {

			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			newStartDate = sdf.format(newStartingDate.getTime());

		} else {

			while (!foundFlag) {

				newStartDate = oldPubDate;
				if (validPubDate(publication, newStartDate)) {
					foundFlag = true;
				} else {
					if (!this.pastMaxDate(newStartDate)) {
						while (!searchFlag) {
							startPubDate = addStartDate(publication, newStartDate);
							if (validPubDate(publication, startPubDate)) {
								newStartDate = startPubDate;
								searchFlag = true;
								foundFlag = true;
							} else {
								newStartDate = startPubDate;
							}
						}
					} else {
						// invalid date specified
						newStartDate = null;
						foundFlag = true;
						throw new UsatException(
								"UTPubDates::calcStartDate() - Cannot calculate start date because it is past the maximum supported date. Date requested: "
										+ newStartDate);
					}
				}
			}
			// format start date
			datemap_new = PublicationDates.getDateFields(newStartDate);

			year_new = (String) datemap_new.get("YEAR");
			month_new = (String) datemap_new.get("MONTH");
			day_new = (String) datemap_new.get("DAY");

			// set calendar
			newStartingDate = new GregorianCalendar();

			newStartingDate.set(Integer.parseInt(year_new), (Integer.parseInt(month_new) - 1), Integer.parseInt(day_new));

			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			newStartDate = sdf.format(newStartingDate.getTime());
		}

		return newStartDate;

	}

	/**
	 * 
	 * @return string
	 */
	public String calcNextStartDate() {

		String nextStartDate = null;
		// Set current date
		GregorianCalendar gcDate = new GregorianCalendar();

		int startPubDay = gcDate.get(Calendar.DATE);
		int startPubMth = gcDate.get(Calendar.MONTH);
		int startPubYear = gcDate.get(Calendar.YEAR);

		String currentDate = ((startPubMth + 1) + "/" + startPubDay + "/" + startPubYear);

		Connection connection = null;
		try {
			// get a connection to the database
			connection = DataSourceConnectionFactory.getDBConnection();

			// create a prepared statement and substitute the parameters
			PreparedStatement statement = connection.prepareStatement(JDBC_PUBCALC);
			statement.setString(1, this.getPubCode());
			statement.setString(2, currentDate);

			// execute the SQL
			statement.execute();

			// get the result
			ResultSet resultSet = statement.getResultSet();
			boolean resultFound = false;

			// Calculate Start Date using PUB_DAYS_OUT SEW
			int count = 1;

			while (resultSet.next()) {

				if (count == UTCommon.PUB_DAYS_OUT) {
					resultFound = true;

					String date = resultSet.getString("PubDate");
					HashMap<String, String> datemap = UTPubDates.getDateFields(date);

					String year = (String) datemap.get("YEAR");
					String month = (String) datemap.get("MONTH");
					String day = (String) datemap.get("DAY");

					nextStartDate = month + "/" + day + "/" + year;
					break;

				}

				count = count + 1;
			}

			// close the database connection
			statement.close();
			connection.close();

			// inspect the result
			if (!resultFound) {
				return nextStartDate;
			} else {
				return nextStartDate;
			}

		} catch (SQLException e) {
			return nextStartDate;

		} catch (UsatDBConnectionException e) {
			return nextStartDate;
		} finally {
			DataSourceConnectionFactory.closeConnection(connection);
		}

	}

	@Override
	public int getMaxDaysInFuture() {
		return UsaTodayConstants.MAX_DAYS_IN_FUTURE_FOR_UT_BRAND_SUBSCRIPTION_START;
	}

}
