package com.usatoday.esub.common;

// 
// This servlet captures the FEEDBACK data.
//
//

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import com.usatoday.business.interfaces.products.SubscriptionProductIntf;
import com.usatoday.businessObjects.products.SubscriptionProductBO;
import com.usatoday.util.constants.UsaTodayConstants;

public class FeedbackServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2155147609635444182L;
	public static final String SUCCESS_URL = "SUCCESSURL";
	public static final String FAILURE_URL = "FAILUREURL";
	public static final String SELECT_URL = "SELECTURL";
	public static final String FIRST_NAME = "FIRST_NAME";
	public static final String LAST_NAME = "LAST_NAME";
	public static final String ACCOUNT_NUMBER = "ACCOUNT_NUMBER";
	public static final String LOGIN_ID = "LOGIN_ID";
	public static final String COMPANY_NAME = "COMPANY_NAME";
	public static final String DAY_NUMBER = "DAY_NUMBER";
	public static final String MESSAGE_BOX = "MESSAGE_BOX";
	public static final String CATEGORY = "CATEGORY";
	public static final String PUBLICATION_NAMES[] = { "USA TODAY", "Sports Weekly" };

	/**
	 * LoginServlet constructor comment.
	 */
	public FeedbackServlet() {
		super();
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		// get the session object; create a session if none exists
		HttpSession session = req.getSession(true);

		boolean userForwarded = false;
		boolean ncsSent = false;
		boolean devSent = false;

		// set content type and other response header fields
		UTCommon.setResponseHeader(res);

		// retrieve the URLs
		String successURL = UTCommon.getParameterAsString(req, SUCCESS_URL);
		String failureURL = UTCommon.getParameterAsString(req, FAILURE_URL);

		if (successURL == null) {
			successURL = UTCommon.BASE_URL + "/feedback/confirm.jsp";
		}
		if (failureURL == null) {
			failureURL = UTCommon.BASE_URL + "/feedback/error.jsp";
		}

		StringBuffer emailMessage = new StringBuffer();
		String daynumber = null;
		String company = null;
		String emailid = null;
		String publication = null;
		String category = null;
		String accountnumber = null;
		StringBuffer codedValue = new StringBuffer("\n\n");

		// user not logged in; retrieve the input parameters
		try {
			String firstname = UTCommon.getParameterAsString(req, FeedbackServlet.FIRST_NAME).trim().toLowerCase();
			String lastname = UTCommon.getParameterAsString(req, FeedbackServlet.LAST_NAME).trim().toLowerCase();

			try {
				accountnumber = UTCommon.getParameterAsString(req, FeedbackServlet.ACCOUNT_NUMBER).trim().toLowerCase();
			} catch (NullPointerException e) {
				accountnumber = "";
			}

			try {
				company = UTCommon.getParameterAsString(req, FeedbackServlet.COMPANY_NAME).trim().toLowerCase();
			} catch (NullPointerException e) {
				company = " ";
			}

			emailid = UTCommon.getParameterAsString(req, FeedbackServlet.LOGIN_ID).trim().toLowerCase();
			try {
				daynumber = UTCommon.getParameterAsString(req, FeedbackServlet.DAY_NUMBER).trim().toLowerCase();
			} catch (NullPointerException e) {
				daynumber = " ";
			}

			String messagebox = UTCommon.getParameterAsString(req, FeedbackServlet.MESSAGE_BOX).trim();

			publication = (String) session.getAttribute(UTCommon.SESSION_PUB_NAME);

			SubscriptionProductIntf product = null;
			SubscriptionProductIntf brandingProduct = null;
			// default pub if not set
			if (publication == null) {
				publication = "UT";
			} else {
				try {
					product = SubscriptionProductBO.getSubscriptionProduct(publication);
					if (product == null) {
						product = SubscriptionProductBO.getUsaTodaySubscriptionProduct();
					}
					brandingProduct = SubscriptionProductBO.getSubscriptionProduct(product.getBrandingPubCode());
				} catch (Exception e) {
					;
				}
			}

			if (brandingProduct != null && brandingProduct.getProductCode().equalsIgnoreCase(UsaTodayConstants.BW_PUBCODE)) {
				codedValue.append("BBW - SB"); // Sports Weekly Code
			} else {
				codedValue.append("USAT - SB");
			}

			if (product != null) {
				publication = product.getName();
			} else {
				publication = "USA TODAY";
			}

			try {
				category = UTCommon.getParameterAsString(req, FeedbackServlet.CATEGORY).trim();
				codedValue.append("\n").append(category);
				if (category.equalsIgnoreCase("SB.SB")) {
					category = "Subscribe";
				} else if (category.equalsIgnoreCase("SB.RS")) {
					category = "Renew Subscription";
				} else if (category.equalsIgnoreCase("SB.DE")) {
					category = "I want to deliver publication";
				} else if (category.equalsIgnoreCase("SB.VH")) {
					category = "Vacation Hold";
				} else if (category.equalsIgnoreCase("SB.CA")) {
					category = "Change Address";
				} else if (category.equalsIgnoreCase("SB.ED")) {
					category = "Expiration Date Request";
				} else if (category.equalsIgnoreCase("SB.DC")) {
					category = "Delivery Complaint";
				} else if (category.equalsIgnoreCase("SB.SG")) {
					category = "Suggestion";
				} else {
					category = "Other or Unknown";
				}
			} catch (NullPointerException e) {
				category = " ";
			}

			emailMessage.append("Publication = ");
			emailMessage.append(publication);
			emailMessage.append("\nCategory = ");
			emailMessage.append(category);
			emailMessage.append("\n\nThe comments below were provided by:\nFirst Name = ");
			emailMessage.append(firstname);
			emailMessage.append("\nLast Name = ");
			emailMessage.append(lastname);
			emailMessage.append("\nAccount Number = ");
			emailMessage.append(accountnumber);
			emailMessage.append("\nCompany Name = ");
			emailMessage.append(company);
			emailMessage.append("\nDaytime phone number = ");
			emailMessage.append(daynumber);
			emailMessage.append("\nE-mail address = ");
			emailMessage.append(emailid);
			emailMessage.append("\nMessage = ");
			emailMessage.append(messagebox);
			emailMessage.append("\n");
			emailMessage.append(codedValue.toString());

			// send message to NCS
			UTCommon.sendFeedbackEmail(emailid, UTCommon.FEEDBACK_EMAIL, UTCommon.FEEDBACK_SUBJECT, emailMessage.toString());
			ncsSent = true;

			// send user on their way since it was successful
			UTCommon.showUrl(res, successURL);
			userForwarded = true;

			// Add additional data for dev.
			emailMessage.append("\n\n******************************************************************\n");
			emailMessage.append("********** DEV ONLY DATA - DO NOT FORWARD TO CUSTOMER  ***********\n");
			emailMessage.append("******************************************************************\n\n");

			emailMessage.append("Date/Time Entered: ").append(new java.util.Date(System.currentTimeMillis()).toString())
					.append("\n\n");

			emailMessage.append("Feedback Sender's Email: " + emailid);

			// Session data dump
			java.util.Enumeration<String> e = session.getAttributeNames();
			if (!e.hasMoreElements()) {
				emailMessage.append("\nNo HTTP Session Parameters.\n");
			} else {
				emailMessage.append("\nHTTP Session Parameters:\n");
				while (e.hasMoreElements()) {
					String attr = e.nextElement();
					if (attr.equalsIgnoreCase("session.usatelogin") || attr.equalsIgnoreCase("session.accountinfo")) {
						continue;
					} else {
						emailMessage.append("\tSession Object Name==> ").append(attr).append("    Type==> ")
								.append(session.getAttribute(attr).getClass().getName()).append("    Value (toString())==> ")
								.append(session.getAttribute(attr).toString()).append("\n");
					}

				}
			}

			UTCommon.sendFeedbackEmail(UTCommon.DEVCC_EMAIL, UTCommon.DEVCC_EMAIL, UTCommon.FEEDBACK_SUBJECT,
					emailMessage.toString());
			devSent = true;

		} catch (Exception e) {
			// no data entered for either field!
			if (!userForwarded) {
				UTCommon.showUrl(res, failureURL);
			}
			// send to dev if success sending to Ncs but not to dev
			if (ncsSent && !devSent) {
				try {
					UTCommon.sendFeedbackEmail(emailid, UTCommon.DEVCC_EMAIL, UTCommon.FEEDBACK_SUBJECT, emailMessage.toString()
							+ "\n\nException during Dev processing: " + e.getMessage());
				} catch (Exception ex) {
					System.out.println("Exception sending Dev email: " + ex.getMessage());
					System.out.println("Message: " + emailMessage.toString());
				}
			}
		}
	}
}
