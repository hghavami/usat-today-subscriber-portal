/*
 * Created on Mar 24, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.usatoday.esub.common;

/**
 * @author aeast
 * 
 *         To change the template for this generated type comment go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and
 *         Comments
 */
public class UsatDBConnectionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4323058937096374648L;
	private Throwable rootCause = null;

	/**
	 * 
	 */
	public UsatDBConnectionException() {
		super();
	}

	/**
	 * @param arg0
	 */
	public UsatDBConnectionException(String arg0) {
		super(arg0);
	}

	/**
	 * @return
	 */
	public Throwable getRootCause() {
		return rootCause;
	}

	/**
	 * @param throwable
	 */
	public void setRootCause(Throwable throwable) {
		rootCause = throwable;
	}

}
