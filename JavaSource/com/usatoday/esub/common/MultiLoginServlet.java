package com.usatoday.esub.common;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.usatoday.business.interfaces.customer.CustomerIntf;
import com.usatoday.business.interfaces.customer.SubscriberAccountIntf;
import com.usatoday.business.interfaces.products.SubscriptionProductIntf;
import com.usatoday.businessObjects.products.SubscriptionProductBO;

public class MultiLoginServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6848778898892966359L;
	public static final String SUCCESS_URL = "SUCCESSURL";
	public static final String FAILURE_URL = "FAILUREURL";
	public static final String SELECT_URL = "SELECTURL";
	public static final String LOGIN_ID = "LOGIN_ID";
	public static final String LOGIN_PASSWORD = "LOGIN_PASSWORD";

	public static final String NOT_SELECT = "/account/select_accnt.jsp";

	/**
	 * Login the current user; return true if successful, false otherwise Creation date: (8/21/00 10:05:38 PM)
	 * 
	 * @return boolean
	 */
	/**
	 * MultiLoginServlet constructor comment.
	 */
	public MultiLoginServlet() {
		super();
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		// get the session object; create a session if none exists
		HttpSession session = req.getSession(true);

		// set content type and other response header fields
		// UTCommon.setResponseHeader(res);

		// retrieve the URLs
		String successURL = UTCommon.getParameterAsString(req, SUCCESS_URL);
		@SuppressWarnings("unused")
		String selectURL = UTCommon.getParameterAsString(req, SELECT_URL);

		try {

			String accountinfo = UTCommon.getParameterAsString(req, "SELECTED");

			if (accountinfo == null || accountinfo.length() == 0) {
				String msg = "Please choose an account to continue.";
				session.setAttribute("MESSAGE", msg);
				UTCommon.showUrl(res, "/account/select_accnt.jsp");
				return;
			}
			accountinfo = accountinfo.trim();

			int pos = accountinfo.indexOf('/');
			String accountnum = accountinfo.substring(0, pos);
			// following stored in session object in finally block below
			// Get customer accounts information
			CustomerIntf customer = null;
			customer = (CustomerIntf) session.getAttribute(UTCommon.SESSION_CUSTOMER_INFO);

			customer.setCurrentAccount(accountnum);

			SubscriberAccountIntf account = customer.getCurrentAccount();

			if (account != null) {

				SubscriptionProductIntf product = SubscriptionProductBO.getSubscriptionProduct(customer.getCurrentEmailRecord()
						.getPubCode());
				session.removeAttribute(UTCommon.SESSION_PUB_NAME);
				session.setAttribute(UTCommon.SESSION_PUB_NAME, product.getBrandingPubCode());

				UTCommon.showUrl(res, successURL);
				return;

			} // end if account != null
			else {
				// new Account

				UTCommon.showUrl(res, "/eAccount/eEditionNewAccountIndex.faces");
				return;
			}
		} catch (Exception e) {
			// no data entered for either field!
			UTCommon.showUrl(res, "/login/auth.faces");
			return;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void doGet(HttpServletRequest arg0, HttpServletResponse arg1) throws ServletException, IOException {

		this.doPost(arg0, arg1);
	}
}
