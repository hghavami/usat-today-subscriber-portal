/*
 * Created on May 26, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.usatoday.esub.common;

/**
 * @author aeast
 * 
 *         To change the template for this generated type comment go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and
 *         Comments
 */
public class USATRequestHelper {
	public static final String SW_PHONE = "1-800-872-1415";
	public static final String UT_PHONE = "1-800-872-0001";

	private String pubCode = UTCommon.UTPUBCODE;

	/**
	 * @return
	 */
	public String getPubCode() {
		return pubCode;
	}

	/**
	 * @param string
	 */
	public void setPubCode(String string) {
		if (UTCommon.BWPUBCODE.equalsIgnoreCase(string)) {
			pubCode = UTCommon.BWPUBCODE;
		} else {
			pubCode = UTCommon.UTPUBCODE;
		}
	}

	/**
	 * @return Name of the publication
	 */
	public String getPublicationName() {
		if (this.getPubCode().equalsIgnoreCase(UTCommon.UTPUBCODE)) {
			return "USA TODAY";
		} else {
			return "Sports Weekly";
		}
	}

	public String getPhoneNumber() {
		if (this.getPubCode().equalsIgnoreCase(UTCommon.UTPUBCODE)) {
			return USATRequestHelper.UT_PHONE;
		} else {
			return USATRequestHelper.SW_PHONE;
		}
	}

	public String getUsatMailingAddressFooter() {
		if (this.getPubCode() == null || this.getPubCode().equalsIgnoreCase("UT")) {
			return "USA TODAY, 7950 Jones Branch Dr, McLean, Virginia, 22108";
		} else {
			return "USA TODAY Sports Weekly, 7950 Jones Branch Dr, McLean, Virginia, 22108";
		}
	}
}
