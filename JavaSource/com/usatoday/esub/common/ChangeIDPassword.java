package com.usatoday.esub.common;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.CharUtils;
import org.apache.commons.lang3.StringUtils;

import com.gannett.usat.iconapi.domainbeans.GenesysBaseAPIResponse;
import com.gannett.usat.userserviceapi.client.ChangeUserPassword;
import com.gannett.usat.userserviceapi.client.GetUser;
import com.gannett.usat.userserviceapi.client.Login;
import com.gannett.usat.userserviceapi.client.UpdateUser;
import com.gannett.usat.userserviceapi.domainbeans.changeUserPassword.ChangeUserPasswordResponse;
import com.gannett.usat.userserviceapi.domainbeans.getUser.GetUserResponse;
import com.gannett.usat.userserviceapi.domainbeans.login.LoginResponse;
import com.gannett.usat.userserviceapi.domainbeans.users.UsersResponse;
import com.usatoday.business.interfaces.customer.CustomerIntf;
import com.usatoday.business.interfaces.customer.EmailRecordIntf;
import com.usatoday.businessObjects.customer.EmailRecordBO;
import com.usatoday.businessObjects.customer.service.CustomerService;
import com.usatoday.businessObjects.subscriptionTransactions.GenesysResponse;
import com.usatoday.esub.cookies.AuthenticationCookieProcessor;
import com.usatoday.esub.ncs.handlers.CustomerHandler;

public class ChangeIDPassword extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5916557281491447725L;

	/**
	 * 
	 */
	public static final String JDBC_UPDATE_EMAIL = "UPDATE XTRNTEMAIL SET EmailAddress = ?, DateUpdated = ?, TimeUpdated = ? WHERE EmailAddress = ?";

	public static final String JDBC_UPDATE_PASSWORD = "UPDATE XTRNTEMAIL SET WebPassword = ?, DateUpdated = ?, TimeUpdated = ? WHERE WebPassword = ? and EmailAddress = ?";

	public static final String JDBC_UPDATE_TRANS_EMAIL = "UPDATE XTRNTDWLD SET EmailAddress = ? WHERE EmailAddress = ?";

	public static final String JDBC_EMAIL = "SELECT * from XTRNTEMAIL where EmailAddress=? and WebPassword=?";

	public static final String ERROR_URL = UTCommon.ID_PASSWORD_URL + "/changeIDpassword.jsp";
	public static final String COMPLETE_URL = UTCommon.ID_PASSWORD_URL + "/changeIDpasswordcomplete.jsp";
	private String errorMessage = "";

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	/**
 *
 *
 */
	public static String displayCreateIDText(HttpSession session) throws ServletException, IOException {
		StringBuffer result = new StringBuffer();

		// Determine the Publication Code.
		CustomerHandler ch = (CustomerHandler) session.getAttribute("customerHandler");
		String pubcode = null;
		try {

			pubcode = ch.getCurrentAccount().getAccount().getPubCode();
			if ((pubcode == null) || (pubcode.trim().equals(""))) {
				pubcode = (String) session.getAttribute(UTCommon.SESSION_PUB_NAME);
			}
		} catch (Exception e) {
			pubcode = (String) session.getAttribute(UTCommon.SESSION_PUB_NAME);
		}

		// Build Text

		result.append("</font><p align=\"left\" style=\"margin-bottom: 20\"><font face=\"Arial\" size=\"2\">");
		result.append("If this is your first time accessing your account online, you must first ");
		result.append("<a href=\"/firsttime/first_time.jsp\"");
		if (pubcode == null || pubcode.equals(UTCommon.PUB_NAMES[0])) { // USATODAY publication
			result.append("<font color=\"#5B748A\" onmouseover=\"this.style.color='#0000FF'\" onmouseout=\"this.style.color='#5B748A'\">");
		} else {
			result.append(">");
		}
		result.append("set up an online account");
		result.append("</a>.</p></font>");

		return result.toString();
	}

	/**
	 * FirstTimeServlet constructor comment.
	 */
	public ChangeIDPassword() {
		super();
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		// get the session object; create a session if none exists
		HttpSession session = req.getSession(true);

		// set content type and other response header fields
		UTCommon.setResponseHeader(res);

		String errorMsg = null;
		boolean loginChanged = false;
		boolean passwordChanged = false;

		// Get screen parameters
		ChangeIDPasswordRequestHelper data = new ChangeIDPasswordRequestHelper();

		try {
			data.setCurrLogin(UTCommon.getParameterAsString(req, "CURR_LOGIN").trim().toLowerCase());
			if (UTCommon.getParameterAsString(req, "NEW_LOGIN") != null) {
				data.setNewLogin(UTCommon.getParameterAsString(req, "NEW_LOGIN").trim().toLowerCase());
				// Check new email address for correctness
				try {
					new javax.mail.internet.InternetAddress(data.getNewLogin());
				} catch (javax.mail.internet.AddressException e) {
					errorMsg = "The new email address format is invalid.  Please enter a new email address.";
					// save the current session's data object for the next servlet
					session.setAttribute(UTCommon.SESSION_FIELD_ERROR, errorMsg);
					UTCommon.showUrl(res, ERROR_URL);
					return;
				}
			} else {
				data.setNewLogin("");
			}
			// Check password and error for bad data
			char[] pwdSearchChars = new char[] { '|', '&', '"', '\'', ',', '<', '>', '(', ')', '+', '\\', CharUtils.CR,
					CharUtils.LF };
			data.setCurrPassword(UTCommon.getParameterAsString(req, "CURR_PASSWORD").trim());
			if (UTCommon.getParameterAsString(req, "NEW_PASSWORD") != null) {
				String newPwd = UTCommon.getParameterAsString(req, "NEW_PASSWORD");
				if (StringUtils.containsAny(newPwd, pwdSearchChars)) {
					errorMsg = "Invalid characters used in password. Please retry.";
					session.setAttribute(UTCommon.SESSION_FIELD_ERROR, errorMsg);
					UTCommon.showUrl(res, ERROR_URL);
					return;
				} else {
					data.setNewPassword(UTCommon.getParameterAsString(req, "NEW_PASSWORD").trim());
				}

			} else {
				data.setNewPassword("");
			}
			if (UTCommon.getParameterAsString(req, "NEW_PASSWORD_CONFIRM") != null) {
				UTCommon.getParameterAsString(req, "NEW_PASSWORD_CONFIRM").trim();
			} else {
			}

			CustomerHandler ch = (CustomerHandler) session.getAttribute("customerHandler");
			CustomerIntf cust = null;
			if (ch != null) {
				cust = ch.getCustomer();
			}
			// First check and update email address
			if (queryAtyponEmailPassword(data.getCurrLogin(), data.getCurrPassword())) {

				if ((data.getNewLogin() != null) && (!data.getNewLogin().trim().equals(""))
						&& (!data.getNewLogin().equals(data.getCurrLogin()))) {
					// if (!updateEmailOnly(data)) {
					if (!updateAtyponEmailOnly(data)) {
						// errorMsg = "Unable to save data.  Please try again.";
						errorMsg = this.getErrorMessage();
						session.setAttribute(UTCommon.SESSION_FIELD_ERROR, errorMsg);
						UTCommon.showUrl(res, ERROR_URL);
						return;
					} else {
						errorMsg = "  ";
						// updateTransEmailOnly(data);
						// save the current session's data object for the next servlet
						session.setAttribute(UTCommon.SESSION_FIELD_ERROR, errorMsg);
					}

					// set primaryEmail_Indicator to Y because we are changing ID
					String primaryEmail_Indicator = "Y";

					try {

						// GenesysResponse response = CustomerService
						// .updateEmail(ch.getCustomer().getCurrentAccount().getPubCode(), ch.getCustomer()
						// .getCurrentAccount().getAccountNumber(), data.getNewLogin(), primaryEmail_Indicator);
						// Update all accounts associated with that email address
						GenesysResponse response = CustomerService.updateEmailAccounts(ch.getCustomer().getAccounts(),
								data.getNewLogin(), primaryEmail_Indicator);

						// if no errors
						// if no errors
						if (!response.isContainsErrors()) {
							// complaintHandler.resetWebFormValuesOnly();
							// complaintHandler.setLastComplaint(c);

							try {
								// CustomerServiceEmails.sendComplaintConfirmationEmail(ch.getCustomer(), account, c);
							} catch (Exception e) {
								// ignore email failure;
								// System.out.println("Failed to send complaint confirmation email for account: " +
								// account.getAccountNumber() + ". " + e.getMessage());
							}

						} else {
							// we got errors
							Collection<GenesysBaseAPIResponse> responses = response.getResponses();
							for (GenesysBaseAPIResponse resEmail : responses) {
								if (resEmail.redirectResponse()) {
									// System Error exists
									// complaintHandler.setRawAPIResponse(res.getRawResponse());
									// this.getDialogErrorDialog().setInitiallyShow(true);
									throw new Exception(
											"Error Occurred: Unexpected Response Received. Please call Customer Service.");
								} else {
									// business rule errors exist
									Collection<String> errors = resEmail.getErrorMessages();
									if (errors != null) {
										// StringBuilder errorBuf = new StringBuilder();
										for (String msg : errors) {
											FacesContext context = FacesContext.getCurrentInstance();
											FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, null);

											context.addMessage(null, facesMsg);
										}
										throw new Exception();
									} else {
										throw new Exception("Failed : Reason Unknown.");
									}
								}
							}
						}

					} catch (Exception e) {
						System.out.println("Failed to update Genesys with the new email/password information: " + e);
					}

					// end HDCONS - 91

					// update the current login in the helper class in case the password needs updating.
					data.setCurrLogin(data.getNewLogin());

					if (cust != null) {
						cust.getCurrentEmailRecord().setEmailAddress(data.getNewLogin());
					}
					loginChanged = true;
				} else {
					data.setNewLogin(data.getCurrLogin());
				}

				// Now check and update password
				if (!data.getNewPassword().equals("") && !data.getNewPassword().equals(data.getCurrPassword())) {
					if (!updateAtyponPasswordOnly(data)) {
						// errorMsg = "Unable to save data.  Please try again.";
						errorMsg = this.getErrorMessage();
						session.setAttribute(UTCommon.SESSION_FIELD_ERROR, errorMsg);
						UTCommon.showUrl(res, ERROR_URL);
						return;
					} else {
						errorMsg = "  ";
						// save the current session's data object for the next servlet
						session.setAttribute(UTCommon.SESSION_FIELD_ERROR, errorMsg);
					}
					passwordChanged = true;
				}

			} else {
				errorMsg = "Email/Password not found.  Please try again or call Customer Service.";

				// save the current session's data object for the next servlet
				session.setAttribute(UTCommon.SESSION_FIELD_ERROR, errorMsg);
				UTCommon.showUrl(res, ERROR_URL);
				return;
			}

			// update remember me cookie if necessary
			if (loginChanged) {
				try {
					AuthenticationCookieProcessor cp = new AuthenticationCookieProcessor();

					// javax.servlet.http.Cookie c = cp.returnRememberMeCookie(req.getCookies());
					javax.servlet.http.Cookie c = cp.returnAutoLoginCookie(req.getCookies());
					if (c != null) {
						c.setValue(data.getNewLogin().trim());
						c = cp.extendExpiration(c);
						res.addCookie(c);
					}
				} catch (Exception exp) {
					; // ignore
				}
			}

			StringBuffer messageStr = new StringBuffer();
			if (loginChanged && passwordChanged) {
				messageStr.append("<br>Your email address and password have been updated.<br><br>");
			} else if (loginChanged) {
				messageStr.append("<br>Your email address has been updated.<br><br>");
			} else if (passwordChanged) {
				messageStr.append("<br>Your password has been updated.<br><br>");
			} else {
				messageStr.append(" Your inforation has been updated.<br><br>");
			}
			messageStr.append("If you would like a record of this change, please print this page.<br>");
			messageStr.append("An email confirmation will not be generated.<br><br> Thank You.");

			req.getSession().setAttribute("MESSAGE_STR", messageStr.toString());
			UTCommon.showUrl(res, COMPLETE_URL);

		} catch (NullPointerException e) {
			errorMsg = "Please enter valid data.";

			// save the current session's data object for the next servlet
			session.setAttribute(UTCommon.SESSION_FIELD_ERROR, errorMsg);

			UTCommon.showUrl(res, ERROR_URL);
			return;

		}
	}

	/**
	 * Check current User ID and Password Creation date: (8/21/00 10:05:38 PM)
	 * 
	 * @return boolean
	 */

	public boolean queryAtyponEmailPassword(String emailaddress, String password) {

		boolean found = false;
		try {

			Collection<EmailRecordIntf> emailRecords = EmailRecordBO.getEmailRecordsForEmailPwd(emailaddress, password);
			if (emailRecords != null && emailRecords.size() > 0) {
				found = true;
			}
			/*
			 * Iterator<EmailRecordIntf> itr = emailRecords.iterator();
			 * 
			 * EmailRecordIntf eRec = null; while (itr.hasNext()) { eRec = itr.next(); if
			 * (eRec.getPassword().equalsIgnoreCase(password)) { found = true; break; } }
			 */
			// inspect the result
			if (found) {
				return true;
			} else {
				return false;
			}

		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Update email address only Creation date: (8/21/00 10:05:38 PM)
	 * 
	 * @return boolean
	 */

	public boolean updateAtyponEmailOnly(ChangeIDPasswordRequestHelper data) {

		try {
			// Use Users method to get user ID and then using updateUser method update the user with the new email address
			this.errorMessage = "";
			GetUserResponse response = null;
			GetUser gUser = new GetUser();
			response = gUser.getUsers(data.getCurrLogin());
			if (!response.containsErrors()) {
				UsersResponse uResponse = null;
				UpdateUser uUser = new UpdateUser();
				uResponse = uUser.updateUser(null, null, data.getNewLogin(), response.getFireflyUserId());

				if (!uResponse.containsErrors()) {
					return true;
				} else if (uResponse.getErrorCode() == 500) { // Error code 500 means new email already exists
					// Ensure user will be able to login with either old or if entered new password
					String passwordCheck = data.getCurrPassword();
					if (data.getNewPassword() != null && !data.getNewPassword().trim().equals("")) {
						passwordCheck = data.getNewPassword();
					}
					LoginResponse lResponse = null;
					Login lUser = new Login();
					lResponse = lUser.getLogin(data.getNewLogin(), passwordCheck);
					if (!lResponse.containsErrors()) {
						return true;
					} else {
						this.errorMessage = "New email exists, but email/password combination does not match our records.";
						return false;
					}
					// }
					// return true;
				} else {
					this.errorMessage = uResponse.getMessage();
					System.out.println(response.getErrorMessages().toString());
					return false;
				}
			} else {
				this.errorMessage = response.getMessage();
				System.out.println(response.getErrorMessages().toString());
				return false;
			}

		} catch (Exception e) {
			return false;

		}

	}

	/**
	 * Update email address only Creation date: (8/21/00 10:05:38 PM)
	 * 
	 * @return boolean
	 */

	public boolean updateAtyponPasswordOnly(ChangeIDPasswordRequestHelper data) {

		try {
			// Use user services reset method to update password
			ChangeUserPasswordResponse response = null;
			ChangeUserPassword cUserP = new ChangeUserPassword();
			response = cUserP.createChangeUserPassword(data.getCurrLogin(), data.getNewPassword());
			if (!response.containsErrors()) {
				return true;
			} else {
				this.errorMessage = response.getMessage();
				System.out.println(response.getErrorMessages().toString());
				return false;
			}
		} catch (Exception e) {
			return false;
		}

	}

	/**
	 * Update XTRNTDWLD file's email address only Creation date: (8/21/00 10:05:38 PM)
	 * 
	 * @return boolean
	 */

	public boolean updateAtyponTransEmailOnly(ChangeIDPasswordRequestHelper data) {
		// Not sure why previously this method was called, it is no longer needed, so it now always will return true
		return true;
	}

	/**
	 * Update email address only Creation date: (8/21/00 10:05:38 PM)
	 * 
	 * @return boolean
	 */

	public boolean updateEmailOnly(ChangeIDPasswordRequestHelper data) {

		// VERIFY CUSTOMER HAS AN ACCOUNT
		// Lookup users account
		Connection connection = null;
		try {
			// get a connection to the database
			connection = DataSourceConnectionFactory.getDBConnection();

			// create a prepared statement and substitute the parameters
			PreparedStatement statement = connection.prepareStatement(JDBC_UPDATE_EMAIL);
			statement.setString(1, data.getNewLogin());
			statement.setString(2, UTCommon.getSystemDateYYYYMMDD());
			statement.setString(3, UTCommon.getSystemTimeHHMMSS());
			statement.setString(4, data.getCurrLogin());

			// execute the SQL
			statement.execute();

			// get the result
			int count = statement.getUpdateCount();

			if (count == 0) {
				return false;
			}

			statement.close();
			return true;

		} catch (SQLException e) {
			return false;

		} catch (UsatDBConnectionException e) {
			return false;
		} finally {
			DataSourceConnectionFactory.closeConnection(connection);
		}

	}

	/**
	 * Update password Creation date: (8/21/00 10:05:38 PM)
	 * 
	 * @return boolean
	 */

	public boolean updatePasswordOnly(ChangeIDPasswordRequestHelper data) {

		// VERIFY CUSTOMER HAS AN ACCOUNT
		// Lookup users account
		Connection connection = null;
		try {
			// get a connection to the database
			connection = DataSourceConnectionFactory.getDBConnection();

			// create a prepared statement and substitute the parameters
			PreparedStatement statement = connection.prepareStatement(JDBC_UPDATE_PASSWORD);

			statement.setString(1, data.getNewPassword());
			statement.setString(2, UTCommon.getSystemDateYYYYMMDD());
			statement.setString(3, UTCommon.getSystemTimeHHMMSS());
			statement.setString(4, data.getCurrPassword());
			statement.setString(5, data.getCurrLogin());

			// execute the SQL
			statement.execute();

			// get the result
			int count = statement.getUpdateCount();

			if (count == 0) {
				return false;
			}

			statement.close();
			return true;

		} catch (SQLException e) {
			return false;

		} catch (UsatDBConnectionException e) {
			return false;
		} finally {
			DataSourceConnectionFactory.closeConnection(connection);
		}

	}

	public boolean updateTransEmailOnly(ChangeIDPasswordRequestHelper data) {

		// VERIFY CUSTOMER HAS AN ACCOUNT
		// Lookup users account
		Connection connection = null;
		try {
			// get a connection to the database
			connection = DataSourceConnectionFactory.getDBConnection();

			// create a prepared statement and substitute the parameters
			PreparedStatement statement = connection.prepareStatement(JDBC_UPDATE_TRANS_EMAIL);

			// First the new email address
			try {
				statement.setString(1, data.getNewLogin());
			} catch (NullPointerException e) {
				statement.setString(1, "");
			}
			// Now do the current login
			try {
				statement.setString(2, data.getCurrLogin());
			} catch (NullPointerException e) {
				statement.setString(2, "");
			}

			// execute the SQL
			statement.execute();

			// get the result
			int count = statement.getUpdateCount();

			if (count == 0) {
				return false;
			}

			statement.close();
			return true;

		} catch (SQLException e) {
			return false;

		} catch (UsatDBConnectionException e) {
			return false;
		} finally {
			DataSourceConnectionFactory.closeConnection(connection);
		}

	}
}
