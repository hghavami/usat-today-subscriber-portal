package com.usatoday.esub.common;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.CharUtils;
import org.apache.commons.lang3.StringUtils;

import com.gannett.usat.iconapi.domainbeans.GenesysBaseAPIResponse;
import com.gannett.usat.userserviceapi.client.CreateUser;
import com.gannett.usat.userserviceapi.domainbeans.users.UsersResponse;
import com.usatoday.business.interfaces.customer.CustomerIntf;
import com.usatoday.business.interfaces.customer.EmailRecordIntf;
import com.usatoday.business.interfaces.customer.SubscriberAccountIntf;
import com.usatoday.businessObjects.customer.CustomerBO;
import com.usatoday.businessObjects.customer.EmailRecordBO;
import com.usatoday.businessObjects.customer.SubscriberAccountBO;
import com.usatoday.businessObjects.customer.service.CustomerService;
import com.usatoday.businessObjects.subscriptionTransactions.GenesysResponse;
import com.usatoday.esub.ncs.handlers.CustomerHandler;

//

public class FirstTimeServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 579081058133562579L;

	/**
	 * Login the current user; return true if successful, false otherwise Creation date: (8/21/00 10:05:38 PM)
	 * 
	 * @return boolean
	 */

	public static final String JDBC_INSERT = "INSERT INTO XTRNTEMAIL "
		+ "(EmailAddress, WebPassword, ACTIVE, PermStartDate, TimeUpdated, DateUpdated)" + "VALUES" + "(?, ?, 'Y', ?, ?, ?)";

	// public static final String JDBC_UPDATE_PASSWORD =
	// "UPDATE XTRNTEMAIL SET WebPassword = ?, DateUpdated = ?, TimeUpdated = ? WHERE WebPassword = ? and EmailAddress = ? and AccountNum = ?";
	public static final String JDBC_UPDATE_PASSWORD = "UPDATE XTRNTEMAIL SET WebPassword = ?, DateUpdated = ?, TimeUpdated = ? WHERE WebPassword = ? and EmailAddress = ?";

	public static final String ERROR_URL = UTCommon.FIRSTTIME_URL + "/first_time.jsp";

	public static final String INSERT = "/account/accounthistory/accountSummary.faces";

	public static final String EMAIL_ASSOCIATION_URL = UTCommon.FIRSTTIME_URL + "/emailAssociation.jsp";

	public static final String ADD_EMAIL_CONFIRM_URL = UTCommon.FIRSTTIME_URL + "/addemailconfirm.jsp";

	public static final String CONTINUE = "CONTINUE";

	/**
	 * FirstTimeServlet constructor comment.
	 */
	public FirstTimeServlet() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void doGet(HttpServletRequest arg0, HttpServletResponse arg1) throws ServletException, IOException {

		this.doPost(arg0, arg1);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		req.getSession(true);

		// set content type and other response header fields
		UTCommon.setResponseHeader(res);

		// Call the appropriate dopost methos based on the step

		int step = 0;
		try {
			step = UTCommon.getParameterAsInt(req, UTCommon.STEP);
		} catch (NumberFormatException e) {
			String message = "Internal data error.  Number data was expected but character data was found.  " + "("
			+ this.getClass().getName() + ": " + " STEP=" + UTCommon.getParameterAsString(req, UTCommon.STEP) + ")";
			UTCommon.showError(req, res, 0, message, e);
			return;
		}

		switch (step) {

		case 1:
			doPost1(req, res);
			break;

		case 2:
			doPost2(req, res);
			break;

		case 3:
			doPost3(req, res);
			break;

		}
	}

	public void doPost1(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		HttpSession session = req.getSession(false);
		if (session == null) {
			UTCommon.showError(req, res, UTError.NO_SESSION_DATA_TEXT);
			return;
		}

		UTFirstTime data = null;
		// SubscriberAccountIntf account = null;
		CustomerIntf customer = null;

		try {
			data = UTCommon.getFirstTimeSessionDefaultDataObject(session);
		} catch (NullPointerException e) {
			// create a new data object
			data = new UTFirstTime();
		}

		// String result = null;
		String errorMsg = null;
		// String resultEmailTranType = null;
		String accountid = "";
		String phoneNum = "";
		String pubcode = "";
		// Check password and error for bad data
		char[] charsNotAllowed = new char[] { '|', '&', '"', '\'', ',', '<', '>', '(', ')', '+', '\\', CharUtils.CR, CharUtils.LF };

		// Get screen parameters
		try {
			pubcode = UTCommon.getParameterAsString(req, "PUB_NAMES");
		} catch (NullPointerException e) {
			pubcode = "UT";
		}
		try {
			accountid = UTCommon.getParameterAsString(req, "ACCNT_ID").trim().toLowerCase();
			if (StringUtils.containsAny(accountid, charsNotAllowed)) {
				data.setErrorMsg("Invalid characters used in account number. Please retry.");
				data.setErrorID(4);
				session.setAttribute(UTCommon.SESSION_FIRST_TIME, data);
				UTCommon.showUrl(res, ERROR_URL);
				return;
			}
		} catch (NullPointerException e) {
			accountid = "";
		}
		try {
			phoneNum = UTCommon.getParameterAsString(req, "PHONE_NUM1").trim().toLowerCase()
			+ UTCommon.getParameterAsString(req, "PHONE_NUM2").trim().toLowerCase()
			+ UTCommon.getParameterAsString(req, "PHONE_NUM3").trim().toLowerCase();
		} catch (NullPointerException e) {
			phoneNum = "";
		}

		SubscriberAccountIntf theAccount = null;

		try {
			String emailid = UTCommon.getParameterAsString(req, "EMAIL").trim().toLowerCase();
			try {
				new javax.mail.internet.InternetAddress(emailid);
			} catch (javax.mail.internet.AddressException e) {
				data.setErrorMsg("The email address format is invalid.  Please enter a new email address.");
				// save the current session's data object for the next servlet
				session.setAttribute(UTCommon.SESSION_FIRST_TIME, data);
				data.setErrorID(3);
				UTCommon.showUrl(res, ERROR_URL);
				return;
			}

			String zipcode = UTCommon.getParameterAsString(req, "ZIPCODE").trim().toLowerCase();
			String webpasswordid = UTCommon.getParameterAsString(req, "PASSWORD").trim();
			if (webpasswordid.length() > 30) {
				data.setErrorMsg("Password length should be less than 30.");
				data.setErrorID(1);
				session.setAttribute(UTCommon.SESSION_FIRST_TIME, data);
				UTCommon.showUrl(res, ERROR_URL);
				return;
			}
			if (StringUtils.containsAny(webpasswordid, charsNotAllowed)) {
				data.setErrorMsg("Invalid characters used in password. Please retry.");
				data.setErrorID(1);
				session.setAttribute(UTCommon.SESSION_FIRST_TIME, data);
				UTCommon.showUrl(res, ERROR_URL);
				return;
			}

			UTCommon.getParameterAsString(req, "VERIFY_PASSWORD").trim();

			data.setAccountNumber(accountid);
			data.setPhoneNumber(phoneNum);
			data.setEmailAddress(emailid);
			data.setDeliveryZip(zipcode);
			data.setPassword(webpasswordid);

			Collection<SubscriberAccountIntf> accounts = null;

			boolean usedAccountNumber = false;

			// No account number means get it using phone number
			if (accountid == null || accountid.trim().equals("")) {
				//
				// phone number is associated with mutiple accounts
				accounts = SubscriberAccountBO.getSubscriberAccountsForPhoneAndZip(phoneNum, zipcode);
			} else { // use account number
				usedAccountNumber = true;
				// accounts = SubscriberAccountBO.getSubscriberAccountByAccountNumPub(accountid, null);

				// if (accounts.size() > 1) {
				// try to limit by current publication
				// String pubCode = (String) session.getAttribute(UTCommon.SESSION_PUB_NAME);
				accounts = SubscriberAccountBO.getSubscriberAccountByAccountNumPub(accountid, pubcode);
				// }
			}

			Iterator<SubscriberAccountIntf> acctItr = accounts.iterator();
			// remove any accounts that don't match the zip
			while (acctItr.hasNext()) {
				SubscriberAccountIntf cAcct = acctItr.next();
				if (!cAcct.getDeliveryContact().getUIAddress().getZip().equalsIgnoreCase(zipcode)) {
					acctItr.remove();
				}
			}

			if (accounts.size() > 1) { // multiple accounts
				if (!usedAccountNumber) {
					errorMsg = "The phone number you entered is listed on more than one account.  Please use an account number to setup your account online.";
				} else {
					errorMsg = "An error has occurred while attempting to set up your account access. Please call our National Customer Service number to place your transaction and to report this issue.";
				}
				data.setErrorID(7);
				data.setAddEmailPassLink(false);
				data.setAddGiftEmail(false);
				// save the current session's data object for the next
				// servlet
				data.setErrorMsg(errorMsg);
				data.setPhoneNumber("");
				session.setAttribute(UTCommon.SESSION_FIRST_TIME, data);
				UTCommon.showUrl(res, ERROR_URL);
				return;
			} else if (accounts.size() == 0) {
				if (!usedAccountNumber) {
					errorMsg = "No matching accounts found. Please verify the Phone Number and the Delivery Zipcode associated with this account are correct.";
				} else {
					errorMsg = "No matching accounts found. Please verify the Account Number and the Delivery Zipcode associated with this account are correct.";
				}
				data.setErrorID(7);
				// save the current session's data object for the next servlet
				data.setAddEmailPassLink(false);
				data.setAddGiftEmail(false);
				data.setErrorMsg(errorMsg);
				session.setAttribute(UTCommon.SESSION_FIRST_TIME, data);
				UTCommon.showUrl(res, ERROR_URL);
				return;
			} else {
				theAccount = accounts.iterator().next();
				accountid = theAccount.getAccountNumber();
				data.setAccountNumber(accountid);
			}
			// First check the database for a matching email record with blank
			// password and if so only
			// update the password
			// Collection<EmailRecordIntf> emailRecords = EmailRecordBO.getEmailRecordsForEmailPwdAccount(emailid, "", accountid);
			/*			Collection<EmailRecordIntf> emailRecords = EmailRecordBO.getEmailRecordsForEmailAddress(emailid);
			if (emailRecords.size() == 1) {
				// Ordered online and already have an email addres w/ no password
				// updatePasswordOnly(data);
				// Log the customer in
				errorMsg = "";
				data.setErrorMsg(errorMsg);
				data.setErrorID(0);
				// save the current session's data object for the next
				// servlet
				session.setAttribute(UTCommon.SESSION_FIRST_TIME, data);
				// Pub code is set by the retrieved account, not the session
				// default
				// String pubcode = (String) session.getAttribute(UTCommon.SESSION_PUB_NAME);
				try {
					try {
						// customer = CustomerBO.loginCustomer(emailid, webpasswordid, pubcode);
						customer = CustomerBO.loginCustomerEmailAccountPub(emailid, accountid, pubcode);
						customer.setCurrentAccount(accountid);
						// account = customer.getCurrentAccount();

						CustomerHandler ch = null;
						ch = (CustomerHandler) session.getAttribute("customerHandler");
						if (ch == null) {
							ch = new CustomerHandler();
							session.setAttribute("customerHandler", ch);
						}

						ch.setCustomer(customer);
						ch.setAuthenticated(true);
						ch.setCookieAuthenticated(false);
					} catch (Exception e) {
						System.out.println("unexpected exception: " + e.getMessage());
					}

					// save the current data into the session
					session.setAttribute(UTCommon.SESSION_CUSTOMER_INFO, customer);

				} catch (NullPointerException e) {
				}
				// display the next 'successful account'
				UTCommon.showUrl(res, INSERT);
				return;

				// Check account information using non-blanks password
			} else {
			 */
			data.setPubCode(theAccount.getPubCode());
			// query Atypon by email and password
			// emailRecords = EmailRecordBO.getEmailRecordsForAccount(theAccount.getAccountNumber());
			Collection<EmailRecordIntf> emailRecords = EmailRecordBO.getEmailRecordsForEmailAddress(emailid);
			boolean emailAlreadyExists = false;
			if (emailRecords.size() > 0) {		// Email already exists
				emailAlreadyExists = true;
			}
			emailRecords = EmailRecordBO.getEmailRecordsForEmailPwd(emailid, webpasswordid);
			if (emailRecords.size() == 0) {
				if (emailAlreadyExists) {
					errorMsg = "Email already exists, but email and password combination does not match our records.";
					data.setErrorID(7);
					data.setAddEmailPassLink(false);
					data.setAddGiftEmail(false);
					data.setErrorMsg(errorMsg);
					session.setAttribute(UTCommon.SESSION_FIRST_TIME, data);
					UTCommon.showUrl(res, ERROR_URL);
					return;
				}
				// set up new account
				if (!insertEmail(webpasswordid, emailid, theAccount)) { // any errors
					errorMsg = "Unable to save data.  Please try again.";
					data.setAddEmailPassLink(false);
					data.setAddGiftEmail(false);
					data.setErrorMsg(errorMsg);
					data.setErrorID(4);
					// save the current session's data object for
					// the next servlet
					session.setAttribute(UTCommon.SESSION_FIRST_TIME, data);
					UTCommon.showUrl(res, ERROR_URL);
					return;
				}
			}
			// } else { // Inserted record into email file or 1 already exists
			errorMsg = "";
			data.setErrorMsg(errorMsg);
			data.setErrorID(0);
			// save the current session's data object for
			// the next servlet
			session.setAttribute(UTCommon.SESSION_FIRST_TIME, data);
			// Pub code is set by the retrieved account, not
			// the session default
			// String pubcode = data.getPubCode();

			// set primaryEmail_Indicator to Y because we are changing ID
			String primaryEmail_Indicator = "Y";

			try {

				GenesysResponse response = CustomerService.updateEmail(data.getPubCode(), data.getAccountNumber(),
						data.getEmailAddress(), primaryEmail_Indicator);
				// if no errors
				// if no errors
				if (!response.isContainsErrors()) {
					// complaintHandler.resetWebFormValuesOnly();

					try {
						// CustomerServiceEmails.sendComplaintConfirmationEmail(ch.getCustomer(), account, c);
					} catch (Exception e) {
						// ignore email failure;
						// System.out.println("Failed to send complaint confirmation email for account: " +
						// account.getAccountNumber() + ". " + e.getMessage());
					}

				} else {
					// we got errors
					Collection<GenesysBaseAPIResponse> responses = response.getResponses();
					for (GenesysBaseAPIResponse resEmail : responses) {
						if (resEmail.redirectResponse()) {
							// System Error exists
							// complaintHandler.setRawAPIResponse(res.getRawResponse());
							// this.getDialogErrorDialog().setInitiallyShow(true);
							throw new Exception("Error Occurred: Unexpected Response Received. Please call Customer Service.");
						} else {
							// business rule errors exist
							Collection<String> errors = resEmail.getErrorMessages();
							if (errors != null) {
								// StringBuilder errorBuf = new StringBuilder();
								for (String msg : errors) {
									data.setErrorMsg(msg);
									data.setErrorID(7);
									session.setAttribute(UTCommon.SESSION_FIRST_TIME, data);
									UTCommon.showUrl(res, ERROR_URL);
									return;
								}
								throw new Exception();
							} else {
								throw new Exception("Failed : Reason Unknown.");
							}
						}
					}
				}

			} catch (Exception e) {

			}

			// end HDCONS - 91

			try { // Log in user with this information
				try {
					// customer = CustomerBO.loginCustomer(emailid, webpasswordid);
					customer = CustomerBO.loginCustomerEmailAccountPub(emailid, accountid, pubcode);
					customer.setCurrentAccount(accountid);
					theAccount = customer.getCurrentAccount();
				} catch (Exception e) {
					System.out.println("unexpected exception: " + e.getMessage());
				}

				session.removeAttribute(UTCommon.SESSION_CUSTOMER_INFO);
				CustomerHandler ch = null;
				ch = (CustomerHandler) session.getAttribute("customerHandler");
				if (ch == null) {
					ch = new CustomerHandler();
					session.setAttribute("customerHandler", ch);
				}

				ch.setCustomer(customer);
				ch.setAuthenticated(true);
				ch.setCookieAuthenticated(false);

				// save the current data into the session
				session.setAttribute(UTCommon.SESSION_CUSTOMER_INFO, customer);

				// set the pub in the session or the account and clear any keycode
				session.removeAttribute(UTCommon.SESSION_PUB_NAME);
				session.setAttribute(UTCommon.SESSION_PUB_NAME, theAccount.getPubCode());
				session.removeAttribute(UTCommon.SESSION_KEYCODE);
				;

			} catch (NullPointerException e) {
			}
			// display the next 'successful account'
			UTCommon.showUrl(res, INSERT);
			return;
			//			}

		} catch (Exception e) {
			errorMsg = "Unable to find a matching account. Please check your entries and try again.";
			data.setAddEmailPassLink(false);
			data.setAddGiftEmail(false);
			data.setErrorMsg(errorMsg);
			data.setErrorID(6);
			// save the current session's data object for the next servlet
			session.setAttribute(UTCommon.SESSION_FIRST_TIME, data);
			UTCommon.showUrl(res, ERROR_URL);
			return;
		}
	}

	/**
	 * 
	 * @param req
	 * @param res
	 * @throws ServletException
	 * @throws IOException
	 */
	public void doPost2(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		// get the session object; error if no session object is retrieved
		HttpSession session = req.getSession(false);
		if (session == null) {
			UTCommon.showError(req, res, UTError.NO_SESSION_DATA_TEXT);
			return;
		}

		res.getWriter();

		// set content type and other response header fields
		UTCommon.setResponseHeader(res);

		String errorMsg = null;
		// String transtype = "";
		CustomerIntf customer = null;

		UTFirstTime data = null;
		try {
			data = UTCommon.getFirstTimeSessionDefaultDataObject(session);
			customer = CustomerBO.loginCustomerEmailAccountPub(data.getEmailAddress(), data.getAccountNumber(), data.getPubCode());
			// retrieve the input parameters
			int continueOrder = 0;
			@SuppressWarnings("unused")
			int step = 0;
			try {
				continueOrder = UTCommon.getParameterAsInt(req, CONTINUE);
				step = UTCommon.getParameterAsInt(req, UTCommon.STEP);

			} catch (NumberFormatException e) {
				String message = "Internal data error.  Number data was expected but character data was found.  " + "("
				+ this.getClass().getName() + ": " + " CONTINUE=" + UTCommon.getParameterAsString(req, CONTINUE) + ")";
				UTCommon.showError(req, res, 0, message, e);
				return;
			}
			/*
			 * switch (continueOrder) { case 0: transtype = "N"; // Recipient of the newspaper break;
			 * 
			 * case 1: transtype = "G"; // Only paying for the newspaper break; }
			 */
			if (continueOrder == 0 || continueOrder == 1) {
				if (!insertEmail(data.getPassword(), data.getEmailAddress(), customer.getCurrentAccount())) { // any errors
					errorMsg = "Unable to save data.  Please try again.";
					data.setErrorMsg(errorMsg);
					data.setErrorID(4);
					// save the current session's data object for the next
					// servlet
					session.setAttribute(UTCommon.SESSION_FIRST_TIME, data);
					UTCommon.showUrl(res, ERROR_URL);
					return;
				} else { // Insert record into email file
					errorMsg = "";
					data.setErrorMsg(errorMsg);
					data.setErrorID(0);
					// save the current session's data object for the next
					// servlet
					session.setAttribute(UTCommon.SESSION_FIRST_TIME, data);
					try { // Log in user with this information
						try {
							customer = CustomerBO.loginCustomer(data.getEmailAddress(), data.getPassword(), data.getPubCode());
							customer.setCurrentAccount(data.getAccountNumber());

							CustomerHandler ch = null;

							ch = (CustomerHandler) session.getAttribute("customerHandler");
							if (ch == null) {
								ch = new CustomerHandler();
								session.setAttribute("customerHandler", ch);
							}

							ch.setCustomer(customer);
							ch.setAuthenticated(true);
							ch.setCookieAuthenticated(false);
						} catch (Exception e) {
							System.out.println("unexpected exception: " + e.getMessage());
						}
						// save the current data into the session
						session.setAttribute(UTCommon.SESSION_CUSTOMER_INFO, customer);

					} catch (NullPointerException e) {
					}
					// display the next 'successful account'
					UTCommon.showUrl(res, INSERT);
					return;
				}
			}
		} catch (Exception e) {
			errorMsg = "Transaction has completed.";
			data = new UTFirstTime();
			data.setAccountNumber("");
			data.setAddEmailPassLink(false);
			data.setAddGiftEmail(false);
			data.setDeliveryZip("");
			data.setEmailAddress("");
			data.setPassword("");
			data.setResultEmailTranType("");
			data.setPhoneNumber("");
			data.setErrorMsg(errorMsg);
			data.setErrorID(6);
			// save the current session's data object for the next servlet
			session.setAttribute(UTCommon.SESSION_FIRST_TIME, data);
			UTCommon.showUrl(res, ERROR_URL);
			return;
		}
	}

	// This routine is very similar to doPost2, however, for maintenance reasons
	// I kept them separate
	public void doPost3(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		// get the session object; error if no session object is retrieved
		HttpSession session = req.getSession(false);
		if (session == null) {
			UTCommon.showError(req, res, UTError.NO_SESSION_DATA_TEXT);
			return;
		}

		// set content type and other response header fields
		UTCommon.setResponseHeader(res);

		String errorMsg = null;
		String transtype = "";
		CustomerIntf customer = null;

		UTFirstTime data = null;
		try {
			data = UTCommon.getFirstTimeSessionDefaultDataObject(session);
			customer = CustomerBO.loginCustomerEmailAccountPub(data.getEmailAddress(), data.getAccountNumber(), data.getPubCode());
			// retrieve the input parameters
			@SuppressWarnings("unused")
			int step = 0;
			try {
				step = UTCommon.getParameterAsInt(req, UTCommon.STEP);

			} catch (NumberFormatException e) {
				String message = "Internal data error.  Number data was expected but character data was found.  " + "("
				+ this.getClass().getName() + ": " + " CONTINUE=" + UTCommon.getParameterAsString(req, CONTINUE) + ")";
				UTCommon.showError(req, res, 0, message, e);
				return;
			}
			transtype = data.isAddGiftEmail() ? "G" : "N";
			if (transtype.equals(data.getResultEmailTranType().trim())) {
				if (transtype.equals("G")) {
					transtype = "N";
				} else {
					transtype = "G";
				}
			}

			if (!insertEmail(data.getPassword(), data.getEmailAddress(), customer.getCurrentAccount())) { // any errors
				errorMsg = "Unable to save data.  Please try again.";
				data.setErrorMsg(errorMsg);
				data.setErrorID(4);
				// save the current session's data object for the next servlet
				session.setAttribute(UTCommon.SESSION_FIRST_TIME, data);
				UTCommon.showUrl(res, ERROR_URL);
				return;
			} else { // Insert record into email file
				errorMsg = "";
				data.setErrorMsg(errorMsg);
				data.setErrorID(0);
				// save the current session's data object for the next servlet
				session.setAttribute(UTCommon.SESSION_FIRST_TIME, data);
				try { // Log in user with this information
					try {
						customer = CustomerBO.loginCustomer(data.getEmailAddress(), data.getPassword(), data.getPubCode());
						customer.setCurrentAccount(data.getAccountNumber());
						CustomerHandler ch = null;
						ch = (CustomerHandler) session.getAttribute("customerHandler");
						if (ch == null) {
							ch = new CustomerHandler();
							session.setAttribute("customerHandler", ch);
						}

						ch.setCustomer(customer);
						ch.setAuthenticated(true);
						ch.setCookieAuthenticated(false);
					} catch (Exception e) {
						System.out.println("unexpected exception: " + e.getMessage());
					}
					// save the current data into the session
					session.removeAttribute("UTCommon.SESSION_CUSTOMER_INFO");
					session.setAttribute(UTCommon.SESSION_CUSTOMER_INFO, customer);

				} catch (NullPointerException e) {
				}
				// display the next 'successful account'
				UTCommon.showUrl(res, INSERT);
				return;
			}
		} catch (Exception e) {
			errorMsg = "Transaction has completed.";
			// create a new data object
			data = new UTFirstTime();
			data.setAccountNumber("");
			data.setAddEmailPassLink(false);
			data.setAddGiftEmail(false);
			data.setDeliveryZip("");
			data.setEmailAddress("");
			data.setPassword("");
			data.setResultEmailTranType("");
			data.setPhoneNumber("");
			data.setErrorMsg(errorMsg);
			data.setErrorID(6);
			// save the current session's data object for the next servlet
			session.setAttribute(UTCommon.SESSION_FIRST_TIME, data);
			UTCommon.showUrl(res, ERROR_URL);
			return;
		}
	}

	/**
	 * Update password Creation date: (8/21/00 10:05:38 PM)
	 * 
	 * @return boolean
	 */

	public boolean updatePasswordOnly(UTFirstTime data) {

		// VERIFY CUSTOMER HAS AN ACCOUNT
		// Lookup users account
		Connection connection = null;
		try {
			// get a connection to the database
			connection = DataSourceConnectionFactory.getDBConnection();

			// create a prepared statement and substitute the parameters
			PreparedStatement statement = connection.prepareStatement(JDBC_UPDATE_PASSWORD);

			statement.setString(1, data.getPassword());
			statement.setString(2, UTCommon.getSystemDateYYYYMMDD());
			statement.setString(3, UTCommon.getSystemTimeHHMMSS());
			statement.setString(4, "");
			statement.setString(5, data.getEmailAddress());
			// statement.setString(6, data.getAccountNumber());

			// execute the SQL
			statement.execute();

			// get the result
			int count = statement.getUpdateCount();

			if (count == 0) {
				return false;
			}

			statement.close();
			return true;

		} catch (SQLException e) {
			return false;

		} catch (UsatDBConnectionException e) {
			return false;
		} finally {
			DataSourceConnectionFactory.closeConnection(connection);
		}

	}

	/**
	 * Login the current user; return true if successful, false otherwise Creation date: (8/21/00 10:05:38 PM)
	 * 
	 * @return boolean
	 */

	public boolean insertEmail(String webpasswordid, String emailid, SubscriberAccountIntf theAccount) {

		try {
			UsersResponse responseC = null;
			CreateUser cUser = new CreateUser();
			responseC = cUser.createUser(theAccount.getDeliveryContact().getFirstName(), theAccount.getDeliveryContact()
					.getLastName(), emailid, webpasswordid);  //Creating user, so no user ID is provided
			if (!responseC.containsErrors()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}

		/* 		// VERIFY CUSTOMER HAS AN ACCOUNT
		// Lookup users account
		boolean found = false;
		Connection connection = null;
		try {
			// get a connection to the database
			// get a connection to the database
			connection = DataSourceConnectionFactory.getDBConnection();

			// create a prepared statement and substitute the parameters
			PreparedStatement statement = connection.prepareStatement(JDBC_INSERT);
			statement.setString(1, emailid);
			statement.setString(2, webpasswordid);
			// Get system date for Permanent Start Date
			try {
				statement.setString(3, UTCommon.getSystemDateYYYYMMDD());
			} catch (NullPointerException e) {
				statement.setString(3, "");
			}
			// Get system time
			try {
				statement.setString(4, UTCommon.getSystemTimeHHMMSS());
			} catch (NullPointerException e) {
				statement.setString(4, "");
			}
			try {
				statement.setString(5, UTCommon.getSystemDateYYYYMMDD());
			} catch (NullPointerException e) {
				statement.setString(5, "");
			}

			// execute the SQL
			statement.execute();
			// get the result

			int count = statement.getUpdateCount();

			if (count == 0) {
				found = false;
			} else {
				found = true;
			}
			// close the database connection
			statement.close();
			connection.close();

			// return the result
			return found;

		} catch (SQLException e) {
			return false;
		} catch (UsatDBConnectionException e) {
			return false;
		} finally {
			DataSourceConnectionFactory.closeConnection(connection);
		}
		 */	}
}
