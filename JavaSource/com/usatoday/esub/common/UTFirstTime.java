/*
 * Created on May 26, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.usatoday.esub.common;

/**
 * @author hghavami
 * 
 *         To change the template for this generated type comment go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and
 *         Comments
 */
public class UTFirstTime extends EmailAlert {

	private String pubCode = null;
	private String accountNumber = null;
	private String phoneNumber = null;
	private String emailAddress = null;
	private String deliveryZip = null;
	private String password = null;
	private String errorMsg = null;
	private boolean addGiftEmail = false;
	private boolean addEmailPassLink = false;
	private int errorID = 0;
	private String resultEmailTranType = null;

	/**
	 * @return
	 */
	public String getAccountNumber() {
		return accountNumber;
	}

	/**
	 * @return
	 */
	public String getDeliveryZip() {
		return deliveryZip;
	}

	/**
	 * @return
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * @return
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param string
	 */
	public void setAccountNumber(String string) {
		accountNumber = string;
	}

	/**
	 * @param string
	 */
	public void setDeliveryZip(String string) {
		deliveryZip = string;
	}

	/**
	 * @param string
	 */
	public void setEmailAddress(String string) {
		emailAddress = string;
	}

	/**
	 * @param string
	 */
	public void setPassword(String string) {
		password = string;
	}

	/**
	 * @return
	 */
	public String getErrorMsg() {
		return errorMsg;
	}

	/**
	 * @param string
	 */
	public void setErrorMsg(String string) {
		errorMsg = string;
	}

	/**
	 * @return
	 */
	public int getErrorID() {
		return errorID;
	}

	/**
	 * @param i
	 */
	public void setErrorID(int i) {
		errorID = i;
	}

	/**
	 * @return
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param string
	 */
	public void setPhoneNumber(String string) {
		phoneNumber = string;
	}

	/**
	 * @return Returns the addGiftEmail.
	 */
	public boolean isAddGiftEmail() {
		return addGiftEmail;
	}

	/**
	 * @param addGiftEmail
	 *            The addGiftEmail to set.
	 */
	public void setAddGiftEmail(boolean addGiftEmail) {
		this.addGiftEmail = addGiftEmail;
	}

	/**
	 * @return Returns the addEmailPassLink.
	 */
	public boolean isAddEmailPassLink() {
		return addEmailPassLink;
	}

	/**
	 * @param addEmailPassLink
	 *            The addEmailPassLink to set.
	 */
	public void setAddEmailPassLink(boolean addEmailPassLink) {
		this.addEmailPassLink = addEmailPassLink;
	}

	/**
	 * @return Returns the pubCode.
	 */
	public String getPubCode() {
		return pubCode;
	}

	/**
	 * @param pubCode
	 *            The pubCode to set.
	 */
	public void setPubCode(String pubCode) {
		this.pubCode = pubCode;
	}

	/**
	 * @return Returns the resultEmailTranType.
	 */
	public String getResultEmailTranType() {
		return resultEmailTranType;
	}

	/**
	 * @param resultEmailTranType
	 *            The resultEmailTranType to set.
	 */
	public void setResultEmailTranType(String resultEmailTranType) {
		this.resultEmailTranType = resultEmailTranType;
	}
}
