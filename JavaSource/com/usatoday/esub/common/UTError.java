package com.usatoday.esub.common;

//
// UTError represents an error message and number to display to the user and add to the log.
// The number is included only if the number is > 0.  If the text is null, the DEFAULT_TEXT is used.
//

import javax.servlet.http.*;

public class UTError {
	public final static String DEFAULT_TEXT = "An error has occurred.";
	public final static String NO_SESSION_TEXT = "Servlet session was exected but does not exist.";
	public final static String NO_SESSION_DATA_TEXT = "Servlet session data object was expected but does not exist.";
	public final static String STEP_SEQUENCE_TEXT = "Servlet pages were accessed in an unexpected sequence.";
	public final static String COPPA_TEXT = "You have reached this page because USATODAY.com is in compliance with the Children's Online Privacy Protection Act (COPPA).  "
			+ "COPPA prohibits USATODAY.com and other Internet web sites from collecting personal information (e.g, name, email address, etc.) from children under the age of 13 "
			+ "without first obtaining verifiable parental consent and providing parents with the ability to review the information collected about their children's website "
			+ "activities.  USATODAY.com has decided to comply with COPPA simply by not collecting personal information from children under 13 at all.<br><br>"
			+ "According to the information you have entered, you are currently under the age of 13.  Because you are under the age of 13, we apologize but USATODAY.com cannot "
			+ "offer this service to you.<br><br>"
			+ "For more information about COPPA, visit <a href=\"http://www.cdt.org/legislation/105th/privacy/coppa.html\">http://www.cdt.org/legislation/105th/privacy/coppa.html</a>.";

	private int number = 0;
	private String text = null;
	private Exception exception = null;

	/**
	 * UTError constructor comment.
	 */
	public UTError() {
		super();
	}

	/**
	 * 
	 * @param number
	 *            int
	 * @param text
	 *            java.lang.String
	 */
	public UTError(int number, String text) {
		this.setNumber(number);
		this.setText(text);
	}

	/**
	 * 
	 * @param text
	 *            java.lang.String
	 */
	public UTError(String text) {
		this.setText(text);
	}

	/**
	 * 
	 * @return java.lang.Exception
	 */
	public Exception getException() {
		return exception;
	}

	/**
	 * 
	 * @return int
	 */
	public int getNumber() {
		return number;
	}

	/**
	 * 
	 * @return java.lang.String
	 */
	public String getText() {
		return text;
	}

	/**
	 * Log this error to the servlet log.
	 * 
	 * @param servlet
	 *            HttpServlet
	 */
	public void log(HttpServlet servlet) {
		try {
			this.exception.printStackTrace();
		} catch (NullPointerException e) {
			// No exception!
		}

		servlet.log(this.toString());
	}

	/**
	 * 
	 * @param newValue
	 *            java.lang.Exception
	 */
	public void setException(Exception newValue) {
		this.exception = newValue;
	}

	/**
	 * 
	 * @param newValue
	 *            int
	 */
	public void setNumber(int newValue) {
		this.number = newValue;
	}

	/**
	 * 
	 * @param newValue
	 *            java.lang.String
	 */
	public void setText(String newValue) {
		this.text = newValue;
	}

	/**
	 * Format this error as HTML.
	 * 
	 * @return java.lang.String
	 */
	public String toHTML() {
		return "<font color=\"#FF0000\"><b>" + this.toString() + "</b></font>";
	}

	/**
	 * 
	 * @return java.lang.String
	 */
	public String toString() {
		StringBuffer result = new StringBuffer();

		if (this.number > 0) {
			result.append(this.number);
			result.append(": ");
		}

		if (this.text != null) {
			result.append(this.text + "<br>");
		} else {
			result.append(DEFAULT_TEXT + "<br>");
		}

		if (this.exception != null) {
			result.append(this.exception + "<br>");
		}

		return result.toString();
	}
}
