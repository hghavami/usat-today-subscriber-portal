package com.usatoday.esub.common;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.gannett.usat.userserviceapi.client.ResetPassword;
import com.gannett.usat.userserviceapi.domainbeans.resetPassword.ResetPasswordResponse;
import com.usatoday.business.interfaces.customer.EmailRecordIntf;
import com.usatoday.business.interfaces.customer.SubscriberAccountIntf;
import com.usatoday.business.interfaces.customer.TrialCustomerIntf;
import com.usatoday.business.interfaces.products.SubscriptionProductIntf;
import com.usatoday.businessObjects.customer.EmailRecordBO;
import com.usatoday.businessObjects.customer.SubscriberAccountBO;
import com.usatoday.businessObjects.customer.TrialCustomerBO;
import com.usatoday.businessObjects.products.SubscriptionProductBO;
import com.usatoday.serviceHandler.trials.TrialCustomerForgotPassword;

public class ForgotPassword extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8034582902397431985L;

	public static final String COMPLETE_URL = UTCommon.ID_PASSWORD_URL + "/forgotpasswordcomplete.jsp";

	public static final String ERROR_URL = UTCommon.ID_PASSWORD_URL + "/forgotpassword.jsp";
	public static final String FORGOT_PASSWORD_INFO = "forgotPasswordInfo";
	public static final String MULTI_EMAIL_URL = UTCommon.ID_PASSWORD_URL + "/multiEmailConfirm.jsp";
	public static final String SINGLE_EMAIL_URL = UTCommon.ID_PASSWORD_URL + "/EmailConfirm.jsp";

	/**
	 * FirstTimeServlet constructor comment.
	 */
	public ForgotPassword() {
		super();
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		// Call the appropriate dopost methos based on the step

		int step = 0;
		try {
			step = UTCommon.getParameterAsInt(req, UTCommon.STEP);
		} catch (NumberFormatException e) {
			String message = "Internal data error.  Number data was expected but character data was found.  " + "("
					+ this.getClass().getName() + ": " + " STEP=" + UTCommon.getParameterAsString(req, UTCommon.STEP) + ")";
			UTCommon.showError(req, res, 0, message, e);
			return;
		}

		switch (step) {

		case 1:
			doPost1(req, res);
			break;

		case 2:
			doPost2(req, res);
			break;

		case 3:
			doPost3(req, res);
			break;
		}

	}

	public void doPost1(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		// get the session object; create a session if none exists
		HttpSession session = req.getSession(true);

		String errorMsg = null;
		// Get screen parameters
		ForgotPasswordRequestHelper requestData = new ForgotPasswordRequestHelper();

		try {
			String pubcode = (String) session.getAttribute(UTCommon.SESSION_PUB_NAME);
			if (pubcode == null) {
				pubcode = "UT";
			}
			SubscriptionProductIntf product = null;
			try {
				product = SubscriptionProductBO.getSubscriptionProduct(pubcode);
			} catch (Exception e) {
				try {
					product = SubscriptionProductBO.getUsaTodaySubscriptionProduct();
				} catch (Exception exp) {
				}
			}

			requestData.setPubCode(product.getBrandingPubCode());

			// Check if the customer does not account email and is using other means to get login information
			if (UTCommon.getCheckboxParameterAsBoolean(req, "NO_EMAIL_KNOWN")) {
				requestData.setNoEmailKnown(UTCommon.getCheckboxParameterAsBoolean(req, "NO_EMAIL_KNOWN"));
				requestData.setZipCode(UTCommon.getParameterAsString(req, "ZIPCODE").trim());
				// Either need account or phone number
				if (UTCommon.getParameterAsString(req, "ACCNT_ID") != null
						&& !UTCommon.getParameterAsString(req, "ACCNT_ID").trim().equals("")) {
					requestData.setAccntID(UTCommon.getParameterAsString(req, "ACCNT_ID").trim());
					// Check account number and zip code
					if (!queryEmailByAccountZipcode(requestData)) {
						errorMsg = "No login information for account number and zip code combination found."
								+ " Please try again or call Customer Service.";
						// save the current session's data object for the next servlet
						session.setAttribute(UTCommon.SESSION_FIELD_ERROR, errorMsg);
						UTCommon.showUrl(res, ERROR_URL);
						return;
					}
				} else {
					requestData.setPhoneNum1(UTCommon.getParameterAsString(req, "PHONE_NUM1").trim());
					requestData.setPhoneNum2(UTCommon.getParameterAsString(req, "PHONE_NUM2").trim());
					requestData.setPhoneNum3(UTCommon.getParameterAsString(req, "PHONE_NUM3").trim());
					if (!queryEmailByPhoneZipcode(requestData)) {
						errorMsg = "No login information for phone number and zip code combination found."
								+ " Please try again or call Customer Service.";
						// save the current session's data object for the next servlet
						session.setAttribute(UTCommon.SESSION_FIELD_ERROR, errorMsg);
						UTCommon.showUrl(res, ERROR_URL);
						return;
					}
				}
				// If multiple emails found send user to the screen to choose
				if (requestData.getEmailFound1() != null && !requestData.getEmailFound1().trim().equals("")
						&& requestData.getEmailFound2() != null && !requestData.getEmailFound2().trim().equals("")) {
					UTCommon.showUrl(res, MULTI_EMAIL_URL);
					session.setAttribute(FORGOT_PASSWORD_INFO, requestData);
					return;
				} else {
					UTCommon.showUrl(res, SINGLE_EMAIL_URL);
					session.setAttribute(FORGOT_PASSWORD_INFO, requestData);
					return;
				}

			} else {
				// Get email address to find login information
				requestData.setLoginID(UTCommon.getParameterAsString(req, "LOGIN_ID").trim().toLowerCase());
				// First check email address
				// if (!queryEmail(requestData)) {
				if (!resetPassword(requestData)) {
					// process trial customer forgot password.
					TrialCustomerIntf tCust = null;
					try {
						tCust = TrialCustomerBO.getTrialCustomerByEmail(requestData.getLoginID());
					} catch (Exception eeee) {
						tCust = null;
					}

					if (tCust != null) {
						try {
							TrialCustomerForgotPassword.sendPassword(tCust);
							UTCommon.showUrl(res, COMPLETE_URL);
							return;
						} catch (Exception trialSendFailed) {
							errorMsg = "Failed to send email to: '"
									+ requestData.getLoginID()
									+ "' not found. Error Detail: "
									+ trialSendFailed.getMessage();

							// save the current session's data object for the next servlet
							session.setAttribute(UTCommon.SESSION_FIELD_ERROR, errorMsg);
							UTCommon.showUrl(res, ERROR_URL);
							return;
						}
					} else {
						errorMsg = "Email '"
								+ requestData.getLoginID()
								+ "' not found.";

						// save the current session's data object for the next servlet
						session.setAttribute(UTCommon.SESSION_FIELD_ERROR, errorMsg);
						UTCommon.showUrl(res, ERROR_URL);
						return;
					}
				}
				// First check xtrntupld. With the addition of e-Editon, no longer need to check the accounts table
				/*
				 * if (!queryXtrntupld(requestData)) { errorMsg =
				 * "Account no longer valid.  Please try again or call Customer Service.";
				 * 
				 * //save the current session's data object for the next servlet session.setAttribute(UTCommon.SESSION_FIELD_ERROR,
				 * errorMsg); UTCommon.showUrl(res, ERROR_URL); return; }
				 */// Email login information for email address entered
					// sendEmail(requestData, req, res, 1);
				UTCommon.showUrl(res, COMPLETE_URL);
			}

		} catch (NullPointerException e) {
			errorMsg = "Please enter valid data.";

			// save the current session's data object for the next servlet
			session.setAttribute(UTCommon.SESSION_FIELD_ERROR, errorMsg);

			UTCommon.showUrl(res, ERROR_URL);
		}
	}

	public void doPost2(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		// get the session object; create a session if none exists
		HttpSession session = req.getSession(true);

		// set content type and other response header fields
		UTCommon.setResponseHeader(res);

		String errorMsg = null;
		try {
			// Get screen parameters
			boolean email1Chosen = UTCommon.getCheckboxParameterAsBoolean(req, "EMAIL1");
			boolean email2Chosen = UTCommon.getCheckboxParameterAsBoolean(req, "EMAIL2");
			ForgotPasswordRequestHelper requestData = (ForgotPasswordRequestHelper) session.getAttribute(FORGOT_PASSWORD_INFO);

			if (email1Chosen) {
				sendEmail(requestData, req, res, 2);
			}

			if (email2Chosen) {
				sendEmail(requestData, req, res, 3);
			}

		} catch (NullPointerException e) {
			errorMsg = "Please choose one of the email addresses.";

			// save the current session's data object for the next servlet
			session.setAttribute(UTCommon.SESSION_FIELD_ERROR, errorMsg);

			UTCommon.showUrl(res, ERROR_URL);
		}
	}

	public void doPost3(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		// get the session object; create a session if none exists
		HttpSession session = req.getSession(true);

		// set content type and other response header fields
		UTCommon.setResponseHeader(res);

		String errorMsg = null;
		try {
			// Get screen parameters
			ForgotPasswordRequestHelper requestData = (ForgotPasswordRequestHelper) session.getAttribute(FORGOT_PASSWORD_INFO);

			// Email login information for 1 email address found
			if (requestData.getEmailFound1() != null && !requestData.getEmailFound1().trim().equals("")) {
				sendEmail(requestData, req, res, 2);
			}
			if (requestData.getEmailFound2() != null && !requestData.getEmailFound2().trim().equals("")) {
				sendEmail(requestData, req, res, 3);
			}

		} catch (NullPointerException e) {
			errorMsg = "Login information could not be sent to the Email address.";

			// save the current session's data object for the next servlet
			session.setAttribute(UTCommon.SESSION_FIELD_ERROR, errorMsg);

			UTCommon.showUrl(res, ERROR_URL);
		}
	}

	/**
	 * Check current User ID and Password Creation date: (8/21/00 10:05:38 PM)
	 * 
	 * @return boolean
	 */

	public boolean queryEmail(ForgotPasswordRequestHelper data) {

		boolean found = false;
		try {
			Collection<EmailRecordIntf> emailRecords = EmailRecordBO.getEmailRecordsForEmailAddress(data.getLoginID());
			Iterator<EmailRecordIntf> itr = emailRecords.iterator();

			int numRecords = emailRecords.size();

			if (numRecords == 1) {
				EmailRecordIntf eRec = itr.next();
				found = true;
				data.setCurrPassword(eRec.getPassword());
				if (!data.getPubCode().equalsIgnoreCase(eRec.getPubCode())) {
					SubscriptionProductIntf product = null;
					try {
						product = SubscriptionProductBO.getSubscriptionProduct(eRec.getPubCode());
					} catch (Exception e) {
						try {
							product = SubscriptionProductBO.getUsaTodaySubscriptionProduct();
						} catch (Exception exp) {
						}
					}
					data.setPubCode(product.getBrandingPubCode());
				}

			} else {
				// multiple email records..update them to be same password if necessary

				boolean gotPassword = false;
				EmailRecordIntf goodRec = null;
				data.setCurrPassword("");
				while (itr.hasNext()) {
					EmailRecordIntf eRec = itr.next();

					// first time grab password
					if (!gotPassword) {
						if (eRec.getPassword() != null && eRec.getPassword().trim().length() > 0) {
							goodRec = eRec;
							gotPassword = true;
							break;
						} else {
							continue;
						}
					}
				}

				if (gotPassword) {

					found = true;
					data.setCurrPassword(goodRec.getPassword());

					SubscriptionProductIntf product = SubscriptionProductBO.getSubscriptionProduct(goodRec.getPubCode());
					data.setPubCode(product.getBrandingPubCode());

					// only update if less than 10 records.
					if (emailRecords.size() < 10) {
						itr = emailRecords.iterator();
						while (itr.hasNext()) {
							EmailRecordIntf eRec = itr.next();
							// update any other accounts with the passord
							if (!goodRec.getPassword().equalsIgnoreCase(eRec.getPassword())) {

								try {
									if (eRec instanceof EmailRecordBO) {
										eRec.setPassword(goodRec.getPassword());
										EmailRecordBO eBo = (EmailRecordBO) eRec;
										eBo.save();
									}
								} catch (Exception e) {
									;
								}

							}
						} // end while more email records
					} // end if less than 10 records
				} // if at least one has a valid password
			}
		} catch (Exception e) {
			try {
				SubscriptionProductIntf product = null;
				product = SubscriptionProductBO.getUsaTodaySubscriptionProduct();
				data.setPubCode(product.getBrandingPubCode());
			} catch (Exception e1) {
				return false;
			}

		}
		return found;
	}

	/**
	 * Check current User ID and Password Creation date: (8/21/00 10:05:38 PM)
	 * 
	 * @return boolean
	 */

	public boolean queryEmailByAccountZipcode(ForgotPasswordRequestHelper data) {

		boolean found = false;

		try {
			// VERIFY CUSTOMER HAS AN XTRNTUPLD RECORD
			// Lookup users account
//			if (queryXtrntupldByAccountZipcode(data)) {
			Collection<EmailRecordIntf> emailRecords = EmailRecordBO.getEmailRecordsForAccountPub(data.getPubCode(),
					data.getAccntID());
			if (emailRecords.isEmpty()) {
				return false;
			}
			Iterator<EmailRecordIntf> itr = emailRecords.iterator();

			boolean nSubTypeFound = false; // N type subscription found
			boolean gSubTypeFound = false; // G type subscription found
			// The returned collection is sorted by descending serial number to have the latest records first
			while (itr.hasNext()) {
				EmailRecordIntf eRec = itr.next();
				if (!eRec.isGift()) { // N type subscription
					if (!nSubTypeFound) {
						nSubTypeFound = true;
						data.setEmailFound1(eRec.getEmailAddress());
						data.setCurrPassword1(eRec.getPassword());
						found = true;
					}
				} else {
					if (!gSubTypeFound) {
						gSubTypeFound = true;
						data.setEmailFound2(eRec.getEmailAddress());
						data.setCurrPassword2(eRec.getPassword());
						found = true;
					}
				}
			}
		} catch (Exception e) {
			return false;
		}

		return found;
	}

	/**
	 * Check current User ID and Password Creation date: (8/21/00 10:05:38 PM)
	 * 
	 * @return boolean
	 */

	public boolean queryEmailByPhoneZipcode(ForgotPasswordRequestHelper data) {

		boolean found = false;

		try {
			// VERIFY CUSTOMER HAS AN XTRNTUPLD RECORD
			// Lookup users account
			if (queryXtrntupldByPhoneZipcode(data)) {
				Collection<EmailRecordIntf> emailRecords = EmailRecordBO.getEmailRecordsForAccountPub(data.getPubCode(),
						data.getAccntID());
				if (!emailRecords.isEmpty()) {
					Iterator<EmailRecordIntf> itr = emailRecords.iterator();

					boolean nSubTypeFound = false; // N type subscription found
					boolean gSubTypeFound = false; // G type subscription found
					// The returned collection is sorted by descending serial number to have the latest records first
					while (itr.hasNext()) {
						EmailRecordIntf eRec = itr.next();
						if (!eRec.isGift()) { // N type subscription
							if (!nSubTypeFound) {
								nSubTypeFound = true;
								data.setEmailFound1(eRec.getEmailAddress());
								data.setCurrPassword1(eRec.getPassword());
								found = true;
							}
						} else {
							if (!gSubTypeFound) {
								gSubTypeFound = true;
								data.setEmailFound2(eRec.getEmailAddress());
								data.setCurrPassword2(eRec.getPassword());
								found = true;
							}
						}
					}
				}
			}

		} catch (Exception e) {
			return false;
		}

		return found;
	}

	/**
	 * Check current User ID and Password Creation date: (8/21/00 10:05:38 PM)
	 * 
	 * @return boolean
	 */

	public boolean queryXtrntupld(ForgotPasswordRequestHelper data) {

		boolean found = false;

		try {
			Collection<EmailRecordIntf> emailRecords = EmailRecordBO.getEmailRecordsForEmailAddress(data.getLoginID());
			Iterator<EmailRecordIntf> itr = emailRecords.iterator();

			while (itr.hasNext()) {
				EmailRecordIntf eRec = itr.next();
				SubscriberAccountIntf acct = null;
				acct = SubscriberAccountBO.getSubscriberAccount(eRec.getAccountNumber());
				if (acct != null) {
					found = true;

					break;

				}
			}

		} catch (Exception e) {
			return false;
		}

		return found;
	}

	/**
	 * Check current User ID and Password Creation date: (8/21/00 10:05:38 PM)
	 * 
	 * @return boolean
	 */

	public boolean queryXtrntupldByAccountZipcode(ForgotPasswordRequestHelper data) {

		boolean found = false;

		try {
			// First check if account exists in XTRNTUPLD table
			Collection<SubscriberAccountIntf> subAccountRecords = SubscriberAccountBO.getSubscriberAccountByAccountNumPub(
					data.getAccntID(), data.getPubCode());
			if (!subAccountRecords.isEmpty()) {
				Iterator<SubscriberAccountIntf> itr = subAccountRecords.iterator();
				while (itr.hasNext()) {
					SubscriberAccountIntf acct = itr.next();
					// Customer entered zipcode and the account's must match
					if (acct.getDeliveryContact().getPersistentAddress().getZip().equals(data.getZipCode())) {
						found = true;
					}
				}
			}
		} catch (Exception e) {
			return false;
		}

		return found;
	}

	/**
	 * Check current User ID and Password Creation date: (8/21/00 10:05:38 PM)
	 * 
	 * @return boolean
	 */

	public boolean queryXtrntupldByPhoneZipcode(ForgotPasswordRequestHelper data) {

		boolean found = false;

		try {
			// First check if account exists in XTRNTUPLD table
			Collection<SubscriberAccountIntf> subAccountRecords = SubscriberAccountBO.getSubscriberAccountsForPhoneAndZip(
					data.getPhoneNumComplete(), data.getZipCode());
			if (!subAccountRecords.isEmpty()) {
				Iterator<SubscriberAccountIntf> itr = subAccountRecords.iterator();
				while (itr.hasNext()) {
					SubscriberAccountIntf acct = itr.next();
					// Customer entered zipcode and the account's must match
					if (acct.getDeliveryContact().getPersistentAddress().getZip().equals(data.getZipCode())) {
						data.setAccntID(acct.getAccountNumber());
						found = true;
					}
				}
			}
		} catch (Exception e) {
			return false;
		}

		return found;
	}

	public boolean resetPassword(ForgotPasswordRequestHelper data) {
		boolean emailFound = false;
		try {
			ResetPasswordResponse response = null;
			ResetPassword resetPassword = new ResetPassword();
			response = resetPassword.getResetPassword(data.getLoginID());
			if (!response.containsErrors()) {
				emailFound = true;
				SubscriptionProductIntf product = null;
				product = SubscriptionProductBO.getUsaTodaySubscriptionProduct();
				data.setPubCode(product.getBrandingPubCode());
			}
		} catch (Exception e) {
			System.out.println("Failed to reset password in Atypon alert: " + e);
		}
		return emailFound;
	}

	public boolean sendEmail(ForgotPasswordRequestHelper data, HttpServletRequest req, HttpServletResponse res, int whichLogin) {

		boolean success = false;
		boolean showError = false;
		HttpSession session = req.getSession(true);
		String emailAddress = null;
		String password = null;

		// Determine which login information to use for sending email
		try {
			switch (whichLogin) {

			case 1:
				emailAddress = data.getLoginID();
				password = data.getCurrPassword();
				break;

			case 2:
				emailAddress = data.getEmailFound1();
				password = data.getCurrPassword1();
				break;

			case 3:
				emailAddress = data.getEmailFound2();
				password = data.getCurrPassword2();
				break;
			}

			String emailSubject = UTCommon.EMAIL_SUBJECT;
			if (data.getPubCode().equalsIgnoreCase(UTCommon.BWPUBCODE)) {
				emailSubject = emailSubject.replaceAll("USA TODAY", "Sports Weekly");
			}

			try {
				if ((password != null) && (password.trim().length() > 0)) {
					UTCommon.sendEmail(session, emailAddress, emailSubject, data.toString(emailAddress, password));
				} else {
					UTCommon.sendEmail(session, emailAddress, emailSubject, data.toStringBlankPassword());
				}
			} catch (Exception e1) {
				e1.printStackTrace();
				this.log("E-mail not sent user: toEmail=" + data.getLoginID() + " subject=" + emailSubject);

				String errorMsg = "Email not sent.  Please call Customer Service at the number below for assistance.";

				// save the current session's data object for the next servlet
				session.removeAttribute(UTCommon.SESSION_FIELD_ERROR);
				session.setAttribute(UTCommon.SESSION_FIELD_ERROR, errorMsg);
				showError = true;
			}

			try {
				if ((password != null) && (password.trim().length() > 0)) {
					UTCommon.sendEmail(session, UTCommon.BLINDCC_EMAIL, emailSubject, data.toString(emailAddress, password));
				} else {
					UTCommon.sendEmail(session, UTCommon.BLINDCC_EMAIL, emailSubject, data.toStringBlankPassword());
				}
			} catch (Exception e2) {

				this.log("E-mail not sent to BLINDCC: toEmail=" + UTCommon.BLINDCC_EMAIL + " subject=" + emailSubject);
			}
			try {
				if ((password != null) && (password.trim().length() > 0)) {
					UTCommon.sendEmail(session, UTCommon.DEVCC_EMAIL, emailSubject, data.toString(emailAddress, password));
				} else {
					UTCommon.sendEmail(session, UTCommon.DEVCC_EMAIL, emailSubject, data.toStringBlankPassword());
				}
			} catch (Exception e3) {

				this.log("Forgot Password E-mail not sent DEVCC account: toEmail=" + UTCommon.DEVCC_EMAIL + " subject="
						+ emailSubject);
			}

			if (showError) {
				UTCommon.showUrl(res, ERROR_URL);
			} else {
				UTCommon.showUrl(res, COMPLETE_URL);
			}
		} catch (Exception e) {
			return false;
		}
		return success;
	}

	/**
	 * Check current User ID and Password Creation date: (8/21/00 10:05:38 PM)
	 * 
	 * @return boolean
	 */

	public String queryGenesysEmailByAccountZipcode(ForgotPasswordRequestHelper data) {

		String foundEmail = "";

		try {
			// First check if account exists in XTRNTUPLD table
			Collection<SubscriberAccountIntf> subAccountRecords = SubscriberAccountBO.getSubscriberAccountByAccountNumPub(
					data.getAccntID(), data.getPubCode());
			if (!subAccountRecords.isEmpty()) {
				Iterator<SubscriberAccountIntf> itr = subAccountRecords.iterator();
				while (itr.hasNext()) {
					SubscriberAccountIntf acct = itr.next();
					// Customer entered zipcode and the account's must match
					if (acct.getDeliveryContact().getPersistentAddress().getZip().equals(data.getZipCode())) {
						foundEmail = acct.getDeliveryContact().getEmailAddress();
					}
				}
			}
		} catch (Exception e) {
			return "";
		}

		return foundEmail;
	}
}
