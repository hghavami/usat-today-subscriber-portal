package com.usatoday.esub.common;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @version 1.0
 * @author
 */
public class DomainFilter implements Filter {

	private String defaultRedirectDomain = "https://service.usatoday.com";

	int totalRedirectCount = 0;

	int totalRequestCount = 0;

	/**
	 * @see javax.servlet.Filter#void ()
	 */
	public void destroy() {

	}

	/**
	 * @see javax.servlet.Filter#void (javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {

		this.totalRequestCount++;

		// System.out.println("Inside Domain Filter");

		if (req instanceof HttpServletRequest) {

			HttpServletRequest request = (HttpServletRequest) req;

			String reqStr = request.getRequestURL().toString();
			reqStr = reqStr.toLowerCase();

			if (reqStr.indexOf("service.usatoday.com") > 0) {
				// continue on
				chain.doFilter(req, resp);
				return;
			}
			String redirectURL = "";
			// if coming in on vanity then redirect to regular domain
			if (reqStr.indexOf("subscribe.usatoday.com") > 0 || reqStr.indexOf("myusatoday.com") > 0
					|| reqStr.indexOf("mysportsweekly.com") > 0 || reqStr.indexOf("usatodaycustomer.com") > 0
					|| reqStr.indexOf("usatodayprint.com") > 0 || reqStr.indexOf("usatodayservice.com") > 0
					|| reqStr.indexOf("usatodaysubscribe.com") > 0 || reqStr.indexOf("usatodayservices.com") > 0) {

				redirectURL = this.generateRedirectURLStr(request);
			}
			if (redirectURL != null && !redirectURL.equals("")) {
				UTCommon.showUrl((HttpServletResponse) resp, redirectURL);
				this.totalRedirectCount++;

				if ((this.totalRedirectCount % 1000) == 0) {
					System.out.println("Domain Filter Stats: Total Requests=" + this.totalRequestCount + " Total Redirects="
							+ this.totalRedirectCount);
				}
				return;
			}
		}

		chain.doFilter(req, resp);

	}

	/**
	 * Method init.
	 * 
	 * @param config
	 * @throws javax.servlet.ServletException
	 */
	public void init(FilterConfig config) throws ServletException {

	}

	/**
	 * @return
	 */
	public String getDefaultRedirectDomain() {
		return defaultRedirectDomain;
	}

	private String generateRedirectURLStr(HttpServletRequest request) {
		StringBuffer reqStr = new StringBuffer();

		// if no redirect URL configured in UTCommon/ini file
		if (UTCommon.REDIRECT_URL == null || UTCommon.REDIRECT_URL.trim().length() == 0) {
			reqStr.append(this.getDefaultRedirectDomain());
		} else {
			reqStr.append(UTCommon.REDIRECT_URL);
		}

		if (request.getRequestURI() != null) {
			reqStr.append(request.getRequestURI());
		}

		if (request.getQueryString() != null) {
			reqStr.append("?").append(request.getQueryString());
		}

		return reqStr.toString();
	}
}
