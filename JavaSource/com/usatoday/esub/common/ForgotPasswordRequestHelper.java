/*
 * Created on Apr 15, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.usatoday.esub.common;

/**
 * @author aeast
 * 
 *         To change the template for this generated type comment go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and
 *         Comments
 */
public class ForgotPasswordRequestHelper extends USATRequestHelper {
	private java.lang.String loginID = null;
	private boolean noEmailKnown = false;
	private java.lang.String accntID = null;
	private String phoneNum1 = null;
	private String phoneNum2 = null;
	private String phoneNum3 = null;
	private String zipCode = null;
	private java.lang.String currPassword = null;
	private java.lang.String currPassword1 = null;
	private java.lang.String currPassword2 = null;
	private String emailFound1 = null;
	private String emailFound2 = null;

	/**
	 * @return
	 */
	public java.lang.String getCurrPassword() {
		return currPassword;
	}

	/**
	 * @return
	 */
	public java.lang.String getLoginID() {
		return loginID;
	}

	/**
	 * @param string
	 */
	public void setCurrPassword(java.lang.String string) {
		currPassword = string;
	}

	/**
	 * @param string
	 */
	public void setLoginID(java.lang.String string) {
		loginID = string;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString(String emailAddress, String password) {

		StringBuffer forgotPasswordEmailText = new StringBuffer();

		forgotPasswordEmailText.append("\n\nThank you for informing ");
		forgotPasswordEmailText.append(this.getPublicationName());
		forgotPasswordEmailText
				.append(" of your subscription needs.  Our records indicate your email address and password are as follows: \n\n");
		forgotPasswordEmailText.append("Email Address: ").append(emailAddress);
		forgotPasswordEmailText.append("\nPassword: ").append(password);
		forgotPasswordEmailText.append("\n\nIf you have any questions about your subscription, please call us at ");
		forgotPasswordEmailText.append(this.getPhoneNumber());
		forgotPasswordEmailText.append(".");
		forgotPasswordEmailText.append("\n\n");
		forgotPasswordEmailText.append(this.getUsatMailingAddressFooter());

		return forgotPasswordEmailText.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toStringBlankPassword() {

		StringBuffer forgotPasswordEmailText = new StringBuffer();

		String subURL = "https://service.usatoday.com/firsttime/first_time.jsp";
		if (this.getPubCode() != null && !(this.getPubCode().equals("UT"))) {
			subURL = "https://service.usatoday.com/firsttime/first_time.jsp";
		}

		forgotPasswordEmailText.append("\n\nThank you for informing ");
		forgotPasswordEmailText.append(this.getPublicationName());
		forgotPasswordEmailText.append(" of your subscription needs.  Our records indicate your email address is as follows: \n\n");
		forgotPasswordEmailText.append(this.loginID);
		forgotPasswordEmailText.append("\n\n");
		forgotPasswordEmailText.append("In order to access your subscription information online, you will need \n");
		forgotPasswordEmailText.append("to set up your account.  To do this, please use the following link:");
		forgotPasswordEmailText.append("\n\n");
		forgotPasswordEmailText.append(subURL);
		forgotPasswordEmailText.append("\n\n");
		forgotPasswordEmailText.append("This will allow you to perform any of the following services online when it \n");
		forgotPasswordEmailText.append("is convenient for you:");
		forgotPasswordEmailText.append("\n\n");
		forgotPasswordEmailText.append("Suspend/Resume Delivery \n");
		forgotPasswordEmailText.append("Pay Your Bill \n");
		forgotPasswordEmailText.append("Change Your Delivery Information \n");
		forgotPasswordEmailText.append("Change Your Billing Information \n");
		forgotPasswordEmailText.append("Report a Delivery Problem \n");
		forgotPasswordEmailText.append("Buy a Gift Subscription and More... ");
		forgotPasswordEmailText.append("\n\n");
		forgotPasswordEmailText.append("If you have any questions about your subscription, please call us at ");
		forgotPasswordEmailText.append(this.getPhoneNumber());
		forgotPasswordEmailText.append(".");
		forgotPasswordEmailText.append("\n\n");
		forgotPasswordEmailText.append(this.getUsatMailingAddressFooter());

		return forgotPasswordEmailText.toString();
	}

	public java.lang.String getAccntID() {
		return this.accntID;
	}

	public void setAccntID(java.lang.String accntID) {
		this.accntID = accntID;
	}

	public String getPhoneNum1() {
		return this.phoneNum1;
	}

	public void setPhoneNum1(String phoneNum1) {
		this.phoneNum1 = phoneNum1;
	}

	public String getPhoneNum2() {
		return this.phoneNum2;
	}

	public void setPhoneNum2(String phoneNum2) {
		this.phoneNum2 = phoneNum2;
	}

	public String getPhoneNum3() {
		return this.phoneNum3;
	}

	public void setPhoneNum3(String phoneNum3) {
		this.phoneNum3 = phoneNum3;
	}

	public String getZipCode() {
		return this.zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public boolean isNoEmailKnown() {
		return this.noEmailKnown;
	}

	public void setNoEmailKnown(boolean noEmailKnown) {
		this.noEmailKnown = noEmailKnown;
	}

	public String getEmailFound1() {
		return this.emailFound1;
	}

	public void setEmailFound1(String emailFound1) {
		this.emailFound1 = emailFound1;
	}

	public String getEmailFound2() {
		return this.emailFound2;
	}

	public void setEmailFound2(String emailFound2) {
		this.emailFound2 = emailFound2;
	}

	public String getPhoneNumComplete() {
		return this.phoneNum1 + this.phoneNum2 + this.phoneNum3;
	}

	public void setPhoneNumComplete(String phoneComplete) {
	}

	public java.lang.String getCurrPassword2() {
		return this.currPassword2;
	}

	public void setCurrPassword2(java.lang.String currPassword2) {
		this.currPassword2 = currPassword2;
	}

	public java.lang.String getCurrPassword1() {
		return this.currPassword1;
	}

	public void setCurrPassword1(java.lang.String currPassword1) {
		this.currPassword1 = currPassword1;
	}
}
