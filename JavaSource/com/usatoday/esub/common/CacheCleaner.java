/*
 * Created on May 31, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.esub.common;

import java.io.PrintStream;

import org.joda.time.DateTime;

import com.usatoday.businessObjects.partners.TrialPartnerBO;
import com.usatoday.businessObjects.products.ProductBO;
import com.usatoday.businessObjects.products.SubscriptionPublishDateUtility;
import com.usatoday.util.constants.UsaTodayConstants;

/**
 * @author aeast
 * @date May 31, 2006
 * @class CacheCleaner
 * 
 * 
 */
public class CacheCleaner implements Runnable {

	DateTime nextCleanTime = null;
	boolean done = false;

	PrintStream stdoutStream = null;

	/**
     * 
     */
	public CacheCleaner() {
		super();
		this.stdoutStream = System.out;

	}

	public static void cleanCaches() {
		try {
			// UTCommon configs
			com.usatoday.esub.common.UTCommon.getInitValues();
			// reload config files
			com.usatoday.util.constants.UsaTodayConstants.loadProperties();
			com.usatoday.businessObjects.shopping.payment.PTIConfig.resetConfig();
			com.usatoday.businessObjects.util.Code1Config.resetConfig();

			ProductBO.clearCachedProducts();

			com.usatoday.businessObjects.products.promotions.PromotionManager.getInstance().clearCache();
			// com.usatoday.businessObjects.products.SubscriptionTermsBO.resetDefaultForceEZPAYTerms();
			com.usatoday.businessObjects.products.SubscriptionOfferManager.getInstance().clearCache();

			SubscriptionPublishDateUtility.resetCache();

			TrialPartnerBO.reloadPartnerCache();

			com.usatoday.businessObjects.util.TaxRateManager.getInstance().clearCache();
			com.usatoday.businessObjects.util.GUIZipCodeManager.getInstance().clearCache();
		} catch (Exception e) {
			System.out.println("Cache Cleaner Failed to Clean Caches: " + e.getMessage());
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		DateTime now = new DateTime();

		// load initial stuff
		CacheCleaner.cleanCaches();

		if (UsaTodayConstants.debug) {
			this.nextCleanTime = new DateTime();
			this.nextCleanTime = this.nextCleanTime.plusMinutes(30);
		} else {
			this.nextCleanTime = new DateTime(now.getYear(), now.getMonthOfYear(), now.getDayOfMonth(), 17, 30, 0, 0);
			if (nextCleanTime.isBeforeNow()) {
				this.nextCleanTime = this.nextCleanTime.plusDays(1);
			}
		}

		while (!done) {
			long sleepTime = this.nextCleanTime.getMillis() - now.getMillis();
			try {

				Thread.sleep(sleepTime);

				System.out.println(">>>>>>>>    Cache Cleaner clearing caches..........");

				// UTCommon configs
				try {
					com.usatoday.esub.common.UTCommon.getInitValues();
				} catch (Exception e) {
					e.printStackTrace();
				}

				// redesign configs
				com.usatoday.util.constants.UsaTodayConstants.loadProperties();
				try {
					com.usatoday.businessObjects.shopping.payment.PTIConfig.resetConfig();
				} catch (Exception e) {
					e.printStackTrace();
				}
				com.usatoday.businessObjects.util.Code1Config.resetConfig();

				// clear all caches
				CacheCleaner.cleanCaches();

				System.out.println(">>>>>>>>    Cache Cleaner: Caches Cleaned.");

				if (UsaTodayConstants.debug) {
					this.nextCleanTime = this.nextCleanTime.plusMinutes(30);
				} else {
					this.nextCleanTime = this.nextCleanTime.plusDays(1);
				}
				now = new DateTime();

			} catch (InterruptedException ie) {
				this.done = true;
			}
		}

		System.out.println(">>>>>>  Cache Cleaner Thread terminating.");

	}

	/**
	 * @return Returns the nextCleanTime.
	 */
	public DateTime getNextCleanTime() {
		return this.nextCleanTime;
	}

	/**
	 * @param nextCleanTime
	 *            The nextCleanTime to set.
	 */
	public void setNextCleanTime(DateTime nextCleanTime) {
		this.nextCleanTime = nextCleanTime;
	}

	/**
	 * @return Returns the stdoutStream.
	 */
	public PrintStream getStdoutStream() {
		return this.stdoutStream;
	}

	/**
	 * @param stdoutStream
	 *            The stdoutStream to set.
	 */
	public void setStdoutStream(PrintStream stdoutStream) {
		this.stdoutStream = stdoutStream;
	}
}
