/*
 * Created on Apr 15, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.usatoday.esub.common;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.SubscriptionProductIntf;
import com.usatoday.businessObjects.products.SubscriptionProductBO;
import com.usatoday.util.constants.UsaTodayConstants;

/**
 * @author aeast
 * 
 *         To change the template for this generated type comment go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and
 *         Comments
 */
public abstract class PublicationDates {

	public static final String JDBC_SQL = "SELECT AccountNum, PubCode from XTRNTEMAIL where EmailAddress=? and WebPASSWORD=?";
	public static final String JDBC_PUBDATE = "SELECT * from XTRNTPUBDATE where PubCode=? and PubDate=?";
	public static final String JDBC_PUBCALC = "SELECT * from XTRNTPUBDATE where PubCode=? and PubDate >? Order By PubDate";
	public static final String JDBC_PUBEND = "SELECT * from XTRNTPUBDATE where PubCode=? and PubDate <=?";
	public static final String JDBC_PUB_MAX_DATE = "SELECT MAX(PubDate) FROM XTRNTPUBDATE where pubcode = ?";
	protected static final String INVALID_MESSAGE = "E-mail and/or password not valid.  Please retry.";
	protected static final String DEFAULT_MESSAGE = "Please login to continue.";

	private java.lang.String message = null;
	private java.lang.String pubDate = null;
	private java.lang.String end_date = null;
	private java.lang.String pubCode = null;
	private java.lang.String new_pubdate = null;

	/**
	 * 
	 */
	public PublicationDates() {
		super();
	}

	public PublicationDates(String publication, String begin, String end) {
		this.setPubCode(publication);
		this.setPubDate(begin);
		this.setEnd_date(end);
	}

	/**
	 * @return
	 */
	public java.lang.String getEnd_date() {
		return end_date;
	}

	/**
	 * 
	 * @param end_date
	 * @return
	 */
	public String getEndDate(String end_date) {

		HashMap<String, String> datemap = PublicationDates.getDateFields(end_date);

		String year = datemap.get("YEAR");
		String month = datemap.get("MONTH");
		String day = datemap.get("DAY");

		// set calendar
		GregorianCalendar endingDate = new GregorianCalendar();

		endingDate.set(Integer.parseInt(year), (Integer.parseInt(month) - 1), Integer.parseInt(day));

		// work date backwards to get previous valid pub date
		// endingDate.add(Calendar.DATE, -1);

		// work date forward to get next valid pub date
		endingDate.add(Calendar.DATE, +1);

		int endPubDay = endingDate.get(Calendar.DATE);
		int endPubMth = endingDate.get(Calendar.MONTH);
		int endPubYear = endingDate.get(Calendar.YEAR);

		String endPubDateNew = ((endPubMth + 1) + "/" + endPubDay + "/" + endPubYear);

		return endPubDateNew;

	}

	/**
	 * @return
	 */
	public java.lang.String getMessage() {
		return message;
	}

	/**
	 * @return
	 */
	public java.lang.String getNew_pubdate() {
		return new_pubdate;
	}

	/**
	 * @return
	 */
	public java.lang.String getPubCode() {
		return pubCode;
	}

	/**
	 * @return
	 */
	public java.lang.String getPubDate() {
		return pubDate;
	}

	/**
	 * @param string
	 */
	public void setEnd_date(java.lang.String string) {
		end_date = string;
	}

	/**
	 * @param string
	 */
	public void setMessage(java.lang.String string) {
		message = string;
	}

	/**
	 * @param string
	 */
	public void setNew_pubdate(java.lang.String string) {
		new_pubdate = string;
	}

	/**
	 * @param string
	 */
	public void setPubCode(java.lang.String string) {
		pubCode = string;
	}

	/**
	 * @param string
	 */
	public void setPubDate(java.lang.String string) {
		pubDate = string;
	}

	/**
	 * 
	 * @return boolean
	 */
	public boolean validPubDate() {

		Connection connection = null;
		try {
			// get a connection to the database
			connection = DataSourceConnectionFactory.getDBConnection();

			// create a prepared statement and substitute the parameters
			PreparedStatement statement = connection.prepareStatement(JDBC_PUBDATE);
			statement.setString(1, this.getPubCode());
			statement.setString(2, this.getPubDate());

			statement.execute();

			// get the result
			ResultSet resultSet = statement.getResultSet();
			boolean resultFound = false;
			while (resultSet.next()) {
				resultFound = true;
				break;
			}

			// close the database connection
			statement.close();
			connection.close();

			// inspect the result
			if (!resultFound) {
				// PubCode/PubDate not valid
				return false;
			} else {
				return true;
			}
		} catch (SQLException e) {
			return false;
		} catch (UsatDBConnectionException e) {
			return false;
		} finally {
			DataSourceConnectionFactory.closeConnection(connection);
		}
	}

	public boolean isFuturePubDate(String tranType) {
		boolean isFutureDate = false;

		if (this.pubDate == null) {
			return false;
		}

		try {
			// now check that the date is greater than or equal today
			HashMap<String, String> datemap = PublicationDates.getDateFields(this.pubDate);

			String year = datemap.get("YEAR");
			String month = datemap.get("MONTH");
			String day = datemap.get("DAY");
			String earliestPossibleStart = "";

			// set calendar
			GregorianCalendar selectedDate = new GregorianCalendar();

			selectedDate.set(Integer.parseInt(year), (Integer.parseInt(month) - 1), Integer.parseInt(day), 23, 59);

			SubscriptionProductIntf product = SubscriptionProductBO.getSubscriptionProduct(this.getPubCode());

			if (tranType.equals(UTCommon.TRAN_NEW_START_DATE)) { // Check for new start date processing
				earliestPossibleStart = determineEarliestStart(this.getPubCode(), null);
			}
			if (tranType.equals(UTCommon.TRAN_STOP_DATE) || tranType.equals(UTCommon.TRAN_START_DATE)) { // Check for vac hold date
																											// processing
				DateTime dt = product.getEarliestPossibleHoldStartDate();
				earliestPossibleStart = dt.toString("MM/dd/yyyy");
			}
			datemap.clear();
			datemap = PublicationDates.getDateFields(earliestPossibleStart);
			year = datemap.get("YEAR");
			month = datemap.get("MONTH");
			day = datemap.get("DAY");

			Calendar earliestStart = Calendar.getInstance();
			earliestStart.set(Integer.parseInt(year), (Integer.parseInt(month) - 1), Integer.parseInt(day), 0, 0);

			if (!selectedDate.before(earliestStart)) {
				isFutureDate = true;
			}
		} catch (Exception e) {
			isFutureDate = false;
		}

		return isFutureDate;
	}

	/**
	 * 
	 * @param endPubCode
	 * @param endPubDate
	 * @return boolean
	 */
	public boolean validPubDate(String endPubCode, String endPubDate) {

		Connection connection = null;
		try {
			// get a connection to the database
			connection = DataSourceConnectionFactory.getDBConnection();

			// create a prepared statement and substitute the parameters
			PreparedStatement statement = connection.prepareStatement(JDBC_PUBDATE);
			statement.setString(1, endPubCode);
			statement.setString(2, endPubDate);

			statement.execute();

			// get the result
			ResultSet resultSet = statement.getResultSet();
			boolean resultFound = false;
			while (resultSet.next()) {
				resultFound = true;
				break;
			}

			// close the database connection
			statement.close();
			connection.close();

			// inspect the result
			if (!resultFound) {
				// PubCode/PubDate not valid
				return false;
			} else {
				return true;
			}
		} catch (SQLException e) {
			return false;
		} catch (UsatDBConnectionException e) {
			return false;
		} finally {
			DataSourceConnectionFactory.closeConnection(connection);
		}
	}

	/**
	 * 
	 * @return boolean
	 */
	public boolean validEndDate() {

		Connection connection = null;
		try {
			// get a connection to the database
			connection = DataSourceConnectionFactory.getDBConnection();

			// create a prepared statement and substitute the parameters
			PreparedStatement statement = connection.prepareStatement(JDBC_PUBDATE);
			statement.setString(1, this.getPubCode());
			statement.setString(2, this.getEnd_date());

			statement.execute();

			// get the result
			ResultSet resultSet = statement.getResultSet();
			boolean resultFound = false;
			while (resultSet.next()) {
				resultFound = true;
				break;
			}

			// close the database connection
			statement.close();
			connection.close();

			// inspect the result
			if (!resultFound) {
				// PubCode/PubDate not valid
				return false;
			} else {
				return true;
			}
		} catch (SQLException e) {
			return false;
		} catch (UsatDBConnectionException e) {
			return false;
		} finally {
			DataSourceConnectionFactory.closeConnection(connection);
		}
	}

	/**
	 * 
	 * @param pub
	 * @param begin_date
	 * @return String
	 */
	public String addStartDate(String pub, String begin_date) {
		HashMap<String, String> datemap = PublicationDates.getDateFields(begin_date);

		String year = datemap.get("YEAR");
		String month = datemap.get("MONTH");
		String day = datemap.get("DAY");

		// set calendar
		GregorianCalendar startingDate = new GregorianCalendar();

		startingDate.set(Integer.parseInt(year), (Integer.parseInt(month) - 1), Integer.parseInt(day));

		startingDate.add(Calendar.DATE, 1);

		int startPubDay = startingDate.get(Calendar.DATE);
		int startPubMth = startingDate.get(Calendar.MONTH);
		int startPubYear = startingDate.get(Calendar.YEAR);

		String startPubDateNew = ((startPubMth + 1) + "/" + startPubDay + "/" + startPubYear);

		return startPubDateNew;

	}

	/**
	 * method to prevent the infinite loop problem
	 * 
	 * @param dateStr
	 * @return
	 */
	protected boolean pastMaxDate(String dateStr) {
		boolean pastMax = true;
		Connection connection = null;
		java.util.Date dateToBeChecked = UTCommon.formatStringAsCalendar(dateStr).getTime();
		try {
			// get a connection to the database
			connection = DataSourceConnectionFactory.getDBConnection();

			// create a prepared statement and substitute the parameters
			PreparedStatement statement = connection.prepareStatement(JDBC_PUB_MAX_DATE);
			statement.setString(1, this.getPubCode());

			statement.execute();

			// get the result
			ResultSet resultSet = statement.getResultSet();
			if (resultSet.next()) {
				Date maxDate = resultSet.getDate(1);
				if (maxDate == null || maxDate.before(dateToBeChecked)) {
					pastMax = true;
				} else {
					pastMax = false;
				}
			} else {
				pastMax = true;
			}

			// close the database connection
			statement.close();
			connection.close();

		} catch (SQLException e) {
			return true;
		} catch (UsatDBConnectionException e) {
			return true;
		} finally {
			DataSourceConnectionFactory.closeConnection(connection);
		}
		return pastMax;
	}

	/**
	 * 
	 * Creation date: (8/22/00 12:18:32 AM)
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getMessageAsString() {
		return (this.getMessage() == null) ? PublicationDates.DEFAULT_MESSAGE : this.getMessage();
	}

	/**
	 * 
	 * @param unknown_date
	 * @return
	 */
	public static HashMap<String, String> getDateFields(String unknown_date) {

		String trimdate = unknown_date;

		String chkDate = trimdate.trim();

		int len = chkDate.length();

		if (len > 10) {
			String sub = chkDate.substring(0, 10);
			chkDate = sub;
		}

		String date = chkDate.replace('-', '/');

		HashMap<String, String> row = new HashMap<String, String>();

		int pos = date.indexOf('/');

		int year = 0;
		int month = 0;
		int day = 0;

		if (pos == 4) // yyyy/mm/dd format
		{
			year = Integer.parseInt(date.substring(0, pos)); // year

			int pos_prev = pos;
			pos = date.indexOf('/', pos + 1);

			month = Integer.parseInt(date.substring((pos_prev + 1), pos)); // month

			day = Integer.parseInt(date.substring(pos + 1)); // day

		} else { // mm/dd/yyyy format
			month = Integer.parseInt(date.substring(0, pos)); // month

			int pos_prev = pos;
			pos = date.indexOf('/', pos + 1);

			day = Integer.parseInt(date.substring((pos_prev + 1), pos)); // day

			year = Integer.parseInt(date.substring(pos + 1)); // year

		}

		row.put("MONTH", String.valueOf(month));
		row.put("DAY", String.valueOf(day));
		row.put("YEAR", String.valueOf(year));

		return row;
	}

	/**
	 * Login the current user; return true if successful, false otherwise Creation date: (8/21/00 10:05:38 PM)
	 * 
	 * @return boolean
	 */
	public static boolean verifyInputDate(String unknown_date) {

		String trimdate = unknown_date;
		String chkDate = trimdate.trim();

		int len = chkDate.length();
		if (len < 8) {
			return false;
		}

		String date = chkDate.replace('-', '/');
		int pos = date.indexOf('/');

		if (pos < 0) {
			return false;
		}

		if (pos == 4) // yyyy/mm/dd format
		{
			int year = Integer.parseInt(date.substring(0, pos)); // year
			if (year < 2000) {
				return false;
			}

			int pos_prev = pos;
			pos = date.indexOf('/', pos + 1);

			int month = Integer.parseInt(date.substring((pos_prev + 1), pos)); // month
			if (month > 12) {
				return false;
			}

			int day = Integer.parseInt(date.substring(pos + 1)); // day
			if (day > 31) {
				return false;
			}

		} else { // mm/dd/yyyy format
			int month = Integer.parseInt(date.substring(0, pos)); // month
			if (month > 12) {
				return false;
			}

			int pos_prev = pos;
			pos = date.indexOf('/', pos + 1);

			int day = Integer.parseInt(date.substring((pos_prev + 1), pos)); // day
			if (day > 31) {
				return false;
			}

			int year = Integer.parseInt(date.substring(pos + 1)); // year
			if (year < 2000) {
				return false;
			}

		}

		return true;

	}

	/**
	 * 
	 * @param unknown_date
	 * @return
	 */
	public static String convertCYYMMDD(String unknown_date) {

		String trimdate = unknown_date;

		String date = trimdate.trim();

		new StringBuffer("");

		String century = (date.substring(0, 1));

		String year = null;
		String sub_year = (date.substring(1, 3));

		String month = (date.substring(3, 5));

		String day = (date.substring(5, 7));

		if (Integer.parseInt(century) == 0) {
			year = "19" + sub_year;
		} else {
			year = "20" + sub_year;
		}

		// set calendar
		GregorianCalendar formatDate = new GregorianCalendar();

		formatDate.set(Integer.parseInt(year), (Integer.parseInt(month) - 1), Integer.parseInt(day));

		String displayDate = UTCommon.formatCalendarAsString(formatDate, "MMMM d, yyyy");

		return displayDate;
	}

	/**
	 * 
	 * @param unknown_date
	 * @return
	 */
	public static String convertDateFormat(String unknown_date) {

		String trimdate = unknown_date;

		String chkDate = trimdate.trim();

		int len = chkDate.length();

		if (len > 10) {
			String sub = chkDate.substring(0, 10);
			chkDate = sub;
		}
		//
		String date = chkDate.replace('-', '/');

		int pos = date.indexOf('/');

		StringBuffer formattedDate = new StringBuffer("");

		String month = null;
		String day = null;
		String year = null;

		if (pos == 4) // yyyy/mm/dd format
		{
			formattedDate.append(date.substring(0, pos)); // year

			int pos_prev = pos;
			pos = date.indexOf('/', pos + 1);

			if ((pos - pos_prev) == 3) {
				formattedDate.append(date.substring((pos_prev + 1), pos)); // month
			} else {
				formattedDate.append("0" + date.substring((pos_prev + 1), pos));
			}

			if ((len - pos) == 3) {
				formattedDate.append(date.substring(pos + 1)); // day
			} else {
				formattedDate.append("0" + date.substring(pos + 1));
			}

		} else { // mm/dd/yyyy format

			if (pos == 2) {
				month = (date.substring(0, pos)); // month
			} else {
				month = "0" + (date.substring(0, pos));
			}

			int pos_prev = pos;
			pos = date.indexOf('/', pos + 1);

			if ((pos - pos_prev) == 3) {
				day = (date.substring((pos_prev + 1), pos)); // day
			} else {
				day = ("0" + date.substring((pos_prev + 1), pos));
			}

			year = (date.substring(pos + 1)); // year

			formattedDate.append(year + month + day);

		}

		String systemDate = formattedDate.toString();

		return systemDate;
	}

	/**
	 * 
	 * @param beg_date
	 * @param end_date
	 * @return
	 */
	public static boolean compareVacDates(String beg_date, String end_date) {
		// Set current date
		GregorianCalendar currentDate = new GregorianCalendar();

		// set begin date
		String date = beg_date;
		HashMap<String, String> datemap = PublicationDates.getDateFields(date);

		String year = datemap.get("YEAR");
		String month = datemap.get("MONTH");
		String day = datemap.get("DAY");

		GregorianCalendar startingDate = new GregorianCalendar();

		startingDate.set(Integer.parseInt(year), (Integer.parseInt(month) - 1), Integer.parseInt(day));

		if (currentDate.after(startingDate)) {
			return false;
		}

		// set end date
		String date_new = end_date;
		HashMap<String, String> datemap_new = PublicationDates.getDateFields(date_new);

		String year_new = datemap_new.get("YEAR");
		String month_new = datemap_new.get("MONTH");
		String day_new = datemap_new.get("DAY");

		GregorianCalendar endingDate = new GregorianCalendar();

		endingDate.set(Integer.parseInt(year_new), (Integer.parseInt(month_new) - 1), Integer.parseInt(day_new));

		if (startingDate.equals(endingDate)) {
			return false;
		}

		if (startingDate.after(endingDate)) {
			return false;
		}

		return true;

	}

	/**
	 * 
	 * @param beg_date
	 * @param end_date
	 * @return This method returns false if date1 < date2
	 */
	public static boolean compareDates(String date1, String date2) {

		// Set current date
		GregorianCalendar currentDate = new GregorianCalendar();

		// set date1
		String date = date1;
		HashMap<String, String> datemap = PublicationDates.getDateFields(date);

		String year = datemap.get("YEAR");
		String month = datemap.get("MONTH");
		String day = datemap.get("DAY");

		GregorianCalendar startingDate = new GregorianCalendar();

		startingDate.set(Integer.parseInt(year), (Integer.parseInt(month) - 1), Integer.parseInt(day));

		if (currentDate.after(startingDate)) {
			return false;
		}

		// set date2
		String date_new = date2;
		HashMap<String, String> datemap_new = PublicationDates.getDateFields(date_new);

		String year_new = datemap_new.get("YEAR");
		String month_new = datemap_new.get("MONTH");
		String day_new = datemap_new.get("DAY");

		GregorianCalendar endingDate = new GregorianCalendar();

		endingDate.set(Integer.parseInt(year_new), (Integer.parseInt(month_new) - 1), Integer.parseInt(day_new));

		if (startingDate.after(endingDate)) {
			return false;
		}

		return true;

	}

	public abstract String calcNextStartDate();

	public abstract String calcStartDate() throws UsatException;

	public abstract String calcNextVacStartDate();

	public boolean isFuturePubDate() {
		boolean isFutureDate = false;

		if (this.pubDate == null) {
			return false;
		}

		try {
			// now check that the date is greater than or equal today
			HashMap<String, String> datemap = PublicationDates.getDateFields(this.pubDate);

			String year = datemap.get("YEAR");
			String month = datemap.get("MONTH");
			String day = datemap.get("DAY");
			String earliestPossibleStart = "";

			// set calendar
			GregorianCalendar selectedDate = new GregorianCalendar();

			selectedDate.set(Integer.parseInt(year), (Integer.parseInt(month) - 1), Integer.parseInt(day), 23, 59);

			earliestPossibleStart = determineEarliestStart(this.getPubCode(), null);
			datemap.clear();
			datemap = PublicationDates.getDateFields(earliestPossibleStart);
			year = datemap.get("YEAR");
			month = datemap.get("MONTH");
			day = datemap.get("DAY");

			Calendar earliestStart = Calendar.getInstance();
			earliestStart.set(Integer.parseInt(year), (Integer.parseInt(month) - 1), Integer.parseInt(day), 0, 0);

			if (!selectedDate.before(earliestStart)) {
				isFutureDate = true;
			}
		} catch (Exception e) {
			isFutureDate = false;
		}

		return isFutureDate;
	}

	/**
	 * 
	 * @param pubcode
	 *            - the publication code you are interested in
	 * @param date
	 *            - send null if you want to use today's date
	 * @return
	 */
	public static String determineEarliestStart(String pubcode, String date) {
		String earliestStart = "";

		try {
			if (date == null || !PublicationDates.verifyInputDate(date)) {
				date = UTCommon.getSystemDate("MM/dd/yyyy");
			}

			PublicationDates pd = null;
			if (pubcode == null || UTCommon.UTPUBCODE.equalsIgnoreCase(pubcode)) {
				pd = new UTPubDates();
			} else {
				pd = new BWPubDates();
			}
			pd.setPubCode(pubcode);
			pd.setPubDate(date);
			String temp = pd.calcNextStartDate();
			pd.setNew_pubdate(temp);
			earliestStart = pd.calcStartDate();

		} catch (Exception e) {
			System.out.println("PublicationDates::determineEarliestStart() - Exception: " + e.getMessage());
		}

		return earliestStart;
	}

	public boolean isDatePastConfiguredMaximum() {
		boolean pastMax = false;

		try {
			if (this.pubDate == null) {
				return true;
			}

			DateTime now = new DateTime();
			DateTime maxStartDate = now.plusDays(this.getMaxDaysInFuture());
			maxStartDate = maxStartDate.plusMillis((DateTimeConstants.MILLIS_PER_DAY - now.getMillisOfDay()));

			// date to verify
			HashMap<String, String> datemap = PublicationDates.getDateFields(this.pubDate);

			String year = datemap.get("YEAR");
			String month = datemap.get("MONTH");
			String day = datemap.get("DAY");

			// set calendar
			GregorianCalendar selectedDate = new GregorianCalendar();

			selectedDate.set(Integer.parseInt(year), (Integer.parseInt(month) - 1), Integer.parseInt(day), 23, 59);

			DateTime pubDateDT = new DateTime(selectedDate.getTimeInMillis());

			if (pubDateDT.isAfter(maxStartDate)) {
				pastMax = true;
			}
		} catch (Exception e) {
			System.out.println("PublicationDates::isDatePastConfiguredMaximum() = Failed to check date: " + this.pubDate
					+ " for pub:" + this.pubCode + "  Stacktrace:");
			e.printStackTrace();
			pastMax = true;
		}
		return pastMax;
	}

	public int getMaxDaysInFuture() {
		// be default use the ut setting.
		return UsaTodayConstants.MAX_DAYS_IN_FUTURE_FOR_UT_BRAND_SUBSCRIPTION_START;
	}
}
