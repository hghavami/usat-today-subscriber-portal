package com.usatoday.esub.common;

import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.usatoday.business.interfaces.products.SubscriptionOfferIntf;
import com.usatoday.business.interfaces.shopping.OrderItemIntf;
import com.usatoday.business.interfaces.shopping.RenewalOrderItemIntf;
import com.usatoday.business.interfaces.shopping.ShoppingCartIntf;
import com.usatoday.business.interfaces.shopping.SubscriptionOrderItemIntf;
import com.usatoday.businessObjects.shopping.OrderBO;
import com.usatoday.esub.handlers.ShoppingCartHandler;

/**
 * Insert the type's description here. Creation date: (1/13/2003 3:49:26 PM)
 * 
 * @author:
 */
public class WebBug implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6327046546100794897L;

	private static int MAX_OMNITURE_ATTR_LENGTH = 100;

	private boolean omnitureActive = false;
	private java.lang.String omnitureScriptLocation = null;
	private boolean omnitureUseDynamicPageNames = true;

	private java.lang.String baseURL = null;

	private java.lang.String currentPage = null;
	private java.lang.String referringPage = null;
	private java.lang.String keycode = null;
	private java.lang.String pubcode = null;
	private java.lang.String errorCode = null;
	private java.lang.String payType = null;
	private java.lang.String zipcode = null;
	private String ezPaySelected = null;
	private String orderType = "New";
	private int numCopies = 0;
	private String state = null;
	private String amount = null;
	private String terms = null;
	private java.lang.String pointOfEntryCode = null;

	// Omniture specific
	private String omnitureChannel = "";
	private static String omnitureServer = null;
	private String omniturePageNameOverride = null;
	private String omnitureProductCategory = null;
	private String omnitureProp1 = ""; // temp keycode
	private String omnitureProp2 = ""; // temp subkeycode
	private String omnitureProp3 = "";
	private String omnitureProp4 = "";
	private String omnitureProp5 = "";
	private String omnitureProp41 = ""; // tracking code or keycode
	private String omnitureProp42 = ""; // subkeycode
	private String omnitureEvent = "";
	private String omnitureProduct = "";
	private String omniturePurchaseID = "";
	private String omnitureEVar1 = "";
	private String omnitureEVar2 = "";
	private String omnitureEVar3 = "";
	private String omnitureEVar4 = "";
	private String omnitureEVar5 = "";
	private boolean isErrorPage = false;

	/**
	 * WebBug constructor comment.
	 */
	public WebBug(boolean oActive, String oScriptLoc, boolean oUseDynamicPageNames, String webSiteBaseURL) {
		super();
		this.setOmnitureActive(oActive);
		this.setOmnitureScriptLocation(oScriptLoc);
		this.setBaseURL(webSiteBaseURL);
		this.setOmnitureUseDynamicPageNames(oUseDynamicPageNames);

		try {
			// Initialize the static server variable if not initialized
			if (WebBug.omnitureServer == null) {

				java.net.InetAddress serverAddress = java.net.InetAddress.getLocalHost();

				WebBug.omnitureServer = serverAddress.getHostName();
			}
		} catch (Exception e) {
			// ignore
			WebBug.omnitureServer = "";
		}

	}

	/**
	 * Insert the method's description here. Creation date: (1/13/2003 3:59:44 PM)
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBaseURL() {
		return baseURL;
	}

	/**
	 * Insert the method's description here. Creation date: (1/13/2003 4:06:49 PM)
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCurrentPage() {
		return currentPage;
	}

	/**
	 * Insert the method's description here. Creation date: (1/13/2003 4:10:51 PM)
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getErrorCode() {
		return errorCode;
	}

	public String getOmnitureHTML() {

		if (!this.isOmnitureActive()) {
			return ""; // return an empty string if omniture tracking is not active
		} else {
			return this.getOmnitureHTML_versionH10();
		}
	}

	private String getOmnitureHTML_versionH10() {

		StringBuffer oStr = new StringBuffer("<!-- SiteCatalyst code version: H.10. \n"
				+ "Copyright 1997-2007 Omniture, Inc. More info available at\n" + "   http://www.omniture.com --> \n"
				+ "<script language=\"JavaScript\" src=\"" + this.getOmnitureScriptLocation() + "\"></script> \n"
				+ " <script language=\"JavaScript\"><!-- \n"
				+ "/* You may give each page an identifying name, server, and channel on " + " the next lines. */ \n");

		// PAGE NAME MUST be Called First since it may determine the values of other attributes
		// specifically the campaign events and such
		String pageName = this.getOmniturePageName();
		oStr.append("s.pageName=\"").append(pageName).append("\"\n");
		oStr.append("s.server=\"").append(this.getOmnitureServer()).append("\"\n");
		oStr.append("s.channel=\"").append(this.getOmnitureChannel()).append("\"\n");
		oStr.append("s.pageType=\"").append(this.getOmniturePageType()).append("\"\n");
		oStr.append("s.prop1=\"").append(this.getOmnitureProp1()).append("\"\n");
		oStr.append("s.prop2=\"").append(this.getOmnitureProp2()).append("\"\n");
		oStr.append("s.prop3=\"").append(this.getOmnitureProp3()).append("\"\n");
		oStr.append("s.prop4=\"").append(this.getOmnitureProp4()).append("\"\n");
		oStr.append("s.prop5=\"").append(this.getOmnitureProp5()).append("\"\n");
		oStr.append("s.prop41=\"").append(this.getOmnitureProp41() + pageName).append("\"\n");
		oStr.append("s.prop42=\"").append(this.getOmnitureProp42() + pageName).append("\"\n");
		oStr.append("/* E-commerce Variables */\n");

		// set up campaign tracking variable
		oStr.append("s.campaign=\"").append(this.getKeycode()).append("\"\n");
		oStr.append("s.state=\"").append(this.getState()).append("\"\n");
		// set up zip code tracking
		oStr.append("s.zip=\"").append(this.getZipcode()).append("\"\n");
		// set event string
		oStr.append("s.events=\"").append(this.getOmnitureEvent()).append("\"\n");
		// set product tracking
		oStr.append("s.products=\"").append(this.getOmnitureProduct()).append("\"\n");
		// order id
		oStr.append("s.purchaseID=\"").append(this.getOmniturePurchaseID()).append("\"\n");

		oStr.append("s.eVar1=\"").append(this.getOmnitureEVar1()).append("\"\n");
		oStr.append("s.eVar2=\"").append(this.getOmnitureEVar2()).append("\"\n");
		oStr.append("s.eVar3=\"").append(this.getOmnitureEVar3()).append("\"\n");
		oStr.append("s.eVar4=\"").append(this.getOmnitureEVar4()).append("\"\n");
		oStr.append("s.eVar5=\"").append(this.getOmnitureEVar5()).append("\"\n");

		// new
		oStr.append("/************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/ \n");
		oStr.append("var s_code=s.t();if(s_code)document.write(s_code)//--></script>");
		oStr.append("<script language=\"JavaScript\"><!-- \n");
		oStr.append("if(navigator.appVersion.indexOf('MSIE')>=0)document.write(unescape('%3C')+'\\!-'+'-')");
		oStr.append("//--></script><noscript><a href=\"http://www.omniture.com\" title=\"Web Analytics\"><img");

		oStr.append(" src=\"https://102.112.2O7.net/b/ss/ganusatodaycirc");
		if (this.getOmnitureScriptLocation().indexOf("dev") > 0) {
			oStr.append("dev");
		}
		oStr.append("/1/H.10--NS/0\"");
		oStr.append(" height=\"1\" width=\"1\" border=\"0\" alt=\"\" /></a></noscript><!--/DO NOT REMOVE/--> \n");
		oStr.append("<!-- End SiteCatalyst code version: H.8. -->");

		return oStr.toString();
	}

	@SuppressWarnings("unused")
	private String getOmnitureHTML_versionG7() {

		StringBuffer oStr = new StringBuffer("<!-- SiteCatalyst code version: G.7.    \n"
				+ "Copyright 1997-2004 Omniture, Inc. More info available at\n"
				+ "    http://www.omniture.com --><script language=\"JavaScript\"><!--\n"
				+ " /* You may give each page an identifying name, server, and channel on\n" + "    the next lines. */\n");

		// PAGE NAME MUST be Called First since it may determine the values of other attributes
		// specifically the campaign events and such
		oStr.append("var s_pageName=\"").append(this.getOmniturePageName()).append("\"\n");
		oStr.append("var s_server=\"").append(this.getOmnitureServer()).append("\"\n");
		oStr.append("var s_channel=\"").append(this.getOmnitureChannel()).append("\"\n");
		oStr.append("var s_pageType=\"").append(this.getOmniturePageType()).append("\"\n");
		oStr.append("var s_prop1=\"").append(this.getOmnitureProp1()).append("\"\n");
		oStr.append("var s_prop2=\"").append(this.getOmnitureProp2()).append("\"\n");
		oStr.append("var s_prop3=\"").append(this.getOmnitureProp3()).append("\"\n");
		oStr.append("var s_prop4=\"").append(this.getOmnitureProp4()).append("\"\n");
		oStr.append("var s_prop5=\"").append(this.getOmnitureProp5()).append("\"\n");
		oStr.append("var s_prop41=\"").append(this.getOmnitureProp41()).append("\"\n");
		oStr.append("var s_prop42=\"").append(this.getOmnitureProp42()).append("\"\n");
		oStr.append("/* E-commerce Variables */\n");

		// set up campaign tracking variable
		oStr.append("var s_campaign=\"").append(this.getKeycode()).append("\"\n");
		oStr.append("var s_state=\"").append(this.getState()).append("\"\n");
		// set up zip code tracking
		oStr.append("var s_zip=\"").append(this.getZipcode()).append("\"\n");
		// set event string
		oStr.append("var s_events=\"").append(this.getOmnitureEvent()).append("\"\n");
		// set product tracking
		oStr.append("var s_products=\"").append(this.getOmnitureProduct()).append("\"\n");
		// order id
		oStr.append("var s_purchaseID=\"").append(this.getOmniturePurchaseID()).append("\"\n");

		oStr.append("var s_eVar1=\"").append(this.getOmnitureEVar1()).append("\"\n");
		oStr.append("var s_eVar2=\"").append(this.getOmnitureEVar2()).append("\"\n");
		oStr.append("var s_eVar3=\"").append(this.getOmnitureEVar3()).append("\"\n");
		oStr.append("var s_eVar4=\"").append(this.getOmnitureEVar4()).append("\"\n");
		oStr.append("var s_eVar5=\"").append(this.getOmnitureEVar5()).append("\"\n");
		oStr.append("//--></script><script language=\"JavaScript\" src=\"");
		// replace following with ini file setting
		oStr.append(this.getOmnitureScriptLocation());
		oStr.append("\"></script>\n" + "<!-- End SiteCatalyst code version: G.7. -->\n");

		return oStr.toString();
	}

	/**
	 * No longer using Visual Sciences tracking. Creation date: (1/13/2003 4:12:32 PM)
	 * 
	 * @return java.lang.String
	 */
	public String getVisualSciencesHTML() {

		return "";
	}

	/**
	 * Insert the method's description here. Creation date: (1/13/2003 4:07:33 PM)
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getKeycode() {
		String keycodeValue = "";
		if (keycode != null && keycode.length() > 0) {
			keycodeValue = keycode;
		}
		return keycodeValue;
	}

	/**
	 * Insert the method's description here. Creation date: (1/17/2003 10:52:01 AM)
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getPayType() {
		return payType;
	}

	/**
	 * Insert the method's description here. Creation date: (1/13/2003 4:07:55 PM)
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getPubcode() {
		return pubcode;
	}

	/**
	 * Insert the method's description here. Creation date: (1/13/2003 4:07:12 PM)
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getReferringPage() {
		return referringPage;
	}

	/**
	 * Insert the method's description here. Creation date: (1/29/2003 9:36:18 AM)
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getZipcode() {
		String zipStr = "";
		if (this.zipcode != null && this.zipcode.length() > 0) {
			zipStr = this.zipcode;
		}
		return zipStr;
	}

	/**
	 * Insert the method's description here. Creation date: (1/13/2003 3:59:44 PM)
	 * 
	 * @param newBaseURL
	 *            java.lang.String
	 */
	private void setBaseURL(java.lang.String newBaseURL) {
		baseURL = newBaseURL;
	}

	/**
	 * Insert the method's description here. Creation date: (1/13/2003 4:06:49 PM)
	 * 
	 * @param newCurrentPage
	 *            java.lang.String
	 */
	public void setCurrentPage(java.lang.String newCurrentPage) {
		currentPage = newCurrentPage;
	}

	/**
	 * Insert the method's description here. Creation date: (1/13/2003 4:10:51 PM)
	 * 
	 * @param newErrorCode
	 *            java.lang.String
	 */
	public void setErrorCode(java.lang.String newErrorCode) {
		errorCode = newErrorCode;
	}

	/**
	 * Insert the method's description here. Creation date: (1/13/2003 4:10:51 PM)
	 * 
	 * @param newErrorCode
	 *            java.lang.String
	 */
	public void setErrorCode(HttpSession session) {
		if (session == null) {
			this.setErrorCode("Session_does_not_exist");
		} else {
			UTError error = null;
			// retrieve the current session's error object
			Object eObj = session.getAttribute(UTCommon.SESSION_FIELD_ERROR);
			if (eObj != null) {
				try {
					if (eObj instanceof UTError) {
						error = (UTError) eObj;
						this.setErrorCode(String.valueOf(error.getNumber()));
					} else {
						error = new UTError();
						error.setNumber(451);
						this.setErrorCode(String.valueOf(error.getNumber()));
					}
				} catch (Exception e) {
					this.setErrorCode("Error retrieving error number.");
				}
			} else {
				// delete this
				error = new UTError();
				error.setNumber(451);
				this.setErrorCode(String.valueOf(error.getNumber()));
			}
		}
		// errorCode = newErrorCode;
	}

	/**
	 * Insert the method's description here. Creation date: (1/13/2003 4:07:33 PM)
	 * 
	 * @param newKeycode
	 *            java.lang.String
	 */
	public void setKeycode(java.lang.String newKeycode) {
		keycode = newKeycode;
	}

	/**
	 * Insert the method's description here. Creation date: (1/13/2003 4:07:33 PM)
	 * 
	 * @param newKeycode
	 *            java.lang.String
	 */
	public void setKeycode(HttpServletRequest req) {
		if (req == null) {
			return;
		}

		String attribute = (String) req.getParameter("keycode");
		if (attribute != null && attribute.trim().length() > 0 && attribute.trim().length() < 6) {
			this.setKeycode(attribute.trim());
		} else {
			this.setKeycode(req.getSession());
		}
	}

	/**
	 * Insert the method's description here. Creation date: (1/13/2003 4:07:33 PM)
	 * 
	 * @param newKeycode
	 *            java.lang.String
	 */
	public void setKeycode(HttpSession session) {
		if (session == null) {
			return;
		}

		String attribute = (String) session.getAttribute(UTCommon.SESSION_KEYCODE);
		if (attribute != null && attribute.trim().length() > 0) {
			this.setKeycode(attribute);
		}
	}

	/**
	 * Insert the method's description here. Creation date: (1/17/2003 10:52:01 AM)
	 * 
	 * @param newPayType
	 *            java.lang.String
	 */
	public void setPayType(java.lang.String newPayType) {
		payType = newPayType;
	}

	/**
	 * Insert the method's description here. Creation date: (1/17/2003 10:52:01 AM)
	 * 
	 * @param newPayType
	 *            java.lang.String
	 */
	public void setPayType(HttpSession session) {
		if (session == null) {
			return;
		}

		String attribute = (String) session.getAttribute(UTCommon.SESSION_PAYTYPE);
		if (attribute != null && attribute.trim().length() > 0) {
			this.setPayType(attribute);
		}
	}

	/**
	 * Insert the method's description here. Creation date: (1/13/2003 4:07:55 PM)
	 * 
	 * @param newPubcode
	 *            java.lang.String
	 */
	public void setPubcode(java.lang.String newPubcode) {
		pubcode = newPubcode;
	}

	/**
	 * Insert the method's description here. Creation date: (1/13/2003 4:07:55 PM)
	 * 
	 * @param newPubcode
	 *            java.lang.String
	 */
	public void setPubcode(HttpServletRequest req) {
		if (req == null) {
			return;
		}

		String attribute = (String) req.getParameter("pub");
		if (attribute != null && attribute.trim().length() > 0) {
			this.setPubcode(attribute);
		} else {
			this.setPubcode(req.getSession());
		}

	}

	/**
	 * Insert the method's description here. Creation date: (1/13/2003 4:07:55 PM)
	 * 
	 * @param newPubcode
	 *            java.lang.String
	 */
	public void setPubcode(HttpSession session) {
		if (session == null) {
			return;
		}

		String attribute = (String) session.getAttribute(UTCommon.SESSION_PUB_NAME);
		if (attribute != null && attribute.trim().length() > 0) {
			this.setPubcode(attribute);
		}
	}

	/**
	 * Insert the method's description here. Creation date: (1/13/2003 4:07:12 PM)
	 * 
	 * @param newReferringPage
	 *            java.lang.String
	 */
	public void setReferringPage(java.lang.String newReferringPage) {
		referringPage = newReferringPage;
	}

	/**
	 * Insert the method's description here. Creation date: (1/13/2003 4:07:12 PM)
	 * 
	 * @param newReferringPage
	 *            java.lang.String
	 */
	public void setReferringPage(HttpServletRequest req) {
		if (req == null) {
			return;
		}

		String referrer = req.getHeader("Referer");
		if (referrer != null) {
			this.setReferringPage(referrer);
		}
	}

	/**
	 * Insert the method's description here. Creation date: (1/29/2003 9:36:18 AM)
	 * 
	 * @param newZipcode
	 *            java.lang.String
	 */
	public void setZipcode(java.lang.String newZipcode) {
		zipcode = newZipcode;
	}

	/**
	 * Insert the method's description here. Creation date: (1/29/2003 9:36:18 AM)
	 * 
	 * @param newZipcode
	 *            java.lang.String
	 */
	public void setZipcode(HttpSession session) {
		if (session == null) {
			return;
		}

		// check from the order type [ Single page Order Entry]
		OrderBO order = null;
		try {
			order = (OrderBO) session.getAttribute("lastOrder");
			if (order != null) {
				if (order.isBillingSameAsDelivery()) {
					this.setZipcode(order.getDeliveryContact().getUIAddress().getZip());
				} else {
					this.setZipcode(order.getBillingContact().getUIAddress().getZip());
				}
			} // end if order != null
		} catch (Exception exp) {
			; // ignore
		}
	}

	/**
	 * @return
	 */
	public java.lang.String getPointOfEntryCode() {
		return pointOfEntryCode;
	}

	/**
	 * @param string
	 */
	public void setPointOfEntryCode(java.lang.String string) {
		pointOfEntryCode = string;
	}

	/**
	 * @param string
	 */
	public void setPointOfEntryCode(HttpServletRequest request) {
		try {
			String str = request.getParameter("POE");
			if (str != null && str.length() > 0) {
				this.setPointOfEntryCode(str);
			} else {
				str = (String) request.getSession().getAttribute("POE");
				if (str != null) {
					this.setPointOfEntryCode(str);
					// remove the attribute from the session
					request.getSession().removeAttribute("POE");
				}
			}
		} catch (Exception e) {
			// do nothing if an exception occurs
			;
		}
	}

	/**
	 * @return
	 */
	public String getAmount() {
		String tempStr = "";
		if (this.amount != null && this.amount.length() > 0) {
			tempStr = this.amount;
		}

		if (tempStr.length() > WebBug.MAX_OMNITURE_ATTR_LENGTH) {
			tempStr = tempStr.substring(0, WebBug.MAX_OMNITURE_ATTR_LENGTH);
		}

		return tempStr;
	}

	/**
	 * @return
	 */
	public boolean isOmnitureActive() {
		return omnitureActive;
	}

	/**
	 * @return
	 */
	public String getOmnitureChannel() {
		String tempStr = "";
		if (this.omnitureChannel != null && this.omnitureChannel.length() > 0) {
			tempStr = this.omnitureChannel;
		}

		if (tempStr.length() > WebBug.MAX_OMNITURE_ATTR_LENGTH) {
			tempStr = tempStr.substring(0, WebBug.MAX_OMNITURE_ATTR_LENGTH);
		}

		return tempStr;
	}

	/**
	 * @return
	 */
	public String getOmnitureEVar1() {
		String tempStr = "";
		if (this.omnitureEVar1 != null && this.omnitureEVar1.length() > 0) {
			tempStr = this.omnitureEVar1;
		}

		if (tempStr.length() > WebBug.MAX_OMNITURE_ATTR_LENGTH) {
			tempStr = tempStr.substring(0, WebBug.MAX_OMNITURE_ATTR_LENGTH);
		}

		return tempStr;
	}

	/**
	 * @return
	 */
	public String getOmnitureEVar2() {
		String tempStr = "";
		if (this.omnitureEVar2 != null && this.omnitureEVar2.length() > 0) {
			tempStr = this.omnitureEVar2;
		}

		if (tempStr.length() > WebBug.MAX_OMNITURE_ATTR_LENGTH) {
			tempStr = tempStr.substring(0, WebBug.MAX_OMNITURE_ATTR_LENGTH);
		}

		return tempStr;
	}

	/**
	 * @return
	 */
	public String getOmnitureEvent() {
		String tempStr = "";
		if (this.omnitureEvent != null && this.omnitureEvent.length() > 0) {
			tempStr = this.omnitureEvent;
		}

		if (tempStr.length() > WebBug.MAX_OMNITURE_ATTR_LENGTH) {
			tempStr = tempStr.substring(0, WebBug.MAX_OMNITURE_ATTR_LENGTH);
		}

		return tempStr;
	}

	/**
	 * @return
	 */
	public String getOmniturePageNameOverride() {
		return omniturePageNameOverride;
	}

	/**
	 * @return
	 */
	public String getOmniturePageType() {
		String tempStr = "";
		if (this.isErrorPage()) {
			tempStr = "errorPage"; // Omniture specified value
		}

		return tempStr;
	}

	/**
	 * @return
	 */
	public String getOmnitureProduct() {
		String tempStr = "";
		if (this.omnitureProduct != null && this.omnitureProduct.length() > 0) {
			tempStr = this.omnitureProduct;
		}

		if (tempStr.length() > WebBug.MAX_OMNITURE_ATTR_LENGTH) {
			tempStr = tempStr.substring(0, WebBug.MAX_OMNITURE_ATTR_LENGTH);
		}

		return tempStr;
	}

	/**
	 * @return
	 */
	public String getOmnitureProp1() {
		String tempStr = "";
		if (this.omnitureProp1 != null && this.omnitureProp1.length() > 0) {
			tempStr = this.omnitureProp1;
		}

		if (tempStr.length() > WebBug.MAX_OMNITURE_ATTR_LENGTH) {
			tempStr = tempStr.substring(0, WebBug.MAX_OMNITURE_ATTR_LENGTH);
		}

		return tempStr;
	}

	/**
	 * @return
	 */
	public String getOmnitureProp2() {
		String tempStr = "";
		if (this.omnitureProp2 != null && this.omnitureProp2.length() > 0) {
			tempStr = this.omnitureProp2;
		}

		if (tempStr.length() > WebBug.MAX_OMNITURE_ATTR_LENGTH) {
			tempStr = tempStr.substring(0, WebBug.MAX_OMNITURE_ATTR_LENGTH);
		}

		return tempStr;
	}

	/**
	 * @return
	 */
	public String getOmnitureProp3() {
		String tempStr = "";
		if (this.omnitureProp3 != null && this.omnitureProp3.length() > 0) {
			tempStr = this.omnitureProp3;
		}

		if (tempStr.length() > WebBug.MAX_OMNITURE_ATTR_LENGTH) {
			tempStr = tempStr.substring(0, WebBug.MAX_OMNITURE_ATTR_LENGTH);
		}

		return tempStr;
	}

	/**
	 * @return
	 */
	public String getOmniturePurchaseID() {
		String tempStr = "";
		if (this.omniturePurchaseID != null && this.omniturePurchaseID.length() > 0) {
			tempStr = this.omniturePurchaseID;
		}

		if (tempStr.length() > 20) {
			tempStr = tempStr.substring(0, 20);
		}

		return tempStr;
	}

	/**
	 * @return
	 */
	public java.lang.String getOmnitureScriptLocation() {
		return omnitureScriptLocation;
	}

	/**
	 * @return
	 */
	public String getOmnitureServer() {
		String tempStr = "";
		if (WebBug.omnitureServer != null && WebBug.omnitureServer.length() > 0) {
			tempStr = WebBug.omnitureServer;
		}

		if (tempStr.length() > WebBug.MAX_OMNITURE_ATTR_LENGTH) {
			tempStr = tempStr.substring(0, WebBug.MAX_OMNITURE_ATTR_LENGTH);
		}

		return tempStr;
	}

	/**
	 * @return
	 */
	public String getState() {
		String tempStr = "";
		if (this.state != null && this.state.length() > 0) {
			tempStr = this.state;
		}

		if (tempStr.length() > WebBug.MAX_OMNITURE_ATTR_LENGTH) {
			tempStr = tempStr.substring(0, WebBug.MAX_OMNITURE_ATTR_LENGTH);
		}

		return tempStr;
	}

	/**
	 * @param string
	 */
	public void setAmount(String string) {
		amount = string;
	}

	/**
	 * @param b
	 */
	private void setOmnitureActive(boolean b) {
		omnitureActive = b;
	}

	/**
	 * @param string
	 */
	public void setOmnitureChannel(String string) {
		omnitureChannel = string;
	}

	/**
	 * @param string
	 */
	public void setOmnitureEVar1(String string) {
		omnitureEVar1 = string;
	}

	/**
	 * @param string
	 */
	public void setOmnitureEVar2(String string) {
		omnitureEVar2 = string;
	}

	/**
	 * @param string
	 */
	public void setOmnitureEvent(String string) {
		omnitureEvent = string;
	}

	/**
	 * @param string
	 */
	public void setOmniturePageNameOverride(String string) {
		omniturePageNameOverride = string;
	}

	/**
	 * @param string
	 */
	public void setOmnitureProduct(String string) {
		omnitureProduct = string;
	}

	/**
	 * @param string
	 */
	public void setOmnitureProp1(String string) {
		omnitureProp1 = string;
	}

	/**
	 * @param string
	 */
	public void setOmnitureProp2(String string) {
		omnitureProp2 = string;
	}

	/**
	 * @param string
	 */
	public void setOmnitureProp3(String string) {
		omnitureProp3 = string;
	}

	/**
	 * @param string
	 */
	public void setOmniturePurchaseID(String string) {
		omniturePurchaseID = string;
	}

	/**
	 * @param string
	 */
	private void setOmnitureScriptLocation(java.lang.String string) {
		omnitureScriptLocation = string;
	}

	/**
	 * @param string
	 */
	public void setOmnitureServer(String string) {
		omnitureServer = string;
	}

	/**
	 * @param string
	 */
	public void setState(String string) {
		state = string;
	}

	protected String getOmniturePageName() {
		String pName = "";

		if (this.isErrorPage()) {
			return pName; // if error page then never set the page name field
		}

		// call this first to set all dynamic elements
		if (this.isOmnitureUseDynamicPageNames() && this.getCurrentPage() != null && this.getCurrentPage().length() > 0) {
			// Attempt to build a more useful page name:
			pName = this.generateOmnitureDynamicPageName(this.getCurrentPage());
		}

		// check to see if the page name is overrideen
		if (this.getOmniturePageNameOverride() != null && this.getOmniturePageNameOverride().trim().length() > 0) {
			pName = this.getOmniturePageNameOverride().trim();
		}

		// max character limit on name
		if (pName.length() > WebBug.MAX_OMNITURE_ATTR_LENGTH) {
			System.out.println("Truncating Omniture Page Name. Exceeds Max Length: " + pName);
			pName = pName.substring(0, WebBug.MAX_OMNITURE_ATTR_LENGTH);
		}

		return pName;
	}

	/**
	 * @return
	 */
	public String getOmnitureProp4() {
		String tempStr = "";
		if (this.omnitureProp4 != null && this.omnitureProp4.length() > 0) {
			tempStr = this.omnitureProp4;
		}

		if (tempStr.length() > WebBug.MAX_OMNITURE_ATTR_LENGTH) {
			tempStr = tempStr.substring(0, WebBug.MAX_OMNITURE_ATTR_LENGTH);
		}

		return tempStr;
	}

	/**
	 * @return
	 */
	public String getOmnitureProp5() {
		String tempStr = "";
		if (this.omnitureProp5 != null && this.omnitureProp5.length() > 0) {
			tempStr = this.omnitureProp5;
		}

		if (tempStr.length() > WebBug.MAX_OMNITURE_ATTR_LENGTH) {
			tempStr = tempStr.substring(0, WebBug.MAX_OMNITURE_ATTR_LENGTH);
		}

		return tempStr;
	}

	/**
	 * 
	 * @return
	 */
	public String getOmnitureProp41() {
		String tempStr = "";
		if (this.omnitureProp41 != null && this.omnitureProp41.length() > 0) {
			tempStr = this.omnitureProp41;
		}

		if (tempStr.length() > WebBug.MAX_OMNITURE_ATTR_LENGTH) {
			tempStr = tempStr.substring(0, WebBug.MAX_OMNITURE_ATTR_LENGTH);
		}

		return tempStr;
	}

	/**
	 * 
	 * @return
	 */
	public String getOmnitureProp42() {
		String tempStr = "";
		if (this.omnitureProp42 != null && this.omnitureProp42.length() > 0) {
			tempStr = this.omnitureProp42;
		}

		if (tempStr.length() > WebBug.MAX_OMNITURE_ATTR_LENGTH) {
			tempStr = tempStr.substring(0, WebBug.MAX_OMNITURE_ATTR_LENGTH);
		}

		return tempStr;
	}

	/**
	 * @param string
	 */
	public void setOmnitureProp4(String string) {
		omnitureProp4 = string;
	}

	/**
	 * @param string
	 */
	public void setOmnitureProp41(String string) {
		omnitureProp41 = string;
	}

	/**
	 * @param string
	 */
	public void setOmnitureProp42(String string) {
		omnitureProp42 = string;
	}

	/**
	 * @param string
	 */
	public void setOmnitureProp5(String string) {
		omnitureProp5 = string;
	}

	/**
	 * @return
	 */
	public String getOmnitureEVar3() {
		String tempStr = "";
		if (this.omnitureEVar3 != null && this.omnitureEVar3.length() > 0) {
			tempStr = this.omnitureEVar3;
		}

		if (tempStr.length() > WebBug.MAX_OMNITURE_ATTR_LENGTH) {
			tempStr = tempStr.substring(0, WebBug.MAX_OMNITURE_ATTR_LENGTH);
		}

		return tempStr;
	}

	/**
	 * @return
	 */
	public String getOmnitureEVar4() {
		String tempStr = "";
		if (this.omnitureEVar4 != null && this.omnitureEVar4.length() > 0) {
			tempStr = this.omnitureEVar4;
		}

		if (tempStr.length() > WebBug.MAX_OMNITURE_ATTR_LENGTH) {
			tempStr = tempStr.substring(0, WebBug.MAX_OMNITURE_ATTR_LENGTH);
		}

		return tempStr;
	}

	/**
	 * @return
	 */
	public String getOmnitureEVar5() {
		String tempStr = "";
		if (this.omnitureEVar5 != null && this.omnitureEVar5.length() > 0) {
			tempStr = this.omnitureEVar5;
		}

		if (tempStr.length() > WebBug.MAX_OMNITURE_ATTR_LENGTH) {
			tempStr = tempStr.substring(0, WebBug.MAX_OMNITURE_ATTR_LENGTH);
		}

		return tempStr;
	}

	/**
	 * @param string
	 */
	public void setOmnitureEVar3(String string) {
		omnitureEVar3 = string;
	}

	/**
	 * @param string
	 */
	public void setOmnitureEVar4(String string) {
		omnitureEVar4 = string;
	}

	/**
	 * @param string
	 */
	public void setOmnitureEVar5(String string) {
		omnitureEVar5 = string;
	}

	protected String getOmnitureProducts() {
		String prodStr = "";
		if (this.getPubcode() != null && this.getPubcode().length() > 0) {
			prodStr = this.getPubcode();
		}

		if (prodStr.length() > WebBug.MAX_OMNITURE_ATTR_LENGTH) {
			prodStr = prodStr.substring(0, WebBug.MAX_OMNITURE_ATTR_LENGTH);
		}
		return prodStr;
	}

	/**
	 * @return
	 */
	public boolean isErrorPage() {
		return isErrorPage;
	}

	/**
	 * @param b
	 */
	public void setErrorPage(boolean b) {
		isErrorPage = b;
	}

	/**
	 * @return
	 */
	public boolean isOmnitureUseDynamicPageNames() {
		return omnitureUseDynamicPageNames;
	}

	/**
	 * @param b
	 */
	private void setOmnitureUseDynamicPageNames(boolean b) {
		omnitureUseDynamicPageNames = b;
	}

	/**
	 * This method is a hard coded method based on the customer service portal. Changes to the extranet site pages will result in
	 * poory structured names, or the default name being used.
	 * 
	 * I apologize in advance for anyone maintaining this method. It is difficult to follow at times. Essentially, I was trying to
	 * be efficient where possible without coming up with a recursive method for parsing out page names.
	 * 
	 * This is a big nasty method.
	 * 
	 * @param page
	 * @return
	 */
	private String generateOmnitureDynamicPageName(String cPage) {

		if (cPage == null || cPage.trim().length() == 0) {
			return "";
		}

		String part1 = "";
		String part2 = "";
		String part3 = "";
		String part4 = "";
		String part5 = "";
		String pagePart = "";
		boolean pagePartSet = false;

		try {
			if (cPage.indexOf("/account/") >= 0) { // Customer Service Page
				part1 = "Customer Service";
				this.setOmnitureChannel("Customer Service");

				// since all account features use index.jsp as log in page then mark it as such
				if (cPage.indexOf("/select_accnt.jsp") >= 0) { // multiple account selection page
					pagePart = ":Multiple Account Selection Page";
					pagePartSet = true;
				} else if (cPage.indexOf("/eEditionOnlyIndex") >= 0) {
					// override channel
					this.setOmnitureChannel("Electronic Edition");
					part1 = "Electronic Edition";
					pagePart = ":Electronic Edition Home Page";
					pagePartSet = true;
				} else { // need to dig deeper

					boolean isCompletePage = false;
					// pages that apply to all levels
					if ((cPage.indexOf("/complete.jsp") >= 0) || (cPage.indexOf("/thankyou.") >= 0)
							|| (cPage.indexOf("/delissueComplete.") >= 0)) {
						pagePart = ":Trans Completed Page";
						pagePartSet = true;
						isCompletePage = true;
					} else if (cPage.indexOf("/confirm") >= 0) {
						pagePart = ":Trans Confirm Page";
						pagePartSet = true;
					}

					if (cPage.indexOf("/vacationholds/") >= 0) { // Delivery Stops/Resumes, Vacation Holds
						part2 = ":Suspend/Resume Delivery";
						if ((cPage.indexOf("/holds.jsp") >= 0) || (cPage.indexOf("/vacationholds.jsp") >= 0)) { // main vacation
																												// hold page
							pagePart = ":Date Entry Page";
							pagePartSet = true;
						} else { // otherwise just use page name since confirmation and complete pages are handled above
							if (isCompletePage) {
								setOmnitureEvent("event1"); // translates to vaca holds
							}
							if (!pagePartSet) { // make sure page name not already set
								try {
									pagePart = ":" + cPage.substring((cPage.lastIndexOf('/') + 1));
									pagePartSet = true;
								} catch (Exception e) {
									// if problem setting part 6 then go back to default setting of all blanks
									System.out.println("Unexpected problem setting Omniture page name: " + this.getCurrentPage());
									part1 = "";
									part2 = "";
									pagePart = "";
								}
							} // end if page already set
						} // end else not /vacationholds.jsp
					} // end if vacation hold
					else if (cPage.indexOf("/cp/") >= 0) { // complaints
						part2 = ":Complaints";
						if (cPage.indexOf("/delissue.") >= 0) {
							pagePart = ":Enter Complaint";
							pagePartSet = true;
						} else {
							if (isCompletePage) {
								setOmnitureEvent("event4"); // translates to complaints
							}
							if (!pagePartSet) { // make sure page name not already set
								try {
									pagePart = ":" + cPage.substring((cPage.lastIndexOf('/') + 1));
									pagePartSet = true;
								} catch (Exception e) {
									// if problem setting part 6 then go back to default setting of all blanks
									System.out.println("Unexpected problem setting Omniture page name: " + this.getCurrentPage());
									part1 = "";
									part2 = "";
									pagePart = "";
								}
							} // end if page already set
						}
					} // end else complaint
					else if (cPage.indexOf("/changeaddress/") >= 0) { // change delivery information
						part2 = ":Change Delivery Address";
						if (cPage.indexOf("/changeaddress.jsp") >= 0) {
							pagePart = ":Address Data Entry Page";
							pagePartSet = true;
						} else if (cPage.indexOf("/address.jsp") >= 0) {
							pagePart = ":Code-1 Address Selection Page";
							pagePartSet = true;
						} else if (cPage.indexOf("/effective_date.jsp") >= 0) {
							pagePart = ":Effective Date Page";
							pagePartSet = true;
						} else { // use page name
							if (isCompletePage) {
								setOmnitureEvent("event2"); // // Change delivery event
							}

							if (!pagePartSet) { // make sure page name not already set
								try {
									pagePart = ":" + cPage.substring((cPage.lastIndexOf('/') + 1));
									pagePartSet = true;
								} catch (Exception e) {
									// if problem setting part 6 then go back to default setting of all blanks
									System.out.println("Unexpected problem setting Omniture page name: " + this.getCurrentPage());
									part1 = "";
									part2 = "";
									pagePart = "";
								}
							} // end if page already set
						}
					} else if (cPage.indexOf("/changebilling/") >= 0) { // change billing information
						part2 = ":Change Billing Address";
						if (cPage.indexOf("/changebililng.jsp") >= 0) {
							pagePart = ":Address Data Entry Page";
							pagePartSet = true;
						} else if (cPage.indexOf("/billaddress.jsp") >= 0) {
							pagePart = ":Code-1 Address Selection Page";
							pagePartSet = true;
						} else { // use page name
							if (isCompletePage) {
								setOmnitureEvent("event3"); // // Change billing event
							}
							if (!pagePartSet) { // make sure page name not already set
								try {
									pagePart = ":" + cPage.substring((cPage.lastIndexOf('/') + 1));
									pagePartSet = true;
								} catch (Exception e) {
									// if problem setting part 6 then go back to default setting of all blanks
									System.out.println("Unexpected problem setting Omniture page name: " + this.getCurrentPage());
									part1 = "";
									part2 = "";
									pagePart = "";
								}
							} // end if page already set
						}
					} else if (cPage.indexOf("/accounthistory/") >= 0) { // Account Info
						// there is really only one page in here
						pagePart = ":Subscription Account Information Page";
						pagePartSet = true;
						setOmnitureEvent("event5"); // View account information
					} else {
						// finally set up the page name part if not already done
						if (!pagePartSet) {
							try {
								pagePart = ":" + cPage.substring((cPage.lastIndexOf('/') + 1));
								pagePartSet = true;
							} catch (Exception e) {
								// if problem setting part 6 then go back to default setting of all blanks
								System.out.println("Unexpected problem setting Omniture page name: " + this.getCurrentPage());
								part1 = "";
								pagePart = "";
							}
						}
					}
				} // end else need to dig deeper

			} // end if Customer Service pages
				// LOGIN Path
			else if (cPage.indexOf("/login/") >= 0) {
				part1 = "Customer Service";
				this.setOmnitureChannel("Customer Service");
				if (cPage.indexOf("/auth.") >= 0) { // log in page
					pagePart = ":Log In Page";
					pagePartSet = true;
				} else if (cPage.indexOf("/trialLogin") >= 0) { // trial log in page
					this.setOmnitureChannel("Electronic Edition");
					pagePart = ":Trial Log In Page";
					pagePartSet = true;
				}

			} // end if login path
				// new start electronic edition access page
			else if (cPage.indexOf("/eAccount/") >= 0) { //
				part1 = "Electronic Edition";
				this.setOmnitureChannel("Electronic Edition");

				if (cPage.indexOf("/eEditionNewAccountIndex.") >= 0) {
					pagePart = ":EE Launch Page - Before Account Num Assigned";
					pagePartSet = true;
				} else if (cPage.indexOf("/trials/trialReaderLaunchPage.") >= 0) {
					pagePart = ":EE Trial Launch Page";
					pagePartSet = true;
				} else if (cPage.indexOf("/trials/trialOver.") >= 0) {
					pagePart = ":EE Trial Over Page";
					pagePartSet = true;
				}

			}
			// set up access
			else if (cPage.indexOf("/firsttime/") >= 0) { // Account Info
				// there is really only one page in here
				part1 = "Customer Service";
				if (cPage.indexOf("first_time.jsp") >= 0) {
					pagePart = ":Online Account Setup Page";
					pagePartSet = true;
				}
			}
			// trials
			else if (cPage.indexOf("/subscriptions/electronic/samples/") >= 0) {
				part1 = "EE Trial Subscriptions";
				if (cPage.indexOf("/complete.") >= 0) {
					pagePart = ":Sign Up Completed Page";
					pagePartSet = true;
				} else if (cPage.indexOf("/index.") >= 0) {
					pagePart = ":Sign Up Page";
					pagePartSet = true;
				} else if (cPage.indexOf("/trialErrorPage.") >= 0) {
					pagePart = ":Invalid Trial Page";
					pagePartSet = true;
				}
			}
			// New/Gift/Renewal
			else if (cPage.indexOf("/subscriptions/") >= 0) {
				part1 = "Subscriptions";

				if (cPage.indexOf("/subscriptions/index.jsp") >= 0) {
					pagePart = ":Subscribe Page";
					pagePartSet = true;

					this.setOmnitureEvent("prodView");
					this.setOmnitureChannel("Subscription");
				} else if (cPage.indexOf("/subscribeLaunchPage") >= 0) {
					pagePart = ":EE and Print Dual Subscribe Page";
					pagePartSet = true;
					this.setOmnitureEvent("prodView");
					this.setOmnitureChannel("Subscription");
				} else if (cPage.indexOf("/index_promo_default.jsp") >= 0) {
					pagePart = ":Offer Expired Subscribe Page";
					pagePartSet = true;

					this.setOmnitureChannel("Subscription");
				} else { // dig deeper
					boolean isNewSub = ((cPage.indexOf("/order/") >= 0) || (cPage.indexOf("/dc/") >= 0)
							|| (cPage.indexOf("/newsubscription/") >= 0) || (cPage.indexOf("/bpath/") >= 0)) ? true : false;
					boolean isGiftSub = cPage.indexOf("/giftsubscription/") >= 0 ? true : false;
					boolean isMPF = (cPage.indexOf("/mpf/") >= 0) || (cPage.indexOf("/specials") >= 0)
							|| (cPage.indexOf("/renewalspecials") >= 0) ? true : false;
					boolean isRenewal = cPage.indexOf("/renewals/") >= 0 ? true : false;
					boolean isEE = cPage.indexOf("/electronic/") >= 0 ? true : false;

					// pages that apply to all levels or subscription types
					if (cPage.indexOf("/complete.jsp") >= 0 || cPage.indexOf("/renewals/thankyou.") >= 0) {
						pagePart = ":Trans Completed Page";
						pagePartSet = true;

						// set the campaign event
						if (isNewSub) {
							this.setOmnitureEvent("purchase,event6"); // new start
						} else if (isRenewal) {
							this.setOmnitureEvent("purchase,event8"); // renewal
						} else if (isGiftSub) {
							this.setOmnitureEvent("purchase,event7"); // gift start
						} else {
							this.setOmnitureEvent("purchase");
						}
						this.setOmnitureProduct(this.buildOmnitureCompletePageProductString());
						this.setOmnitureEVar2(this.getEzPaySelected());
						this.setOmnitureEVar4(this.getOrderType());
					} else if (cPage.indexOf("/confirm.jsp") >= 0) {
						pagePart = ":Trans Confirm Page";
						pagePartSet = true;
					} else if (cPage.indexOf("/complete_printable.jsp") >= 0) {
						pagePart = ":Printer Friendly Complete Page";
						pagePartSet = true;
					}

					if (isNewSub || isEE) {
						part2 = ":New Subscription";
					}

					if (isGiftSub) {
						part2 = ":Gift Subscription";
					}

					if (isMPF) {
						part2 = ":MPF Page";
					}
					// type of subscription
					if (isNewSub || isGiftSub || isEE) { // all new starts have same page flow
						this.setOmnitureChannel("Subscription");

						// Determine the page in the new subscription process
						if (cPage.indexOf("/checkout.") >= 0) {
							pagePart = ":One Page Check Out Page";
							pagePartSet = true;

							this.setOmnitureEvent("scOpen,scAdd,prodView");
							this.setOmnitureProduct(this.buildOmnitureProductString());
						} else if (cPage.indexOf("/order/thankyou.") >= 0) {
							pagePart = ":One Page Order Thank You Page";
							pagePartSet = true;

							// set the campaign event
							this.setOmnitureEvent("purchase,event6");
							this.setOmnitureProduct(this.buildOmnitureCompletePageProductString());
							this.setOmnitureEVar2(this.getEzPaySelected());
							this.setOmnitureEVar4(this.getOrderType());
						} else if (cPage.indexOf("/dc/thankyou.") >= 0) {
							pagePart = ":USATODAY.com One Page Thank You Page";
							pagePartSet = true;

							// set the campaign event
							this.setOmnitureEvent("purchase,event6");
							this.setOmnitureProduct(this.buildOmnitureCompletePageProductString());
							this.setOmnitureEVar2(this.getEzPaySelected());
							this.setOmnitureEVar4(this.getOrderType());
						} else if (cPage.indexOf("/index.jsp") >= 0) {
							pagePart = ":Terms And Start Date Page";
							pagePartSet = true;

							// set the campaign event
							this.setOmnitureEvent("scOpen,scAdd,prodView");
							this.setOmnitureProduct(this.buildOmnitureProductString());
						} else if (cPage.indexOf("/orderEntry.faces") >= 0 || cPage.indexOf("/orderEntry.jsp") >= 0
								|| cPage.indexOf("/mpfOrderEntry.jsp") >= 0) {
							if (isEE) {
								pagePart = ":EE One Page Subscription Order Page";
							} else {
								pagePart = ":One Page Subscription Order Page";
							}
							pagePartSet = true;

							// set the campaign event
							this.setOmnitureEvent("scOpen,scAdd, scCheckout");
							this.setOmnitureProduct(this.buildOmnitureProductString());
						} else if (cPage.indexOf("/thankyou.faces") >= 0 || cPage.indexOf("/thankyou.jsp") >= 0
								|| cPage.indexOf("/mpfThankYouPage.jsp") >= 0) {
							if (isEE) {
								pagePart = ":EE Single Page Order Thank You Page";
							} else {
								pagePart = ":Single Page Order Thank You Page";
							}
							pagePartSet = true;

							// set the campaign event
							this.setOmnitureEvent("purchase,event6");
							this.setOmnitureProduct(this.buildOmnitureCompletePageProductString());
							this.setOmnitureEVar2(this.getEzPaySelected());
							this.setOmnitureEVar4(this.getOrderType());
						} else if (cPage.indexOf("/index1.jsp") >= 0) {
							pagePart = ":Delivery Address Page";
							pagePartSet = true;

							// set the campaign event
							this.setOmnitureEvent("scCheckout");
							this.setOmnitureProduct(this.buildOmnitureProductString());
						} else if (cPage.indexOf("/index2.jsp") >= 0) {
							pagePart = ":Payment Method Page";
							pagePartSet = true;
						} else if (cPage.indexOf("/index3.jsp") >= 0) {
							pagePart = ":Billing Address Page";
							pagePartSet = true;
						} else if (cPage.indexOf("/address.jsp") >= 0) {
							pagePart = ":Code-1 Address Selection Page";
							pagePartSet = true;
						} else if (cPage.indexOf("/billaddress.jsp") >= 0) {
							pagePart = ":Code-1 Billing Address Selection Page";
							pagePartSet = true;
						} else { // use page name
							if (!pagePartSet) { // make sure page name not already set
								try {
									pagePart = ":" + cPage.substring((cPage.lastIndexOf('/') + 1));
									pagePartSet = true;
								} catch (Exception e) {
									// if problem setting part 6 then go back to default setting of all blanks
									System.out.println("Unexpected problem setting Omniture page name: " + this.getCurrentPage());
									part1 = "";
									part2 = "";
									pagePart = "";
								}
							} // end if page already set
						} // end else use page name
					} else if (cPage.indexOf("/renewals/") >= 0) { // renewals
						part2 = ":Renew Subscription";
						this.setOmnitureChannel("Renewals");

						if (cPage.indexOf("/index.jsp") >= 0) { // log in page
							pagePart = ":Log In Page";
							pagePartSet = true;
						} else if (cPage.indexOf("/renewal.jsp") >= 0 || cPage.indexOf("/renew.") >= 0) { // first renewal page
							pagePart = ":Terms And Payment Page";
							pagePartSet = true;

							// set the campaign event
							this.setOmnitureEvent("scOpen,scAdd,prodView,scCheckout");
							this.setOmnitureProduct(this.buildOmnitureProductString());
						} else if (cPage.indexOf("/billchange.jsp") >= 0) { // multiple account selection page
							pagePart = ":Change Billing Address Page";
							pagePartSet = true;
						} else if (cPage.indexOf("/billaddress.jsp") >= 0) { // multiple account selection page
							pagePart = ":Code-1 Billing Address Selection Page";
							pagePartSet = true;
						} else if (cPage.indexOf("/select_accnt.jsp") >= 0) { // multiple account selection page
							pagePart = ":Multiple Account Selection Page";
							pagePartSet = true;
						} else { // use page name
							if (!pagePartSet) { // make sure page name not already set
								try {
									pagePart = ":" + cPage.substring((cPage.lastIndexOf('/') + 1));
									pagePartSet = true;
								} catch (Exception e) {
									// if problem setting part 6 then go back to default setting of all blanks
									System.out.println("Unexpected problem setting Omniture page name: " + this.getCurrentPage());
									part1 = "";
									part2 = "";
									pagePart = "";
								}
							} // end if page already set
						} // end else use page name
					} // end else renewals
					else {
						this.setOmnitureChannel("Subscription");
						// finally set up the page name part if not already done
						if (!pagePartSet) {
							try {
								pagePart = ":" + cPage.substring((cPage.lastIndexOf('/') + 1));
								pagePartSet = true;
							} catch (Exception e) {
								// if problem setting part 6 then go back to default setting of all blanks
								System.out.println("Unexpected problem setting Omniture page name: " + this.getCurrentPage());
								part1 = "";
								pagePart = "";
							}
						} // end if page part not set
					} // end else not one of new/gift/renewal
				} // end else need to dig deeper
			} // end if New/Gift/Renewal
			else if (cPage.indexOf("/idpassword/") >= 0) { // email password checks/changes
				part1 = "Online Account Maintenance";
				this.setOmnitureChannel("Online Account Maintenance");

				if (cPage.indexOf("/index.jsp") >= 0) { // main account page
					pagePart = ":Home Page";
					pagePartSet = true;
				} else if (cPage.indexOf("/forgotpassword.jsp") >= 0) {
					pagePart = ":Forgot Password Page";
					pagePartSet = true;
				} else if (cPage.indexOf("/forgotpasswordcomplete.jsp") >= 0) {
					pagePart = ":Forgot Password Complete Page";
					pagePartSet = true;
				} else if (cPage.indexOf("/changeIDpassword.jsp") >= 0) {
					pagePart = ":Change Email And Password Page";
					pagePartSet = true;
				} else if (cPage.indexOf("/changeIDpasswordcomplete.jsp") >= 0) {
					pagePart = ":Change Email And Password Complete Page";
					pagePartSet = true;
				} else if (cPage.indexOf("/trialForgotPassword.") >= 0) {
					pagePart = ":Trial Forgot Password Page";
					pagePartSet = true;
				} else {
					// finally set up the page name part if not already done
					if (!pagePartSet) {
						try {
							pagePart = ":" + cPage.substring((cPage.lastIndexOf('/') + 1));
							pagePartSet = true;
						} catch (Exception e) {
							// if problem setting part 6 then go back to default setting of all blanks
							System.out.println("Unexpected problem setting Omniture page name: " + this.getCurrentPage());
							part1 = "";
							pagePart = "";
						}
					} // end if page part not set
				} // end else

			} // end if email/passowrd maintenance
			else if (cPage.indexOf("/international/") >= 0) {
				part1 = "International";

				this.setOmnitureChannel("USAT International");

				if (cPage.indexOf("/welcomeint.jsp") >= 0) {
					pagePart = ":Home Page";
					pagePartSet = true;
				} else {
					// finally set up the page name part if not already done
					if (!pagePartSet) {
						try {
							pagePart = ":" + cPage.substring((cPage.lastIndexOf('/') + 1));
							pagePartSet = true;
						} catch (Exception e) {
							// if problem setting part 6 then go back to default setting of all blanks
							System.out.println("Unexpected problem setting Omniture page name: " + this.getCurrentPage());
							part1 = "";
							pagePart = "";
						}
					} // end if page part not set
				} // end else

			} // end if international
		} catch (Exception e) {
			System.out
					.println("WebBug::generateOmnitureDynamicPageName() Outer CATCH:  Unexpected exception generating dynamic omniture page name: "
							+ cPage + "  Exception: " + e.getMessage());
			part1 = part2 = part3 = part4 = part5 = pagePart = "";
		}

		// if we don't fall into any of the above cases these should all be blanks
		// All blanks will result in the page name (URL minus host portion) as the page name

		// concatentate the parts into the page name
		return part1 + part2 + part3 + part4 + part5 + pagePart;
	}

	/**
	 * Call this method with the current request object to automatically pull out the request and session values.
	 * 
	 * @param req
	 */
	public void deriveValuesFromRequest(HttpServletRequest req) {

		// do nothing if Omniture not on.
		if (!this.omnitureActive) {
			return;
		}

		try {

			SubscriptionOfferIntf offer = UTCommon.getCurrentOfferVersion2(req);

			if (offer != null) {
				this.setPubcode(offer.getPubCode());
				this.setKeycode(offer.getKeyCode());
			}

			this.setCurrentPage(req.getRequestURI());

			String internalCampaign = req.getParameter("intcmp");
			if (internalCampaign != null && internalCampaign.length() > 0) {
				this.setOmnitureEVar1(internalCampaign);
			}
			HttpSession session = req.getSession();

			String campCode = req.getParameter("ccode");
			if (campCode != null && campCode.trim().length() > 0) {
				// prefix the prop41 with the campaign code if available.
				this.setOmnitureProp41(campCode.trim() + " : ");
			}
			try {

				ShoppingCartIntf cart = null;
				com.usatoday.business.interfaces.shopping.OrderIntf lastOrder = null;

				try {
					cart = (ShoppingCartIntf) session.getAttribute("shoppingCart");
					lastOrder = (com.usatoday.business.interfaces.shopping.OrderIntf) session.getAttribute("lastOrder");
				} catch (Exception e) {
					;
				}
				if (cart == null) {
					ShoppingCartHandler sch = null;
					// electronic edition cart
					try {
						sch = (ShoppingCartHandler) session.getAttribute("shoppingCartHandler");
					} catch (Exception e) {
						;
					}

					if (sch != null) {
						cart = sch.getCart();
						lastOrder = (com.usatoday.business.interfaces.shopping.OrderIntf) sch.getLastOrder();
					}
				}
				// only gather these items if a cart exists and order exists
				if (cart != null && lastOrder != null) {

					String temp = String.valueOf(lastOrder.getSubTotal());
					if (temp != null && temp.length() > 0) {
						this.setAmount(temp);
					}

					SubscriptionOrderItemIntf item = null;
					Collection<OrderItemIntf> items = lastOrder.getOrderedItems();
					Iterator<OrderItemIntf> itemItr = items.iterator();
					while (itemItr.hasNext()) {
						OrderItemIntf i = itemItr.next();
						if (i instanceof SubscriptionOrderItemIntf) {
							item = (SubscriptionOrderItemIntf) i;
							break;
						}
					}
					this.setNumCopies(item.getQuantity());

					// override pubCode
					this.pubcode = item.getProduct().getProductCode();

					temp = lastOrder.getOrderID();
					if (temp != null && temp.length() > 0) {
						this.setOmniturePurchaseID(temp);

						temp = lastOrder.getDeliveryContact().getUIAddress().getZip();
						if (temp != null && temp.length() > 0) {
							this.setZipcode(temp);
						}

						temp = lastOrder.getDeliveryContact().getUIAddress().getState();
						if (temp != null && temp.length() > 0) {
							this.setState(temp);
						}
					}

					temp = item.getSelectedTerm().getDuration();
					if (temp != null && temp.length() > 0) {
						this.setTerms(temp);
					}

					if (item.isChoosingEZPay()) {
						this.setEzPaySelected("EZ-PAY");
					} else {
						this.setEzPaySelected("Bill Me");
					}

					if (item instanceof RenewalOrderItemIntf) {
						this.setOrderType("Renewal");
					} else if (item.isGiftItem()) {
						this.setOrderType("Gift");
					}

					// if this is an order request
					if (this.getCurrentPage().indexOf("/renewals/") >= 0) {
						this.setOmnitureProductCategory("Renewal");
					} else if (this.getCurrentPage().indexOf("/subscriptions/") >= 0) {
						this.setOmnitureProductCategory("Subscription");
					}

				} // end if cart
			} catch (Exception e) {
				// skip order specific stuff
			}
		} catch (Exception e) {
			System.out.println("WebBug() - Exception while deriving values from Request: " + e.getMessage());
		}
	}

	/**
	 * @return
	 */
	public int getNumCopies() {
		return numCopies;
	}

	/**
	 * @param i
	 */
	public void setNumCopies(int i) {
		numCopies = i;
	}

	/**
	 * @return
	 */
	public String getTerms() {
		return terms;
	}

	/**
	 * @param string
	 */
	public void setTerms(String string) {
		terms = string;
	}

	private String buildOmnitureCompletePageProductString() {
		// first add the product and terms
		StringBuffer prodBuf = new StringBuffer(this.buildOmnitureProductString());

		// then tack on the quantity and amount
		prodBuf.append(";").append(this.getNumCopies());
		if (this.getAmount() != null && this.getAmount().length() > 0) {
			prodBuf.append(";").append(this.getAmount());
		} else {
			prodBuf.append(";");
		}

		return prodBuf.toString();
	}

	private String buildOmnitureProductString() {
		StringBuffer prodBuf = new StringBuffer(this.getOmnitureProductCategory());

		prodBuf.append(";").append(this.getPubcode());
		if (this.getTerms() != null && this.getTerms().length() > 0) {
			prodBuf.append(" (").append(this.getTerms()).append(")");
		}

		return prodBuf.toString();
	}

	/**
	 * @return
	 */
	public String getOmnitureProductCategory() {
		if (omnitureProductCategory == null) {
			return "";
		}
		return omnitureProductCategory;
	}

	/**
	 * @param string
	 */
	public void setOmnitureProductCategory(String string) {
		omnitureProductCategory = string;
	}

	/**
	 * @return
	 */
	public String getEzPaySelected() {
		return ezPaySelected;
	}

	/**
	 * @param string
	 */
	private void setEzPaySelected(String string) {
		ezPaySelected = string;
	}

	/**
	 * @return
	 */
	public String getOrderType() {
		return orderType;
	}

	/**
	 * @param string
	 */
	public void setOrderType(String string) {
		orderType = string;
	}

}
