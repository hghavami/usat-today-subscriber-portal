/*
 * Created on Mar 24, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.usatoday.esub.common;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.naming.NamingException;

import com.usatoday.util.constants.UsaTodayConstants;

/**
 * @author aeast
 * 
 *         To change the template for this generated type comment go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and
 *         Comments
 */
public final class DataSourceConnectionFactory {

	// TODO: Add methods to obtain a secure connection

	private static boolean jndiAlertSent = false;
	private static boolean jdbcAlertSent = false;

	// private static int openedConns = 0;
	// private static int closedConns = 0;

	/**
	 * 
	 */
	private DataSourceConnectionFactory() {
		super();
	}

	public static synchronized java.sql.Connection getDBConnection() throws UsatDBConnectionException {
		java.sql.Connection conn = null;

		try {
			if (UsaTodayConstants.debug) {
				System.out.println("Opening JNDI DB Conection to esub.");
			}
			conn = DataSourceConnectionFactory.getJNDIDataSource();

			if (UsaTodayConstants.debug) {
				System.out.println("Done Opening JNDI DB Conection to esub.");
			}

			// DataSourceConnectionFactory.openedConns++;
			// System.out.println("Opened Count: " + DataSourceConnectionFactory.openedConns);

			// if the indicator that an alert has been set, and we get here
			// then we can assume all is well again so set the flag back again
			if (DataSourceConnectionFactory.jndiAlertSent) {
				DataSourceConnectionFactory.jndiAlertSent = false;
			}
		} catch (UsatDBConnectionException dbe) {
			if (DataSourceConnectionFactory.jndiAlertSent == false) {
				DataSourceConnectionFactory.jndiAlertSent = true;

				if (UTCommon.SEND_ALERTS) {
					// send alert
					EmailAlert eAlert = new EmailAlert();
					eAlert.setSender(UTCommon.ALERT_SENDER_EMAIL);
					eAlert.setReceiverList(UTCommon.ALERT_RECEIVER_EMAIL);
					eAlert.setSubject("Unable to establish DB connection via JNDI");
					eAlert.setBodyText(dbe.getMessage());
					eAlert.sendAlert();
				}
				System.out.println(dbe.getMessage());
			}

			// faled to obtain JNDI connection set flag to attempt a direct JDBC connection
			conn = null;
		}

		// if no connect attempt direct connect
		if (conn == null) {
			try {
				if (UsaTodayConstants.debug) {
					System.out.println("Opening JDBC Conection ");
				}
				conn = DataSourceConnectionFactory.getJDBCDataSource();

				if (UsaTodayConstants.debug) {
					System.out.println("Done Opening JDBC Conection ");
				}
				// DataSourceConnectionFactory.openedConns++;
				// System.out.println("Opened Count: " + DataSourceConnectionFactory.openedConns);

				if (DataSourceConnectionFactory.jdbcAlertSent) {
					DataSourceConnectionFactory.jdbcAlertSent = false;
				}
			} catch (UsatDBConnectionException dbe) {
				if (DataSourceConnectionFactory.jdbcAlertSent == false) {
					DataSourceConnectionFactory.jdbcAlertSent = true;
					if (UTCommon.SEND_ALERTS) {
						// send alert
						EmailAlert eAlert = new EmailAlert();
						eAlert.setSender(UTCommon.ALERT_SENDER_EMAIL);
						eAlert.setReceiverList(UTCommon.ALERT_RECEIVER_EMAIL);
						eAlert.setSubject("Unable to establish DB connection via direct JDBC");
						eAlert.setBodyText(dbe.getMessage());
						eAlert.sendAlert();
					}
					System.out.println(dbe.getMessage());
				}
				throw dbe;
			}
		}
		return conn;
	}

	public static synchronized java.sql.Connection getDBConnection(String userID, String password) throws UsatDBConnectionException {
		java.sql.Connection conn = null;

		if (userID == null || password == null) {
			return DataSourceConnectionFactory.getDBConnection();
		}

		try {
			conn = DataSourceConnectionFactory.getJNDIDataSource(userID, password);

			// DataSourceConnectionFactory.openedConns++;
			// System.out.println("Opened Count: " + DataSourceConnectionFactory.openedConns);

			// if the indicator that an alert has been set, and we get here
			// then we can assume all is well again so set the flag back again
			if (DataSourceConnectionFactory.jndiAlertSent) {
				DataSourceConnectionFactory.jndiAlertSent = false;
			}
		} catch (UsatDBConnectionException dbe) {
			if (DataSourceConnectionFactory.jndiAlertSent == false) {
				DataSourceConnectionFactory.jndiAlertSent = true;

				if (UTCommon.SEND_ALERTS) {
					// send alert
					EmailAlert eAlert = new EmailAlert();
					eAlert.setSender(UTCommon.ALERT_SENDER_EMAIL);
					eAlert.setReceiverList(UTCommon.ALERT_RECEIVER_EMAIL);
					eAlert.setSubject("Unable to establish DB connection via JNDI - with userid - password specified.");
					eAlert.setBodyText(dbe.getMessage());
					eAlert.sendAlert();
				}
				System.out.println(dbe.getMessage());
			}

			// faled to obtain JNDI connection set flag to attempt a direct JDBC connection
			conn = null;
		}

		// if no connect attempt direct connect
		if (conn == null) {
			try {
				conn = DataSourceConnectionFactory.getJDBCDataSource();

				// DataSourceConnectionFactory.openedConns++;
				// System.out.println("Opened Count: " + DataSourceConnectionFactory.openedConns);

				if (DataSourceConnectionFactory.jdbcAlertSent) {
					DataSourceConnectionFactory.jdbcAlertSent = false;
				}
			} catch (UsatDBConnectionException dbe) {
				if (DataSourceConnectionFactory.jdbcAlertSent == false) {
					DataSourceConnectionFactory.jdbcAlertSent = true;
					if (UTCommon.SEND_ALERTS) {
						// send alert
						EmailAlert eAlert = new EmailAlert();
						eAlert.setSender(UTCommon.ALERT_SENDER_EMAIL);
						eAlert.setReceiverList(UTCommon.ALERT_RECEIVER_EMAIL);
						eAlert.setSubject("Unable to establish DB connection via direct JDBC");
						eAlert.setBodyText(dbe.getMessage());
						eAlert.sendAlert();
					}
					System.out.println(dbe.getMessage());
				}
				throw dbe;
			}
		}
		return conn;
	}

	public static void closeConnection(Connection conn) {
		try {
			if (conn != null) {
				if (!conn.isClosed()) {
					conn.close();
					// DataSourceConnectionFactory.closedConns++;
					// System.out.println("Closed Count: " + DataSourceConnectionFactory.closedConns);
				}
				// else {
				// conn closed already
				// DataSourceConnectionFactory.closedConns++;
				// System.out.println("Closed Count: " + DataSourceConnectionFactory.closedConns);
				// }
			}
		} catch (Exception e) {
			System.out.println("Exception closing db connection: " + e.getMessage());
		}
	}

	private static java.sql.Connection getJNDIDataSource() throws UsatDBConnectionException {
		java.sql.Connection conn = null;
		Object dataSourceObj = null;
		try {

			javax.naming.InitialContext ic = new javax.naming.InitialContext();

			if (UsaTodayConstants.debug) {
				System.out.println("Looking up :" + UTCommon.JNDI_DATA_SOURCE_NAME);
			}

			dataSourceObj = ic.lookup(UTCommon.JNDI_DATA_SOURCE_NAME);

			if (dataSourceObj != null) {
				javax.sql.DataSource ds = (javax.sql.DataSource) dataSourceObj;

				try {
					// first try the container managed credentials
					conn = ds.getConnection();
					if (UsaTodayConstants.debug) {
						System.out.println("Got a JNDI DB Conection using container credentials.");
					}
				} catch (SQLException f1) {
					try {
						// as a backup try the user id and password in config file
						conn = ds.getConnection(UTCommon.JDBC_USER_ID, UTCommon.JDBC_PASSWORD);
						if (UsaTodayConstants.debug) {
							System.out.println("Got a JNDI DB Conection using UTCommon credentials." + UTCommon.JDBC_USER_ID
									+ " / " + UTCommon.JDBC_PASSWORD);
						}
					} catch (SQLException f2) {
						// throw original exception
						throw f1;
					}
				}
			}
		} catch (NamingException ne) {
			System.out.println("NamingException: " + ne.getExplanation() + "  " + ne.getMessage());

			UsatDBConnectionException dbc = new UsatDBConnectionException("NamingException during Data source lookup: "
					+ ne.getExplanation());
			dbc.setRootCause(ne);
			throw dbc;
		} catch (SQLException sqle) {
			System.out.println("SQL Exception in JNDI lookup: " + sqle.getErrorCode() + sqle.getMessage());
			UsatDBConnectionException dbc = new UsatDBConnectionException("SQL Exception during Data source lookup: ERROR CODE: "
					+ sqle.getErrorCode() + "; ERR MESSAGE: " + sqle.getMessage() + "; SQL STATE:" + sqle.getSQLState());
			dbc.setRootCause(sqle);
			throw dbc;
		}

		return conn;
	}

	private static java.sql.Connection getJNDIDataSource(String userID, String password) throws UsatDBConnectionException {
		java.sql.Connection conn = null;
		Object dataSourceObj = null;
		try {

			javax.naming.InitialContext ic = new javax.naming.InitialContext();
			dataSourceObj = ic.lookup(UTCommon.JNDI_DATA_SOURCE_NAME);

			if (dataSourceObj != null) {
				javax.sql.DataSource ds = (javax.sql.DataSource) dataSourceObj;

				try {
					// first try the container managed credentials
					conn = ds.getConnection();
				} catch (SQLException f1) {
					try {
						// as a backup try the user id and password in config file
						conn = ds.getConnection(userID, password);
					} catch (SQLException f2) {
						System.out.println("Exception trying to log into data source: " + f2.getMessage());
						// throw original exception
						throw f1;
					}
				}
			}
		} catch (NamingException ne) {
			System.out.println("NamingException: " + ne.getExplanation() + "  " + ne.getMessage());

			UsatDBConnectionException dbc = new UsatDBConnectionException("NamingException during Data source lookup: "
					+ ne.getExplanation());
			dbc.setRootCause(ne);
			throw dbc;
		} catch (SQLException sqle) {
			System.out.println("SQL Exception in JNDI lookup: " + sqle.getErrorCode() + sqle.getMessage());
			UsatDBConnectionException dbc = new UsatDBConnectionException("SQL Exception during Data source lookup: ERROR CODE: "
					+ sqle.getErrorCode() + "; ERR MESSAGE: " + sqle.getMessage() + "; SQL STATE:" + sqle.getSQLState());
			dbc.setRootCause(sqle);
			throw dbc;
		}

		return conn;
	}

	private static java.sql.Connection getJDBCDataSource() throws UsatDBConnectionException {
		java.sql.Connection conn = null;
		try {
			// load the JDBC driver
			Class.forName(UTCommon.JDBC_DRIVER);
			conn = DriverManager.getConnection(UTCommon.JDBC_DSN_NAME, UTCommon.JDBC_USER_ID, UTCommon.JDBC_PASSWORD);
		} catch (Exception ee) {
			throw new UsatDBConnectionException(ee.getMessage());
		}
		return conn;
	}

}
