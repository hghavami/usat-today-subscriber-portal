package com.usatoday.esub.common;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.joda.time.DateTime;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.customer.CustomerIntf;
import com.usatoday.business.interfaces.customer.TrialCustomerIntf;
import com.usatoday.business.interfaces.products.OfferIntf;
import com.usatoday.business.interfaces.products.SubscriptionOfferIntf;
import com.usatoday.business.interfaces.products.SubscriptionProductIntf;
import com.usatoday.business.interfaces.products.promotions.PromotionIntf;
import com.usatoday.businessObjects.customer.CustomerBO;
import com.usatoday.businessObjects.customer.TrialCustomerBO;
import com.usatoday.businessObjects.products.SubscriptionOfferManager;
import com.usatoday.businessObjects.products.SubscriptionProductBO;
import com.usatoday.businessObjects.products.promotions.PromotionManager;
import com.usatoday.businessObjects.products.promotions.PromotionSet;
import com.usatoday.esub.cookies.AuthenticationCookieProcessor;
import com.usatoday.esub.ncs.handlers.CustomerHandler;
import com.usatoday.esub.trials.handlers.TrialCustomerHandler;
import com.usatoday.util.constants.UsaTodayConstants;

public class UTCommon {

	protected static long lastConfigTime = 0;

	// Nexternal Integration
	public static String NEXTERNAL_INTEGRATION_URL = "https://www.nexternal.com/usatoday/default.aspx";
	public static String NEXTERNAL_REPLYTO_URL = "https://service.usatoday.com/shop/integrationFailed.html";

	public final static int ACCNT_ADDL_ADDRESS_ONE = 6;
	public final static int ACCNT_ADDL_ADDRESS_TWO = 7;
	public final static int ACCNT_ADDRESS_ONE = 5;
	public final static int ACCNT_ADDRESS_TWO = 8;
	public final static int ACCNT_HOME_PHONE = 12;
	public final static int ACCNT_AUTO_PAY = 10;
	public final static int ACCNT_NUM_COPIES = 11;
	public final static int ACCNT_EXPIRE_DATE = 2;
	public final static int ACCNT_FIRM = 9;
	public final static int ACCNT_LAST_PAY = 0;
	public final static int ACCNT_NAME = 3;
	public final static int ACCNT_NUMBER = 4;
	public final static int ACCNT_START_DATE = 1;

	public static String BASE_URL = ""; // extranet.ini
	public static String REDIRECT_URL = "";
	public static String REDIRECT_COM_URL = "";

	public static String ACCOUNT_LOGIN_URL = BASE_URL + "/login";
	public static String FIRSTTIME_URL = BASE_URL + "/firsttime";
	public static String ALERT_RECEIVER_EMAIL = ""; // extranet.ini
	public static String ALERT_SENDER_EMAIL = ""; // extranet.ini

	public final static int BILLING_ADDRESS = 6;
	public final static int BILLING_CODE1_ADDRESS = 1;
	public final static int BILLING_USER_ADDRESS = 0;
	public static String BLINDCC_EMAIL = ""; // extranet.ini
	public static String BWFROM_EMAIL = ""; // extranet.ini
	public final static String BWPUBCODE = "BW";
	public static String CHANGEADDRESS_URL = BASE_URL + "/account/changeaddress";
	public static String CHANGEBILLING_URL = BASE_URL + "/account/changebilling";
	public static String CID_NUMBER = "";
	public static String CID_PROMO_1 = "";
	public static String CID_PROMO_2 = "";
	public static String CODE_1_WEB_SERVICE_URL = ""; // Actual location of code1 server from extranet.ini or default value.
	public static String CODE_1_PORT = ""; // Code-1 port from extranet.ini
	public static String CODE_1_PATH = ""; // Code-1 path from extranet.ini
	public static String CODE_1_ACCOUNT_ID = ""; // Code-1 account from extranet.ini
	public static String CODE_1_ACCOUNT_PASSWORD = ""; // Code-1 password from extranet.ini
	public static String CODE_1_CONNECTION_TYPE = ""; // Code-1 connection type from extranet.ini
	public static String CODE_1_CONNECTION_TIMEOUT = ""; // Code-1 connection timeout from extranet.ini
	public static String CODE_1_OUT_REC_TYPE = ""; // Code-1 output type from extranet.ini
	public static String CODE_1_OUT_FORMAT = ""; // Code-1 output format from extranet.ini
	public static String CODE_1_OUT_CASE = ""; // Code-1 casing from extranet.ini
	public static String CODE_1_OUT_CODE_SEP = ""; // Code-1 output separators from extranet.ini
	public static String CODE_1_MULTI_NAT_CHAR = ""; // Code-1 national characters from extranet.ini
	public static String CODE_1_OUT_COUNTRY = ""; // Code-1 output country from extranet.ini
	public static String CODE_1_OUT_RET_CODES = ""; // Code-1 output field return codes from extranet.ini
	public static String CODE_1_DPV = ""; // Code-1 DPV from extranet.ini
	public static String CODE_1_RDI = ""; // Code-1 RDI from extranet.ini
	public static String CODE_1_ESM = ""; // Code-1 ESM
	public static String CODE_1_STREET_STR = ""; // Code-1 street strictness from extranet.ini
	public static String CODE_1_FIRM_STR = ""; // Code-1 firm strictness from extranet.ini
	public static String CODE_1_DIR_STR = ""; // Code-1 directional strictness from extranet.ini
	public static String CODE_1_DUAL_ADDR = ""; // Code-1 dual address logic from extranet.ini
	public static String CODE_1_OUT_SHORT_CITY = ""; // Code-1 output short city from extranet.ini

	public final static int CODE1_ADDRESS = 1;
	public static String COMM_AMOUNT = "";
	public final static int COMM_JUNCTION = 8;

	// Commission Junction Account Number - These parms are pulled from extranet.ini
	public static boolean COMM_JUNCTION_ACTIVE = true;
	public static String COMM_JUNCTION_SW_TYPE = "108456";
	public static String COMM_JUNCTION_URL = "https://www.commission-junction.com/track/receive.dll";
	public static String COMM_JUNCTION_UT_TYPE = "3986";
	public static String COMPLETED_TRANS_URL = BASE_URL + "/common/completedtransaction.jsp";
	public final static int CREDIT_CARD = 5;
	public final static int CUSTOMER_DATA = 3;
	public final static int CREDITCARD_INFO = 4;
	public static String CUSTOPT_QUERYSTRING = "custopt";
	public final static String DATABASE_ERROR_MESSAGE = "Your transaction could not be processes.  "
			+ "Please, return to the previous screen and retry your action.  "
			+ "If unsuccessful, please contact our customer service to complete your transaction.   ";
	public static String DEFAULT_BWPROMO_KEYCODE = ""; // extranet.ini
	public static String DEFAULT_BWRATECODE = ""; // extranet.ini

	public static final String DEFAULT_CODE_1_WEB_SERVICE_URL = "asp.g1.com"; // Default Code 1 validation Server.

	// CRYPTO
	public static boolean STRONG_ENCRYPT = false;
	public static String PUBLIC_KEY_ALIAS = "";
	public static String KEYSTORE = "";
	public static String KEYSTORE_PASSWORD = "";

	public static String DEFAULT_PROMO_TYPE = ""; // extranet.ini
	public static String DEFAULT_PTI_CREDENTIAL_USER_NAME = ""; // PTI server Credential User Name
	public static String DEFAULT_PTI_CREDENTIAL_VALUE = ""; // PTI server Credential Value
	public static String DEFAULT_PTI_EXTERNAL_ISSUER_ID = ""; // PTI server External Issuer ID
	public static String DEFAULT_PTI_EXTERNAL_MERCHANT_ID = ""; // PTI server External Merchant ID
	public static String DEFAULT_PTI_TERMINAL_ID = ""; // PTI server External Merchant ID
	public static String DEFAULT_PTI_ISSUER_ID = ""; // PTI server Issuer ID
	public static String DEFAULT_PTI_PATH = ""; // path of pti.ini for PTI credential defaults
	public static String DEFAULT_PTI_SW_CLIENT_ID = "2161781645918776038"; // Client ID for PTI SW
	public static String DEFAULT_PTI_URL = ""; // PTI server URL
	public static String DEFAULT_PTI_UT_CLIENT_ID = "2161781645918776038"; // Client ID for PTI UT
	public static String DEFAULT_PTI_LOCATION_CLIENT_ID = ""; // ini file
	public static String DEFAULT_UTPROMO_KEYCODE = ""; // extranet.ini
	public static String DEFAULT_UTRATECODE = ""; // extranet.ini
	public final static int DELIVERY_CODE1_ADDRESS = 1;
	public final static int DELIVERY_START_DATE = 2;

	public final static int DELIVERY_USER_ADDRESS = 0;
	public static String DEVCC_EMAIL = ""; // extranet.ini
	public final static int EMAIL_DATA = 7;
	public static String EMAIL_REMOVAL_ADDRESS = "";
	public static String EMAIL_SUBJECT = ""; // extranet.ini
	public static String ERROR_URL = BASE_URL + "/error.jsp";
	public static String FEEDBACK_EMAIL = ""; // extranet.ini
	public static String FEEDBACK_SUBJECT = ""; // extranet.ini

	public static String ID_PASSWORD_URL = BASE_URL + "/idpassword";

	public static String IMAGEMAP_LINKS[] = { ".", "." };

	public static String IMAGEMAP_TEXT[] = { "Index", "Links" };
	public static String IMAGES_URL = BASE_URL + "/images";
	public static java.lang.String INI_FILE = "\\usat\\extranet\\extranet.ini";
	public static String JDBC_DRIVER = ""; // extranet.ini

	public static String JDBC_DSN_NAME = ""; // extranet.ini
	public final static String JDBC_PASSWORD = "subscribe";

	public final static String JDBC_USER_ID = "usatSPODBC";
	public static String JNDI_DATA_SOURCE_NAME = "jdbc/esub"; // override in extranet.ini

	public static String KEYCODE_QUERYSTRING = "keycode";

	public static boolean LINKSHARE_ACTIVE = false;
	public static String LINKSHARE_DEFAULT_SW_BOUNTY_AMOUNT = null;
	public static String LINKSHARE_DEFAULT_UT_BOUNTY_AMOUNT = null;
	public static String LINKSHARE_SW_BOUNTY_AMOUNT = null;
	public static boolean LINKSHARE_USE_BOUNTY_AMOUNTS = false;
	public static String LINKSHARE_UT_BOUNTY_AMOUNT = null;
	public static String LOGIN_SESSION_OBJECT_QUERYSTRING = "logsessobj";
	public static String LOGIN_URL = BASE_URL + "/login.jsp";
	public static String NAVBAR_HOME_IMAGE = IMAGES_URL + "/home_link.gif";
	public static String NAVBAR_HOME_IMAGE_RO = IMAGES_URL + "/home_link_RO.gif";

	public static String NAVBAR_HOME_LINK = BASE_URL + "/index.jsp";
	public static String NAVBAR_HOME_TEXT = "Home";

	public static String NAVBAR_SWHOME_LINK = BASE_URL + "/index.jsp?pub=BW";

	public static String NEWSUBSCRIPTION_URL = BASE_URL + "/subscriptions/newsubscription";
	public final static int OFFER = 2;
	public static String OTHEROPT_QUERYSTRING = "otheropt";
	public static String PAYTYPE_QUERYSTRING = "paytype";
	public static String PUB_BANNERS[] = { IMAGES_URL + "/banner_usat2.gif", IMAGES_URL + "/banner_bbw2.jpg" };

	// SEW
	public final static int PUB_DAYS_OUT = 4;
	public final static String TRAN_NEW_START_DATE = "TRAN_NEW_START_DATE";

	public static String PUB_NAMES[] = { "UT", "BW" };
	public static String PUB_QUERYSTRING = "pub";
	public static String RENEWAL_TRACKING_CODE_QUERYSTRING = "RTC";
	public static String DEFAULT_RENEWAL_TRACKING_CODE = ""; // extranet.ini
	public static String SESSION_RENEWAL_TRACKING_CODE = "session.renewal_tracking_code";
	public static String SESSION_URL_RENEWAL_OFFER = "session.renewal_terms";
	public final static int PUB_UT_VAC_DAYS_OUT = 3; // # of pub days
	public final static int PUB_BW_VAC_DAYS_OUT = 2; // # of pub days (1 per week)
	public final static String TRAN_START_DATE = "TRAN_START_DATE";
	public final static String TRAN_STOP_DATE = "TRAN_STOP_DATE";
	public final static int PUBLICATION = 1;
	public static String RENEWALS_URL = "/subscriptions/renewals";
	public static boolean SEND_ALERTS = true; // extranet.ini
	// get information from the XTRNTPROMO table

	public static String SERVER_URL = ""; // extranet.ini SEW

	public static String SERVLET_URL = ""; // extranet.ini
	public static String SESSION_ACCOUNT_INFO = "session.accountinfo";
	public static String SESSION_CUSTOMER_INFO = "session.customerinfo";
	public static String SESSION_ACCOUNT_PATH = "session.accountpath";

	public static String SESSION_ADDRESS = "session.usatesubaddress";
	public static String SESSION_ADDRESS_ERROR = "session.usatesubaddresserror";
	public static java.lang.String SESSION_ALT_START_DATE_ERROR = "session.usatesubaltdateerror";
	public static String SESSION_BILLING_ADDRESS = "session.usatesubbilladdress";
	public static String SESSION_BILLING_ADDRESS_ERROR = "session.usatesubbilladdresserror";
	public static String SESSION_BILLMEALLOWED = "session.billmeallowed";
	public static String SESSION_CHANGE_ADDRESS = "session.usatesubchaddress";
	public static String SESSION_CHANGE_BILLING = "session.usatesubchbilling";
	public static String SESSION_CJ_PHONE = "session.cjphone";

	public static java.lang.String SESSION_CREDIT_CARD_CHECK = "session.usatesubcccheck";
	public static String SESSION_CREDIT_CARD_ERROR = "session.usatesubccerror";
	public static String SESSION_ERROR_SWITCH = "session.usatesubswitch";
	public static String SESSION_FEEDBACK_INFO = "session.feedbackinfo";
	public static String SESSION_FIELD = "session.usatesub";
	public static String SESSION_FIELD_ERROR = "session.usatesuberror";
	public static String SESSION_FIELD_HOLD = "session.usatehold";
	public static String SESSION_FIELD_RENEWAL = "session.usatrenewal";
	public static String SESSION_KEYCODE = "session.pub_keycode";
	public static String SESSION_URL_RATECODE = "session.url_ratecode";
	public static final String SESSION_LINKSHARE = "SESSION.LINKSHARE_TRANS";
	public static String SESSION_PAYTYPE = "session.pub_paytype";
	public static String SESSION_PUB_BANNER = "session.pub_banner";
	public static String SESSION_PUB_NAME = "session.pub_name";
	public static String SESSION_NEXT_CATG = "session.next_catg";
	public static String SESSION_EARLY_ALERT = "session.early_alert";
	public static String SESSION_EARLY_ALERT_ACCOUNT = "session.early_alert_account";

	public static String SESSION_UNIQUE_ID = "session.usatuniqueid";
	public static String SESSION_VACATION_HOLD = "session.usatesubvacationhold";
	public static String SESSION_FIRST_TIME = "session.firsttime";
	public static String SMTP_SERVER = ""; // extranet.ini
	public static String GLOBAL_EMAIL_PROMO_TEXT = "";
	public static String UT_EMAIL_PROMO_TEXT = "";
	public static String UT_EMAIL_COMP_TEXT = "";
	public static String BW_EMAIL_COMP_TEXT = "";

	public final static int START_DATE = 4;

	public final static String STEP = "STEP";

	public static String SUBSCRIPTION_URL = BASE_URL + "/subscriptions";
	public static String SUBSOPT_QUERYSTRING = "subsopt";

	public final static java.lang.String SUBTYPE = "SUBTYPE";

	// hidden field in each form used to manage the order of the forms
	public final static int TERMS = 0;

	public final static int USER_ADDRESS = 0;

	public static String UTFROM_EMAIL = ""; // extranet.ini
	public final static String UTPUBCODE = "UT";
	public final static String NEXT_CATG_PARM = "catg";

	public final static int VAC_NIE_TEXT = 0;
	public final static int VAC_START_DATE = 0;
	public final static int VAC_STOP_DATE = 1;
	public static String VACATIONHOLDS_URL = BASE_URL + "/account/vacationholds";
	public static boolean VALIDATE_CC = false; // extranet.ini should we validate CCs

	// APE - 01/13/2002
	public static boolean OMNITURE_TRACKING_ACTIVE = false; // override in .ini file
	public static String OMNITURE_SCRIPT_LOCATION = "/include/s_code.js"; // override in .ini file
	public static boolean OMNITURE_USE_DYNAMIC_PAGE_NAMES = true;

	// This method converts a date field to CYYMMDD (e.g. 1051215) format and returns a string
	public static String convertDateCYYMMDD(Date inputDate) {

		DateTime dt = new DateTime(inputDate.getTime());
		StringBuffer as400Format = new StringBuffer();
		DateTime y2k = new DateTime(2000, 1, 1, 0, 0, 0, 0);
		String result = null;

		if (dt.isAfter(y2k)) {
			as400Format.append("1");
		} else {
			as400Format.append("0");
		}
		as400Format.append(dt.toString("YYMMdd"));
		result = as400Format.toString();

		return result;
	}

	public static String displayChangeIDPasswordMessage(HttpServletRequest req, HttpServletResponse res, HttpServlet servlet)
			throws ServletException, IOException {
		StringBuffer result = new StringBuffer();

		// get the session object; create one if necessary
		HttpSession session = req.getSession(true);

		// retrieve the current session's data object
		String errmsg = null;
		try {
			Object e = session.getAttribute(UTCommon.SESSION_FIELD_ERROR);

			if (e != null) {
				session.removeAttribute(UTCommon.SESSION_FIELD_ERROR);
			}

			if (e instanceof String) {
				errmsg = (String) e;
			} else if (e instanceof UTError) {
				errmsg = ((UTError) e).getText();
			} else {
				errmsg = e.toString();
			}
		} catch (Exception e) {
			errmsg = " ";
			// no login data object
		}

		if (errmsg == null) {
			errmsg = " ";
		}

		// display the message
		result.append(errmsg);

		return result.toString();
	}

	public static String displayError(HttpServletRequest req, HttpServletResponse res, HttpServlet servlet)
			throws ServletException, IOException {
		StringBuffer result = new StringBuffer("");

		UTError error = null;
		Object eObj = null;

		// get the session object; error if no session object is retrieved
		HttpSession session = req.getSession(false);
		if (session == null) {
			error = new UTError("Session does not exist.");
		} else {
			// retrieve the current session's error object
			eObj = session.getAttribute(UTCommon.SESSION_FIELD_ERROR);
			if (eObj == null) {
				error = new UTError("Unexpected Error Occurred.");
			}
		}

		try {
			// log and display the error

			if (eObj != null && eObj instanceof UTError) {
				error = (UTError) eObj;
				result.append(error.toHTML());
			} else {
				if (error != null) {
					result.append(error.toHTML());
				} else {
					if ((eObj != null) && (eObj instanceof String)) {
						result.append((String) eObj);
					}
				}
			}

			if (error != null) {
				servlet.log(error.getText() + " Referer: " + req.getHeader("Referer") + " request URL: " + req.getRequestURL());
			}
			// error.getException().printStackTrace(out);
		} catch (Exception e) {
			// No exception
		}

		return result.toString();
	}

	public static String displayFirstTimeMessage(HttpServletRequest req, HttpServletResponse res, HttpServlet servlet)
			throws ServletException, IOException {
		StringBuffer result = new StringBuffer();

		// get the session object; create one if necessary
		HttpSession session = req.getSession(true);
		res.getWriter();

		// retrieve the current session's data object
		String errmsg = null;
		try {
			Object eObj = session.getAttribute(UTCommon.SESSION_FIELD_ERROR);
			// Now that we have saved the session error object just remove it
			session.removeAttribute(UTCommon.SESSION_FIELD_ERROR);
			if (eObj instanceof String) {
				errmsg = (String) eObj;
			} else if (eObj instanceof UTError) {
				errmsg = ((UTError) eObj).getText();
			} else {
				errmsg = " ";
			}
		} catch (Exception e) {
			errmsg = " ";
			// no login data object
		}

		if (errmsg == null) {
			errmsg = " ";
		}

		// display the message
		result.append(errmsg);

		// Blank out error message object
		// session.setAttribute(UTCommon.SESSION_FIELD_ERROR, " ");

		return result.toString();
	}

	public static String displayForgotPasswordMessage(HttpServletRequest req, HttpServletResponse res, HttpServlet servlet)
			throws ServletException, IOException {
		StringBuffer result = new StringBuffer();

		// get the session object; create one if necessary
		HttpSession session = req.getSession(true);

		// retrieve the current session's data object
		String errmsg = null;
		try {
			Object eObj = session.getAttribute(UTCommon.SESSION_FIELD_ERROR);
			if (eObj instanceof String) {
				errmsg = (String) eObj;
			} else {
				errmsg = " ";
			}
		} catch (Exception e) {
			errmsg = " ";
			// no login data object
		}

		// display the message
		result.append(errmsg);

		// save the login data object
		session.setAttribute(UTCommon.SESSION_FIELD_ERROR, " ");

		return result.toString();
	}

	public static String displayPubFAQLinkLearnMoreEZPAY(HttpSession session) throws ServletException, IOException {
		StringBuffer result = new StringBuffer();

		// Determine the Publication Code.
		String pubcode = null;
		try {
			pubcode = (String) session.getAttribute(UTCommon.SESSION_PUB_NAME);
			if (pubcode == null) {
				pubcode = UsaTodayConstants.UT_PUBCODE;
			}
		} catch (Exception e) {
			pubcode = UsaTodayConstants.UT_PUBCODE;
		}

		if (pubcode.equals(UTCommon.PUB_NAMES[0])) {
			result.append("<a href=\"javascript:;\" onClick=\"javascript:window.open('/faq/utfaq.jsp#section6','FAQ', 'width=800,height=800,menubar=no,titlebar=yes,scrollbars=yes,status=no,location=no,directories=no')\"<SPAN style=\"font-family: Arial; font-size: 7pt\">&nbsp;&nbsp;Learn More...</font></a>");

		} else {
			result.append("<a href=\"javascript:;\" onClick=\"javascript:window.open('/faq/bwfaq.jsp#section5','FAQ', 'width=800,height=800,menubar=no,titlebar=yes,scrollbars=yes,status=no,location=no,directories=no')\"<SPAN style=\"font-family: Arial; font-size: 7pt\">&nbsp;&nbsp;Learn More...</font></a>");

		}

		return result.toString();
	}

	public static String displayPubFAQLink(HttpSession session) throws ServletException, IOException {
		StringBuffer result = new StringBuffer();

		// Determine the Publication Code.
		String pubcode = null;
		try {
			pubcode = (String) session.getAttribute(UTCommon.SESSION_PUB_NAME);
		} catch (Exception e) {
			pubcode = (String) session.getAttribute(UTCommon.SESSION_PUB_NAME);
		}

		if (pubcode == null || pubcode.equals(UTCommon.PUB_NAMES[0])) {
			result.append("<ul><li><font face=Arial><a target=\"_blank\" href=utfaq.jsp>View FAQ</a></font></li></ul>");
		} else {
			result.append("<ul><li><font face=Arial><a target=\"_blank\" href=bwfaq.jsp>View FAQ</a></font></li></ul>");
		}

		return result.toString();
	}

	public static String displayPubFAQText(HttpSession session) throws ServletException, IOException {
		StringBuffer result = new StringBuffer();

		// Determine the Publication Code.
		String pubcode = null;
		try {
			pubcode = (String) session.getAttribute(UTCommon.SESSION_PUB_NAME);
		} catch (Exception e) {
			pubcode = (String) session.getAttribute(UTCommon.SESSION_PUB_NAME);
		} finally {
			if (pubcode == null) {
				pubcode = UTCommon.UTPUBCODE;
				session.setAttribute(UTCommon.SESSION_PUB_NAME, UTCommon.UTPUBCODE);
			}
		}

		if (pubcode.equals(UTCommon.PUB_NAMES[0])) {
			result.append("Welcome to USA TODAY's Frequently Asked Questions "
					+ "(FAQ) section.  Use this section to find answers to general questions.  "
					+ "If you need answers to questions that are not addressed here, please call us at "
					+ "1-800-872-0001 M-F 8AM - 7PM ET.  You will be able to speak to our "
					+ "National Customer Service Department directly.");
		} else {
			result.append("Welcome to USA TODAY Sports Weekly's Frequently Asked Questions "
					+ "(FAQ) section.  Use this section to find answers to general questions.  If "
					+ "you need answers to questions that are not addressed here, please call us at \n"
					+ "1-800-872-1415 M-F 8AM - 7PM ET.  You will be able to speak to our "
					+ "National Customer Service Department directly.");
		}

		return result.toString();
	}

	// Display Graphics using "Default" image type
	public static String displayPubGraphics(HttpServletRequest request) throws ServletException, IOException {
		StringBuffer result = new StringBuffer();

		String pubcode = "";
		pubcode = (String) request.getSession().getAttribute(UTCommon.SESSION_PUB_NAME);
		if (pubcode == null) {
			pubcode = UsaTodayConstants.UT_PUBCODE;
		}

		String imagename = "/images/shim.gif";
		String altName = "";

		try {
			SubscriptionOfferIntf offer = SubscriptionOfferManager.getInstance().getDefaultOffer(pubcode);

			PromotionIntf promo = (PromotionIntf) offer.getPromotionSet().getPromoConfigs().get(UTCommon.DEFAULT_PROMO_TYPE);

			if (promo != null) {
				imagename = promo.getName();
				altName = promo.getAltName();
			}
		} catch (Exception e) {
			; // skip it
		}

		result.append("<td valign=\"middle\" valign=\"top\"><fontface=\"Arial\"><img border=\"0\"src=\"" + imagename + "\" alt=\""
				+ altName + "\">");

		result.append("</font></td>");

		return result.toString();
	}

	/**
	 * 
	 * Creation date: (8/20/00 10:57:26 PM)
	 * 
	 * @return java.lang.String
	 * @param calendar
	 *            java.util.Calendar
	 * @param format
	 *            java.lang.String
	 */
	public static String formatCalendarAsString(Calendar calendar, String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(calendar.getTime());
	}

	/**
	 * 
	 * Creation date: (8/20/00 10:57:26 PM)
	 * 
	 * @return java.lang.String
	 * @param calendar
	 *            java.util.Calendar
	 * @param date
	 *            java.lang.String must be in MM/DD/YYYY format
	 */
	public static GregorianCalendar formatStringAsCalendar(String date) {
		HashMap<String, String> datemap = UTPubDates.getDateFields(date); // For conversion to GregorianCalendar

		// Convert the string to GregorianCalender for formating

		int pubyear = (new Integer((String) datemap.get("YEAR"))).intValue();
		int pubmonth = (new Integer((String) datemap.get("MONTH"))).intValue();
		int pubday = (new Integer((String) datemap.get("DAY"))).intValue();

		GregorianCalendar startDate = new GregorianCalendar(pubyear, pubmonth - 1, pubday);

		return startDate;

	}

	/**
	 * Retrieves the boolean parameter (a checkbox) from the HttpServletRequest and returns a false if no value is returned or true
	 * if any value is returned
	 * 
	 * @return boolean
	 * @param req
	 *            javax.servlet.http.HttpServletRequest
	 * @param parm
	 *            java.lang.String
	 */
	public static boolean getCheckboxParameterAsBoolean(HttpServletRequest req, String parm) {
		String value = req.getParameter(parm);

		if (value == null) {
			return false;
		} else {
			return true;
		}
	}

	// Get Initial values from the EXTRANET.INI file
	public static String getInitValues() throws ServletException, IOException {
		String result = ""; // set as empty space for now - can be used for messaging

		// retrieve ini values
		iniFiles INI = new iniFiles(UTCommon.INI_FILE);

		INI.loadIni();

		// URL_Address
		String server = INI.getValue("URL_ADDRESS", "server");
		String base = INI.getValue("URL_ADDRESS", "base");
		String servlet = INI.getValue("URL_ADDRESS", "servlet");
		UTCommon.REDIRECT_URL = INI.getValue("URL_ADDRESS", "redirectDomainURL");
		UTCommon.REDIRECT_COM_URL = INI.getValue("URL_ADDRESS", "redirectCOMDomainURL");

		// Renewal Tracking Code defaults
		String default_RTC = INI.getValue("RENEWAL_TRACKING_CODE_DEFAULT", "default_renewal_tracking_code");

		// Rate code defaults
		String bwratecode = INI.getValue("RATECODE_DEFAULT", "bwratecode");
		String utratecode = INI.getValue("RATECODE_DEFAULT", "utratecode");

		// connection parameters
		String jdbcdsnname = INI.getValue("CONNECTION_PARMS", "jdbcdsnname");
		String jdbcdriver = INI.getValue("CONNECTION_PARMS", "jdbcdriver");
		String jndiDataSource = INI.getValue("CONNECTION_PARMS", "JNDI_DATA_SOURCE_NAME");

		// mail parameters
		String smtpserver = INI.getValue("MAIL_PARMS", "smtpserver");
		String utfrommail = INI.getValue("MAIL_PARMS", "utfrommail");
		String bwfrommail = INI.getValue("MAIL_PARMS", "bwfrommail");
		String emailsubject = INI.getValue("MAIL_PARMS", "emailsubject");
		String feedbacksubject = INI.getValue("MAIL_PARMS", "feedbacksubject");
		String feedbackemail = INI.getValue("MAIL_PARMS", "feedbackemail");
		String blindccemail = INI.getValue("MAIL_PARMS", "blindccemail");
		String devccemail = INI.getValue("MAIL_PARMS", "devccemail");

		String utEmailText = INI.getValue("MAIL_PARMS", "ut_email_promo_text");
		if (utEmailText == null || utEmailText.trim().length() == 0) {
			UTCommon.UT_EMAIL_PROMO_TEXT = "";
		} else {
			int newline = utEmailText.indexOf("\\n");
			String ttemp = "";
			while (newline >= 0) {
				ttemp = ttemp + utEmailText.substring(0, newline) + "\n";
				utEmailText = utEmailText.substring(newline + 2);
				newline = utEmailText.indexOf("\\n");
			}
			ttemp = ttemp + utEmailText;
			utEmailText = ttemp;
			UTCommon.UT_EMAIL_PROMO_TEXT = utEmailText.trim();
		}

		String utEmailCompText = INI.getValue("MAIL_PARMS", "ut_email_comp_text");
		if (utEmailCompText == null || utEmailCompText.trim().length() == 0) {
			UTCommon.UT_EMAIL_COMP_TEXT = "";
		} else {
			int newline = utEmailCompText.indexOf("\\n");
			String ttemp = "";
			while (newline >= 0) {
				ttemp = ttemp + utEmailCompText.substring(0, newline) + "\n";
				utEmailCompText = utEmailCompText.substring(newline + 2);
				newline = utEmailCompText.indexOf("\\n");
			}
			ttemp = ttemp + utEmailCompText;
			utEmailCompText = ttemp;
			UTCommon.UT_EMAIL_COMP_TEXT = utEmailCompText.trim();
		}

		String bwEmailCompText = INI.getValue("MAIL_PARMS", "bw_email_comp_text");
		if (bwEmailCompText == null || bwEmailCompText.trim().length() == 0) {
			UTCommon.BW_EMAIL_COMP_TEXT = "";
		} else {
			int newline = bwEmailCompText.indexOf("\\n");
			String ttemp = "";
			while (newline >= 0) {
				ttemp = ttemp + bwEmailCompText.substring(0, newline) + "\n";
				bwEmailCompText = bwEmailCompText.substring(newline + 2);
				newline = bwEmailCompText.indexOf("\\n");
			}
			ttemp = ttemp + bwEmailCompText;
			bwEmailCompText = ttemp;
			UTCommon.BW_EMAIL_COMP_TEXT = bwEmailCompText.trim();
		}

		String globalEmailText = INI.getValue("MAIL_PARMS", "global_email_promo_text");
		if (globalEmailText == null || globalEmailText.trim().length() == 0) {
			UTCommon.GLOBAL_EMAIL_PROMO_TEXT = "";
		} else {
			int newline = globalEmailText.indexOf("\\n");
			String ttemp = "";
			while (newline >= 0) {
				ttemp = ttemp + globalEmailText.substring(0, newline) + "\n";
				globalEmailText = globalEmailText.substring(newline + 2);
				newline = globalEmailText.indexOf("\\n");
			}
			ttemp = ttemp + globalEmailText;
			globalEmailText = ttemp;
			UTCommon.GLOBAL_EMAIL_PROMO_TEXT = globalEmailText.trim();
		}

		UTCommon.SEND_ALERTS = INI.getBoolValue("MAIL_PARMS", "enablealerts");
		if (UTCommon.SEND_ALERTS) {
			UTCommon.ALERT_RECEIVER_EMAIL = INI.getValue("MAIL_PARMS", "alertreceiveremail");
			UTCommon.ALERT_SENDER_EMAIL = INI.getValue("MAIL_PARMS", "alertsender");
		}

		// Commission Junction parameters
		String cidnumber = INI.getValue("COMMISSION_PARMS", "cidnumber");
		String cidpromo1 = INI.getValue("COMMISSION_PARMS", "cidpromo1");
		String cidpromo2 = INI.getValue("COMMISSION_PARMS", "cidpromo2");
		String cidamount = INI.getValue("COMMISSION_PARMS", "cidamount");
		String cidActive = INI.getValue("COMMISSION_PARMS", "commissionActive");
		if (cidActive.equalsIgnoreCase("false")) {
			UTCommon.COMM_JUNCTION_ACTIVE = false;
		} else { // in this case we want a config error to turn on commission junction
			UTCommon.COMM_JUNCTION_ACTIVE = true;
			UTCommon.COMM_JUNCTION_UT_TYPE = INI.getValue("COMMISSION_PARMS", "commissionUTType");
			UTCommon.COMM_JUNCTION_SW_TYPE = INI.getValue("COMMISSION_PARMS", "commissionSWType");
			UTCommon.COMM_JUNCTION_URL = INI.getValue("COMMISSION_PARMS", "commissionURL");
		}

		String linkshareUTBountyAmt = INI.getValue("LINKSHARE_PARMS", "linkshareUTBountyAmount");
		String linkshareSWBountyAmt = INI.getValue("LINKSHARE_PARMS", "linkshareSWBountyAmount");
		String linkshareUseBounty = INI.getValue("LINKSHARE_PARMS", "linkshareUseBountys");
		String linkshareActive = INI.getValue("LINKSHARE_PARMS", "linkshareActive");
		if (linkshareActive != null && linkshareActive.equalsIgnoreCase("true")) {
			UTCommon.LINKSHARE_ACTIVE = true;
		} else {// in this case we want a config error to turn on commission junction
			UTCommon.LINKSHARE_ACTIVE = false;
		}
		if (linkshareUseBounty != null && linkshareUseBounty.equalsIgnoreCase("true")) {
			UTCommon.LINKSHARE_USE_BOUNTY_AMOUNTS = true;
			if (linkshareUTBountyAmt == null || linkshareUTBountyAmt.length() == 0) {
				System.out
						.println("No LinkShare Bounty amount specified for UT when expected. Set value linkshareUTBountyAmount in extranet.ini.");
				UTCommon.LINKSHARE_USE_BOUNTY_AMOUNTS = false;
				UTCommon.LINKSHARE_UT_BOUNTY_AMOUNT = UTCommon.LINKSHARE_DEFAULT_UT_BOUNTY_AMOUNT;
			} else {
				try {
					java.math.BigDecimal bd = new java.math.BigDecimal(linkshareUTBountyAmt);
					bd.setScale(2);

					if (bd.doubleValue() <= 0.0) {
						throw new Exception("UT Bounty less than or equal to 0.0");
					}
					UTCommon.LINKSHARE_UT_BOUNTY_AMOUNT = linkshareUTBountyAmt;
				} catch (Exception ee) {
					System.out.println("Invalid UT Bounty Amount Specified: " + linkshareUTBountyAmt);
					UTCommon.LINKSHARE_USE_BOUNTY_AMOUNTS = false;
					UTCommon.LINKSHARE_UT_BOUNTY_AMOUNT = UTCommon.LINKSHARE_DEFAULT_UT_BOUNTY_AMOUNT;
				}
			}
			// if we don't have an issue with UT then continue with SW
			if (UTCommon.LINKSHARE_USE_BOUNTY_AMOUNTS = true) {
				if (linkshareSWBountyAmt == null || linkshareSWBountyAmt.length() == 0) {
					System.out
							.println("No LinkShare Bounty amount specified for SW when expected. Set value linkshareSWBountyAmount in extranet.ini.");
					UTCommon.LINKSHARE_USE_BOUNTY_AMOUNTS = false;
					UTCommon.LINKSHARE_SW_BOUNTY_AMOUNT = UTCommon.LINKSHARE_DEFAULT_SW_BOUNTY_AMOUNT;
				} else {
					try {
						java.math.BigDecimal bd = new java.math.BigDecimal(linkshareSWBountyAmt);
						bd.setScale(2);

						if (bd.doubleValue() <= 0.0) {
							throw new Exception("SW Bounty less than or equal to 0.0");
						}
						UTCommon.LINKSHARE_SW_BOUNTY_AMOUNT = linkshareSWBountyAmt;
					} catch (Exception ee) {
						System.out.println("Invalid SW Bounty Amount Specified: " + linkshareSWBountyAmt);
						UTCommon.LINKSHARE_USE_BOUNTY_AMOUNTS = false;
						UTCommon.LINKSHARE_SW_BOUNTY_AMOUNT = UTCommon.LINKSHARE_DEFAULT_SW_BOUNTY_AMOUNT;
					}
				}
			}
		} else {
			UTCommon.LINKSHARE_USE_BOUNTY_AMOUNTS = false;
			UTCommon.LINKSHARE_UT_BOUNTY_AMOUNT = UTCommon.LINKSHARE_DEFAULT_UT_BOUNTY_AMOUNT;
			UTCommon.LINKSHARE_SW_BOUNTY_AMOUNT = UTCommon.LINKSHARE_DEFAULT_SW_BOUNTY_AMOUNT;
		}

		// Omniture Tracking Parameters
		UTCommon.OMNITURE_TRACKING_ACTIVE = INI.getBoolValue("WEB_ANALYTICS", "Omniture_trackingActive");
		if (UTCommon.OMNITURE_TRACKING_ACTIVE) {
			String scriptLoc = INI.getValue("WEB_ANALYTICS", "Omniture_scriptLocation");
			if (scriptLoc != null && scriptLoc.trim().length() > 0) {
				UTCommon.OMNITURE_SCRIPT_LOCATION = scriptLoc.trim();
			} else {
				System.out
						.println("Omniture set inactive since no script location was specifiid under [WEB_ANALYTICS] for the 'Omniture_scriptLocation' config.");
				UTCommon.OMNITURE_TRACKING_ACTIVE = false;
			}
			UTCommon.OMNITURE_USE_DYNAMIC_PAGE_NAMES = INI.getBoolValue("WEB_ANALYTICS", "Omniture_useDynamicPageNames");
		}

		// Code 1 Address Validation Server
		String code1Server = INI.getValue("CODE_1", "code1ServerURL");
		code1Server = code1Server.trim();
		if (code1Server.length() > 0) {
			// only update if it's changed.
			if (!code1Server.equalsIgnoreCase(UTCommon.CODE_1_WEB_SERVICE_URL)) {
				UTCommon.CODE_1_WEB_SERVICE_URL = code1Server;
			}
		} else {
			CODE_1_WEB_SERVICE_URL = UTCommon.DEFAULT_CODE_1_WEB_SERVICE_URL;
		}

		// Set up the Code-1 HotData ver 4.0 connection properties

		String code1Port = INI.getValue("CODE_1", "code1Port");
		code1Port = code1Port.trim();
		UTCommon.CODE_1_PORT = code1Port;

		String code1Path = INI.getValue("CODE_1", "code1Path");
		code1Path = code1Path.trim();
		UTCommon.CODE_1_PATH = code1Path;

		String code1AccountId = INI.getValue("CODE_1", "code1AccountId");
		code1AccountId = code1AccountId.trim();
		UTCommon.CODE_1_ACCOUNT_ID = code1AccountId;

		String code1AccountPassword = INI.getValue("CODE_1", "code1AccountPassword");
		code1AccountPassword = code1AccountPassword.trim();
		UTCommon.CODE_1_ACCOUNT_PASSWORD = code1AccountPassword;

		String code1ConnectionType = INI.getValue("CODE_1", "code1ConnectionType");
		code1ConnectionType = code1ConnectionType.trim();
		UTCommon.CODE_1_CONNECTION_TYPE = code1ConnectionType;

		String code1ConnectionTimeout = INI.getValue("CODE_1", "code1ConnectionTimeout");
		code1ConnectionTimeout = code1ConnectionTimeout.trim();
		UTCommon.CODE_1_CONNECTION_TIMEOUT = code1ConnectionTimeout;

		// Set up the Code-1 HotData ver 4.0 option properties
		String code1OutRecType = INI.getValue("CODE_1", "code1OutRecType");
		code1OutRecType = code1OutRecType.trim();
		UTCommon.CODE_1_OUT_REC_TYPE = code1OutRecType;

		String code1OutFormat = INI.getValue("CODE_1", "code1OutFormat");
		code1OutFormat = code1OutFormat.trim();
		UTCommon.CODE_1_OUT_FORMAT = code1OutFormat;

		String code1OutCase = INI.getValue("CODE_1", "code1OutCase");
		code1OutCase = code1OutCase.trim();
		UTCommon.CODE_1_OUT_CASE = code1OutCase;

		String code1OutCodeSep = INI.getValue("CODE_1", "code1OutCodeSep");
		code1OutCodeSep = code1OutCodeSep.trim();
		UTCommon.CODE_1_OUT_CODE_SEP = code1OutCodeSep;

		String code1MultiNatChar = INI.getValue("CODE_1", "code1MultiNatChar");
		code1MultiNatChar = code1MultiNatChar.trim();
		UTCommon.CODE_1_MULTI_NAT_CHAR = code1MultiNatChar;

		String code1OutCountry = INI.getValue("CODE_1", "code1OutCountry");
		code1OutCountry = code1OutCountry.trim();
		UTCommon.CODE_1_OUT_COUNTRY = code1OutCountry;

		String code1OutRetCodes = INI.getValue("CODE_1", "code1OutRetCodes");
		code1OutRetCodes = code1OutRetCodes.trim();
		UTCommon.CODE_1_OUT_RET_CODES = code1OutRetCodes;

		String code1DPV = INI.getValue("CODE_1", "code1DPV");
		code1DPV = code1DPV.trim();
		UTCommon.CODE_1_DPV = code1DPV;

		String code1RDI = INI.getValue("CODE_1", "code1RDI");
		code1RDI = code1RDI.trim();
		UTCommon.CODE_1_RDI = code1RDI;

		String code1ESM = INI.getValue("CODE_1", "code1ESM");
		code1ESM = code1ESM.trim();
		UTCommon.CODE_1_ESM = code1ESM;

		String code1StreetStr = INI.getValue("CODE_1", "code1StreetStr");
		code1StreetStr = code1StreetStr.trim();
		UTCommon.CODE_1_STREET_STR = code1StreetStr;

		String code1FirmStr = INI.getValue("CODE_1", "code1FirmStr");
		code1FirmStr = code1FirmStr.trim();
		UTCommon.CODE_1_FIRM_STR = code1FirmStr;

		String code1DirStr = INI.getValue("CODE_1", "code1DirStr");
		code1DirStr = code1DirStr.trim();
		UTCommon.CODE_1_DIR_STR = code1DirStr;

		String code1DualAddr = INI.getValue("CODE_1", "code1DualAddr");
		code1DualAddr = code1DualAddr.trim();
		UTCommon.CODE_1_DUAL_ADDR = code1DualAddr;

		String code1OutShortCity = INI.getValue("CODE_1", "code1OutShortCity");
		code1OutShortCity = code1OutShortCity.trim();
		UTCommon.CODE_1_OUT_SHORT_CITY = code1OutShortCity;

		UTCommon.VALIDATE_CC = false;
		// Check if application should validate CCs and if so get the path to PTI.INI
		String validateCC = INI.getValue("PTI", "validateCC");
		if (validateCC.equals("true")) {
			UTCommon.VALIDATE_CC = true;
			String ptiInitLoc = INI.getValue("PTI", "PTIinitloc");
			UTCommon.DEFAULT_PTI_PATH = ptiInitLoc;
			UTCommon.DEFAULT_PTI_UT_CLIENT_ID = INI.getValue("PTI", "UTClientID");
			UTCommon.DEFAULT_PTI_SW_CLIENT_ID = INI.getValue("PTI", "SWClientID");
			UTCommon.DEFAULT_PTI_URL = INI.getValue("PTI", "PTI_SERVER_URL");
			UTCommon.DEFAULT_PTI_ISSUER_ID = INI.getValue("PTI", "PTI_Issuer_ID");
			UTCommon.DEFAULT_PTI_CREDENTIAL_VALUE = INI.getValue("PTI", "PTI_Credential_Value");
			UTCommon.DEFAULT_PTI_CREDENTIAL_USER_NAME = INI.getValue("PTI", "PTI_Credential_User_Name");
			UTCommon.DEFAULT_PTI_EXTERNAL_ISSUER_ID = INI.getValue("PTI", "PTI_External_Issuer_ID");
			UTCommon.DEFAULT_PTI_EXTERNAL_MERCHANT_ID = INI.getValue("PTI", "PTI_External_Merchant_ID");
			UTCommon.DEFAULT_PTI_TERMINAL_ID = INI.getValue("PTI", "PTI_Terminal_ID");

			// Configuration.initialize(ptiInitLoc);
		}
		// for products other than SW/UT
		UTCommon.DEFAULT_PTI_LOCATION_CLIENT_ID = INI.getValue("PTI", "ProductClientID");

		// Get email related addresses
		UTCommon.EMAIL_REMOVAL_ADDRESS = INI.getValue("EMAILS", "Removal_Address");

		// Nexternal Integration Config
		String tempStr = INI.getValue("NEXTERNAL_INTEGRATION", "integrationURL");
		if (tempStr != null && tempStr.trim().length() > 0) {
			UTCommon.NEXTERNAL_INTEGRATION_URL = tempStr.trim();
		}

		tempStr = INI.getValue("NEXTERNAL_INTEGRATION", "replyToURL");
		if (tempStr != null && tempStr.trim().length() > 0) {
			UTCommon.NEXTERNAL_REPLYTO_URL = tempStr.trim();
		}

		// assign values
		SERVER_URL = server;
		BASE_URL = base;
		SERVLET_URL = servlet;

		DEFAULT_RENEWAL_TRACKING_CODE = default_RTC;
		DEFAULT_BWRATECODE = bwratecode;
		DEFAULT_UTRATECODE = utratecode;

		JDBC_DSN_NAME = jdbcdsnname;
		JDBC_DRIVER = jdbcdriver;
		if (jndiDataSource != null && jndiDataSource.trim().length() > 0) {
			JNDI_DATA_SOURCE_NAME = jndiDataSource.trim();
		}

		SMTP_SERVER = smtpserver;
		UTFROM_EMAIL = utfrommail;
		BWFROM_EMAIL = bwfrommail;
		EMAIL_SUBJECT = emailsubject;
		FEEDBACK_SUBJECT = feedbacksubject;
		FEEDBACK_EMAIL = feedbackemail;
		BLINDCC_EMAIL = blindccemail;
		DEVCC_EMAIL = devccemail;

		CID_NUMBER = cidnumber;
		CID_PROMO_1 = cidpromo1;
		CID_PROMO_2 = cidpromo2;
		COMM_AMOUNT = cidamount;

		// update the last time the system config was updated
		UTCommon.setLastConfigTime(System.currentTimeMillis());

		return result;
	}

	/**
	 * Retrieves the input parameter from the HttpServletRequest and converts it to an int.
	 * 
	 * @return int
	 * @param req
	 *            javax.servlet.http.HttpServletRequest
	 * @param parm
	 *            java.lang.String
	 * @exception java.lang.NumberFormatException
	 *                Throws NumberFormatException whenever the String parameter value cannot be converted to an Integer
	 */
	public static int getParameterAsInt(HttpServletRequest req, String parm) throws NumberFormatException {
		String value = req.getParameter(parm);
		int result = (new Integer(value)).intValue();
		return result;
	}

	/**
	 * Retrieves the input parameter from the HttpServletRequest and converts it to an int.
	 * 
	 * @return long
	 * @param req
	 *            javax.servlet.http.HttpServletRequest
	 * @param parm
	 *            java.lang.String
	 * @exception java.lang.NumberFormatException
	 *                Throws NumberFormatException whenever the String parameter value cannot be converted to an Integer
	 */
	public static long getParameterAsLong(HttpServletRequest req, String parm) throws NumberFormatException {
		String value = req.getParameter(parm);
		long result = (new Long(value)).longValue();
		return result;
	}

	/**
	 * Retrieves the string parameter from the HttpServletRequest and returns a null whenever the field value is empty (instead of
	 * an empty string)
	 * 
	 * @return java.lang.String
	 * @param req
	 *            javax.servlet.http.HttpServletRequest
	 * @param parm
	 *            java.lang.String
	 * @exception java.lang.IllegalArgumentException
	 *                Throws IllegalArgumentException whenever an error occurs retrieving a string parameter
	 */
	public static String getParameterAsString(HttpServletRequest req, String parm) throws IllegalArgumentException {
		String value = null;

		try {
			value = req.getParameter(parm);
			value = value.trim();

			if (value.length() == 0) {
				value = null;
			}
		} catch (IllegalArgumentException ie) {
			value = null;
			throw ie;
		} catch (Exception e) {
			value = null;
		}

		return value;
	}

	/**
	 * Retrieves the input parameter from the HttpServletRequest and converts it to an int.
	 * 
	 * @return int
	 * @param req
	 *            javax.servlet.http.HttpServletRequest
	 * @exception java.lang.NumberFormatException
	 *                Throws NumberFormatException whenever the String parameter value cannot be converted to an Integer
	 */
	public static int getQueryStringAsInt(HttpServletRequest req) throws NumberFormatException {
		String value = req.getQueryString();
		int result = (new Integer(value)).intValue();
		return result;
	}

	/**
	 * Returns a boolean true if a set of radio buttons parameter value matches the given option
	 * 
	 * @return boolean
	 * @param req
	 *            javax.servlet.http.HttpServletRequest
	 * @param parm
	 *            java.lang.String
	 * @param option
	 *            java.lang.String
	 */
	public static boolean getRadiobuttonParameterAsBoolean(HttpServletRequest req, String parm, String option) {
		try {
			String value = req.getParameter(parm);
			if (value.equals(option)) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Returns the current session's data object
	 * 
	 * @return com.usatoday.esub.UTSubscription
	 * @param session
	 *            javax.servlet.http.HttpSession
	 * @exception java.lang.NullPointerException
	 *                Throws a NullPointerException when no session objet is found.
	 * @exception java.io.IOException
	 *                Throws IOException from calling showError().
	 */
	public static UTFirstTime getFirstTimeSessionDefaultDataObject(HttpSession session) throws NullPointerException, IOException {
		UTFirstTime data = null;

		try {
			data = (UTFirstTime) session.getAttribute(UTCommon.SESSION_FIRST_TIME);
			// Error if no data object is present
			if (data == null) {
				throw new NullPointerException();
			}
		} catch (Exception e) {
			throw new NullPointerException();
		}

		return data;
	}

	/**
	 * Returns the current session's data object
	 * 
	 * @return com.usatoday.esub.UTSubscription
	 * @param request
	 *            javax.servlet.http.HttpServletRequest
	 * @exception java.lang.NullPointerException
	 *                Throws a NullPointerException when no session objet is found.
	 * @exception java.io.IOException
	 *                Throws IOException from calling showError().
	 */
	public static UTFirstTime getFirstTimeSessionDefaultDataObject(HttpServletRequest request) throws NullPointerException,
			IOException {
		HttpSession session = request.getSession(false);
		if (session == null) {
			throw new NullPointerException();
		} else {
			return getFirstTimeSessionDefaultDataObject(session);
		}
	}

	/**
	 * Insert the method's description here. Creation date: (2/20/2003 11:36:12 AM)
	 * 
	 * @return java.lang.String
	 */
	public static String getSystemDateYYYYMMDD() {

		String systemdate = "";

		try {
			SimpleDateFormat updatedate = new SimpleDateFormat("yyyyMMdd");
			Date sysdate = new Date();
			systemdate = updatedate.format(sysdate);
		} catch (Exception e) {
			System.out.println("Error getting date: " + e.getMessage());
			e.printStackTrace();
			systemdate = "ERR";
		}

		return systemdate;
	}

	// Return system date in the desired format

	public static String getSystemDate(String format) {

		SimpleDateFormat updatedate = new SimpleDateFormat(format);
		Date sysdate = new Date();
		String systemdate = updatedate.format(sysdate);

		return systemdate;
	}

	/**
	 * Insert the method's description here. Creation time: (2/20/2003 11:36:12 AM)
	 * 
	 * @return java.lang.String
	 */
	public static String getSystemTimeHHMMSS() {

		SimpleDateFormat updatetime = new SimpleDateFormat("hhmmss");
		Date systime = new Date();
		String systemtime = updatetime.format(systime);

		return systemtime;
	}

	/**
	 * This method generates a Visual Sciences bug for the purpose of tracking users throught the extranet site.
	 * 
	 * Sample Usage: StingBuffer visualSciencesBug = UTCommon.getVisualSciencesBug();
	 * 
	 * Creation date: (1/7/2003 10:41:56 AM)
	 * 
	 * @return com.usatoday.esub.common.WebBug
	 */
	public static WebBug getWebAnalyticsBug() {
		return new WebBug(UTCommon.OMNITURE_TRACKING_ACTIVE, UTCommon.OMNITURE_SCRIPT_LOCATION,
				UTCommon.OMNITURE_USE_DYNAMIC_PAGE_NAMES, UTCommon.BASE_URL);
	}

	public static void sendEmail(HttpSession session1, String toEmail, String subject, String message) throws Exception {
		if ((toEmail == null) || (subject == null) || (message == null)) {
			return;
		}

		SmtpMailSender mailSender = new SmtpMailSender();
		mailSender.setMailServerHost(SMTP_SERVER);
		mailSender.setMessageSubject(subject);
		mailSender.setMessageText(message);
		String publication = (String) session1.getAttribute(UTCommon.SESSION_PUB_NAME);
		try {
			SubscriptionProductIntf prod = SubscriptionProductBO.getSubscriptionProduct(publication);
			if (prod != null) {
				publication = prod.getBrandingPubCode();
			}
		} catch (Exception e) {
			;
		}
		if (publication == null || publication.equals(UTCommon.UTPUBCODE)) {
			mailSender.setSender(UTFROM_EMAIL);
		} else {
			mailSender.setSender(BWFROM_EMAIL);
		}
		mailSender.addTORecipient(toEmail);
		mailSender.sendMessage();
		return;
	}

	public static void sendFeedbackEmail(String fromEmail, String toEmail, String subject, String message) throws Exception {
		if ((toEmail == null) || (subject == null) || (message == null)) {
			return;
		}

		SmtpMailSender mailSender = new SmtpMailSender();
		mailSender.setMailServerHost(SMTP_SERVER);
		mailSender.setMessageSubject(subject);
		mailSender.setMessageText(message);
		mailSender.setSender(fromEmail);
		mailSender.addTORecipient(toEmail);
		mailSender.sendMessage();

		return;
	}

	public static void setResponseHeader(HttpServletResponse res) throws ServletException {
		setResponseHeader(res, "text/html");
	}

	public static void setResponseHeader(HttpServletResponse res, String mimeType) throws ServletException {
		res.setContentType(mimeType);
		res.setHeader("Pragma", "no-cache");
		res.setHeader("Cache-Control", "no-cache");
		res.setDateHeader("Expires", 0);
	}

	/**
	 * Show the error message
	 * 
	 * @param res
	 *            javax.servlet.http.HttpServletResponse
	 * @exception java.lang.IOException
	 *                Throws an IOException from the call to sendRedirectUrl(String)
	 */
	public static void showError(HttpServletRequest req, HttpServletResponse res, int number, String message) throws IOException {
		showError(req, res, number, message, null);
	}

	/**
	 * Show the error message
	 * 
	 * @param res
	 *            javax.servlet.http.HttpServletResponse
	 * @exception java.lang.IOException
	 *                Throws an IOException from the call to sendRedirectUrl(String)
	 */
	public static void showError(HttpServletRequest req, HttpServletResponse res, int number, String message, Exception e)
			throws IOException {
		HttpSession session = null;
		UTError error = null;

		// get the old error data object (or create a new one) and then set the error data
		try {
			session = req.getSession(false);
			error = (UTError) session.getAttribute(UTCommon.SESSION_FIELD_ERROR);
			if (error == null) {
				error = new UTError();
			}
		} catch (Exception ex) {
			error = new UTError();
		} finally {
			error.setNumber(number);
			error.setText(message);
			error.setException(e);
		}

		// save the error object in the session
		try {
			session.setAttribute(UTCommon.SESSION_FIELD_ERROR, error);
		} catch (Exception ex) {
			// no session to save the data in!
		}

		// show the error message
		showUrl(res, UTCommon.ERROR_URL);
	}

	/**
	 * Show the error message
	 * 
	 * @param res
	 *            javax.servlet.http.HttpServletResponse
	 * @exception java.lang.IOException
	 *                Throws an IOException from the call to sendRedirectUrl(String)
	 */
	public static void showError(HttpServletRequest req, HttpServletResponse res, String message) throws IOException {
		showError(req, res, 0, message);
	}

	/**
	 * Show the specified URL
	 * 
	 * @param res
	 *            javax.servlet.http.HttpServletResponse
	 * @param url
	 *            java.lang.String
	 * @exception java.lang.IOException
	 *                Throws an IOException from the call to sendRedirectUrl(String)
	 */
	public static void showUrl(HttpServletResponse res, String url) throws IOException {
		try {
			res.sendRedirect(res.encodeRedirectURL(url));
		} catch (Exception e) {
			String resStat = "";
			if (res == null) {
				resStat = "IS NULL";
			} else {
				resStat = "IS VALID";
				if (res.isCommitted()) {
					resStat += " Already Committed Response";
				}
			}
			System.out.println("Exception UTCommon.showUrl() : response object " + resStat + "   Redirect URL: " + url
					+ "  StackTrace Follows;");
			System.out.println("Message: " + e.getMessage());
			e.printStackTrace(System.out);
			if (e instanceof IOException) {
				throw (IOException) e;
			}
		}
	}

	public static boolean validateSession(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		javax.servlet.http.HttpSession session = request.getSession(false);

		if (session == null || session.isNew()) {

			if (session == null) {
				System.out.println("UTCommon::validateSession() session is null...attempting redirect to error page. ");
			} else {
				System.out.println("UTCommon::validateSession() session is new...attempting redirect to error page");
			}

			try {
				if (!response.isCommitted()) {
					// get a new session
					session = request.getSession(true);
					UTError error = new UTError(
							"Session Expired.<BR>This condition may be due to inactivity. Please start your transaction again or contact National Customer Service to complete your transaction.");
					session.setAttribute(UTCommon.SESSION_FIELD_ERROR, error);
					String urlString = "/welcome.jsp";
					response.sendRedirect(urlString);
				}
			} catch (java.lang.IllegalStateException ise) {
				System.out.println("UTCommon.validateSession(): " + ise.getMessage());
				ise.printStackTrace(System.out);
			}
			return false;
		}

		return true;
	}

	/**
	 * @return
	 */
	public static String getLastConfigTime() {

		String responseStr = "";
		try {
			SimpleDateFormat updatetime = new SimpleDateFormat("EEE, MMMMMMM d, yyyy 'at' hh:mm:ss a zzz");
			Date systime = new Date(lastConfigTime);

			responseStr = updatetime.format(systime);
		} catch (Exception e) {
			responseStr = "Error formatting last config update time: " + e.getMessage();
		}
		return responseStr;
	}

	/**
	 * @param l
	 */
	public static void setLastConfigTime(long l) {
		lastConfigTime = l;
	}

	/**
	 * 
	 * Creation date: (8/20/00 10:57:26 PM)
	 * 
	 * @return java.lang.String
	 * @param calendar
	 *            java.util.Calendar
	 * @param format
	 *            java.lang.String
	 */
	// Convert input string input correct customer name format
	public static String formatNameCapitalization(String name) {
		String customerName = name;
		String firstChar = "";

		if (customerName != null) {
			customerName = customerName.trim().toLowerCase();
			firstChar = customerName.substring(0, 1).toUpperCase();
			customerName = firstChar + customerName.substring(1);
		}
		return customerName;
	}

	/**
	 * This method is used to retrive the offer associated with a user's session. It should be the sole method of determining the
	 * current pub and keycode for a user's session.
	 * 
	 * @param request
	 *            - The web request
	 * @return The offer tied to the session or the request parameters
	 * 
	 */
	public static OfferIntf getCurrentOffer(HttpServletRequest request) {

		return UTCommon.getCurrentOfferVersion2(request);
	}

	/**
	 * This version is added to support a one page order form capable of supporting any product.
	 * 
	 * @param request
	 * @return
	 */
	public static SubscriptionOfferIntf getCurrentOfferVersion2(HttpServletRequest request) {
		SubscriptionOfferIntf offer = null;
		HttpSession session = request.getSession();

		boolean pubChanged = false;
		// attempt to get an override from the request
		String pubCode = request.getParameter("pub");
		@SuppressWarnings("unused")
		String brandingPub = null;
		SubscriptionProductIntf product = null;
		String keyCode = null;
		if (pubCode != null) {
			pubCode = pubCode.trim().toUpperCase();
			try {
				if (pubCode.length() > 2) {
					pubCode = pubCode.substring(0, 2);
					if (!Character.isLetter(pubCode.charAt(0)) || !Character.isLetter(pubCode.charAt(1))) {
						pubCode = null;
						throw new UsatException();
					}
				}
				product = SubscriptionProductBO.getSubscriptionProduct(pubCode);
				brandingPub = product.getBrandingPubCode();
				keyCode = product.getDefaultKeycode();
			} catch (UsatException e) {
				;
			}
		}

		boolean keyCodeInRequest = false;
		if (request.getParameter("keycode") != null && !request.getParameter("keycode").trim().equals("")) {
			keyCode = request.getParameter("keycode");
			if (keyCode != null) {
				keyCode = keyCode.trim().toUpperCase();
				try {
					if (keyCode.length() > 5) {
						keyCode = keyCode.substring(0, 5);
						for (char aChar : keyCode.toCharArray()) {
							if (!Character.isLetter(aChar)) {
								keyCode = null;
								throw new UsatException();
							}
						}
					}
					keyCodeInRequest = true;
				} catch (UsatException e) {
					;
				}
			} else {
				// check for keycode in session - andy added since override offer not working suddenly
				// keyCode = (String)session.getAttribute(UTCommon.SESSION_KEYCODE);
			}
		}

		// if pub not overridden on URL by valid pub try to get from session.
		if (pubCode == null || product == null) {
			pubCode = (String) session.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_PUB_NAME);

			try {
				if (pubCode != null) {
					product = SubscriptionProductBO.getSubscriptionProduct(pubCode);
					brandingPub = product.getBrandingPubCode();
				}
			} catch (UsatException e) {
				;
			}
		} else {

			String oldPub = (String) session.getAttribute(UTCommon.SESSION_PUB_NAME);
			if (oldPub != null && !pubCode.equalsIgnoreCase(oldPub)) {
				// pubCode is changing so clear any old keycode session object and log out user
				pubChanged = true;

				// remove any logged in user session data
				session.removeAttribute(UTCommon.SESSION_CUSTOMER_INFO);

				String oldKeyCode = (String) session.getAttribute(UTCommon.SESSION_KEYCODE);
				session.removeAttribute(UTCommon.SESSION_KEYCODE);

				if (oldKeyCode != null) { // there was an original keycode but not renewal keycode
					if (oldPub.equalsIgnoreCase(UsaTodayConstants.UT_PUBCODE)
							|| oldPub.equalsIgnoreCase(UsaTodayConstants.DEFAULT_UT_EE_PUBCODE)) {
						// store UT original keycode
						session.removeAttribute("UT_ORIG_KEYCODE");
						session.setAttribute("UT_ORIG_KEYCODE", oldKeyCode);
					} else {
						// SW
						session.removeAttribute("SW_ORIG_KEYCODE");
						session.setAttribute("SW_ORIG_KEYCODE", oldKeyCode);
					}
				}
			}

			// set the new pub
			session.removeAttribute(UTCommon.SESSION_PUB_NAME);
			session.setAttribute(UTCommon.SESSION_PUB_NAME, pubCode);
		}

		// otherwise get default of UT
		if (pubCode == null) {
			pubCode = UsaTodayConstants.UT_PUBCODE;
			session.setAttribute(UTCommon.SESSION_PUB_NAME, pubCode);
		}

		// Check for original keycode for pub
		String origKeyCode = null;
		if (pubChanged && pubCode.equalsIgnoreCase(UsaTodayConstants.SW_PUBCODE)) {
			origKeyCode = (String) session.getAttribute("SW_ORIG_KEYCODE");
		} else {
			if (pubChanged) {
				origKeyCode = (String) session.getAttribute("UT_ORIG_KEYCODE");
			}
		}

		if (origKeyCode != null && keyCode == null) { // return to original keycode if not overridden by request parameter
			keyCode = origKeyCode;
		}

		// if keycode not in request then get from session.
		if (keyCode == null) {
			keyCode = (String) session.getAttribute(UTCommon.SESSION_KEYCODE);
		} else { // otherwise replace keycode
			session.removeAttribute(UTCommon.SESSION_KEYCODE);
			session.setAttribute(UTCommon.SESSION_KEYCODE, keyCode);
		}

		// if keycode not in request then get from product.
		if (keyCode == null) {
			try {
				if (product == null) {
					product = SubscriptionProductBO.getSubscriptionProduct(pubCode);
				}
				keyCode = product.getDefaultKeycode();
				session.setAttribute(UTCommon.SESSION_KEYCODE, keyCode);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		try {
			offer = SubscriptionOfferManager.getInstance().getOffer(keyCode, pubCode);
		} catch (Exception e) {
			// System.out.println("UTCOmmon::getCurrentOffer() Pub: " + pubCode + ", Keycode: " + keyCode + "  Exception: " +
			// e.getMessage());
		}

		try {
			if (offer == null) {
				if (keyCodeInRequest) {
					// expired promotion

					String expiredPromoKeycode = product.getExpiredOfferKeycode();

					try {
						offer = SubscriptionOfferManager.getInstance().getOffer(expiredPromoKeycode, pubCode);
					} catch (Exception eee) {
						// last chance go for normal default
						offer = SubscriptionOfferManager.getInstance().getDefaultOffer(pubCode);
					}
				} else {
					// get default keycode
					offer = SubscriptionOfferManager.getInstance().getDefaultOffer(pubCode);
				}
				if (offer != null) {
					session.removeAttribute(UTCommon.SESSION_KEYCODE);
					session.setAttribute(UTCommon.SESSION_KEYCODE, ((SubscriptionOfferIntf) offer).getKeyCode());
				}
			}

		} catch (UsatException e) {
			System.out.println("UTCOmmon::getDefaultOffer() Pub: " + pubCode + ", Keycode: " + keyCode + "  Exception: "
					+ e.getMessage());
		}
		// session.removeAttribute(UTCommon.SESSION_URL_RATECODE);
		// If there is rate code in the URL and if it exists in the offer, save it to be used on the order page
		if (request.getParameter("ratecode") != null && !request.getParameter("ratecode").trim().equals("")) {
			String rateCode = request.getParameter("ratecode");
			if (rateCode != null) {
				rateCode = rateCode.trim().toUpperCase();
				try {
					session.setAttribute(UTCommon.SESSION_URL_RATECODE, rateCode);
				} catch (Exception e) {
					System.out.println("UTCommon: URL Rate Code could not be saved into the session object.");
				}
			}
		}

		/*
		 * // If there is rate code in the URL and if it exists in the offer, use it and remove others if
		 * (request.getParameter("ratecode") != null && !request.getParameter("ratecode").trim().equals("")) { String rateCode =
		 * request.getParameter("ratecode"); if (rateCode != null) { rateCode = rateCode.trim().toUpperCase(); try {
		 * Collection<SubscriptionTermsIntf> ratesTerms = offer.getTerms(); for (SubscriptionTermsIntf termsBean : ratesTerms) { if
		 * (termsBean.getPiaRateCode() != null && !termsBean.getPiaRateCode().trim().equals("") &&
		 * termsBean.getPiaRateCode().trim().equals(rateCode)) { SubscriptionOfferBO offerTemp = (SubscriptionOfferBO) offer;
		 * ratesTerms.clear(); ratesTerms.add(termsBean); offerTemp.setTerms(ratesTerms); offer = offerTemp; break; } }
		 * 
		 * } catch (Exception e) {
		 * 
		 * } } }
		 */
		return offer;
	}

	/**
	 * 
	 * @param request
	 * @param newOffer
	 * @throws UsatException
	 */
	public static void overrideCurrentOffer(HttpServletRequest request, SubscriptionOfferIntf newOffer) throws UsatException {
		try {
			HttpSession session = request.getSession();

			if (newOffer == null) {
				throw new UsatException("New offer is not valid.");
			}

			// update session keycode
			session.removeAttribute(UTCommon.SESSION_KEYCODE);
			session.setAttribute(UTCommon.SESSION_KEYCODE, newOffer.getKeyCode());

			// update session pubcode
			session.removeAttribute(UTCommon.SESSION_PUB_NAME);
			session.setAttribute(UTCommon.SESSION_PUB_NAME, newOffer.getPubCode());

		} catch (UsatException ue) {
			throw ue;
		} catch (Exception e) {
			throw new UsatException(e);
		}
	}

	/**
	 * 
	 * @param pubCode
	 * @return
	 */
	public static PromotionSet getDefaultPromotionSetForPub(String pubCode) {
		PromotionSet pSet = null;

		try {
			if (pubCode == null || UsaTodayConstants.UT_PUBCODE.equalsIgnoreCase(pubCode)) {
				pSet = PromotionManager.getInstance().getDefaultPromotionSetForUSAT();
			} else {
				pSet = PromotionManager.getInstance().getDefaultPromotionSetForSW();
			}
		} catch (Exception e) {
			System.out.println("Failed to get the default promotion set. " + e.getMessage());
		}

		return pSet;
	}

	/**
	 * 
	 * @param request
	 * @return
	 */
	public static TrialCustomerIntf authenticateTrialCustomerViaRememberMeCookie(HttpServletRequest request) {
		// skip the select account page
		if (request.getRequestURI().indexOf("/select_accnt.jsp") > -1) {
			return null;
		}

		TrialCustomerIntf customer = null;

		Cookie authCookie = null;

		// check for new Subscription cookie if long term remember me is not set
		AuthenticationCookieProcessor cookieProcessor = new AuthenticationCookieProcessor();

		Cookie[] cookies = request.getCookies();

		if (cookies != null) {
			authCookie = cookieProcessor.returnTrialStartCookie(cookies);
		}

		TrialCustomerHandler tch = null;
		tch = (TrialCustomerHandler) request.getSession().getAttribute("trialCustomerHandler");
		if (tch == null) {
			tch = new TrialCustomerHandler();
			request.getSession().setAttribute("trialCustomerHandler", tch);
		}
		if (authCookie != null) {
			// retrieve user based on cookie id

			String identifier = authCookie.getValue();

			try {
				customer = TrialCustomerBO.getTrialCustomerByEmail(identifier);

				if (customer != null) {
					tch.setTrialCustomer(customer);
				}
			} catch (Exception e) {
				customer = null;
			}

		}

		return customer;
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	public static CustomerIntf authenticateViaRememberMeCookie(HttpServletRequest request, HttpServletResponse response) {

		// skip the select account page
		if (request.getRequestURI().indexOf("/select_accnt.jsp") > -1) {
			return null;
		}

		CustomerIntf customer = null;

		Cookie authCookie = null;
		Cookie authCookie1 = null;
		Cookie authCookie2 = null;

		// check for new Subscription cookie if long term remember me is not set
		AuthenticationCookieProcessor cookieProcessor = new AuthenticationCookieProcessor();

		Cookie[] cookies = request.getCookies();

		if (cookies != null) {
			authCookie = cookieProcessor.returnRememberMeCookie(cookies);
			authCookie1 = cookieProcessor.returnAutoLoginCookie(cookies);
			authCookie2 = cookieProcessor.returnSessionKeyCookie(cookies);
		}

		CustomerHandler ch = null;
		ch = (CustomerHandler) request.getSession().getAttribute("customerHandler");
		if (ch == null) {
			ch = new CustomerHandler();
			request.getSession().setAttribute("customerHandler", ch);
		}
		if (authCookie != null) {
			// retrieve user based on cookie id

			String identifier = authCookie.getValue();
			String identifier1 = authCookie1.getValue();

			try {
				// First check auto login
				boolean result = CustomerBO.loginCustomerByAutoLogin(identifier1);
				if (result) {
					customer = CustomerBO.loginCustomerByEmailAddress(identifier);

					if (customer != null) {

						// for backward compatibility with rest of site
						request.getSession().removeAttribute(UTCommon.SESSION_CUSTOMER_INFO);
						request.getSession().setAttribute(UTCommon.SESSION_CUSTOMER_INFO, customer);

						SubscriptionProductIntf product = SubscriptionProductBO.getSubscriptionProduct(customer
								.getCurrentEmailRecord().getPubCode());

						// only place pub in session if not in new subscription path. (case of a logged in user placing new sub)
						String pathStr = request.getRequestURL().toString();
						pathStr = pathStr.toLowerCase();
						if (pathStr.indexOf("/subscriptions/") < 0) {
							// if not in new subscription path
							request.getSession().removeAttribute(UTCommon.SESSION_PUB_NAME);
							request.getSession().setAttribute(UTCommon.SESSION_PUB_NAME, product.getBrandingPubCode());
						}

						ch.setCustomer(customer);
						ch.setAuthenticated(true);
						ch.setCookieAuthenticated(true);

						authCookie = cookieProcessor.extendExpiration(authCookie);
						response.addCookie(authCookie);
						authCookie1 = cookieProcessor.extendExpiration(authCookie1);
						response.addCookie(authCookie1);
					}
				}
			} catch (Exception e) {
				// usually we get here if the account expired.
				// System.out.println("UTCommon::authenticateViaRemembeMeCookie() : " + e.getMessage());
				// remove the cookie
				if (authCookie != null) {
					authCookie = cookieProcessor.setCookieForDeletion(authCookie);
					response.addCookie(authCookie);
				} 				
				if (authCookie1 != null) {
					authCookie1 = cookieProcessor.setCookieForDeletion(authCookie1);
					response.addCookie(authCookie1);
				}
				if (authCookie2 != null) {
					authCookie2 = cookieProcessor.setCookieForDeletion(authCookie2);
					response.addCookie(authCookie2);
				}

				customer = null;
			}
		}
		return customer;
	}

}
