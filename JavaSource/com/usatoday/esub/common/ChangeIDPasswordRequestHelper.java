/*
 * Created on May 25, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.usatoday.esub.common;

/**
 * @author aeast
 * 
 *         To change the template for this generated type comment go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and
 *         Comments
 */
public class ChangeIDPasswordRequestHelper extends USATRequestHelper {
	private java.lang.String currLogin = null;
	private java.lang.String newLogin = null;
	private java.lang.String currPassword = null;
	private java.lang.String newPassword = null;

	/**
	 * @return
	 */
	public java.lang.String getCurrLogin() {
		return currLogin;
	}

	/**
	 * @return
	 */
	public java.lang.String getCurrPassword() {
		return currPassword;
	}

	/**
	 * @return
	 */
	public java.lang.String getNewLogin() {
		return newLogin;
	}

	/**
	 * @return
	 */
	public java.lang.String getNewPassword() {
		return newPassword;
	}

	/**
	 * @param string
	 */
	public void setCurrLogin(java.lang.String string) {
		currLogin = string;
	}

	/**
	 * @param string
	 */
	public void setCurrPassword(java.lang.String string) {
		currPassword = string;
	}

	/**
	 * @param string
	 */
	public void setNewLogin(java.lang.String string) {
		newLogin = string;
	}

	/**
	 * @param string
	 */
	public void setNewPassword(java.lang.String string) {
		newPassword = string;
	}

	public String getEmailText() {
		String emailPassword = this.newPassword;

		if ((this.newPassword == null) || (this.newPassword.equals(""))) {
			emailPassword = this.currPassword;
		}

		StringBuffer text = new StringBuffer("\n\nThank you for informing ");
		text.append(this.getPublicationName());
		text.append(" of your subscription needs.  Our records will indicate your email address and password to be as follows: \n\nEmail Address: ");
		text.append(this.newLogin);
		text.append("\nPassword: ").append(emailPassword);
		text.append("\n\nIf you have any questions about your subscription, please call us at ");
		text.append(this.getPhoneNumber()).append(".");

		return text.toString();
	}

}
