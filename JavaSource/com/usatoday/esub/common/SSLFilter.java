package com.usatoday.esub.common;

import java.io.IOException;
import java.net.URL;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SSLFilter implements Filter {
	/*
	 * (non-Java-doc)
	 * 
	 * @see java.lang.Object#Object()
	 * 
	 * THIS FILTER'S REDIRECTION ONLY WORKS WHEN THE DEFAULT PORTS ARE USED. If you are on a non-standard SSL port, you should first
	 * manually enter the site via SSL so that this filter does not result in a page not found error.
	 */
	public SSLFilter() {
		super();
	}

	/*
	 * (non-Java-doc)
	 * 
	 * @see javax.servlet.Filter#init(FilterConfig arg0)
	 */
	public void init(FilterConfig arg0) throws ServletException {
	}

	/*
	 * (non-Java-doc)
	 * 
	 * @see javax.servlet.Filter#doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain arg2)
	 */
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {

		// System.out.println("in ssl filter");
		// if already secure ignore
		if (req.isSecure()) {
			chain.doFilter(req, resp);
			return;
		}

		if (req instanceof HttpServletRequest && resp instanceof HttpServletResponse) {
			HttpServletRequest request = (HttpServletRequest) req;

			request.getSession();

			// System.out.println("SSLFilter - Session iD: " + session.getId());
			String reqURL = request.getRequestURL().toString();
			URL oldURL = new URL(reqURL);
			// only perform redirections if default ports used or not set (default).
			if (oldURL.getPort() < 0 || oldURL.getPort() == 80) {
				URL newURL = new URL("https", oldURL.getHost(), oldURL.getFile());

				// redirect to SSL
				HttpServletResponse response = (HttpServletResponse) resp;

				String strURL = newURL.toExternalForm();

				if (request.getQueryString() != null && request.getQueryString().length() > 0) {
					// append query string
					strURL = strURL + "?" + request.getQueryString();
				}

				response.sendRedirect(response.encodeRedirectURL(strURL));
				return;
			} else {
				System.out.println("Normally would have redirected to SSL but don't know SSL port: " + reqURL);
			}
		}

		chain.doFilter(req, resp);
	}

	/*
	 * (non-Java-doc)
	 * 
	 * @see javax.servlet.Filter#destroy()
	 */
	public void destroy() {

	}

}