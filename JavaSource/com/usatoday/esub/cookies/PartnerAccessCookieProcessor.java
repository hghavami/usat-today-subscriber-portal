package com.usatoday.esub.cookies;

import javax.servlet.http.Cookie;

import com.ibm.icu.util.StringTokenizer;
import com.usatoday.businessObjects.util.crypto.USATBase64Decoder;
import com.usatoday.businessObjects.util.crypto.USATBase64Encoder;

/**
 * cookie processor for partner autheorizations for hotels, etc.. Cookie format pid=[partner
 * id]:propid=[property_id]:pkey=[partner_key]:ad=[arrival_date]:dd=[departure_date]:s=[hash]
 * 
 * @author aeast
 * 
 */
public class PartnerAccessCookieProcessor {
	public static final String USAT_PARNTER_ACCESS_COOKIE_NAME = "USATCircPA";

	public static final String PID = "pid";
	public static final String PROP_ID = "propid";
	public static final String PARTNER_KEY = "pkey";
	public static final String ARRIVAL_DATE = "ad";
	public static final String DEPARTURE_DATE = "dd";
	public static final String HASH = "s";

	private static final int rememberMeExpiry = 2678400; // 31 days

	/**
	 * 
	 * @param emailAddress
	 * @return
	 */
	public Cookie createCookie(String value) {
		Cookie c = null;

		USATBase64Encoder encoder = new USATBase64Encoder();
		if (value == null) {
			value = "";
		}
		String encodedValue = encoder.encode(value.toString().getBytes());

		c = new Cookie(PartnerAccessCookieProcessor.USAT_PARNTER_ACCESS_COOKIE_NAME, encodedValue);
		c.setMaxAge(PartnerAccessCookieProcessor.rememberMeExpiry);
		c.setComment("USAT Partner Aaccess");
		c.setPath("/");
		return c;
	}

	public Cookie createCookie(String partnerID, String propertyID, String externalKey, String aDate, String dDate, String hash) {
		Cookie c = null;

		StringBuilder value = new StringBuilder();
		value.append(PartnerAccessCookieProcessor.PID).append("=").append(partnerID).append(":");
		value.append(PartnerAccessCookieProcessor.PROP_ID).append("=").append(propertyID).append(":");
		value.append(PartnerAccessCookieProcessor.PARTNER_KEY).append("=").append(externalKey).append(":");
		value.append(PartnerAccessCookieProcessor.ARRIVAL_DATE).append("=").append(aDate).append(":");
		value.append(PartnerAccessCookieProcessor.DEPARTURE_DATE).append("=").append(dDate).append(":");
		value.append(PartnerAccessCookieProcessor.HASH).append("=").append(hash);

		c = this.createCookie(value.toString());

		return c;
	}

	/**
	 * 
	 * @param cookies
	 * @return
	 */
	public Cookie returnCookie(Cookie[] cookies) {

		Cookie cookie = null;

		if (cookies != null) {
			for (Cookie c : cookies) {
				if (c.getName().equalsIgnoreCase(PartnerAccessCookieProcessor.USAT_PARNTER_ACCESS_COOKIE_NAME)) {
					cookie = c;
					break;
				}
			}
		}
		return cookie;
	}

	/**
	 * 
	 * @param c
	 * @return
	 */
	public Cookie extendExpiration(Cookie c) {
		if (c != null) {
			c.setMaxAge(PartnerAccessCookieProcessor.rememberMeExpiry);
			c.setPath("/");
		}
		return c;
	}

	public Cookie setCookieForDeletion(Cookie c) {
		if (c != null) {
			c.setMaxAge(0);
		}

		return c;
	}

	private String getValue(Cookie c, String keyPart) {
		String value = null;
		try {
			if (c != null) {
				if (c.getName().equalsIgnoreCase(PartnerAccessCookieProcessor.USAT_PARNTER_ACCESS_COOKIE_NAME)) {
					String cookieVal = c.getValue();

					USATBase64Decoder decoder = new USATBase64Decoder();
					cookieVal = new String(decoder.decodeBuffer(cookieVal));

					StringTokenizer tokenizer = new StringTokenizer(cookieVal, ":");
					boolean found = false;
					while (!found && tokenizer.hasMoreElements()) {
						String token = tokenizer.nextToken();
						if (token.startsWith(keyPart + "=")) {
							int index = token.indexOf("=");
							if (index < token.length()) {
								index++;
							}
							value = token.substring(index);
							found = true;
						}
					}
				}
			}
		} catch (Exception e) {
			value = null;
		}
		return value;
	}

	public String getPartnerID(Cookie c) {
		String value = this.getValue(c, PartnerAccessCookieProcessor.PID);
		return value;
	}

	public String getPropertyID(Cookie c) {
		String value = this.getValue(c, PartnerAccessCookieProcessor.PROP_ID);
		return value;
	}

	public String getExternalKey(Cookie c) {
		String value = this.getValue(c, PartnerAccessCookieProcessor.PARTNER_KEY);
		return value;
	}

	public String getArrivalDate(Cookie c) {
		String value = this.getValue(c, PartnerAccessCookieProcessor.ARRIVAL_DATE);
		return value;
	}

	public String getDepartureDate(Cookie c) {
		String value = this.getValue(c, PartnerAccessCookieProcessor.DEPARTURE_DATE);
		return value;
	}

	public String getHash(Cookie c) {
		String value = this.getValue(c, PartnerAccessCookieProcessor.HASH);
		return value;
	}

}
