package com.usatoday.esub.cookies;

import javax.servlet.http.Cookie;

public class PreferencesCookieProcessor {

	public static final String USAT_PREFERENCES_COOKIE_NAME = "USATCircPref";
	public static final String USAT_PREFERENCES_FLASH = "flash";
	public static final String USAT_PREFERENCES_HTML = "html";

	private static final int rememberMeExpiry = 2678400; // 31 days

	/**
	 * 
	 * @param emailAddress
	 * @return
	 */
	public Cookie createCookie(String value) {
		Cookie c = null;

		c = new Cookie(PreferencesCookieProcessor.USAT_PREFERENCES_COOKIE_NAME, value);
		c.setMaxAge(PreferencesCookieProcessor.rememberMeExpiry);
		c.setComment("USAT Preferences");
		c.setPath("/");
		return c;
	}

	/**
	 * 
	 * @param cookies
	 * @return
	 */
	public Cookie returnCookie(Cookie[] cookies) {

		Cookie cookie = null;

		if (cookies != null) {
			for (Cookie c : cookies) {
				if (c.getName().equalsIgnoreCase(PreferencesCookieProcessor.USAT_PREFERENCES_COOKIE_NAME)) {
					cookie = c;
					break;
				}
			}
		}
		return cookie;
	}

	/**
	 * 
	 * @param c
	 * @return
	 */
	public Cookie extendExpiration(Cookie c) {
		if (c != null) {
			c.setMaxAge(PreferencesCookieProcessor.rememberMeExpiry);
			c.setPath("/");
		}
		return c;
	}

	public Cookie setCookieForDeletion(Cookie c) {
		if (c != null) {
			c.setMaxAge(0);
		}

		return c;
	}

	public boolean prefersFlash(Cookie c) {

		boolean prefersFlash = false;
		if (c != null) {
			if (c.getName().equalsIgnoreCase(PreferencesCookieProcessor.USAT_PREFERENCES_COOKIE_NAME)) {
				if (c.getValue().equalsIgnoreCase(PreferencesCookieProcessor.USAT_PREFERENCES_FLASH)) {
					prefersFlash = true;
				}
			}
		}
		return prefersFlash;
	}

	public boolean prefersHTML(Cookie c) {

		boolean prefersHTML = false;
		if (c != null) {
			if (c.getName().equalsIgnoreCase(PreferencesCookieProcessor.USAT_PREFERENCES_COOKIE_NAME)) {
				if (c.getValue().equalsIgnoreCase(PreferencesCookieProcessor.USAT_PREFERENCES_HTML)) {
					prefersHTML = true;
				}
			}
		}
		return prefersHTML;
	}

}
