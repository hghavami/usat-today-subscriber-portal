package com.usatoday.esub.cookies;

import javax.servlet.http.Cookie;

public class AuthenticationCookieProcessor {

	public static final String NEW_START_COOKIE_NAME = "USATCircTempUID";
	public static final String REMEMBER_ME_COOKIE_NAME = "USATCircUID";
	public static final String AUTOLOGIN_COOKIE_NAME = "autologin";
	public static final String SESSION_KEY_COOKIE_NAME = "sessionKey";
	public static final String TRIAL_REMEMBER_ME_COOKIE_NAME = "USATCircUIDTrial";
	public static final String ONE_DAY_REMEMBER_ME_COOKIE_NAME = "USATCircOneDayUID";
	public static final String CHOICE_REMEMBER_ME_COOKIE_NAME = "USATCircChoiceUID";	
	private static final int newStartExpiry = 259200; // 3 days
	private static final int rememberMeExpiry = 2678400; // 31 days
	private static final int atyponSessionKeyExpiry = 18000;  // 5 hours

	/**
	 * 
	 * @param emailAddress
	 * @return
	 */
	public Cookie createAtyponAutoLoginCookie(String autoLogin) {
		Cookie c = null;
	
		c = new Cookie(AUTOLOGIN_COOKIE_NAME, autoLogin);
		c.setMaxAge(AuthenticationCookieProcessor.rememberMeExpiry);
		c.setComment("USAT Atypon Auto Login Cookie");
		c.setPath("/");
		return c;
	}

	/**
	 * 
	 * @param emailAddress
	 * @return
	 */
	public Cookie createAtyponSessionKeyCookie(String sessionKey) {
		Cookie c = null;

		c = new Cookie(SESSION_KEY_COOKIE_NAME, sessionKey);
		c.setMaxAge(AuthenticationCookieProcessor.atyponSessionKeyExpiry);
		c.setComment("USAT Atypon Session Key Cookie");
		c.setPath("/");
		return c;
	}
	private static final int oneDayAccessExpiry = 86400;  // 1 day

	/**
	 * 
	 * @param pubCode
	 * @param serialNumber
	 * @param emailAddress
	 * @return
	 */
	public Cookie createNewStartTempCookie(String pubCode, String serialNumber, String emailAddress) {
		Cookie c = null;

		StringBuilder sb = new StringBuilder();
		sb.append(pubCode).append(":").append(serialNumber).append(":").append(emailAddress);
		c = new Cookie(NEW_START_COOKIE_NAME, sb.toString());
		c.setMaxAge(AuthenticationCookieProcessor.newStartExpiry);
		c.setComment("USAT New Start Cookie");
		c.setPath("/");

		return c;
	}

	/**
	 * 
	 * @param emailAddress
	 * @return
	 */
	public Cookie createRememberMeCookie(String emailAddress) {
		Cookie c = null;
	
		c = new Cookie(REMEMBER_ME_COOKIE_NAME, emailAddress);
		c.setMaxAge(AuthenticationCookieProcessor.rememberMeExpiry);
		c.setComment("USAT Remember Me Cookie");
		c.setPath("/");
		return c;
	}

	public Cookie createRememberMeTrialCookie(String emailAddress, int duration) {
		Cookie c = null;

		c = new Cookie(TRIAL_REMEMBER_ME_COOKIE_NAME, emailAddress);

		// calculate expiry
		if (duration < 1) {
			duration = 1;
		}

		int age = duration * 24 * 60 * 60; // age in second = days * hours in day * minutes in hour * seconds in minute

		c.setMaxAge(age);
		c.setComment("USAT Remember Me Cookie Trial Subscription");
		c.setPath("/");
		return c;
	}

	/**
	 * 
	 * @param c
	 * @return
	 */
	public Cookie extendExpiration(Cookie c) {
		if (c != null) {
			c.setMaxAge(AuthenticationCookieProcessor.rememberMeExpiry);
			c.setPath("/");
		}
		return c;
	}

	/**
	 * 
	 * @param c
	 * @return
	 */
	public boolean isNewStartCookie(Cookie c) {
		boolean newStartCookie = false;

		if (c.getName().equalsIgnoreCase(AuthenticationCookieProcessor.NEW_START_COOKIE_NAME)) {
			newStartCookie = true;
		}
		return newStartCookie;
	}

	/**
	 * 
	 * @param c
	 * @return
	 */
	public boolean isRememberMeCookie(Cookie c) {
		boolean rememberMeCookie = false;
		if (c.getName().equalsIgnoreCase(AuthenticationCookieProcessor.REMEMBER_ME_COOKIE_NAME)) {
			rememberMeCookie = true;
		}

		return rememberMeCookie;
	}

	/**
	 * 
	 * @param c
	 * @return
	 */
	public boolean isTrialStartCookie(Cookie c) {
		boolean rememberMeCookie = false;
		if (c.getName().equalsIgnoreCase(AuthenticationCookieProcessor.TRIAL_REMEMBER_ME_COOKIE_NAME)) {
			rememberMeCookie = true;
		}

		return rememberMeCookie;
	}

	/**
	 * 
	 * @param cookies
	 * @return
	 */
	public Cookie returnNewStartCookie(Cookie[] cookies) {

		Cookie cookie = null;

		if (cookies != null) {
			for (Cookie c : cookies) {
				if (c.getName().equalsIgnoreCase(NEW_START_COOKIE_NAME)) {
					cookie = c;
					break;
				}
			}
		}
		return cookie;
	}

	/**
	 * 
	 * @param cookies
	 * @return
	 */
	public Cookie returnRememberMeCookie(Cookie[] cookies) {

		Cookie cookie = null;

		if (cookies != null) {
			for (Cookie c : cookies) {
				if (c.getName().equalsIgnoreCase(REMEMBER_ME_COOKIE_NAME)) {
					cookie = c;
					break;
				}
			}
		}
		return cookie;
	}

	/**
	 * 
	 * @param cookies
	 * @return
	 */
	public Cookie returnTrialStartCookie(Cookie[] cookies) {

		Cookie cookie = null;

		if (cookies != null) {
			for (Cookie c : cookies) {
				if (c.getName().equalsIgnoreCase(TRIAL_REMEMBER_ME_COOKIE_NAME)) {
					cookie = c;
					break;
				}
			}
		}
		return cookie;
	}

	public Cookie setCookieForDeletion(Cookie c) {
		if (c != null) {
			c.setMaxAge(0);
		}

		return c;
	}

	/**
	 * 
	 * @param cookies
	 * @return
	 */
	public Cookie returnAutoLoginCookie(Cookie[] cookies) {
	
		Cookie cookie = null;
	
		if (cookies != null) {
			for (Cookie c : cookies) {
				if (c.getName().equalsIgnoreCase(AUTOLOGIN_COOKIE_NAME)) {
					cookie = c;
					break;
				}
			}
		}
		return cookie;
	}

	/**
	 * 
	 * @param cookies
	 * @return
	 */
	public Cookie returnSessionKeyCookie(Cookie[] cookies) {
	
		Cookie cookie = null;
	
		if (cookies != null) {
			for (Cookie c : cookies) {
				if (c.getName().equalsIgnoreCase(SESSION_KEY_COOKIE_NAME)) {
					cookie = c;
					break;
				}
			}
		}
		return cookie;
	}

	/**
	 * 
	 * @param accessCode
	 * @return
	 */
	public Cookie createOneDayAccessRememberMeCookie(String accessCode) {
		Cookie c = null;
	
		c = new Cookie(ONE_DAY_REMEMBER_ME_COOKIE_NAME, accessCode);
		c.setMaxAge(AuthenticationCookieProcessor.oneDayAccessExpiry);
		c.setComment("USAT One Day Remember Me Cookie");
		c.setPath("/");
		return c;
	}

	/**
	 * 
	 * @param c
	 * @return
	 */
	public boolean isOneDayAccessRememberMeCookie(Cookie c) {
		boolean rememberMeCookie = false;
		if (c.getName().equalsIgnoreCase(AuthenticationCookieProcessor.ONE_DAY_REMEMBER_ME_COOKIE_NAME)) {
			rememberMeCookie = true;
		}
	
		return rememberMeCookie;
	}

	/**
	 * 
	 * @param cookies
	 * @return
	 */
	public Cookie returnOneDayRememberMeCookie(Cookie[] cookies) {
	
		Cookie cookie = null;
	
		if (cookies != null) {
			for (Cookie c : cookies) {
				if (c.getName().equalsIgnoreCase(ONE_DAY_REMEMBER_ME_COOKIE_NAME)) {
					cookie = c;
					break;
				}
			}
		}
		return cookie;
	}

	public Cookie returnChoiceRememberMeCookie(Cookie[] cookies) {
		
		Cookie cookie = null;
	
		if (cookies != null) {
			for (Cookie c : cookies) {
				if (c.getName().equalsIgnoreCase(CHOICE_REMEMBER_ME_COOKIE_NAME)) {
					cookie = c;
					break;
				}
			}
		}
		return cookie;
	}

	/**
	 * 
	 * @param accessCode
	 * @return
	 */
	public Cookie createChoiceAccessRememberMeCookie(String accessCode) {
		Cookie c = null;
	
		c = new Cookie(CHOICE_REMEMBER_ME_COOKIE_NAME, accessCode);
		c.setMaxAge(AuthenticationCookieProcessor.oneDayAccessExpiry);
		c.setComment("USAT Choice Remember Me Cookie");
		c.setPath("/");
		return c;
	}

	/**
	 * 
	 * @param c
	 * @return
	 */
	public boolean isChoiceAccessRememberMeCookie(Cookie c) {
		boolean rememberMeCookie = false;
		if (c.getName().equalsIgnoreCase(AuthenticationCookieProcessor.CHOICE_REMEMBER_ME_COOKIE_NAME)) {
			rememberMeCookie = true;
		}
	
		return rememberMeCookie;
	}
}
