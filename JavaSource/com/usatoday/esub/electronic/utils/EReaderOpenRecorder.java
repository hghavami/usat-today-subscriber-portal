package com.usatoday.esub.electronic.utils;

import javax.servlet.http.HttpServletRequest;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.customer.CustomerIntf;
import com.usatoday.business.interfaces.customer.TrialCustomerIntf;
import com.usatoday.esub.ncs.handlers.CustomerHandler;
import com.usatoday.esub.trials.handlers.TrialCustomerHandler;
import com.usatoday.integration.ElectronicEditionAccessLogDAO;
import com.usatoday.integration.ElectronicEditionAccessLogTO;

public class EReaderOpenRecorder {

	public static void recordEReaderOpen(HttpServletRequest request) throws Exception {
		CustomerHandler ch = null;
		ch = (CustomerHandler) request.getSession().getAttribute("customerHandler");

		String emailAddress = null;

		try {
			if (ch != null && ch.getCustomer() != null) {
				CustomerIntf customer = ch.getCustomer();
				ElectronicEditionAccessLogTO eLog = new ElectronicEditionAccessLogTO();

				emailAddress = customer.getCurrentEmailRecord().getEmailAddress();
				eLog.setEmailAddress(emailAddress);

				// log the open

				if (customer.getCurrentAccount() != null) {
					// existing account set up
					eLog.setAccountNumber(customer.getCurrentAccount().getAccountNumber());
				}

				eLog.setClientIP(request.getRemoteAddr());

				eLog.setPubCode(customer.getCurrentEmailRecord().getPubCode());

				ElectronicEditionAccessLogDAO dao = new ElectronicEditionAccessLogDAO();
				dao.insert(eLog);
			} else {
				TrialCustomerHandler tch = (TrialCustomerHandler) request.getSession().getAttribute("trialCustomerHandler");
				if (tch != null && tch.getTrialCustomer() != null) {

					TrialCustomerIntf tCust = tch.getTrialCustomer();

					ElectronicEditionAccessLogTO eLog = new ElectronicEditionAccessLogTO();

					emailAddress = tCust.getEmailAddress();
					eLog.setEmailAddress(emailAddress);

					eLog.setClientIP(request.getRemoteAddr());

					eLog.setAccountNumber(tCust.getCurrentInstance().getPartner().getPartnerID());
					eLog.setPubCode(tCust.getCurrentInstance().getPubCode());
					eLog.setTrialAccess(true);

					ElectronicEditionAccessLogDAO dao = new ElectronicEditionAccessLogDAO();
					dao.insert(eLog);
				}
			}
		} catch (Exception e) {
			String origMessage = e.getMessage();
			UsatException ue = new UsatException("Failed to record eReader Open for email: " + emailAddress + " Original message: "
					+ origMessage, e);
			throw ue;
		}
	}
}
