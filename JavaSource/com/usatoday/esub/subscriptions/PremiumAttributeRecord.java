/*
 * Created on Jun 8, 2005
 *
 * This file added as part of the Multiple Premium Fulfillment Project.
 * 
 */
package com.usatoday.esub.subscriptions;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;

import com.usatoday.businessObjects.util.ZeroFilledNumeric;
import com.usatoday.esub.common.DataSourceConnectionFactory;
import com.usatoday.UsatException;

/**
 * @author aeast
 * @version
 * 
 *          <code>PremiumAttributeRecord</code> is a class used for the creation and storage of Multiple Premium Fulfillment
 *          attriutes. It is a basic JavaBean with business rules and persistence logic.
 * 
 */
public class PremiumAttributeRecord implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3768644448087411145L;
	// static state fields
	public static final int NEW = 0;
	public static final int READY_FOR_BATCH = 1;
	public static final int BATCH_IN_PROGRESS = 2;
	public static final int READY_FOR_DELETION = 3;
	public static final int BATCH_ERROR = 4;

	// static user id / password overrides
	private static String userID = null;
	private static String password = null;

	// static SQL strings
	private static final String UPDATE_MPF_ATTRIBUTE = "update PREMTRANSDETAIL set PubCode=?, AccountNum=?, PremPromoCode=?, PremCode=?, PremType=?, AttributeName=?, AttributeValue=?, TransID=?, DateUpdated=?, TimeUpdated=?, State=?, UpdateTimestamp=? GiftPayerReceive=? where PremKey = ?";
	private static final String UPDATE_MPF_BATCH_STATE = "update PREMTRANSDETAIL set State=?, UpdateTimestamp=? where State = ?";

	private static final String FETCH_MPF_ATTRIBUTES_IN_STATE = "select PremKey, PubCode, AccountNum, PremPromoCode, PremCode, PremType, AttributeName, AttributeValue, GiftPayerReceive, TransID, DateUpdated, TimeUpdated, State, UpdateTimestamp from PREMTRANSDETAIL where State = ?";
	private static final String DELETE_MPF_ARRTIBUTES_IN_DELETE_STATE_OLDER_THAN_DATE = "delete from PREMTRANSDETAIL where state = 3 and updateTimestamp <= ?";

	// persistent fields
	private String pubCode = null;
	private String accountNum = null;
	private String premiumPromotionCode = null;
	private String premiumCode = null;
	private String premiumType = null;
	private String giftPayerReceive = null;
	private String attributeName = null;
	private String attributeValue = null;
	private String orderId = null;
	private String dateUpdated = null;
	private String timeUpdated = null;
	private Timestamp updateTimestamp = null;
	private long key = 0;
	private int state = PremiumAttributeRecord.NEW;
	private boolean clubNumber = false;

	/**
	 * @return Returns the accountNum.
	 */
	public String getAccountNum() {
		return this.accountNum;
	}

	/**
	 * @param accountNum
	 *            The accountNum to set.
	 * @throws UsatException
	 */
	public void setAccountNum(String accountNum) throws com.usatoday.UsatException {

		// validate that the size of the account number is acceptable
		if (accountNum != null && accountNum.length() <= 9) {
			this.accountNum = accountNum;
		} else {
			if (accountNum == null) {
				this.accountNum = "";
			} else {
				String msg = "setAccountNum() - Invalid Account Number set on Premium Attribute Record (Max 9 chars): "
						+ accountNum;
				this.logMessage(msg);
				throw new UsatException(msg);
			}
		}
	}

	/**
	 * @return Returns the attributeValue.
	 */
	public String getAttributeValue() {
		return this.attributeValue;
	}

	/**
	 * @param attributeValue
	 *            The attributeValue to set.
	 * @throws UsatException
	 */
	public void setAttributeValue(String attributeValue) throws UsatException {
		if (attributeValue != null && attributeValue.length() <= 50) {
			if (attributeValue.indexOf("'") >= 0) {
				attributeValue = attributeValue.replaceAll("'", "");
			}
			if (attributeValue.indexOf("\"") >= 0) {
				attributeValue = attributeValue.replaceAll("\"", "");
			}

			this.attributeValue = attributeValue;
		} else {
			if (attributeValue == null) {
				this.attributeValue = "";
			} else {
				String msg = "setAttributeValue() - Invalid value set on Premium Attribute Record (Max 50 chars): "
						+ attributeValue;
				this.logMessage(msg);
				throw new UsatException(msg);
			}
		}
	}

	/**
	 * @return Returns the attribuuteName.
	 */
	public String getAttributeName() {
		return this.attributeName;
	}

	/**
	 * @param attribuuteName
	 *            The attribuuteName to set.
	 * @throws UsatException
	 */
	public void setAttribuuteName(String attributeName) throws UsatException {
		if (attributeName != null && attributeName.length() <= 10) {
			this.attributeName = attributeName;
		} else {
			if (attributeName == null) {
				this.attributeName = "";
			} else {
				String msg = "setAttribuuteName() - Invalid value set on Premium Attribute Record (Max 10 chars): " + attributeName;
				this.logMessage(msg);
				throw new UsatException(msg);
			}
		}
	}

	/**
	 * @return Returns the dateUpdated.
	 */
	public String getDateUpdated() {
		return this.dateUpdated;
	}

	/**
	 * @param dateUpdated
	 *            The dateUpdated to set. Format YYYYMMDD.
	 * @throws UsatException
	 */
	protected void setDateUpdated(String dateUpdated) throws UsatException {
		if (dateUpdated != null && dateUpdated.length() == 8) {
			this.dateUpdated = dateUpdated;
		} else {
			if (dateUpdated == null) {
				this.dateUpdated = "";
			} else {
				String msg = "setDateUpdated() - Invalid value set on Premium Attribute Record (Max/Min 8 chars, format YYYYMMDD): "
						+ dateUpdated;
				this.logMessage(msg);
				throw new UsatException(msg);
			}
		}
	}

	/**
	 * @return Returns the key.
	 */
	public long getKey() {
		return this.key;
	}

	/**
	 * @param key
	 *            The key to set.
	 */
	private void setKey(long key) {
		this.key = key;
	}

	/**
	 * @return Returns the giftPayerReceive.
	 */
	public String getGiftPayerReceive() {
		return this.giftPayerReceive;
	}

	/**
	 * @param giftPayerReceive
	 *            The giftPayerReceive to set.
	 */
	public void setGiftPayerReceive(String giftPayerReceive) throws UsatException {
		if (giftPayerReceive != null && giftPayerReceive.length() <= 1) {
			this.giftPayerReceive = giftPayerReceive;
		} else {
			if (giftPayerReceive == null) {
				this.giftPayerReceive = "";
			} else {
				String msg = "setGiftPayerReceive() - Invalid value set on Premium Attribute Record (Max 1 char): "
						+ giftPayerReceive;
				this.logMessage(msg);
				throw new UsatException(msg);
			}
		}
	}

	/**
	 * @return Returns the orderId.
	 */
	public String getOrderId() {
		return this.orderId;
	}

	/**
	 * @param orderId
	 *            The orderId to set.
	 * @throws UsatException
	 */
	public void setOrderId(String orderId) throws UsatException {
		if (orderId != null && orderId.length() <= 20) {
			this.orderId = orderId;
		} else {
			if (orderId == null) {
				this.orderId = "";
			} else {
				String msg = "setOrderId() - Invalid value set on Premium Attribute Record (Max 20 chars): " + orderId;
				this.logMessage(msg);
				throw new UsatException(msg);
			}
		}
	}

	/**
	 * @return Returns the premiumCode.
	 */
	public String getPremiumCode() {
		return this.premiumCode;
	}

	/**
	 * @param premiumCode
	 *            The premiumCode to set.
	 * @throws UsatException
	 */
	public void setPremiumCode(String premiumCode) throws UsatException {
		if (premiumCode != null && premiumCode.length() <= 2) {
			this.premiumCode = premiumCode;
		} else {
			if (premiumCode == null) {
				this.premiumCode = "";
			} else {
				String msg = "setPremiumCode() - Invalid value set on Premium Attribute Record (Max 2 chars): " + premiumCode;
				this.logMessage(msg);
				throw new UsatException(msg);
			}
		}
	}

	/**
	 * @return Returns the premiumPromotionCode.
	 */
	public String getPremiumPromotionCode() {
		return this.premiumPromotionCode;
	}

	/**
	 * @param premiumPromotionCode
	 *            The premiumPromotionCode to set.
	 * @throws UsatException
	 */
	public void setPremiumPromotionCode(String premiumPromotionCode) throws UsatException {
		if (premiumPromotionCode != null && premiumPromotionCode.length() <= 2) {
			this.premiumPromotionCode = premiumPromotionCode;
		} else {
			if (premiumPromotionCode == null) {
				this.premiumPromotionCode = "";
			} else {
				String msg = "premiumPromotionCode() - Invalid value set on Premium Attribute Record (Max 2 chars): "
						+ premiumPromotionCode;
				this.logMessage(msg);
				throw new UsatException(msg);
			}
		}
	}

	/**
	 * @return Returns the premiumType.
	 */
	public String getPremiumType() {
		return this.premiumType;
	}

	/**
	 * @param premiumType
	 *            The premiumType to set.
	 * @throws UsatException
	 */
	public void setPremiumType(String premiumType) throws UsatException {
		if (premiumType != null && premiumType.length() <= 10) {
			this.premiumType = premiumType;
			// flag this attibute if it contains a club number
			if (this.premiumType.equalsIgnoreCase("CLUB")) {
				this.setClubNumber(true);
			}
		} else {
			if (premiumType == null) {
				this.premiumType = "";
			} else {
				String msg = "setPremiumType() - Invalid value set on Premium Attribute Record (Max 10 chars): " + premiumType;
				this.logMessage(msg);
				throw new UsatException(msg);
			}
		}
	}

	/**
	 * @return Returns the pubCode.
	 */
	public String getPubCode() {
		return this.pubCode;
	}

	/**
	 * @param pubCode
	 *            The pubCode to set.
	 * @throws UsatException
	 */
	public void setPubCode(String pubCode) throws UsatException {
		if (pubCode != null && pubCode.length() <= 2) {
			this.pubCode = pubCode;
		} else {
			if (pubCode == null) {
				this.pubCode = "";
			} else {
				String msg = "setPubCode() - Invalid value set on Premium Attribute Record (Max 2 chars): " + pubCode;
				this.logMessage(msg);
				throw new UsatException(msg);
			}
		}
	}

	/**
	 * @return Returns the state.
	 */
	public int getState() {
		return this.state;
	}

	/**
	 * @param state
	 *            The state to set.
	 */
	public void setState(int state) {
		this.state = state;
	}

	/**
	 * @return Returns the timeUpdated.
	 */
	public String getTimeUpdated() {
		return this.timeUpdated;
	}

	/**
	 * @param timeUpdated
	 *            The timeUpdated to set.
	 */
	protected void setTimeUpdated(String timeUpdated) throws UsatException {
		if (timeUpdated != null && timeUpdated.length() == 6) {
			this.timeUpdated = timeUpdated;
		} else {
			if (timeUpdated == null) {
				this.timeUpdated = "";
			} else {
				String msg = "setTimeUpdated() - Invalid value set on Premium Attribute Record (Max/Min 6 chars, Format HHMMSS - military time): "
						+ timeUpdated;
				this.logMessage(msg);
				throw new UsatException(msg);
			}
		}
	}

	/**
	 * @return Returns the updateTimestamp.
	 */
	public Timestamp getUpdateTimestamp() {
		return this.updateTimestamp;
	}

	/**
	 * 
	 * @param ts
	 */

	@SuppressWarnings("unused")
	private void setUpdateTimestamp(Timestamp ts) {
		this.updateTimestamp = ts;
	}

	/**
	 * 
	 * @param msg
	 *            - A string to be printed to System.out
	 */
	public void logMessage(String msg) {
		System.out.println("PremiumAttributeRecord : " + msg);
	}

	/**
	 * Generates a collection of MPF attribute records.
	 * 
	 * @param rs
	 * @return
	 * @throws UsatException
	 */
	protected static Collection<PremiumAttributeRecord> objectFactory(ResultSet rs) throws UsatException {
		ArrayList<PremiumAttributeRecord> results = new ArrayList<PremiumAttributeRecord>();
		try {
			while (rs.next()) {

				long primaryKey = rs.getLong("PremKey");
				int state = rs.getInt("State");

				String tempStr = null;

				PremiumAttributeRecord attribute = new PremiumAttributeRecord();

				attribute.setKey(primaryKey);
				attribute.setState(state);

				tempStr = rs.getString("AccountNum");
				if (tempStr != null) {
					attribute.setAccountNum(tempStr.trim());
				}

				tempStr = rs.getString("PubCode");
				if (tempStr != null) {
					attribute.setPubCode(tempStr.trim());
				}

				tempStr = rs.getString("PremPromoCode");
				if (tempStr != null) {
					attribute.setPremiumPromotionCode(tempStr.trim());
				}

				tempStr = rs.getString("PremCode");
				if (tempStr != null) {
					attribute.setPremiumCode(tempStr.trim());
				}

				tempStr = rs.getString("PremType");
				if (tempStr != null) {
					attribute.setPremiumType(tempStr.trim());
				}

				tempStr = rs.getString("GiftPayerReceive");
				if (tempStr != null) {
					attribute.setGiftPayerReceive(tempStr.trim());
				}

				tempStr = rs.getString("AttributeName");
				if (tempStr != null) {
					attribute.setAttribuuteName(tempStr.trim());
				}

				tempStr = rs.getString("AttributeValue");
				if (tempStr != null) {
					attribute.setAttributeValue(tempStr.trim());
				}

				tempStr = rs.getString("TransID");
				if (tempStr != null) {
					attribute.setOrderId(tempStr.trim());
				}

				tempStr = rs.getString("DateUpdated");
				if (tempStr != null) {
					attribute.setDateUpdated(tempStr.trim());
				}

				tempStr = rs.getString("TimeUpdated");
				if (tempStr != null) {
					attribute.setTimeUpdated(tempStr.trim());
				}

				Timestamp ts = rs.getTimestamp("UpdateTimestamp");
				if (ts != null) {
					attribute.updateTimestamp = ts;
				}

				results.add(attribute);
			}
		} catch (SQLException sqle) {
			throw new UsatException(sqle.getMessage() + "     " + sqle.toString());
		}

		return results;
	}

	/**
	 * Method ot insert or update the object
	 * 
	 * @throws UsatException
	 */
	public void save() throws UsatException {
		// set the update times
		try {
			java.util.Calendar cal = java.util.Calendar.getInstance();
			java.util.Date date = cal.getTime();
			String dateStr = new java.text.SimpleDateFormat("yyyyMMdd").format(date);
			String timeStr = ZeroFilledNumeric.getValue(2, new java.text.SimpleDateFormat("H").format(date))
					+ ZeroFilledNumeric.getValue(2, new java.text.SimpleDateFormat("m").format(date))
					+ ZeroFilledNumeric.getValue(2, new java.text.SimpleDateFormat("s").format(date));
			this.setDateUpdated(dateStr);
			this.setTimeUpdated(timeStr);
		} catch (Exception e) {
			System.out.println("Failed to set update time stamp. " + e.getMessage());
		}

		if (this.getKey() > 0) {
			// has primary key so update the record
			this.updateAttribute();
		} else {
			// no primary key so insert the record
			this.insertAttribute();
		}
	}

	/**
	 * 
	 * @throws UsatException
	 */
	protected void insertAttribute() throws UsatException {

		java.sql.Connection conn = null;
		try {

			conn = PremiumAttributeRecord.getDBConnection();

			java.sql.Statement statement = conn.createStatement();

			// insert into PREMTRANSDETAIL (PubCode, AccountNum, PremPromoCode,
			// PremCode, PremType, AttributeName, AttributeValue, TransID,
			// DateUpdated, TimeUpdated, State, UpdateTimestamp )values
			// (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
			StringBuffer sql = new StringBuffer(
					"insert into PREMTRANSDETAIL (PubCode, AccountNum, PremPromoCode,PremCode, PremType, AttributeName, AttributeValue, GiftPayerReceive, TransID,DateUpdated, TimeUpdated, State, UpdateTimestamp ) values (");

			if (this.getPubCode() != null) {
				sql.append("'").append(this.getPubCode()).append("',");
			} else {
				sql.append("null,");
			}
			if (this.getAccountNum() != null) {
				sql.append("'").append(this.getAccountNum()).append("',");
			} else {
				sql.append("null,");
			}

			if (this.getPremiumPromotionCode() != null) {
				sql.append("'").append(this.getPremiumPromotionCode()).append("',");
			} else {
				sql.append("null,");
			}

			if (this.getPremiumCode() != null) {
				sql.append("'").append(this.getPremiumCode()).append("',");
			} else {
				sql.append("null,");
			}

			if (this.getPremiumType() != null) {
				sql.append("'").append(this.getPremiumType()).append("',");
			} else {
				sql.append("null,");
			}

			if (this.getAttributeName() != null) {
				sql.append("'").append(this.getAttributeName()).append("',");
			} else {
				sql.append("null,");
			}

			if (this.getAttributeValue() != null) {
				sql.append("'").append(this.getAttributeValue()).append("',");
			} else {
				sql.append("null,");
			}

			if (this.getGiftPayerReceive() != null) {
				sql.append("'").append(this.getGiftPayerReceive()).append("',");
			} else {
				sql.append("null,");
			}

			if (this.getOrderId() != null) {
				sql.append("'").append(this.getOrderId()).append("',");
			} else {
				sql.append("null,");
			}

			if (this.getDateUpdated() != null) {
				sql.append("'").append(this.getDateUpdated()).append("',");
			} else {
				sql.append("null,");
			}

			if (this.getTimeUpdated() != null) {
				sql.append("'").append(this.getTimeUpdated()).append("',");
			} else {
				sql.append("null,");
			}

			sql.append(this.getState()).append(",'");

			Timestamp ts = new Timestamp(System.currentTimeMillis());
			sql.append(ts.toString());
			sql.append("')");

			// execute the SQL
			int rowcount = statement.executeUpdate(sql.toString(), Statement.RETURN_GENERATED_KEYS);

			if (rowcount != 1) {
				throw new UsatException(
						"No Premium Attribute Record Inserted Or other error and yet no exception condition exists: Number rows affected: "
								+ rowcount);
			}

			ResultSet rs = statement.getGeneratedKeys();

			rs.next();
			long primaryKey = rs.getLong(1);

			this.setKey(primaryKey);

			statement.close();
		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} finally {
			DataSourceConnectionFactory.closeConnection(conn);
		}
	}

	/**
	 * 
	 * @throws UsatException
	 */
	protected void updateAttribute() throws UsatException {
		java.sql.Connection conn = null;
		try {

			conn = PremiumAttributeRecord.getDBConnection();

			java.sql.PreparedStatement statement = conn.prepareStatement(PremiumAttributeRecord.UPDATE_MPF_ATTRIBUTE);

			// order of colums
			// update ..
			// PubCode=?, AccountNum=?, PremPromoCode=?, PremCode=?, PremType=?,
			// AttributeName=?, AttributeValue=?, TransID=?, DateUpdated=?,
			// TimeUpdated=?, State=?, UpdateTimestamp=? where PremKey = ?
			if (this.getPubCode() != null) {
				statement.setString(1, this.getPubCode());
			} else {
				statement.setNull(1, Types.CHAR);
			}

			if (this.getAccountNum() != null) {
				statement.setString(2, this.getAccountNum());
			} else {
				statement.setNull(2, Types.CHAR);
			}

			if (this.getPremiumPromotionCode() != null) {
				statement.setString(3, this.getPremiumPromotionCode());
			} else {
				statement.setNull(3, Types.CHAR);
			}

			if (this.getPremiumCode() != null) {
				statement.setString(4, this.getPremiumCode());
			} else {
				statement.setNull(4, Types.CHAR);
			}

			if (this.getPremiumType() != null) {
				statement.setString(5, this.getPremiumType());
			} else {
				statement.setNull(5, Types.CHAR);
			}

			if (this.getAttributeName() != null) {
				statement.setString(6, this.getAttributeName());
			} else {
				statement.setNull(6, Types.CHAR);
			}

			if (this.getAttributeValue() != null) {
				statement.setString(7, this.getAttributeValue());
			} else {
				statement.setNull(7, Types.CHAR);
			}

			if (this.getOrderId() != null) {
				statement.setString(8, this.getOrderId());
			} else {
				statement.setNull(8, Types.CHAR);
			}

			if (this.getDateUpdated() != null) {
				statement.setString(9, this.getDateUpdated());
			} else {
				statement.setNull(9, Types.CHAR);
			}

			if (this.getTimeUpdated() != null) {
				statement.setString(10, this.getTimeUpdated());
			} else {
				statement.setNull(10, Types.CHAR);
			}

			statement.setInt(11, this.getState());

			Timestamp ts = new Timestamp(System.currentTimeMillis());
			statement.setTimestamp(12, ts);

			if (this.getGiftPayerReceive() != null) {
				statement.setString(13, this.getGiftPayerReceive());
			} else {
				statement.setNull(13, Types.CHAR);
			}

			// where clause
			statement.setLong(14, this.getKey());

			// execute the SQL
			int rowsAffected = statement.executeUpdate();

			if (rowsAffected != 1) {
				throw new UsatException(
						"No Premium Attribute Record Updated Or too many rows updated and yet no exception condition exists: Number rows affected: "
								+ statement.getUpdateCount());
			}

			statement.close();
		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} finally {
			DataSourceConnectionFactory.closeConnection(conn);
		}
	}

	/**
	 * Saves a collection of attribute records
	 * 
	 * @param attributes
	 * @throws UsatException
	 * @return int Number or records saved which should equal the size of the collection passed into the method unles it is a mixed
	 *         type collection
	 */
	public static int saveAttributes(Collection<PremiumAttributeRecord> attributes) throws UsatException {
		if (attributes == null) {
			return 0;
		}

		int numSaved = 0;

		Iterator<PremiumAttributeRecord> itr = attributes.iterator();
		while (itr.hasNext()) {
			Object item = itr.next();
			PremiumAttributeRecord attribute = null;
			if (item instanceof PremiumAttributeRecord) {
				attribute = (PremiumAttributeRecord) item;
			} else {
				// not of the correct type so go to next one in collection
				continue;
			}

			try {
				attribute.save();
				numSaved++;
			} catch (UsatException ue) {
				// failed to save current item
				System.out.println("Failed to save a MPF attribute record for order: " + attribute.getOrderId() + " pub: "
						+ attribute.getPubCode() + " account:" + attribute.getAccountNum() + " attrName:"
						+ attribute.getAttributeName() + " attrValue:" + attribute.getAttributeValue() + " premPromoCode:"
						+ attribute.getPremiumPromotionCode() + " premCode:" + attribute.getPremiumCode() + " premCodeType:"
						+ attribute.getPremiumType());
			}
		}
		return numSaved;
	}

	/**
	 * 
	 * @throws UsatException
	 */
	public static int changeStateToInBatchProcessing() throws UsatException {
		return PremiumAttributeRecord.changeState(PremiumAttributeRecord.READY_FOR_BATCH, PremiumAttributeRecord.BATCH_IN_PROGRESS);
	}

	public static int changeStateToReadyForDelete() throws UsatException {
		return PremiumAttributeRecord.changeState(PremiumAttributeRecord.BATCH_IN_PROGRESS,
				PremiumAttributeRecord.READY_FOR_DELETION);
	}

	/**
	 * 
	 * @param oldState
	 * @param newState
	 * @return
	 * @throws UsatException
	 */
	private static int changeState(int oldState, int newState) throws UsatException {
		java.sql.Connection conn = null;
		int numRowsAffected = 0;
		try {

			conn = PremiumAttributeRecord.getDBConnection();

			java.sql.PreparedStatement statement = conn.prepareStatement(PremiumAttributeRecord.UPDATE_MPF_BATCH_STATE);

			// "update PREMTRANSDETAIL set State=?, UpdateTimestamp=? where State = ?"
			statement.setInt(1, newState);
			Timestamp ts = new Timestamp(System.currentTimeMillis());
			statement.setTimestamp(2, ts);
			statement.setInt(3, oldState);

			numRowsAffected = statement.executeUpdate();
		} catch (Exception e) {
			System.out.println("PremiumAttributeRecord:changeStatetoInBatchProcessing() " + e.getMessage());
			throw new UsatException(e);
		} finally {
			if (conn != null) {
				DataSourceConnectionFactory.closeConnection(conn);
			}
		}
		return numRowsAffected;
	}

	/**
	 * 
	 * @return
	 * @throws UsatException
	 */
	public static Collection<PremiumAttributeRecord> fetchAttributesRecordsForBatchProcessing() throws UsatException {
		return PremiumAttributeRecord.fetchAttributesInState(PremiumAttributeRecord.BATCH_IN_PROGRESS);
	}

	/**
	 * 
	 * @param state
	 * @return
	 * @throws UsatException
	 */
	private static Collection<PremiumAttributeRecord> fetchAttributesInState(int state) throws UsatException {
		java.sql.Connection conn = null;
		Collection<PremiumAttributeRecord> records = null;
		try {

			conn = PremiumAttributeRecord.getDBConnection();

			java.sql.PreparedStatement statement = conn.prepareStatement(PremiumAttributeRecord.FETCH_MPF_ATTRIBUTES_IN_STATE);

			statement.setInt(1, state);

			ResultSet rs = statement.executeQuery();

			records = PremiumAttributeRecord.objectFactory(rs);
		} catch (Exception e) {
			System.out.println("PremiumAttributeRecord:changeStatetoInBatchProcessing() " + e.getMessage());
			throw new UsatException(e);
		} finally {
			if (conn != null) {
				DataSourceConnectionFactory.closeConnection(conn);
			}
		}
		return records;
	}

	/**
	 * Output this object in the form: Field Attributes Field Text Input/Output PFPUB Char 2 PUBLICATION CODE 1 PFACT# Znd 7, 0
	 * SUBSCRIBER ACCT # 3 PFPREM Char 2 Prem Promo Code 10 PFPRMC Char 2 PREMIUM OFFER CODE 12 PFPRTY Char 10 Premium Type 14
	 * PFOPTC Char 10 Prem Type Opt Code 24 PFOPVL Char 50 Option Value 34 PFGPPR Char 1 Gift Payer Premium 84 PFTRID Char 20
	 * Transaction ID 85 PFSTAT Char 1 MPF State 86 PFUDAT Znd 8, 0 Record Last Updated 106 PFUTIM Znd 6, 0 Record Time Updated 114
	 * 
	 * @return
	 */
	public String getBatchFormattedString() throws UsatException {
		StringBuffer buf = new StringBuffer();
		try {

			this.appendPaddedString(buf, 2, this.getPubCode());

			if (this.getAccountNum() != null) {
				// remove leading '34' from account number

				// hd-cons92 sew
				// this.appendPaddedString(buf, 7, this.getAccountNum().substring(2));
				this.appendPaddedString(buf, 7, this.getAccountNum());
			} else {
				this.appendPaddedString(buf, 7, null);
			}

			this.appendPaddedString(buf, 2, this.getPremiumPromotionCode());
			this.appendPaddedString(buf, 2, this.getPremiumCode());
			this.appendPaddedString(buf, 10, this.getPremiumType());
			this.appendPaddedString(buf, 10, this.getAttributeName());
			this.appendPaddedString(buf, 50, this.getAttributeValue());
			this.appendPaddedString(buf, 1, this.getGiftPayerReceive());
			this.appendPaddedString(buf, 20, this.getOrderId());
			this.appendPaddedString(buf, 1, " "); // order state is always blank
			this.appendPaddedString(buf, 8, this.getDateUpdated());
			this.appendPaddedString(buf, 6, this.getTimeUpdated());

		} catch (Exception e) {
			System.out.println("PremiumAttributeRecord:getBatchFormattedString() - Failed to create batch record: "
					+ e.getMessage());
			throw new UsatException(e);
		}

		return buf.toString();
	}

	private void appendPaddedString(StringBuffer buf, int fieldLength, String field) {
		if (buf == null) {
			return;
		}

		if (field == null) {
			for (int i = 0; i < fieldLength; i++) {
				buf.append(" ");
			}
			return;
		}

		int currentFieldLength = field.length();

		if (currentFieldLength <= fieldLength) {
			// no truncation
			buf.append(field);
			if (currentFieldLength != fieldLength) {
				// buffer remaining field with blanks
				for (int i = currentFieldLength; i < fieldLength; i++) {
					buf.append(" ");
				}
			}
		} else {
			// truncate
			buf.append(field.substring(0, fieldLength));
		}
	}

	public static int deleteAttributesOlderThanSpecifiedDays(int numDays) throws UsatException {
		int rowsAffected = 0;
		java.sql.Connection conn = null;
		try {

			conn = PremiumAttributeRecord.getDBConnection();

			java.sql.PreparedStatement statement = conn
					.prepareStatement(PremiumAttributeRecord.DELETE_MPF_ARRTIBUTES_IN_DELETE_STATE_OLDER_THAN_DATE);

			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			cal.add(Calendar.DAY_OF_YEAR, (-1 * numDays));

			Timestamp ts = new Timestamp(cal.getTimeInMillis());

			statement.setTimestamp(1, ts);

			rowsAffected = statement.executeUpdate();
		} catch (Exception e) {
			System.out.println("PremiumAttributeRecord:changeStatetoInBatchProcessing() " + e.getMessage());
			throw new UsatException(e);
		} finally {
			if (conn != null) {
				DataSourceConnectionFactory.closeConnection(conn);
			}
		}
		return rowsAffected;
	}

	/**
	 * @param password
	 *            The password to set.
	 */
	public static void setPassword(String password) {
		PremiumAttributeRecord.password = password;
	}

	/**
	 * @param userID
	 *            The userID to set.
	 */
	public static void setUserID(String userID) {
		PremiumAttributeRecord.userID = userID;
	}

	private static java.sql.Connection getDBConnection() throws Exception {
		java.sql.Connection conn = null;

		if (PremiumAttributeRecord.userID != null && PremiumAttributeRecord.password != null) {
			conn = DataSourceConnectionFactory.getDBConnection(PremiumAttributeRecord.userID, PremiumAttributeRecord.password);
		} else {
			conn = DataSourceConnectionFactory.getDBConnection();
		}

		return conn;
	}

	public boolean isClubNumber() {
		return this.clubNumber;
	}

	public void setClubNumber(boolean clubNumber) {
		this.clubNumber = clubNumber;
	}
}
