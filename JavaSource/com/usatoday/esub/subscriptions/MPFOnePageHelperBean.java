/*
 * Created on Nov 13, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.esub.subscriptions;

import java.io.Serializable;

/**
 * @author aeast
 * @date Nov 13, 2006
 * @class MPFOnePageHelperBean
 * 
 *        TODO Add a brief description of this class.
 * 
 */
public class MPFOnePageHelperBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4838266011740443624L;
	private String startDate = null;
	private int numCopies = 1;
	private String term = null;
	private boolean giftSubscription = false;
	private String keyCode = null;
	private String pubCode = null;
	private String rateCode = null;
	private String originalLandingPage = null;

	/**
     * 
     */
	public MPFOnePageHelperBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return Returns the giftSubscription.
	 */
	public boolean isGiftSubscription() {
		return this.giftSubscription;
	}

	public boolean getGiftSubscription() {
		return this.giftSubscription;
	}

	/**
	 * @param giftSubscription
	 *            The giftSubscription to set.
	 */
	public void setGiftSubscription(boolean giftSubscription) {
		this.giftSubscription = giftSubscription;
	}

	/**
	 * @return Returns the numCopies.
	 */
	public int getNumCopies() {
		return this.numCopies;
	}

	/**
	 * @param numCopies
	 *            The numCopies to set.
	 */
	public void setNumCopies(int numCopies) {
		this.numCopies = numCopies;
	}

	/**
	 * @return Returns the startDate.
	 */
	public String getStartDate() {
		return this.startDate;
	}

	/**
	 * @param startDate
	 *            The startDate to set.
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return Returns the term.
	 */
	public String getTerm() {
		return this.term;
	}

	/**
	 * @param term
	 *            The term to set.
	 */
	public void setTerm(String term) {
		this.term = term;
	}

	/**
	 * @return Returns the keyCode.
	 */
	public String getKeyCode() {
		return this.keyCode;
	}

	/**
	 * @param keyCode
	 *            The keyCode to set.
	 */
	public void setKeyCode(String keyCode) {
		this.keyCode = keyCode;
	}

	/**
	 * @return Returns the pubCode.
	 */
	public String getPubCode() {
		return this.pubCode;
	}

	/**
	 * @param pubCode
	 *            The pubCode to set.
	 */
	public void setPubCode(String pubCode) {
		this.pubCode = pubCode;
	}

	/**
	 * @return Returns the rateCode.
	 */
	public String getRateCode() {
		return this.rateCode;
	}

	/**
	 * @param rateCode
	 *            The rateCode to set.
	 */
	public void setRateCode(String rateCode) {
		this.rateCode = rateCode;
	}

	/**
	 * @return Returns the originalLandingPage.
	 */
	public String getOriginalLandingPage() {
		return this.originalLandingPage;
	}

	/**
	 * @param originalLandingPage
	 *            The originalLandingPage to set.
	 */
	public void setOriginalLandingPage(String originalLandingPage) {
		this.originalLandingPage = originalLandingPage;
	}
}
