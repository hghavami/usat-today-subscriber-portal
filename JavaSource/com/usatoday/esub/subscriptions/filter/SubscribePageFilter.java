package com.usatoday.esub.subscriptions.filter;

import javax.servlet.Filter;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;

import com.usatoday.business.interfaces.products.OfferIntf;
import com.usatoday.business.interfaces.products.SubscriptionProductIntf;
import com.usatoday.businessObjects.products.SubscriptionProductBO;
import com.usatoday.esub.common.UTCommon;
import com.usatoday.util.constants.UsaTodayConstants;

import java.io.IOException;

public class SubscribePageFilter implements Filter {
	/*
	 * (non-Java-doc)
	 * 
	 * @see java.lang.Object#Object()
	 */
	public SubscribePageFilter() {
		super();
	}

	/*
	 * (non-Java-doc)
	 * 
	 * @see javax.servlet.Filter#init(FilterConfig arg0)
	 */
	public void init(FilterConfig arg0) throws ServletException {

	}

	/*
	 * (non-Java-doc)
	 * 
	 * @see javax.servlet.Filter#doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain arg2)
	 */
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {

		try {
			HttpServletRequest request = (HttpServletRequest) req;

			OfferIntf offer = UTCommon.getCurrentOffer(request);

			SubscriptionProductIntf product = SubscriptionProductBO.getSubscriptionProduct(offer.getPubCode());

			// if bw offer
			if (product.getBrandingPubCode().equalsIgnoreCase(UsaTodayConstants.SW_PUBCODE)) {
				request.getRequestDispatcher("/subscriptions/order/checkout.faces").forward(req, res);
				return;
			} else {
				// UT offer
				if (UsaTodayConstants.UT_EE_DUAL_OFFER_ACTIVE) {
					request.getRequestDispatcher("/subscriptions/subscribeLaunchPage.faces").forward(req, res);
					return;
				} else {
					request.getRequestDispatcher("/subscriptions/order/checkout.faces").forward(req, res);
					return;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			chain.doFilter(req, res);
		}
		chain.doFilter(req, res);
	}

	/*
	 * (non-Java-doc)
	 * 
	 * @see javax.servlet.Filter#destroy()
	 */
	public void destroy() {

	}

}