package com.usatoday.esub.subscriptions.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usatoday.esub.trials.handlers.TrialCustomerHandler;

/**
 * Servlet Filter implementation class TrialSubscriptionAuthFilter
 */
public class TrialSubscriptionAuthFilter implements Filter {

	/**
	 * Default constructor.
	 */
	public TrialSubscriptionAuthFilter() {

	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {

	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;

		TrialCustomerHandler tch = (TrialCustomerHandler) req.getSession().getAttribute("trialCustomerHandler");
		if (tch == null || tch.getTrialCustomer() == null) {
			// send a redirect to the login page
			HttpServletResponse res = (HttpServletResponse) response;
			res.sendRedirect("/login/trialLogin.faces");
		} else {
			// pass the request along the filter chain
			chain.doFilter(request, response);
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {

	}

}
