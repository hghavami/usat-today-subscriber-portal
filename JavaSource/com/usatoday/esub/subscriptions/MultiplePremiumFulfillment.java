/*
 * Created on Jun 9, 2005
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.esub.subscriptions;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.usatoday.UsatException;

/**
 * @author aeast
 * @date Jun 9, 2005
 * @class MultiplePremiumFulfillment
 * 
 *        this class holds all attributes associated with an order
 * 
 */
public class MultiplePremiumFulfillment implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4992683986086072036L;
	private Collection<PremiumAttributeRecord> attributes = null;
	private String descriptionText = "";
	private String emailText = "";
	private String premiumPromoCode = null;
	private String landingPage = null;
	private boolean sendToGiftPayer = false;
	private boolean suppress4WeekOffer = true;
	private boolean hasClubNumber = false;
	private String clubNumber = null;

	/**
	 * @return Returns the suppress4WeekOffer.
	 */
	public boolean isSuppress4WeekOffer() {
		return this.suppress4WeekOffer;
	}

	/**
	 * @param suppress4WeekOffer
	 *            The suppress4WeekOffer to set.
	 */
	public void setSuppress4WeekOffer(boolean suppress4WeekOffer) {
		this.suppress4WeekOffer = suppress4WeekOffer;
	}

	/**
	 * @return Returns the sendToGiftPayer.
	 */
	public boolean isSendToGiftPayer() {
		return this.sendToGiftPayer;
	}

	/**
	 * @param sendToGiftPayer
	 *            The sendToGiftPayer to set.
	 */
	public void setSendToGiftPayer(boolean sendToGiftPayer) {
		this.sendToGiftPayer = sendToGiftPayer;
	}

	/**
	 * @return Returns the landingPage.
	 */
	public String getLandingPage() {
		return this.landingPage;
	}

	/**
	 * @param landingPage
	 *            The landingPage to set.
	 */
	public void setLandingPage(String landingPage) {
		this.landingPage = landingPage;
	}

	/**
	 * @return Returns the attributes.
	 */
	public Collection<PremiumAttributeRecord> getAttributes() {
		if (this.attributes == null) {
			this.attributes = new ArrayList<PremiumAttributeRecord>();
		}
		return this.attributes;
	}

	/**
	 * @param attributes
	 *            The attributes to set.
	 */
	public void setAttributes(Collection<PremiumAttributeRecord> attributes) {
		this.attributes = attributes;
	}

	/**
	 * @return Returns the descriptionText.
	 */
	public String getDescriptionText() {
		return this.descriptionText;
	}

	/**
	 * @param descriptionText
	 *            The descriptionText to set.
	 */
	public void setDescriptionText(String descriptionText) {
		this.descriptionText = descriptionText;
	}

	/**
	 * @return Returns the emailText.
	 */
	public String getEmailText() {
		return this.emailText;
	}

	/**
	 * @param emailText
	 *            The emailText to set.
	 */
	public void setEmailText(String emailText) {
		this.emailText = emailText;
	}

	/**
	 * @return Returns the premiumPromoCode.
	 */
	public String getPremiumPromoCode() {
		return this.premiumPromoCode;
	}

	/**
	 * @param premiumPromoCode
	 *            The premiumPromoCode to set.
	 */
	public void setPremiumPromoCode(String premiumPromoCode) {
		this.premiumPromoCode = premiumPromoCode;
	}

	/**
	 * 
	 * @param par
	 */
	public void addAttribute(PremiumAttributeRecord par) {
		if (par == null) {
			return;
		}

		// if the attribute is a club number then keep the value in
		// this object for easy synching with the subscription transaction
		if (par.isClubNumber()) {
			this.setHasClubNumber(true);
			this.setClubNumber(par.getAttributeValue());
		}
		this.getAttributes().add(par);
	}

	/**
	 * 
	 * @param orderID
	 */
	public void applyOrderID(String orderID) {
		if (orderID == null) {
			return;
		}
		if (this.getAttributes().size() > 0) {
			for (Iterator<PremiumAttributeRecord> iter = this.getAttributes().iterator(); iter.hasNext();) {
				PremiumAttributeRecord element = iter.next();
				try {
					element.setOrderId(orderID);
				} catch (UsatException exp) {
					System.out.println("Failed to apply orderid: " + orderID + " exception: " + exp.getMessage());
				}
			}
		}
	}

	/**
	 * '
	 * 
	 * @param accountNum
	 */
	public void applyAccountNumber(String accountNum) {
		if (accountNum == null) {
			return;
		}
		if (this.getAttributes().size() > 0) {
			for (Iterator<PremiumAttributeRecord> iter = this.getAttributes().iterator(); iter.hasNext();) {
				PremiumAttributeRecord element = iter.next();
				try {
					element.setAccountNum(accountNum);
				} catch (UsatException exp) {
					System.out.println("Failed to apply account number: " + accountNum + " exception: " + exp.getMessage());
				}
			}
		}
	}

	/**
	 * 
	 * @param pubCode
	 */
	public void applyPubcode(String pubCode) {
		if (pubCode == null) {
			return;
		}
		if (this.getAttributes().size() > 0) {
			for (Iterator<PremiumAttributeRecord> iter = this.getAttributes().iterator(); iter.hasNext();) {
				PremiumAttributeRecord element = iter.next();
				try {
					element.setPubCode(pubCode);
				} catch (UsatException exp) {
					System.out.println("Failed to apply pub code: " + pubCode + " exception: " + exp.getMessage());
				}
			}
		}
	}

	/**
	 * 
	 * @param state
	 */
	public void applyState(int state) {
		if (this.getAttributes().size() > 0) {
			for (Iterator<PremiumAttributeRecord> iter = this.getAttributes().iterator(); iter.hasNext();) {
				PremiumAttributeRecord element = iter.next();
				element.setState(state);
			}
		}
	}

	/**
	 * 
	 * @param hasClubNumber
	 */
	public void setHasClubNumber(boolean hasClubNumber) {
		this.hasClubNumber = hasClubNumber;
	}

	/**
	 * 
	 * @return true if one of the attibutes is a club number
	 */
	public boolean hasClubNumber() {
		return this.hasClubNumber;
	}

	/**
	 * 
	 * @return
	 */
	public String getClubNumber() {
		return this.clubNumber;
	}

	/**
	 * 
	 * @param clubNumber
	 */
	public void setClubNumber(String clubNumber) {
		this.clubNumber = clubNumber;
	}
}
