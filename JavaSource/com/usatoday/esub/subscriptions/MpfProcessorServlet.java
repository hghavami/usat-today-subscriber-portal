// @annotations-disabled tagSet="web"
package com.usatoday.esub.subscriptions;

import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.customer.CustomerIntf;
import com.usatoday.business.interfaces.customer.EmailRecordIntf;
import com.usatoday.business.interfaces.customer.SubscriberAccountIntf;
import com.usatoday.business.interfaces.products.SubscriptionOfferIntf;
import com.usatoday.business.interfaces.products.SubscriptionTermsIntf;
import com.usatoday.businessObjects.customer.CustomerBO;
import com.usatoday.businessObjects.customer.EmailRecordBO;
import com.usatoday.businessObjects.customer.SubscriberAccountBO;
import com.usatoday.businessObjects.products.SubscriptionOfferManager;
import com.usatoday.businessObjects.util.UsatDateTimeFormatter;
import com.usatoday.businessObjects.util.mail.EmailAlert;
import com.usatoday.esub.common.UTCommon;
import com.usatoday.esub.ncs.handlers.CustomerHandler;
import com.usatoday.util.constants.UsaTodayConstants;

/**
 * Servlet implementation class for Servlet: MpfProcessor
 * 
 * @web.servlet name="mpfProcessor" display-name="mpfProcessor" description="Processes Landing page forms"
 * 
 * @web.servlet-mapping url-pattern="/subscriptions/premiumProcessor"
 * 
 */
public class MpfProcessorServlet extends javax.servlet.http.HttpServlet implements javax.servlet.Servlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5303162435688801655L;

	/*
	 * (non-Java-doc)
	 * 
	 * @see javax.servlet.http.HttpServlet#HttpServlet()
	 */
	public MpfProcessorServlet() {
		super();
	}

	/*
	 * (non-Java-doc)
	 * 
	 * @see javax.servlet.http.HttpServlet#doGet(HttpServletRequest arg0, HttpServletResponse arg1)
	 */
	protected void doGet(HttpServletRequest arg0, HttpServletResponse arg1) throws ServletException, IOException {
		this.doPost(arg0, arg1);
	}

	/*
	 * (non-Java-doc)
	 * 
	 * @see javax.servlet.http.HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		int numberOfEmailRecords = 0;

		// if first time coming in on a special deal link, forward to the
		// specials jsp
		if (request.getRequestURI().indexOf("/subscriptions/specials") >= 0) {
			response.sendRedirect(response.encodeRedirectURL("/subscriptions/mpf/special.jsp"));
			return;
		} else if (request.getRequestURI().indexOf("/subscriptions/renewalspecials") >= 0) {
			String pubCode = request.getParameter("pub");
			// if link includes account # information
			String acctNum = request.getParameter("AID");
			// if link includes keycode information
			String keycode = request.getParameter("keycode");

			String emailTrackingCode = request.getParameter("ccode");

			// if link includes email information
			String email = null;
			try {
				email = URLDecoder.decode(request.getParameter("e"), "UTF-8");
			} catch (NullPointerException e) {

			}
			if (email != null) {
				// ape added due to issue with Exact Target malformed URL's
				int indexofCCode = email.indexOf("?ccode=");
				if (indexofCCode > -1) {
					// bad ExactTarget formed URL...remove the ccode from email address
					email = email.substring(0, indexofCCode);
				}

				email = email.replace('?', ' ');
				email = email.trim();
				if (acctNum != null && acctNum.length() > 0) {
					if (email.trim().length() > 60) {
						email = email.trim().substring(0, 59);
					}
					numberOfEmailRecords = this.autoLoginByEmailAndAccount(request, acctNum, email);
				}
			} else if (acctNum != null && acctNum.length() > 0) {
				numberOfEmailRecords = this.autoLogin(request, acctNum);
			}

			// if ( (email == null || numberOfEmailRecords == 0) && numberOfEmailRecords != 1) {
			if (email == null || numberOfEmailRecords == 0) {
				// No email in the URL and don't have online access set up
				// set an indicator that they need to be redirect to online setup page.
				session.setAttribute("SETUP_ONLINE_ACCESS", new Boolean(true));
			}
			// Use any keycode present in the URL to generate renewal subscription terms
			session.removeAttribute(UTCommon.SESSION_URL_RENEWAL_OFFER);
			if (keycode != null && !keycode.trim().equals("")) {
				// UTCommon.getCurrentOffer(request);
				SubscriptionOfferIntf offer = null;
				Collection<SubscriptionTermsIntf> renewalTerms = null;
				try {
					offer = SubscriptionOfferManager.getInstance().getOffer(keycode, pubCode);
					if (offer != null) {
						renewalTerms = offer.getRenewalTerms();
						if (renewalTerms != null) {
							session.setAttribute(UTCommon.SESSION_URL_RENEWAL_OFFER, offer);
						}
					}
				} catch (UsatException e) {
					offer = null;
				}
			}
			// Use rtc from URL or default from .ini file sewrtc
			String renewal_tracking_code = request.getParameter(UTCommon.RENEWAL_TRACKING_CODE_QUERYSTRING);
			try {
				// RTC
				if (renewal_tracking_code != null) {
					session.removeAttribute(UTCommon.SESSION_RENEWAL_TRACKING_CODE);
					session.setAttribute(UTCommon.SESSION_RENEWAL_TRACKING_CODE, renewal_tracking_code);
				}
			} catch (Exception e) {
				// ignore
				;
			}

			// forward any Omniture tracking code on to landing page
			if (emailTrackingCode != null && emailTrackingCode.trim().length() > 0) {
				emailTrackingCode = emailTrackingCode.trim();
				String tempEmailCode = emailTrackingCode.toUpperCase();
				// only forward email tracking codes that begin with RTN
				if (tempEmailCode.indexOf("RTN") == 0) {
					emailTrackingCode = "?ccode=" + URLEncoder.encode(emailTrackingCode, "UTF-8");
				} else {
					emailTrackingCode = "";
				}
			} else {
				emailTrackingCode = "";
			}

			response.sendRedirect(response.encodeRedirectURL("/subscriptions/mpf/renewalspecial.jsp" + emailTrackingCode));

			return;
		}
		String typeOfPremium = request.getParameter("mpfOfferType");

		// if in doubt go with new / gift
		if (typeOfPremium == null || !typeOfPremium.equalsIgnoreCase("RENEWAL")) {
			// no longer used for MPF pages .. only renewal specials
			response.sendRedirect(response.encodeRedirectURL("/welcome.jsp"));

			return;
		} else {
			this.processRenewalMPF(request, response);
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	private String[] parseParameter(String source) {
		String[] tokenArray = null;
		if (source != null) {
			StringTokenizer tokenizer = new StringTokenizer(source, "_");
			tokenArray = new String[tokenizer.countTokens()];

			for (int i = 0; i < tokenArray.length; i++) {
				tokenArray[i] = tokenizer.nextToken();
			}
		}

		return tokenArray;
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void processRenewalMPF(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		String referer = request.getHeader("referer");
		boolean suppress4WeekOffer = true;

		String premiumPromoCodeParam = request.getParameter("mpfPremiumPromoCode");
		String pubCodeParam = request.getParameter("mpfPubCode");

		String s4 = request.getParameter("mpfSuppress4WeekOffer");
		if (s4 != null && s4.equalsIgnoreCase("false")) {
			suppress4WeekOffer = false;
		}

		// Use rtc from URL or default from .ini file sewrtc
		String renewal_tracking_code = request.getParameter(UTCommon.RENEWAL_TRACKING_CODE_QUERYSTRING);
		try {
			// RTC
			if (renewal_tracking_code != null) {
				session.removeAttribute(UTCommon.SESSION_RENEWAL_TRACKING_CODE);
				session.setAttribute(UTCommon.SESSION_RENEWAL_TRACKING_CODE, renewal_tracking_code);
			}
		} catch (Exception e) {
			// ignore
			;
		}
		// process mpf attributes
		MultiplePremiumFulfillment mpf = new MultiplePremiumFulfillment();
		mpf.setPremiumPromoCode(premiumPromoCodeParam);

		mpf.setSuppress4WeekOffer(suppress4WeekOffer);

		// save landing page for complete page redirect
		mpf.setLandingPage(referer);

		String descriptiveText = request.getParameter("mpfDescriptionText");
		if (descriptiveText != null) {
			mpf.setDescriptionText(descriptiveText);
		}

		String emailText = request.getParameter("mpfEmailText");
		if (emailText != null) {
			mpf.setEmailText(emailText);
		}

		// gift payer does not apply
		mpf.setSendToGiftPayer(false);

		// iterate over remaining form fields and process as attributes
		Enumeration<String> enumer = (Enumeration<String>) request.getParameterNames();

		while (enumer.hasMoreElements()) {
			String currentName = enumer.nextElement();
			if (currentName.startsWith("mpf")) {
				// skip the non attribute fields
				continue;
			}

			PremiumAttributeRecord par = new PremiumAttributeRecord();

			try {
				par.setPremiumPromotionCode(premiumPromoCodeParam);

				par.setAttributeValue(request.getParameter(currentName));

				String[] tokens = this.parseParameter(currentName);
				if (tokens == null || tokens.length != 3) {
					session.setAttribute("MSG", "INVALID MPF FORM SUBMITTED: Field (" + currentName
							+ ") Should be in format of [PremiumAttributeName_PremCode_PremType]");
					response.sendRedirect(response.encodeRedirectURL(referer + "?error=505&msg=INVALID MPF FORM SUBMITTED: Field ("
							+ currentName + ") Should be in format of [PremiumAttributeName_PremCode_PremType]"));
					return;
				}

				par.setAttribuuteName(tokens[0]);
				par.setPremiumCode(tokens[1]);
				par.setPremiumType(tokens[2]);

				par.setState(PremiumAttributeRecord.NEW);

				mpf.addAttribute(par);

			} catch (Exception e) {
				System.out.println("MpfProcessorServlet::doPost(): Exception creating attributes: " + e.getMessage());
				e.printStackTrace();
				session.setAttribute("MSG", "Error: " + e.getMessage());
			}
		} // end while more paramaters

		session.setAttribute("MPF", mpf);

		try {
			// forward to next page
			Boolean forwardToOnlineAccoutSetup = (Boolean) session.getAttribute("SETUP_ONLINE_ACCESS");
			if (forwardToOnlineAccoutSetup == null) {
				forwardToOnlineAccoutSetup = new Boolean(false);
			} else {
				session.removeAttribute("SETUP_ONLINE_ACCESS");
			}

			if (forwardToOnlineAccoutSetup.booleanValue()) {
				UTCommon.showUrl(response, "/firsttime/first_time.jsp");
			} else if (pubCodeParam == null) {
				UTCommon.showUrl(response, "/subscriptions/renewals/renew.faces");
			} else if (pubCodeParam.equalsIgnoreCase("UT")) {
				UTCommon.showUrl(response, "/custserviceindex.jsp?pub=UT&custopt=RENEW");
			} else {
				UTCommon.showUrl(response, "/custserviceindex.jsp?pub=BW&custopt=RENEW");
			}
		} catch (Exception e) {
			System.out.println("Forwarding failed: " + e.getMessage());
		}
	}

	/**
	 * 
	 * @param request
	 * @param accountNum
	 * @return
	 */
	private int autoLogin(HttpServletRequest request, String accountNum) {
		// hd-cons92 sew
		// accountNum = "34" + accountNum;

		String pubCode = request.getParameter("pub");

		Collection<EmailRecordIntf> c = null;

		int numEmailRecords = 0;

		HttpSession session = request.getSession();
		// Use rtc from URL or default from .ini file sewrtc
		String renewal_tracking_code = request.getParameter(UTCommon.RENEWAL_TRACKING_CODE_QUERYSTRING);
		try {
			// RTC
			if (renewal_tracking_code != null) {
				session.removeAttribute(UTCommon.SESSION_RENEWAL_TRACKING_CODE);
				session.setAttribute(UTCommon.SESSION_RENEWAL_TRACKING_CODE, renewal_tracking_code);
			}
		} catch (Exception e) {
			// ignore
			;
		}
		try {
			if (pubCode != null) {
				c = EmailRecordBO.getEmailRecordsForAccountPub(pubCode, accountNum);
			} else {
				c = EmailRecordBO.getEmailRecordsForAccount(accountNum);
			}

			numEmailRecords = c.size();

			// until we get ExactTarget lookups working, we can only log in if
			// one account returned.
			if (c.size() == 1) {
				EmailRecordIntf emailRec = c.iterator().next();
				CustomerIntf customer = CustomerBO.loginCustomer(emailRec.getEmailAddress(), emailRec.getPassword());
				customer.setCurrentAccount(emailRec.getAccountNumber());
				session.setAttribute(com.usatoday.esub.common.UTCommon.SESSION_CUSTOMER_INFO, customer);

				session.setAttribute(UTCommon.SESSION_PUB_NAME, emailRec.getPubCode());
			}
		} catch (Exception e) {
			// if an error occurs simply move on without logging in
			return numEmailRecords;
		}
		return numEmailRecords;
	}

	/**
	 * 
	 * @param request
	 * @param accountNum
	 * @return
	 */
	private int autoLoginByEmailAndAccount(HttpServletRequest request, String accountNum, String email) {

		String pubcode = request.getParameter("pub");

		// hd-cons92 sew
		/*
		 * if (accountNum.length() == 7) { accountNum = "34" + accountNum; }
		 */

		Collection<EmailRecordIntf> c = null;
		CustomerIntf customer = null;

		int numEmailRecords = 0;

		HttpSession session = request.getSession();

		// Use rtc from URL or default from .ini file sewrtc
		String renewal_tracking_code = request.getParameter(UTCommon.RENEWAL_TRACKING_CODE_QUERYSTRING);
		// set customer handler object
		CustomerHandler ch = null;
		try {
			ch = (CustomerHandler) session.getAttribute("customerHandler");
			if (ch == null) {
				ch = new CustomerHandler();
				ch.setAuthenticated(true);
				session.setAttribute("customerHandler", ch);
			}
			// RTC
			if (renewal_tracking_code != null) {
				session.removeAttribute(UTCommon.SESSION_RENEWAL_TRACKING_CODE);
				session.setAttribute(UTCommon.SESSION_RENEWAL_TRACKING_CODE, renewal_tracking_code);
			}
		} catch (Exception e) {
			// ignore
			;
		}
		try {
			if (pubcode != null) {
				// Check if one with the same email address and account number exists
				c = EmailRecordBO.getEmailRecordsForPubEmailAccount(pubcode, email, accountNum);
			} else {
				c = EmailRecordBO.getEmailRecordsForEmailAccount(email, accountNum);
			}
			// No email address and account exists
			if (c.size() == 0) {
				// Check email file for account and pub alone
				if (pubcode != null) {
					c = EmailRecordBO.getEmailRecordsForAccountPub(pubcode, accountNum);
				} else {
					c = EmailRecordBO.getEmailRecordsForAccount(accountNum);
				}
				numEmailRecords = c.size();
				if (c.size() == 0) {
					// No record for this email address exists, add 1
					SubscriberAccountIntf subscriberAccount = SubscriberAccountBO.getSubscriberAccount(accountNum);
					// Set up and add new email record for the same account with the new email address and transaction type
					String startDateStr = "";
					try {
						startDateStr = UTCommon.getSystemDateYYYYMMDD();
					} catch (Exception e) {
						startDateStr = "";
					}

					EmailRecordIntf emailRecord = new EmailRecordBO(email, "yagtuc", -1, null, accountNum, null, pubcode, true,
							false, "Y", startDateStr,
							subscriberAccount.getDeliveryContact().getPersistentAddress().getZip().trim(), "", subscriberAccount
									.getDeliveryContact().getFirstName(), subscriberAccount.getDeliveryContact().getLastName(), "", "");
					try {
						EmailRecordBO emailRecordBO = new EmailRecordBO(emailRecord);
						emailRecordBO.save();
					} catch (Exception e) {
					}
					ch.setAuthenticated(false);
					customer = CustomerBO.loginCustomerEmailAccountPub(email, accountNum, pubcode);
				}
				if (c.size() == 1) {
					// A record with different email address exists, add a new one with URL's email and account number
					EmailRecordIntf emailRec = c.iterator().next();
					// Set transaction type base on existing email record for the URL email address
					SubscriberAccountIntf subscriberAccount = SubscriberAccountBO.getSubscriberAccount(accountNum);
					// Set up and add new email record for the same account with the new email address and transaction type
					String startDateStr = "";
					try {
						startDateStr = UTCommon.getSystemDateYYYYMMDD();
					} catch (Exception e) {
						startDateStr = "";
					}

					EmailRecordIntf emailRecord = new EmailRecordBO(email, "yagtuc", -1, null, accountNum, null, pubcode, true,
							!emailRec.isGift(), "Y", startDateStr, subscriberAccount.getDeliveryContact().getPersistentAddress()
									.getZip().trim(), "", subscriberAccount.getDeliveryContact().getFirstName(), subscriberAccount
									.getDeliveryContact().getLastName(), "", "");
					try {
						EmailRecordBO emailRecordBO = new EmailRecordBO(emailRecord);
						emailRecordBO.save();
					} catch (Exception e) {
						StringBuffer bodyText = new StringBuffer();
						try {
							EmailAlert alert = new EmailAlert();
							alert.setSender(UsaTodayConstants.EMAIL_DEFAULT_FROM_ADDRESS);
							alert.setSubject("Subscription Order Entry Alert - FAILED to Save Email Record to XTRNTEMAIL");
							bodyText.append("Exception: " + e.getMessage());
							bodyText.append("\n \nEmail record should be added manually.");
							bodyText.append("\n \nPubCode: " + emailRecord.getPubCode());
							bodyText.append("\nEmail Address: " + emailRecord.getEmailAddress());
							bodyText.append("\n:Order/WebID: " + emailRecord.getOrderID());
							bodyText.append("\nPerm Start Date: " + emailRecord.getPermStartDate());
							bodyText.append("\nZip: " + emailRecord.getZip());
							bodyText.append("\nPassword: use blank password");
							bodyText.append("\nDateUpdated: " + UsatDateTimeFormatter.getCurrentDateInYYYYMMDDFormat());
							bodyText.append("\nTimeUpdated: " + UsatDateTimeFormatter.getSystemTimeHHMMSS());

							alert.setBodyText(bodyText.toString());
							alert.setReceiverList(UsaTodayConstants.EMAIL_ALERT_RECEIVER_EMAILS);
							alert.sendAlert();
						} catch (Exception exp) {
							System.out.println("Failed to send alert: " + bodyText.toString());
						}
					}
					ch.setAuthenticated(false);
					customer = CustomerBO.loginCustomerEmailAccountPub(email, accountNum, pubcode);
				}
				if (c.size() > 1) {
					// Multiple records with different email addresses exist, create a customer account info session object
					// Do not log customer in
					HashMap<String, EmailRecordIntf> emailRecordMap = new HashMap<String, EmailRecordIntf>();
					HashMap<String, SubscriberAccountIntf> accountMap = new HashMap<String, SubscriberAccountIntf>();
					SubscriberAccountIntf subscriberAccount = SubscriberAccountBO.getSubscriberAccount(accountNum);
					// Set up and add new inactive email record for the same account with the new email address and transaction type
					String startDateStr = "";
					try {
						startDateStr = UTCommon.getSystemDateYYYYMMDD();
					} catch (Exception e) {
						startDateStr = "";
					}

					EmailRecordBO emailRecord = new EmailRecordBO(email, "", -1, null, accountNum, null, pubcode, false, false,
							"Y", startDateStr, subscriberAccount.getDeliveryContact().getPersistentAddress().getZip().trim(), "",
							subscriberAccount.getDeliveryContact().getFirstName(), subscriberAccount.getDeliveryContact()
									.getLastName(), "", "");
					// Fill in the email and account information in the customer object
					emailRecordMap.put(emailRecord.getAccountNumber(), emailRecord);
					accountMap.put(emailRecord.getAccountNumber(), subscriberAccount);
					customer = new CustomerBO(accountMap, emailRecordMap);
					ch.setAuthenticated(false);
					/*
					 * try { EmailRecordBO.peristNewEmailRecord(emailRecord); } catch (Exception e) { }
					 */}
			} else {
				numEmailRecords = c.size();
				c.iterator().next();
				customer = CustomerBO.loginCustomerEmailAccountPub(email, accountNum, pubcode);
			}

			session.removeAttribute(com.usatoday.esub.common.UTCommon.SESSION_CUSTOMER_INFO);
			session.setAttribute(com.usatoday.esub.common.UTCommon.SESSION_CUSTOMER_INFO, customer);
			// Save customer handler session object
			ch.setRememberMe(false);
			ch.setUserEnteredPwd("");
			ch.setUserEnteredUserID(email);
			ch.setCustomer(customer);
			ch.setCookieAuthenticated(false);

			session.removeAttribute("customerHandler");
			session.setAttribute("customerHandler", ch);

			session.removeAttribute(UTCommon.SESSION_PUB_NAME);
			session.setAttribute(UTCommon.SESSION_PUB_NAME, pubcode);
		} catch (Exception e) {
			// if an error occurs simply move on without logging in
			return numEmailRecords;
		}
		return numEmailRecords;
	}
}