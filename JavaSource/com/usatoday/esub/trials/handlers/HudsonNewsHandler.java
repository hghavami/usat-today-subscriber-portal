package com.usatoday.esub.trials.handlers;

public class HudsonNewsHandler {

	private String accesscode = "";

	public String getAccesscode() {
		return accesscode;
	}

	public void setAccesscode(String accesscode) {
		this.accesscode = accesscode;
	}
}
