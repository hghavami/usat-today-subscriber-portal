package com.usatoday.esub.trials.handlers;

import org.joda.time.DateTime;

import com.usatoday.business.interfaces.customer.TrialInstanceIntf;
import com.usatoday.business.interfaces.partners.TrialPartnerIntf;
import com.usatoday.businessObjects.customer.ContactBO;
import com.usatoday.businessObjects.customer.CustomerBO;
import com.usatoday.businessObjects.customer.TrialInstanceBO;
import com.usatoday.businessObjects.customer.UIAddressBO;

public class NewTrialOrderHandler {

	private String partnerID = "";
	private String partnerClubNumber = null;
	private String deliveryEmailAddress = null;
	private String deliveryEmailAddressConfirmation = null;
	private String deliveryFirstName = null;
	private String deliveryLastName = null;
	private String deliveryAddress1 = null;
	private String deliveryAddress2 = "";
	private String deliveryAptSuite = null;
	private String deliveryCity = null;
	private String deliveryState = null;
	private String deliveryZipCode = null;
	private String deliveryPhoneAreaCode = null;
	private String deliveryPhoneExchange = null;
	private String deliveryPhoneExtension = null;
	private String password = null;
	private String passwordConfirmation = null;
	private String ncsRepID = "";

	public String getPartnerID() {
		return partnerID;
	}

	public void setPartnerID(String partnerID) {
		this.partnerID = partnerID;
	}

	public String getPartnerClubNumber() {
		return partnerClubNumber;
	}

	public void setPartnerClubNumber(String partnerClubNumber) {
		this.partnerClubNumber = partnerClubNumber;
	}

	public String getDeliveryEmailAddress() {
		return deliveryEmailAddress;
	}

	public void setDeliveryEmailAddress(String deliveryEmailAddress) {
		this.deliveryEmailAddress = deliveryEmailAddress;
	}

	public String getDeliveryEmailAddressConfirmation() {
		return deliveryEmailAddressConfirmation;
	}

	public void setDeliveryEmailAddressConfirmation(String deliveryEmailAddressConfirmation) {
		this.deliveryEmailAddressConfirmation = deliveryEmailAddressConfirmation;
	}

	public String getDeliveryFirstName() {
		return deliveryFirstName;
	}

	public void setDeliveryFirstName(String deliveryFirstName) {
		this.deliveryFirstName = deliveryFirstName;
	}

	public String getDeliveryLastName() {
		return deliveryLastName;
	}

	public void setDeliveryLastName(String deliveryLastName) {
		this.deliveryLastName = deliveryLastName;
	}

	public String getDeliveryAddress1() {
		return deliveryAddress1;
	}

	public void setDeliveryAddress1(String deliveryAddress1) {
		this.deliveryAddress1 = deliveryAddress1;
	}

	public String getDeliveryAddress2() {
		return deliveryAddress2;
	}

	public void setDeliveryAddress2(String deliveryAddress2) {
		this.deliveryAddress2 = deliveryAddress2;
	}

	public String getDeliveryAptSuite() {
		return deliveryAptSuite;
	}

	public void setDeliveryAptSuite(String deliveryAptSuite) {
		this.deliveryAptSuite = deliveryAptSuite;
	}

	public String getDeliveryCity() {
		return deliveryCity;
	}

	public void setDeliveryCity(String deliveryCity) {
		this.deliveryCity = deliveryCity;
	}

	public String getDeliveryState() {
		return deliveryState;
	}

	public void setDeliveryState(String deliveryState) {
		this.deliveryState = deliveryState;
	}

	public String getDeliveryZipCode() {
		return deliveryZipCode;
	}

	public void setDeliveryZipCode(String deliveryZipCode) {
		this.deliveryZipCode = deliveryZipCode;
	}

	public String getDeliveryPhoneAreaCode() {
		return deliveryPhoneAreaCode;
	}

	public void setDeliveryPhoneAreaCode(String deliveryPhoneAreaCode) {
		this.deliveryPhoneAreaCode = deliveryPhoneAreaCode;
	}

	public String getDeliveryPhoneExchange() {
		return deliveryPhoneExchange;
	}

	public void setDeliveryPhoneExchange(String deliveryPhoneExchange) {
		this.deliveryPhoneExchange = deliveryPhoneExchange;
	}

	public String getDeliveryPhoneExtension() {
		return deliveryPhoneExtension;
	}

	public void setDeliveryPhoneExtension(String deliveryPhoneExtension) {
		this.deliveryPhoneExtension = deliveryPhoneExtension;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordConfirmation() {
		return passwordConfirmation;
	}

	public void setPasswordConfirmation(String passwordConfirmation) {
		this.passwordConfirmation = passwordConfirmation;
	}

	public TrialInstanceIntf generatedNewTrialCustomerFromFields(TrialPartnerIntf partner) throws Exception {
		TrialInstanceBO newTrialCustomer = new TrialInstanceBO();

		newTrialCustomer.setClubNumber(this.partnerClubNumber);

		newTrialCustomer.setNcsRepID(this.ncsRepID);

		ContactBO contact = null;
		contact = new ContactBO();
		contact.setBusinessPhone("");
		contact.setHomePhone(this.getDeliveryPhoneAreaCode() + this.getDeliveryPhoneExchange() + this.getDeliveryPhoneExtension());

		contact.setEmailAddress(this.getDeliveryEmailAddress());
		contact.setFirstName(this.getDeliveryFirstName());
		contact.setLastName(this.getDeliveryLastName());

		if (this.getPassword() == null) {
			this.setPassword(CustomerBO.generateNewPassword());
		}
		contact.setPassword(this.getPassword());

		UIAddressBO address = new UIAddressBO();
		address.setAddress1(this.getDeliveryAddress1());
		address.setAddress2(this.getDeliveryAddress2());
		address.setAptSuite(this.getDeliveryAptSuite());
		address.setCity(this.getDeliveryCity());
		address.setState(this.getDeliveryState());
		address.setZip(this.getDeliveryZipCode());
		address.setValidated(true);

		contact.setUIAddress(address);

		newTrialCustomer.setContactInformation(contact);

		// The partner should be set up prior to reaching this page but may not be.
		// Custom landing pages wil use a hard coded partner id value.

		if (partner == null) {
			throw new Exception("NULL Partner trying to set up a new trial.");
		}

		newTrialCustomer.setPubCode(partner.getPubCode());
		newTrialCustomer.setKeyCode(partner.getKeyCode());

		// perform partner validations.

		newTrialCustomer.setPartner(partner);

		DateTime start = new DateTime();
		DateTime end = new DateTime();
		end = end.plusDays(partner.getDurationInDays());

		newTrialCustomer.setStartDate(start);
		newTrialCustomer.setEndDate(end);

		return newTrialCustomer;
	}

	/**
	 * 
	 * @return true if email and confirm email are the same and non null
	 */
	public boolean getIsEmailAndConfirmEmailSame() {
		boolean theSame = false;
		if (this.deliveryEmailAddress != null && this.deliveryEmailAddressConfirmation != null) {
			this.deliveryEmailAddress = this.deliveryEmailAddress.trim();
			this.deliveryEmailAddressConfirmation = this.deliveryEmailAddressConfirmation.trim();

			if (this.deliveryEmailAddress.equalsIgnoreCase(this.deliveryEmailAddressConfirmation)) {
				theSame = true;
			}

		}
		return theSame;
	}

	/**
	 * 
	 * @return true if the password and confirm password are the same and non null.
	 */
	public boolean getIsPasswordAndConfirmPasswordSame() {
		boolean theSame = false;
		if (this.password != null && this.passwordConfirmation != null) {
			this.password = this.password.trim();
			this.passwordConfirmation = this.passwordConfirmation.trim();

			if (this.password.equalsIgnoreCase(this.passwordConfirmation)) {
				theSame = true;
			}

		}
		return theSame;
	}

	public boolean getIsEmailValid() {
		String e1 = this.deliveryEmailAddress;

		boolean validDeliveryEmail = true;

		int ampIndex = e1.indexOf('@');

		if (ampIndex == -1) {
			validDeliveryEmail = false;
		} else {
			e1 = e1.substring(ampIndex + 1);
			int dotIndex = e1.indexOf('.');

			if (dotIndex == -1) {
				validDeliveryEmail = false;
			}

		}
		return validDeliveryEmail;
	}

	public String getNcsRepID() {
		return ncsRepID;
	}

	public void setNcsRepID(String ncsRepID) {
		this.ncsRepID = ncsRepID;
	}
}
