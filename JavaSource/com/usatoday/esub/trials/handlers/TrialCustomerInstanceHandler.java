package com.usatoday.esub.trials.handlers;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.joda.time.DateTime;

import com.usatoday.business.interfaces.customer.TrialInstanceIntf;
import com.usatoday.business.interfaces.partners.TrialPartnerIntf;
import com.usatoday.business.interfaces.products.SubscriptionProductIntf;
import com.usatoday.businessObjects.products.SubscriptionProductBO;
import com.usatoday.businessObjects.products.utils.ElectronicEditionFulfillmentHelper;
import com.usatoday.util.MobileDeviceDetectionUtil;

public class TrialCustomerInstanceHandler {

	private TrialInstanceIntf trial = null;

	public TrialInstanceIntf getTrial() {
		return trial;
	}

	public void setTrial(TrialInstanceIntf trial) {
		this.trial = trial;
	}

	public java.util.Date getStartDate() {
		if (this.trial != null) {
			return this.trial.getStartDate().toDate();
		}
		return null;
	}

	public java.util.Date getEndDate() {
		if (this.trial != null) {
			return this.trial.getEndDate().toDate();
		}
		return null;
	}

	public SubscriptionProductIntf getProduct() {

		SubscriptionProductIntf prod = null;

		if (this.trial != null) {
			String pubCode = this.trial.getPubCode();
			try {
				prod = SubscriptionProductBO.getSubscriptionProduct(pubCode);
			} catch (Exception e) {
				;
			}
		}

		return prod;
	}

	public String getTrialDurationInDays() {
		String duration = null;
		if (this.trial != null) {
			TrialPartnerIntf partner = this.trial.getPartner();
			duration = String.valueOf(partner.getDurationInDays());
		}
		return duration;
	}

	public String getElectronicReadNowURL() {
		String eEditionLink = null;

		SubscriptionProductIntf prod = null;

		if (this.trial != null) {
			boolean useAlternate = false;

			String pubCode = this.trial.getPubCode();
			try {
				prod = SubscriptionProductBO.getSubscriptionProduct(pubCode);
				eEditionLink = ElectronicEditionFulfillmentHelper.buildOliveFulfillmentURL(prod, this.trial.getContactInformation()
						.getEmailAddress(), null, null, useAlternate);
			} catch (Exception e) {
				;
			}
		}

		return eEditionLink;
	}

	public String getAlternateElectronicReadNowURL() {
		String eEditionLink = null;

		SubscriptionProductIntf prod = null;

		if (this.trial != null) {
			boolean useAlternate = true;

			String pubCode = this.trial.getPubCode();
			try {
				prod = SubscriptionProductBO.getSubscriptionProduct(pubCode);
				eEditionLink = ElectronicEditionFulfillmentHelper.buildOliveFulfillmentURL(prod, this.trial.getContactInformation()
						.getEmailAddress(), null, null, useAlternate);
			} catch (Exception e) {
				;
			}
		}

		return eEditionLink;
	}

	public String getDefaultElectronicReadNowURL() {
		String eEditionLink = null;

		SubscriptionProductIntf prod = null;

		HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();

		if (this.trial != null) {
			boolean useAlternate = MobileDeviceDetectionUtil.isMobileDevice(req);

			String pubCode = this.trial.getPubCode();
			try {
				prod = SubscriptionProductBO.getSubscriptionProduct(pubCode);
				eEditionLink = ElectronicEditionFulfillmentHelper.buildOliveFulfillmentURL(prod, this.trial.getContactInformation()
						.getEmailAddress(), null, null, useAlternate);
			} catch (Exception e) {
				;
			}
		}

		return eEditionLink;
	}

	public String getDaysRemainingString() {
		String daysLeft = "0";

		if (this.trial != null) {
			DateTime now = new DateTime();
			DateTime endDate = this.trial.getEndDate();

			if (endDate != null) {
				int numDays = 0;
				if (now.isBefore(endDate)) {
					while (now.isBefore(endDate)) {
						numDays++;
						now = now.plusDays(1);
					}
				}
				if (numDays < 1) {
					daysLeft = "Less than 1";

				} else {
					daysLeft = String.valueOf(numDays);
				}
			} else {
				daysLeft = "No End Date";
			}
		}
		return daysLeft;
	}

	public boolean getIsShowClubNumber() {
		boolean showClubNumber = false;
		if (this.trial != null) {
			try {
				if (this.trial.getPartner().getIsShowClubNumber()) {
					showClubNumber = true;
				}
			} catch (Exception e) {
				System.out.println("Failed to determine if should show club number: " + e.getMessage() + " :  Trial Cust ID: "
						+ this.trial.getID());
			}
		}
		return showClubNumber;
	}

	public String getClubNumberLabel() {
		String label = "";
		if (this.trial != null) {
			try {
				if (this.trial.getPartner().getClubNumberLabel() != null) {
					label = this.trial.getPartner().getClubNumberLabel();
				}
			} catch (Exception e) {
				//
				label = "";
			}
		}
		return label;
	}

	public void setIsShowClubNumber(boolean showIt) {
		; // ignore
	}

}
