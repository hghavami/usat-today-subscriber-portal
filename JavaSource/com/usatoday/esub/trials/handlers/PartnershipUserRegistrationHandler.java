package com.usatoday.esub.trials.handlers;

import java.sql.Timestamp;

public class PartnershipUserRegistrationHandler {

	private String email_address = "";
	private String password = "";
	private Timestamp start_date = null;
	private Timestamp end_date = null;
	private Timestamp insert_time_stamp = null;
	private String first_Name = "";
	private String last_name = "";
	private String company_name = "";
	private String address1 = "";
	private String address2 = "";
	private String apt_suite = "";
	private String city = "";
	private String state = "";
	private String zip = "";
	private String phone = "";
	private String ip_address = "";
	private String partner_id = "ABCD1001";
	private String partner_number = "";

	public String getAddress1() {
		return address1;
	}

	public String getAddress2() {
		return address2;
	}

	public String getApt_suite() {
		return apt_suite;
	}

	public String getCity() {
		return city;
	}

	public String getCompany_name() {
		return company_name;
	}

	public String getEmail_address() {
		return email_address;
	}

	public Timestamp getEnd_date() {
		return end_date;
	}

	public String getFirst_Name() {
		return first_Name;
	}

	public Timestamp getInsert_time_stamp() {
		this.insert_time_stamp = new Timestamp(System.currentTimeMillis());
		return insert_time_stamp;
	}

	public String getIp_address() {
		return ip_address;
	}

	public String getLast_name() {
		return last_name;
	}

	public String getPartner_id() {
		return partner_id;
	}

	public String getPartner_number() {
		return partner_number;
	}

	public String getPassword() {
		return password;
	}

	public String getPhone() {
		return phone;
	}

	public Timestamp getStart_date() {
		this.start_date = new Timestamp(System.currentTimeMillis());
		return start_date;
	}

	public String getState() {
		return state;
	}

	public String getZip() {
		return zip;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public void setApt_suite(String apt_suite) {
		this.apt_suite = apt_suite;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setCompany_name(String company_name) {
		this.company_name = company_name;
	}

	public void setEmail_address(String email_address) {
		this.email_address = email_address;
	}

	public void setEnd_date(Timestamp end_date) {
		this.end_date = end_date;
	}

	public void setFirst_Name(String first_Name) {
		this.first_Name = first_Name;
	}

	public void setInsert_time_stamp(Timestamp insert_time_stamp) {
		this.insert_time_stamp = insert_time_stamp;
	}

	public void setIp_address(String ip_address) {
		this.ip_address = ip_address;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public void setPartner_id(String partner_id) {
		this.partner_id = partner_id;
	}

	public void setPartner_number(String partner_number) {
		this.partner_number = partner_number;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setStart_date(Timestamp start_date) {
		this.start_date = start_date;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}
}
