package com.usatoday.esub.trials.handlers;

import java.net.URLEncoder;

import com.usatoday.business.interfaces.customer.TrialCustomerIntf;

public class TrialCustomerHandler {

	private TrialCustomerIntf trialCustomer = null;

	private String earlySubscribeKeycode = null;

	private String partnerID = null;

	public TrialCustomerIntf getTrialCustomer() {
		return trialCustomer;
	}

	public void setTrialCustomer(TrialCustomerIntf trialCustomer) {
		this.trialCustomer = trialCustomer;
	}

	public String getExactTargetEmailPreferenceLink() {
		String etLink = "#";
		try {

			etLink = "http://pages.exacttarget.com/page.aspx?QS=3935619f7de112ef775078742a3b88d821295ee4dbc792deb89bccf5ead4c967&email="
					+ URLEncoder.encode(this.trialCustomer.getCurrentInstance().getContactInformation().getEmailAddress(), "UTF8");
		} catch (Exception e) {
		}
		return etLink;
	}

	public String getSubscribeOfferLink() {
		String url = "";
		if (this.earlySubscribeKeycode != null && this.earlySubscribeKeycode.length() == 5 && this.trialCustomer != null) {
			// use early keycode
			url = "/subscriptions/electronic/orderEntry.faces?pub=" + this.trialCustomer.getCurrentInstance().getPubCode()
					+ "&keycode=" + this.earlySubscribeKeycode;
		} else {
			if (this.trialCustomer != null) {
				// use default tied to customer
				url = "/subscriptions/electronic/orderEntry.faces?pub=" + this.trialCustomer.getCurrentInstance().getPubCode()
						+ "&keycode=" + this.trialCustomer.getCurrentInstance().getKeyCode();
			} else {
				// use basic link
				url = "/subscriptions/electronic/orderEntry.faces";
			}
		}
		return url;
	}

	public String getEarlySubscribeKeycode() {
		return earlySubscribeKeycode;
	}

	public void setEarlySubscribeKeycode(String earlySubscribeKeycode) {
		this.earlySubscribeKeycode = earlySubscribeKeycode;
	}

	public String getSubscribeKeycode() {
		if (this.earlySubscribeKeycode != null && this.earlySubscribeKeycode.length() == 5) {
			return this.earlySubscribeKeycode;
		} else {
			if (this.trialCustomer != null) {
				return this.trialCustomer.getCurrentInstance().getKeyCode();
			}
		}
		return "";
	}

	public String getPartnerID() {
		return partnerID;
	}

	public void setPartnerID(String partnerID) {
		this.partnerID = partnerID;
	}

	public TrialCustomerInstanceHandler getCurrentInstance() {
		TrialCustomerInstanceHandler tcih = null;
		tcih = new TrialCustomerInstanceHandler();

		if (this.trialCustomer != null) {
			if (this.trialCustomer.getCurrentInstance() != null) {
				tcih.setTrial(this.trialCustomer.getCurrentInstance());
			}
		}

		return tcih;
	}

	public boolean getIsTrialConversion() {
		boolean converting = false;
		if (this.trialCustomer != null) {
			if (this.trialCustomer.getCurrentInstance() != null) {
				converting = true;
			}
		}
		return converting;
	}
}
