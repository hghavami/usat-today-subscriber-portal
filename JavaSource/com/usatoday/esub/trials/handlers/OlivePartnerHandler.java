package com.usatoday.esub.trials.handlers;

public class OlivePartnerHandler {

	private String partnerId = "ABCD1001";

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
}
