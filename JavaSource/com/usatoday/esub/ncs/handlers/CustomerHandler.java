package com.usatoday.esub.ncs.handlers;

import java.net.URLEncoder;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import com.usatoday.business.interfaces.customer.CustomerIntf;
import com.usatoday.business.interfaces.customer.SubscriberAccountIntf;
import com.usatoday.business.interfaces.products.ProductIntf;
import com.usatoday.business.interfaces.products.SubscriptionOfferIntf;
import com.usatoday.businessObjects.products.SubscriptionOfferManager;
import com.usatoday.businessObjects.products.SubscriptionProductBO;
import com.usatoday.businessObjects.products.utils.ElectronicEditionFulfillmentHelper;
import com.usatoday.esub.cookies.PreferencesCookieProcessor;
import com.usatoday.esub.handlers.SubscriptionOfferHandler;
import com.usatoday.util.MobileDeviceDetectionUtil;
import com.usatoday.util.constants.UsaTodayConstants;

public class CustomerHandler {
	private CustomerIntf customer = null;
	private boolean authenticated = false; // set to true if user is authenticated
	private boolean cookieAuthenticated = false; // set to true if authenticated via cookie

	// user entered credentials
	private String userEnteredUserID = null;
	private String userEnteredPwd = null;

	private boolean rememberMe = true;

	// current account handler
	SubscriberAccountHandler currentAccountHandler = null;

	public CustomerIntf getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerIntf customer) {
		this.customer = customer;
	}

	public boolean isAuthenticated() {
		return authenticated;
	}

	public void setAuthenticated(boolean authenticated) {
		this.authenticated = authenticated;
	}

	public String getUserEnteredUserID() {
		return userEnteredUserID;
	}

	public void setUserEnteredUserID(String userEnteredUserID) {
		this.userEnteredUserID = userEnteredUserID;
	}

	public String getUserEnteredPwd() {
		return userEnteredPwd;
	}

	public void setUserEnteredPwd(String userEnteredPwd) {
		this.userEnteredPwd = userEnteredPwd;
	}

	public boolean isRememberMe() {
		return rememberMe;
	}

	public void setRememberMe(boolean rememberMe) {
		this.rememberMe = rememberMe;
	}

	public boolean isCookieAuthenticated() {
		return cookieAuthenticated;
	}

	public void setCookieAuthenticated(boolean cookieAuthenticated) {
		this.cookieAuthenticated = cookieAuthenticated;
	}

	public boolean isCustomerDataAvailable() {
		boolean dataAvailable = false;

		if (this.customer != null && this.authenticated == true) {
			dataAvailable = true;
		}

		return dataAvailable;
	}

	public String getUTeReaderLink() {
		String eEditionLink = "";

		// HttpServletRequest req = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();

		if (this.customer != null && this.authenticated == true) {
			try {

				// boolean useAlternate = MobileDeviceDetectionUtil.isMobileDevice(req);
				boolean useAlternate = false;
				ProductIntf p = SubscriptionProductBO.getSubscriptionProduct(UsaTodayConstants.DEFAULT_UT_EE_PUBCODE);

				eEditionLink = ElectronicEditionFulfillmentHelper.buildOliveFulfillmentURL(p, this.customer.getCurrentEmailRecord()
						.getEmailAddress(), null, null, useAlternate);

			} catch (Exception e) {
				eEditionLink = "";
			}

		}
		return eEditionLink;
	}

	public String getDefaultUTeReaderLink() {
		String eEditionLink = "";

		HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();

		if (this.customer != null && this.authenticated == true) {
			try {

				boolean useAlternate = MobileDeviceDetectionUtil.isMobileDevice(req);

				ProductIntf p = SubscriptionProductBO.getSubscriptionProduct(UsaTodayConstants.DEFAULT_UT_EE_PUBCODE);

				eEditionLink = ElectronicEditionFulfillmentHelper.buildOliveFulfillmentURL(p, this.customer.getCurrentEmailRecord()
						.getEmailAddress(), null, null, useAlternate);

			} catch (Exception e) {
				eEditionLink = "";
			}

		}
		return eEditionLink;
	}

	public String getAlternateUTeReaderLink() {
		String eEditionLink = "";

		if (this.customer != null && this.authenticated == true) {
			try {

				boolean useAlternate = true;
				ProductIntf p = SubscriptionProductBO.getSubscriptionProduct(UsaTodayConstants.DEFAULT_UT_EE_PUBCODE);

				eEditionLink = ElectronicEditionFulfillmentHelper.buildOliveFulfillmentURL(p, this.customer.getCurrentEmailRecord()
						.getEmailAddress(), null, null, useAlternate);

			} catch (Exception e) {
				eEditionLink = "";
			}

		}
		return eEditionLink;
	}

	public String getSavedPreferenceUTeReaderLink() {
		String eEditionLink = "";

		HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();

		if (this.customer != null && this.authenticated == true) {
			try {
				PreferencesCookieProcessor pc = new PreferencesCookieProcessor();
				javax.servlet.http.Cookie c = pc.returnCookie(req.getCookies());
				boolean useAlternate = false;
				if (c == null) {
					useAlternate = MobileDeviceDetectionUtil.isMobileDevice(req);
				} else {
					if (pc.prefersHTML(c)) {
						useAlternate = true;
					}
				}
				ProductIntf p = SubscriptionProductBO.getSubscriptionProduct(UsaTodayConstants.DEFAULT_UT_EE_PUBCODE);

				eEditionLink = ElectronicEditionFulfillmentHelper.buildOliveFulfillmentURL(p, this.customer.getCurrentEmailRecord()
						.getEmailAddress(), null, null, useAlternate);

			} catch (Exception e) {
				eEditionLink = "";
			}

		}
		return eEditionLink;
	}

	public void setUTeReaderLink(String link) {
		; // do nothing
	}

	public String getUTeReaderTutorialLink() {
		String eEditionTutorialLink = "";

		if (this.customer != null && this.authenticated == true) {
			try {

				ProductIntf p = SubscriptionProductBO.getSubscriptionProduct(UsaTodayConstants.DEFAULT_UT_EE_PUBCODE);

				eEditionTutorialLink = p.getSupplier().getTutorialURL();

			} catch (Exception e) {
				eEditionTutorialLink = "";
			}

		}
		return eEditionTutorialLink;
	}

	public void setUTeReaderTutorialLink(String link) {
		; // do nothing
	}

	public String getSportsWeeklyeReaderLink() {
		String eEditionLink = "";

		// olive does not support SW
		// if (this.customer != null && this.authenticated == true) {
		// try {
		// ProductIntf p = SubscriptionProductBO.getSubscriptionProduct(UsaTodayConstants.DEFAULT_SW_EE_PUBCODE);
		//
		// eEditionLink = ElectronicEditionFulfillmentHelper.buildOliveFulfillmentURL(p,
		// this.customer.getCurrentEmailRecord().getEmailAddress(), null, null);
		//
		// }
		// catch (Exception e) {
		// eEditionLink = "";
		// }
		//
		// }
		eEditionLink = "http://usatodaysportsweekly.va.newsmemory.com";

		return eEditionLink;
	}

	public void setSportsWeeklyeReaderLink(String link) {
		; // do nothing
	}

	public String getCurrentAccountEZPAyStatusString() {
		String ezPay = "No";
		try {
			if (this.customer != null && this.authenticated == true) {
				SubscriberAccountIntf acct = this.customer.getCurrentAccount();
				if (acct != null) {
					if (acct.isOnEZPay()) {
						ezPay = "Yes";
					}
				}
			}
		} catch (Exception e) {
			ezPay = "";
		}
		return ezPay;
	}

	public String getExactTargetEmailPreferenceLink() {
		String etLink = "#";
		try {

			etLink = "http://pages.exacttarget.com/page.aspx?QS=3935619f7de112ef775078742a3b88d821295ee4dbc792deb89bccf5ead4c967&email="
					+ URLEncoder.encode(this.customer.getCurrentEmailRecord().getEmailAddress(), "UTF8");
		} catch (Exception e) {
		}
		return etLink;
	}

	public SubscriberAccountHandler getCurrentAccount() {
		if (this.currentAccountHandler == null) {
			this.currentAccountHandler = new SubscriberAccountHandler();
		}

		try {
			if (this.customer != null && this.customer.getCurrentAccount() != null) {
				this.currentAccountHandler.setAccount(this.customer.getCurrentAccount());
			}
		} catch (Exception e) {
			;

		}
		return this.currentAccountHandler;
	}

	public SubscriptionOfferHandler getCurrentAccountPubDefaultOffer() {
		SubscriptionOfferHandler handler = null;
		try {
			if (this.customer != null && this.customer.getCurrentAccount() != null) {
				SubscriptionOfferIntf o = customer.getCurrentAccount().getProduct().getDefaultOffer();
				handler = new SubscriptionOfferHandler();
				handler.setCurrentOffer(o);
			} else {
				SubscriptionOfferIntf o = SubscriptionOfferManager.getInstance().getDefaultOffer(UsaTodayConstants.UT_PUBCODE);
				handler = new SubscriptionOfferHandler();
				handler.setCurrentOffer(o);
			}
		} catch (Exception e) {
			;
		}

		return handler;
	}

	public boolean getIsCurrentAccountUTBranded() {
		boolean isUT = true;
		try {
			if (this.customer != null && this.customer.getCurrentAccount() != null) {
				if (!this.customer.getCurrentAccount().getProduct().getBrandingPubCode()
						.equalsIgnoreCase(UsaTodayConstants.UT_PUBCODE)) {
					isUT = false;
				}
			}
		} catch (Exception e) {
			;
		}

		return isUT;
	}

	public boolean getIsCurrentAccountBWBranded() {
		boolean isBW = false;
		try {
			if (this.customer != null && this.customer.getCurrentAccount() != null) {
				if (this.customer.getCurrentAccount().getProduct().getBrandingPubCode()
						.equalsIgnoreCase(UsaTodayConstants.BW_PUBCODE)) {
					isBW = true;
				}
			}
		} catch (Exception e) {
			;
		}

		return isBW;
	}

}
