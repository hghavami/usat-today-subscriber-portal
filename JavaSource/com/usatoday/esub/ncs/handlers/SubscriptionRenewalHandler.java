package com.usatoday.esub.ncs.handlers;

import java.util.ArrayList;

import javax.faces.model.SelectItem;

import org.joda.time.DateTime;

import com.usatoday.business.interfaces.shopping.SubscriptionOrderItemIntf;

public class SubscriptionRenewalHandler {

	private CustomerHandler currentCustomer = null;

	private String selectedTerm = null;
	private String creditCardNumber = null;
	private String creditCardVerificationNumber = null;
	private String creditCardExpirationMonth = null;
	private String creditCardExpirationYear = null;
	private String existingCreditCardExpirationMonth = null;
	private String existingCreditCardExpirationYear = null;
	private String selectedPaymentOption = null;
	private String selectedRenewalOption = SubscriptionOrderItemIntf.EZPAY;
	private boolean subscriptionActive = true;

	public String getSelectedTerm() {
		return selectedTerm;
	}

	public void setSelectedTerm(String selectedTerm) {
		this.selectedTerm = selectedTerm;
	}

	public String getCreditCardNumber() {
		return creditCardNumber;
	}

	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	public String getCreditCardVerificationNumber() {
		return creditCardVerificationNumber;
	}

	public void setCreditCardVerificationNumber(String creditCardVerificationNumber) {
		this.creditCardVerificationNumber = creditCardVerificationNumber;
	}

	public String getCreditCardExpirationMonth() {
		return creditCardExpirationMonth;
	}

	public void setCreditCardExpirationMonth(String creditCardExpirationMonth) {
		this.creditCardExpirationMonth = creditCardExpirationMonth;
	}

	public String getCreditCardExpirationYear() {
		return creditCardExpirationYear;
	}

	public void setCreditCardExpirationYear(String creditCardExpirationYear) {
		this.creditCardExpirationYear = creditCardExpirationYear;
	}

	public String getSelectedPaymentOption() {
		if (this.selectedPaymentOption == null) {
			return "new_cc_info";
		}
		return selectedPaymentOption;
	}

	public void setSelectedPaymentOption(String selectedPaymentOption) {
		this.selectedPaymentOption = selectedPaymentOption;
	}

	public String getSelectedRenewalOption() {
		return selectedRenewalOption;
	}

	public void setSelectedRenewalOption(String selectedRenewalOption) {
		this.selectedRenewalOption = selectedRenewalOption;
	}

	public ArrayList<SelectItem> getCreditCardExpirationYears() {

		ArrayList<SelectItem> items = new ArrayList<SelectItem>();

		try {
			SelectItem si = null;

			DateTime today = new DateTime();

			for (int c = 0; c < 11; c++) {
				String year = today.toString("yyyy");

				si = new SelectItem();

				si.setDescription(year);
				si.setLabel(year);
				si.setValue(year);

				items.add(si);

				today = today.plusYears(1);
			}
		} catch (Exception e) {
			System.out.println("SubscriptionRenewalHandler:  Failed to build terms items: " + e.getMessage());
		}
		return items;
	}

	public String getExistingCreditCardExpirationMonth() {
		// if never been modified get from stored value.
		if (this.currentCustomer != null && this.existingCreditCardExpirationMonth == null) {
			this.existingCreditCardExpirationMonth = this.currentCustomer.getCustomer().getCurrentAccount().getCreditCardExpMonth();
		}
		return existingCreditCardExpirationMonth;
	}

	public void setExistingCreditCardExpirationMonth(String existingCreditCardExpirationMonth) {
		this.existingCreditCardExpirationMonth = existingCreditCardExpirationMonth;
	}

	public String getExistingCreditCardExpirationYear() {
		if (this.currentCustomer != null && this.existingCreditCardExpirationYear == null) {
			this.existingCreditCardExpirationYear = this.currentCustomer.getCustomer().getCurrentAccount().getCreditCardExpYear();
		}
		return existingCreditCardExpirationYear;
	}

	public void setExistingCreditCardExpirationYear(String existingCreditCardExpirationYear) {
		this.existingCreditCardExpirationYear = existingCreditCardExpirationYear;
	}

	public CustomerHandler getCurrentCustomer() {
		return currentCustomer;
	}

	public void setCurrentCustomer(CustomerHandler currentCustomer) {
		this.currentCustomer = currentCustomer;
	}

	public boolean getIsChoseBillMeForFutureRenewal() {
		if (this.selectedRenewalOption.equalsIgnoreCase(SubscriptionOrderItemIntf.BILL_ME)) {
			return true;
		}

		return false;
	}

	public void resetForNewOrder() {
		this.selectedTerm = null;
		this.creditCardNumber = null;
		this.creditCardVerificationNumber = null;
		this.creditCardExpirationMonth = null;
		this.creditCardExpirationYear = null;
		this.existingCreditCardExpirationMonth = null;
		this.existingCreditCardExpirationYear = null;
		this.selectedPaymentOption = null;
		this.selectedRenewalOption = SubscriptionOrderItemIntf.EZPAY;
		this.subscriptionActive = true;
	}

	public boolean isSubscriptionActive() {
		try {
			if (this.currentCustomer.getCustomer().getCurrentAccount().getTransRecType().equals("NI")
					|| this.currentCustomer.getCustomer().getCurrentAccount().isOnEZPay()) {
				subscriptionActive = false;
			}
		} catch (Exception e) {
		}
		return subscriptionActive;
	}

	public void setSubscriptionActive(boolean renewalTermsExist) {
		this.subscriptionActive = renewalTermsExist;
	}
}
