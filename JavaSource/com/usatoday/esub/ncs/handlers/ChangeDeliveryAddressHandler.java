package com.usatoday.esub.ncs.handlers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.faces.model.SelectItem;

import com.usatoday.business.interfaces.customer.AdditionalDeliveryAddressIntf;
import com.usatoday.business.interfaces.customer.CustomerIntf;
import com.usatoday.businessObjects.customer.ContactBO;

public class ChangeDeliveryAddressHandler {

	private String rawAPIResponse = null;

	private CustomerIntf customer = null;
	private String additionalAddressChoices = "AdditionalAddressesChoices";
	private Date changeDeliveryAddressDate = null;
	private Date changeDeliveryAddressStopDate = null;
	private ContactBO deliveryContact = null;
	private String firstName = "";
	private String lastName = "";
	private String firmName = "";
	private String streetAddress = "";
	private String aptDeptSuite = "";
	private String addlAddr1 = "";
	private String city = "";
	private String state = "";
	private String zip = "";
	private String homePhoneAreaCode = "";
	private String homePhoneExchange = "";
	private String homePhoneExtension = "";
	private String homePhoneComplete = "";
	private String homePhoneFormatted = "";
	private String busPhoneAreaCode = "";
	private String busPhoneExchange = "";
	private String busPhoneExtension = "";
	private String busPhoneComplete = "";
	private String busPhoneFormatted = "";
	private String deliveryMethod = "";
	private boolean deliveryMethodCheck = false;
	private String deliveryMethodText = "";
	private boolean transType3 = true;
	private boolean noDeliveryInfoChange = false;
	private ArrayList<SelectItem> additionalDeliveryAddressCollection = new ArrayList<SelectItem>();
	private boolean savePreviousAddress = false;
	private boolean previousAddressAlreadyWritten = false;

	public String getAdditionalAddressChoices() {
		return additionalAddressChoices;
	}

	public ArrayList<SelectItem> getAdditionalDeliveryAddress() {
		try {
			if (this.additionalDeliveryAddressCollection == null || this.additionalDeliveryAddressCollection.isEmpty()) {
				HashMap<String, AdditionalDeliveryAddressIntf> additionalDeliveryAddresses = new HashMap<String, AdditionalDeliveryAddressIntf>();
				additionalDeliveryAddresses = customer.getAdditionalDeliveryAddress();
				if (additionalDeliveryAddresses != null) {
					try {
						Iterator<AdditionalDeliveryAddressIntf> aItr = additionalDeliveryAddresses.values().iterator();
						while (aItr.hasNext()) {
							AdditionalDeliveryAddressIntf adai = aItr.next();
							SelectItem selectI = new SelectItem();
							String address = adai.getStreetAddress() + " " + adai.getAptDeptSuite() + " " + adai.getCity() + " "
									+ adai.getState() + " " + adai.getZip();
							selectI.setDescription(address);
							selectI.setLabel(address);
							selectI.setValue(String.valueOf(adai.getId()));
							this.additionalDeliveryAddressCollection.add(selectI);
						}
					} catch (Exception e) {
						this.additionalDeliveryAddressCollection = null;
					}
				}
			}
		} catch (Exception e) {
			this.additionalDeliveryAddressCollection = null;
		}
		return this.additionalDeliveryAddressCollection;
	}

	public ArrayList<SelectItem> getAdditionalDeliveryAddressCollection() {
		return additionalDeliveryAddressCollection;
	}

	public boolean getAdditionalDeliveryAddressExist() {
		if (getAdditionalDeliveryAddress() == null || getAdditionalDeliveryAddress().isEmpty()) {
			return false;
		} else {
			return true;
		}

	}

	public String getAddlAddr1() {
		if (addlAddr1.equals("") || addlAddr1 == null) {
			addlAddr1 = customer.getCurrentAccount().getDeliveryContact().getUIAddress().getAddress2();
			if (addlAddr1.equals("") || addlAddr1 == null) {
				addlAddr1 = "";
			}
		}
		return addlAddr1;
	}

	public String getAptDeptSuite() {
		if (aptDeptSuite.equals("") || aptDeptSuite == null) {
			aptDeptSuite = customer.getCurrentAccount().getDeliveryContact().getUIAddress().getAptSuite();
			if (aptDeptSuite.equals("") || aptDeptSuite == null) {
				aptDeptSuite = "";
			}
		}
		return aptDeptSuite;
	}

	public String getBusPhoneAreaCode() {
		if ((busPhoneAreaCode.equals("") || busPhoneAreaCode == null)
				&& customer.getCurrentAccount().getDeliveryContact().getBusinessPhone() != null
				&& !customer.getCurrentAccount().getDeliveryContact().getBusinessPhone().equals("")) {
			busPhoneAreaCode = customer.getCurrentAccount().getDeliveryContact().getBusinessPhone().substring(0, 3);
		}
		return busPhoneAreaCode;
	}

	public String getBusPhoneComplete() {
		busPhoneComplete = this.busPhoneAreaCode + this.busPhoneExchange + this.busPhoneExtension;
		return busPhoneComplete;
	}

	public String getBusPhoneExchange() {
		if ((busPhoneExchange.equals("") || busPhoneExchange == null)
				&& customer.getCurrentAccount().getDeliveryContact().getBusinessPhone() != null
				&& !customer.getCurrentAccount().getDeliveryContact().getBusinessPhone().equals("")) {
			busPhoneExchange = customer.getCurrentAccount().getDeliveryContact().getBusinessPhone().substring(3, 6);
		}
		return busPhoneExchange;
	}

	public String getBusPhoneExtension() {
		if ((busPhoneExtension.equals("") || busPhoneExtension == null)
				&& customer.getCurrentAccount().getDeliveryContact().getBusinessPhone() != null
				&& !customer.getCurrentAccount().getDeliveryContact().getBusinessPhone().equals("")) {
			busPhoneExtension = customer.getCurrentAccount().getDeliveryContact().getBusinessPhone().substring(6, 10);
		}
		return busPhoneExtension;
	}

	public String getBusPhoneFormatted() {
		StringBuffer phone = new StringBuffer("");
		if (this.getBusPhoneComplete() != null && this.getBusPhoneComplete().length() == 10) {
			phone.append("(").append(this.getBusPhoneComplete().substring(0, 3));
			phone.append(") ").append(this.getBusPhoneComplete().substring(3, 6));
			phone.append("-").append(this.getBusPhoneComplete().substring(6));
		}
		busPhoneFormatted = phone.toString();
		return busPhoneFormatted;
	}

	public Date getChangeDeliveryAddressDate() {
		return changeDeliveryAddressDate;
	}

	public Date getChangeDeliveryAddressStopDate() {
		return changeDeliveryAddressStopDate;
	}

	public boolean getCheckAddlAddr1() {
		if (this.addlAddr1.trim().equals("") || this.addlAddr1 == null) {
			return false;
		} else {
			return true;
		}
	}

	public boolean getCheckAptSuite() {
		if (this.aptDeptSuite.trim().equals("") || this.aptDeptSuite == null) {
			return false;
		} else {
			return true;
		}
	}

	public boolean getCheckBusPhone() {
		if (this.busPhoneAreaCode.trim().equals("") || this.busPhoneAreaCode == null) {
			return false;
		} else {
			return true;
		}
	}

	public boolean getCheckFirmName() {
		if (this.firmName.trim().equals("") || this.firmName == null) {
			return false;
		} else {
			return true;
		}
	}

	public String getCity() {
		if (city.trim().equals("") || city == null) {
			city = customer.getCurrentAccount().getDeliveryContact().getUIAddress().getCity();
		}
		return city;
	}

	public CustomerIntf getCustomer() {
		return customer;
	}

	public ContactBO getDeliveryContact() {
		return deliveryContact;
	}

	public String getDeliveryMethod() {
		return deliveryMethod;
	}

	public String getDeliveryMethodText() {
		return deliveryMethodText;
	}

	public String getFirmName() {
		if (firmName.equals("") || firmName == null) {
			firmName = customer.getCurrentAccount().getDeliveryContact().getFirmName();
			if (firmName.equals("") || firmName == null) {
				firmName = "";
			}
		}
		return firmName;
	}

	public String getFirstName() {
		if (firstName.trim().equals("") || firstName == null) {
			firstName = customer.getCurrentAccount().getDeliveryContact().getFirstName();
			if (firstName.trim().equals("") || firstName == null) {
				firstName = "";
			}
		}
		return firstName;
	}

	public String getHomePhoneAreaCode() {
		if ((homePhoneAreaCode.trim().equals("") || homePhoneAreaCode == null)
				&& customer.getCurrentAccount().getDeliveryContact().getHomePhone() != null
				&& !customer.getCurrentAccount().getDeliveryContact().getHomePhone().equals("")) {
			homePhoneAreaCode = customer.getCurrentAccount().getDeliveryContact().getHomePhone().substring(0, 3);
		}
		return homePhoneAreaCode;
	}

	public String getHomePhoneComplete() {
		homePhoneComplete = this.homePhoneAreaCode + this.homePhoneExchange + this.homePhoneExtension;
		return homePhoneComplete;
	}

	public String getHomePhoneExchange() {
		if ((homePhoneExchange.trim().equals("") || homePhoneExchange == null)
				&& customer.getCurrentAccount().getDeliveryContact().getHomePhone() != null
				&& !customer.getCurrentAccount().getDeliveryContact().getHomePhone().equals("")) {
			homePhoneExchange = customer.getCurrentAccount().getDeliveryContact().getHomePhone().substring(3, 6);
		}
		return homePhoneExchange;
	}

	public String getHomePhoneExtension() {
		if ((homePhoneExtension.trim().equals("") || homePhoneExtension == null)
				&& customer.getCurrentAccount().getDeliveryContact().getHomePhone() != null
				&& !customer.getCurrentAccount().getDeliveryContact().getHomePhone().equals("")) {
			homePhoneExtension = customer.getCurrentAccount().getDeliveryContact().getHomePhone().substring(6, 10);
		}
		return homePhoneExtension;
	}

	public String getHomePhoneFormatted() {
		if (this.getHomePhoneComplete() != null && this.getHomePhoneComplete().length() == 10) {
			StringBuffer phone = new StringBuffer("");
			phone.append("(").append(this.getHomePhoneComplete().substring(0, 3));
			phone.append(") ").append(this.getHomePhoneComplete().substring(3, 6));
			phone.append("-").append(this.getHomePhoneComplete().substring(6));
			homePhoneFormatted = phone.toString();
		} else {
			homePhoneFormatted = customer.getCurrentAccount().getDeliveryContact().getHomePhoneNumberFormatted();
		}
		return homePhoneFormatted;
	}

	public String getLastName() {
		if (lastName.trim().equals("") || lastName == null) {
			lastName = customer.getCurrentAccount().getDeliveryContact().getLastName();
			if (lastName.trim().equals("") || lastName == null) {
				lastName = "";
			}
		}
		return lastName;
	}

	public String getState() {
		if (state.trim().equals("") || state == null) {
			state = customer.getCurrentAccount().getDeliveryContact().getUIAddress().getState();
		}
		return state;
	}

	public String getStreetAddress() {
		if (streetAddress.trim().equals("") || streetAddress == null) {
			streetAddress = customer.getCurrentAccount().getDeliveryContact().getUIAddress().getAddress1();
		}
		return streetAddress;
	}

	public String getZip() {
		if (zip.trim().equals("") || zip == null) {
			zip = customer.getCurrentAccount().getDeliveryContact().getUIAddress().getZip();
		}
		return zip;
	}

	public boolean isDeliveryMethodCheck() {
		return deliveryMethodCheck;
	}

	public boolean isNoDeliveryInfoChange() {
		return noDeliveryInfoChange;
	}

	public boolean isPreviousAddressAlreadyWritten() {
		return previousAddressAlreadyWritten;
	}

	public boolean isSavePreviousAddress() {
		return savePreviousAddress;
	}

	public boolean isTransType3() {
		return transType3;
	}

	public void setAdditionalAddressChoices(String additionalAddressChoices) {
		this.additionalAddressChoices = additionalAddressChoices;
	}

	public void setAdditionalDeliveryAddressCollection(ArrayList<SelectItem> additionalDeliveryAddressCollection) {
		this.additionalDeliveryAddressCollection = additionalDeliveryAddressCollection;
	}

	public void setAddlAddr1(String addlAddr1) {
		this.addlAddr1 = addlAddr1;
	}

	public void setAptDeptSuite(String aptDeptSuite) {
		this.aptDeptSuite = aptDeptSuite;
	}

	public void setBusPhoneAreaCode(String busPhone) {
		this.busPhoneAreaCode = busPhone;
	}

	public void setBusPhoneComplete(String busPhoneComplete) {
		this.busPhoneComplete = busPhoneComplete;
	}

	public void setBusPhoneExchange(String busPhoneExchange) {
		this.busPhoneExchange = busPhoneExchange;
	}

	public void setBusPhoneExtension(String busPhoneExtension) {
		this.busPhoneExtension = busPhoneExtension;
	}

	public void setBusPhoneFormatted(String formattedBusPhone) {
		this.busPhoneFormatted = formattedBusPhone;
	}

	public void setChangeDeliveryAddressDate(Date changeDeliveryAddressDate) {
		this.changeDeliveryAddressDate = changeDeliveryAddressDate;
	}

	public void setChangeDeliveryAddressStopDate(Date changeDeliveryAddressStopDate) {
		this.changeDeliveryAddressStopDate = changeDeliveryAddressStopDate;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setCustomer(CustomerIntf customer) {
		this.customer = customer;
	}

	public void setDeliveryContact(ContactBO deliveryContact) {
		this.deliveryContact = deliveryContact;
	}

	public void setDeliveryMethod(String deliveryMethod) {
		this.deliveryMethod = deliveryMethod;
	}

	public void setDeliveryMethodCheck(boolean deliveryMethodCheck) {
		this.deliveryMethodCheck = deliveryMethodCheck;
	}

	public void setDeliveryMethodText(String deliveryMethodText) {
		this.deliveryMethodText = deliveryMethodText;
	}

	public void setFirmName(String firmName) {
		this.firmName = firmName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setHomePhoneAreaCode(String homePhoneAreaCode) {
		this.homePhoneAreaCode = homePhoneAreaCode;
	}

	public void setHomePhoneComplete(String homePhoneComplete) {
		this.homePhoneComplete = homePhoneComplete;
	}

	public void setHomePhoneExchange(String homePhoneExchange) {
		this.homePhoneExchange = homePhoneExchange;
	}

	public void setHomePhoneExtension(String homePhoneExtension) {
		this.homePhoneExtension = homePhoneExtension;
	}

	public void setHomePhoneFormatted(String formattedHomePhone) {
		this.homePhoneFormatted = formattedHomePhone;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setNoDeliveryInfoChange(boolean noDeliveryInfoChange) {
		this.noDeliveryInfoChange = noDeliveryInfoChange;
	}

	public void setPreviousAddressAlreadyWritten(boolean previousAddressAlredayWritten) {
		this.previousAddressAlreadyWritten = previousAddressAlredayWritten;
	}

	public void setSavePreviousAddress(boolean savePreviousAddress) {
		this.savePreviousAddress = savePreviousAddress;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public void setTransType3(boolean transType3) {
		this.transType3 = transType3;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getRawAPIResponse() {
		return rawAPIResponse;
	}

	public void setRawAPIResponse(String rawAPIResponse) {
		this.rawAPIResponse = rawAPIResponse;
	}

}
