/**
 * 
 */
package com.usatoday.esub.ncs.handlers;

import com.usatoday.business.interfaces.customer.CustomerIntf;
import com.usatoday.businessObjects.customer.ContactBO;
import com.usatoday.businessObjects.shopping.payment.CreditCardPaymentMethodBO;

/**
 * @author hghavami
 * 
 */
public class ChangeBillingAddressHandler {

	private CustomerIntf customer = null;
	private boolean signupForEzpay = false;
	private String firstName = "";
	private String lastName = "";
	private String firmName = "";
	private String streetAddress = "";
	private String aptDeptSuite = "";
	private String addlAddr1 = "";
	private String city = "";
	private String state = "";
	private String zip = "";
	private String homePhoneAreaCode = "";
	private String homePhoneExchange = "";
	private String homePhoneExtension = "";
	private String homePhoneComplete = "";
	private String homePhoneFormatted = "";
	private String creditCardNumber = null;
	private String creditCardNumberPartial = null;
	private String creditCardExpirationMonth = null;
	private String creditCardExpirationYear = null;
	private String creditCardExpirationFormatted = null;
	private String existingCreditCardExpirationMonth = null;
	private String existingCreditCardExpirationYear = null;
	private boolean creditCardInfoExist = false;
	private boolean billingInfoExist = false;
	private boolean noBillingInfoChange = false;
	private boolean futurePaymentNotices = false;
	private ContactBO billingContact = null;
	private ContactBO deliveryContact = null;
	private CreditCardPaymentMethodBO billingPayment = null;
	private boolean isOnEzpay = false;

	public String getAddlAddr1() {
		if (addlAddr1 == null || addlAddr1.equals("")) {
			addlAddr1 = customer.getCurrentAccount().getBillingContact().getUIAddress().getAddress2();
		}
		return addlAddr1;
	}

	public String getAptDeptSuite() {
		if (aptDeptSuite == null || aptDeptSuite.equals("")) {
			aptDeptSuite = customer.getCurrentAccount().getBillingContact().getUIAddress().getAptSuite();
		}
		return aptDeptSuite;
	}

	public ContactBO getBillingContact() {
		return billingContact;
	}

	public CreditCardPaymentMethodBO getBillingPayment() {
		return billingPayment;
	}

	public boolean getCheckAddlAddr1() {
		if (this.addlAddr1 == null || this.addlAddr1.trim().equals("")) {
			return false;
		} else {
			return true;
		}
	}

	public boolean getCheckAptSuite() {
		if (this.aptDeptSuite.trim().equals("") || this.aptDeptSuite == null) {
			return false;
		} else {
			return true;
		}
	}

	public boolean getCheckFirmName() {
		if (this.firmName == null || this.firmName.trim().equals("")) {
			return false;
		} else {
			return true;
		}
	}

	public String getCity() {
		if (city == null || city.equals("")) {
			city = customer.getCurrentAccount().getBillingContact().getUIAddress().getCity();
		}
		return city;
	}

	public String getCreditCardExpirationFormatted() {
		creditCardExpirationFormatted = this.creditCardExpirationMonth + "/" + this.creditCardExpirationYear;
		return creditCardExpirationFormatted;
	}

	public String getCreditCardExpirationMonth() {
		if (creditCardExpirationMonth == null || creditCardExpirationMonth.trim().equals("")) {
			if (customer.getCurrentAccount().getCreditCardExp() == null
					|| customer.getCurrentAccount().getCreditCardExp().trim().equals("")) {
				creditCardExpirationMonth = "";
			} else {
				creditCardExpirationMonth = customer.getCurrentAccount().getCreditCardExp().substring(0, 2);
			}
		}
		return creditCardExpirationMonth;
	}

	public String getCreditCardExpirationYear() {
		if (creditCardExpirationYear == null || creditCardExpirationYear.trim().equals("")) {
			if (customer.getCurrentAccount().getCreditCardExp() == null
					|| customer.getCurrentAccount().getCreditCardExp().trim().equals("")) {
				creditCardExpirationYear = "";
			} else {
				creditCardExpirationYear = "20" + customer.getCurrentAccount().getCreditCardExp().substring(2, 4);
			}
		}
		return creditCardExpirationYear;
	}

	public String getCreditCardNumber() {
		if (creditCardNumber == null || creditCardNumber.trim().equals("")) {
			if (!customer.getCurrentAccount().isOnEZPay() || customer.getCurrentAccount().getCreditCardNum() == null
					|| customer.getCurrentAccount().getCreditCardNum().trim().equals("")) {
				creditCardNumber = null;
			} else {
				creditCardNumber = "************" + customer.getCurrentAccount().getCreditCardNum();
			}
		}
		return creditCardNumber;
	}

	public String getCreditCardNumberPartial() {
		if (this.getCreditCardNumber() != null && !this.getCreditCardNumber().trim().equals("")) {
			creditCardNumberPartial = "************"
					+ this.getCreditCardNumber().substring(this.getCreditCardNumber().length() - 4);
		} else {
			creditCardNumberPartial = "";
		}
		return creditCardNumberPartial;
	}

	public CustomerIntf getCustomer() {
		return customer;
	}

	public ContactBO getDeliveryContact() {
		return deliveryContact;
	}

	public String getExistingCreditCardExpirationMonth() {
		return existingCreditCardExpirationMonth;
	}

	public String getExistingCreditCardExpirationYear() {
		return existingCreditCardExpirationYear;
	}

	public String getFirmName() {
		if (firmName == null || firmName.equals("")) {
			firmName = customer.getCurrentAccount().getBillingContact().getFirmName();
		}
		return firmName;
	}

	public String getFirstName() {
		if (firstName == null || firstName.equals("")) {
			firstName = customer.getCurrentAccount().getBillingContact().getFirstName();
		}
		return firstName;
	}

	public String getHomePhoneAreaCode() {
		if ((homePhoneAreaCode == null || homePhoneAreaCode.trim().equals(""))
				&& customer.getCurrentAccount().getBillingContact().getHomePhone() != null
				&& !customer.getCurrentAccount().getBillingContact().getHomePhone().equals("")) {
			homePhoneAreaCode = customer.getCurrentAccount().getBillingContact().getHomePhone().substring(0, 3);
		}
		return homePhoneAreaCode;
	}

	public String getHomePhoneComplete() {
		homePhoneComplete = this.homePhoneAreaCode + this.homePhoneExchange + this.homePhoneExtension;
		return homePhoneComplete;
	}

	public String getHomePhoneExchange() {
		if ((homePhoneExchange == null || homePhoneExchange.trim().equals(""))
				&& customer.getCurrentAccount().getBillingContact().getHomePhone() != null
				&& !customer.getCurrentAccount().getBillingContact().getHomePhone().equals("")) {
			homePhoneExchange = customer.getCurrentAccount().getBillingContact().getHomePhone().substring(3, 6);
		}
		return homePhoneExchange;
	}

	public String getHomePhoneExtension() {
		if ((homePhoneExtension == null || homePhoneExtension.trim().equals(""))
				&& customer.getCurrentAccount().getBillingContact().getHomePhone() != null
				&& !customer.getCurrentAccount().getBillingContact().getHomePhone().equals("")) {
			homePhoneExtension = customer.getCurrentAccount().getBillingContact().getHomePhone().substring(6, 10);
		}
		return homePhoneExtension;
	}

	public String getHomePhoneFormatted() {
		if (this.getHomePhoneComplete() != null && this.getHomePhoneComplete().length() == 10) {
			StringBuffer phone = new StringBuffer("");
			phone.append("(").append(this.getHomePhoneComplete().substring(0, 3));
			phone.append(") ").append(this.getHomePhoneComplete().substring(3, 6));
			phone.append("-").append(this.getHomePhoneComplete().substring(6));
			homePhoneFormatted = phone.toString();
		} else {
			homePhoneFormatted = customer.getCurrentAccount().getBillingContact().getHomePhoneNumberFormatted();
		}
		return homePhoneFormatted;
	}

	public String getLastName() {
		if (lastName == null || lastName.equals("")) {
			lastName = customer.getCurrentAccount().getBillingContact().getLastName();
		}
		return lastName;
	}

	public String getState() {
		if (state == null || state.equals("")) {
			state = customer.getCurrentAccount().getBillingContact().getUIAddress().getState();
		}
		return state;
	}

	public String getStreetAddress() {
		if (streetAddress == null || streetAddress.equals("")) {
			streetAddress = customer.getCurrentAccount().getBillingContact().getUIAddress().getAddress1();
		}
		return streetAddress;
	}

	public String getZip() {
		if (zip == null || zip.equals("")) {
			zip = customer.getCurrentAccount().getBillingContact().getUIAddress().getZip();
		}
		return zip;
	}

	public boolean isBillingInfoExist() {
		if (this.getBillingContact() != null && this.getBillingContact().getUiAddress().getZip() != null
				&& !this.getBillingContact().getUiAddress().getZip().trim().equals("")) {
			billingInfoExist = true;
		} else {
			billingInfoExist = false;
		}
		return billingInfoExist;
	}

	public boolean isCreditCardInfoExist() {
		if (this.getCreditCardNumber() != null && !this.getCreditCardNumber().trim().equals("")) {
			creditCardInfoExist = true;
		} else {
			creditCardInfoExist = false;
		}
		return creditCardInfoExist;
	}

	public boolean isFuturePaymentNotices() {
		return futurePaymentNotices;
	}

	public boolean isNoBillingInfoChange() {
		return noBillingInfoChange;
	}

	public boolean isOnEzpay() {
		if (customer.getCurrentAccount().isOnEZPay()) {
			isOnEzpay = true;
		} else {
			isOnEzpay = false;
		}
		return isOnEzpay;
	}

	public boolean isSignupForEzpay() {
		return signupForEzpay;
	}

	public void resetChangeBilling() {
		this.creditCardNumber = null;
		this.creditCardExpirationMonth = null;
		this.creditCardExpirationYear = null;
		this.existingCreditCardExpirationMonth = null;
		this.existingCreditCardExpirationYear = null;
		this.signupForEzpay = false;
		this.billingPayment = null;
	}

	public void setAddlAddr1(String addlAddr1) {
		this.addlAddr1 = addlAddr1;
	}

	public void setAptDeptSuite(String aptDeptSuite) {
		this.aptDeptSuite = aptDeptSuite;
	}

	public void setBillingContact(ContactBO billingContact) {
		this.billingContact = billingContact;
	}

	public void setBillingInfoExist(boolean billingInfoExist) {
		this.billingInfoExist = billingInfoExist;
	}

	public void setBillingPayment(CreditCardPaymentMethodBO billingPayment) {
		this.billingPayment = billingPayment;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setCreditCardExpirationFormatted(String creditCardExpiration) {
		this.creditCardExpirationFormatted = creditCardExpiration;
	}

	public void setCreditCardExpirationMonth(String creditCardExpirationMonth) {
		this.creditCardExpirationMonth = creditCardExpirationMonth;
	}

	public void setCreditCardExpirationYear(String creditCardExpirationYear) {
		this.creditCardExpirationYear = creditCardExpirationYear;
	}

	public void setCreditCardInfoExist(boolean creditCardInfoExist) {
		this.creditCardInfoExist = creditCardInfoExist;
	}

	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	public void setCreditCardNumberPartial(String creditCardNumberPartial) {
		this.creditCardNumberPartial = creditCardNumberPartial;
	}

	public void setCustomer(CustomerIntf customer) {
		this.customer = customer;
	}

	public void setDeliveryContact(ContactBO deliveryContact) {
		this.deliveryContact = deliveryContact;
	}

	public void setExistingCreditCardExpirationMonth(String existingCreditCardExpirationMonth) {
		this.existingCreditCardExpirationMonth = existingCreditCardExpirationMonth;
	}

	public void setExistingCreditCardExpirationYear(String existingCreditCardExpirationYear) {
		this.existingCreditCardExpirationYear = existingCreditCardExpirationYear;
	}

	public void setFirmName(String firmName) {
		this.firmName = firmName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setFuturePaymentNotices(boolean futurePaymentNotices) {
		this.futurePaymentNotices = futurePaymentNotices;
	}

	public void setHomePhoneAreaCode(String homePhoneAreaCode) {
		this.homePhoneAreaCode = homePhoneAreaCode;
	}

	public void setHomePhoneComplete(String homePhoneComplete) {
		this.homePhoneComplete = homePhoneComplete;
	}

	public void setHomePhoneExchange(String homePhoneExchange) {
		this.homePhoneExchange = homePhoneExchange;
	}

	public void setHomePhoneExtension(String homePhoneExtension) {
		this.homePhoneExtension = homePhoneExtension;
	}

	public void setHomePhoneFormatted(String homePhoneFormatted) {
		this.homePhoneFormatted = homePhoneFormatted;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setNoBillingInfoChange(boolean noBillingInfoChange) {
		this.noBillingInfoChange = noBillingInfoChange;
	}

	public void setOnEzpay(boolean isOnEzpay) {
		this.isOnEzpay = isOnEzpay;
	}

	public void setSignupForEzpay(boolean signupForEzpay) {
		this.signupForEzpay = signupForEzpay;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}
}
