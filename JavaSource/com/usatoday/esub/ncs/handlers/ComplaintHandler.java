package com.usatoday.esub.ncs.handlers;

import java.util.Date;

import com.usatoday.businessObjects.customer.service.Complaint;

public class ComplaintHandler {

	private Complaint lastComplaint = null;

	private String rawAPIResponse = null;

	private String issueType = null;
	private Date date1 = null;
	private Date date2 = null;
	private Date date3 = null;
	private Date date4 = null;
	private Date date5 = null;
	private CustomerHandler currentCustomer = null;
	private boolean subscriptionActive = true;

	public String getIssueType() {
		return issueType;
	}

	public void setIssueType(String issueType) {
		this.issueType = issueType;
	}

	public Date getDate1() {
		return date1;
	}

	public void setDate1(Date date1) {
		this.date1 = date1;
	}

	public Date getDate2() {
		return date2;
	}

	public void setDate2(Date date2) {
		this.date2 = date2;
	}

	public Date getDate3() {
		return date3;
	}

	public void setDate3(Date date3) {
		this.date3 = date3;
	}

	public Date getDate4() {
		return date4;
	}

	public void setDate4(Date date4) {
		this.date4 = date4;
	}

	public Date getDate5() {
		return date5;
	}

	public void setDate5(Date date5) {
		this.date5 = date5;
	}

	public Complaint getLastComplaint() {
		return lastComplaint;
	}

	public void setLastComplaint(Complaint lastComplaint) {
		this.lastComplaint = lastComplaint;
	}

	public void reset() {
		this.setLastComplaint(null);
		resetWebFormValuesOnly();
	}

	public void resetWebFormValuesOnly() {
		this.setIssueType(null);
		this.setRawAPIResponse(null);
		this.setDate1(null);
		this.setDate2(null);
		this.setDate3(null);
		this.setDate4(null);
		this.setDate5(null);
		this.setSubscriptionActive(true);
	}

	public String getRawAPIResponse() {
		return rawAPIResponse;
	}

	public void setRawAPIResponse(String rawAPIResponse) {

		this.rawAPIResponse = rawAPIResponse;
	}

	public CustomerHandler getCurrentCustomer() {
		return currentCustomer;
	}

	public void setCurrentCustomer(CustomerHandler currentCustomer) {
		this.currentCustomer = currentCustomer;
	}

	public boolean isSubscriptionActive() {
		subscriptionActive = true;
		try {
			if (this.currentCustomer.getCustomer().getCurrentAccount().getTransRecType().equals("NI")
					|| this.currentCustomer.getCustomer().getCurrentAccount().getTransRecType().equals("XI")
					|| this.currentCustomer.getCustomer().getCurrentAccount().getTransRecType().equals("PI")
					|| this.currentCustomer.getCustomer().getCurrentAccount().getTransRecType().equals("CI")) {
				subscriptionActive = false;
			}
		} catch (Exception e) {
		}
		return subscriptionActive;
	}

	public void setSubscriptionActive(boolean subscriptionActive) {
		this.subscriptionActive = subscriptionActive;
	}
}
