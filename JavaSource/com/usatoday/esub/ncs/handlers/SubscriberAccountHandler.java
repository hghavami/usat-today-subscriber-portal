package com.usatoday.esub.ncs.handlers;

import java.util.ArrayList;
import java.util.Collection;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import com.usatoday.business.interfaces.customer.ContactIntf;
import com.usatoday.business.interfaces.customer.SubscriberAccountIntf;
import com.usatoday.business.interfaces.products.OfferIntf;
import com.usatoday.business.interfaces.products.SubscriptionTermsIntf;
import com.usatoday.business.interfaces.products.promotions.ImagePromotionIntf;
import com.usatoday.businessObjects.products.promotions.PromotionManager;
import com.usatoday.businessObjects.products.promotions.PromotionSet;
import com.usatoday.businessObjects.util.AS400CurrentStatus;
import com.usatoday.util.constants.UsaTodayConstants;

public class SubscriberAccountHandler {

	private SubscriberAccountIntf account = null;

	public SubscriberAccountIntf getAccount() {
		return account;
	}

	public void setAccount(SubscriberAccountIntf account) {
		this.account = account;
	}

	public boolean isShowCustomerServicePopOverlay() {
		boolean showOverlay = false;

		try {
			if (this.account != null) {
				PromotionSet set = PromotionManager.getInstance().getPromotionsForOffer(this.account.getPubCode(),
						this.account.getKeyCode());
				if (set.getCustomerServicePathPopOverlayPromoImage() != null
						&& !(set.getCustomerServicePathPopOverlayPromoImage().getFulfillText().equalsIgnoreCase("hide"))) {
					showOverlay = true;
				}
			}
		} catch (Exception e) {

		}

		return showOverlay;
	}

	public String getCustomerServicePopOverlayImageLinkURL() {
		String link = "#";
		try {
			if (this.account != null) {
				PromotionSet set = PromotionManager.getInstance().getPromotionsForOffer(this.account.getPubCode(),
						this.account.getKeyCode());
				ImagePromotionIntf imagePromo = set.getCustomerServicePathPopOverlayPromoImage();

				if (imagePromo != null && !(imagePromo.getFulfillText().equalsIgnoreCase("hide"))) {
					link = imagePromo.getImageLinkToURL();
				}
			}
		} catch (Exception e) {
			;
		}
		return link;
	}

	public String getCustomerServicePopOverlayImagePath() {
		String path = "#";
		try {
			if (this.account != null) {
				PromotionSet set = PromotionManager.getInstance().getPromotionsForOffer(this.account.getPubCode(),
						this.account.getKeyCode());
				ImagePromotionIntf imagePromo = set.getCustomerServicePathPopOverlayPromoImage();

				if (imagePromo != null && !(imagePromo.getFulfillText().equalsIgnoreCase("hide"))) {
					path = imagePromo.getImagePathString();
				}
			}
		} catch (Exception e) {
			;
		}
		return path;
	}

	public Collection<SubscriptionTermsIntf> getRenewalTerms() {
		Collection<SubscriptionTermsIntf> terms = null;
		try {
			OfferIntf offer = this.getRenewalOffer();

			terms = offer.getTerms();
			// if (offer instanceof SubscriptionOfferIntf) {
			// SubscriptionOfferIntf sOffer = (SubscriptionOfferIntf)offer;
			// terms = sOffer.getRenewalTerms();
			// }
			// else {
			// terms = offer.getTerms();
			// }
		} catch (Exception e) {

		}
		return terms;
	}

	public ArrayList<SelectItem> getRenewalOfferTermsSelectItems() {

		ArrayList<SelectItem> items = new ArrayList<SelectItem>();

		if (this.account == null) {
			return items;
		}

		try {

			OfferIntf offer = this.getRenewalOffer();

			Collection<SubscriptionTermsIntf> terms = this.getRenewalTerms();

			for (SubscriptionTermsIntf aTerm : terms) {
				SelectItem si = new SelectItem();
				si.setEscape(false);

				StringBuilder sb = new StringBuilder("<strong>");
				sb.append(aTerm.getDescription());
				sb.append("</strong>");
				if ((offer != null && offer.isForceEZPay()) || aTerm.requiresEZPAY()) {
					sb.append("<font color=\"navy\">&nbsp;<sup>*</sup></font>");
				}

				si.setDescription(sb.toString());
				si.setLabel(sb.toString());

				si.setValue(aTerm.getTermAsString());

				items.add(si);
			}

		} catch (Exception e) {
			System.out.println("SubscriptionOfferHandler:  Failed to build terms items: " + e.getMessage());
		}
		return items;
	}

	public OfferIntf getRenewalOffer() {

		OfferIntf offer = null;

		try {
			HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);

			offer = (com.usatoday.business.interfaces.products.OfferIntf) session
					.getAttribute(com.usatoday.esub.common.UTCommon.SESSION_URL_RENEWAL_OFFER);

			if (offer == null) {
				offer = this.account.getDefaultRenewalOffer();
			} else {
				if (!offer.getPubCode().equalsIgnoreCase(this.account.getPubCode())) {
					// offer in renewal offer doesn't match pub code of current account
					session.removeAttribute(com.usatoday.esub.common.UTCommon.SESSION_URL_RENEWAL_OFFER);
					offer = this.account.getDefaultRenewalOffer();
				}
			}
		} catch (Exception e) {
			//
		}
		return offer;
	}

	public boolean isShowEZPayRenewalText() {

		boolean showEZPay = false;

		if (this.account == null) {
			return showEZPay;
		}

		OfferIntf offer = null;
		try {
			// Check for any terms already generated by key code in the one click renewal URL

			offer = this.getRenewalOffer();

			Collection<SubscriptionTermsIntf> terms = null;

			terms = offer.getTerms();

			// No one click renewal use default renewal terms

			for (SubscriptionTermsIntf aTerm : terms) {
				if ((offer != null && offer.isForceEZPay()) || aTerm.requiresEZPAY()) {
					showEZPay = true;
					break;
				}
			}

		} catch (Exception e) {
			showEZPay = false;
		}
		return showEZPay;
	}

	public boolean isShowRenewWithCardOnFileOption() {
		boolean showOption = false;

		if (this.account == null) {
			return false;
		}

		if (this.account.getCreditCardNum() != null && !this.account.getCreditCardNum().equals("")
				&& AS400CurrentStatus.getJdbcActive() && this.account.isBillingSameAsDelivery()) {
			showOption = true;

		}
		return showOption;
	}

	public boolean isBillingSameAsDelivery() {
		boolean sameAsDelivery = true;
		try {
			if (this.account != null) {
				sameAsDelivery = this.account.isBillingSameAsDelivery();
			}
		} catch (Exception e) {
			;
		}
		return sameAsDelivery;
	}

	public String getBillingNameForDisplay() {
		String displayStr = null;
		if (this.account == null) {
			return displayStr;
		}

		if (this.account.isBillingSameAsDelivery()) {
			return "Billing address same as delivery address";
		}

		try {
			ContactIntf contact = this.account.getBillingContact();
			if ((contact.getLastName() != null && contact.getLastName().length() > 0)) {
				if ((contact.getFirstName() != null && contact.getFirstName().length() > 0)) {
					displayStr = contact.getFirstName() + " ";
				}
				displayStr = displayStr + contact.getLastName();
			} else if (contact.getFirmName() != null && contact.getFirmName().length() > 0) {
				displayStr = contact.getFirmName();
			} else {
				displayStr = contact.getFirstName() + " " + contact.getLastName() + " ";
			}
		} catch (Exception e) {
			displayStr = null;
		}
		return displayStr;
	}

	public String getBillingAddressLine1ForDisplay() {
		String displayStr = null;
		if (this.account == null) {
			return displayStr;
		}

		try {
			ContactIntf contact = this.account.getBillingContact();
			displayStr = contact.getUIAddress().getAddress1();
			if (contact.getUIAddress().getAptSuite() != null) {
				displayStr = displayStr + " " + contact.getUIAddress().getAptSuite();
			}
		} catch (Exception e) {
			displayStr = null;
		}
		return displayStr;
	}

	public String getBillingAddressLine2ForDisplay() {
		String displayStr = null;
		if (this.account == null) {
			return displayStr;
		}

		try {
			ContactIntf contact = this.account.getBillingContact();
			displayStr = contact.getUIAddress().getAddress2();
		} catch (Exception e) {
			displayStr = null;
		}
		return displayStr;
	}

	public String getBillingCityStateZipForDisplay() {
		String displayStr = null;
		if (this.account == null) {
			return displayStr;
		}

		try {
			ContactIntf contact = this.account.getBillingContact();
			displayStr = contact.getUIAddress().getCity() + ", " + contact.getUIAddress().getState() + " "
					+ contact.getUIAddress().getZip();
		} catch (Exception e) {
			displayStr = null;
		}
		return displayStr;
	}

	public String getBillingPhoneForDisplay() {
		String displayStr = null;
		if (this.account == null) {
			return displayStr;
		}

		try {
			ContactIntf contact = this.account.getBillingContact();
			if (contact.getHomePhone() != null && contact.getHomePhone().length() > 0) {
				displayStr = contact.getHomePhoneNumberFormatted();
			} else if (contact.getBusinessPhone() != null && contact.getBusinessPhone().length() > 0) {
				displayStr = contact.getWorkPhoneNumberFormatted();
			} else {
				displayStr = "";
			}
		} catch (Exception e) {
			displayStr = null;
		}
		return displayStr;
	}

	public String getDeliveryNameForDisplay() {
		String displayStr = null;
		if (this.account == null) {
			return displayStr;
		}

		try {
			displayStr = "";
			ContactIntf contact = this.account.getDeliveryContact();
			if ((contact.getLastName() != null && contact.getLastName().length() > 0)) {
				if ((contact.getFirstName() != null && contact.getFirstName().length() > 0)) {
					displayStr = contact.getFirstName() + " ";
				}
				displayStr = displayStr + contact.getLastName();
			} else if (contact.getFirmName() != null && contact.getFirmName().length() > 0) {
				displayStr = contact.getFirmName();
			} else {
				displayStr = contact.getFirstName() + " " + contact.getLastName() + " ";
			}
		} catch (Exception e) {
			displayStr = null;
		}
		return displayStr;
	}

	public String getAddressLine1ForDisplay() {
		String displayStr = null;
		if (this.account == null) {
			return displayStr;
		}

		try {
			ContactIntf contact = this.account.getDeliveryContact();
			displayStr = contact.getUIAddress().getAddress1();
			if (contact.getUIAddress().getAptSuite() != null) {
				displayStr = displayStr + " " + contact.getUIAddress().getAptSuite();
			}
		} catch (Exception e) {
			displayStr = null;
		}
		return displayStr;
	}

	public String getAddressLine2ForDisplay() {
		String displayStr = null;
		if (this.account == null) {
			return displayStr;
		}

		try {
			ContactIntf contact = this.account.getDeliveryContact();
			displayStr = contact.getUIAddress().getAddress2();
		} catch (Exception e) {
			displayStr = null;
		}
		return displayStr;
	}

	public String getCityStateZipForDisplay() {
		String displayStr = null;
		if (this.account == null) {
			return displayStr;
		}

		try {
			ContactIntf contact = this.account.getDeliveryContact();
			displayStr = contact.getUIAddress().getCity() + ", " + contact.getUIAddress().getState() + " "
					+ contact.getUIAddress().getZip();
		} catch (Exception e) {
			displayStr = null;
		}
		return displayStr;
	}

	public String getDeliveryPhoneForDisplay() {
		String displayStr = null;
		if (this.account == null) {
			return displayStr;
		}

		try {
			ContactIntf contact = this.account.getDeliveryContact();
			if (contact.getHomePhone() != null && contact.getHomePhone().length() > 0) {
				displayStr = contact.getHomePhoneNumberFormatted();
			} else if (contact.getBusinessPhone() != null && contact.getBusinessPhone().length() > 0) {
				displayStr = contact.getWorkPhoneNumberFormatted();
			} else {
				displayStr = "";
			}
		} catch (Exception e) {
			displayStr = null;
		}
		return displayStr;
	}

	public String getNumberOfCopiesForRenewalDisplay() {
		String displayStr = null;
		if (this.account == null) {
			return displayStr;
		}

		try {
			if (account.getNumberOfPapers() > 1) {
				displayStr = "Copies: " + String.valueOf(account.getNumberOfPapers());
			} else {
				displayStr = "";
			}
		} catch (Exception e) {
			displayStr = null;
		}
		return displayStr;
	}

	public String getCardOnFileProcessing() {
		if (this.isShowRenewWithCardOnFileOption()) {
			return "true";
		}
		return "false";
	}

	public void setCardOnFileProcessing(String value) {
		; // this is a hidden field that doesn't change. Java Beans, must have getters and setters
	}

	public ArrayList<SelectItem> getComplatintIssueOptionSelectItems() {
		ArrayList<SelectItem> items = null;

		ComplaintSelectItems csi = (ComplaintSelectItems) FacesContext.getCurrentInstance().getExternalContext()
				.getApplicationMap().get("complaintIssuesAppScope");

		if (this.account == null || this.account.getProduct().isElectronicDelivery()) {
			items = csi.getGenesysComplaintItems();
			return items;
		}
		if (this.account.getProduct().getProductCode().equalsIgnoreCase(UsaTodayConstants.UT_PUBCODE)) {
			items = csi.getUTComplatintIssueOptionSelectItems();
		} else if (this.account.getProduct().getProductCode().equalsIgnoreCase(UsaTodayConstants.SW_PUBCODE)) {
			items = csi.getBWComplatintIssueOptionSelectItems();
		} else {
			items = csi.getGenesysComplaintItems();
		}

		return items;
	}

	public String getEZPayDisclaimer() {
		String disclaimerText = "";
		try {

			OfferIntf offer = this.getRenewalOffer();
			Collection<SubscriptionTermsIntf> terms = this.getRenewalTerms();
			for (SubscriptionTermsIntf aTerm : terms) {
//				if ((offer != null && offer.isForceEZPay()) || aTerm.requiresEZPAY()) {
				if (offer != null) {					
					disclaimerText = aTerm.getDisclaimerText();
					break;
				}
			}
		} catch (Exception e) {
			System.out.println("SubscriptionOfferHandler:  Failed to derive EZPay disclaimer text: " + e.getMessage());
		}
		return disclaimerText;
	}
}
