package com.usatoday.esub.ncs.handlers;

import java.util.ArrayList;

import javax.faces.model.SelectItem;

import com.usatoday.businessObjects.customer.service.Complaint;
import com.usatoday.businessObjects.customer.service.SWComplaint;
import com.usatoday.businessObjects.customer.service.UTComplaint;

public class ComplaintSelectItems {

	private ArrayList<SelectItem> utComplaintItems = null;
	private ArrayList<SelectItem> bwComplaintItems = null;
	private ArrayList<SelectItem> genesysComplaintItems = null;

	public ComplaintSelectItems() {
		super();

		this.loadUTComplaintItems();
		this.loadBWComplaintItems();
		this.loadGannettComplaintItems();
	}

	public ArrayList<SelectItem> getUTComplatintIssueOptionSelectItems() {
		return utComplaintItems;
	}

	public ArrayList<SelectItem> getBWComplatintIssueOptionSelectItems() {
		return bwComplaintItems;
	}

	private void loadUTComplaintItems() {
		this.utComplaintItems = new ArrayList<SelectItem>();

		SelectItem si = new SelectItem();
		si.setDescription("Newspaper Not Delivered");
		si.setLabel("Newspaper Not Delivered");
		si.setValue(UTComplaint.NEWSPAPER_NOT_DELIVERED);
		this.utComplaintItems.add(si);

		si = new SelectItem();
		si.setDescription("Newspaper Was Late");
		si.setLabel("Newspaper Was Late");
		si.setValue(UTComplaint.NEWSPAPER_WAS_LATE);
		this.utComplaintItems.add(si);

		si = new SelectItem();
		si.setDescription("Subscription Did Not Start");
		si.setLabel("Subscription Did Not Start");
		si.setValue(UTComplaint.SUBSCRIPTION_DID_NOT_START);
		this.utComplaintItems.add(si);

		si = new SelectItem();
		si.setDescription("Subscription Did Not Restart from Vacation");
		si.setLabel("Subscription Did Not Restart from Vacation");
		si.setValue(UTComplaint.SUBSCRIPTION_DID_NOT_RESTART);
		this.utComplaintItems.add(si);

		si = new SelectItem();
		si.setDescription("Newspaper Delivered During Vacation");
		si.setLabel("Newspaper Delivered During Vacation");
		si.setValue(UTComplaint.NEWSPAPER_DELIVERED_DURING_VACA);
		this.utComplaintItems.add(si);

		si = new SelectItem();
		si.setDescription("Incorrect Publication Delivered");
		si.setLabel("Incorrect Publication Delivered");
		si.setValue(UTComplaint.INCORRECT_NEWSPAPER_DELIVERED);
		this.utComplaintItems.add(si);

		si = new SelectItem();
		si.setDescription("Wet Newspaper");
		si.setLabel("Wet Newspaper");
		si.setValue(UTComplaint.WET_NEWSPAPER);
		this.utComplaintItems.add(si);

		si = new SelectItem();
		si.setDescription("Torn Newspaper");
		si.setLabel("Torn Newspaper");
		si.setValue(UTComplaint.TORN_NEWSPAPER);
		this.utComplaintItems.add(si);

		si = new SelectItem();
		si.setDescription("Section Missing");
		si.setLabel("Section Missing");
		si.setValue(UTComplaint.MISSING_SECTION);
		this.utComplaintItems.add(si);
	}

	private void loadBWComplaintItems() {
		this.bwComplaintItems = new ArrayList<SelectItem>();

		SelectItem si = new SelectItem();
		si.setDescription("Issue Not Delivered");
		si.setLabel("Issue Not Delivered");
		si.setValue(SWComplaint.ISSUE_NOT_DELIVERED);
		this.bwComplaintItems.add(si);

		si = new SelectItem();
		si.setDescription("Issue Was Late");
		si.setLabel("Issue Was Late");
		si.setValue(SWComplaint.ISSUE_WAS_LATE);
		this.bwComplaintItems.add(si);

		si = new SelectItem();
		si.setDescription("Subscription Did Not Start");
		si.setLabel("Subscription Did Not Start");
		si.setValue(SWComplaint.SUBSCRIPTION_DID_NOT_START);
		this.bwComplaintItems.add(si);

		si = new SelectItem();
		si.setDescription("Issue Was Torn");
		si.setLabel("Issue Was Torn");
		si.setValue(SWComplaint.ISSUE_WAS_TORN);
		this.bwComplaintItems.add(si);
	}

	private void loadGannettComplaintItems() {
		this.genesysComplaintItems = new ArrayList<SelectItem>();

		SelectItem si = new SelectItem();
		si.setDescription("Issue Not Delivered");
		si.setLabel("Issue Not Delivered");
		si.setValue(Complaint.ISSUE_NOT_DELIVERED);
		this.genesysComplaintItems.add(si);

		si = new SelectItem();
		si.setDescription("Issue Was Damaged");
		si.setLabel("Issue Was Damaged");
		si.setValue(Complaint.ISSUE_WAS_DAMAGED);
		this.genesysComplaintItems.add(si);

		si = new SelectItem();
		si.setDescription("Issue Was Wet");
		si.setLabel("Issue Was Wet");
		si.setValue(SWComplaint.ISSUE_WAS_WET);
		this.genesysComplaintItems.add(si);
	}

	public ArrayList<SelectItem> getGenesysComplaintItems() {
		return genesysComplaintItems;
	}

	public void setGenesysComplaintItems(ArrayList<SelectItem> genesysComplaintItems) {
		this.genesysComplaintItems = genesysComplaintItems;
	}
}
