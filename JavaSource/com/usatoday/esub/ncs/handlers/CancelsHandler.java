package com.usatoday.esub.ncs.handlers;

import java.util.ArrayList;
import java.util.Iterator;

import javax.faces.model.SelectItem;

import com.usatoday.business.interfaces.CancelSubscriptionIntf;
import com.usatoday.business.interfaces.customer.CustomerIntf;
import com.usatoday.businessObjects.customer.CancelSubscriptionBO;

public class CancelsHandler {

	private boolean address2Exists = false;
	private String cancelCode = null;
	private String cancelReason = null;
	private String comment1 = null;
	private String comment2 = null;
	private String comment3 = null;
	private CustomerIntf customer = null;
	private String onEZPay = "No";

	public String getCancelCode() {
		return cancelCode;
	}

	public String getCancelReason() {
		return cancelReason;
	}

	public ArrayList<SelectItem> getCancelSubsReasons() {

		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		String pubCode = this.customer.getCurrentAccount().getPubCode();

		try {
			Iterator<CancelSubscriptionIntf> reasons = CancelSubscriptionBO.getCancelSubscriptionReasons(pubCode).iterator();
			while (reasons.hasNext()) {
				SelectItem selectI = new SelectItem();
				CancelSubscriptionIntf cancelReason = reasons.next();
				selectI.setDescription(cancelReason.getCancelSubsReasonDesc());
				selectI.setLabel(cancelReason.getCancelSubsReasonDesc());
				selectI.setValue(cancelReason.getCancelSubsReasonCode());
				si.add(selectI);
			}
		} catch (Exception e) {
			System.out.println("CancelsHandler:  Failed to build cancel subscriptions reasons select items: " + e.getMessage());
		}
		return si;
	}

	public String getComment1() {
		return comment1;
	}

	public String getComment2() {
		return comment2;
	}

	public String getComment3() {
		return comment3;
	}

	public CustomerIntf getCustomer() {
		return customer;
	}

	public String getOnEZPay() {
		if (this.customer.getCurrentAccount().isOnEZPay()) {
			setOnEZPay("Yes");
		}
		return onEZPay;
	}

	public boolean isAddress2Exists() {

		this.address2Exists = false;
		try {
			String address2 = this.customer.getCurrentAccount().getDeliveryContact().getUIAddress().getAddress2().trim();

			if (address2 != null && !address2.equals("")) {
				this.address2Exists = true;
			}
		} catch (Exception e) {

		}

		return this.address2Exists;

	}

	public void setAddress2Exists(boolean aptSuiteExists) {
		this.address2Exists = aptSuiteExists;
	}

	public void setCancelCode(String cancelCode) {
		this.cancelCode = cancelCode;
	}

	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}

	public void setComment1(String comment1) {
		this.comment1 = comment1;
	}

	public void setComment2(String comment2) {
		this.comment2 = comment2;
	}

	public void setComment3(String comment3) {
		this.comment3 = comment3;
	}

	public void setCustomer(CustomerIntf customer) {
		this.customer = customer;
	}

	public void setOnEZPay(String onEZPay) {
		this.onEZPay = onEZPay;
	}
}
