package com.usatoday.esub.ncs.handlers;

import java.util.Collection;
import java.util.Date;

import javax.el.ELContext;
import javax.faces.context.FacesContext;

import org.joda.time.DateTime;

import com.usatoday.business.interfaces.subscriptionTransactions.ExtranetSubscriberTransactionIntf;
import com.usatoday.util.constants.UsaTodayConstants;

public class SuspendResumeSubscriptionHandler {

	private CustomerHandler currentCustomer = null;
	private Date holdStart = null;
	private Date holdEnd = null;
	private boolean donateToNIE = true;
	private boolean unsureOfResumeDate = false;
	private boolean subscriptionActive = true;

	private Collection<ExtranetSubscriberTransactionIntf> postedTransactions = null;

	private boolean transactionPlaced = false;

	public Date getHoldStart() {
		return holdStart;
	}

	public void setHoldStart(Date holdStart) {
		this.holdStart = holdStart;
	}

	public Date getHoldEnd() {
		return holdEnd;
	}

	public void setHoldEnd(Date holdEnd) {
		this.holdEnd = holdEnd;
	}

	public boolean isDonateToNIE() {
		return donateToNIE;
	}

	public String getDonatingToNIEString() {
		if (this.donateToNIE) {
			return "Yes";
		} else {
			return "No";
		}
	}

	public void setDonateToNIE(boolean donateToNIE) {
		this.donateToNIE = donateToNIE;
	}

	public boolean isUnsureOfResumeDate() {
		return unsureOfResumeDate;
	}

	public void setUnsureOfResumeDate(boolean setResumeDate) {
		this.unsureOfResumeDate = setResumeDate;
	}

	public Date getEarliestPossibleStartDateForValidation() {
		Date start = null;

		// this is for calendar validation only

		try {
			ELContext elContext = FacesContext.getCurrentInstance().getELContext();
			CustomerHandler ch = (CustomerHandler) FacesContext.getCurrentInstance().getApplication().getELResolver()
					.getValue(elContext, null, "customerHandler");

			DateTime day = ch.getCurrentAccount().getAccount().getProduct().getEarliestPossibleHoldStartDate();
			start = day.minusDays(1).toDate();
		} catch (Exception e) {
			//
		}
		return start;
	}

	public Date getLatestPossibleStartDateForValidation() {
		Date end = null;

		// this is for calendar validation only

		int monthsIntoFuture = -1;

		try {
			monthsIntoFuture = Integer.valueOf(UsaTodayConstants.DATE_RANGE_ALLOWED);
		} catch (Exception e) {
			monthsIntoFuture = -1;
		}

		DateTime day = new DateTime();

		if (monthsIntoFuture > 0) {

		} else {
			monthsIntoFuture = 12;
		}

		end = day.plusMonths(monthsIntoFuture).toDate();

		return end;
	}

	public void resetHandler() {
		this.donateToNIE = true;
		this.holdEnd = null;
		this.holdStart = null;
		this.unsureOfResumeDate = false;
		this.transactionPlaced = false;
		this.subscriptionActive = true;
	}
	
	public void resetMIE() {
		this.donateToNIE = true;
	}

	public boolean isTransactionPlaced() {
		return transactionPlaced;
	}

	public void setTransactionPlaced(boolean transactionPlaced) {
		this.transactionPlaced = transactionPlaced;
	}

	public Collection<ExtranetSubscriberTransactionIntf> getPostedTransactions() {
		return postedTransactions;
	}

	public void setPostedTransactions(Collection<ExtranetSubscriberTransactionIntf> postedTransactions) {
		this.postedTransactions = postedTransactions;
	}

	public boolean isSubscriptionActive() {
		subscriptionActive = true;
		try {
			if (this.currentCustomer.getCustomer().getCurrentAccount().getTransRecType().equals("NI")
					|| this.currentCustomer.getCustomer().getCurrentAccount().getTransRecType().equals("XI")) {
				subscriptionActive = false;
			}
		} catch (Exception e) {
		}
		return subscriptionActive;
	}

	public void setSubscriptionActive(boolean subscriptionActive) {
		this.subscriptionActive = subscriptionActive;
	}

	public CustomerHandler getCurrentCustomer() {
		return currentCustomer;
	}

	public void setCurrentCustomer(CustomerHandler currentCustomer) {
		this.currentCustomer = currentCustomer;
	}
}
