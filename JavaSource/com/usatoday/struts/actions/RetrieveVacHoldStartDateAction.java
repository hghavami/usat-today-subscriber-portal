package com.usatoday.struts.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.usatoday.business.interfaces.products.SubscriptionProductIntf;
import com.usatoday.businessObjects.products.SubscriptionProductBO;
import com.usatoday.esub.common.UTCommon;

/**
 * @version 1.0
 * @author
 */
public class RetrieveVacHoldStartDateAction extends Action

{

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		ActionForward forward = null; // return value
		StringBuffer sb = new StringBuffer("<DateValue>");

		try {

			HttpSession session = request.getSession();
			String pub = (String) session.getAttribute(UTCommon.SESSION_PUB_NAME);
			pub = pub == null ? UTCommon.UTPUBCODE : pub;

			SubscriptionProductIntf p = SubscriptionProductBO.getSubscriptionProduct(pub);
			String date = p.getEarliestPossibleHoldStartDate().toString("MM/dd/yyyy");

			sb.append(date);
			sb.append("</DateValue>");

			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.getWriter().write(sb.toString());
			response.getWriter().close();

		} catch (Exception e) {

			System.out.println("RetrieveStartDate() - Failed to retrieve pub date: " + e.getMessage());
		}

		// Finish with
		return (forward);
	}
}
