package com.usatoday.struts.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.joda.time.DateTime;

import com.usatoday.businessObjects.products.SubscriptionPublishDateUtility;
import com.usatoday.businessObjects.util.UsatDateTimeFormatter;
import com.usatoday.esub.ncs.handlers.CustomerHandler;

/**
 * @version 1.0
 * @author
 */
public class ValidatePubDateForCurrentPub extends Action

{

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		ActionForward forward = null; // return value

		StringBuilder sb = new StringBuilder("<dateresponse>");

		try {

			HttpSession session = request.getSession();

			CustomerHandler ch = (CustomerHandler) session.getAttribute("customerHandler");

			// Date should be in format MM/dd/yyyy
			String targetDate = request.getParameter("selectedDate");

			SubscriptionPublishDateUtility pdu = SubscriptionPublishDateUtility.getInstance();

			DateTime date = UsatDateTimeFormatter.convertMMDDYYYYToDateTime(targetDate);

			boolean dateValid = pdu.isValidPublishingDate(ch.getCurrentAccount().getAccount().getPubCode(), date); // yyyyMMdd
																													// format for
																													// date

			if (dateValid) {
				sb.append("<datevalid>true</datevalid>");
			} else {
				sb.append("<datevalid>false</datevalid>");
				sb.append("<message>").append(ch.getCurrentAccount().getAccount().getProduct().getName());
				sb.append(" was not published on the selected date.</message>");
			}

			sb.append("</dateresponse>");

			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.getWriter().write(sb.toString());
			response.getWriter().close();

		} catch (Exception e) {

			try {
				sb.append("<message>unable to verify date at this time. ").append(e.getMessage())
						.append("</message></dateresponse>");
				response.setContentType("text/xml");
				response.setHeader("Cache-Control", "no-cache");
				response.getWriter().write(sb.toString());
				response.getWriter().close();
			} catch (Exception eee) {
			}
			System.out.println("ValidatePubDateForCurrentPub() - Failed to retrieve pub date: " + e.getMessage());
		}

		// Finish with
		return (forward);

	}
}
