package com.usatoday.struts.actions.eEdition;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.gannett.usat.sp.partners.UsatEnewspaperPartner;
import com.gannett.usat.sp.partners.UsatEnewspaperPartnerAccessLog;
import com.gannett.usat.sp.partners.controller.UsatEnewspaperPartnerAccessLogManager;
import com.gannett.usat.sp.partners.controller.UsatEnewspaperPartnerManager;
import com.usatoday.business.interfaces.products.ProductIntf;
import com.usatoday.businessObjects.products.ProductBO;
import com.usatoday.businessObjects.products.utils.ElectronicEditionFulfillmentHelper;
import com.usatoday.esub.cookies.PartnerAccessCookieProcessor;
import com.usatoday.util.MobileDeviceDetectionUtil;
import com.usatoday.util.constants.UsaTodayConstants;

/**
 * @version 1.0
 * @author
 */
public class PartnerElectronicAuthAction extends Action

{
	protected DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		ActionForward forward = null;

		String eEditionLink = null;

		// use the ee partner table to look up which product to use for fulfillment and build links according to that.
		EntityManagerFactory emf = null;
		UsatEnewspaperPartnerAccessLog log = new UsatEnewspaperPartnerAccessLog();
		try {

			// If you create an EntityManagerFactory you must call the
			// dispose method when you are done using it.
			emf = Persistence.createEntityManagerFactory("USATDomainModel");

			boolean requestContainsParms = true;

			String partnerID = request.getParameter("partner_id");
			String propertyID = request.getParameter("prop_id");
			String partnerExternalKey = request.getParameter("partner_key");
			String aDateStr = request.getParameter("a_date");
			String dDateStr = request.getParameter("d_date");
			String partnerCalculatedHash = request.getParameter("s");

			PartnerAccessCookieProcessor partnerCookieProcessor = new PartnerAccessCookieProcessor();
			Cookie cookie = partnerCookieProcessor.returnCookie(request.getCookies());

			// determine if request parms are present
			if ((partnerID == null || partnerID.length() == 0 || partnerID.length() > 10)
					|| (propertyID == null || propertyID.length() == 0)
					|| (partnerExternalKey == null || partnerExternalKey.length() == 0)
					|| (aDateStr == null || aDateStr.length() != 10) || (dDateStr == null || dDateStr.length() != 10)
					|| (partnerCalculatedHash == null)) {

				requestContainsParms = false;
			}

			// use request parms if present
			if (requestContainsParms) {
				// update / add cookie
				cookie = partnerCookieProcessor.createCookie(partnerID, propertyID, partnerExternalKey, aDateStr, dDateStr,
						partnerCalculatedHash);
			} else {
				// use cookie values
				if (cookie != null) {
					partnerID = partnerCookieProcessor.getPartnerID(cookie);
					propertyID = partnerCookieProcessor.getPropertyID(cookie);
					partnerExternalKey = partnerCookieProcessor.getExternalKey(cookie);
					aDateStr = partnerCookieProcessor.getArrivalDate(cookie);
					dDateStr = partnerCookieProcessor.getDepartureDate(cookie);
					partnerCalculatedHash = partnerCookieProcessor.getHash(cookie);
				}
			}

			if (cookie != null) {
				response.addCookie(cookie);
			}

			DateTime now = new DateTime();
			// String todaysDate = now.toString("YYYY-MM-dd");

			UsatEnewspaperPartnerManager partnerManager = new UsatEnewspaperPartnerManager(emf);
			List<UsatEnewspaperPartner> partnerList = partnerManager.getUsatEnewspaperPartner(partnerID);

			log.setClientIp(request.getRemoteAddr());
			log.setClientUserAgent(request.getHeader("user-agent"));
			log.setPartnerExternalKey(partnerExternalKey);
			log.setPartnerId(partnerID);
			log.setPropertyId(propertyID);
			log.setTimeStamp(new Timestamp(now.getMillis()));

			UsatEnewspaperPartner partner = null;
			if (partnerList.size() > 0) {
				partner = partnerList.iterator().next();
			}

			if (partner == null || !partner.isCurrentlyActive()) {
				// not valid partner - error page
				log.setResponseCode(UsatEnewspaperPartnerAccessLog.FAILED_NOT_ACTIVE);
//				log.setRedirectedTo(partner.getErrorPage3());
				log.setRedirectedTo(UsaTodayConstants.EEDITION_OLIVE_PARTNER_ERROR_PAGE);				
				if (partnerID != null && partnerID.length() < 11) {
					log.setPartnerId(partnerID);
				} else {
					log.setPartnerId("INVALID");
				}
//				response.sendRedirect(partner.getErrorPage3());
				response.sendRedirect(UsaTodayConstants.EEDITION_OLIVE_PARTNER_ERROR_PAGE);				
				UsatEnewspaperPartnerAccessLogManager logManager = new UsatEnewspaperPartnerAccessLogManager(emf);
				logManager.createUsatEnewspaperPartnerAccessLog(log);
				return null;
			}

			// validate arrival / departure dates
			DateTime aDate = null;
			DateTime dDate = null;
			try {
				aDate = formatter.parseDateTime(aDateStr);
				dDate = formatter.parseDateTime(dDateStr);
				// add one to dDate so it's basically midnight on the day they check out.
				dDate = dDate.plusDays(1);
			} catch (Exception e) {
				// failed to format date
				aDate = null;
			}

			String secretKey = partner.getSecretPassphrase();
			StringBuilder hash = new StringBuilder();
			// create the hash
			hash.append(partnerID).append(propertyID).append(partnerExternalKey).append(aDateStr).append(dDateStr)
					.append(secretKey);
			String hashReponse = ElectronicEditionFulfillmentHelper.getMD5Hash(hash.toString());

			boolean hashMatches = false;
			if (hashReponse.equalsIgnoreCase(partnerCalculatedHash)) {
				// valid request
				hashMatches = true;
			}

			if (!hashMatches) {
				log.setResponseCode(UsatEnewspaperPartnerAccessLog.FAILED_INVALID);

				log.setRedirectedTo(partner.getErrorPage3());

				response.sendRedirect(partner.getErrorPage3());

				UsatEnewspaperPartnerAccessLogManager logManager = new UsatEnewspaperPartnerAccessLogManager(emf);
				logManager.createUsatEnewspaperPartnerAccessLog(log);

				return null;
			}

			if (aDate == null || now.isBefore(aDate)) {
				// too soon error page
				log.setResponseCode(UsatEnewspaperPartnerAccessLog.FAILED_TOO_SOON);
				log.setRedirectedTo(partner.getErrorPage1());
				response.sendRedirect(partner.getErrorPage1());
				UsatEnewspaperPartnerAccessLogManager logManager = new UsatEnewspaperPartnerAccessLogManager(emf);
				logManager.createUsatEnewspaperPartnerAccessLog(log);
				return null;
			}
			if (dDate == null || now.isAfter(dDate)) {
				// after departure date message
				log.setResponseCode(UsatEnewspaperPartnerAccessLog.FAILED_TOO_LATE);
				log.setRedirectedTo(partner.getErrorPage2());
				response.sendRedirect(partner.getErrorPage2());
				UsatEnewspaperPartnerAccessLogManager logManager = new UsatEnewspaperPartnerAccessLogManager(emf);
				logManager.createUsatEnewspaperPartnerAccessLog(log);
				return null;
			}

			// probably need to change this to an expried product like we do for starbucks since going to different skin
			ProductIntf product = ProductBO.fetchProductIncludeInactive(partner.getEeProductCode());

			// this block of code will first detect what we believe is the best
			// viewer. If they have clicked an override link
			// we will ignore what we think is best and use what they selected.
			boolean useAlternate = MobileDeviceDetectionUtil.isMobileDevice(request);

			// eEditionLink = ElectronicEditionFulfillmentHelper.buildOliveFulfillmentURL(product, partnerID, null, "1100000",
			// useAlternate);
			String restrictions = "none";
			if (partner.getEeProductRestrictions() != null && partner.getEeProductRestrictions().trim().length() > 0) {
				restrictions = partner.getEeProductRestrictions().trim();
			}

			eEditionLink = ElectronicEditionFulfillmentHelper.buildOliveFulfillmentURL(product, partnerID, null, restrictions,
					useAlternate);

			log.setResponseCode(UsatEnewspaperPartnerAccessLog.SUCCESS);
			log.setRedirectedTo(eEditionLink);

			response.sendRedirect(eEditionLink);

			UsatEnewspaperPartnerAccessLogManager logManager = new UsatEnewspaperPartnerAccessLogManager(emf);
			logManager.createUsatEnewspaperPartnerAccessLog(log);

			forward = null;

		} catch (Exception e) {
			e.printStackTrace();
			try {
				log.setResponseCode(UsatEnewspaperPartnerAccessLog.FAILED_INVALID);
				log.setRedirectedTo("/eedition/invalidPartnerRequest.faces");
				log.setPartnerId("INVALID");
				response.sendRedirect("/eedition/invalidPartnerRequest.faces");
				
				UsatEnewspaperPartnerAccessLogManager logManager = new UsatEnewspaperPartnerAccessLogManager(emf);
				logManager.createUsatEnewspaperPartnerAccessLog(log);
			} catch (Exception eee) {
				eee.printStackTrace();
			}

		} finally {

			if (emf != null) {
				emf.close();
			}
		}

		// Finish with
		return (forward);

	}
}
