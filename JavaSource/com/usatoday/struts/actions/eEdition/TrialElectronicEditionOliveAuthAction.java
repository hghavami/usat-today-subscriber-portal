package com.usatoday.struts.actions.eEdition;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.usatoday.business.interfaces.customer.TrialCustomerIntf;
import com.usatoday.businessObjects.customer.LoginException;
import com.usatoday.esub.common.UTCommon;
import com.usatoday.esub.trials.handlers.PartnerHandler;
import com.usatoday.esub.trials.handlers.TrialCustomerHandler;
import com.usatoday.util.constants.UsaTodayConstants;

/**
 * @version 1.0
 * @author
 */
public class TrialElectronicEditionOliveAuthAction extends Action

{

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		ActionForward forward = null;

		String forwardLocation = "loginPage";

		try {

			HttpServletRequest req = (HttpServletRequest) request;
			// Save From parameter from Olive
			// String fromParam = request.getParameter("From");

			TrialCustomerIntf trialCustomer = UTCommon.authenticateTrialCustomerViaRememberMeCookie(request);

			TrialCustomerHandler tch = (TrialCustomerHandler) req.getSession().getAttribute("trialCustomerHandler");
			if (tch == null) {
				tch = new TrialCustomerHandler();
				req.getSession().setAttribute("trialCustomerHandler", tch);
			}
			tch.setTrialCustomer(trialCustomer);

			String keyCodeOverride = req.getParameter("keycode");
			if (keyCodeOverride != null && keyCodeOverride.trim().length() == 5) {
				tch.setEarlySubscribeKeycode(keyCodeOverride.trim());
			}

			// if partner id specified on link use it to set current instance
			String partnerID = req.getParameter("pid");
			if (partnerID != null && partnerID.trim().length() > 0) {
				try {
					trialCustomer.setCurrentInstance(partnerID.trim());
				} catch (Exception e) {
					; //
				}
			}

			if (trialCustomer != null) {
				// set up partner session object
				PartnerHandler ph = (PartnerHandler) req.getSession().getAttribute("trialPartnerHandler");
				if (ph == null) {
					ph = new PartnerHandler();
					req.getSession().setAttribute("trialPartnerHandler", ph);
				}

				ph.setPartner(trialCustomer.getCurrentInstance().getPartner());

				if (trialCustomer.getCurrentInstance().getIsTrialPeriodExpired()) {
					forwardLocation = "trialPeriodOver";
				} else {
					forwardLocation = "trialSubscription";
				}
			} else {
				// no cookie set so go to login page
				forwardLocation = "loginPage";
			}

		} catch (Exception e) {

			StringBuilder message = new StringBuilder();

			message.append("e-Newspaper Authorization Failed: ");
			if (e instanceof LoginException) {
				LoginException le = (LoginException) e;
				message.append(le.getErrorMessage());
			} else {
				message.append(e.getMessage());
			}
			forwardLocation = "loginPage";
		}

		// Forward control to the appropriate URI (change name as desired)
		forward = mapping.findForward(forwardLocation);

		if (UsaTodayConstants.debug) {
			System.out.println("e-Newspaper Email Link Authorization.....Sending to : " + forwardLocation);
		}

		// Finish with
		return (forward);

	}
}
