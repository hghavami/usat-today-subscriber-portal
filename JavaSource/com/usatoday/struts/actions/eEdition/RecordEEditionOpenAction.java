package com.usatoday.struts.actions.eEdition;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.usatoday.esub.electronic.utils.EReaderOpenRecorder;

/**
 * @version 1.0
 * @author
 */
public class RecordEEditionOpenAction extends Action

{

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		try {

			// System.out.println("......Recording eEdtion Open Action......");

			EReaderOpenRecorder.recordEReaderOpen(request);

		} catch (Exception e) {

			System.out.println("Exceptions saving EditionAccessLog: " + e.getMessage());
			e.printStackTrace();
		}

		// Finish with
		return null;

	}
}
