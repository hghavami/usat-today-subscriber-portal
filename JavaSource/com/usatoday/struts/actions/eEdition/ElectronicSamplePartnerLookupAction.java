package com.usatoday.struts.actions.eEdition;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.usatoday.business.interfaces.partners.TrialPartnerIntf;
import com.usatoday.businessObjects.partners.TrialPartnerBO;
import com.usatoday.esub.trials.handlers.NewTrialOrderHandler;
import com.usatoday.esub.trials.handlers.PartnerHandler;

/**
 * @version 1.0
 * @author
 */
public class ElectronicSamplePartnerLookupAction extends Action

{

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		/*
		 * This method will look up the partner program passed as a paramater and forward any information to the landing page.
		 * 
		 * Paramaters include: pid - (Required) The partner ID of the program (not primary key) cnum - (Optional) The club number
		 * value if email - (Optional) The customers email address tfn - (Optional) The customers first name tln - (Optional) The
		 * customers last name ta1 - (Optional) Customer address 1 ta2 - (Optional) Customer address 2 tapt - (Optional) Customer
		 * apt/suite tcty - (Optional) Customer city tst - (Optional) Customer state tzip - (Optional) customer zip tph - (Optional)
		 * Customer Phone tcsrid - (Optional) The customer service Rep ID from iQuEST
		 */
		ActionForward forward = new ActionForward(); // return value

		String forwardPath = "failure";

		HttpSession session = request.getSession();
		session.removeAttribute("trialPartnerHandler");

		try {

			String partnerID = request.getParameter("pid");

			if (partnerID == null || partnerID.length() == 0) {
				response.sendRedirect("/subscriptions/electronic/samples/trialErrorPage.faces");
				return null;
			}

			// boolean useCache = true;
			TrialPartnerIntf partner = TrialPartnerBO.fetchPartner(partnerID, true);

			// if no partner or program not running.
			if (partner == null || !partner.getIsOfferCurrentlyRunning()) {
				response.sendRedirect("/subscriptions/electronic/samples/trialErrorPage.faces");
				return null;
			}

			PartnerHandler ph = new PartnerHandler();
			ph.setPartner(partner);

			session.removeAttribute("trialPartnerHandler");
			session.setAttribute("trialPartnerHandler", ph);

			// capture optional params
			NewTrialOrderHandler ntoh = new NewTrialOrderHandler();

			String email = request.getParameter("email");
			String clubNumber = request.getParameter("cnum");
			String firstName = request.getParameter("tfn");
			String lastName = request.getParameter("tln");
			String address1 = request.getParameter("ta1");
			String address2 = request.getParameter("ta2");
			String aptSuite = request.getParameter("tapt");
			String city = request.getParameter("tcty");
			String state = request.getParameter("tst");
			String zip = request.getParameter("tzip");
			String phone = request.getParameter("tph");
			String ncsRepID = request.getParameter("tcsrid");

			// email
			if (email != null && email.length() > 0) {
				ntoh.setDeliveryEmailAddress(email);
				ntoh.setDeliveryEmailAddressConfirmation(email);
			}

			// club number
			if (clubNumber != null && clubNumber.length() > 0) {
				ntoh.setPartnerClubNumber(clubNumber);
			}

			// first name
			if (firstName != null && firstName.length() > 0) {
				if (firstName.length() > 10) {
					try {
						firstName = firstName.substring(0, 10);
					} catch (Exception e) {
						; // ignore
					}
				}
				ntoh.setDeliveryFirstName(firstName);
			}

			// last name
			if (lastName != null && lastName.length() > 0) {
				if (lastName.length() > 15) {
					try {
						lastName = lastName.substring(0, 15);
					} catch (Exception e) {
						; // ignore
					}
				}
				ntoh.setDeliveryLastName(lastName);
			}

			// address1
			if (address1 != null && address1.length() > 0) {
				if (address1.length() > 128) {
					try {
						address1 = address1.substring(0, 128);
					} catch (Exception e) {
						; // ignore
					}
				}
				ntoh.setDeliveryAddress1(address1);
			}

			// address2
			if (address2 != null && address2.length() > 0) {
				if (address2.length() > 128) {
					try {
						address2 = address2.substring(0, 128);
					} catch (Exception e) {
						; // ignore
					}
				}
				ntoh.setDeliveryAddress2(address2);
			}

			// apt/suite
			if (aptSuite != null && aptSuite.length() > 0) {
				if (aptSuite.length() > 28) {
					try {
						aptSuite = aptSuite.substring(0, 28);
					} catch (Exception e) {
						; // ignore
					}
				}
				ntoh.setDeliveryAptSuite(aptSuite);
			}

			// city
			if (city != null && city.length() > 0) {
				if (city.length() > 128) {
					try {
						city = city.substring(0, 128);
					} catch (Exception e) {
						; // ignore
					}
				}
				ntoh.setDeliveryCity(city);
			}

			// state
			if (state != null && state.length() > 0) {
				if (state.length() > 30) {
					try {
						state = state.substring(0, 30);
					} catch (Exception e) {
						; // ignore
					}
				}
				ntoh.setDeliveryState(state);
			}

			// zip
			if (zip != null && zip.length() > 0) {
				if (zip.length() > 10) {
					try {
						zip = zip.substring(0, 10);
					} catch (Exception e) {
						; // ignore
					}
				}
				ntoh.setDeliveryZipCode(zip);
			}

			// phone
			if (phone != null && phone.length() > 0) {
				String areaCode = "";
				String exchange = "";
				String extension = "";

				if (phone.length() > 10) {
					StringBuilder newPhone = new StringBuilder();
					// yank digits
					for (int i = 0; i < phone.length(); i++) {
						if (Character.isDigit(phone.charAt(i))) {
							newPhone.append(phone.charAt(i));
						}
					}
					if (newPhone.length() == 10) {
						phone = newPhone.toString();
					}
				}

				if (phone.length() == 10) {
					try {
						areaCode = phone.substring(0, 3);
						exchange = phone.substring(3, 6);
						extension = phone.substring(6);
					} catch (Exception e) {
						; // ignore
					}
					ntoh.setDeliveryPhoneAreaCode(areaCode);
					ntoh.setDeliveryPhoneExchange(exchange);
					ntoh.setDeliveryPhoneExtension(extension);
				}

			}
			// ncs rep id
			if (ncsRepID != null && ncsRepID.length() > 0) {
				if (ncsRepID.length() > 64) {
					ncsRepID = ncsRepID.substring(0, 64);
				}
				ntoh.setNcsRepID(ncsRepID);
			}

			request.setAttribute("newSampleOrderHandler", ntoh);

			// determine forward location
			String partnerLandingPage = partner.getCustomLandingPageURL();
			if (partnerLandingPage != null && partnerLandingPage.length() > 0) {
				request.getRequestDispatcher(partnerLandingPage).forward(request, response);
				return null;
			} else {
				// use default forward for samples (probably the error page initially)
				forwardPath = "default";
			}

		} catch (Exception e) {

			System.out.println("Electronic Samples Partner LookUp: " + e.getMessage());
			forwardPath = "failure";
		}

		forward = mapping.findForward(forwardPath);

		// Finish with
		return (forward);

	}
}
