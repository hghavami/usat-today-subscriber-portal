package com.usatoday.struts.actions.eEdition;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.usatoday.serviceHandler.trials.TrialSignUp;

/**
 * @version 1.0
 * @author
 */
public class TrialAddAction extends Action

{

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		ActionForward forward = null; // return value
		StringBuilder sb = new StringBuilder("<TrialCustomerAddResponse>");

		try {

			String partnerID = request.getParameter("partnerID");
			String partnerIP = request.getParameter("partnerIP");
			String clubNumber = request.getParameter("clubNumber");
			String firstName = request.getParameter("firstName");
			String lastName = request.getParameter("lastName");
			String address1 = request.getParameter("address1");
			String apartmentSuite = request.getParameter("apartmentSuite");
			String city = request.getParameter("city");
			String state = request.getParameter("state");
			String zip = request.getParameter("zip");
			String phone = request.getParameter("phone");
			String emailAddress = request.getParameter("emailAddress");
			String password = request.getParameter("password");
			String hash = request.getParameter("hash");
			String showQueryString = request.getParameter("debug");

			if (showQueryString != null && showQueryString.equalsIgnoreCase("Y")) {
				String queryString = request.getQueryString();
				if (queryString != null) {
					System.out.println("API Trial Add Query String (GET): " + queryString);
				} else {
					System.out.println("API Trial Add Query String (POST): PID: " + partnerID + " IP: " + partnerIP + " ClubNum: "
							+ clubNumber + " FirstName: " + firstName + " Last Name: " + lastName + " Email: " + emailAddress
							+ " Password: " + password + " ADDR1: " + address1 + " APT: " + apartmentSuite + " City: " + city
							+ " State: " + state + " Zip: " + zip + " phone: " + phone + " HASH: " + hash);
				}
			}

			TrialSignUp newTrial = new TrialSignUp();
			String responseString = newTrial.addTrialSubscriber(partnerID, partnerIP, clubNumber, firstName, lastName, address1,
					apartmentSuite, city, state, zip, phone, emailAddress, password, hash);

			if (newTrial.isLastAddSuccessful()) {
				sb.append("<Added>Y</Added><Detail>http://ee.usatoday.com</Detail>");
			} else {
				sb.append("<Added>N</Added><Detail>");
				sb.append(responseString).append("</Detail>");
			}
			sb.append("</TrialCustomerAddResponse>");

			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.getWriter().write(sb.toString());
			response.getWriter().close();

		} catch (Exception e) {

			try {
				if (response.isCommitted() == false) {

					sb = new StringBuilder("<TrialCustomerAddResponse><Added>N</Added><Detail>");
					sb.append(e.getMessage()).append("</Detail></TrialCustomerAddResponse>");

					response.setContentType("text/xml");
					response.setHeader("Cache-Control", "no-cache");
					response.getWriter().write(sb.toString());
					response.getWriter().close();
				}
			} catch (Exception exp) {
			}
			System.out.println("TrialAddAction() - Failed to add trial user. Exception: " + e.getMessage() + " Query String : "
					+ request.getQueryString());
		}

		// Finish with
		return (forward);

	}
}
