package com.usatoday.struts.actions.eEdition;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.usatoday.business.interfaces.customer.CustomerIntf;
import com.usatoday.business.interfaces.customer.SubscriberAccountIntf;
import com.usatoday.business.interfaces.customer.TrialCustomerIntf;
import com.usatoday.business.interfaces.products.SubscriptionProductIntf;
import com.usatoday.businessObjects.customer.LoginException;
import com.usatoday.businessObjects.products.SubscriptionProductBO;
import com.usatoday.businessObjects.products.utils.ElectronicEditionFulfillmentHelper;
import com.usatoday.esub.common.UTCommon;
import com.usatoday.esub.electronic.utils.EReaderOpenRecorder;
import com.usatoday.util.MobileDeviceDetectionUtil;
import com.usatoday.util.constants.UsaTodayConstants;

/**
 * This action is used for the daily email reminder links or from Olive if they send us someone to validte. It attempt to validate
 * based on a cookie stored on the user's computer and sends to login page if none found.
 * 
 * @version 1.0
 * @author
 */
public class ElectronicEditionOlivePrimaryAuthAction extends Action

{

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		ActionForward forward = null; // return value

		String forwardLocation = "failure";
		boolean directToPaper = false;

		CustomerIntf customer = null;
		SubscriptionProductIntf product = null;

		String fromParam = null;
		try {

			// Save From parameter from Olive
			fromParam = request.getParameter("From");

			customer = UTCommon.authenticateViaRememberMeCookie(request, response);

			if (customer != null) {

				if (customer.getNumberOfAccounts() == 0) {
					product = SubscriptionProductBO.getSubscriptionProduct(customer.getCurrentEmailRecord().getPubCode());

					if (product.isElectronicDelivery() || product.getProductCode().equalsIgnoreCase(UsaTodayConstants.UT_PUBCODE)) {
						forwardLocation = "eEditionPreAccountSetupLaunchPage";
						directToPaper = true;
					} else {
						// should never get here
						throw new LoginException(LoginException.NO_AS400_ACCOUNT_SETUP);
					}
				} else {
					if (customer.getNumberOfAccounts() == 1) {
						product = SubscriptionProductBO.getSubscriptionProduct(customer.getCurrentAccount().getPubCode());

						if (product.isElectronicDelivery()) {
							// eEdition subscriber
							forwardLocation = "eEditionLaunchPage";
							directToPaper = true;
						} else {
							if (product.getProductCode().equalsIgnoreCase(UsaTodayConstants.UT_PUBCODE)) {
								// print subscriber
								forwardLocation = "printSubscriberLaunchPage";
								directToPaper = true;
							} else {
								// sports weekly print subscribers go to account information page
								forwardLocation = "accountInformation";
							}
						}
					} // end if customer has a single account
					else {
						// for now we just go to the eEdition Launch Page
						String tempForward = null;
						String accountNumber = null;
						for (SubscriberAccountIntf account : customer.getAccounts().values()) {
							product = SubscriptionProductBO.getSubscriptionProduct(account.getPubCode());
							if (product.isElectronicDelivery()) {
								tempForward = "eEditionLaunchPage";
								accountNumber = account.getAccountNumber();
								directToPaper = true;
								break;
							} else if (product.getProductCode().equalsIgnoreCase(UsaTodayConstants.UT_PUBCODE)) {
								tempForward = "printSubscriberLaunchPage";
								accountNumber = account.getAccountNumber();
								directToPaper = true;
								break;
							}
						}
						if (tempForward == null) {
							tempForward = "accountInformation";
						} else {
							// load account information so authentication filter will work properly.
							customer.setCurrentAccount(accountNumber);
						}
						forwardLocation = tempForward;
					}

				}
			} // end if customer authenticated via cookie
			else {

				TrialCustomerIntf trialCustomer = UTCommon.authenticateTrialCustomerViaRememberMeCookie(request);

				if (trialCustomer != null && !trialCustomer.getCurrentInstance().getIsTrialPeriodExpired()) {
					forwardLocation = "trialSubscription";
				} else {
					// no cookie set so go to login page
					forwardLocation = "loginPage";

					// save from param and direct to paper attributes.
					if (fromParam != null && fromParam.length() > 0) {
						request.getSession().removeAttribute("eEditionRedirectURL");
						request.getSession().setAttribute("eEditionRedirectURL", fromParam);
					}
					request.getSession().removeAttribute("eEditionDirectTOPaperFlag");
					request.getSession().setAttribute("eEditionDirectTOPaperFlag", Boolean.TRUE);

				}
			}

		} catch (Exception e) {

			StringBuilder message = new StringBuilder();

			message.append("e-Newspaper Authorization Failed: ");
			if (e instanceof LoginException) {
				LoginException le = (LoginException) e;
				message.append(le.getErrorMessage());
			} else {
				message.append(e.getMessage());
			}
			request.getSession().removeAttribute("LOGIN_FAILED_MESSAGE");
			request.getSession().setAttribute("LOGIN_FAILED_MESSAGE", message.toString());
			forwardLocation = "loginPage";
		}

		if (directToPaper) {

			String eEditionLink = null;
			try {

				if (product == null) {

					product = SubscriptionProductBO.getSubscriptionProduct(UsaTodayConstants.DEFAULT_UT_EE_PUBCODE);
				} else if (!product.isElectronicDelivery()) {
					if (product.getProductCode().equalsIgnoreCase(UsaTodayConstants.UT_PUBCODE)) {
						product = SubscriptionProductBO.getSubscriptionProduct(UsaTodayConstants.DEFAULT_UT_EE_PUBCODE);
					} else {
						// future for when we do SW
						forwardLocation = "loginPage";
						throw new Exception("Unsupported Electronic Edition product.");
					}
				}

				// this block of code will first detect what we believe is the best viewer. If they have clicked an override link
				// we will ignore what we think is best and use what they selected.
				boolean useAlternate = MobileDeviceDetectionUtil.isMobileDevice(request);
				String overrideProductParam = request.getParameter("epref");
				if (overrideProductParam != null) {
					if (overrideProductParam.equalsIgnoreCase("html")) {
						useAlternate = true;
					} else {
						if (overrideProductParam.equalsIgnoreCase("flash")) {
							useAlternate = false;
						}
					}
				}

				if (fromParam != null && fromParam.length() > 0) {
					eEditionLink = ElectronicEditionFulfillmentHelper.buildOliveFulfillmentURL(product, customer
							.getCurrentEmailRecord().getEmailAddress(), fromParam, null, useAlternate);
				} else {
					eEditionLink = ElectronicEditionFulfillmentHelper.buildOliveFulfillmentURL(product, customer
							.getCurrentEmailRecord().getEmailAddress(), null, null, useAlternate);
				}

				// record open send redirect
				EReaderOpenRecorder.recordEReaderOpen(request);

				response.sendRedirect(eEditionLink);

				forward = null;
			} catch (Exception e) {
				System.out
						.println("ElectronicEditionOlivePrimaryAuthAction() - Failed to auto redirect to paper..sending non auto page: "
								+ forwardLocation);
				System.out.println("Exception causing issue: " + e.getClass().getName() + " : " + e.getMessage());
				if (customer != null && customer.getCurrentEmailRecord() != null) {
					System.out.println("Email Address of customer that failed: "
							+ customer.getCurrentEmailRecord().getEmailAddress());
				} else {
					System.out.println("No current email for failed customer olive auth.");
				}
				// Forward control to the appropriate URI (change name as desired)
				forward = mapping.findForward(forwardLocation);

			}
		} else {
			// Forward control to the appropriate URI (change name as desired)
			forward = mapping.findForward(forwardLocation);

			if (UsaTodayConstants.debug) {
				System.out.println("e-Newspaper Email Link Authorization.....Sending to : " + forwardLocation);
			}
		}

		// Finish with
		return (forward);

	}
}
