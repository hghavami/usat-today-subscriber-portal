package com.usatoday.struts.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.joda.time.DateTime;

import com.usatoday.business.interfaces.products.SubscriptionProductIntf;
import com.usatoday.businessObjects.products.SubscriptionProductBO;
import com.usatoday.esub.common.BWPubDates;
import com.usatoday.esub.common.PublicationDates;
import com.usatoday.esub.common.UTCommon;
import com.usatoday.esub.common.UTPubDates;
import com.usatoday.util.constants.UsaTodayConstants;

/**
 * @version 1.0
 * @author
 */
public class ActionValidatePubDate extends Action

{

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		ActionForward forward = null; // return value
		StringBuffer sb = new StringBuffer("<pubDateValid>");

		String pub = UTCommon.UTPUBCODE;

		SubscriptionProductIntf product = null;
		try {

			String earliestStart = "";
			String targetDate = request.getParameter("selectedDate");
			String tranType = request.getParameter("tranType");
			HttpSession session = request.getSession();
			pub = (String) session.getAttribute(UTCommon.SESSION_PUB_NAME);
			pub = pub == null ? UTCommon.UTPUBCODE : pub;

			product = SubscriptionProductBO.getSubscriptionProduct(pub);

			if (!PublicationDates.verifyInputDate(targetDate)) {
				sb.append("<datevalid>false</datevalid>");
				sb.append("<nextValidDate>");
				if (tranType.equals(UTCommon.TRAN_NEW_START_DATE)) { // Check for new start date processing
					DateTime dt = product.getEarliestPossibleStartDate();
					sb.append(dt.toString("M/d/yyyy"));
				}
				if (tranType.equals(UTCommon.TRAN_STOP_DATE) || tranType.equals(UTCommon.TRAN_START_DATE)) { // Check for vac hold
																												// date processing
					// sb.append(VacationHoldsServlet.determineEarliestStart(pub, null));
					DateTime eStart = product.getEarliestPossibleHoldStartDate();
					sb.append(eStart.toString("M/d/yyyy"));
				}
				sb.append("</nextValidDate>");
				sb.append("<message>Invalid start date format. Must be in MM/DD/YYYY form.</message>");
				sb.append("</pubDateValid>");
				response.setContentType("text/xml");
				response.setHeader("Cache-Control", "no-cache");
				response.getWriter().write(sb.toString());
				response.flushBuffer();
				response.getWriter().close();
				return null;
			}

			PublicationDates pd = null;
			boolean isUT = true;
			if (product.getBrandingPubCode().equalsIgnoreCase(UsaTodayConstants.UT_PUBCODE)) {
				pd = new UTPubDates();
			} else {
				isUT = false;
				pd = new BWPubDates();
			}
			pd.setPubCode(product.getBrandingPubCode());
			pd.setPubDate(targetDate);

			boolean isFutureDate = pd.isFuturePubDate(tranType);

			boolean pastMaxConfigured = false;

			if (isFutureDate && tranType.equalsIgnoreCase(UTCommon.TRAN_NEW_START_DATE)) {
				pastMaxConfigured = pd.isDatePastConfiguredMaximum();
			}

			if (isFutureDate && !pastMaxConfigured && pd.validPubDate()) {
				// date is good.
				sb.append("<datevalid>true</datevalid>");
			} else {
				String futureDateString = "";
				// date is bad
				if (tranType.equals(UTCommon.TRAN_NEW_START_DATE)) { // Check for new start date processing
					if (!pastMaxConfigured) {
						earliestStart = pd.calcNextStartDate();
					} else {
						DateTime now = new DateTime();
						now = now.plusDays(pd.getMaxDaysInFuture());
						futureDateString = now.toString("M/d/yyyy");
						pd.setPubDate(futureDateString);
						int count = 14;
						earliestStart = futureDateString;
						// back up a max of 14 days to the next valid pub date.
						while (count > 0 && !pd.validPubDate()) {
							count--;
							now = now.minusDays(1);
							earliestStart = now.toString("M/d/yyyy");
							pd.setPubDate(earliestStart);
						}

					}
				}
				if (tranType.equals(UTCommon.TRAN_STOP_DATE) || tranType.equals(UTCommon.TRAN_START_DATE)) { // Check for vac hold
																												// date processing
					earliestStart = product.getEarliestPossibleHoldStartDate().toString("M/d/yyyy");
				}

				sb.append("<datevalid>false</datevalid>");
				sb.append("<nextValidDate>");
				sb.append(earliestStart);
				sb.append("</nextValidDate>");
				sb.append("<message>");

				if (isFutureDate && !pastMaxConfigured) {
					if (isUT) {
						sb.append("USA TODAY ");
					} else {
						sb.append("Sports Weekly ");
					}
					sb.append("is not published on ").append(targetDate)
							.append(". The next possible start date after your date is ").append(earliestStart)
							.append("</message>");
				} else {
					if (pastMaxConfigured) {
						if (pub.equalsIgnoreCase(UTCommon.UTPUBCODE)) {
							sb.append("USA TODAY ");
							sb.append("new starts must begin within ").append(pd.getMaxDaysInFuture());
							sb.append(" days in the future. The first publishing day on or before ").append(futureDateString)
									.append(".</message>");
						} else {
							sb.append("Sports Weekly ");
							sb.append("new starts must begin on a Wednesday within ").append(pd.getMaxDaysInFuture());
							sb.append(" days in the future. The first publishing day on or before ").append(futureDateString)
									.append(".</message>");
						}
					} else {
						if (tranType.equals(UTCommon.TRAN_START_DATE) || tranType.equals(UTCommon.TRAN_NEW_START_DATE)) { // Issue
																															// VAC
																															// HOLD
																															// start
																															// message
							sb.append("We are unable to start your subscription on ").append(targetDate)
									.append(". The earliest possible start date is ").append(earliestStart).append("</message>");
						}
						if (tranType.equals(UTCommon.TRAN_STOP_DATE)) { // Issue VAC HOLD stop message
							sb.append("We are unable to stop your subscription on ").append(targetDate)
									.append(". The earliest possible stop date is ").append(earliestStart).append("</message>");
						}
					}
				}
			}

			sb.append("</pubDateValid>");

			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.getWriter().write(sb.toString());
			response.getWriter().close();

		} catch (Exception e) {

			try {
				if (response.isCommitted() == false) {

					sb = new StringBuffer("<pubDateValid><datevalid>false</datevalid><nextValidDate>");
					DateTime dt = product.getEarliestPossibleStartDate();
					sb.append(dt.toString("M/d/yyyy"))
							.append("</nextValidDate><message>We are unable to verify the specified publication date. It is likely too far into the future.</message></pubDateValid>");
					response.setContentType("text/xml");
					response.setHeader("Cache-Control", "no-cache");
					response.getWriter().write(sb.toString());
					response.getWriter().close();
				}
			} catch (Exception exp) {
				System.out.println("ActionValidatePubDate() - Failed to validate pub date. Original Exception: " + e.getMessage()
						+ " \nSecondary Exception: " + exp.getMessage());
			}
		}

		// Finish with
		return (forward);

	}
}
