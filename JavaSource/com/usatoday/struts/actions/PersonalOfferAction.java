package com.usatoday.struts.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 * @version 1.0
 * @author
 */
public class PersonalOfferAction extends Action

{

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward(); // return value

		try {

			/*
			 * // do something here String name = request.getParameter("ref");
			 * 
			 * String firstName = null; String lastName = null;
			 * 
			 * // parse name int nameLength = name.length(); // sip first character int lastNameStartIndex = 0; for (int i=1; i <
			 * nameLength; i++) { if (Character.isUpperCase(name.charAt(i))) { lastNameStartIndex = i; break; } }
			 * 
			 * firstName = name.substring(0,lastNameStartIndex); lastName = name.substring(lastNameStartIndex);
			 * 
			 * request.getSession().setAttribute("personaliziatonFName", firstName);
			 * request.getSession().setAttribute("personaliziatonLName", lastName); // forward to order entry page
			 * System.out.println("Name: " + firstName + " " + lastName);
			 */

		} catch (Exception e) {

			// Report the error using the appropriate name and ID.
			errors.add("name", new ActionMessage("id"));

		}

		// If a message is required, save the specified key(s)
		// into the request for use by the <struts:errors> tag.

		if (!errors.isEmpty()) {
			saveErrors(request, errors);

			// Forward control to the appropriate 'failure' URI (change name as desired)
			forward = mapping.findForward("failure");

		} else {

			// Forward control to the appropriate 'success' URI (change name as desired)
			forward = mapping.findForward("success");

		}

		// Finish with
		return (forward);

	}
}
