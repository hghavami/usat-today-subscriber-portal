package com.usatoday.struts.actions;

import java.util.Enumeration;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.SubscriptionOfferIntf;
import com.usatoday.business.interfaces.products.promotions.premiums.PremiumAttributeIntf;
import com.usatoday.businessObjects.products.SubscriptionOfferManager;
import com.usatoday.businessObjects.products.promotions.premiums.PremiumAttributeBO;
import com.usatoday.businessObjects.products.promotions.premiums.PremiumPromotion;
import com.usatoday.esub.common.UTCommon;
import com.usatoday.esub.subscriptions.MPFOnePageHelperBean;

/**
 * @version 1.0
 * @author
 * @deprecated Not used in Genesys
 */
public class MpfProcessorV2Action extends Action

{

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward(); // return value

		MPFOnePageHelperBean mpfBean = new MPFOnePageHelperBean();

		// get the referring page
		String referer = request.getHeader("referer");

		mpfBean.setOriginalLandingPage(referer);

		try {
			// Temporary holders
			String pubCodeParam = null;
			String keyCodeParam = null;
			String rateCodeParam = null;
			String termParam = null;
			boolean isGift = false;
			boolean sendToGiftPayer = false;

			// the mpfTerms should be in the form [pub_keycode_ratecode_rate_term]
			String terms = request.getParameter("mpfTerms");
			String[] tokens = this.parseParameter(terms);
			if (tokens == null || tokens.length != 5) {
				// Invalid form field
				errors.add("mpfTerms", new ActionMessage("com.usatoday.mpf.terms.format"));
				throw new Exception("Data Entry Error");
			} else {
				// process terms fields
				pubCodeParam = tokens[0];
				keyCodeParam = tokens[1];
				rateCodeParam = tokens[2];
				termParam = tokens[4];
			}

			mpfBean.setPubCode(pubCodeParam);
			mpfBean.setKeyCode(keyCodeParam);
			mpfBean.setTerm(termParam);
			mpfBean.setRateCode(rateCodeParam);

			// determine if it is a gift subscription
			String isGiftStr = request.getParameter("mpfIsGift");
			if (isGiftStr != null && isGiftStr.equalsIgnoreCase("Y")) {
				isGift = true;
				String temp = request.getParameter("mpfSendToGiftPayer");
				if (temp != null && temp.equalsIgnoreCase("Y")) {
					sendToGiftPayer = true;
				}
			}

			// get Start Date
			String startDate = request.getParameter("dataArea0");

			HttpSession session = request.getSession();

			SubscriptionOfferIntf offer = null;
			try {
				offer = SubscriptionOfferManager.getInstance().getOffer(keyCodeParam, pubCodeParam);
				session.removeAttribute("SUBSCRIPTION_OFFER");
				session.setAttribute("SUBSCRIPTION_OFFER", offer);
			} catch (UsatException e) {
				errors.add("mpfTerms", new ActionMessage("com.usatoday.mpf.terms.format"));
				referer = "/subscriptions/order/checkout.faces?pub=" + pubCodeParam + "&keycode=" + keyCodeParam;
				session.removeAttribute("MPFV2");
				session.removeAttribute("MPFOnePageHelper");
				session.removeAttribute("premiumPromotionHandler");
				throw new Exception("Expired Offer");
			}

			// reset session data
			session.removeAttribute(UTCommon.SESSION_KEYCODE);
			session.setAttribute(UTCommon.SESSION_KEYCODE, keyCodeParam);
			session.removeAttribute(UTCommon.SESSION_PUB_NAME);
			session.setAttribute(UTCommon.SESSION_PUB_NAME, pubCodeParam);
			session.removeAttribute(UTCommon.SESSION_PUB_BANNER);
			if (pubCodeParam.equalsIgnoreCase("UT")) {
				session.setAttribute(UTCommon.SESSION_PUB_BANNER, UTCommon.PUB_BANNERS[0]);
			} else {
				session.setAttribute(UTCommon.SESSION_PUB_BANNER, UTCommon.PUB_BANNERS[1]);
			}

			mpfBean.setStartDate(startDate);
			mpfBean.setGiftSubscription(isGift);

			PremiumPromotion promotion = new PremiumPromotion();

			String premiumPromoCodeParam = request.getParameter("mpfPremiumPromoCode");

			promotion.setPremiumPromoCode(premiumPromoCodeParam);

			// save landing page for complete page redirect
			promotion.setLandingPage(referer);

			String descriptiveText = request.getParameter("mpfDescriptionText");
			if (descriptiveText != null) {
				promotion.setDescriptionText(descriptiveText);
			}

			String emailText = request.getParameter("mpfEmailText");
			if (emailText != null) {
				promotion.setEmailText(emailText);
			}

			promotion.setSendToGiftPayer(sendToGiftPayer);

			// iterate over remaining form fields and process as attributes
			Enumeration<String> enumer = request.getParameterNames();

			while (enumer.hasMoreElements()) {
				String currentName = enumer.nextElement();
				if (currentName.startsWith("mpf") || currentName.startsWith("dataArea")) {
					// skip the non attribute fields
					continue;
				}

				PremiumAttributeBO par = new PremiumAttributeBO();

				try {
					par.setPremiumPromotionCode(premiumPromoCodeParam);
					par.setPubCode(pubCodeParam);

					par.setAttributeValue(request.getParameter(currentName));

					tokens = this.parseParameter(currentName);
					if (tokens == null || tokens.length != 3) {
						errors.add(ActionErrors.GLOBAL_MESSAGE, new ActionMessage("com.usatoday.mpf.terms.format"));
						referer += "?error=505&msg=INVALID MPF FORM SUBMITTED: Field (" + currentName
								+ ") Should be in format of [PremiumAttributeName_PremCode_PremType]";
						throw new Exception("Invalid MPF Form Submittted");

					}

					par.setAttributeName(tokens[0]);
					par.setPremiumCode(tokens[1]);
					par.setPremiumType(tokens[2]);
					if (sendToGiftPayer) {
						par.setGiftPayerReceive("Y");
					}

					par.setState(PremiumAttributeIntf.NEW);

					promotion.addAttribute(par);

				} catch (Exception e) {
					System.out.println("MpfProcessorServlet::doPost(): Exception creating attributes: " + e.getMessage());
					e.printStackTrace();
					errors.add(ActionErrors.GLOBAL_MESSAGE, new ActionMessage("com.usatoday.mpf.terms.format"));
				}
			} // end while more paramaters

			// remove any old premium object
			session.removeAttribute("MPFV2");

			session.setAttribute("MPFV2", promotion);

			session.removeAttribute("MPFOnePageHelper");
			session.setAttribute("MPFOnePageHelper", mpfBean);

		} catch (Exception e) {

			// Report the error using the appropriate name and ID.
			errors.add(ActionErrors.GLOBAL_MESSAGE, new ActionMessage("com.usatoday.mpf.landingPageError", e.getMessage()));

		}

		// If a message is required, save the specified key(s)
		// into the request for use by the <struts:errors> tag.

		if (!errors.isEmpty()) {
			saveErrors(request, errors);
			response.sendRedirect(response.encodeRedirectURL(referer));
			return null;
		}
		// Write logic determining how the user should be forwarded.
		forward = mapping.findForward("success");

		// Finish with
		return (forward);

	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	private String[] parseParameter(String source) {
		String[] tokenArray = null;
		if (source != null) {
			StringTokenizer tokenizer = new StringTokenizer(source, "_");
			tokenArray = new String[tokenizer.countTokens()];

			for (int i = 0; i < tokenArray.length; i++) {
				tokenArray[i] = tokenizer.nextToken();
			}
		}

		return tokenArray;
	}

}
