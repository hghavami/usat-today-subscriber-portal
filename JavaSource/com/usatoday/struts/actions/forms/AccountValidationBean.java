package com.usatoday.struts.actions.forms;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * Form bean for a Struts application. Users may access 5 fields on this form:
 * <ul>
 * <li>phoneAreaCode - [your comment here]
 * <li>accountNumber - [your comment here]
 * <li>phoneExtension - [your comment here]
 * <li>zip - [your comment here]
 * <li>phoneExchange - [your comment here]
 * </ul>
 * 
 * @version 1.0
 * @author
 */
public class AccountValidationBean extends ActionForm

{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6610027699531787055L;

	private String emailAddress = null;
	private String phoneAreaCode = null;

	private String accountNumber = null;

	private String phoneExtension = null;

	private String zip = null;

	private String phoneExchange = null;

	/**
	 * Get phoneAreaCode
	 * 
	 * @return String
	 */
	public String getPhoneAreaCode() {
		return phoneAreaCode;
	}

	/**
	 * Set phoneAreaCode
	 * 
	 * @param <code>String</code>
	 */
	public void setPhoneAreaCode(String p) {
		this.phoneAreaCode = p;
	}

	/**
	 * Get accountNumber
	 * 
	 * @return String
	 */
	public String getAccountNumber() {
		return accountNumber;
	}

	/**
	 * Set accountNumber
	 * 
	 * @param <code>String</code>
	 */
	public void setAccountNumber(String a) {
		this.accountNumber = a;
	}

	/**
	 * Get phoneExtension
	 * 
	 * @return String
	 */
	public String getPhoneExtension() {
		return phoneExtension;
	}

	/**
	 * Set phoneExtension
	 * 
	 * @param <code>String</code>
	 */
	public void setPhoneExtension(String p) {
		this.phoneExtension = p;
	}

	/**
	 * Get zip
	 * 
	 * @return String
	 */
	public String getZip() {
		return zip;
	}

	/**
	 * Set zip
	 * 
	 * @param <code>String</code>
	 */
	public void setZip(String z) {
		this.zip = z;
	}

	/**
	 * Get phoneExchange
	 * 
	 * @return String
	 */
	public String getPhoneExchange() {
		return phoneExchange;
	}

	/**
	 * Set phoneExchange
	 * 
	 * @param <code>String</code>
	 */
	public void setPhoneExchange(String p) {
		this.phoneExchange = p;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		// Reset values are provided as samples only. Change as appropriate.

		emailAddress = null;
		phoneAreaCode = null;
		accountNumber = null;
		phoneExtension = null;
		zip = null;
		phoneExchange = null;

	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		String reqAccountNumber = request.getParameter("AID");
		// if valid account number on initial request simply move on
		if (reqAccountNumber != null && reqAccountNumber.trim().length() > 0) {
			return errors;
		}

		// if valid account number
		if (this.emailAddress == null || this.emailAddress.trim().length() == 0) {
			errors.add("email", new org.apache.struts.action.ActionMessage("com.usatoday.validateAccount.invalidForm"));
		}
		if (this.accountNumber != null && this.accountNumber.trim().length() > 0) {
			; // account is valid just check zip
		} else {
			// not a valid account number check for valid phone
			if ((this.accountNumber != null && this.accountNumber.trim().length() > 0 && this.accountNumber.trim().length() != 9)
					|| this.phoneAreaCode == null || this.phoneExchange == null || this.phoneExtension == null) {
				// add action error invalid form submitted
				errors.add(Globals.ERROR_KEY,
						new org.apache.struts.action.ActionMessage("com.usatoday.validateAccount.invalidForm"));
			} else {
				String phone = this.phoneAreaCode.trim() + this.phoneExchange.trim() + this.phoneExtension.trim();
				if (phone.length() != 10) {
					// add action error invlid phone
					errors.add("phone", new org.apache.struts.action.ActionMessage("com.usatoday.validateAccount.invalidForm"));
				}
			}
		}

		if (this.zip == null || this.zip.trim().length() != 5) {
			// add zip action error
			errors.add("zip", new org.apache.struts.action.ActionMessage("com.usatoday.validateAccount.zip"));
		}

		return errors;

	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
}
