package com.usatoday.struts.actions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.struts.Globals;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.joda.time.DateTime;

import com.usatoday.business.interfaces.customer.EmailRecordIntf;
import com.usatoday.business.interfaces.customer.SubscriberAccountIntf;
import com.usatoday.business.interfaces.products.OfferIntf;
import com.usatoday.business.interfaces.products.RenewalOfferIntf;
import com.usatoday.businessObjects.customer.EmailRecordBO;
import com.usatoday.businessObjects.customer.SubscriberAccountBO;
import com.usatoday.esub.common.UTCommon;
import com.usatoday.struts.actions.forms.AccountValidationBean;
import com.usatoday.util.constants.UsaTodayConstants;

/**
 * @version 1.0
 * @author
 */
public class ValidateAccountForNexternalAction extends Action {

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward(); // return value
		AccountValidationBean accountValidationBean = (AccountValidationBean) form;

		DefaultHttpClient httpclient = new DefaultHttpClient();

		try {

			String accountID = request.getParameter("AID");
			// if (accountID == null) {
			// accountID = accountValidationBean.getAccountNumber();
			// }

			String nexternalProductID = request.getParameter("NPID");

			String pub = request.getParameter("PUB");
			if (pub != null) {
				pub = pub.trim();
				if (pub.length() != 2) {
					// invalid pub set to nul
					pub = null;
				}
			}
			if (nexternalProductID != null && nexternalProductID.trim().length() > 0) {
				request.getSession().removeAttribute("NEXTERNAL_PROD_ID");
				request.getSession().setAttribute("NEXTERNAL_PROD_ID", nexternalProductID);
			}
			if (accountID != null && accountID.trim().length() > 0) {
				// request if from an email link..check to see if account exist in Nexternal already.
				boolean integrationSuccessful = false;

				HttpPost httppost = new HttpPost(UTCommon.NEXTERNAL_INTEGRATION_URL);
				httppost.setHeader("Content-Type", "application/x-www-form-urlencoded");
				httppost.setHeader("REFERER", "https://service.usatoday.com/validateAccountForNexternal.do");
				httppost.setHeader("User-Agent", "USA TODAY Integration Client");

				List<NameValuePair> nvps = new ArrayList<NameValuePair>();
				nvps.add(new BasicNameValuePair("CUSTOM_FIELD1", accountID));
				nvps.add(new BasicNameValuePair("Match1", "CUSTOM_FIELD1"));
				nvps.add(new BasicNameValuePair("Mode", "Display-Only"));
				nvps.add(new BasicNameValuePair("Submit", "Shop Online"));
				if (pub != null) {
					nvps.add(new BasicNameValuePair("CUSTOM_FIELD6", pub));
					nvps.add(new BasicNameValuePair("Match2", "CUSTOM_FIELD6"));
				}
				httppost.setEntity(new UrlEncodedFormEntity(nvps, "utf-8"));

				HttpResponse reqResponse = httpclient.execute(httppost);
				// System.out.println("Integration form get: " + reqResponse.getStatusLine());

				HttpEntity entity = reqResponse.getEntity();

				// If the response does not enclose an entity, there is no need
				// to bother about connection release
				if (entity != null) {
					InputStream instream = entity.getContent();
					try {
						BufferedReader rd = new BufferedReader(new InputStreamReader(instream));
						String line;
						while ((line = rd.readLine()) != null) {
							// Process line...
							// System.out.println(line);
							if (line.indexOf("Success") > -1) {
								integrationSuccessful = true;
								break;
							}
						}
						rd.close();
						// do something useful with the response
					} catch (IOException ex) {
						// In case of an IOException the connection will be released
						// back to the connection manager automatically
						throw ex;
					} catch (RuntimeException ex) {
						// In case of an unexpected exception you may want to abort
						// the HTTP request in order to shut down the underlying
						// connection immediately.
						httppost.abort();
						throw ex;
					} finally {
						// Closing the input stream will trigger connection release
						try {
							instream.close();
						} catch (Exception ignore) {
						}
					}
				}

				EntityUtils.consume(entity);

				try {
					httpclient.getConnectionManager().shutdown(); // shut down the connection since following methods may call out
																	// on new connection.
				} catch (Exception e) {
					System.out.println("Failed to release HTTPPost connectio in nexternal integration. " + e.getMessage());
				}

				// already in Nexternal
				if (integrationSuccessful) {
					// redirect to Nexternal
					String nexternalProd = "";
					// Nexternal Category sew
					if (request.getSession().getAttribute(UTCommon.SESSION_NEXT_CATG) != null) {
						nexternalProd = (String) request.getSession().getAttribute(UTCommon.SESSION_NEXT_CATG);
					}
					if (request.getSession().getAttribute("NEXTERNAL_PROD_ID") != null) {
						nexternalProd = (String) request.getSession().getAttribute("NEXTERNAL_PROD_ID");
					}

					this.processSuccessRedirection(response.getWriter(), accountID, nexternalProd, pub);
				} else {

					// attempt to add customer
					SubscriberAccountIntf account = null;
					account = SubscriberAccountBO.getSubscriberAccount(accountID);

					if (account != null && pub != null) {
						if (!account.getPubCode().equalsIgnoreCase(pub)) {
							account = null; // retrieved account doesn't match pubs
						}
					}

					if (account != null) {
						integrationSuccessful = this.processAccountIntegration(account, null);

						if (integrationSuccessful) {
							String nexternalProd = "";

							// Nexternal Category sew
							if (request.getSession().getAttribute(UTCommon.SESSION_NEXT_CATG) != null) {
								nexternalProd = (String) request.getSession().getAttribute(UTCommon.SESSION_NEXT_CATG);
							}

							if (request.getSession().getAttribute("NEXTERNAL_PROD_ID") != null) {
								nexternalProd = (String) request.getSession().getAttribute("NEXTERNAL_PROD_ID");
							}

							this.processSuccessRedirection(response.getWriter(), account.getAccountNumber(), nexternalProd, pub);
						} else {
							// add error message
							errors.add(
									Globals.ERROR_KEY,
									new ActionMessage(
											"com.usatoday.validateAccount.unexpected",
											"We must validate your subscription account before continuing. Please enter your subscription account number OR phone number and your delivery zip code to continue."));
						}
					} else {
						// go to input form
						errors.add(
								Globals.ERROR_KEY,
								new ActionMessage(
										"com.usatoday.validateAccount.unexpected",
										"We must validate your subscription account before continuing. Please enter your subscription account number OR phone number and your delivery zip code to continue."));
					}
				}

			} else { // request is result of form submission not an email click

				String phone = accountValidationBean.getPhoneAreaCode().trim() + accountValidationBean.getPhoneExchange().trim()
						+ accountValidationBean.getPhoneExtension().trim();
				String emailAddress = accountValidationBean.getEmailAddress().trim();
				Collection<SubscriberAccountIntf> records = null;
				SubscriberAccountIntf account = null;
				if (accountValidationBean.getAccountNumber().trim().length() > 0) {
					// try to use account and zip
					account = SubscriberAccountBO.getSubscriberAccount(accountValidationBean.getAccountNumber());
					if (account != null
							&& !accountValidationBean.getZip().equalsIgnoreCase(
									account.getDeliveryContact().getPersistentAddress().getZip())) {
						account = null;
					}
				}

				if (account == null && phone != null && phone.trim().length() == 10) {
					// otherwise try phone
					records = SubscriberAccountBO.getSubscriberAccountsForPhoneAndZip(phone, accountValidationBean.getZip());
					if (records.size() > 0) {
						// use the first one
						account = (SubscriberAccountIntf) records.iterator().next();
					} else {
						errors.add(Globals.ERROR_KEY, new ActionMessage("com.usatoday.validateAccount.noneFound"));
					}
				}

				// attempt integration if account is not null
				if (account != null) {

					boolean integrationSuccessful = this.processAccountIntegration(account, emailAddress);

					if (integrationSuccessful) {
						String nexternalProd = "";

						// Nexternal Category sew
						if (request.getSession().getAttribute(UTCommon.SESSION_NEXT_CATG) != null) {
							nexternalProd = (String) request.getSession().getAttribute(UTCommon.SESSION_NEXT_CATG);
						}

						if (request.getSession().getAttribute("NEXTERNAL_PROD_ID") != null) {
							nexternalProd = (String) request.getSession().getAttribute("NEXTERNAL_PROD_ID");
						}

						this.processSuccessRedirection(response.getWriter(), account.getAccountNumber(), nexternalProd,
								account.getPubCode());
					} else {
						// add error message
						errors.add(
								Globals.ERROR_KEY,
								new ActionMessage(
										"com.usatoday.validateAccount.unexpected",
										"We were unable to automatically forward you to the online store. Please call National Customer Service for assistance at 1-800-872-0001."));
					}

				} // end if account != null
				else {
					if (errors.isEmpty()) {
						// no matching account found
						errors.add(Globals.ERROR_KEY, new ActionMessage("com.usatoday.validateAccount.noneFound"));
					}
				}
			}
		} catch (Exception e) {

			if (errors.isEmpty()) {
				// Report the error using the appropriate name and ID.
				errors.add(Globals.ERROR_KEY, new ActionMessage("com.usatoday.validateAccount.unexpected", e.getMessage()));
			}

		} finally {
			try {
				httpclient.getConnectionManager().shutdown();
			} catch (Exception eeee) {
			}
		}

		// If a message is required, save the specified key(s)
		// into the request for use by the <struts:errors> tag.

		if (!errors.isEmpty()) {
			saveErrors(request, errors);

			// Forward control to the appropriate 'failure' URI (change name as desired)
			forward = mapping.findForward("failure");

		} else {

			// Forward control to the appropriate 'success' URI (change name as desired)
			// nexternal will forward ?
			forward = null;

		}

		// Finish with
		return (forward);

	}

	/**
	 * This method generates a auto redirect page
	 * 
	 * @param out
	 */
	private void processSuccessRedirection(PrintWriter out, String accountNumber, String productID, String pub) {
		try {
			StringBuffer redirectContent = new StringBuffer();

			redirectContent
					.append("<HTML><SCRIPT language=\"javascript\">function submitForm(){document.Customer.submit();}</SCRIPT></HEAD><BODY bgColor=\"white\" leftMargin=\"0\" topMargin=\"0\" onload=\"javascript:submitForm()\">");
			redirectContent
					.append("<TABLE cellSpacing=\"0\" cellPadding=\"0\" width=\"930\" border=\"0\"><TBODY><TR height=\"126\"><TD vAlign=\"top\" align=\"left\" width=\"930\" colSpan=\"3\" height=\"126\"><IMG src=\"/shop/images/shop_header.jpg\" border=\"0\" ");
			redirectContent.append("</TD>	</TR></TBODY></TABLE>");

			redirectContent
					.append("<BR><BR><BR>Automatically Redirecting to Online Store. <BR>If this page does not redirect you within 5 seconds, try clicking the Shop Online button below.<BR><BR><FORM NAME=Customer METHOD=POST ACTION=");
			redirectContent.append(UTCommon.NEXTERNAL_INTEGRATION_URL);

			// Add Nexternal Category sew
			if (productID != null && productID.indexOf("Category") == 0) {
				String catId = productID.substring("Category".length());
				redirectContent.append("?CategoryID=").append(catId);
			} else {
				// or Add Nexternal Product
				if (productID != null && productID.trim().length() > 0) {
					redirectContent.append("?ProductID=").append(productID);
				}
			}

			redirectContent.append("><INPUT TYPE=Hidden NAME=CUSTOM_FIELD1 VALUE=\"");
			redirectContent.append(accountNumber).append("\">");
			if (pub != null) {
				redirectContent.append("<INPUT TYPE=Hidden NAME=CUSTOM_FIELD6 VALUE=\"");
				redirectContent.append(pub).append("\">");
				redirectContent.append("<INPUT TYPE=Hidden NAME=Match2 VALUE=\"CUSTOM_FIELD6\">");
			}
			redirectContent.append("<INPUT TYPE=Hidden NAME=Match1 VALUE=\"CUSTOM_FIELD1\">");
			redirectContent.append("<INPUT TYPE=Hidden NAME=Mode VALUE=\"Redirect-Failure\">");
			redirectContent.append("<INPUT TYPE=Hidden NAME=\"ReplyURL\" VALUE=\"");
			redirectContent.append(UTCommon.NEXTERNAL_REPLYTO_URL);
			redirectContent.append("\">");
			redirectContent.append("<INPUT TYPE=Submit VALUE=\"Shop Online\">");
			redirectContent.append("</FORM><BR>");

			redirectContent.append("</BODY></HTML>");

			out.write(redirectContent.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();

			System.out.println("Failed to process redirection to Nexternal.");
		}
	}

	/**
	 * 
	 * @param account
	 * @return
	 * @throws Exception
	 */
	// private boolean processAccountIntegration(SubscriberAccountIntf account) throws Exception {
	private boolean processAccountIntegration(SubscriberAccountIntf account, String emailAddress) throws Exception {
		// attempt to retrieve an email record
		Collection<EmailRecordIntf> emails = EmailRecordBO.getEmailRecordsForEmailAddress(emailAddress);
		EmailRecordIntf email = null;
		// if (emails.size() == 1) {
		if (emails.size() > 0) {
			email = (EmailRecordIntf) emails.iterator().next();
		}

		// /////////// NEW
		HttpClient httpclient = new DefaultHttpClient();
		boolean integrationSuccessful = false;
		try {
			HttpPost httppost = new HttpPost(UTCommon.NEXTERNAL_INTEGRATION_URL);
			httppost.setHeader("Content-Type", "application/x-www-form-urlencoded");
			httppost.setHeader("REFERER", "https://service.usatoday.com/validateAccountForNexternal.do");
			// httppost.setHeader("User-Agent",
			// "Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; .NET CLR 1.0.3705; .NET CLR 1.1.4322)");
			httppost.setHeader("User-Agent", "USA TODAY Integration Client");

			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			nvps.add(new BasicNameValuePair("LAST_NAME", account.getDeliveryContact().getLastName()));
			nvps.add(new BasicNameValuePair("FIRST_NAME", account.getDeliveryContact().getFirstName()));
			if (email != null) {
				nvps.add(new BasicNameValuePair("EMAIL", email.getEmailAddress()));
				nvps.add(new BasicNameValuePair("PASSWORD", email.getPassword()));
			} else {
				nvps.add(new BasicNameValuePair("EMAIL", "custorder@usatoday.com"));
			}

			nvps.add(new BasicNameValuePair("ADDRESS1", account.getDeliveryContact().getUIAddress().getAddress1()));
			nvps.add(new BasicNameValuePair("CITY", account.getDeliveryContact().getUIAddress().getCity()));
			nvps.add(new BasicNameValuePair("STATE", account.getDeliveryContact().getUIAddress().getState()));
			nvps.add(new BasicNameValuePair("POSTAL_CODE", account.getDeliveryContact().getUIAddress().getZip()));
			nvps.add(new BasicNameValuePair("COUNTRY", "US"));
			nvps.add(new BasicNameValuePair("ADDRESS_TYPE", "0"));
			nvps.add(new BasicNameValuePair("PHONE_NUM", account.getDeliveryContact().getHomePhone()));
			nvps.add(new BasicNameValuePair("PRIMARY_SHIP", "1"));
			nvps.add(new BasicNameValuePair("MAILING_LIST", "0"));
			nvps.add(new BasicNameValuePair("CUST_TYPE", "Subscriber"));
			nvps.add(new BasicNameValuePair("CUSTOM_FIELD1", account.getAccountNumber()));
			String rateCode = null;
			try {
				OfferIntf offer = account.getDefaultRenewalOffer();
				if (offer instanceof RenewalOfferIntf) {
					rateCode = ((RenewalOfferIntf) offer).getRateCode();
				}
			} catch (Exception e) {
			}
			if (rateCode == null) {
				rateCode = "";
			}
			nvps.add(new BasicNameValuePair("CUSTOM_FIELD2", account.getKeyCode() + rateCode));
			if (account.getDeliveryContact().getFirmName() != null && account.getDeliveryContact().getFirmName().length() > 0) {
				nvps.add(new BasicNameValuePair("CUSTOM_FIELD3", account.getDeliveryContact().getFirmName()));
			}
			DateTime dt = new DateTime(account.getStartDate().getTime());
			nvps.add(new BasicNameValuePair("CUSTOM_FIELD4", dt.toString("yyyyMMdd")));
			dt = new DateTime(account.getExpirationDate().getTime());
			nvps.add(new BasicNameValuePair("CUSTOM_FIELD5", dt.toString("yyyyMMdd")));
			nvps.add(new BasicNameValuePair("CUSTOM_FIELD6", account.getPubCode()));
			nvps.add(new BasicNameValuePair("Match1", "CUSTOM_FIELD1"));
			nvps.add(new BasicNameValuePair("Mode", "Display-Only"));
			nvps.add(new BasicNameValuePair("Submit", "Shop Online"));

			httppost.setEntity(new UrlEncodedFormEntity(nvps, "utf-8"));

			HttpResponse response = httpclient.execute(httppost);

			HttpEntity entity = response.getEntity();

			// If the response does not enclose an entity, there is no need
			// to bother about connection release
			if (entity != null) {
				InputStream instream = entity.getContent();
				try {
					BufferedReader rd = new BufferedReader(new InputStreamReader(instream));
					String line;
					while ((line = rd.readLine()) != null) {
						// Process line...
						if (UsaTodayConstants.debug) {
							System.out.println(line);
						}
						if (line.indexOf("Success") > -1) {
							integrationSuccessful = true;
							break;
						}
					}
					rd.close();
					// do something useful with the response
				} catch (IOException ex) {
					// In case of an IOException the connection will be released
					// back to the connection manager automatically
					throw ex;
				} catch (RuntimeException ex) {
					// In case of an unexpected exception you may want to abort
					// the HTTP request in order to shut down the underlying
					// connection immediately.
					httppost.abort();
					throw ex;
				} finally {
					// Closing the input stream will trigger connection release
					try {
						instream.close();
					} catch (Exception ignore) {
					}
				}
			}

			EntityUtils.consume(entity);

		} finally {
			// When HttpClient instance is no longer needed,
			// shut down the connection manager to ensure
			// immediate deallocation of all system resources
			httpclient.getConnectionManager().shutdown();
		}

		return integrationSuccessful;
	}
}
