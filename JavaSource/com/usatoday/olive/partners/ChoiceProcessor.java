package com.usatoday.olive.partners;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.joda.time.DateTime;

import com.gannett.usat.olive.partners.OlivePartner;
import com.gannett.usat.olive.partners.OlivePartnerAccessCode;
import com.gannett.usat.olive.partners.controller.OlivePartnerAccessCodeManager;
import com.gannett.usat.olive.partners.controller.OlivePartnerManager;
import com.gannett.usat.sp.partners.UsatEnewspaperPartnerAccessLog;
import com.gannett.usat.sp.partners.controller.UsatEnewspaperPartnerAccessLogManager;
import com.usatoday.business.interfaces.products.ProductIntf;
import com.usatoday.businessObjects.products.ProductBO;
import com.usatoday.businessObjects.products.utils.ElectronicEditionFulfillmentHelper;
import com.usatoday.esub.cookies.AuthenticationCookieProcessor;
import com.usatoday.util.MobileDeviceDetectionUtil;
import com.usatoday.util.constants.UsaTodayConstants;


public class ChoiceProcessor {

	public static String checkEnewspaperAvailable (HttpServletRequest request) {

		String eEditionLink = "";
		try {
//			HttpServletRequest request = (HttpServletRequest) this.getFacesContext().getExternalContext().getRequest();
//			HttpServletResponse response = (HttpServletResponse) this.getFacesContext().getExternalContext().getResponse();
			AuthenticationCookieProcessor cp = new AuthenticationCookieProcessor();
			Cookie cookie = cp.returnChoiceRememberMeCookie(request.getCookies());
			if (cookie != null) {
				String accessCode = cookie.getValue();
				if (accessCode != null && !accessCode.trim().equals("")) {
					EntityManagerFactory emf = null;
//					UsatEnewspaperPartnerAccessLog log = new UsatEnewspaperPartnerAccessLog();
					emf = Persistence.createEntityManagerFactory("USATDomainModel");
					String partnerID = UsaTodayConstants.CHOICE_OLIVE_PARTNER_ID;
//					String propertyID = "NONE";
					
					// Get access code information
					OlivePartnerAccessCodeManager partnerAccessCodeManager = new OlivePartnerAccessCodeManager(emf);
					List<OlivePartnerAccessCode> partnerListAccessCode = partnerAccessCodeManager.getAccessCode(accessCode);
					OlivePartnerAccessCode opac = null;
					if (partnerListAccessCode.size() > 0) {
						opac = partnerListAccessCode.iterator().next();
						partnerID = opac.getPartnerId();
						// Get partner information
						OlivePartnerManager partnerManager = new OlivePartnerManager(emf);
						List<OlivePartner> partnerList = partnerManager.getOlivePartnerId(partnerID);
						OlivePartner op = null;
						if (partnerList.size() > 0) {
							op = partnerList.iterator().next();
							ProductIntf product = ProductBO.fetchProductIncludeInactive("EE");
							boolean useAlternate = MobileDeviceDetectionUtil.isMobileDevice(request);
							eEditionLink = ElectronicEditionFulfillmentHelper.buildOliveFulfillmentURL(product, op.getPartnerId()
									.trim(), op.getOliveProductUrl().trim(), op.getOliveProductRestriction().trim(), useAlternate);
							System.out.print("Successfully redirected from Olive partner" + op.getPartnerDescription()
									+ " customer to USA TODAY e-Edition.");
							// Log the access
							UsatEnewspaperPartnerAccessLog log = new UsatEnewspaperPartnerAccessLog();
							DateTime now = new DateTime();
							log.setClientIp(request.getRemoteAddr().trim());
							log.setClientUserAgent(request.getHeader("user-agent"));
							log.setPartnerExternalKey("NONE");
							log.setPartnerId(partnerID);
							log.setPropertyId(opac.getPropertyId());
							log.setTimeStamp(new Timestamp(now.getMillis()));
							log.setResponseCode(UsatEnewspaperPartnerAccessLog.SUCCESS);
							log.setRedirectedTo(eEditionLink);
							log.setAccessCodeUsed(accessCode);
							UsatEnewspaperPartnerAccessLogManager logManager = new UsatEnewspaperPartnerAccessLogManager(emf);
							logManager.createUsatEnewspaperPartnerAccessLog(log);
						}
					}
				}
			}
		} catch (Exception e) {
			System.out.println("Could not process customer cookie access code, because: " + e);
		}
		return eEditionLink;
	}
}
