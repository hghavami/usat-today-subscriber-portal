/**
 * 
 */
package com.usatoday.olive.partners;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.gannett.usat.olive.partners.OlivePartner;
import com.gannett.usat.olive.partners.OlivePartnerAccessCode;
import com.gannett.usat.olive.partners.OlivePartnerIp;
import com.gannett.usat.olive.partners.controller.OlivePartnerAccessCodeManager;
import com.gannett.usat.olive.partners.controller.OlivePartnerIpManager3;
import com.gannett.usat.olive.partners.controller.OlivePartnerManager1;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.usatoday.businessObjects.util.mail.SmtpMailSender;
import com.usatoday.util.constants.UsaTodayConstants;

/**
 * @author hghavami
 * 
 */
public class GenerateOlivePartnersAccess {

	/**
	 * @param args
	 */

	public static String OLIVE_PARTNERS_URL = "";
	public static String OLIVE_PARTNERS_ACCESS_FILE_PATH = "";
	public static String EMAIL_SERVER = "10.209.4.25";
	public static String EMAIL_DEFAULT_FROM_ADDRESS = "*USA TODAY*<hghavami@usatoday.com>";

	// public static void main(String[] args) {
	public static void generateEmailAccessCode(String partnerId) {
		// This class will generate an access code, write it to the
		// OlivePartnersAccessCode table, and send this access code in an email
		// to all partners listed in the OlivePartner table
		// Check if partner ID is provided
		if (partnerId == null || partnerId.trim().equals("")) {
			// Generate access code for all partner IDs
			EntityManagerFactory emf = Persistence.createEntityManagerFactory("USATDomainModel");
			OlivePartnerManager1 partnerManager = new OlivePartnerManager1(emf);
			List<OlivePartner> partnerList = partnerManager.getAllOlivePartner();
			OlivePartner op = null;
			if (partnerList.size() > 0) {
				Iterator<OlivePartner> itr = partnerList.iterator();
				while (itr.hasNext()) {
					op = itr.next();
					boolean accessAllowed = false;
					Timestamp sd = op.getStartDate();
					Timestamp ed = op.getEndDate();
					Date cd = new Date();
					Timestamp cts = new Timestamp(cd.getTime());
					if (sd != null && cd != null && cts.after(sd) && cts.before(ed)) {
						accessAllowed = true;
					}
					if (op.getPartnerId() != null && !op.getPartnerId().trim().equals("") && accessAllowed) {
						processPartnerIdEmailAccessCode(op.getPartnerId().trim());
					}
				}
			}
		} else {
			// Generate access code for the given partner ID
			processPartnerIdEmailAccessCode(partnerId);
		}
	}

	private static void processPartnerIdEmailAccessCode(String partnerId) {

		// Process access code for each property in the PartnerIp table
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("USATDomainModel");
		OlivePartnerIpManager3 partnerIpManager = new OlivePartnerIpManager3(emf);
		List<OlivePartnerIp> partnerListIp = partnerIpManager.getOlivePartnerId(partnerId);
		OlivePartnerIp opi = null;
		if (partnerListIp.size() > 0) {
			Iterator<OlivePartnerIp> itr = partnerListIp.iterator();
			while (itr.hasNext()) {
				opi = itr.next();
				if (opi.getEmailAddress() != null && !opi.getEmailAddress().trim().equals("")) {
					// First generate the new access code
					String accessCode = generateAccessCode();
					if (!writeOlivePartnerAccessCode(accessCode, partnerId, opi.getPropertyId())) {
						System.out.println("Olive Partner Access Code: Could not write the new code to the table. ");
					} else {
						// Email the new code plus the URL to use to all users
						if (!sendAccessCodeToPartners(accessCode, opi)) {
							System.out.println("Olive Partner Access Code: Could not send the new access code to partners. ");
						}
					}
				}
			}
		}
		return;
	}

	private static String generateAccessCode() {
		String accessCode = "";
		int len = 6;
		String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		Random rnd = new Random();

		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++)
			sb.append(AB.charAt(rnd.nextInt(AB.length())));

		accessCode = sb.toString();
		return accessCode;
	}

	private static boolean writeOlivePartnerAccessCode(String accessCode, String partnerId, String propertyId) {

		boolean success = true;
		try {
			// Write to the OlivePartnerAccessCode table
			EntityManagerFactory emf = null;
			emf = Persistence.createEntityManagerFactory("USATDomainModel");
			OlivePartnerAccessCodeManager partnerAccessCodeManager = new OlivePartnerAccessCodeManager(emf);
			OlivePartnerAccessCode opac = new OlivePartnerAccessCode();
			opac.setAccessCode(accessCode);
			Date cd = new Date();
			opac.setDateTime(new Timestamp(cd.getTime()));
			opac.setStartDate(new Timestamp(cd.getTime()));
			// set end date a week ahead
			Timestamp ed = new Timestamp(cd.getTime());
			Calendar cal = Calendar.getInstance();
			cal.setTime(opac.getStartDate());
			cal.add(Calendar.DAY_OF_WEEK, UsaTodayConstants.OLIVE_PARTNER_ACCESS_CODE_DURATION);
			ed.setTime(cal.getTime().getTime());
			opac.setEndDate(ed);
			opac.setPartnerId(partnerId);
			opac.setPropertyId(propertyId);
			partnerAccessCodeManager.createOlivePartnerAccessCode(opac);
		} catch (Exception e) {
			System.out.println("Olive Partner Access Code: Could not write access code to its table, because: " + e);
			success = false;
		}
		return success;
	}

	private static boolean sendAccessCodeToPartners(String accessCode, OlivePartnerIp opi) {
		boolean success = true;
		try {
			generateQRCodes(accessCode, opi);
			sendAccessEmail(accessCode, opi);
		} catch (Exception e) {
			System.out.println("Olive Partner Access Code: Could not send access code to customer, because: " + e);
		}

		return success;
	}

	private static void generateQRCodes(String accessCode, OlivePartnerIp opi) throws WriterException, IOException {

		if (opi.getQrLink() != null && !opi.getQrLink().trim().equals("")) {
			String qrCodeLink = opi.getQrLink();
			if (qrCodeLink.contains("?")) {
				qrCodeLink = qrCodeLink.replaceAll("\\?", "?ac=" + accessCode);
			}
			String filePath = UsaTodayConstants.OLIVE_PARTNERS_ACCESS_FILE_PATH.trim()
					+ UsaTodayConstants.OLIVE_PARTNERS_ACCESS_FILE_NAME.trim() + opi.getPropertyName().trim()
					+ UsaTodayConstants.OLIVE_PARTNERS_ACCESS_FILE_EXT.trim();
			int size = 250;
			String fileType = "png";
			File qrFile = new File(filePath);
			createQRImage(qrFile, qrCodeLink, size, fileType);
			System.out.println("Done with creating the QR code for partner:" + opi.getPartnerId().trim() + ". File path is "
					+ filePath);
		}
		// else {
		// qrCodeLink = UsaTodayConstants.OLIVE_PARTNERS_URL;
		// }
	}

	private static void createQRImage(File qrFile, String qrCodeLink, int size, String fileType) throws WriterException,
			IOException {
		// Create the ByteMatrix for the QR-Code that encodes the given String
		Hashtable<EncodeHintType, ErrorCorrectionLevel> hintMap = new Hashtable<EncodeHintType, ErrorCorrectionLevel>();
		hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
		QRCodeWriter qrCodeWriter = new QRCodeWriter();
		BitMatrix byteMatrix = qrCodeWriter.encode(qrCodeLink, BarcodeFormat.QR_CODE, size, size, hintMap);
		// Make the BufferedImage that are to hold the QRCode
		int matrixWidth = byteMatrix.getWidth();
		BufferedImage image = new BufferedImage(matrixWidth, matrixWidth, BufferedImage.TYPE_INT_RGB);
		image.createGraphics();

		Graphics2D graphics = (Graphics2D) image.getGraphics();
		graphics.setColor(Color.WHITE);
		graphics.fillRect(0, 0, matrixWidth, matrixWidth);
		// Paint and save the image using the ByteMatrix
		graphics.setColor(Color.BLACK);

		for (int i = 0; i < matrixWidth; i++) {
			for (int j = 0; j < matrixWidth; j++) {
				if (byteMatrix.get(i, j)) {
					graphics.fillRect(i, j, 1, 1);
				}
			}
		}
		ImageIO.write(image, fileType, qrFile);
	}

	private static void sendAccessEmail(String accessCode, OlivePartnerIp opi) {

		try {
			// Construct the text
			String emailTextString = "";
			String emailContent = "";
			StringBuilder emailText = new StringBuilder();
			if (opi.getEmailContent() != null && !opi.getEmailContent().trim().equals("")) {
				emailContent = opi.getEmailContent();
				if (emailContent.contains("@@@@2")) {
					if (opi.getEmailLink() != null && !opi.getEmailLink().trim().equals("")) {
						emailContent = emailContent.replaceAll("@@@@2", opi.getEmailLink().trim());
					} else {
						emailContent = emailContent.replaceAll("@@@@2", UsaTodayConstants.OLIVE_PARTNERS_URL);
					}
				}
				if (emailContent.contains("@@@@3")) {
					emailContent = emailContent.replaceAll("@@@@3", "ac=" + accessCode);
				}
				if (emailContent.contains("@@@@1")) {
					if (opi.getPropertyName() != null && !opi.getPropertyName().trim().equals("")) {
						emailContent = emailContent.replaceAll("@@@@1", opi.getPropertyName().trim());
					}
				}
				if (emailContent.contains("@@@@4")) {
					emailContent = emailContent.replaceAll("@@@@4", accessCode);
				}
				if (emailContent.contains("@@@@5")) {
					emailContent = emailContent.replaceAll("@@@@5", UsaTodayConstants.OLIVE_PARTNERS_CONTACT_EMAIL);
				}

				if (emailContent.contains("'")) {
					emailContent = emailContent.replaceAll("'", "\'");
				}
				int j = emailContent.indexOf("\\n");
				if (j > 0) {
					emailText.append(emailContent.substring(0, j));
					emailText.append("\n");
					j = emailContent.indexOf("\\n") + 1;
					int k = j;
					for (int i = j; i < emailContent.length() - 1; i++) {
						if (emailContent.charAt(i) == '\\' && emailContent.charAt(i + 1) == 'n') {
							if (k < i) {
								emailText.append(emailContent.substring(k, i - 1));
							}
							emailText.append("\n");
							System.out.println(emailText.toString());
							k = i + 2;
							i++;
						}
					}
					emailText.append(emailContent.substring(k));
				}
				emailTextString = emailText.toString();
			} else {
				emailText.append("Hello " + opi.getPropertyName().trim() + ":\n\n");
				emailText.append("The following is this week's guest access code for the USA TODAY e-Newspaper: \n\n");
				emailText.append("This Week's Access Code: " + accessCode + "\n");
				emailText.append("Link to USA TODAY e-Newspaper: " + opi.getEmailLink().trim() + "\n\n");
				emailText
						.append("Your guests may visit "
								+ opi.getEmailLink().trim()
								+ " and enter this week's access code from any internet connected device to enjoy the USA TODAY e-Newspaper for the next seven (7) days.  ");

				emailText
						.append("We encourage you to distribute this access code along with the e-Newspaper URL through the best methods for your hotel: \n\n");
				emailText.append("-  Signs at the front desk \n");
				emailText.append("-  Sign in the breakfast or common areas \n");
				emailText.append("-  In room messaging \n\n");
				emailText.append("If you have questions about the USA TODAY e-Newspaper, please call 1-800-872-0001 or email "
						+ UsaTodayConstants.OLIVE_PARTNERS_CONTACT_EMAIL.trim() + ".\n\n");
				emailText.append(" Sincerely, \n\n");
				emailText.append("The USA TODAY Team \n\n");
				emailText
						.append("Note: Distribution of this weekly access code and e-Newspaper URL to non-hotel guests is prohibited. \n\n");
				emailTextString = emailText.toString();
			}

			// Send email to partner
			SmtpMailSender mail = new SmtpMailSender();
			mail.setMailServerHost(UsaTodayConstants.EMAIL_SERVER);
			mail.setSender(UsaTodayConstants.EMAIL_DEFAULT_FROM_ADDRESS);
			mail.addTORecipient(opi.getEmailAddress());
			mail.setMessageSubject(UsaTodayConstants.OLIVE_PARTNERS_ACCESS_EMAIL_SUBJECT);
			if (opi.getQrLink() != null && !opi.getQrLink().trim().equals("")) {
				String filePath = UsaTodayConstants.OLIVE_PARTNERS_ACCESS_FILE_PATH.trim()
						+ UsaTodayConstants.OLIVE_PARTNERS_ACCESS_FILE_NAME.trim() + opi.getPropertyName().trim()
						+ UsaTodayConstants.OLIVE_PARTNERS_ACCESS_FILE_EXT.trim();
				mail.addAttachment(filePath);
				mail.setMessageText(emailTextString);
			}
			mail.setMessageText(emailTextString);
			try {
				mail.sendMessage();
			} catch (Exception e) {
				System.out.println("Olive Partner Access Code: Could not email access code to partner, because: " + e);
				e.printStackTrace();
			}
		} catch (Exception e) {
			System.out.println("Olive Partner Access Code: Could not email access code to partner, because: " + e);
		}
	}
}
