package com.usatoday.olive.partners;

import java.net.InetAddress;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTime;

import com.gannett.usat.olive.partners.OlivePartner;
import com.gannett.usat.olive.partners.OlivePartnerAccessCode;
import com.gannett.usat.olive.partners.OlivePartnerIp;
import com.gannett.usat.olive.partners.controller.OlivePartnerAccessCodeManager;
import com.gannett.usat.olive.partners.controller.OlivePartnerIpManager2;
import com.gannett.usat.olive.partners.controller.OlivePartnerManager;
import com.gannett.usat.sp.partners.UsatEnewspaperPartnerAccessLog;
import com.gannett.usat.sp.partners.controller.UsatEnewspaperPartnerAccessLogManager;
import com.usatoday.business.interfaces.products.ProductIntf;
import com.usatoday.businessObjects.products.ProductBO;
import com.usatoday.businessObjects.products.utils.ElectronicEditionFulfillmentHelper;
import com.usatoday.businessObjects.util.IPtoLongConversion;
import com.usatoday.util.MobileDeviceDetectionUtil;
import com.usatoday.util.constants.UsaTodayConstants;

public class OlivePartnersProcessor {

	public static void OlivePartners(ServletContext application, HttpServletRequest request, HttpServletResponse response,
			String clientIP, String accessCode) {

		String eEditionLink = null;
		// use the ee partner table to look up which product to use for
		// fulfillment and build links according to that.
		EntityManagerFactory emf = null;
		String partnerID = "";
		String propertyID = "";

		try {
			DateTime now = new DateTime();
			// Instantiate the log
			UsatEnewspaperPartnerAccessLog log = new UsatEnewspaperPartnerAccessLog();
			// If you create an EntityManagerFactory you must call the dispose
			// method when you are done using it.
			emf = Persistence.createEntityManagerFactory("USATDomainModel");
			// First check the incoming access code
			if (accessCode != null && !accessCode.trim().equals("")) {
				OlivePartnerAccessCodeManager partnerAccessCodeManager = new OlivePartnerAccessCodeManager(emf);
				List<OlivePartnerAccessCode> partnerListAccessCode = partnerAccessCodeManager.getAccessCode(accessCode);
				OlivePartnerAccessCode opac = null;
				if (partnerListAccessCode.size() > 0) {
					opac = partnerListAccessCode.iterator().next();
					// Check access code still is valid
					Timestamp sd = opac.getStartDate();
					Timestamp ed = opac.getEndDate();
					Date cd = new Date();
					Timestamp cts = new Timestamp(cd.getTime());
					if (sd == null || ed == null || cts.before(sd) || cts.after(ed)) {
						opac = null;
					} else {
						// Set the partner ID to be used for the Olive Partner
						// table
						partnerID = opac.getPartnerId();
						propertyID = opac.getPropertyId();
					}
				}
				if (opac == null) {
					log.setClientIp(clientIP);
					log.setClientUserAgent(request.getHeader("user-agent"));
					log.setPartnerExternalKey("NONE");
					log.setPartnerId("NONE");
					log.setPropertyId("NONE");
					log.setTimeStamp(new Timestamp(now.getMillis()));
					log.setResponseCode(UsatEnewspaperPartnerAccessLog.FAILED_INVALID);
					log.setRedirectedTo(UsaTodayConstants.EEDITION_CHOICE_OLIVE_PARTNER_ERROR_PAGE);
					log.setAccessCodeUsed(accessCode);
					UsatEnewspaperPartnerAccessLogManager logManager = new UsatEnewspaperPartnerAccessLogManager(emf);
					logManager.createUsatEnewspaperPartnerAccessLog(log);
					System.out.print("Failed to redirect from travel partner to USA TODAY e-Edition, because IP address was invalid.");
					response.sendRedirect(UsaTodayConstants.EEDITION_CHOICE_OLIVE_PARTNER_ERROR_PAGE);
					return;
				}
			}
			// Second check the incoming IP address is between the 2 IP
			// addresses
			boolean ipFound = false;
			if (accessCode == null || accessCode.trim().equals("")) {
				OlivePartnerIpManager2 partnerIpManager = new OlivePartnerIpManager2(emf);
				List<OlivePartnerIp> partnerListIp = partnerIpManager.getOlivePartnerIp();
				OlivePartnerIp opi = null;
				if (partnerListIp.size() > 0) {
					Iterator<OlivePartnerIp> itr = partnerListIp.iterator();
					while (itr.hasNext()) {
						opi = itr.next();
						if (opi.getClientIp() != null && !opi.getClientIp().trim().equals("")) {
							// Check end IP address
							if (opi.getClientIpEnd() != null && !opi.getClientIpEnd().trim().equals("")) {
								long ipLo = IPtoLongConversion.ipToLong(InetAddress.getByName(opi.getClientIp().trim()));
								long ipToTest = IPtoLongConversion.ipToLong(InetAddress.getByName(clientIP));
								long ipHi = IPtoLongConversion.ipToLong(InetAddress.getByName(opi.getClientIpEnd().trim()));
								if (ipLo <= ipToTest && ipToTest <= ipHi) {
									partnerID = opi.getPartnerId();
									propertyID = opi.getPropertyId();
									ipFound = true;
									break;
								}
								// No end IP address, just check the begin IP address
							} else if (opi.getClientIp().trim().equals(clientIP)) {
								partnerID = opi.getPartnerId();
								propertyID = opi.getPropertyId();
								ipFound = true;
								break;
							}
						}
					}
				}
				if (!ipFound) {
					log.setClientIp(clientIP);
					log.setClientUserAgent(request.getHeader("user-agent"));
					log.setPartnerExternalKey("NONE");
					log.setPartnerId("NONE");
					log.setPropertyId("NONE");
					log.setTimeStamp(new Timestamp(now.getMillis()));
					log.setResponseCode(UsatEnewspaperPartnerAccessLog.FAILED_INVALID);
					log.setRedirectedTo(UsaTodayConstants.EEDITION_CHOICE_OLIVE_PARTNER_ERROR_PAGE);
					UsatEnewspaperPartnerAccessLogManager logManager = new UsatEnewspaperPartnerAccessLogManager(emf);
					logManager.createUsatEnewspaperPartnerAccessLog(log);
					System.out.print("Failed to redirect from travel partner to USA TODAY e-Edition, because IP address was invalid.");
					response.sendRedirect(UsaTodayConstants.EEDITION_CHOICE_OLIVE_PARTNER_ERROR_PAGE);
					return;
				}
			}

			OlivePartnerManager partnerManager = new OlivePartnerManager(emf);
			List<OlivePartner> partnerList = partnerManager.getOlivePartnerId(partnerID);
			OlivePartner op = null;
			if (partnerList.size() > 0) {
				op = partnerList.iterator().next();
			}

			if (op == null || !op.isCurrentlyActive()) {
				log.setClientIp(clientIP);
				log.setClientUserAgent(request.getHeader("user-agent"));
				log.setPartnerExternalKey("NONE");
				log.setPartnerId(partnerID);
				log.setPropertyId(propertyID);
				log.setAccessCodeUsed(accessCode);
				log.setTimeStamp(new Timestamp(now.getMillis()));
				log.setResponseCode(UsatEnewspaperPartnerAccessLog.FAILED_NOT_ACTIVE);
				if (op == null) {
					log.setRedirectedTo(UsaTodayConstants.EEDITION_CHOICE_OLIVE_PARTNER_ERROR_PAGE);
				} else {
					log.setRedirectedTo(op.getErrorPage());
				}
				UsatEnewspaperPartnerAccessLogManager logManager = new UsatEnewspaperPartnerAccessLogManager(emf);
				logManager.createUsatEnewspaperPartnerAccessLog(log);
				if (op == null) {
					System.out.print("Failed to redirect from travel partner to USA TODAY e-Edition, because partner does not exist.");
				} else {
					System.out.print("Failed to redirect from travel partner to USA TODAY e-Edition, because partner is not active.");
				}
				response.sendRedirect(UsaTodayConstants.EEDITION_CHOICE_OLIVE_PARTNER_ERROR_PAGE);
				return;
			} else {
				ProductIntf product = ProductBO.fetchProductIncludeInactive("EE");
				boolean useAlternate = MobileDeviceDetectionUtil.isMobileDevice(request);
				eEditionLink = ElectronicEditionFulfillmentHelper.buildOliveFulfillmentURL(product, op.getPartnerId().trim(), op
						.getOliveProductUrl().trim(), op.getOliveProductRestriction().trim(), useAlternate);
				System.out.print("Successfully redirected from travel partner to USA TODAY e-Edition.");
				log.setClientIp(clientIP);
				log.setClientUserAgent(request.getHeader("user-agent"));
				log.setPartnerExternalKey("NONE");
				log.setPartnerId(partnerID);
				log.setPropertyId(propertyID);
				log.setTimeStamp(new Timestamp(now.getMillis()));
				log.setResponseCode(UsatEnewspaperPartnerAccessLog.SUCCESS);
				log.setRedirectedTo(eEditionLink);
				log.setAccessCodeUsed(accessCode);
				UsatEnewspaperPartnerAccessLogManager logManager = new UsatEnewspaperPartnerAccessLogManager(emf);
				logManager.createUsatEnewspaperPartnerAccessLog(log);
				response.sendRedirect(eEditionLink);
				return;
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
