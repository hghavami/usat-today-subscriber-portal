package com.usatoday.olive.partners;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import com.gannett.usat.olive.partners.OlivePartner;
import com.gannett.usat.olive.partners.OlivePartnerAccessCode;
import com.gannett.usat.olive.partners.controller.OlivePartnerAccessCodeManager;
import com.gannett.usat.olive.partners.controller.OlivePartnerManager;
import com.usatoday.business.interfaces.products.ProductIntf;
import com.usatoday.businessObjects.products.ProductBO;
import com.usatoday.businessObjects.products.utils.ElectronicEditionFulfillmentHelper;
import com.usatoday.esub.cookies.AuthenticationCookieProcessor;
import com.usatoday.util.MobileDeviceDetectionUtil;
import com.usatoday.util.constants.UsaTodayConstants;


public class HudsonNewsProcessor {

	public boolean genrateHudsonNewsAccessCode(String propertyId) {
	
		boolean success = true;
		try {
			String accessCode = generateAccessCode();
			// Write to the OlivePartnerAccessCode table
			EntityManagerFactory emf = null;
			emf = Persistence.createEntityManagerFactory("USATDomainModel");
			OlivePartnerAccessCodeManager partnerAccessCodeManager = new OlivePartnerAccessCodeManager(emf);
			OlivePartnerAccessCode opac = new OlivePartnerAccessCode();
			opac.setAccessCode(accessCode);
			Date cd = new Date();
			opac.setDateTime(new Timestamp(cd.getTime()));
//			opac.setStartDate(new Timestamp(cd.getTime()));
			// Begin and end date will be populated when customer uses the access code
			Timestamp ed = new Timestamp(cd.getTime());
			Calendar cal = Calendar.getInstance();
			cal.setTime(opac.getStartDate());
			cal.add(Calendar.DAY_OF_WEEK, UsaTodayConstants.OLIVE_PARTNER_ACCESS_CODE_DURATION);
			ed.setTime(cal.getTime().getTime());
//			opac.setEndDate(ed);
			opac.setPropertyId(propertyId);
			partnerAccessCodeManager.createOlivePartnerAccessCode(opac);
		} catch (Exception e) {
			System.out.println("Olive Partner Access Code: Could not generate or write access code to its table, because: " + e);
			success = false;
		}
		return success;
	}

	public static boolean writeOlivePartnerAccessCode(String accessCode, String partnerId, String propertyId) {
	
		boolean success = true;
		try {
			// Write to the OlivePartnerAccessCode table
			EntityManagerFactory emf = null;
			emf = Persistence.createEntityManagerFactory("USATDomainModel");
			OlivePartnerAccessCodeManager partnerAccessCodeManager = new OlivePartnerAccessCodeManager(emf);
			OlivePartnerAccessCode opac = new OlivePartnerAccessCode();
			opac.setAccessCode(accessCode);
			opac.setPropertyId(propertyId);
			opac.setPartnerId(partnerId);
			Date cd = new Date();
			opac.setDateTime(new Timestamp(cd.getTime()));
			// start and end Dates will be updated when customer uses the access code to get e-edition
/*			opac.setStartDate(new Timestamp(cd.getTime()));
			// set end date a week ahead
			Timestamp ed = new Timestamp(cd.getTime());
			Calendar cal = Calendar.getInstance();
			cal.setTime(opac.getStartDate());
			cal.add(Calendar.DAY_OF_WEEK, UsaTodayConstants.OLIVE_PARTNER_ACCESS_CODE_DURATION);
			ed.setTime(cal.getTime().getTime());
			opac.setEndDate(ed);
*/
			partnerAccessCodeManager.createOlivePartnerAccessCode(opac);
		} catch (Exception e) {
			System.out.println("Hudson News Olive Partner Access Code: Could not write access code to its table, because: " + e);
			success = false;
		}
		return success;
	}

	private static String generateAccessCode() {
		String accessCode = "";
		int len = 6;
		String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		Random rnd = new Random();

		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++)
			sb.append(AB.charAt(rnd.nextInt(AB.length())));

		accessCode = sb.toString();
		return accessCode;
	}
	
	public static String checkEnewspaperAvailable (HttpServletRequest request) {

		String eEditionLink = "";
		try {
//			HttpServletRequest request = (HttpServletRequest) this.getFacesContext().getExternalContext().getRequest();
//			HttpServletResponse response = (HttpServletResponse) this.getFacesContext().getExternalContext().getResponse();
			AuthenticationCookieProcessor cp = new AuthenticationCookieProcessor();
			Cookie cookie = cp.returnOneDayRememberMeCookie(request.getCookies());
			if (cookie != null) {
				String accessCode = cookie.getValue();
				if (accessCode != null && !accessCode.trim().equals("")) {
					EntityManagerFactory emf = null;
//					UsatEnewspaperPartnerAccessLog log = new UsatEnewspaperPartnerAccessLog();
					emf = Persistence.createEntityManagerFactory("USATDomainModel");
					String partnerID = UsaTodayConstants.HUDSON_NEWS_OLIVE_PARTNER_ID;
//					String propertyID = "NONE";
					
					// Get access code information
					OlivePartnerAccessCodeManager partnerAccessCodeManager = new OlivePartnerAccessCodeManager(emf);
					List<OlivePartnerAccessCode> partnerListAccessCode = partnerAccessCodeManager.getAccessCode(accessCode);
					OlivePartnerAccessCode opac = null;
					if (partnerListAccessCode.size() > 0) {
						opac = partnerListAccessCode.iterator().next();
//						propertyID = opac.getPropertyId();
						partnerID = opac.getPartnerId();
						// Get partner information
						OlivePartnerManager partnerManager = new OlivePartnerManager(emf);
						List<OlivePartner> partnerList = partnerManager.getOlivePartnerId(partnerID);
						OlivePartner op = null;
						if (partnerList.size() > 0) {
							op = partnerList.iterator().next();
							ProductIntf product = ProductBO.fetchProductIncludeInactive("EE");
							boolean useAlternate = MobileDeviceDetectionUtil.isMobileDevice(request);
							eEditionLink = ElectronicEditionFulfillmentHelper.buildOliveFulfillmentURL(product, op.getPartnerId()
									.trim(), op.getOliveProductUrl().trim(), op.getOliveProductRestriction().trim(), useAlternate);
							System.out.print("Successfully redirected from Olive partner" + op.getPartnerDescription()
									+ " customer to USA TODAY e-Edition.");
						}
						// No log needed if cookie is present
/*						DateTime now = new DateTime();
						log.setClientIp(request.getRemoteAddr().trim());
						log.setClientUserAgent(request.getHeader("user-agent"));
						log.setPartnerExternalKey("NONE");
						log.setPartnerId(partnerID);
						log.setPropertyId(propertyID);
						log.setTimeStamp(new Timestamp(now.getMillis()));
						log.setResponseCode(UsatEnewspaperPartnerAccessLog.SUCCESS);
						log.setRedirectedTo(eEditionLink);
						log.setAccessCodeUsed(accessCode);
						UsatEnewspaperPartnerAccessLogManager logManager = new UsatEnewspaperPartnerAccessLogManager(emf);
						logManager.createUsatEnewspaperPartnerAccessLog(log);
						response.sendRedirect(eEditionLink);
*/					}
				}
			}
		} catch (Exception e) {
			System.out.println("Could not process customer cookie access code, because: " + e);
		}
		return eEditionLink;
	}
}
