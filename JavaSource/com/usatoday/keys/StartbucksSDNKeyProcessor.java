package com.usatoday.keys;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usatoday.UsatException;
import com.usatoday.crypto.USATBase64Encoder;
import com.usatoday.esub.common.UTCommon;

public class StartbucksSDNKeyProcessor {
	@SuppressWarnings("unused")
	public static StarbuckSDNKey starbucksSDNGetKey(ServletContext application, HttpServletRequest request,
			HttpServletResponse response, String companyId) throws UsatException {

		StarbuckSDNKey sbySDNKey = new StarbuckSDNKey();
		StarbuckSDNKey currSbySDNKey = new StarbuckSDNKey();
		sbySDNKey.setSecretKey("ERROR");
		// Determine what server (production or test) we are on
		String serverName = "";
		String serverIP = "";
		String clientIP = "";
		String clientName = "";
		String starbucksKeyServerURL = "";
		String userPassword = "";
		String theUserName = "";
		String thePassword = "";
		String encoding = "";
		long currKeyNxtUpd = 0;
		long currTime = 0;
		long currKeyExp = 0;
		String currKey = "";

		try {
			if (companyId.equals("starbucks")) { // Which server URL is coming from
				currSbySDNKey = (StarbuckSDNKey) application.getAttribute("StarBucksKey");
				// Check for key in the application level cache
				if (currSbySDNKey != null && currSbySDNKey instanceof StarbuckSDNKey) {
					currKey = currSbySDNKey.getSecretKey();
					currKeyNxtUpd = Long.parseLong(currSbySDNKey.getSecretKeyNxtUpd()); // Key next update time
					currKeyExp = Long.parseLong(currSbySDNKey.getSecretKeyExp()); // Key expiration time
					currTime = System.currentTimeMillis() / 1000L; // Current time in seconds
					currTime = currTime - 25200;
					if (currKey != null && !currKey.trim().equals("") && currKeyNxtUpd > currTime) {
						return currSbySDNKey;
					}
				}
			}
			// Get key from Starbucks
			java.net.InetAddress serverAddress = java.net.InetAddress.getLocalHost();
			serverIP = serverAddress.getHostAddress();
			serverName = serverAddress.getHostName();
			clientIP = request.getRemoteAddr();
			clientName = request.getRemoteHost();
			if (serverName.contains("moc-wn")) { // Production server
				if (companyId.equals("starbucks")) { // Which server URL is coming from
					starbucksKeyServerURL = ("https://openapi.starbucks.com/keyserver/v1/sharedkey?apikey=9zgqzacmj8n3z9vs5c67apjy");
				} else { // URL sent by Yahoo
					starbucksKeyServerURL = ("https://keys.starbucks.yahoo.com/sdnkeys/v1/getKey.php");
					theUserName = "sdn_key_usat_Zbv*";
					thePassword = "H.4RaXW3!";
				}
			} else { // Test server
				if (companyId.equals("starbucks")) { // Which server URL is coming from
					starbucksKeyServerURL = ("https://test.openapi.starbucks.com/keyserver/v1/sharedkey?apikey=4tw5bv5e69rmv98txassry35");
				} else { // URL sent by Yahoo
					starbucksKeyServerURL = ("https://keys.starbucks.yahoo.com/sdnkeys/v1/getKey.php");
					theUserName = "sdn_key_usat_Zbv*";
					thePassword = "H.4RaXW3!";
				}
			}
			if (!companyId.equals("starbucks")) { // URL sent by Yahoo
				userPassword = theUserName + ":" + thePassword;
				encoding = new USATBase64Encoder().encode(userPassword.getBytes());
			}
		} catch (Exception e) {
			String message = "Could not process the starbucks get key request, because: " + e;
			System.out.print(message);
		}

		// Create a trust manager that does not validate certificate chains
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
			}

			public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
			}
		} };

		// Install the all-trusting trust manager
		try {
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (Exception e) {
		}

		java.text.DateFormat dateFormat = new java.text.SimpleDateFormat("MM/dd/yyyy");
		java.util.Date date = new java.util.Date();
		String dateString = (String) dateFormat.format(date);
		String starbucksKeyServerParms = "";
		HttpURLConnection starbucksKeyServerConnection = null;
		try {
			URL url = new URL(starbucksKeyServerURL);
			starbucksKeyServerConnection = (HttpURLConnection) url.openConnection();
			if (!companyId.equals("starbucks")) { // URL sent by Yahoo
				starbucksKeyServerConnection.setRequestProperty("Authorization", "Basic " + encoding);
			}
			starbucksKeyServerConnection.setDoOutput(true);
			starbucksKeyServerConnection.setDoInput(true);
			starbucksKeyServerConnection.setUseCaches(false);
			starbucksKeyServerConnection.setRequestMethod("GET");
			/*
			 * DataOutputStream out = new DataOutputStream(starbucksKeyServerConnection.getOutputStream());
			 * System.out.print("Request method: " + starbucksKeyServerConnection.getRequestMethod().toString());
			 * out.writeBytes(starbucksKeyServerParms); System.out.print("Request method: " +
			 * starbucksKeyServerConnection.getRequestMethod().toString()); out.flush(); System.out.print("Request method: " +
			 * starbucksKeyServerConnection.getRequestMethod().toString()); out.close(); System.out.print("Request method: " +
			 * starbucksKeyServerConnection.getRequestMethod().toString());
			 */BufferedReader in = new BufferedReader(new InputStreamReader(starbucksKeyServerConnection.getInputStream()));
			String line = "";
			StringBuffer inputText = new StringBuffer();
			boolean starbucksKeyServerSuccess = false;
			while ((line = in.readLine()) != null) {
				if (line.indexOf("<secret_key") != -1) {
					starbucksKeyServerSuccess = true;
					inputText.append(line);
				}
			}
			if (!starbucksKeyServerSuccess) {
				String message = "Could not process the starbucks get key request. Please return to the previous page and press continue.";
				System.out.print(message);
				UTCommon.showError(request, response, 0, message);
			} else {
				line = inputText.toString();
				sbySDNKey.setSecretKey(line.substring(line.indexOf("<key>") + 5, line.indexOf("</key>")));
				sbySDNKey.setSecretKeyExp(line.substring(line.indexOf("<expiration>") + 12, line.indexOf("</expiration>")));
				sbySDNKey.setSecretKeyNxtUpd(line.substring(line.indexOf("<next_update>") + 13, line.indexOf("</next_update>")));
				if (companyId.equals("starbucks")) { // Which server URL is coming from
					if (sbySDNKey.getSecretKey() != null && !sbySDNKey.getSecretKey().equals("")) {
						// Store new key to cache
						application.setAttribute("StarBucksKey", sbySDNKey);
						if (currKey != null && !currKey.trim().equals("") && currKeyExp > currTime) {
							application.setAttribute("StarBucksKey", currSbySDNKey);
						}
					}
				}
			}
			in.close();
		} catch (Exception e) {
			String message = "Could not process the starbucks get key request, because: " + e;
			System.out.print(message);
			System.out.print("Request method: " + starbucksKeyServerConnection.getRequestMethod().toString());
			return currSbySDNKey;
		} finally {
		}
		return sbySDNKey;
	}

}
