package com.usatoday.keys;

public class StarbuckSDNKey {

	private String secretKey = null;
	private String secretKeyExp = null;
	private String secretKeyNxtUpd = null;

	public String getSecretKey() {
		return secretKey;
	}

	public String getSecretKeyExp() {
		return secretKeyExp;
	}

	public String getSecretKeyNxtUpd() {
		return secretKeyNxtUpd;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public void setSecretKeyExp(String secretKeyExp) {
		this.secretKeyExp = secretKeyExp;
	}

	public void setSecretKeyNxtUpd(String secretKeyNxtUpd) {
		this.secretKeyNxtUpd = secretKeyNxtUpd;
	}
}
