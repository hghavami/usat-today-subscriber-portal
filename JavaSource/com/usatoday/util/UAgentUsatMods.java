package com.usatoday.util;

import com.handinteractive.mobile.UAgentInfo;

public class UAgentUsatMods extends UAgentInfo {

	public static final String deviceGalaxyTab = "galaxy_tab"; // Samsung Galaxy Tab

	public UAgentUsatMods(String userAgent, String httpAccept) {
		super(userAgent, httpAccept);
	}

	/**
	 * Detects if the current device is a Galaxy Tab
	 * 
	 * @return detection of Galaxy Tab
	 */
	public boolean detectGalaxyTablet() {

		if (this.getUserAgent().indexOf(deviceGalaxyTab) != -1) {
			return true;
		}
		return false;
	}

	@Override
	public boolean detectAndroidTablet() {
		boolean isAndroidTablet = false;

		isAndroidTablet = super.detectAndroidTablet();

		if (!isAndroidTablet) {
			// super method does not detect Galaxy tab so we added it.
			if (this.detectGalaxyTablet()) {
				isAndroidTablet = true;
			}
		}
		return isAndroidTablet;
	}

}
