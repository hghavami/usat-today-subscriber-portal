package com.usatoday.util;

import javax.servlet.ServletContextAttributeEvent;
import javax.servlet.ServletContextAttributeListener;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.gannett.usat.iconapi.client.IconAPIContext;
import com.gannett.usat.userserviceapi.client.UserServiceAPIContext;
import com.gannett.usatoday.adminportal.appConfig.PortalApplicationRuntimeConfigurations;
import com.gannett.usatoday.adminportal.appConfig.PortalApplicationSettingIntf;
import com.usatoday.businessObjects.util.AS400StatusMonitor;
import com.usatoday.esub.common.CacheCleaner;
import com.usatoday.esub.common.UTCommon;
import com.usatoday.util.constants.UsaTodayConstants;

/**
 * Application Lifecycle Listener implementation class StartupShutDownListener
 * 
 */
public class StartupShutDownListener implements ServletContextListener, ServletContextAttributeListener {

	private Thread cacheCleanerThread = null;
	private Thread aS400statuMonitor = null;

	/**
	 * Default constructor.
	 */
	public StartupShutDownListener() {
		System.out.println("StartupShutDownListener:: USA TODAY Subscription Portal...LifeCycle Listener Loading.");
	}

	/**
	 * @see ServletContextAttributeListener#attributeRemoved(ServletContextAttributeEvent)
	 */
	public void attributeRemoved(ServletContextAttributeEvent arg0) {

	}

	/**
	 * @see ServletContextAttributeListener#attributeAdded(ServletContextAttributeEvent)
	 */
	public void attributeAdded(ServletContextAttributeEvent arg0) {

	}

	/**
	 * @see ServletContextListener#contextDestroyed(ServletContextEvent)
	 */
	public void contextDestroyed(ServletContextEvent sce) {
		// interrupt the cache cleaner.
		this.cacheCleanerThread.interrupt();

		try {
			this.cacheCleanerThread.join(5000);
		} catch (Exception e) {
			System.out.println("StartupShutDownListener::  Failed to stop cache cleaner thread.");
		}

		this.aS400statuMonitor.interrupt();
		try {
			this.aS400statuMonitor.join(5000);
		} catch (Exception e) {
			System.out.println("StartupShutDownListener::  Failed to stop as400 Status Monitor cleaner thread.");
		}

	}

	/**
	 * @see ServletContextListener#contextInitialized(ServletContextEvent)
	 */
	public void contextInitialized(ServletContextEvent sce) {

		// String sslFlagStr = config.getInitParameter("UsingSSL");
		// if ((sslFlagStr != null) && (sslFlagStr.trim().length() > 0)) {
		// Boolean aBool = new Boolean(sslFlagStr);
		// this.setUsingSSL(aBool.booleanValue());
		// }

		boolean usedSystemProperty = false;

		String path = System.getProperty("EXTRANET_INI_PROPERTY_NAME");

		if (path == null || path.trim().length() == 0) {
			path = sce.getServletContext().getInitParameter("EXTRANET_INI_PROPERTY_NAME");
		}
		String s2 = path;

		if (s2 != null && s2.trim().length() > 0) {
			String iniFileLoc = s2.trim();

			if (iniFileLoc != null && iniFileLoc.trim().length() > 0) {

				java.io.File file = new java.io.File(iniFileLoc);
				if (file.exists()) {
					UTCommon.INI_FILE = iniFileLoc;
					usedSystemProperty = true;
				}
				try {
					UTCommon.getInitValues();
					System.out.println("StartupShutDownListener::  Application initialized with system property file: "
							+ iniFileLoc);
				} catch (Exception e) {
					usedSystemProperty = false;
					try {
						System.out
								.println("StartupShutDownListener:: - Unable to initialize application from System Property. Trying servlet init param."
										+ e.getMessage());
					} catch (Exception ee) {
					}
				}
			}
		}

		if (!usedSystemProperty) {
			String iniFile = sce.getServletContext().getInitParameter("iniFile");
			if ((iniFile != null) && (iniFile.trim().length() > 0)) {
				iniFile = iniFile.trim();

				java.io.File file = new java.io.File(iniFile);
				if (file.exists()) {
					UTCommon.INI_FILE = iniFile;
				}
				try {
					UTCommon.getInitValues();
					System.out
							.println("StartupShutDownListener:: Application initialized with property file: " + UTCommon.INI_FILE);
				} catch (Exception e) {
					try {
						System.out.println("StartupShutDownListener::  - Unable to initialize application. " + e.getMessage());
					} catch (Exception ee) {
					}
				}
			}
		}

		System.out.println("USA TODAY Middle Tier Initializing....");
		com.usatoday.util.constants.UsaTodayConstants.loadProperties();
		System.out.println("USA TODAY Middle Tier Finished Initializing.");

		// Set up ICON API Context
		PortalApplicationRuntimeConfigurations appConfigs = new PortalApplicationRuntimeConfigurations();
		if (UsaTodayConstants.debug) {
			for (PortalApplicationSettingIntf setting : appConfigs.getAllICON_APISettings()) {
				System.out.println(setting.getKey() + " : '" + setting.getValue() + "'");
			}
		}
		// Set up Firefly API Context
		if (UsaTodayConstants.debug) {
			for (PortalApplicationSettingIntf setting : appConfigs.getAllFirefly_APISettings()) {
				System.out.println(setting.getKey() + " : '" + setting.getValue() + "'");
			}
		}

		// Set up Userservice API Context
		if (UsaTodayConstants.debug) {
			for (PortalApplicationSettingIntf setting : appConfigs.getAllUserService_APISettings()) {
				System.out.println(setting.getKey() + " : '" + setting.getValue() + "'");
			}
		}

		try {
			IconAPIContext.setEndPointURL(appConfigs.getSetting(PortalApplicationSettingIntf.ICON_API_BASE_URL).getValue());
			IconAPIContext.setEndPointSecureURL(appConfigs.getSetting(PortalApplicationSettingIntf.ICON_API_SECURE_BASE_URL)
					.getValue());
			IconAPIContext.setEndPointAddressURL(appConfigs.getSetting(PortalApplicationSettingIntf.ICON_API_ADDRESS_URL)
					.getValue());
			IconAPIContext.setEndPointSecureAddressURL(appConfigs.getSetting(
					PortalApplicationSettingIntf.ICON_API_SECURE_ADDRESS_URL).getValue());
			IconAPIContext.setEndPointSelfServeSecureURL(appConfigs.getSetting(
					PortalApplicationSettingIntf.ICON_API_SECURE_SELF_SERV_URL).getValue());
			IconAPIContext.setEndPointSelfServeSecureURLPath(appConfigs.getSetting(
					PortalApplicationSettingIntf.ICON_API_SECURE_SELF_SERV_URL_PATH).getValue());
			IconAPIContext.setEndPointSelfServeSecureURLMakeAPaymentPath(appConfigs.getSetting(
					PortalApplicationSettingIntf.ICON_API_SECURE_SELF_SERV_URL_MAKE_PAYMENT_PATH).getValue());
			IconAPIContext.setEndPointSelfServeSecureURLChangeCreditCardPath(appConfigs.getSetting(
					PortalApplicationSettingIntf.ICON_API_SECURE_SELF_SERV_URL_CHANGE_CARD_PATH).getValue());
			IconAPIContext.setEndPointSelfServeSecureURLSignUpEzpayPath(appConfigs.getSetting(
					PortalApplicationSettingIntf.ICON_API_SECURE_SELF_SERV_URL_CHANGE_CARD_EZPAY_PATH).getValue());

			IconAPIContext
					.setDebugMode(appConfigs.getSetting(PortalApplicationSettingIntf.ICON_API_DEBUG_MODE).getValueAsBoolean());
			IconAPIContext.setApiGCIUnitNumber(appConfigs.getSetting(PortalApplicationSettingIntf.ICON_API_GCI_UNIT).getValue());
			IconAPIContext.setApiUserID(appConfigs.getSetting(PortalApplicationSettingIntf.ICON_API_UID).getValue());
			IconAPIContext.setApiUserPwd(appConfigs.getSetting(PortalApplicationSettingIntf.ICON_API_PWD).getValue());
/*			FireflyAPIContext.setApiMarketId(appConfigs.getSetting(PortalApplicationSettingIntf.FIREFLY_API_MARKET_ID).getValue()); 
			FireflyAPIContext.setEndPointURL(appConfigs.getSetting(PortalApplicationSettingIntf.FIREFLY_API_BASE_URL).getValue());
			FireflyAPIContext.setEndPointUserServiceURL(appConfigs.getSetting(PortalApplicationSettingIntf.FIREFLY_API_BASE_USER_SERVICE_URL).getValue());
			FireflyAPIContext.setDebugMode(appConfigs.getSetting(PortalApplicationSettingIntf.FIREFLY_API_DEBUG_MODE).getValueAsBoolean());
			FireflyAPIContext.setApiUserID(appConfigs.getSetting(PortalApplicationSettingIntf.FIREFLY_API_UID).getValue());
			FireflyAPIContext.setApiUserPwd(appConfigs.getSetting(PortalApplicationSettingIntf.FIREFLY_API_PWD).getValue());
			FireflyAPIContext.setApiExternalSecret(appConfigs.getSetting(PortalApplicationSettingIntf.FIREFLY_API_EXTERNAL_SECRET).getValue());
			FireflyAPIContext.setApiInternalSecret(appConfigs.getSetting(PortalApplicationSettingIntf.FIREFLY_API_INTERNAL_SECRET).getValue());
*/
			UserServiceAPIContext.setApiMarketId(appConfigs.getSetting(PortalApplicationSettingIntf.USERSERVICE_API_MARKET_ID).getValue()); 
			UserServiceAPIContext.setEndPointURL(appConfigs.getSetting(PortalApplicationSettingIntf.USERSERVICE_API_BASE_URL).getValue());
			UserServiceAPIContext.setEndPointUserServiceURL(appConfigs.getSetting(PortalApplicationSettingIntf.USERSERVICE_API_BASE_USER_SERVICE_URL).getValue());
			UserServiceAPIContext.setDebugMode(appConfigs.getSetting(PortalApplicationSettingIntf.USERSERVICE_API_DEBUG_MODE).getValueAsBoolean());
			UserServiceAPIContext.setApiUserID(appConfigs.getSetting(PortalApplicationSettingIntf.USERSERVICE_API_UID).getValue());
			UserServiceAPIContext.setApiUserPwd(appConfigs.getSetting(PortalApplicationSettingIntf.USERSERVICE_API_PWD).getValue());
			UserServiceAPIContext.setApiExternalSecret(appConfigs.getSetting(PortalApplicationSettingIntf.USERSERVICE_API_EXTERNAL_SECRET).getValue());
			UserServiceAPIContext.setApiInternalSecret(appConfigs.getSetting(PortalApplicationSettingIntf.USERSERVICE_API_INTERNAL_SECRET).getValue());
			UserServiceAPIContext.setApiModifiedBy(appConfigs.getSetting(PortalApplicationSettingIntf.USERSERVICE_API_MODIFIED_BY).getValue());
			UserServiceAPIContext.setApiCredentialType(appConfigs.getSetting(PortalApplicationSettingIntf.USERSERVICE_API_CREDENTIAL_TYPE).getValue());
			UserServiceAPIContext.setApiOnSuccessRedirectUrl(appConfigs.getSetting(PortalApplicationSettingIntf.USERSERVICE_API_ON_SUCCESS_REDIRECT_URL).getValue());

		} catch (Exception e) {
			e.printStackTrace();
		}

		// Start the cach Cleaner
		System.out.println("StartupShutDownListener:: Initializing Cache Cleaner.");
		CacheCleaner cleaner = new CacheCleaner();
		cleaner.setStdoutStream(System.out);
		this.cacheCleanerThread = new Thread(cleaner);
		this.cacheCleanerThread.setName("USA TODAY Cache Cleaner Thread");

		this.cacheCleanerThread.start();

		System.out.println("StartupShutDownListener:: Initializing AS400 Monitor.");
		this.aS400statuMonitor = new AS400StatusMonitor();
		this.aS400statuMonitor.setName("USA TODAY iSeries Monitor Thread");

		this.aS400statuMonitor.start();

	}

	/**
	 * @see ServletContextAttributeListener#attributeReplaced(ServletContextAttributeEvent)
	 */
	public void attributeReplaced(ServletContextAttributeEvent scab) {

	}

}
