package com.usatoday.util;

import javax.servlet.http.HttpServletRequest;

import com.usatoday.util.constants.UsaTodayConstants;

/**
 * 
 * @author aeast - copied code from http://detectmobilebrowsers.com/
 * 
 *         I extracted the strings to the property file so we could change/update them more easily without an install.
 */
public class MobileDeviceDetectionUtil {

	/**
	 * 
	 * @param request
	 *            - Request Object
	 * @return true if it's a mobile device which seems to be handhelds/touchscreen devices also includes tablets
	 * 
	 */
	public static final boolean isMobileDevice(HttpServletRequest request) {

		boolean isMobileDevice = false;

		if (request == null) {
			return isMobileDevice;
		}
		String userAgent = null;
		String httpAccept = null;

		try {
			userAgent = request.getHeader("User-Agent").toLowerCase();

			if (UsaTodayConstants.debug) {
				System.out.println("User Agent: " + userAgent);
			}

			if (userAgent.matches(UsaTodayConstants.MOBILE_DEVICES_1)
					|| userAgent.substring(0, 4).matches(UsaTodayConstants.MOBILE_DEVICES_2)) {
				isMobileDevice = true;
			}

			httpAccept = request.getHeader("Accept").toLowerCase();

			UAgentUsatMods uAgent = new UAgentUsatMods(userAgent, httpAccept);

			if (uAgent.detectMobileLong() || uAgent.getIsTierTablet()) {
				isMobileDevice = true;
			}

		} catch (Exception e) {
			isMobileDevice = false;
		}

		if (UsaTodayConstants.debug) {
			System.out.println("Mobile Device Detected. : " + isMobileDevice);
		}
		return isMobileDevice;
	}

	/**
	 * 
	 * @param request
	 * @return true if we think it's a tablet device.
	 */
	public static final boolean isTabletDevice(HttpServletRequest request) {

		boolean isTabletDevice = false;

		if (request == null) {
			return isTabletDevice;
		}

		String userAgent = null;
		String httpAccept = null;
		try {
			userAgent = request.getHeader("User-Agent").toLowerCase();
			httpAccept = request.getHeader("Accept").toLowerCase();

			UAgentUsatMods uAgent = new UAgentUsatMods(userAgent, httpAccept);

			isTabletDevice = uAgent.detectTierTablet();

			if (UsaTodayConstants.debug) {
				System.out.println("Tablet Device Detected.  " + isTabletDevice);
			}
		} catch (Exception e) {
			isTabletDevice = false;
		}

		return isTabletDevice;
	}

	public static final boolean isDesktop(HttpServletRequest request) {

		boolean isDesktop = true;

		if (request == null) {
			return isDesktop;
		}

		isDesktop = !MobileDeviceDetectionUtil.isMobileDevice(request);

		if (isDesktop) {
			String userAgent = null;
			String httpAccept = null;
			try {
				userAgent = request.getHeader("User-Agent").toLowerCase();
				httpAccept = request.getHeader("Accept").toLowerCase();

				UAgentUsatMods uAgent = new UAgentUsatMods(userAgent, httpAccept);

				if (uAgent.detectTierIphone() || uAgent.detectTierOtherPhones() || uAgent.detectTierTablet()
						|| uAgent.detectTierRichCss()) {
					isDesktop = false;
				}
				if (UsaTodayConstants.debug) {
					System.out.println("Is Desktop:  " + isDesktop);
				}
			} catch (Exception e) {
				isDesktop = true;
			}
		}

		return isDesktop;
	}

	/**
	 * 
	 * @param request
	 * @return
	 */
	public static boolean isSmallScreenDevice(HttpServletRequest request) {
		boolean smallScreenDevice = false;

		if (MobileDeviceDetectionUtil.isMobileDevice(request)) {
			if (!MobileDeviceDetectionUtil.isTabletDevice(request)) {
				smallScreenDevice = true;
			}
		}

		return smallScreenDevice;
	}

	/**
	 * 
	 * @param requst
	 * @return
	 */
	public static boolean isIOSDevice(HttpServletRequest request) {
		boolean appleMobileDevice = false;

		if (MobileDeviceDetectionUtil.isMobileDevice(request)) {
			String userAgent = null;
			String httpAccept = null;
			try {
				userAgent = request.getHeader("User-Agent").toLowerCase();
				httpAccept = request.getHeader("Accept").toLowerCase();

				UAgentUsatMods uAgent = new UAgentUsatMods(userAgent, httpAccept);

				appleMobileDevice = uAgent.detectIos();

				if (UsaTodayConstants.debug) {
					System.out.println("Is Apple Mobile Device:  " + appleMobileDevice);
				}
			} catch (Exception e) {
				appleMobileDevice = false;
			}
		}
		return appleMobileDevice;
	}
}
