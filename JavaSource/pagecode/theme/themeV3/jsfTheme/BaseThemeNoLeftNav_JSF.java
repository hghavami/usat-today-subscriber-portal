/**
 * 
 */
package pagecode.theme.themeV3.jsfTheme;

import pagecode.PageCodeBase;
import javax.faces.component.UINamingContainer;

/**
 * @author aeast
 * 
 */
public class BaseThemeNoLeftNav_JSF extends PageCodeBase {

	protected UINamingContainer subviewHeader1;
	protected UINamingContainer subviewHeader2;
	protected UINamingContainer subviewBodyHeaderAreaRight1;
	protected UINamingContainer subviewBodyAreaMain1;
	protected UINamingContainer subviewFooter1;

	protected UINamingContainer getSubviewHeader1() {
		if (subviewHeader1 == null) {
			subviewHeader1 = (UINamingContainer) findComponentInRoot("subviewHeader1");
		}
		return subviewHeader1;
	}

	protected UINamingContainer getSubviewHeader2() {
		if (subviewHeader2 == null) {
			subviewHeader2 = (UINamingContainer) findComponentInRoot("subviewHeader2");
		}
		return subviewHeader2;
	}

	protected UINamingContainer getSubviewBodyHeaderAreaRight1() {
		if (subviewBodyHeaderAreaRight1 == null) {
			subviewBodyHeaderAreaRight1 = (UINamingContainer) findComponentInRoot("subviewBodyHeaderAreaRight1");
		}
		return subviewBodyHeaderAreaRight1;
	}

	protected UINamingContainer getSubviewBodyAreaMain1() {
		if (subviewBodyAreaMain1 == null) {
			subviewBodyAreaMain1 = (UINamingContainer) findComponentInRoot("subviewBodyAreaMain1");
		}
		return subviewBodyAreaMain1;
	}

	protected UINamingContainer getSubviewFooter1() {
		if (subviewFooter1 == null) {
			subviewFooter1 = (UINamingContainer) findComponentInRoot("subviewFooter1");
		}
		return subviewFooter1;
	}

}