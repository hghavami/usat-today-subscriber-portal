/**
 * 
 */
package pagecode.theme.themeV3.jsfTheme;

import pagecode.PageCodeBase;
import javax.faces.component.UINamingContainer;

/**
 * @author aeast
 * 
 */
public class BastThemeLeftNav_JSF extends PageCodeBase {

	protected UINamingContainer subviewBodyHeader1;
	protected UINamingContainer subviewNavPromo1;
	protected UINamingContainer subviewMainBody1;
	protected UINamingContainer subviewFooter1;

	protected UINamingContainer getSubviewBodyHeader1() {
		if (subviewBodyHeader1 == null) {
			subviewBodyHeader1 = (UINamingContainer) findComponentInRoot("subviewBodyHeader1");
		}
		return subviewBodyHeader1;
	}

	protected UINamingContainer getSubviewNavPromo1() {
		if (subviewNavPromo1 == null) {
			subviewNavPromo1 = (UINamingContainer) findComponentInRoot("subviewNavPromo1");
		}
		return subviewNavPromo1;
	}

	protected UINamingContainer getSubviewMainBody1() {
		if (subviewMainBody1 == null) {
			subviewMainBody1 = (UINamingContainer) findComponentInRoot("subviewMainBody1");
		}
		return subviewMainBody1;
	}

	protected UINamingContainer getSubviewFooter1() {
		if (subviewFooter1 == null) {
			subviewFooter1 = (UINamingContainer) findComponentInRoot("subviewFooter1");
		}
		return subviewFooter1;
	}

}