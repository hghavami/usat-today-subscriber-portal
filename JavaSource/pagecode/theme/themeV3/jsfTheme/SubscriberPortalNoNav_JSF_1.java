/**
 * 
 */
package pagecode.theme.themeV3.jsfTheme;

import pagecode.PageCodeBase;
import javax.faces.component.html.HtmlOutputText;

/**
 * @author aeast
 * 
 */
public class SubscriberPortalNoNav_JSF_1 extends PageCodeBase {

	protected HtmlOutputText textThankYouJS13;

	protected HtmlOutputText getTextThankYouJS13() {
		if (textThankYouJS13 == null) {
			textThankYouJS13 = (HtmlOutputText) findComponentInRoot("textThankYouJS13");
		}
		return textThankYouJS13;
	}

}