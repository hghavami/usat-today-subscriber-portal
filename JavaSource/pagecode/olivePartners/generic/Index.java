/**
 * 
 */
package pagecode.olivePartners.generic;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pagecode.PageCodeBase;

import com.gannett.usat.olive.partners.OlivePartner;
import com.gannett.usat.olive.partners.PartnershipUserRegistration;
import com.gannett.usat.olive.partners.controller.OlivePartnerManager;
import com.gannett.usat.olive.partners.controller.PartnershipUserRegistrationManager;
import com.ibm.faces.component.html.HtmlBehavior;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlFormItem;
import com.ibm.faces.component.html.HtmlInputHelperAssist;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.business.interfaces.products.ProductIntf;
import com.usatoday.businessObjects.products.ProductBO;
import com.usatoday.businessObjects.products.utils.ElectronicEditionFulfillmentHelper;
import com.usatoday.esub.trials.handlers.OlivePartnerHandler;
import com.usatoday.esub.trials.handlers.PartnershipUserRegistrationHandler;
import com.usatoday.util.MobileDeviceDetectionUtil;

/**
 * @author hghavami
 * 
 */
public class Index extends PageCodeBase {

	protected PartnershipUserRegistrationHandler olivePartnerHandlder;
	protected OlivePartnerHandler olivePartnerHandler;
	protected PartnershipUserRegistrationHandler partnershipUserRegistrationHandler;
	protected HtmlPanelFormBox formBox1;
	protected HtmlFormItem authEmailFormItem;
	protected HtmlInputText authEmailAddress;
	protected HtmlFormItem formItem1;
	protected HtmlInputHelperAssist assist9;
	protected HtmlBehavior behavior2;
	protected HtmlScriptCollector scriptCollector1;
	protected HtmlForm customerInfoform;
	protected HtmlInputHidden partnerId;
	protected HtmlJspPanel jspPanel2;
	protected HtmlPanelFormBox authFormBox;
	protected HtmlJspPanel jspPanel1;
	protected HtmlPanelFormBox customerInfoformBox;
	protected HtmlFormItem emailFormItem;
	protected HtmlInputText emailAddress;
	protected HtmlInputText firstName;
	protected HtmlInputHelperAssist assist2;
	protected HtmlInputText lastName;
	protected HtmlInputText company;
	protected HtmlInputText address1;
	protected HtmlInputText aptSuite;
	protected HtmlInputText city;
	protected HtmlSelectOneMenu stateMenu;
	protected HtmlInputText zip;
	protected HtmlOutputText headerText;
	protected HtmlPanelGrid grid2;
	protected HtmlCommandExButton authSubmitbutton;
	protected HtmlPanelGrid grid1;
	protected HtmlInputHelperAssist assist8;
	protected HtmlFormItem firstNameFormItem;
	protected HtmlFormItem lastNameFormItem;
	protected HtmlInputHelperAssist assist3;
	protected HtmlFormItem companyFormItem;
	protected HtmlInputHelperAssist assist1;
	protected HtmlFormItem address1formItem;
	protected HtmlInputHelperAssist assist4;
	protected HtmlFormItem aptSuiteFormItem;
	protected HtmlInputHelperAssist assist5;
	protected HtmlFormItem cityFormItem;
	protected HtmlInputHelperAssist assist6;
	protected HtmlFormItem stateFormItem;
	protected HtmlFormItem zipFormItem;
	protected HtmlInputHelperAssist assist7;
	protected HtmlCommandExButton submitButton;

	public String doSubmitButton() {

		// This is java code that runs when this action method is invoked
		String eEditionLink = null;
		// use the ee partner table to look up which product to use for
		// fulfillment and build links according to that.
		EntityManagerFactory emf = null;
		PartnershipUserRegistration pur = new PartnershipUserRegistration();

		try {
			OlivePartnerHandler oph = this.getOlivePartnerHandler();
			PartnershipUserRegistrationHandler purh = this.getPartnershipUserRegistrationHandler();
			// If you create an EntityManagerFactory you must call the dispose
			// method when you are done using it.
			emf = Persistence.createEntityManagerFactory("USATDomainModel");
			OlivePartnerManager partnerManager = new OlivePartnerManager(emf);
			List<OlivePartner> partnerList = partnerManager.getOlivePartnerId(oph.getPartnerId());
			OlivePartner op = null;
			if (partnerList.size() > 0) {
				op = partnerList.iterator().next();
			}

			if (op == null || !op.isCurrentlyActive()) {
				// not valid op- error page
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Offer is no longer valid.", null);

				context.addMessage(null, facesMsg);
				return "failure";
			} else {
				// Check for existing email address
				PartnershipUserRegistrationManager partnershipManager = new PartnershipUserRegistrationManager(emf);
				List<PartnershipUserRegistration> partnershipList = partnershipManager.getPartnershipUserRegistrationEmail(purh
						.getEmail_address());
				if (partnershipList.size() > 0) {
					// Check if they are trying existing email address
					FacesContext context = FacesContext.getCurrentInstance();
					FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Email address already enrolled.", null);

					context.addMessage(null, facesMsg);
					return "failure";
				} else {
					pur.setAddress1(purh.getAddress1());
					pur.setAddress2(purh.getAddress2());
					pur.setAptSuite(purh.getApt_suite());
					pur.setCity(purh.getCity());
					pur.setCompanyName(purh.getCompany_name());
					pur.setEmailAddress(purh.getEmail_address());
					pur.setEndDate(purh.getEnd_date());
					pur.setFirst_Name(purh.getFirst_Name());
					pur.setInsertTimeStamp(purh.getInsert_time_stamp());
					HttpServletRequest httpServletRequest = (HttpServletRequest) FacesContext.getCurrentInstance()
							.getExternalContext().getRequest();
					String ip = httpServletRequest.getRemoteAddr();
					pur.setIpAddress(ip);
					pur.setLastName(purh.getLast_name());
					pur.setPartnerId(purh.getPartner_id());
					pur.setPartnerNumber(purh.getPartner_number());
					pur.setPassword(purh.getPassword());
					pur.setPhone(purh.getPhone());
					pur.setStartDate(purh.getStart_date());
					// Set end date
					Timestamp ts = purh.getStart_date();
					Calendar cal = Calendar.getInstance();
					cal.setTime(ts);
					cal.add(Calendar.DAY_OF_WEEK, op.getDaysToAllowAccess());
					ts.setTime(cal.getTime().getTime());
					// Can not allow pass promotion end date
					if (ts.after(op.getEndDate())) {
						pur.setEndDate(op.getEndDate());
					} else {
						pur.setEndDate(ts);
					}
					pur.setState(purh.getState());
					pur.setZip(purh.getZip());
					// Create enrollee
					partnershipManager.createPartnershipUserRegistration(pur);
				}
			}

			System.out.println(partnerList.toString());
			// Now redirect to Olive EE
			HttpServletRequest request = (HttpServletRequest) this.getFacesContext().getExternalContext().getRequest();
			HttpServletResponse response = (HttpServletResponse) this.getFacesContext().getExternalContext().getResponse();
			// probably need to change this to an expried product like we do for
			// starbucks since going to different skin
			ProductIntf product = ProductBO.fetchProductIncludeInactive("EE");

			// this block of code will first detect what we believe is the best
			// viewer. If they have clicked an override link
			// we will ignore what we think is best and use what they selected.
			boolean useAlternate = MobileDeviceDetectionUtil.isMobileDevice(request);

			// eEditionLink =
			// ElectronicEditionFulfillmentHelper.buildOliveFulfillmentURL(product,
			// partnerID, null, "1100000", useAlternate);

			eEditionLink = ElectronicEditionFulfillmentHelper.buildOliveFulfillmentURL(product, op.getPartnerId(), null, "none",
					useAlternate);
			response.sendRedirect(eEditionLink);
			FacesContext.getCurrentInstance().responseComplete();
			System.out.print("Successfully redirected from partner to USA TODAY e-Edition.");
		} catch (Exception e) {
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
					"Offer could not be created, please retry or call our Customer Service.", null);

			context.addMessage(null, facesMsg);
			System.out.println(e);
			return "failure";
		}

		// TODO: Return an outcome that corresponds to a navigation rule
		return "";
	}

	public String doAuthSubmitButton() {

		// This is java code that runs when this action method is invoked
		// This is java code that runs when this action method is invoked
		String eEditionLink = "http://ee.usatoday.com/sdn";
		;
		// use the ee partner table to look up which product to use for
		// fulfillment and build links according to that.
		EntityManagerFactory emf = null;
		PartnershipUserRegistrationHandler purh = this.getPartnershipUserRegistrationHandler();
		PartnershipUserRegistration pur = new PartnershipUserRegistration();
		try {
			OlivePartnerHandler oph = this.getOlivePartnerHandler();
			// If you create an EntityManagerFactory you must call the dispose
			// method when you are done using it.
			emf = Persistence.createEntityManagerFactory("USATDomainModel");
			OlivePartnerManager partnerManager = new OlivePartnerManager(emf);
			List<OlivePartner> partnerList = partnerManager.getOlivePartnerId(oph.getPartnerId());
			OlivePartner op = null;
			if (partnerList.size() > 0) {
				op = partnerList.iterator().next();
			}
			if (op == null || !op.isCurrentlyActive()) {
				// not valid op- error page
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Offer is no longer valid.", null);

				context.addMessage(null, facesMsg);
				return "failure";
			}
			if (purh.getEmail_address() == null || purh.getEmail_address().trim().equals("")) {
				// not valid op- error page
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Please enter email address.", null);

				context.addMessage(null, facesMsg);
				return "failure";
			}
			// Check for existing email address
			PartnershipUserRegistrationManager partnershipManager = new PartnershipUserRegistrationManager(emf);
			List<PartnershipUserRegistration> partnershipList = partnershipManager.getPartnershipUserRegistrationEmail(purh
					.getEmail_address());
			if (partnershipList.size() > 0) {
				pur = partnershipList.iterator().next();
			}
			Timestamp ts = new Timestamp(System.currentTimeMillis());
			if (partnershipList.size() > 0 && !ts.after(pur.getEndDate())) {
				HttpServletRequest request = (HttpServletRequest) this.getFacesContext().getExternalContext().getRequest();
				HttpServletResponse response = (HttpServletResponse) this.getFacesContext().getExternalContext().getResponse();
				// probably need to change this to an expried product like we do
				// for starbucks since going to different skin
				ProductIntf product = ProductBO.fetchProductIncludeInactive("EE");

				// this block of code will first detect what we believe is the
				// best
				// viewer. If they have clicked an override link
				// we will ignore what we think is best and use what they
				// selected.
				boolean useAlternate = MobileDeviceDetectionUtil.isMobileDevice(request);

				// eEditionLink =
				// ElectronicEditionFulfillmentHelper.buildOliveFulfillmentURL(product,
				// partnerID, null, "1100000", useAlternate);

				eEditionLink = ElectronicEditionFulfillmentHelper.buildOliveFulfillmentURL(product, op.getPartnerId(), null,
						"none", useAlternate);
				response.sendRedirect(eEditionLink);
				FacesContext.getCurrentInstance().responseComplete();
				System.out.print("Successfully redirected from partner to USA TODAY e-Edition.");
			} else {
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
						"No email address found or access has expired, please enter information below to proceed.", null);

				context.addMessage(null, facesMsg);
				return "failure";
			}

		} catch (Exception e) {
			System.out.print("Failed to redirect from partner to USA TODAY e-Edition.");
		} finally {

			if (emf != null) {
				emf.close();
			}
		}
		// TODO: Return an outcome that corresponds to a navigation rule
		// return "failure";
		return "";
	}

	/**
	 * @managed-bean true
	 */
	protected OlivePartnerHandler getOlivePartnerHandler() {
		if (olivePartnerHandler == null) {
			olivePartnerHandler = (OlivePartnerHandler) getManagedBean("olivePartnerHandler");
		}
		return olivePartnerHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setOlivePartnerHandler(OlivePartnerHandler olivePartnerHandler) {
		this.olivePartnerHandler = olivePartnerHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected PartnershipUserRegistrationHandler getPartnershipUserRegistrationHandler() {
		if (partnershipUserRegistrationHandler == null) {
			partnershipUserRegistrationHandler = (PartnershipUserRegistrationHandler) getManagedBean("partnershipUserRegistrationHandler");
		}
		return partnershipUserRegistrationHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setPartnershipUserRegistrationHandler(PartnershipUserRegistrationHandler partnershipUserRegistrationHandler) {
		this.partnershipUserRegistrationHandler = partnershipUserRegistrationHandler;
	}

	protected HtmlPanelFormBox getFormBox1() {
		if (formBox1 == null) {
			formBox1 = (HtmlPanelFormBox) findComponentInRoot("formBox1");
		}
		return formBox1;
	}

	protected HtmlFormItem getAuthEmailFormItem() {
		if (authEmailFormItem == null) {
			authEmailFormItem = (HtmlFormItem) findComponentInRoot("authEmailFormItem");
		}
		return authEmailFormItem;
	}

	protected HtmlInputText getAuthEmailAddress() {
		if (authEmailAddress == null) {
			authEmailAddress = (HtmlInputText) findComponentInRoot("authEmailAddress");
		}
		return authEmailAddress;
	}

	protected HtmlFormItem getFormItem1() {
		if (formItem1 == null) {
			formItem1 = (HtmlFormItem) findComponentInRoot("formItem1");
		}
		return formItem1;
	}

	protected HtmlInputHelperAssist getAssist9() {
		if (assist9 == null) {
			assist9 = (HtmlInputHelperAssist) findComponentInRoot("assist9");
		}
		return assist9;
	}

	protected HtmlBehavior getBehavior2() {
		if (behavior2 == null) {
			behavior2 = (HtmlBehavior) findComponentInRoot("behavior2");
		}
		return behavior2;
	}

	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlForm getCustomerInfoform() {
		if (customerInfoform == null) {
			customerInfoform = (HtmlForm) findComponentInRoot("customerInfoform");
		}
		return customerInfoform;
	}

	protected HtmlInputHidden getPartnerId() {
		if (partnerId == null) {
			partnerId = (HtmlInputHidden) findComponentInRoot("partnerId");
		}
		return partnerId;
	}

	protected HtmlJspPanel getJspPanel2() {
		if (jspPanel2 == null) {
			jspPanel2 = (HtmlJspPanel) findComponentInRoot("jspPanel2");
		}
		return jspPanel2;
	}

	protected HtmlPanelFormBox getAuthFormBox() {
		if (authFormBox == null) {
			authFormBox = (HtmlPanelFormBox) findComponentInRoot("authFormBox");
		}
		return authFormBox;
	}

	protected HtmlJspPanel getJspPanel1() {
		if (jspPanel1 == null) {
			jspPanel1 = (HtmlJspPanel) findComponentInRoot("jspPanel1");
		}
		return jspPanel1;
	}

	protected HtmlPanelFormBox getCustomerInfoformBox() {
		if (customerInfoformBox == null) {
			customerInfoformBox = (HtmlPanelFormBox) findComponentInRoot("customerInfoformBox");
		}
		return customerInfoformBox;
	}

	protected HtmlFormItem getEmailFormItem() {
		if (emailFormItem == null) {
			emailFormItem = (HtmlFormItem) findComponentInRoot("emailFormItem");
		}
		return emailFormItem;
	}

	protected HtmlInputText getEmailAddress() {
		if (emailAddress == null) {
			emailAddress = (HtmlInputText) findComponentInRoot("emailAddress");
		}
		return emailAddress;
	}

	protected HtmlInputText getFirstName() {
		if (firstName == null) {
			firstName = (HtmlInputText) findComponentInRoot("firstName");
		}
		return firstName;
	}

	protected HtmlInputHelperAssist getAssist2() {
		if (assist2 == null) {
			assist2 = (HtmlInputHelperAssist) findComponentInRoot("assist2");
		}
		return assist2;
	}

	protected HtmlInputText getLastName() {
		if (lastName == null) {
			lastName = (HtmlInputText) findComponentInRoot("lastName");
		}
		return lastName;
	}

	protected HtmlInputText getCompany() {
		if (company == null) {
			company = (HtmlInputText) findComponentInRoot("company");
		}
		return company;
	}

	protected HtmlInputText getAddress1() {
		if (address1 == null) {
			address1 = (HtmlInputText) findComponentInRoot("address1");
		}
		return address1;
	}

	protected HtmlInputText getAptSuite() {
		if (aptSuite == null) {
			aptSuite = (HtmlInputText) findComponentInRoot("aptSuite");
		}
		return aptSuite;
	}

	protected HtmlInputText getCity() {
		if (city == null) {
			city = (HtmlInputText) findComponentInRoot("city");
		}
		return city;
	}

	protected HtmlSelectOneMenu getStateMenu() {
		if (stateMenu == null) {
			stateMenu = (HtmlSelectOneMenu) findComponentInRoot("stateMenu");
		}
		return stateMenu;
	}

	protected HtmlInputText getZip() {
		if (zip == null) {
			zip = (HtmlInputText) findComponentInRoot("zip");
		}
		return zip;
	}

	protected HtmlOutputText getHeaderText() {
		if (headerText == null) {
			headerText = (HtmlOutputText) findComponentInRoot("headerText");
		}
		return headerText;
	}

	protected HtmlPanelGrid getGrid2() {
		if (grid2 == null) {
			grid2 = (HtmlPanelGrid) findComponentInRoot("grid2");
		}
		return grid2;
	}

	protected HtmlCommandExButton getAuthSubmitbutton() {
		if (authSubmitbutton == null) {
			authSubmitbutton = (HtmlCommandExButton) findComponentInRoot("authSubmitbutton");
		}
		return authSubmitbutton;
	}

	protected HtmlPanelGrid getGrid1() {
		if (grid1 == null) {
			grid1 = (HtmlPanelGrid) findComponentInRoot("grid1");
		}
		return grid1;
	}

	protected HtmlInputHelperAssist getAssist8() {
		if (assist8 == null) {
			assist8 = (HtmlInputHelperAssist) findComponentInRoot("assist8");
		}
		return assist8;
	}

	protected HtmlFormItem getFirstNameFormItem() {
		if (firstNameFormItem == null) {
			firstNameFormItem = (HtmlFormItem) findComponentInRoot("firstNameFormItem");
		}
		return firstNameFormItem;
	}

	protected HtmlFormItem getLastNameFormItem() {
		if (lastNameFormItem == null) {
			lastNameFormItem = (HtmlFormItem) findComponentInRoot("lastNameFormItem");
		}
		return lastNameFormItem;
	}

	protected HtmlInputHelperAssist getAssist3() {
		if (assist3 == null) {
			assist3 = (HtmlInputHelperAssist) findComponentInRoot("assist3");
		}
		return assist3;
	}

	protected HtmlFormItem getCompanyFormItem() {
		if (companyFormItem == null) {
			companyFormItem = (HtmlFormItem) findComponentInRoot("companyFormItem");
		}
		return companyFormItem;
	}

	protected HtmlInputHelperAssist getAssist1() {
		if (assist1 == null) {
			assist1 = (HtmlInputHelperAssist) findComponentInRoot("assist1");
		}
		return assist1;
	}

	protected HtmlFormItem getAddress1formItem() {
		if (address1formItem == null) {
			address1formItem = (HtmlFormItem) findComponentInRoot("address1formItem");
		}
		return address1formItem;
	}

	protected HtmlInputHelperAssist getAssist4() {
		if (assist4 == null) {
			assist4 = (HtmlInputHelperAssist) findComponentInRoot("assist4");
		}
		return assist4;
	}

	protected HtmlFormItem getAptSuiteFormItem() {
		if (aptSuiteFormItem == null) {
			aptSuiteFormItem = (HtmlFormItem) findComponentInRoot("aptSuiteFormItem");
		}
		return aptSuiteFormItem;
	}

	protected HtmlInputHelperAssist getAssist5() {
		if (assist5 == null) {
			assist5 = (HtmlInputHelperAssist) findComponentInRoot("assist5");
		}
		return assist5;
	}

	protected HtmlFormItem getCityFormItem() {
		if (cityFormItem == null) {
			cityFormItem = (HtmlFormItem) findComponentInRoot("cityFormItem");
		}
		return cityFormItem;
	}

	protected HtmlInputHelperAssist getAssist6() {
		if (assist6 == null) {
			assist6 = (HtmlInputHelperAssist) findComponentInRoot("assist6");
		}
		return assist6;
	}

	protected HtmlFormItem getStateFormItem() {
		if (stateFormItem == null) {
			stateFormItem = (HtmlFormItem) findComponentInRoot("stateFormItem");
		}
		return stateFormItem;
	}

	protected HtmlFormItem getZipFormItem() {
		if (zipFormItem == null) {
			zipFormItem = (HtmlFormItem) findComponentInRoot("zipFormItem");
		}
		return zipFormItem;
	}

	protected HtmlInputHelperAssist getAssist7() {
		if (assist7 == null) {
			assist7 = (HtmlInputHelperAssist) findComponentInRoot("assist7");
		}
		return assist7;
	}

	protected HtmlCommandExButton getSubmitButton() {
		if (submitButton == null) {
			submitButton = (HtmlCommandExButton) findComponentInRoot("submitButton");
		}
		return submitButton;
	}

}