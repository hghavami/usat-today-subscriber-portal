/**
 * 
 */
package pagecode.eAccount.trials;

import javax.faces.component.html.HtmlOutputText;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.joda.time.DateTime;

import pagecode.PageCodeBase;

import com.ibm.faces.component.html.HtmlGraphicImageEx;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.business.interfaces.customer.TrialCustomerIntf;
import com.usatoday.business.interfaces.customer.TrialInstanceIntf;
import com.usatoday.businessObjects.customer.ContactBO;
import com.usatoday.esub.common.UTCommon;
import com.usatoday.esub.handlers.NewSubscriptionOrderHandler;
import com.usatoday.esub.trials.handlers.TrialCustomerHandler;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlForm;
import com.ibm.faces.component.html.HtmlCommandExButton;

/**
 * @author aeast
 * 
 */
public class TrialReaderLaunchPage extends PageCodeBase {

	protected TrialCustomerHandler trialCustomerHandler;
	protected NewSubscriptionOrderHandler newSubscriptionOrderHandler;
	protected HtmlScriptCollector scriptCollectorTopNavCollector;
	protected HtmlJspPanel jspPanelTopNavCustomPanel;
	protected HtmlOutputLinkEx linkExTopNavLogoLink;
	protected HtmlGraphicImageEx imageExPartnerLogo;
	protected HtmlGraphicImageEx imageExPartnerLogoNoLink;
	protected HtmlOutputLinkEx linkExNavAreaLinkTrialNavCP1;
	protected HtmlOutputText textNavAreaTrialNavCP1;
	protected HtmlOutputText mainHeader;
	protected HtmlPanelGrid gridAccountInformation;
	protected HtmlOutputText textAccountLabel;
	protected HtmlOutputText textUpdateEmailPreferenceLinkText;
	protected HtmlPanelGrid gridCustomTextGrid;
	protected HtmlOutputText textCustomText;
	protected HtmlOutputText textSubscribeNowLinkLabel;
	protected HtmlOutputText textSubscribedInformation;
	protected HtmlOutputLinkEx linkExReadNowLink;
	protected HtmlGraphicImageEx imageExReadNowButtonImage;
	protected HtmlScriptCollector scriptCollectorMainBodyCollector;
	protected HtmlForm formMainForm;
	protected HtmlOutputText textAccountNumber;
	protected HtmlOutputText textNameLabel;
	protected HtmlOutputText text3;
	protected HtmlOutputText textStartDateLabel;
	protected HtmlOutputText textStartDate;
	protected HtmlOutputText textSubscriptionEnd;
	protected HtmlOutputText textExpirationDate;
	protected HtmlOutputText textDaysRemainingLabel;
	protected HtmlOutputText textOnEZPay;
	protected HtmlOutputText textAPlaceHolder;
	protected HtmlOutputLinkEx linkExUpdateEmailPreferencesLink;
	protected HtmlPanelGrid gridSubscribeGrid;
	protected HtmlCommandExButton buttonSubscribe;
	protected HtmlOutputText textDisclaimerText;
	protected HtmlPanelGrid gridSubscribed;
	protected HtmlOutputLinkEx linkExOpenDesktopVersion;
	protected HtmlOutputText text1111;
	protected HtmlOutputText text2;
	protected HtmlOutputLinkEx linkExOpenMobileVersion;
	protected HtmlOutputText text1;

	/**
	 * @managed-bean true
	 */
	protected TrialCustomerHandler getTrialCustomerHandler() {
		if (trialCustomerHandler == null) {
			trialCustomerHandler = (TrialCustomerHandler) getManagedBean("trialCustomerHandler");
		}
		return trialCustomerHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setTrialCustomerHandler(TrialCustomerHandler trialCustomerHandler) {
		this.trialCustomerHandler = trialCustomerHandler;
	}

	public String doLinkSubscribeNowAction() {
		// Type Java code that runs when the component is clicked

		FacesContext context = this.getFacesContext();
		try {
			NewSubscriptionOrderHandler nsoh = this.getNewSubscriptionOrderHandler();

			TrialCustomerHandler tch = this.getTrialCustomerHandler();

			TrialCustomerIntf tCust = tch.getTrialCustomer();
			TrialInstanceIntf instance = tCust.getCurrentInstance();

			ContactBO cBO = instance.getContactInformation();
			// tranfer relevant information
			nsoh.setDeliveryAddress1(cBO.getUiAddress().getAddress1());
			nsoh.setDeliveryAddress2(cBO.getUiAddress().getAddress2());
			nsoh.setDeliveryAptSuite(cBO.getUiAddress().getAptSuite());
			nsoh.setDeliveryCity(cBO.getUiAddress().getCity());
			nsoh.setDeliveryEmailAddress(cBO.getEmailAddress());
			nsoh.setDeliveryEmailAddressConfirmation(cBO.getEmailAddress());
			nsoh.setDeliveryFirstName(cBO.getFirstName());
			nsoh.setDeliveryLastName(cBO.getLastName());
			nsoh.setDeliveryState(cBO.getUiAddress().getState());
			nsoh.setDeliveryZipCode(cBO.getUiAddress().getZip());
			nsoh.setPassword(cBO.getPassword());
			nsoh.setConfirmPassword(cBO.getPassword());
			nsoh.setClubNumber(instance.getClubNumber());
			nsoh.setPartnerName(instance.getPartner().getPartnerName());
			nsoh.setStartedAsFreeSample(true);

			String phone = cBO.getHomePhone();
			if (phone != null && phone.length() == 10) {
				nsoh.setDeliveryPhoneAreaCode(phone.substring(0, 3));
				nsoh.setDeliveryPhoneExchange(phone.substring(3, 6));
				nsoh.setDeliveryPhoneExtension(phone.substring(6));
			}

			// use the end date of the trial for start date of subscription
			DateTime startDate = null;
			if (instance.getEndDate() != null) {
				startDate = instance.getEndDate();
			} else {
				startDate = new DateTime();
			}

			nsoh.setStartDate(startDate.toDate());

			HttpSession session = (HttpSession) context.getExternalContext().getSession(true);

			// update pub/keycode in session for next page.
			session.removeAttribute(com.usatoday.esub.common.UTCommon.SESSION_PUB_NAME);
			session.setAttribute(com.usatoday.esub.common.UTCommon.SESSION_PUB_NAME, instance.getPubCode());

			session.removeAttribute(UTCommon.SESSION_KEYCODE);
			session.setAttribute(UTCommon.SESSION_KEYCODE, tch.getSubscribeKeycode());

		} catch (Exception e) {
			System.out.println("Failed to auto fill new sub information from trial subscriber: " + e.getMessage());
			e.printStackTrace();
		}

		return "success";
	}

	/**
	 * @managed-bean true
	 */
	protected NewSubscriptionOrderHandler getNewSubscriptionOrderHandler() {
		if (newSubscriptionOrderHandler == null) {
			newSubscriptionOrderHandler = (NewSubscriptionOrderHandler) getManagedBean("newSubscriptionOrderHandler");
		}
		return newSubscriptionOrderHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setNewSubscriptionOrderHandler(NewSubscriptionOrderHandler newSubscriptionOrderHandler) {
		this.newSubscriptionOrderHandler = newSubscriptionOrderHandler;
	}

	protected HtmlScriptCollector getScriptCollectorTopNavCollector() {
		if (scriptCollectorTopNavCollector == null) {
			scriptCollectorTopNavCollector = (HtmlScriptCollector) findComponentInRoot("scriptCollectorTopNavCollector");
		}
		return scriptCollectorTopNavCollector;
	}

	protected HtmlJspPanel getJspPanelTopNavCustomPanel() {
		if (jspPanelTopNavCustomPanel == null) {
			jspPanelTopNavCustomPanel = (HtmlJspPanel) findComponentInRoot("jspPanelTopNavCustomPanel");
		}
		return jspPanelTopNavCustomPanel;
	}

	protected HtmlOutputLinkEx getLinkExTopNavLogoLink() {
		if (linkExTopNavLogoLink == null) {
			linkExTopNavLogoLink = (HtmlOutputLinkEx) findComponentInRoot("linkExTopNavLogoLink");
		}
		return linkExTopNavLogoLink;
	}

	protected HtmlGraphicImageEx getImageExPartnerLogo() {
		if (imageExPartnerLogo == null) {
			imageExPartnerLogo = (HtmlGraphicImageEx) findComponentInRoot("imageExPartnerLogo");
		}
		return imageExPartnerLogo;
	}

	protected HtmlGraphicImageEx getImageExPartnerLogoNoLink() {
		if (imageExPartnerLogoNoLink == null) {
			imageExPartnerLogoNoLink = (HtmlGraphicImageEx) findComponentInRoot("imageExPartnerLogoNoLink");
		}
		return imageExPartnerLogoNoLink;
	}

	protected HtmlOutputLinkEx getLinkExNavAreaLinkTrialNavCP1() {
		if (linkExNavAreaLinkTrialNavCP1 == null) {
			linkExNavAreaLinkTrialNavCP1 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavAreaLinkTrialNavCP1");
		}
		return linkExNavAreaLinkTrialNavCP1;
	}

	protected HtmlOutputText getTextNavAreaTrialNavCP1() {
		if (textNavAreaTrialNavCP1 == null) {
			textNavAreaTrialNavCP1 = (HtmlOutputText) findComponentInRoot("textNavAreaTrialNavCP1");
		}
		return textNavAreaTrialNavCP1;
	}

	protected HtmlOutputText getMainHeader() {
		if (mainHeader == null) {
			mainHeader = (HtmlOutputText) findComponentInRoot("mainHeader");
		}
		return mainHeader;
	}

	protected HtmlPanelGrid getGridAccountInformation() {
		if (gridAccountInformation == null) {
			gridAccountInformation = (HtmlPanelGrid) findComponentInRoot("gridAccountInformation");
		}
		return gridAccountInformation;
	}

	protected HtmlOutputText getTextAccountLabel() {
		if (textAccountLabel == null) {
			textAccountLabel = (HtmlOutputText) findComponentInRoot("textAccountLabel");
		}
		return textAccountLabel;
	}

	protected HtmlOutputText getTextUpdateEmailPreferenceLinkText() {
		if (textUpdateEmailPreferenceLinkText == null) {
			textUpdateEmailPreferenceLinkText = (HtmlOutputText) findComponentInRoot("textUpdateEmailPreferenceLinkText");
		}
		return textUpdateEmailPreferenceLinkText;
	}

	protected HtmlPanelGrid getGridCustomTextGrid() {
		if (gridCustomTextGrid == null) {
			gridCustomTextGrid = (HtmlPanelGrid) findComponentInRoot("gridCustomTextGrid");
		}
		return gridCustomTextGrid;
	}

	protected HtmlOutputText getTextCustomText() {
		if (textCustomText == null) {
			textCustomText = (HtmlOutputText) findComponentInRoot("textCustomText");
		}
		return textCustomText;
	}

	protected HtmlOutputText getTextSubscribeNowLinkLabel() {
		if (textSubscribeNowLinkLabel == null) {
			textSubscribeNowLinkLabel = (HtmlOutputText) findComponentInRoot("textSubscribeNowLinkLabel");
		}
		return textSubscribeNowLinkLabel;
	}

	protected HtmlOutputText getTextSubscribedInformation() {
		if (textSubscribedInformation == null) {
			textSubscribedInformation = (HtmlOutputText) findComponentInRoot("textSubscribedInformation");
		}
		return textSubscribedInformation;
	}

	protected HtmlOutputLinkEx getLinkExReadNowLink() {
		if (linkExReadNowLink == null) {
			linkExReadNowLink = (HtmlOutputLinkEx) findComponentInRoot("linkExReadNowLink");
		}
		return linkExReadNowLink;
	}

	protected HtmlGraphicImageEx getImageExReadNowButtonImage() {
		if (imageExReadNowButtonImage == null) {
			imageExReadNowButtonImage = (HtmlGraphicImageEx) findComponentInRoot("imageExReadNowButtonImage");
		}
		return imageExReadNowButtonImage;
	}

	protected HtmlScriptCollector getScriptCollectorMainBodyCollector() {
		if (scriptCollectorMainBodyCollector == null) {
			scriptCollectorMainBodyCollector = (HtmlScriptCollector) findComponentInRoot("scriptCollectorMainBodyCollector");
		}
		return scriptCollectorMainBodyCollector;
	}

	protected HtmlForm getFormMainForm() {
		if (formMainForm == null) {
			formMainForm = (HtmlForm) findComponentInRoot("formMainForm");
		}
		return formMainForm;
	}

	protected HtmlOutputText getTextAccountNumber() {
		if (textAccountNumber == null) {
			textAccountNumber = (HtmlOutputText) findComponentInRoot("textAccountNumber");
		}
		return textAccountNumber;
	}

	protected HtmlOutputText getTextNameLabel() {
		if (textNameLabel == null) {
			textNameLabel = (HtmlOutputText) findComponentInRoot("textNameLabel");
		}
		return textNameLabel;
	}

	protected HtmlOutputText getText3() {
		if (text3 == null) {
			text3 = (HtmlOutputText) findComponentInRoot("text3");
		}
		return text3;
	}

	protected HtmlOutputText getTextStartDateLabel() {
		if (textStartDateLabel == null) {
			textStartDateLabel = (HtmlOutputText) findComponentInRoot("textStartDateLabel");
		}
		return textStartDateLabel;
	}

	protected HtmlOutputText getTextStartDate() {
		if (textStartDate == null) {
			textStartDate = (HtmlOutputText) findComponentInRoot("textStartDate");
		}
		return textStartDate;
	}

	protected HtmlOutputText getTextSubscriptionEnd() {
		if (textSubscriptionEnd == null) {
			textSubscriptionEnd = (HtmlOutputText) findComponentInRoot("textSubscriptionEnd");
		}
		return textSubscriptionEnd;
	}

	protected HtmlOutputText getTextExpirationDate() {
		if (textExpirationDate == null) {
			textExpirationDate = (HtmlOutputText) findComponentInRoot("textExpirationDate");
		}
		return textExpirationDate;
	}

	protected HtmlOutputText getTextDaysRemainingLabel() {
		if (textDaysRemainingLabel == null) {
			textDaysRemainingLabel = (HtmlOutputText) findComponentInRoot("textDaysRemainingLabel");
		}
		return textDaysRemainingLabel;
	}

	protected HtmlOutputText getTextOnEZPay() {
		if (textOnEZPay == null) {
			textOnEZPay = (HtmlOutputText) findComponentInRoot("textOnEZPay");
		}
		return textOnEZPay;
	}

	protected HtmlOutputText getTextAPlaceHolder() {
		if (textAPlaceHolder == null) {
			textAPlaceHolder = (HtmlOutputText) findComponentInRoot("textAPlaceHolder");
		}
		return textAPlaceHolder;
	}

	protected HtmlOutputLinkEx getLinkExUpdateEmailPreferencesLink() {
		if (linkExUpdateEmailPreferencesLink == null) {
			linkExUpdateEmailPreferencesLink = (HtmlOutputLinkEx) findComponentInRoot("linkExUpdateEmailPreferencesLink");
		}
		return linkExUpdateEmailPreferencesLink;
	}

	protected HtmlPanelGrid getGridSubscribeGrid() {
		if (gridSubscribeGrid == null) {
			gridSubscribeGrid = (HtmlPanelGrid) findComponentInRoot("gridSubscribeGrid");
		}
		return gridSubscribeGrid;
	}

	protected HtmlCommandExButton getButtonSubscribe() {
		if (buttonSubscribe == null) {
			buttonSubscribe = (HtmlCommandExButton) findComponentInRoot("buttonSubscribe");
		}
		return buttonSubscribe;
	}

	protected HtmlOutputText getTextDisclaimerText() {
		if (textDisclaimerText == null) {
			textDisclaimerText = (HtmlOutputText) findComponentInRoot("textDisclaimerText");
		}
		return textDisclaimerText;
	}

	protected HtmlPanelGrid getGridSubscribed() {
		if (gridSubscribed == null) {
			gridSubscribed = (HtmlPanelGrid) findComponentInRoot("gridSubscribed");
		}
		return gridSubscribed;
	}

	protected HtmlOutputLinkEx getLinkExOpenDesktopVersion() {
		if (linkExOpenDesktopVersion == null) {
			linkExOpenDesktopVersion = (HtmlOutputLinkEx) findComponentInRoot("linkExOpenDesktopVersion");
		}
		return linkExOpenDesktopVersion;
	}

	protected HtmlOutputText getText1111() {
		if (text1111 == null) {
			text1111 = (HtmlOutputText) findComponentInRoot("text1111");
		}
		return text1111;
	}

	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected HtmlOutputLinkEx getLinkExOpenMobileVersion() {
		if (linkExOpenMobileVersion == null) {
			linkExOpenMobileVersion = (HtmlOutputLinkEx) findComponentInRoot("linkExOpenMobileVersion");
		}
		return linkExOpenMobileVersion;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

}