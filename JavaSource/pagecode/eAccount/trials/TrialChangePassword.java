/**
 * 
 */
package pagecode.eAccount.trials;

import pagecode.PageCodeBase;
import com.ibm.faces.component.html.HtmlScriptCollector;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlPanelGrid;
import com.ibm.faces.component.html.HtmlJspPanel;
import javax.faces.component.html.HtmlOutputText;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.component.html.HtmlFormItem;
import javax.faces.component.html.HtmlInputSecret;
import javax.faces.context.FacesContext;

import com.ibm.faces.component.html.HtmlCommandExButton;
import com.usatoday.esub.trials.handlers.TrialCustomerHandler;
import com.usatoday.esub.trials.handlers.TrialChangePasswordHandler;
import javax.faces.component.html.HtmlMessages;
import com.ibm.faces.component.html.HtmlInputHelperSetFocus;
import com.ibm.faces.component.html.HtmlOutputLinkEx;

/**
 * @author aeast
 * 
 */
public class TrialChangePassword extends PageCodeBase {

	protected HtmlScriptCollector scriptCollectorChangePassword;
	protected HtmlForm formChangePassword;
	protected HtmlPanelGrid gridMainBodyGrid;
	protected HtmlPanelFormBox formBoxChangePassword;
	protected HtmlFormItem formItemNewPasswordConfirm;
	protected HtmlFormItem formItemNewPassword;
	protected HtmlInputSecret secretNewPassword;
	protected HtmlInputSecret secretConfirmNewPassword;
	protected HtmlJspPanel jspPanelFooterPanel;
	protected HtmlCommandExButton buttonChangePassword;
	protected TrialCustomerHandler trialCustomerHandler;
	protected TrialChangePasswordHandler trialChangePasswordHandler;
	protected HtmlPanelGrid gridTopMessageGrid;
	protected HtmlMessages messages1;
	protected HtmlInputHelperSetFocus setFocus1;
	protected HtmlOutputText mainHeader;
	protected HtmlOutputLinkEx linkExLeftNavBackCP1;
	protected HtmlOutputText textLeftNavCP1;

	protected HtmlScriptCollector getScriptCollectorChangePassword() {
		if (scriptCollectorChangePassword == null) {
			scriptCollectorChangePassword = (HtmlScriptCollector) findComponentInRoot("scriptCollectorChangePassword");
		}
		return scriptCollectorChangePassword;
	}

	protected HtmlForm getFormChangePassword() {
		if (formChangePassword == null) {
			formChangePassword = (HtmlForm) findComponentInRoot("formChangePassword");
		}
		return formChangePassword;
	}

	protected HtmlPanelGrid getGridMainBodyGrid() {
		if (gridMainBodyGrid == null) {
			gridMainBodyGrid = (HtmlPanelGrid) findComponentInRoot("gridMainBodyGrid");
		}
		return gridMainBodyGrid;
	}

	protected HtmlPanelFormBox getFormBoxChangePassword() {
		if (formBoxChangePassword == null) {
			formBoxChangePassword = (HtmlPanelFormBox) findComponentInRoot("formBoxChangePassword");
		}
		return formBoxChangePassword;
	}

	protected HtmlFormItem getFormItemNewPasswordConfirm() {
		if (formItemNewPasswordConfirm == null) {
			formItemNewPasswordConfirm = (HtmlFormItem) findComponentInRoot("formItemNewPasswordConfirm");
		}
		return formItemNewPasswordConfirm;
	}

	protected HtmlFormItem getFormItemNewPassword() {
		if (formItemNewPassword == null) {
			formItemNewPassword = (HtmlFormItem) findComponentInRoot("formItemNewPassword");
		}
		return formItemNewPassword;
	}

	protected HtmlInputSecret getSecretNewPassword() {
		if (secretNewPassword == null) {
			secretNewPassword = (HtmlInputSecret) findComponentInRoot("secretNewPassword");
		}
		return secretNewPassword;
	}

	protected HtmlInputSecret getSecretConfirmNewPassword() {
		if (secretConfirmNewPassword == null) {
			secretConfirmNewPassword = (HtmlInputSecret) findComponentInRoot("secretConfirmNewPassword");
		}
		return secretConfirmNewPassword;
	}

	protected HtmlJspPanel getJspPanelFooterPanel() {
		if (jspPanelFooterPanel == null) {
			jspPanelFooterPanel = (HtmlJspPanel) findComponentInRoot("jspPanelFooterPanel");
		}
		return jspPanelFooterPanel;
	}

	protected HtmlCommandExButton getButtonChangePassword() {
		if (buttonChangePassword == null) {
			buttonChangePassword = (HtmlCommandExButton) findComponentInRoot("buttonChangePassword");
		}
		return buttonChangePassword;
	}

	public String doButtonChangePasswordAction() {
		// Type Java code that runs when the component is clicked

		TrialChangePasswordHandler newPasswordHandler = this.getTrialChangePasswordHandler();

		TrialCustomerHandler trialCustomer = this.getTrialCustomerHandler();

		String responseString = "success";

		try {
			String newPwd = newPasswordHandler.getNewPassword().trim();
			String confirmPwd = newPasswordHandler.getConfirmNewPassword().trim();

			if (!newPwd.equalsIgnoreCase(confirmPwd)) {
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
						"New Password did not match Confirmation Password. Please try again.", null);

				context.addMessage(null, facesMsg);
				responseString = "failure";
				return responseString;

			}

			trialCustomer.getTrialCustomer().setPassword(newPwd);
			trialCustomer.getTrialCustomer().save();

			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Password changed successfully.", null);

			context.addMessage(null, facesMsg);

			newPasswordHandler.setConfirmNewPassword(null);
			newPasswordHandler.setNewPassword(null);
		} catch (Exception e) {
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, e.getMessage(), null);

			context.addMessage(null, facesMsg);
			responseString = "failure";
			return responseString;
		}
		return "success";
	}

	/**
	 * @managed-bean true
	 */
	protected TrialCustomerHandler getTrialCustomerHandler() {
		if (trialCustomerHandler == null) {
			trialCustomerHandler = (TrialCustomerHandler) getManagedBean("trialCustomerHandler");
		}
		return trialCustomerHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setTrialCustomerHandler(TrialCustomerHandler trialCustomerHandler) {
		this.trialCustomerHandler = trialCustomerHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected TrialChangePasswordHandler getTrialChangePasswordHandler() {
		if (trialChangePasswordHandler == null) {
			trialChangePasswordHandler = (TrialChangePasswordHandler) getManagedBean("trialChangePasswordHandler");
		}
		return trialChangePasswordHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setTrialChangePasswordHandler(TrialChangePasswordHandler trialChangePasswordHandler) {
		this.trialChangePasswordHandler = trialChangePasswordHandler;
	}

	protected HtmlPanelGrid getGridTopMessageGrid() {
		if (gridTopMessageGrid == null) {
			gridTopMessageGrid = (HtmlPanelGrid) findComponentInRoot("gridTopMessageGrid");
		}
		return gridTopMessageGrid;
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

	protected HtmlInputHelperSetFocus getSetFocus1() {
		if (setFocus1 == null) {
			setFocus1 = (HtmlInputHelperSetFocus) findComponentInRoot("setFocus1");
		}
		return setFocus1;
	}

	protected HtmlOutputText getMainHeader() {
		if (mainHeader == null) {
			mainHeader = (HtmlOutputText) findComponentInRoot("mainHeader");
		}
		return mainHeader;
	}

	protected HtmlOutputLinkEx getLinkExLeftNavBackCP1() {
		if (linkExLeftNavBackCP1 == null) {
			linkExLeftNavBackCP1 = (HtmlOutputLinkEx) findComponentInRoot("linkExLeftNavBackCP1");
		}
		return linkExLeftNavBackCP1;
	}

	protected HtmlOutputText getTextLeftNavCP1() {
		if (textLeftNavCP1 == null) {
			textLeftNavCP1 = (HtmlOutputText) findComponentInRoot("textLeftNavCP1");
		}
		return textLeftNavCP1;
	}

}