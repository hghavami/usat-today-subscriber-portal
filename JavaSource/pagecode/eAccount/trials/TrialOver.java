/**
 * 
 */
package pagecode.eAccount.trials;

import pagecode.PageCodeBase;
import com.ibm.faces.component.html.HtmlScriptCollector;
import javax.faces.component.html.HtmlOutputText;

import com.usatoday.business.interfaces.customer.TrialCustomerIntf;
import com.usatoday.businessObjects.customer.ContactBO;
import com.usatoday.esub.common.UTCommon;
import com.usatoday.esub.handlers.NewSubscriptionOrderHandler;
import com.usatoday.esub.trials.handlers.TrialCustomerHandler;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlJspPanel;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlForm;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.joda.time.DateTime;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.ibm.faces.component.html.HtmlGraphicImageEx;

/**
 * @author aeast
 * 
 */
public class TrialOver extends PageCodeBase {

	protected HtmlScriptCollector scriptCollector1;
	protected TrialCustomerHandler trialCustomerHandler;
	protected HtmlCommandExButton button1;
	protected HtmlCommandExButton buttonSubscribe;
	protected HtmlForm formExpiredForm;
	protected HtmlPanelGrid gridMainLeftGrid;
	protected HtmlOutputText textPlaceHolder;
	protected HtmlOutputText textTrialOverCustomText;
	protected NewSubscriptionOrderHandler newSubscriptionOrderHandler;
	protected HtmlOutputText mainHeader;
	protected HtmlScriptCollector scriptCollectorTopNavCollector;
	protected HtmlJspPanel jspPanelTopNavCustomPanel;
	protected HtmlOutputLinkEx linkExTopNavLogoLink;
	protected HtmlGraphicImageEx imageExPartnerLogo;
	protected HtmlGraphicImageEx imageExPartnerLogoNoLink;
	protected HtmlJspPanel jspPanelRightSideEEdition1;

	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	/**
	 * @managed-bean true
	 */
	protected TrialCustomerHandler getTrialCustomerHandler() {
		if (trialCustomerHandler == null) {
			trialCustomerHandler = (TrialCustomerHandler) getManagedBean("trialCustomerHandler");
		}
		return trialCustomerHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setTrialCustomerHandler(TrialCustomerHandler trialCustomerHandler) {
		this.trialCustomerHandler = trialCustomerHandler;
	}

	protected HtmlCommandExButton getButton1() {
		if (button1 == null) {
			button1 = (HtmlCommandExButton) findComponentInRoot("button1");
		}
		return button1;
	}

	protected HtmlCommandExButton getButtonSubscribe() {
		if (buttonSubscribe == null) {
			buttonSubscribe = (HtmlCommandExButton) findComponentInRoot("buttonSubscribe");
		}
		return buttonSubscribe;
	}

	protected HtmlForm getFormExpiredForm() {
		if (formExpiredForm == null) {
			formExpiredForm = (HtmlForm) findComponentInRoot("formExpiredForm");
		}
		return formExpiredForm;
	}

	protected HtmlPanelGrid getGridMainLeftGrid() {
		if (gridMainLeftGrid == null) {
			gridMainLeftGrid = (HtmlPanelGrid) findComponentInRoot("gridMainLeftGrid");
		}
		return gridMainLeftGrid;
	}

	protected HtmlOutputText getTextPlaceHolder() {
		if (textPlaceHolder == null) {
			textPlaceHolder = (HtmlOutputText) findComponentInRoot("textPlaceHolder");
		}
		return textPlaceHolder;
	}

	public String doButtonSubscribeAction() {
		// Type Java code that runs when the component is clicked
		//
		FacesContext context = this.getFacesContext();
		try {
			NewSubscriptionOrderHandler nsoh = this.getNewSubscriptionOrderHandler();

			TrialCustomerHandler tch = this.getTrialCustomerHandler();

			TrialCustomerIntf tCust = tch.getTrialCustomer();

			ContactBO cBO = tCust.getCurrentInstance().getContactInformation();
			// tranfer relevant information
			nsoh.setDeliveryAddress1(cBO.getUiAddress().getAddress1());
			nsoh.setDeliveryAddress2(cBO.getUiAddress().getAddress2());
			nsoh.setDeliveryAptSuite(cBO.getUiAddress().getAptSuite());
			nsoh.setDeliveryCity(cBO.getUiAddress().getCity());
			nsoh.setDeliveryEmailAddress(cBO.getEmailAddress());
			nsoh.setDeliveryEmailAddressConfirmation(cBO.getEmailAddress());
			nsoh.setDeliveryFirstName(cBO.getFirstName());
			nsoh.setDeliveryLastName(cBO.getLastName());
			nsoh.setDeliveryState(cBO.getUiAddress().getState());
			nsoh.setDeliveryZipCode(cBO.getUiAddress().getZip());
			nsoh.setPassword(cBO.getPassword());
			nsoh.setConfirmPassword(cBO.getPassword());
			nsoh.setClubNumber(tCust.getCurrentInstance().getClubNumber());
			nsoh.setPartnerName(tCust.getCurrentInstance().getPartner().getPartnerName());
			nsoh.setStartedAsFreeSample(true);

			String phone = cBO.getHomePhone();
			if (phone != null && phone.length() == 10) {
				nsoh.setDeliveryPhoneAreaCode(phone.substring(0, 3));
				nsoh.setDeliveryPhoneExchange(phone.substring(3, 6));
				nsoh.setDeliveryPhoneExtension(phone.substring(6));
			}

			// calculate start date based on days left on trial

			DateTime startDate = new DateTime();

			nsoh.setStartDate(startDate.toDate());

			HttpSession session = (HttpSession) context.getExternalContext().getSession(true);

			// update pub/keycode in session for next page.
			session.removeAttribute(com.usatoday.esub.common.UTCommon.SESSION_PUB_NAME);
			session.setAttribute(com.usatoday.esub.common.UTCommon.SESSION_PUB_NAME, tCust.getCurrentInstance().getPubCode());

			session.removeAttribute(UTCommon.SESSION_KEYCODE);
			session.setAttribute(UTCommon.SESSION_KEYCODE, tch.getSubscribeKeycode());

		} catch (Exception e) {
			System.out.println("Trial Over: Failed to auto fill new sub information from trial subscriber: " + e.getMessage());
			e.printStackTrace();
		}

		return "success";
	}

	protected HtmlOutputText getTextTrialOverCustomText() {
		if (textTrialOverCustomText == null) {
			textTrialOverCustomText = (HtmlOutputText) findComponentInRoot("textTrialOverCustomText");
		}
		return textTrialOverCustomText;
	}

	/**
	 * @managed-bean true
	 */
	protected NewSubscriptionOrderHandler getNewSubscriptionOrderHandler() {
		if (newSubscriptionOrderHandler == null) {
			newSubscriptionOrderHandler = (NewSubscriptionOrderHandler) getManagedBean("newSubscriptionOrderHandler");
		}
		return newSubscriptionOrderHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setNewSubscriptionOrderHandler(NewSubscriptionOrderHandler newSubscriptionOrderHandler) {
		this.newSubscriptionOrderHandler = newSubscriptionOrderHandler;
	}

	protected HtmlOutputText getMainHeader() {
		if (mainHeader == null) {
			mainHeader = (HtmlOutputText) findComponentInRoot("mainHeader");
		}
		return mainHeader;
	}

	protected HtmlScriptCollector getScriptCollectorTopNavCollector() {
		if (scriptCollectorTopNavCollector == null) {
			scriptCollectorTopNavCollector = (HtmlScriptCollector) findComponentInRoot("scriptCollectorTopNavCollector");
		}
		return scriptCollectorTopNavCollector;
	}

	protected HtmlJspPanel getJspPanelTopNavCustomPanel() {
		if (jspPanelTopNavCustomPanel == null) {
			jspPanelTopNavCustomPanel = (HtmlJspPanel) findComponentInRoot("jspPanelTopNavCustomPanel");
		}
		return jspPanelTopNavCustomPanel;
	}

	protected HtmlOutputLinkEx getLinkExTopNavLogoLink() {
		if (linkExTopNavLogoLink == null) {
			linkExTopNavLogoLink = (HtmlOutputLinkEx) findComponentInRoot("linkExTopNavLogoLink");
		}
		return linkExTopNavLogoLink;
	}

	protected HtmlGraphicImageEx getImageExPartnerLogo() {
		if (imageExPartnerLogo == null) {
			imageExPartnerLogo = (HtmlGraphicImageEx) findComponentInRoot("imageExPartnerLogo");
		}
		return imageExPartnerLogo;
	}

	protected HtmlGraphicImageEx getImageExPartnerLogoNoLink() {
		if (imageExPartnerLogoNoLink == null) {
			imageExPartnerLogoNoLink = (HtmlGraphicImageEx) findComponentInRoot("imageExPartnerLogoNoLink");
		}
		return imageExPartnerLogoNoLink;
	}

	protected HtmlJspPanel getJspPanelRightSideEEdition1() {
		if (jspPanelRightSideEEdition1 == null) {
			jspPanelRightSideEEdition1 = (HtmlJspPanel) findComponentInRoot("jspPanelRightSideEEdition1");
		}
		return jspPanelRightSideEEdition1;
	}

}