/**
 * 
 */
package pagecode.eAccount;

import javax.faces.component.html.HtmlCommandLink;
import javax.faces.component.html.HtmlOutputText;

import pagecode.PageCodeBase;

import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.business.interfaces.products.ProductIntf;
import com.usatoday.businessObjects.products.SubscriptionProductBO;
import com.usatoday.businessObjects.products.utils.ElectronicEditionFulfillmentHelper;
import com.usatoday.esub.ncs.handlers.CustomerHandler;
import com.usatoday.util.MobileDeviceDetectionUtil;
import com.usatoday.util.constants.UsaTodayConstants;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.ibm.faces.component.html.HtmlGraphicImageEx;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlForm;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import com.ibm.faces.component.html.HtmlJspPanel;

/**
 * @author aeast
 * 
 */
public class EEditionNewAccountIndex extends PageCodeBase {

	protected HtmlScriptCollector scriptCollectorMainCollector;
	protected HtmlCommandLink linkReadElectronicEdition;
	protected HtmlOutputText textReadEeditionLabel;
	protected HtmlCommandLink inkReadElectronicEdition;
	protected HtmlGraphicImageEx imageExReadElectronicImage;
	protected CustomerHandler customerHandler;
	protected HtmlScriptCollector scriptCollectorTemplateCollectorLeftNav;
	protected HtmlPanelGrid gridNavigationOuterGrid;
	protected HtmlPanelGrid gridLeftNavGroup1Grid;
	protected HtmlOutputLinkEx linkExNavGroup1SubscriberServices;
	protected HtmlOutputText textNavGroup1LinkLabel1;
	protected HtmlOutputText textNavGroup1LinkLabelInternational;
	protected HtmlOutputLinkEx linkExNavArea2Link1;
	protected HtmlOutputText textNavArea2Link1;
	protected HtmlOutputText textNavArea2Link2;
	protected HtmlOutputText textNavArea2Link3;
	protected HtmlOutputText textNavArea2Link4;
	protected HtmlOutputLinkEx linkExNavGroup2International;
	protected HtmlPanelGrid gridLeftNavGroup2Grid;
	protected HtmlOutputLinkEx linkExNavArea2Link2;
	protected HtmlOutputLinkEx linkExNavArea2Link3;
	protected HtmlOutputLinkEx linkExNavArea2Link4;
	protected HtmlForm formMainForm;
	protected HtmlPanelGrid gridThankYouInformation;
	protected HtmlOutputText textThankYouText1;
	protected HtmlOutputText textThankYouText2;
	protected HtmlPanelGrid gridRightPanelGrid;
	protected HtmlGraphicImageEx imageExFillerImage;
	protected HtmlOutputText textNavGroup1LinkLabelKindle;
	protected HtmlOutputText textNavGroup1LinkLabelDotCom;
	protected HtmlOutputLinkEx linkExNavGroup2Kindle;
	protected HtmlOutputLinkEx linkExNavGroup2DotCom;
	protected HtmlOutputText mainHeader;
	protected HtmlPanelGrid gridUTCompanionGrid;
	protected HtmlPanelGrid notExpiredAccountGrid;
	protected HtmlOutputLinkEx linkExReadNowLink_2;
	protected HtmlOutputLinkEx linkExReadNowLink;
	protected HtmlGraphicImageEx imageExReadNowButtonImage;
	protected HtmlOutputLinkEx linkExOpenDesktopVersion;
	protected HtmlOutputText text1111;
	protected HtmlOutputText text2;
	protected HtmlOutputLinkEx linkExOpenMobileVersion;
	protected HtmlOutputText text1;
	protected HtmlOutputText textEEFAQtext;
	protected HtmlJspPanel eReaderLinkPanel;
	protected HtmlOutputLinkEx linkExEEFAQ1;
	protected HtmlJspPanel RenewRequiredPanel;

	protected HtmlScriptCollector getScriptCollectorMainCollector() {
		if (scriptCollectorMainCollector == null) {
			scriptCollectorMainCollector = (HtmlScriptCollector) findComponentInRoot("scriptCollectorMainCollector");
		}
		return scriptCollectorMainCollector;
	}

	protected HtmlCommandLink getLinkReadElectronicEdition() {
		if (linkReadElectronicEdition == null) {
			linkReadElectronicEdition = (HtmlCommandLink) findComponentInRoot("linkReadElectronicEdition");
		}
		return linkReadElectronicEdition;
	}

	protected HtmlOutputText getTextReadEeditionLabel() {
		if (textReadEeditionLabel == null) {
			textReadEeditionLabel = (HtmlOutputText) findComponentInRoot("textReadEeditionLabel");
		}
		return textReadEeditionLabel;
	}

	@SuppressWarnings("unchecked")
	public String doLinkReadElectronicEditionAction() {
		// Type Java code that runs when the component is clicked

		String eEditionLink = "";

		CustomerHandler ch = this.getCustomerHandler();

		try {
			if (ch.isAuthenticated()) {

			}
			if (ch != null && ch.getCustomer() != null) {
				String pubCode = ch.getCustomer().getCurrentEmailRecord().getPubCode();

				if (pubCode.equalsIgnoreCase(UsaTodayConstants.UT_PUBCODE)) {
					pubCode = UsaTodayConstants.DEFAULT_UT_EE_PUBCODE;
				}

				HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
				boolean useAlternate = MobileDeviceDetectionUtil.isMobileDevice(req);

				ProductIntf p = SubscriptionProductBO.getSubscriptionProduct(pubCode);

				eEditionLink = ElectronicEditionFulfillmentHelper.buildOliveFulfillmentURL(p, ch.getCustomer()
						.getCurrentEmailRecord().getEmailAddress(), null, null, useAlternate);

				// set attribute to openReaderWindow
				this.getRequestScope().put("openReaderWindow", "true");
				this.getRequestScope().put("eEditionCustomURL", eEditionLink);

			} else {
				// don't open print add faces message

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "success";
	}

	protected HtmlCommandLink getInkReadElectronicEdition() {
		if (inkReadElectronicEdition == null) {
			inkReadElectronicEdition = (HtmlCommandLink) findComponentInRoot("inkReadElectronicEdition");
		}
		return inkReadElectronicEdition;
	}

	protected HtmlGraphicImageEx getImageExReadElectronicImage() {
		if (imageExReadElectronicImage == null) {
			imageExReadElectronicImage = (HtmlGraphicImageEx) findComponentInRoot("imageExReadElectronicImage");
		}
		return imageExReadElectronicImage;
	}

	/**
	 * @managed-bean true
	 */
	protected CustomerHandler getCustomerHandler() {
		if (customerHandler == null) {
			customerHandler = (CustomerHandler) getManagedBean("customerHandler");
		}
		return customerHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setCustomerHandler(CustomerHandler customerHandler) {
		this.customerHandler = customerHandler;
	}

	protected HtmlScriptCollector getScriptCollectorTemplateCollectorLeftNav() {
		if (scriptCollectorTemplateCollectorLeftNav == null) {
			scriptCollectorTemplateCollectorLeftNav = (HtmlScriptCollector) findComponentInRoot("scriptCollectorTemplateCollectorLeftNav");
		}
		return scriptCollectorTemplateCollectorLeftNav;
	}

	protected HtmlPanelGrid getGridNavigationOuterGrid() {
		if (gridNavigationOuterGrid == null) {
			gridNavigationOuterGrid = (HtmlPanelGrid) findComponentInRoot("gridNavigationOuterGrid");
		}
		return gridNavigationOuterGrid;
	}

	protected HtmlPanelGrid getGridLeftNavGroup1Grid() {
		if (gridLeftNavGroup1Grid == null) {
			gridLeftNavGroup1Grid = (HtmlPanelGrid) findComponentInRoot("gridLeftNavGroup1Grid");
		}
		return gridLeftNavGroup1Grid;
	}

	protected HtmlOutputLinkEx getLinkExNavGroup1SubscriberServices() {
		if (linkExNavGroup1SubscriberServices == null) {
			linkExNavGroup1SubscriberServices = (HtmlOutputLinkEx) findComponentInRoot("linkExNavGroup1SubscriberServices");
		}
		return linkExNavGroup1SubscriberServices;
	}

	protected HtmlOutputText getTextNavGroup1LinkLabel1() {
		if (textNavGroup1LinkLabel1 == null) {
			textNavGroup1LinkLabel1 = (HtmlOutputText) findComponentInRoot("textNavGroup1LinkLabel1");
		}
		return textNavGroup1LinkLabel1;
	}

	protected HtmlOutputText getTextNavGroup1LinkLabelInternational() {
		if (textNavGroup1LinkLabelInternational == null) {
			textNavGroup1LinkLabelInternational = (HtmlOutputText) findComponentInRoot("textNavGroup1LinkLabelInternational");
		}
		return textNavGroup1LinkLabelInternational;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link1() {
		if (linkExNavArea2Link1 == null) {
			linkExNavArea2Link1 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link1");
		}
		return linkExNavArea2Link1;
	}

	protected HtmlOutputText getTextNavArea2Link1() {
		if (textNavArea2Link1 == null) {
			textNavArea2Link1 = (HtmlOutputText) findComponentInRoot("textNavArea2Link1");
		}
		return textNavArea2Link1;
	}

	protected HtmlOutputText getTextNavArea2Link2() {
		if (textNavArea2Link2 == null) {
			textNavArea2Link2 = (HtmlOutputText) findComponentInRoot("textNavArea2Link2");
		}
		return textNavArea2Link2;
	}

	protected HtmlOutputText getTextNavArea2Link3() {
		if (textNavArea2Link3 == null) {
			textNavArea2Link3 = (HtmlOutputText) findComponentInRoot("textNavArea2Link3");
		}
		return textNavArea2Link3;
	}

	protected HtmlOutputText getTextNavArea2Link4() {
		if (textNavArea2Link4 == null) {
			textNavArea2Link4 = (HtmlOutputText) findComponentInRoot("textNavArea2Link4");
		}
		return textNavArea2Link4;
	}

	protected HtmlOutputLinkEx getLinkExNavGroup2International() {
		if (linkExNavGroup2International == null) {
			linkExNavGroup2International = (HtmlOutputLinkEx) findComponentInRoot("linkExNavGroup2International");
		}
		return linkExNavGroup2International;
	}

	protected HtmlPanelGrid getGridLeftNavGroup2Grid() {
		if (gridLeftNavGroup2Grid == null) {
			gridLeftNavGroup2Grid = (HtmlPanelGrid) findComponentInRoot("gridLeftNavGroup2Grid");
		}
		return gridLeftNavGroup2Grid;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link2() {
		if (linkExNavArea2Link2 == null) {
			linkExNavArea2Link2 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link2");
		}
		return linkExNavArea2Link2;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link3() {
		if (linkExNavArea2Link3 == null) {
			linkExNavArea2Link3 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link3");
		}
		return linkExNavArea2Link3;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link4() {
		if (linkExNavArea2Link4 == null) {
			linkExNavArea2Link4 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link4");
		}
		return linkExNavArea2Link4;
	}

	protected HtmlForm getFormMainForm() {
		if (formMainForm == null) {
			formMainForm = (HtmlForm) findComponentInRoot("formMainForm");
		}
		return formMainForm;
	}

	protected HtmlPanelGrid getGridThankYouInformation() {
		if (gridThankYouInformation == null) {
			gridThankYouInformation = (HtmlPanelGrid) findComponentInRoot("gridThankYouInformation");
		}
		return gridThankYouInformation;
	}

	protected HtmlOutputText getTextThankYouText1() {
		if (textThankYouText1 == null) {
			textThankYouText1 = (HtmlOutputText) findComponentInRoot("textThankYouText1");
		}
		return textThankYouText1;
	}

	protected HtmlOutputText getTextThankYouText2() {
		if (textThankYouText2 == null) {
			textThankYouText2 = (HtmlOutputText) findComponentInRoot("textThankYouText2");
		}
		return textThankYouText2;
	}

	protected HtmlPanelGrid getGridRightPanelGrid() {
		if (gridRightPanelGrid == null) {
			gridRightPanelGrid = (HtmlPanelGrid) findComponentInRoot("gridRightPanelGrid");
		}
		return gridRightPanelGrid;
	}

	protected HtmlGraphicImageEx getImageExFillerImage() {
		if (imageExFillerImage == null) {
			imageExFillerImage = (HtmlGraphicImageEx) findComponentInRoot("imageExFillerImage");
		}
		return imageExFillerImage;
	}

	protected HtmlOutputText getTextNavGroup1LinkLabelKindle() {
		if (textNavGroup1LinkLabelKindle == null) {
			textNavGroup1LinkLabelKindle = (HtmlOutputText) findComponentInRoot("textNavGroup1LinkLabelKindle");
		}
		return textNavGroup1LinkLabelKindle;
	}

	protected HtmlOutputText getTextNavGroup1LinkLabelDotCom() {
		if (textNavGroup1LinkLabelDotCom == null) {
			textNavGroup1LinkLabelDotCom = (HtmlOutputText) findComponentInRoot("textNavGroup1LinkLabelDotCom");
		}
		return textNavGroup1LinkLabelDotCom;
	}

	protected HtmlOutputLinkEx getLinkExNavGroup2Kindle() {
		if (linkExNavGroup2Kindle == null) {
			linkExNavGroup2Kindle = (HtmlOutputLinkEx) findComponentInRoot("linkExNavGroup2Kindle");
		}
		return linkExNavGroup2Kindle;
	}

	protected HtmlOutputLinkEx getLinkExNavGroup2DotCom() {
		if (linkExNavGroup2DotCom == null) {
			linkExNavGroup2DotCom = (HtmlOutputLinkEx) findComponentInRoot("linkExNavGroup2DotCom");
		}
		return linkExNavGroup2DotCom;
	}

	protected HtmlOutputText getMainHeader() {
		if (mainHeader == null) {
			mainHeader = (HtmlOutputText) findComponentInRoot("mainHeader");
		}
		return mainHeader;
	}

	protected HtmlPanelGrid getGridUTCompanionGrid() {
		if (gridUTCompanionGrid == null) {
			gridUTCompanionGrid = (HtmlPanelGrid) findComponentInRoot("gridUTCompanionGrid");
		}
		return gridUTCompanionGrid;
	}

	protected HtmlPanelGrid getNotExpiredAccountGrid() {
		if (notExpiredAccountGrid == null) {
			notExpiredAccountGrid = (HtmlPanelGrid) findComponentInRoot("notExpiredAccountGrid");
		}
		return notExpiredAccountGrid;
	}

	protected HtmlOutputLinkEx getLinkExReadNowLink_2() {
		if (linkExReadNowLink_2 == null) {
			linkExReadNowLink_2 = (HtmlOutputLinkEx) findComponentInRoot("linkExReadNowLink_2");
		}
		return linkExReadNowLink_2;
	}

	protected HtmlOutputLinkEx getLinkExReadNowLink() {
		if (linkExReadNowLink == null) {
			linkExReadNowLink = (HtmlOutputLinkEx) findComponentInRoot("linkExReadNowLink");
		}
		return linkExReadNowLink;
	}

	protected HtmlGraphicImageEx getImageExReadNowButtonImage() {
		if (imageExReadNowButtonImage == null) {
			imageExReadNowButtonImage = (HtmlGraphicImageEx) findComponentInRoot("imageExReadNowButtonImage");
		}
		return imageExReadNowButtonImage;
	}

	protected HtmlOutputLinkEx getLinkExOpenDesktopVersion() {
		if (linkExOpenDesktopVersion == null) {
			linkExOpenDesktopVersion = (HtmlOutputLinkEx) findComponentInRoot("linkExOpenDesktopVersion");
		}
		return linkExOpenDesktopVersion;
	}

	protected HtmlOutputText getText1111() {
		if (text1111 == null) {
			text1111 = (HtmlOutputText) findComponentInRoot("text1111");
		}
		return text1111;
	}

	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected HtmlOutputLinkEx getLinkExOpenMobileVersion() {
		if (linkExOpenMobileVersion == null) {
			linkExOpenMobileVersion = (HtmlOutputLinkEx) findComponentInRoot("linkExOpenMobileVersion");
		}
		return linkExOpenMobileVersion;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlOutputText getTextEEFAQtext() {
		if (textEEFAQtext == null) {
			textEEFAQtext = (HtmlOutputText) findComponentInRoot("textEEFAQtext");
		}
		return textEEFAQtext;
	}

	protected HtmlJspPanel getEReaderLinkPanel() {
		if (eReaderLinkPanel == null) {
			eReaderLinkPanel = (HtmlJspPanel) findComponentInRoot("eReaderLinkPanel");
		}
		return eReaderLinkPanel;
	}

	protected HtmlOutputLinkEx getLinkExEEFAQ1() {
		if (linkExEEFAQ1 == null) {
			linkExEEFAQ1 = (HtmlOutputLinkEx) findComponentInRoot("linkExEEFAQ1");
		}
		return linkExEEFAQ1;
	}

}