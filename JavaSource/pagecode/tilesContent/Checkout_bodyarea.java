/**
 * 
 */
package pagecode.tilesContent;

import pagecode.PageCodeBase;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.component.html.HtmlOutputText;
import com.ibm.faces.component.html.HtmlPanelDialog;
import javax.faces.component.html.HtmlOutputFormat;
import javax.faces.component.UIParameter;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlBehavior;
import com.ibm.faces.component.html.HtmlBehaviorKeyPress;
import javax.faces.component.html.HtmlInputHidden;

/**
 * @author aeast
 * 
 */
public class Checkout_bodyarea extends PageCodeBase {

	protected HtmlOutputText RightColHTMLSpot1Text;
	protected HtmlOutputText RightColHTMLSpot2Text;
	protected HtmlPanelGrid gridGeoTrust;
	protected HtmlPanelDialog dialogEZPayLearnMore1;
	protected HtmlPanelGrid gridEZPayLearnMoreGrid2;
	protected HtmlOutputFormat formatEZPayHelpText;
	protected UIParameter param7;
	protected UIParameter param8;
	protected HtmlPanelGroup group1;
	protected HtmlCommandExButton button2;
	protected HtmlBehavior behaviorEZPay4;
	protected HtmlPanelDialog dialogStartDateLearnMore1;
	protected HtmlPanelGrid gridStartDateLearnMoreGrid2;
	protected HtmlOutputFormat formatGiftFutureStartDateText;
	protected UIParameter param1;
	protected UIParameter param2;
	protected UIParameter param3;
	protected HtmlPanelGroup groupStartDate1;
	protected HtmlCommandExButton buttonStartDate2;
	protected HtmlBehavior behaviorStartDate4;
	protected HtmlPanelDialog dialogQty;
	protected HtmlPanelGrid grid2;
	protected HtmlOutputText text3;
	protected HtmlPanelGroup group2;
	protected HtmlCommandExButton button5;
	protected HtmlBehavior behavior5;
	protected HtmlPanelDialog dialogClubNum;
	protected HtmlPanelGrid gridClubLearnMore;
	protected HtmlOutputText textClubLearnMore;
	protected HtmlPanelGroup group3;
	protected HtmlCommandExButton button1;
	protected HtmlBehavior behavior2;
	protected HtmlPanelDialog dialogCVV;
	protected HtmlPanelGrid gridCVVLearn;
	protected HtmlPanelGroup group6;
	protected HtmlOutputText text4;
	protected HtmlPanelGroup group5;
	protected HtmlCommandExButton button4;
	protected HtmlBehavior behavior4;
	protected HtmlPanelDialog dialogDelMethodDialog;
	protected HtmlPanelGroup groupDelMethod1;
	protected HtmlPanelGrid gridDelMethod2;
	protected HtmlOutputText textDelMethodDetailHelp;
	protected HtmlCommandExButton buttonDelMethod;
	protected HtmlBehavior behaviorDelMethod4;
	protected HtmlPanelDialog dialogAdditionalAddrHelp;
	protected HtmlPanelGroup groupAddlAddrHelpGroup1;
	protected HtmlPanelGrid grid1;
	protected HtmlOutputText textAdditionalAddrLearnMoreInfoText;
	protected HtmlCommandExButton button3;
	protected HtmlBehavior behavior3;
	protected HtmlBehaviorKeyPress behaviorKeyPress1;
	protected HtmlInputHidden numberEZPayFreeWeeks;

	protected HtmlPanelGrid getGridGeoTrust() {
		if (gridGeoTrust == null) {
			gridGeoTrust = (HtmlPanelGrid) findComponentInRoot("gridGeoTrust");
		}
		return gridGeoTrust;
	}

	protected HtmlPanelDialog getDialogEZPayLearnMore1() {
		if (dialogEZPayLearnMore1 == null) {
			dialogEZPayLearnMore1 = (HtmlPanelDialog) findComponentInRoot("dialogEZPayLearnMore1");
		}
		return dialogEZPayLearnMore1;
	}

	protected HtmlPanelGrid getGridEZPayLearnMoreGrid2() {
		if (gridEZPayLearnMoreGrid2 == null) {
			gridEZPayLearnMoreGrid2 = (HtmlPanelGrid) findComponentInRoot("gridEZPayLearnMoreGrid2");
		}
		return gridEZPayLearnMoreGrid2;
	}

	protected HtmlOutputFormat getFormatEZPayHelpText() {
		if (formatEZPayHelpText == null) {
			formatEZPayHelpText = (HtmlOutputFormat) findComponentInRoot("formatEZPayHelpText");
		}
		return formatEZPayHelpText;
	}

	protected UIParameter getParam7() {
		if (param7 == null) {
			param7 = (UIParameter) findComponentInRoot("param7");
		}
		return param7;
	}

	protected UIParameter getParam8() {
		if (param8 == null) {
			param8 = (UIParameter) findComponentInRoot("param8");
		}
		return param8;
	}

	protected HtmlPanelGroup getGroup1() {
		if (group1 == null) {
			group1 = (HtmlPanelGroup) findComponentInRoot("group1");
		}
		return group1;
	}

	protected HtmlCommandExButton getButton2() {
		if (button2 == null) {
			button2 = (HtmlCommandExButton) findComponentInRoot("button2");
		}
		return button2;
	}

	protected HtmlBehavior getBehaviorEZPay4() {
		if (behaviorEZPay4 == null) {
			behaviorEZPay4 = (HtmlBehavior) findComponentInRoot("behaviorEZPay4");
		}
		return behaviorEZPay4;
	}

	protected HtmlPanelDialog getDialogStartDateLearnMore1() {
		if (dialogStartDateLearnMore1 == null) {
			dialogStartDateLearnMore1 = (HtmlPanelDialog) findComponentInRoot("dialogStartDateLearnMore1");
		}
		return dialogStartDateLearnMore1;
	}

	protected HtmlPanelGrid getGridStartDateLearnMoreGrid2() {
		if (gridStartDateLearnMoreGrid2 == null) {
			gridStartDateLearnMoreGrid2 = (HtmlPanelGrid) findComponentInRoot("gridStartDateLearnMoreGrid2");
		}
		return gridStartDateLearnMoreGrid2;
	}

	protected HtmlOutputFormat getFormatGiftFutureStartDateText() {
		if (formatGiftFutureStartDateText == null) {
			formatGiftFutureStartDateText = (HtmlOutputFormat) findComponentInRoot("formatGiftFutureStartDateText");
		}
		return formatGiftFutureStartDateText;
	}

	protected UIParameter getParam1() {
		if (param1 == null) {
			param1 = (UIParameter) findComponentInRoot("param1");
		}
		return param1;
	}

	protected UIParameter getParam2() {
		if (param2 == null) {
			param2 = (UIParameter) findComponentInRoot("param2");
		}
		return param2;
	}

	protected UIParameter getParam3() {
		if (param3 == null) {
			param3 = (UIParameter) findComponentInRoot("param3");
		}
		return param3;
	}

	protected HtmlPanelGroup getGroupStartDate1() {
		if (groupStartDate1 == null) {
			groupStartDate1 = (HtmlPanelGroup) findComponentInRoot("groupStartDate1");
		}
		return groupStartDate1;
	}

	protected HtmlCommandExButton getButtonStartDate2() {
		if (buttonStartDate2 == null) {
			buttonStartDate2 = (HtmlCommandExButton) findComponentInRoot("buttonStartDate2");
		}
		return buttonStartDate2;
	}

	protected HtmlBehavior getBehaviorStartDate4() {
		if (behaviorStartDate4 == null) {
			behaviorStartDate4 = (HtmlBehavior) findComponentInRoot("behaviorStartDate4");
		}
		return behaviorStartDate4;
	}

	protected HtmlPanelDialog getDialogQty() {
		if (dialogQty == null) {
			dialogQty = (HtmlPanelDialog) findComponentInRoot("dialogQty");
		}
		return dialogQty;
	}

	protected HtmlPanelGrid getGrid2() {
		if (grid2 == null) {
			grid2 = (HtmlPanelGrid) findComponentInRoot("grid2");
		}
		return grid2;
	}

	protected HtmlOutputText getText3() {
		if (text3 == null) {
			text3 = (HtmlOutputText) findComponentInRoot("text3");
		}
		return text3;
	}

	protected HtmlPanelGroup getGroup2() {
		if (group2 == null) {
			group2 = (HtmlPanelGroup) findComponentInRoot("group2");
		}
		return group2;
	}

	protected HtmlCommandExButton getButton5() {
		if (button5 == null) {
			button5 = (HtmlCommandExButton) findComponentInRoot("button5");
		}
		return button5;
	}

	protected HtmlBehavior getBehavior5() {
		if (behavior5 == null) {
			behavior5 = (HtmlBehavior) findComponentInRoot("behavior5");
		}
		return behavior5;
	}

	protected HtmlPanelDialog getDialogClubNum() {
		if (dialogClubNum == null) {
			dialogClubNum = (HtmlPanelDialog) findComponentInRoot("dialogClubNum");
		}
		return dialogClubNum;
	}

	protected HtmlPanelGrid getGridClubLearnMore() {
		if (gridClubLearnMore == null) {
			gridClubLearnMore = (HtmlPanelGrid) findComponentInRoot("gridClubLearnMore");
		}
		return gridClubLearnMore;
	}

	protected HtmlOutputText getTextClubLearnMore() {
		if (textClubLearnMore == null) {
			textClubLearnMore = (HtmlOutputText) findComponentInRoot("textClubLearnMore");
		}
		return textClubLearnMore;
	}

	protected HtmlPanelGroup getGroup3() {
		if (group3 == null) {
			group3 = (HtmlPanelGroup) findComponentInRoot("group3");
		}
		return group3;
	}

	protected HtmlCommandExButton getButton1() {
		if (button1 == null) {
			button1 = (HtmlCommandExButton) findComponentInRoot("button1");
		}
		return button1;
	}

	protected HtmlBehavior getBehavior2() {
		if (behavior2 == null) {
			behavior2 = (HtmlBehavior) findComponentInRoot("behavior2");
		}
		return behavior2;
	}

	protected HtmlPanelDialog getDialogCVV() {
		if (dialogCVV == null) {
			dialogCVV = (HtmlPanelDialog) findComponentInRoot("dialogCVV");
		}
		return dialogCVV;
	}

	protected HtmlPanelGrid getGridCVVLearn() {
		if (gridCVVLearn == null) {
			gridCVVLearn = (HtmlPanelGrid) findComponentInRoot("gridCVVLearn");
		}
		return gridCVVLearn;
	}

	protected HtmlPanelGroup getGroup6() {
		if (group6 == null) {
			group6 = (HtmlPanelGroup) findComponentInRoot("group6");
		}
		return group6;
	}

	protected HtmlOutputText getText4() {
		if (text4 == null) {
			text4 = (HtmlOutputText) findComponentInRoot("text4");
		}
		return text4;
	}

	protected HtmlPanelGroup getGroup5() {
		if (group5 == null) {
			group5 = (HtmlPanelGroup) findComponentInRoot("group5");
		}
		return group5;
	}

	protected HtmlCommandExButton getButton4() {
		if (button4 == null) {
			button4 = (HtmlCommandExButton) findComponentInRoot("button4");
		}
		return button4;
	}

	protected HtmlBehavior getBehavior4() {
		if (behavior4 == null) {
			behavior4 = (HtmlBehavior) findComponentInRoot("behavior4");
		}
		return behavior4;
	}

	protected HtmlPanelDialog getDialogDelMethodDialog() {
		if (dialogDelMethodDialog == null) {
			dialogDelMethodDialog = (HtmlPanelDialog) findComponentInRoot("dialogDelMethodDialog");
		}
		return dialogDelMethodDialog;
	}

	protected HtmlPanelGroup getGroupDelMethod1() {
		if (groupDelMethod1 == null) {
			groupDelMethod1 = (HtmlPanelGroup) findComponentInRoot("groupDelMethod1");
		}
		return groupDelMethod1;
	}

	protected HtmlPanelGrid getGridDelMethod2() {
		if (gridDelMethod2 == null) {
			gridDelMethod2 = (HtmlPanelGrid) findComponentInRoot("gridDelMethod2");
		}
		return gridDelMethod2;
	}

	protected HtmlOutputText getTextDelMethodDetailHelp() {
		if (textDelMethodDetailHelp == null) {
			textDelMethodDetailHelp = (HtmlOutputText) findComponentInRoot("textDelMethodDetailHelp");
		}
		return textDelMethodDetailHelp;
	}

	protected HtmlCommandExButton getButtonDelMethod() {
		if (buttonDelMethod == null) {
			buttonDelMethod = (HtmlCommandExButton) findComponentInRoot("buttonDelMethod");
		}
		return buttonDelMethod;
	}

	protected HtmlBehavior getBehaviorDelMethod4() {
		if (behaviorDelMethod4 == null) {
			behaviorDelMethod4 = (HtmlBehavior) findComponentInRoot("behaviorDelMethod4");
		}
		return behaviorDelMethod4;
	}

	protected HtmlPanelDialog getDialogAdditionalAddrHelp() {
		if (dialogAdditionalAddrHelp == null) {
			dialogAdditionalAddrHelp = (HtmlPanelDialog) findComponentInRoot("dialogAdditionalAddrHelp");
		}
		return dialogAdditionalAddrHelp;
	}

	protected HtmlPanelGroup getGroupAddlAddrHelpGroup1() {
		if (groupAddlAddrHelpGroup1 == null) {
			groupAddlAddrHelpGroup1 = (HtmlPanelGroup) findComponentInRoot("groupAddlAddrHelpGroup1");
		}
		return groupAddlAddrHelpGroup1;
	}

	protected HtmlPanelGrid getGrid1() {
		if (grid1 == null) {
			grid1 = (HtmlPanelGrid) findComponentInRoot("grid1");
		}
		return grid1;
	}

	protected HtmlOutputText getTextAdditionalAddrLearnMoreInfoText() {
		if (textAdditionalAddrLearnMoreInfoText == null) {
			textAdditionalAddrLearnMoreInfoText = (HtmlOutputText) findComponentInRoot("textAdditionalAddrLearnMoreInfoText");
		}
		return textAdditionalAddrLearnMoreInfoText;
	}

	protected HtmlCommandExButton getButton3() {
		if (button3 == null) {
			button3 = (HtmlCommandExButton) findComponentInRoot("button3");
		}
		return button3;
	}

	protected HtmlBehavior getBehavior3() {
		if (behavior3 == null) {
			behavior3 = (HtmlBehavior) findComponentInRoot("behavior3");
		}
		return behavior3;
	}

	protected HtmlBehaviorKeyPress getBehaviorKeyPress1() {
		if (behaviorKeyPress1 == null) {
			behaviorKeyPress1 = (HtmlBehaviorKeyPress) findComponentInRoot("behaviorKeyPress1");
		}
		return behaviorKeyPress1;
	}

	protected HtmlInputHidden getNumberEZPayFreeWeeks() {
		if (numberEZPayFreeWeeks == null) {
			numberEZPayFreeWeeks = (HtmlInputHidden) findComponentInRoot("numberEZPayFreeWeeks");
		}
		return numberEZPayFreeWeeks;
	}

}