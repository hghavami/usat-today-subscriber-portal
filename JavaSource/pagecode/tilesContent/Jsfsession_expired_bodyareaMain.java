/**
 * 
 */
package pagecode.tilesContent;

import pagecode.PageCodeBase;
import javax.faces.component.html.HtmlOutputText;
import com.ibm.faces.component.html.HtmlOutputLinkEx;

/**
 * @author aeast
 * 
 */
public class Jsfsession_expired_bodyareaMain extends PageCodeBase {

	protected HtmlOutputText text1;
	protected HtmlOutputText textSessionExpiredMessage;
	protected HtmlOutputText textSessionExpiredMessage2;
	protected HtmlOutputLinkEx linkEx1;

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlOutputText getTextSessionExpiredMessage() {
		if (textSessionExpiredMessage == null) {
			textSessionExpiredMessage = (HtmlOutputText) findComponentInRoot("textSessionExpiredMessage");
		}
		return textSessionExpiredMessage;
	}

	protected HtmlOutputText getTextSessionExpiredMessage2() {
		if (textSessionExpiredMessage2 == null) {
			textSessionExpiredMessage2 = (HtmlOutputText) findComponentInRoot("textSessionExpiredMessage2");
		}
		return textSessionExpiredMessage2;
	}

	protected HtmlOutputLinkEx getLinkEx1() {
		if (linkEx1 == null) {
			linkEx1 = (HtmlOutputLinkEx) findComponentInRoot("linkEx1");
		}
		return linkEx1;
	}

}