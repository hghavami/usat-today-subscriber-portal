/**
 * 
 */
package pagecode.tilesContent;

import pagecode.PageCodeBase;
import javax.faces.component.UINamingContainer;

/**
 * @author aeast
 * 
 */
public class Checkout_headarea extends PageCodeBase {

	protected UINamingContainer viewFragmentHeader;

	protected UINamingContainer getViewFragmentHeader() {
		if (viewFragmentHeader == null) {
			viewFragmentHeader = (UINamingContainer) findComponentInRoot("viewFragmentHeader");
		}
		return viewFragmentHeader;
	}

}