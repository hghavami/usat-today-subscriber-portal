/**
 * 
 */
package pagecode.giftcard;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.context.FacesContext;

import pagecode.PageCodeBase;

import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.businessObjects.util.mail.ExactTargetGiftCard;
import com.usatoday.esub.handlers.GiftCardHandler;
import com.usatoday.esub.handlers.ShoppingCartHandler;
import com.usatoday.util.constants.UsaTodayConstants;
import javax.faces.component.UISelectItem;
import javax.faces.component.html.HtmlSelectOneListbox;

/**
 * @author swong
 * 
 */
public class Sendcard extends PageCodeBase {

	protected HtmlScriptCollector scriptCollector1;
	protected HtmlForm form1;
	protected HtmlInputText text2;
	protected ShoppingCartHandler shoppingCartHandler;
	protected HtmlOutputText textSName1;
	protected HtmlPanelGroup group2;
	protected HtmlPanelGroup group1;
	protected HtmlPanelGrid grid1;
	protected HtmlInputText textSFName;
	protected HtmlOutputText textSLName1;
	protected HtmlPanelGroup group4;
	protected HtmlPanelGroup group3;
	protected HtmlPanelGrid grid2;
	protected HtmlInputText textSLName;
	protected HtmlOutputText textSEmail1;
	protected HtmlPanelGroup group6;
	protected HtmlPanelGroup group5;
	protected HtmlPanelGrid grid3;
	protected HtmlInputText textSEmail;
	protected HtmlOutputText textRFName1;
	protected HtmlPanelGroup group8;
	protected HtmlPanelGroup group7;
	protected HtmlPanelGrid grid4;
	protected HtmlInputText text1;
	protected HtmlOutputText textRLName1;
	protected HtmlPanelGroup group10;
	protected HtmlPanelGroup group9;
	protected HtmlPanelGrid grid5;
	protected HtmlInputText textRLName;
	protected HtmlOutputText textREmail1;
	protected HtmlPanelGroup group12;
	protected HtmlPanelGroup group11;
	protected HtmlPanelGrid grid6;
	protected HtmlInputText textREmail;
	protected HtmlOutputText textCustomLabel;
	protected HtmlPanelGroup group16;
	protected HtmlPanelGroup group15;
	protected HtmlPanelGrid grid8;
	protected HtmlInputTextarea textareaCustom;
	protected HtmlCommandExButton buttonSubmit;
	protected HtmlPanelGroup group14;
	protected HtmlPanelGroup group13;
	protected HtmlPanelGrid grid7;
	protected HtmlPanelGrid grid9;
	protected HtmlPanelGroup group17;
	protected HtmlPanelGroup group18;
	protected HtmlMessages messages1;
	protected HtmlOutputText textMainTableHeader;
	protected GiftCardHandler giftCardHandler;
	protected UISelectItem selectItem1;
	protected HtmlPanelGroup group20;
	protected HtmlPanelGroup group19;
	protected HtmlPanelGrid grid10;
	protected HtmlSelectOneListbox listboxPub;
	protected UISelectItem selectItem3;
	protected UISelectItem selectItem2;

	public String doButtonPlaceOrderAction() {
		String responseString = "success";
		GiftCardHandler newGiftCard = this.getGiftCardHandler();
		String sEmail = newGiftCard.getSubEmailAddress();
		String sFname = newGiftCard.getSubFirstName();
		String sLname = newGiftCard.getSubLastName();
		String refFname = newGiftCard.getRefFirstName();
		String refLname = newGiftCard.getRefLastName();
		String refEmail = newGiftCard.getRefEmailAddress();
		// String term = newGiftCard.getTerm();
		String pub = newGiftCard.getPub();
		// String startDate = newGiftCard.getStartDate();
		String refMsg = newGiftCard.getGiftMessage();

		// interaction

		String orderGiftCardID = UsaTodayConstants.EEDITION_GIFT_CARD_ID;

		String firstNL = sFname.toLowerCase();
		String firstNameU = firstNL.substring(0, 1).toUpperCase() + firstNL.substring(1);
		String lastNL = sLname.toLowerCase();
		String lastNameU = lastNL.substring(0, 1).toUpperCase() + lastNL.substring(1);
		String sGPName = firstNameU + " " + lastNameU;

		// String dateSub1 = startDate.substring(3,10);
		// String dateSub2 = startDate.substring(24,28);
		// String dateFormatted = dateSub1 + ",  " + dateSub2;

		try {
			// String returnCode = ExactTargetOrderGiftCard.sendExactTargetNoDates(refEmail,refFname,refLname,sGPName,sEmail,refMsg,
			// orderGiftCardID, pub);

			String returnCode = ExactTargetGiftCard.sendExactTargetNoDates(refEmail, refFname, refLname, sGPName, sEmail, refMsg,
					orderGiftCardID, pub);

			if (returnCode.equalsIgnoreCase("Error")) {
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
						"Sorry.  There were problems sending your email.  Please try again later.  Thank you.", null);

				context.addMessage(null, facesMsg);
				responseString = "failure";
			}

		} catch (Exception e) {
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
					"Sorry.  There were problems sending your email.  Please try again later.  Thank you.", null);

			context.addMessage(null, facesMsg);
			responseString = "failure";

		}

		return responseString;
	}

	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlForm getForm1() {
		if (form1 == null) {
			form1 = (HtmlForm) findComponentInRoot("form1");
		}
		return form1;
	}

	protected HtmlInputText getText2() {
		if (text2 == null) {
			text2 = (HtmlInputText) findComponentInRoot("text2");
		}
		return text2;
	}

	/**
	 * @managed-bean true
	 */
	protected ShoppingCartHandler getShoppingCartHandler() {
		if (shoppingCartHandler == null) {
			shoppingCartHandler = (ShoppingCartHandler) getManagedBean("shoppingCartHandler");
		}
		return shoppingCartHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setShoppingCartHandler(ShoppingCartHandler shoppingCartHandler) {
		this.shoppingCartHandler = shoppingCartHandler;
	}

	protected HtmlOutputText getTextSName1() {
		if (textSName1 == null) {
			textSName1 = (HtmlOutputText) findComponentInRoot("textSName1");
		}
		return textSName1;
	}

	protected HtmlPanelGroup getGroup2() {
		if (group2 == null) {
			group2 = (HtmlPanelGroup) findComponentInRoot("group2");
		}
		return group2;
	}

	protected HtmlPanelGroup getGroup1() {
		if (group1 == null) {
			group1 = (HtmlPanelGroup) findComponentInRoot("group1");
		}
		return group1;
	}

	protected HtmlPanelGrid getGrid1() {
		if (grid1 == null) {
			grid1 = (HtmlPanelGrid) findComponentInRoot("grid1");
		}
		return grid1;
	}

	protected HtmlInputText getTextSFName() {
		if (textSFName == null) {
			textSFName = (HtmlInputText) findComponentInRoot("textSFName");
		}
		return textSFName;
	}

	protected HtmlOutputText getTextSLName1() {
		if (textSLName1 == null) {
			textSLName1 = (HtmlOutputText) findComponentInRoot("textSLName1");
		}
		return textSLName1;
	}

	protected HtmlPanelGroup getGroup4() {
		if (group4 == null) {
			group4 = (HtmlPanelGroup) findComponentInRoot("group4");
		}
		return group4;
	}

	protected HtmlPanelGroup getGroup3() {
		if (group3 == null) {
			group3 = (HtmlPanelGroup) findComponentInRoot("group3");
		}
		return group3;
	}

	protected HtmlPanelGrid getGrid2() {
		if (grid2 == null) {
			grid2 = (HtmlPanelGrid) findComponentInRoot("grid2");
		}
		return grid2;
	}

	protected HtmlInputText getTextSLName() {
		if (textSLName == null) {
			textSLName = (HtmlInputText) findComponentInRoot("textSLName");
		}
		return textSLName;
	}

	protected HtmlOutputText getTextSEmail1() {
		if (textSEmail1 == null) {
			textSEmail1 = (HtmlOutputText) findComponentInRoot("textSEmail1");
		}
		return textSEmail1;
	}

	protected HtmlPanelGroup getGroup6() {
		if (group6 == null) {
			group6 = (HtmlPanelGroup) findComponentInRoot("group6");
		}
		return group6;
	}

	protected HtmlPanelGroup getGroup5() {
		if (group5 == null) {
			group5 = (HtmlPanelGroup) findComponentInRoot("group5");
		}
		return group5;
	}

	protected HtmlPanelGrid getGrid3() {
		if (grid3 == null) {
			grid3 = (HtmlPanelGrid) findComponentInRoot("grid3");
		}
		return grid3;
	}

	protected HtmlInputText getTextSEmail() {
		if (textSEmail == null) {
			textSEmail = (HtmlInputText) findComponentInRoot("textSEmail");
		}
		return textSEmail;
	}

	protected HtmlOutputText getTextRFName1() {
		if (textRFName1 == null) {
			textRFName1 = (HtmlOutputText) findComponentInRoot("textRFName1");
		}
		return textRFName1;
	}

	protected HtmlPanelGroup getGroup8() {
		if (group8 == null) {
			group8 = (HtmlPanelGroup) findComponentInRoot("group8");
		}
		return group8;
	}

	protected HtmlPanelGroup getGroup7() {
		if (group7 == null) {
			group7 = (HtmlPanelGroup) findComponentInRoot("group7");
		}
		return group7;
	}

	protected HtmlPanelGrid getGrid4() {
		if (grid4 == null) {
			grid4 = (HtmlPanelGrid) findComponentInRoot("grid4");
		}
		return grid4;
	}

	protected HtmlInputText getText1() {
		if (text1 == null) {
			text1 = (HtmlInputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlOutputText getTextRLName1() {
		if (textRLName1 == null) {
			textRLName1 = (HtmlOutputText) findComponentInRoot("textRLName1");
		}
		return textRLName1;
	}

	protected HtmlPanelGroup getGroup10() {
		if (group10 == null) {
			group10 = (HtmlPanelGroup) findComponentInRoot("group10");
		}
		return group10;
	}

	protected HtmlPanelGroup getGroup9() {
		if (group9 == null) {
			group9 = (HtmlPanelGroup) findComponentInRoot("group9");
		}
		return group9;
	}

	protected HtmlPanelGrid getGrid5() {
		if (grid5 == null) {
			grid5 = (HtmlPanelGrid) findComponentInRoot("grid5");
		}
		return grid5;
	}

	protected HtmlInputText getTextRLName() {
		if (textRLName == null) {
			textRLName = (HtmlInputText) findComponentInRoot("textRLName");
		}
		return textRLName;
	}

	protected HtmlOutputText getTextREmail1() {
		if (textREmail1 == null) {
			textREmail1 = (HtmlOutputText) findComponentInRoot("textREmail1");
		}
		return textREmail1;
	}

	protected HtmlPanelGroup getGroup12() {
		if (group12 == null) {
			group12 = (HtmlPanelGroup) findComponentInRoot("group12");
		}
		return group12;
	}

	protected HtmlPanelGroup getGroup11() {
		if (group11 == null) {
			group11 = (HtmlPanelGroup) findComponentInRoot("group11");
		}
		return group11;
	}

	protected HtmlPanelGrid getGrid6() {
		if (grid6 == null) {
			grid6 = (HtmlPanelGrid) findComponentInRoot("grid6");
		}
		return grid6;
	}

	protected HtmlInputText getTextREmail() {
		if (textREmail == null) {
			textREmail = (HtmlInputText) findComponentInRoot("textREmail");
		}
		return textREmail;
	}

	protected HtmlOutputText getTextCustomLabel() {
		if (textCustomLabel == null) {
			textCustomLabel = (HtmlOutputText) findComponentInRoot("textCustomLabel");
		}
		return textCustomLabel;
	}

	protected HtmlPanelGroup getGroup16() {
		if (group16 == null) {
			group16 = (HtmlPanelGroup) findComponentInRoot("group16");
		}
		return group16;
	}

	protected HtmlPanelGroup getGroup15() {
		if (group15 == null) {
			group15 = (HtmlPanelGroup) findComponentInRoot("group15");
		}
		return group15;
	}

	protected HtmlPanelGrid getGrid8() {
		if (grid8 == null) {
			grid8 = (HtmlPanelGrid) findComponentInRoot("grid8");
		}
		return grid8;
	}

	protected HtmlInputTextarea getTextareaCustom() {
		if (textareaCustom == null) {
			textareaCustom = (HtmlInputTextarea) findComponentInRoot("textareaCustom");
		}
		return textareaCustom;
	}

	protected HtmlCommandExButton getButtonSubmit() {
		if (buttonSubmit == null) {
			buttonSubmit = (HtmlCommandExButton) findComponentInRoot("buttonSubmit");
		}
		return buttonSubmit;
	}

	protected HtmlPanelGroup getGroup14() {
		if (group14 == null) {
			group14 = (HtmlPanelGroup) findComponentInRoot("group14");
		}
		return group14;
	}

	protected HtmlPanelGroup getGroup13() {
		if (group13 == null) {
			group13 = (HtmlPanelGroup) findComponentInRoot("group13");
		}
		return group13;
	}

	protected HtmlPanelGrid getGrid7() {
		if (grid7 == null) {
			grid7 = (HtmlPanelGrid) findComponentInRoot("grid7");
		}
		return grid7;
	}

	protected HtmlPanelGrid getGrid9() {
		if (grid9 == null) {
			grid9 = (HtmlPanelGrid) findComponentInRoot("grid9");
		}
		return grid9;
	}

	protected HtmlPanelGroup getGroup17() {
		if (group17 == null) {
			group17 = (HtmlPanelGroup) findComponentInRoot("group17");
		}
		return group17;
	}

	protected HtmlPanelGroup getGroup18() {
		if (group18 == null) {
			group18 = (HtmlPanelGroup) findComponentInRoot("group18");
		}
		return group18;
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

	protected HtmlOutputText getTextMainTableHeader() {
		if (textMainTableHeader == null) {
			textMainTableHeader = (HtmlOutputText) findComponentInRoot("textMainTableHeader");
		}
		return textMainTableHeader;
	}

	/**
	 * @managed-bean true
	 */
	protected GiftCardHandler getGiftCardHandler() {
		if (giftCardHandler == null) {
			giftCardHandler = (GiftCardHandler) getManagedBean("giftCardHandler");
		}
		return giftCardHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setGiftCardHandler(GiftCardHandler giftCardHandler) {
		this.giftCardHandler = giftCardHandler;
	}

	protected UISelectItem getSelectItem1() {
		if (selectItem1 == null) {
			selectItem1 = (UISelectItem) findComponentInRoot("selectItem1");
		}
		return selectItem1;
	}

	protected HtmlPanelGroup getGroup20() {
		if (group20 == null) {
			group20 = (HtmlPanelGroup) findComponentInRoot("group20");
		}
		return group20;
	}

	protected HtmlPanelGroup getGroup19() {
		if (group19 == null) {
			group19 = (HtmlPanelGroup) findComponentInRoot("group19");
		}
		return group19;
	}

	protected HtmlPanelGrid getGrid10() {
		if (grid10 == null) {
			grid10 = (HtmlPanelGrid) findComponentInRoot("grid10");
		}
		return grid10;
	}

	protected HtmlSelectOneListbox getListboxPub() {
		if (listboxPub == null) {
			listboxPub = (HtmlSelectOneListbox) findComponentInRoot("listboxPub");
		}
		return listboxPub;
	}

	protected UISelectItem getSelectItem3() {
		if (selectItem3 == null) {
			selectItem3 = (UISelectItem) findComponentInRoot("selectItem3");
		}
		return selectItem3;
	}

	protected UISelectItem getSelectItem2() {
		if (selectItem2 == null) {
			selectItem2 = (UISelectItem) findComponentInRoot("selectItem2");
		}
		return selectItem2;
	}

}