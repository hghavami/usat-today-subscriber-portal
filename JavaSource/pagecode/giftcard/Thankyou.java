/**
 * 
 */
package pagecode.giftcard;

import pagecode.PageCodeBase;
import com.ibm.faces.component.html.HtmlScriptCollector;
import javax.faces.component.html.HtmlOutputText;

/**
 * @author swong
 * 
 */
public class Thankyou extends PageCodeBase {

	protected HtmlScriptCollector scriptCollector1;
	protected HtmlOutputText textGiftEmailSent;
	protected HtmlOutputText mainHeader;

	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlOutputText getTextGiftEmailSent() {
		if (textGiftEmailSent == null) {
			textGiftEmailSent = (HtmlOutputText) findComponentInRoot("textGiftEmailSent");
		}
		return textGiftEmailSent;
	}

	protected HtmlOutputText getMainHeader() {
		if (mainHeader == null) {
			mainHeader = (HtmlOutputText) findComponentInRoot("mainHeader");
		}
		return mainHeader;
	}

}