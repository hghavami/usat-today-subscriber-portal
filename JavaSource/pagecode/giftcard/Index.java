/**
 * 
 */
package pagecode.giftcard;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.context.FacesContext;

import pagecode.PageCodeBase;

import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.businessObjects.util.mail.ExactTargetOrderGiftCard;
import com.usatoday.esub.handlers.GiftCardHandler;
import com.usatoday.esub.handlers.ShoppingCartHandler;
import com.usatoday.util.constants.UsaTodayConstants;

/**
 * @author swong
 * 
 */
public class Index extends PageCodeBase {

	protected HtmlScriptCollector scriptCollector1;
	protected HtmlPanelGrid grid1;
	protected HtmlPanelGroup group1;
	protected HtmlPanelGroup group2;
	protected HtmlForm form1;
	protected HtmlOutputText text1;
	protected HtmlPanelGrid grid3;
	protected HtmlPanelGroup group5;
	protected HtmlPanelGroup group6;
	protected HtmlInputTextarea textareaGiftMsg;
	protected HtmlOutputText text3;
	protected HtmlInputText textSenderEmail;
	protected HtmlOutputText text2;
	protected HtmlPanelGroup group4;
	protected HtmlPanelGroup group3;
	protected HtmlPanelGrid grid2;
	protected HtmlInputText textRecipientEmail;
	protected HtmlPanelGrid grid4;
	protected HtmlPanelGrid grid5;
	protected HtmlPanelGroup group7;
	protected HtmlPanelGroup group8;
	protected HtmlPanelGrid grid6;
	protected HtmlPanelGroup group9;
	protected HtmlPanelGroup group10;
	protected HtmlOutputText text4;
	protected HtmlInputText textSenderFName;
	protected HtmlOutputText text5;
	protected HtmlInputText textSenderLName;
	protected HtmlPanelGrid grid7;
	protected HtmlPanelGroup group11;
	protected HtmlPanelGroup group12;
	protected HtmlOutputText text6;
	protected HtmlInputText textRecipFName;
	protected HtmlPanelGrid grid8;
	protected HtmlPanelGroup group13;
	protected HtmlPanelGroup group14;
	protected HtmlOutputText text7;
	protected HtmlInputText textRecipLName;
	protected HtmlPanelGrid grid9;
	protected HtmlPanelGroup group15;
	protected HtmlPanelGroup group16;
	protected HtmlMessages messages1;
	protected HtmlOutputText text8;
	protected HtmlCommandExButton button1;
	protected HtmlOutputText textGiveHeader;
	protected HtmlOutputText mainHeader;
	protected HtmlOutputText textGiftUTLink;
	protected ShoppingCartHandler shoppingCartHandler;
	protected HtmlPanelGrid grid10;
	protected HtmlPanelGroup group17;
	protected HtmlPanelGroup group18;
	protected HtmlOutputLinkEx linkExUTGiftLink;
	protected HtmlOutputText textSWThankYouGift;
	protected HtmlOutputLinkEx linkExSWGiftLink;
	protected HtmlOutputText textGiftSWLink;
	protected HtmlOutputText textThankYouGift;
	protected HtmlJspPanel jspPanelUTGiftPanel;
	protected HtmlJspPanel jspPanelSWGiftPanel;
	protected GiftCardHandler giftCardHandler;

	public String doButtonPlaceOrderAction() {
		String responseString = "success";
		GiftCardHandler newGiftCard = this.getGiftCardHandler();
		String sEmail = newGiftCard.getSubEmailAddress();
		String sFname = newGiftCard.getSubFirstName();
		String sLname = newGiftCard.getSubLastName();
		String refFname = newGiftCard.getRefFirstName();
		String refLname = newGiftCard.getRefLastName();
		String refEmail = newGiftCard.getRefEmailAddress();
		String refMsg = newGiftCard.getGiftMessage();
		String pub = newGiftCard.getPub();

		String term = newGiftCard.getTerm();
		String startDate = newGiftCard.getStartDate();
		// interaction

		String giftCardID = UsaTodayConstants.EEDITION_ORDER_GIFT_CARD_ID;

		String firstNL = sFname.toLowerCase();
		String firstNameU = firstNL.substring(0, 1).toUpperCase() + firstNL.substring(1);
		String lastNL = sLname.toLowerCase();
		String lastNameU = lastNL.substring(0, 1).toUpperCase() + lastNL.substring(1);
		String sGPName = firstNameU + " " + lastNameU;

		String dateSub1 = startDate.substring(3, 10);
		String dateSub2 = startDate.substring(24, 28);
		String dateFormatted = dateSub1 + ",  " + dateSub2;

		try {
			// String returnCode = ExactTargetGiftCard.sendExactTargetNoDates(refEmail,refFname,refLname,sGPName,sEmail,refMsg,
			// giftCardID);
			String returnCode = ExactTargetOrderGiftCard.sendExactTargetNoDates(refEmail, refFname, refLname, sGPName, sEmail,
					refMsg, giftCardID, pub, term, dateFormatted);

			if (returnCode.equalsIgnoreCase("Error")) {
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
						"Sorry.  There were problems sending your email.  Please try again later.  Thank you.", null);

				context.addMessage(null, facesMsg);
				responseString = "failure";
			}

		} catch (Exception e) {
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
					"Sorry.  There were problems sending your email.  Please try again later.  Thank you.", null);

			context.addMessage(null, facesMsg);
			responseString = "failure";

		}

		return responseString;
	}

	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlPanelGrid getGrid1() {
		if (grid1 == null) {
			grid1 = (HtmlPanelGrid) findComponentInRoot("grid1");
		}
		return grid1;
	}

	protected HtmlPanelGroup getGroup1() {
		if (group1 == null) {
			group1 = (HtmlPanelGroup) findComponentInRoot("group1");
		}
		return group1;
	}

	protected HtmlPanelGroup getGroup2() {
		if (group2 == null) {
			group2 = (HtmlPanelGroup) findComponentInRoot("group2");
		}
		return group2;
	}

	protected HtmlForm getForm1() {
		if (form1 == null) {
			form1 = (HtmlForm) findComponentInRoot("form1");
		}
		return form1;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlPanelGrid getGrid3() {
		if (grid3 == null) {
			grid3 = (HtmlPanelGrid) findComponentInRoot("grid3");
		}
		return grid3;
	}

	protected HtmlPanelGroup getGroup5() {
		if (group5 == null) {
			group5 = (HtmlPanelGroup) findComponentInRoot("group5");
		}
		return group5;
	}

	protected HtmlPanelGroup getGroup6() {
		if (group6 == null) {
			group6 = (HtmlPanelGroup) findComponentInRoot("group6");
		}
		return group6;
	}

	protected HtmlInputTextarea getTextareaGiftMsg() {
		if (textareaGiftMsg == null) {
			textareaGiftMsg = (HtmlInputTextarea) findComponentInRoot("textareaGiftMsg");
		}
		return textareaGiftMsg;
	}

	protected HtmlOutputText getText3() {
		if (text3 == null) {
			text3 = (HtmlOutputText) findComponentInRoot("text3");
		}
		return text3;
	}

	protected HtmlInputText getTextSenderEmail() {
		if (textSenderEmail == null) {
			textSenderEmail = (HtmlInputText) findComponentInRoot("textSenderEmail");
		}
		return textSenderEmail;
	}

	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected HtmlPanelGroup getGroup4() {
		if (group4 == null) {
			group4 = (HtmlPanelGroup) findComponentInRoot("group4");
		}
		return group4;
	}

	protected HtmlPanelGroup getGroup3() {
		if (group3 == null) {
			group3 = (HtmlPanelGroup) findComponentInRoot("group3");
		}
		return group3;
	}

	protected HtmlPanelGrid getGrid2() {
		if (grid2 == null) {
			grid2 = (HtmlPanelGrid) findComponentInRoot("grid2");
		}
		return grid2;
	}

	protected HtmlInputText getTextRecipientEmail() {
		if (textRecipientEmail == null) {
			textRecipientEmail = (HtmlInputText) findComponentInRoot("textRecipientEmail");
		}
		return textRecipientEmail;
	}

	protected HtmlPanelGrid getGrid4() {
		if (grid4 == null) {
			grid4 = (HtmlPanelGrid) findComponentInRoot("grid4");
		}
		return grid4;
	}

	protected HtmlPanelGrid getGrid5() {
		if (grid5 == null) {
			grid5 = (HtmlPanelGrid) findComponentInRoot("grid5");
		}
		return grid5;
	}

	protected HtmlPanelGroup getGroup7() {
		if (group7 == null) {
			group7 = (HtmlPanelGroup) findComponentInRoot("group7");
		}
		return group7;
	}

	protected HtmlPanelGroup getGroup8() {
		if (group8 == null) {
			group8 = (HtmlPanelGroup) findComponentInRoot("group8");
		}
		return group8;
	}

	protected HtmlPanelGrid getGrid6() {
		if (grid6 == null) {
			grid6 = (HtmlPanelGrid) findComponentInRoot("grid6");
		}
		return grid6;
	}

	protected HtmlPanelGroup getGroup9() {
		if (group9 == null) {
			group9 = (HtmlPanelGroup) findComponentInRoot("group9");
		}
		return group9;
	}

	protected HtmlPanelGroup getGroup10() {
		if (group10 == null) {
			group10 = (HtmlPanelGroup) findComponentInRoot("group10");
		}
		return group10;
	}

	protected HtmlOutputText getText4() {
		if (text4 == null) {
			text4 = (HtmlOutputText) findComponentInRoot("text4");
		}
		return text4;
	}

	protected HtmlInputText getTextSenderFName() {
		if (textSenderFName == null) {
			textSenderFName = (HtmlInputText) findComponentInRoot("textSenderFName");
		}
		return textSenderFName;
	}

	protected HtmlOutputText getText5() {
		if (text5 == null) {
			text5 = (HtmlOutputText) findComponentInRoot("text5");
		}
		return text5;
	}

	protected HtmlInputText getTextSenderLName() {
		if (textSenderLName == null) {
			textSenderLName = (HtmlInputText) findComponentInRoot("textSenderLName");
		}
		return textSenderLName;
	}

	protected HtmlPanelGrid getGrid7() {
		if (grid7 == null) {
			grid7 = (HtmlPanelGrid) findComponentInRoot("grid7");
		}
		return grid7;
	}

	protected HtmlPanelGroup getGroup11() {
		if (group11 == null) {
			group11 = (HtmlPanelGroup) findComponentInRoot("group11");
		}
		return group11;
	}

	protected HtmlPanelGroup getGroup12() {
		if (group12 == null) {
			group12 = (HtmlPanelGroup) findComponentInRoot("group12");
		}
		return group12;
	}

	protected HtmlOutputText getText6() {
		if (text6 == null) {
			text6 = (HtmlOutputText) findComponentInRoot("text6");
		}
		return text6;
	}

	protected HtmlInputText getTextRecipFName() {
		if (textRecipFName == null) {
			textRecipFName = (HtmlInputText) findComponentInRoot("textRecipFName");
		}
		return textRecipFName;
	}

	protected HtmlPanelGrid getGrid8() {
		if (grid8 == null) {
			grid8 = (HtmlPanelGrid) findComponentInRoot("grid8");
		}
		return grid8;
	}

	protected HtmlPanelGroup getGroup13() {
		if (group13 == null) {
			group13 = (HtmlPanelGroup) findComponentInRoot("group13");
		}
		return group13;
	}

	protected HtmlPanelGroup getGroup14() {
		if (group14 == null) {
			group14 = (HtmlPanelGroup) findComponentInRoot("group14");
		}
		return group14;
	}

	protected HtmlOutputText getText7() {
		if (text7 == null) {
			text7 = (HtmlOutputText) findComponentInRoot("text7");
		}
		return text7;
	}

	protected HtmlInputText getTextRecipLName() {
		if (textRecipLName == null) {
			textRecipLName = (HtmlInputText) findComponentInRoot("textRecipLName");
		}
		return textRecipLName;
	}

	protected HtmlPanelGrid getGrid9() {
		if (grid9 == null) {
			grid9 = (HtmlPanelGrid) findComponentInRoot("grid9");
		}
		return grid9;
	}

	protected HtmlPanelGroup getGroup15() {
		if (group15 == null) {
			group15 = (HtmlPanelGroup) findComponentInRoot("group15");
		}
		return group15;
	}

	protected HtmlPanelGroup getGroup16() {
		if (group16 == null) {
			group16 = (HtmlPanelGroup) findComponentInRoot("group16");
		}
		return group16;
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

	public void handleTextSenderFNameValidate(FacesContext facescontext, UIComponent component, Object object)
			throws javax.faces.validator.ValidatorException {
		// Type Java code to handle validate here

		// void validate(FacesContext facescontext, UIComponent component, Object object) throws ValidatorException
	}

	protected HtmlOutputText getText8() {
		if (text8 == null) {
			text8 = (HtmlOutputText) findComponentInRoot("text8");
		}
		return text8;
	}

	protected HtmlCommandExButton getButton1() {
		if (button1 == null) {
			button1 = (HtmlCommandExButton) findComponentInRoot("button1");
		}
		return button1;
	}

	protected HtmlOutputText getTextGiveHeader() {
		if (textGiveHeader == null) {
			textGiveHeader = (HtmlOutputText) findComponentInRoot("textGiveHeader");
		}
		return textGiveHeader;
	}

	protected HtmlOutputText getMainHeader() {
		if (mainHeader == null) {
			mainHeader = (HtmlOutputText) findComponentInRoot("mainHeader");
		}
		return mainHeader;
	}

	protected HtmlOutputText getTextGiftUTLink() {
		if (textGiftUTLink == null) {
			textGiftUTLink = (HtmlOutputText) findComponentInRoot("textGiftUTLink");
		}
		return textGiftUTLink;
	}

	protected HtmlOutputLinkEx getLinkExUTGiftLink() {
		if (linkExUTGiftLink == null) {
			linkExUTGiftLink = (HtmlOutputLinkEx) findComponentInRoot("linkExUTGiftLink");
		}
		return linkExUTGiftLink;
	}

	/**
	 * @managed-bean true
	 */
	protected ShoppingCartHandler getShoppingCartHandler() {
		if (shoppingCartHandler == null) {
			shoppingCartHandler = (ShoppingCartHandler) getManagedBean("shoppingCartHandler");
		}
		return shoppingCartHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setShoppingCartHandler(ShoppingCartHandler shoppingCartHandler) {
		this.shoppingCartHandler = shoppingCartHandler;
	}

	protected HtmlPanelGrid getGrid10() {
		if (grid10 == null) {
			grid10 = (HtmlPanelGrid) findComponentInRoot("grid10");
		}
		return grid10;
	}

	protected HtmlPanelGroup getGroup17() {
		if (group17 == null) {
			group17 = (HtmlPanelGroup) findComponentInRoot("group17");
		}
		return group17;
	}

	protected HtmlPanelGroup getGroup18() {
		if (group18 == null) {
			group18 = (HtmlPanelGroup) findComponentInRoot("group18");
		}
		return group18;
	}

	protected HtmlOutputText getTextSWThankYouGift() {
		if (textSWThankYouGift == null) {
			textSWThankYouGift = (HtmlOutputText) findComponentInRoot("textSWThankYouGift");
		}
		return textSWThankYouGift;
	}

	protected HtmlOutputLinkEx getLinkExSWGiftLink() {
		if (linkExSWGiftLink == null) {
			linkExSWGiftLink = (HtmlOutputLinkEx) findComponentInRoot("linkExSWGiftLink");
		}
		return linkExSWGiftLink;
	}

	protected HtmlOutputText getTextGiftSWLink() {
		if (textGiftSWLink == null) {
			textGiftSWLink = (HtmlOutputText) findComponentInRoot("textGiftSWLink");
		}
		return textGiftSWLink;
	}

	protected HtmlOutputText getTextThankYouGift() {
		if (textThankYouGift == null) {
			textThankYouGift = (HtmlOutputText) findComponentInRoot("textThankYouGift");
		}
		return textThankYouGift;
	}

	protected HtmlJspPanel getJspPanelUTGiftPanel() {
		if (jspPanelUTGiftPanel == null) {
			jspPanelUTGiftPanel = (HtmlJspPanel) findComponentInRoot("jspPanelUTGiftPanel");
		}
		return jspPanelUTGiftPanel;
	}

	protected HtmlJspPanel getJspPanelSWGiftPanel() {
		if (jspPanelSWGiftPanel == null) {
			jspPanelSWGiftPanel = (HtmlJspPanel) findComponentInRoot("jspPanelSWGiftPanel");
		}
		return jspPanelSWGiftPanel;
	}

	/**
	 * @managed-bean true
	 */
	protected GiftCardHandler getGiftCardHandler() {
		if (giftCardHandler == null) {
			giftCardHandler = (GiftCardHandler) getManagedBean("giftCardHandler");
		}
		return giftCardHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setGiftCardHandler(GiftCardHandler giftCardHandler) {
		this.giftCardHandler = giftCardHandler;
	}

}