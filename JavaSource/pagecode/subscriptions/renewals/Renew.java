/**
 * 
 */
package pagecode.subscriptions.renewals;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIParameter;
import javax.faces.component.UISelectItem;
import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputFormat;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.component.html.HtmlSelectOneRadio;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import pagecode.PageCodeBase;

import com.ibm.faces.component.html.HtmlBehavior;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlFormItem;
import com.ibm.faces.component.html.HtmlGraphicImageEx;
import com.ibm.faces.component.html.HtmlInputHelperAssist;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlPanelDialog;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.UsatException;
import com.usatoday.business.interfaces.customer.ContactIntf;
import com.usatoday.business.interfaces.products.OfferIntf;
import com.usatoday.business.interfaces.products.SubscriptionProductIntf;
import com.usatoday.business.interfaces.products.SubscriptionTermsIntf;
import com.usatoday.business.interfaces.products.promotions.PromotionIntf;
import com.usatoday.business.interfaces.shopping.OrderIntf;
import com.usatoday.business.interfaces.shopping.OrderItemIntf;
import com.usatoday.business.interfaces.shopping.RenewalOrderItemIntf;
import com.usatoday.business.interfaces.shopping.ShoppingCartIntf;
import com.usatoday.business.interfaces.shopping.SubscriptionOrderItemIntf;
import com.usatoday.businessObjects.customer.CreditCardInfoBO;
import com.usatoday.businessObjects.customer.ExpiredSubscriptionRenewedAccessBO;
import com.usatoday.businessObjects.customer.service.CustomerServiceEmails;
import com.usatoday.businessObjects.products.SubscriptionProductBO;
import com.usatoday.businessObjects.shopping.CheckOutService;
import com.usatoday.businessObjects.shopping.RenewalOrderItemBO;
import com.usatoday.businessObjects.shopping.payment.CreditCardPaymentMethodBO;
import com.usatoday.esub.handlers.ShoppingCartHandler;
import com.usatoday.esub.handlers.SubscriptionOfferHandler;
import com.usatoday.esub.ncs.handlers.CustomerHandler;
import com.usatoday.esub.ncs.handlers.SubscriberAccountHandler;
import com.usatoday.esub.ncs.handlers.SubscriptionRenewalHandler;
import com.usatoday.util.constants.UsaTodayConstants;

/**
 * @author aeast
 * 
 */
public class Renew extends PageCodeBase {

	protected HtmlOutputText textMainTableHeader;
	protected HtmlScriptCollector renewalFormScriptCollector;
	protected HtmlForm formOrderEntryForm;
	protected HtmlMessages messages1;
	protected HtmlFormItem formItemSubscriptionTerms;
	protected HtmlSelectOneRadio radioTermsSelection;
	protected HtmlInputHelperAssist assist1;
	protected HtmlPanelFormBox formBoxTermsFormBox;
	protected SubscriptionOfferHandler currentOfferHandler;
	protected CustomerHandler customerHandler;
	protected SubscriptionRenewalHandler renewalHandler;
	protected HtmlPanelGrid gridCustomerInfoBorderGrid;
	protected HtmlPanelGrid gridProductDataGrid;
	protected HtmlPanelGroup group2;
	protected HtmlPanelGrid grid1;
	protected HtmlOutputText text2;
	protected HtmlPanelGroup group3;
	protected HtmlPanelGrid gridDelInfoHeadGrid;
	protected HtmlOutputText textDeliveryInfoHeaderText;
	protected HtmlPanelGroup groupBillingInfoHeader3;
	protected HtmlPanelGrid gridBillingInfoHeadGrid;
	protected HtmlOutputText textBillingInfoHeaderText;
	protected HtmlGraphicImageEx imageExRenewalMarkeingImg1;
	protected HtmlOutputText textProductNameLabel;
	protected HtmlOutputText textProductName;
	protected HtmlOutputText textAccountNumberLabel;
	protected HtmlOutputText textAccountNumber;
	protected HtmlPanelGrid gridDeliveryInfoGrid;
	protected HtmlOutputText text1;
	protected HtmlOutputText text5;
	protected HtmlOutputText text6;
	protected HtmlOutputText text7;
	protected HtmlOutputText text8;
	protected HtmlOutputText text9;
	protected HtmlPanelGrid gridBillingInfoGrid;
	protected HtmlOutputText textBill1;
	protected HtmlOutputText textBill5;
	protected HtmlOutputText textBill6;
	protected HtmlOutputText textBill7;
	protected HtmlOutputText textBill8;
	protected HtmlPanelGrid gridRenewalMarketingGrid;
	protected HtmlPanelGrid gridEZPayLearnMoreGrid2;
	protected HtmlCommandExButton button2;
	protected HtmlBehavior behaviorEZPay4;
	protected HtmlPanelDialog dialogEZPayLearnMore1;
	protected HtmlPanelGroup group1;
	protected HtmlPanelGrid gridCVVLearn;
	protected HtmlPanelGroup group6666;
	protected HtmlOutputText text44444;
	protected HtmlCommandExButton button4444;
	protected HtmlBehavior behavior4444;
	protected HtmlPanelDialog dialogCVV;
	protected HtmlPanelGroup group5;
	protected HtmlPanelFormBox formBoxPaymentHeaderSelection;
	protected HtmlFormItem formItemUseExistingCardItem;
	protected UISelectItem selectItem1;
	protected UISelectItem selectItem311;
	protected UISelectItems selectItemsExistingCCExpirationYears;
	protected UISelectItem selectItem16;
	protected UISelectItems selectItemsCCExpirationYears;
	protected UISelectItem selectItem2;
	protected HtmlInputHelperAssist assist2;
	protected UISelectItem selectItem411;
	protected UISelectItem selectItem511;
	protected UISelectItem selectItem611;
	protected UISelectItem selectItem711;
	protected UISelectItem selectItem811;
	protected UISelectItem selectItem911;
	protected UISelectItem selectItem1011;
	protected UISelectItem selectItem1111;
	protected UISelectItem selectItem1211;
	protected UISelectItem selectItem1311;
	protected UISelectItem selectItem1411;
	protected HtmlInputHelperAssist assist6;
	protected UISelectItem selectItem3;
	protected UISelectItem selectItem4;
	protected UISelectItem selectItem5;
	protected UISelectItem selectItem6;
	protected UISelectItem selectItem7;
	protected UISelectItem selectItem8;
	protected UISelectItem selectItem9;
	protected UISelectItem selectItem10;
	protected UISelectItem selectItem11;
	protected UISelectItem selectItem12;
	protected UISelectItem selectItem13;
	protected UISelectItem selectItem14;
	protected HtmlSelectOneRadio radioRenewalPaymentOption;
	protected HtmlPanelFormBox formBoxExistingCardVerifyExpirationForm;
	protected HtmlFormItem formItemExistingCCExpirationDate;
	protected HtmlSelectOneMenu menuExistingCCExpireMonth;
	protected HtmlPanelGrid gridExistingCCpayementheader;
	protected HtmlOutputText text3;
	protected HtmlSelectOneMenu menuExistingCCExpireYear;
	protected HtmlPanelFormBox formBoxPaymentInfo;
	protected HtmlFormItem formItemCreditCardNumber;
	protected HtmlInputText textCreditCardNumber;
	protected HtmlInputText textCVV;
	protected HtmlSelectOneMenu menuCCExpireMonth;
	protected HtmlPanelGrid panelGridCreditCardImageGrid;
	protected HtmlJspPanel jspPanelCreditCardImages;
	protected HtmlGraphicImageEx imageExAmEx1;
	protected HtmlFormItem formItemCreditCardCVV;
	protected HtmlOutputText textCVVLearnMore;
	protected HtmlFormItem formItemCCExpirationDate;
	protected HtmlSelectOneMenu menuCCExpireYear;
	protected HtmlGraphicImageEx imageExDiscover1;
	protected HtmlGraphicImageEx imageExMasterCard1;
	protected HtmlGraphicImageEx imageExVisaLogo1;
	protected HtmlOutputText text4;
	protected HtmlPanelGrid gridOnEZPayGrid;
	protected HtmlPanelGrid gridNotOnEZPayGrid;
	protected HtmlOutputText textCOPPAText;
	protected HtmlPanelGrid COPPAGrid;
	protected HtmlGraphicImageEx imageEx3;
	protected HtmlPanelGrid panelGridFormSubmissionGrid;
	protected HtmlCommandExButton buttonPlaceOrder;
	protected HtmlJspPanel jspPanelGeoTrustPanel;
	protected HtmlGraphicImageEx imageEx33333;
	protected HtmlJspPanel pleaseWaitPanel;
	protected HtmlPanelGrid gridRenewalDisclaimerGrid;
	protected HtmlInputHidden renewWithCardOnFailAvailable;
	protected HtmlPanelFormBox formBoxPaymentSelection;
	protected ShoppingCartHandler shoppingCartHandler;
	protected HtmlInputHidden showOverlayPopUp;
	protected HtmlOutputFormat format1;
	protected UIParameter param1;
	protected HtmlPanelGrid gridEZPAYOptionsGrid;
	protected HtmlOutputText text10;
	protected HtmlSelectOneRadio radioRenewalOptions;
	protected UISelectItem selectItem15;
	protected UISelectItem selectItem17;
	protected HtmlPanelGrid gridEZPAYRequiredGrid;
	protected HtmlOutputText textForcedEzPayInfoText;
	protected HtmlOutputFormat textEZPayHelpText;
	protected HtmlJspPanel jspPanel1;
	protected HtmlOutputText textPageJS2;
	protected HtmlOutputText textPageJS3;
	protected HtmlPanelGrid gridTermsFormBoxBottomFacetGrid;
	protected HtmlOutputText textEZPayExplanationMessage;
	protected HtmlOutputText textEZPayLearnMore;
	protected HtmlOutputText textEZPayDisclaimer;

	protected HtmlOutputText getTextMainTableHeader() {
		if (textMainTableHeader == null) {
			textMainTableHeader = (HtmlOutputText) findComponentInRoot("textMainTableHeader");
		}
		return textMainTableHeader;
	}

	protected HtmlScriptCollector getRenewalFormScriptCollector() {
		if (renewalFormScriptCollector == null) {
			renewalFormScriptCollector = (HtmlScriptCollector) findComponentInRoot("renewalFormScriptCollector");
		}
		return renewalFormScriptCollector;
	}

	protected HtmlForm getFormOrderEntryForm() {
		if (formOrderEntryForm == null) {
			formOrderEntryForm = (HtmlForm) findComponentInRoot("formOrderEntryForm");
		}
		return formOrderEntryForm;
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

	protected HtmlFormItem getFormItemSubscriptionTerms() {
		if (formItemSubscriptionTerms == null) {
			formItemSubscriptionTerms = (HtmlFormItem) findComponentInRoot("formItemSubscriptionTerms");
		}
		return formItemSubscriptionTerms;
	}

	protected HtmlSelectOneRadio getRadioTermsSelection() {
		if (radioTermsSelection == null) {
			radioTermsSelection = (HtmlSelectOneRadio) findComponentInRoot("radioTermsSelection");
		}
		return radioTermsSelection;
	}

	protected HtmlInputHelperAssist getAssist1() {
		if (assist1 == null) {
			assist1 = (HtmlInputHelperAssist) findComponentInRoot("assist1");
		}
		return assist1;
	}

	protected HtmlPanelFormBox getFormBoxTermsFormBox() {
		if (formBoxTermsFormBox == null) {
			formBoxTermsFormBox = (HtmlPanelFormBox) findComponentInRoot("formBoxTermsFormBox");
		}
		return formBoxTermsFormBox;
	}

	/**
	 * @managed-bean true
	 */
	protected SubscriptionOfferHandler getCurrentOfferHandler() {
		if (currentOfferHandler == null) {
			currentOfferHandler = (SubscriptionOfferHandler) getManagedBean("currentOfferHandler");
		}
		return currentOfferHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setCurrentOfferHandler(SubscriptionOfferHandler currentOfferHandler) {
		this.currentOfferHandler = currentOfferHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected CustomerHandler getCustomerHandler() {
		if (customerHandler == null) {
			customerHandler = (CustomerHandler) getManagedBean("customerHandler");
		}
		return customerHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setCustomerHandler(CustomerHandler customerHandler) {
		this.customerHandler = customerHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected SubscriptionRenewalHandler getRenewalHandler() {
		if (renewalHandler == null) {
			renewalHandler = (SubscriptionRenewalHandler) getManagedBean("renewalHandler");
		}
		return renewalHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setRenewalHandler(SubscriptionRenewalHandler renewalHandler) {
		this.renewalHandler = renewalHandler;
	}

	protected HtmlPanelGrid getGridCustomerInfoBorderGrid() {
		if (gridCustomerInfoBorderGrid == null) {
			gridCustomerInfoBorderGrid = (HtmlPanelGrid) findComponentInRoot("gridCustomerInfoBorderGrid");
		}
		return gridCustomerInfoBorderGrid;
	}

	protected HtmlPanelGrid getGridProductDataGrid() {
		if (gridProductDataGrid == null) {
			gridProductDataGrid = (HtmlPanelGrid) findComponentInRoot("gridProductDataGrid");
		}
		return gridProductDataGrid;
	}

	protected HtmlPanelGroup getGroup2() {
		if (group2 == null) {
			group2 = (HtmlPanelGroup) findComponentInRoot("group2");
		}
		return group2;
	}

	protected HtmlPanelGrid getGrid1() {
		if (grid1 == null) {
			grid1 = (HtmlPanelGrid) findComponentInRoot("grid1");
		}
		return grid1;
	}

	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected HtmlPanelGroup getGroup3() {
		if (group3 == null) {
			group3 = (HtmlPanelGroup) findComponentInRoot("group3");
		}
		return group3;
	}

	protected HtmlPanelGrid getGridDelInfoHeadGrid() {
		if (gridDelInfoHeadGrid == null) {
			gridDelInfoHeadGrid = (HtmlPanelGrid) findComponentInRoot("gridDelInfoHeadGrid");
		}
		return gridDelInfoHeadGrid;
	}

	protected HtmlOutputText getTextDeliveryInfoHeaderText() {
		if (textDeliveryInfoHeaderText == null) {
			textDeliveryInfoHeaderText = (HtmlOutputText) findComponentInRoot("textDeliveryInfoHeaderText");
		}
		return textDeliveryInfoHeaderText;
	}

	protected HtmlPanelGroup getGroupBillingInfoHeader3() {
		if (groupBillingInfoHeader3 == null) {
			groupBillingInfoHeader3 = (HtmlPanelGroup) findComponentInRoot("groupBillingInfoHeader3");
		}
		return groupBillingInfoHeader3;
	}

	protected HtmlPanelGrid getGridBillingInfoHeadGrid() {
		if (gridBillingInfoHeadGrid == null) {
			gridBillingInfoHeadGrid = (HtmlPanelGrid) findComponentInRoot("gridBillingInfoHeadGrid");
		}
		return gridBillingInfoHeadGrid;
	}

	protected HtmlOutputText getTextBillingInfoHeaderText() {
		if (textBillingInfoHeaderText == null) {
			textBillingInfoHeaderText = (HtmlOutputText) findComponentInRoot("textBillingInfoHeaderText");
		}
		return textBillingInfoHeaderText;
	}

	protected HtmlGraphicImageEx getImageExRenewalMarkeingImg1() {
		if (imageExRenewalMarkeingImg1 == null) {
			imageExRenewalMarkeingImg1 = (HtmlGraphicImageEx) findComponentInRoot("imageExRenewalMarkeingImg1");
		}
		return imageExRenewalMarkeingImg1;
	}

	protected HtmlOutputText getTextProductNameLabel() {
		if (textProductNameLabel == null) {
			textProductNameLabel = (HtmlOutputText) findComponentInRoot("textProductNameLabel");
		}
		return textProductNameLabel;
	}

	protected HtmlOutputText getTextProductName() {
		if (textProductName == null) {
			textProductName = (HtmlOutputText) findComponentInRoot("textProductName");
		}
		return textProductName;
	}

	protected HtmlOutputText getTextAccountNumberLabel() {
		if (textAccountNumberLabel == null) {
			textAccountNumberLabel = (HtmlOutputText) findComponentInRoot("textAccountNumberLabel");
		}
		return textAccountNumberLabel;
	}

	protected HtmlOutputText getTextAccountNumber() {
		if (textAccountNumber == null) {
			textAccountNumber = (HtmlOutputText) findComponentInRoot("textAccountNumber");
		}
		return textAccountNumber;
	}

	protected HtmlPanelGrid getGridDeliveryInfoGrid() {
		if (gridDeliveryInfoGrid == null) {
			gridDeliveryInfoGrid = (HtmlPanelGrid) findComponentInRoot("gridDeliveryInfoGrid");
		}
		return gridDeliveryInfoGrid;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlOutputText getText5() {
		if (text5 == null) {
			text5 = (HtmlOutputText) findComponentInRoot("text5");
		}
		return text5;
	}

	protected HtmlOutputText getText6() {
		if (text6 == null) {
			text6 = (HtmlOutputText) findComponentInRoot("text6");
		}
		return text6;
	}

	protected HtmlOutputText getText7() {
		if (text7 == null) {
			text7 = (HtmlOutputText) findComponentInRoot("text7");
		}
		return text7;
	}

	protected HtmlOutputText getText8() {
		if (text8 == null) {
			text8 = (HtmlOutputText) findComponentInRoot("text8");
		}
		return text8;
	}

	protected HtmlOutputText getText9() {
		if (text9 == null) {
			text9 = (HtmlOutputText) findComponentInRoot("text9");
		}
		return text9;
	}

	protected HtmlPanelGrid getGridBillingInfoGrid() {
		if (gridBillingInfoGrid == null) {
			gridBillingInfoGrid = (HtmlPanelGrid) findComponentInRoot("gridBillingInfoGrid");
		}
		return gridBillingInfoGrid;
	}

	protected HtmlOutputText getTextBill1() {
		if (textBill1 == null) {
			textBill1 = (HtmlOutputText) findComponentInRoot("textBill1");
		}
		return textBill1;
	}

	protected HtmlOutputText getTextBill5() {
		if (textBill5 == null) {
			textBill5 = (HtmlOutputText) findComponentInRoot("textBill5");
		}
		return textBill5;
	}

	protected HtmlOutputText getTextBill6() {
		if (textBill6 == null) {
			textBill6 = (HtmlOutputText) findComponentInRoot("textBill6");
		}
		return textBill6;
	}

	protected HtmlOutputText getTextBill7() {
		if (textBill7 == null) {
			textBill7 = (HtmlOutputText) findComponentInRoot("textBill7");
		}
		return textBill7;
	}

	protected HtmlOutputText getTextBill8() {
		if (textBill8 == null) {
			textBill8 = (HtmlOutputText) findComponentInRoot("textBill8");
		}
		return textBill8;
	}

	protected HtmlPanelGrid getGridRenewalMarketingGrid() {
		if (gridRenewalMarketingGrid == null) {
			gridRenewalMarketingGrid = (HtmlPanelGrid) findComponentInRoot("gridRenewalMarketingGrid");
		}
		return gridRenewalMarketingGrid;
	}

	protected HtmlPanelGrid getGridEZPayLearnMoreGrid2() {
		if (gridEZPayLearnMoreGrid2 == null) {
			gridEZPayLearnMoreGrid2 = (HtmlPanelGrid) findComponentInRoot("gridEZPayLearnMoreGrid2");
		}
		return gridEZPayLearnMoreGrid2;
	}

	protected HtmlCommandExButton getButton2() {
		if (button2 == null) {
			button2 = (HtmlCommandExButton) findComponentInRoot("button2");
		}
		return button2;
	}

	protected HtmlBehavior getBehaviorEZPay4() {
		if (behaviorEZPay4 == null) {
			behaviorEZPay4 = (HtmlBehavior) findComponentInRoot("behaviorEZPay4");
		}
		return behaviorEZPay4;
	}

	protected HtmlPanelDialog getDialogEZPayLearnMore1() {
		if (dialogEZPayLearnMore1 == null) {
			dialogEZPayLearnMore1 = (HtmlPanelDialog) findComponentInRoot("dialogEZPayLearnMore1");
		}
		return dialogEZPayLearnMore1;
	}

	protected HtmlPanelGroup getGroup1() {
		if (group1 == null) {
			group1 = (HtmlPanelGroup) findComponentInRoot("group1");
		}
		return group1;
	}

	protected HtmlPanelGrid getGridCVVLearn() {
		if (gridCVVLearn == null) {
			gridCVVLearn = (HtmlPanelGrid) findComponentInRoot("gridCVVLearn");
		}
		return gridCVVLearn;
	}

	protected HtmlPanelGroup getGroup6666() {
		if (group6666 == null) {
			group6666 = (HtmlPanelGroup) findComponentInRoot("group6666");
		}
		return group6666;
	}

	protected HtmlOutputText getText44444() {
		if (text44444 == null) {
			text44444 = (HtmlOutputText) findComponentInRoot("text44444");
		}
		return text44444;
	}

	protected HtmlCommandExButton getButton4444() {
		if (button4444 == null) {
			button4444 = (HtmlCommandExButton) findComponentInRoot("button4444");
		}
		return button4444;
	}

	protected HtmlBehavior getBehavior4444() {
		if (behavior4444 == null) {
			behavior4444 = (HtmlBehavior) findComponentInRoot("behavior4444");
		}
		return behavior4444;
	}

	protected HtmlPanelDialog getDialogCVV() {
		if (dialogCVV == null) {
			dialogCVV = (HtmlPanelDialog) findComponentInRoot("dialogCVV");
		}
		return dialogCVV;
	}

	protected HtmlPanelGroup getGroup5() {
		if (group5 == null) {
			group5 = (HtmlPanelGroup) findComponentInRoot("group5");
		}
		return group5;
	}

	public void onPageLoadBegin(FacesContext facescontext) {
		// Type Java code to handle page load begin event here

		// void <method>(FacesContext facescontext)

		SubscriptionRenewalHandler srh = this.getRenewalHandler();

		CustomerHandler ch = this.getCustomerHandler();

		if (srh.getCurrentCustomer() == null) {
			srh.setCurrentCustomer(ch);
		}

		// If NI trans type, then the account is pending and no rates can be displayed
		if (!srh.isSubscriptionActive()) {
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage facesMsg = null;
			if (srh.getCurrentCustomer().getCustomer().getCurrentAccount().isOnEZPay()) {

				facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
						"Your subscription in on EZ-Pay automatic payment plan.  Pay Bill is unavailable.", null);
			} else {
				facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
						"Your subscription currently is pending, please try later or call our customer service.", null);
			}
			context.addMessage(null, facesMsg);
		} else {
			try {
				SubscriptionTermsIntf selTerm = ch.getCurrentAccount().getRenewalOffer().getRenewalTerm(srh.getSelectedTerm());

				if (selTerm == null) {

					for (SubscriptionTermsIntf term : ch.getCurrentAccount().getRenewalTerms()) {
						srh.setSelectedTerm(term.getTermAsString());
						// No terms, no pay bill
						if (term.getDescription() == null || term.getDescription().trim().equals("")) {
							srh.setSubscriptionActive(false);
							FacesContext context = FacesContext.getCurrentInstance();
							FacesMessage facesMsg = new FacesMessage(
									FacesMessage.SEVERITY_INFO,
									"Your subscription currently is unavailable for pay bill, please try later or call our customer service.",
									null);

							context.addMessage(null, facesMsg);
						}
						this.getRadioTermsSelection().resetValue();
						break;
					}
				}
				// this.getNumberEZPayFreeWeeks().resetValue();
			} catch (Exception e) {
				; // ignore
			}
		}

	}

	protected HtmlPanelFormBox getFormBoxPaymentHeaderSelection() {
		if (formBoxPaymentHeaderSelection == null) {
			formBoxPaymentHeaderSelection = (HtmlPanelFormBox) findComponentInRoot("formBoxPaymentHeaderSelection");
		}
		return formBoxPaymentHeaderSelection;
	}

	protected HtmlFormItem getFormItemUseExistingCardItem() {
		if (formItemUseExistingCardItem == null) {
			formItemUseExistingCardItem = (HtmlFormItem) findComponentInRoot("formItemUseExistingCardItem");
		}
		return formItemUseExistingCardItem;
	}

	protected UISelectItem getSelectItem1() {
		if (selectItem1 == null) {
			selectItem1 = (UISelectItem) findComponentInRoot("selectItem1");
		}
		return selectItem1;
	}

	protected UISelectItem getSelectItem311() {
		if (selectItem311 == null) {
			selectItem311 = (UISelectItem) findComponentInRoot("selectItem311");
		}
		return selectItem311;
	}

	protected UISelectItems getSelectItemsExistingCCExpirationYears() {
		if (selectItemsExistingCCExpirationYears == null) {
			selectItemsExistingCCExpirationYears = (UISelectItems) findComponentInRoot("selectItemsExistingCCExpirationYears");
		}
		return selectItemsExistingCCExpirationYears;
	}

	protected UISelectItem getSelectItem16() {
		if (selectItem16 == null) {
			selectItem16 = (UISelectItem) findComponentInRoot("selectItem16");
		}
		return selectItem16;
	}

	protected UISelectItems getSelectItemsCCExpirationYears() {
		if (selectItemsCCExpirationYears == null) {
			selectItemsCCExpirationYears = (UISelectItems) findComponentInRoot("selectItemsCCExpirationYears");
		}
		return selectItemsCCExpirationYears;
	}

	protected UISelectItem getSelectItem2() {
		if (selectItem2 == null) {
			selectItem2 = (UISelectItem) findComponentInRoot("selectItem2");
		}
		return selectItem2;
	}

	protected HtmlInputHelperAssist getAssist2() {
		if (assist2 == null) {
			assist2 = (HtmlInputHelperAssist) findComponentInRoot("assist2");
		}
		return assist2;
	}

	protected UISelectItem getSelectItem411() {
		if (selectItem411 == null) {
			selectItem411 = (UISelectItem) findComponentInRoot("selectItem411");
		}
		return selectItem411;
	}

	protected UISelectItem getSelectItem511() {
		if (selectItem511 == null) {
			selectItem511 = (UISelectItem) findComponentInRoot("selectItem511");
		}
		return selectItem511;
	}

	protected UISelectItem getSelectItem611() {
		if (selectItem611 == null) {
			selectItem611 = (UISelectItem) findComponentInRoot("selectItem611");
		}
		return selectItem611;
	}

	protected UISelectItem getSelectItem711() {
		if (selectItem711 == null) {
			selectItem711 = (UISelectItem) findComponentInRoot("selectItem711");
		}
		return selectItem711;
	}

	protected UISelectItem getSelectItem811() {
		if (selectItem811 == null) {
			selectItem811 = (UISelectItem) findComponentInRoot("selectItem811");
		}
		return selectItem811;
	}

	protected UISelectItem getSelectItem911() {
		if (selectItem911 == null) {
			selectItem911 = (UISelectItem) findComponentInRoot("selectItem911");
		}
		return selectItem911;
	}

	protected UISelectItem getSelectItem1011() {
		if (selectItem1011 == null) {
			selectItem1011 = (UISelectItem) findComponentInRoot("selectItem1011");
		}
		return selectItem1011;
	}

	protected UISelectItem getSelectItem1111() {
		if (selectItem1111 == null) {
			selectItem1111 = (UISelectItem) findComponentInRoot("selectItem1111");
		}
		return selectItem1111;
	}

	protected UISelectItem getSelectItem1211() {
		if (selectItem1211 == null) {
			selectItem1211 = (UISelectItem) findComponentInRoot("selectItem1211");
		}
		return selectItem1211;
	}

	protected UISelectItem getSelectItem1311() {
		if (selectItem1311 == null) {
			selectItem1311 = (UISelectItem) findComponentInRoot("selectItem1311");
		}
		return selectItem1311;
	}

	protected UISelectItem getSelectItem1411() {
		if (selectItem1411 == null) {
			selectItem1411 = (UISelectItem) findComponentInRoot("selectItem1411");
		}
		return selectItem1411;
	}

	protected HtmlInputHelperAssist getAssist6() {
		if (assist6 == null) {
			assist6 = (HtmlInputHelperAssist) findComponentInRoot("assist6");
		}
		return assist6;
	}

	protected UISelectItem getSelectItem3() {
		if (selectItem3 == null) {
			selectItem3 = (UISelectItem) findComponentInRoot("selectItem3");
		}
		return selectItem3;
	}

	protected UISelectItem getSelectItem4() {
		if (selectItem4 == null) {
			selectItem4 = (UISelectItem) findComponentInRoot("selectItem4");
		}
		return selectItem4;
	}

	protected UISelectItem getSelectItem5() {
		if (selectItem5 == null) {
			selectItem5 = (UISelectItem) findComponentInRoot("selectItem5");
		}
		return selectItem5;
	}

	protected UISelectItem getSelectItem6() {
		if (selectItem6 == null) {
			selectItem6 = (UISelectItem) findComponentInRoot("selectItem6");
		}
		return selectItem6;
	}

	protected UISelectItem getSelectItem7() {
		if (selectItem7 == null) {
			selectItem7 = (UISelectItem) findComponentInRoot("selectItem7");
		}
		return selectItem7;
	}

	protected UISelectItem getSelectItem8() {
		if (selectItem8 == null) {
			selectItem8 = (UISelectItem) findComponentInRoot("selectItem8");
		}
		return selectItem8;
	}

	protected UISelectItem getSelectItem9() {
		if (selectItem9 == null) {
			selectItem9 = (UISelectItem) findComponentInRoot("selectItem9");
		}
		return selectItem9;
	}

	protected UISelectItem getSelectItem10() {
		if (selectItem10 == null) {
			selectItem10 = (UISelectItem) findComponentInRoot("selectItem10");
		}
		return selectItem10;
	}

	protected UISelectItem getSelectItem11() {
		if (selectItem11 == null) {
			selectItem11 = (UISelectItem) findComponentInRoot("selectItem11");
		}
		return selectItem11;
	}

	protected UISelectItem getSelectItem12() {
		if (selectItem12 == null) {
			selectItem12 = (UISelectItem) findComponentInRoot("selectItem12");
		}
		return selectItem12;
	}

	protected UISelectItem getSelectItem13() {
		if (selectItem13 == null) {
			selectItem13 = (UISelectItem) findComponentInRoot("selectItem13");
		}
		return selectItem13;
	}

	protected UISelectItem getSelectItem14() {
		if (selectItem14 == null) {
			selectItem14 = (UISelectItem) findComponentInRoot("selectItem14");
		}
		return selectItem14;
	}

	protected HtmlSelectOneRadio getRadioRenewalPaymentOption() {
		if (radioRenewalPaymentOption == null) {
			radioRenewalPaymentOption = (HtmlSelectOneRadio) findComponentInRoot("radioRenewalPaymentOption");
		}
		return radioRenewalPaymentOption;
	}

	protected HtmlPanelFormBox getFormBoxExistingCardVerifyExpirationForm() {
		if (formBoxExistingCardVerifyExpirationForm == null) {
			formBoxExistingCardVerifyExpirationForm = (HtmlPanelFormBox) findComponentInRoot("formBoxExistingCardVerifyExpirationForm");
		}
		return formBoxExistingCardVerifyExpirationForm;
	}

	protected HtmlFormItem getFormItemExistingCCExpirationDate() {
		if (formItemExistingCCExpirationDate == null) {
			formItemExistingCCExpirationDate = (HtmlFormItem) findComponentInRoot("formItemExistingCCExpirationDate");
		}
		return formItemExistingCCExpirationDate;
	}

	protected HtmlSelectOneMenu getMenuExistingCCExpireMonth() {
		if (menuExistingCCExpireMonth == null) {
			menuExistingCCExpireMonth = (HtmlSelectOneMenu) findComponentInRoot("menuExistingCCExpireMonth");
		}
		return menuExistingCCExpireMonth;
	}

	protected HtmlPanelGrid getGridExistingCCpayementheader() {
		if (gridExistingCCpayementheader == null) {
			gridExistingCCpayementheader = (HtmlPanelGrid) findComponentInRoot("gridExistingCCpayementheader");
		}
		return gridExistingCCpayementheader;
	}

	protected HtmlOutputText getText3() {
		if (text3 == null) {
			text3 = (HtmlOutputText) findComponentInRoot("text3");
		}
		return text3;
	}

	protected HtmlSelectOneMenu getMenuExistingCCExpireYear() {
		if (menuExistingCCExpireYear == null) {
			menuExistingCCExpireYear = (HtmlSelectOneMenu) findComponentInRoot("menuExistingCCExpireYear");
		}
		return menuExistingCCExpireYear;
	}

	protected HtmlPanelFormBox getFormBoxPaymentInfo() {
		if (formBoxPaymentInfo == null) {
			formBoxPaymentInfo = (HtmlPanelFormBox) findComponentInRoot("formBoxPaymentInfo");
		}
		return formBoxPaymentInfo;
	}

	protected HtmlFormItem getFormItemCreditCardNumber() {
		if (formItemCreditCardNumber == null) {
			formItemCreditCardNumber = (HtmlFormItem) findComponentInRoot("formItemCreditCardNumber");
		}
		return formItemCreditCardNumber;
	}

	protected HtmlInputText getTextCreditCardNumber() {
		if (textCreditCardNumber == null) {
			textCreditCardNumber = (HtmlInputText) findComponentInRoot("textCreditCardNumber");
		}
		return textCreditCardNumber;
	}

	protected HtmlInputText getTextCVV() {
		if (textCVV == null) {
			textCVV = (HtmlInputText) findComponentInRoot("textCVV");
		}
		return textCVV;
	}

	protected HtmlSelectOneMenu getMenuCCExpireMonth() {
		if (menuCCExpireMonth == null) {
			menuCCExpireMonth = (HtmlSelectOneMenu) findComponentInRoot("menuCCExpireMonth");
		}
		return menuCCExpireMonth;
	}

	protected HtmlPanelGrid getPanelGridCreditCardImageGrid() {
		if (panelGridCreditCardImageGrid == null) {
			panelGridCreditCardImageGrid = (HtmlPanelGrid) findComponentInRoot("panelGridCreditCardImageGrid");
		}
		return panelGridCreditCardImageGrid;
	}

	protected HtmlJspPanel getJspPanelCreditCardImages() {
		if (jspPanelCreditCardImages == null) {
			jspPanelCreditCardImages = (HtmlJspPanel) findComponentInRoot("jspPanelCreditCardImages");
		}
		return jspPanelCreditCardImages;
	}

	protected HtmlGraphicImageEx getImageExAmEx1() {
		if (imageExAmEx1 == null) {
			imageExAmEx1 = (HtmlGraphicImageEx) findComponentInRoot("imageExAmEx1");
		}
		return imageExAmEx1;
	}

	protected HtmlFormItem getFormItemCreditCardCVV() {
		if (formItemCreditCardCVV == null) {
			formItemCreditCardCVV = (HtmlFormItem) findComponentInRoot("formItemCreditCardCVV");
		}
		return formItemCreditCardCVV;
	}

	protected HtmlOutputText getTextCVVLearnMore() {
		if (textCVVLearnMore == null) {
			textCVVLearnMore = (HtmlOutputText) findComponentInRoot("textCVVLearnMore");
		}
		return textCVVLearnMore;
	}

	protected HtmlFormItem getFormItemCCExpirationDate() {
		if (formItemCCExpirationDate == null) {
			formItemCCExpirationDate = (HtmlFormItem) findComponentInRoot("formItemCCExpirationDate");
		}
		return formItemCCExpirationDate;
	}

	protected HtmlSelectOneMenu getMenuCCExpireYear() {
		if (menuCCExpireYear == null) {
			menuCCExpireYear = (HtmlSelectOneMenu) findComponentInRoot("menuCCExpireYear");
		}
		return menuCCExpireYear;
	}

	protected HtmlGraphicImageEx getImageExDiscover1() {
		if (imageExDiscover1 == null) {
			imageExDiscover1 = (HtmlGraphicImageEx) findComponentInRoot("imageExDiscover1");
		}
		return imageExDiscover1;
	}

	protected HtmlGraphicImageEx getImageExMasterCard1() {
		if (imageExMasterCard1 == null) {
			imageExMasterCard1 = (HtmlGraphicImageEx) findComponentInRoot("imageExMasterCard1");
		}
		return imageExMasterCard1;
	}

	protected HtmlGraphicImageEx getImageExVisaLogo1() {
		if (imageExVisaLogo1 == null) {
			imageExVisaLogo1 = (HtmlGraphicImageEx) findComponentInRoot("imageExVisaLogo1");
		}
		return imageExVisaLogo1;
	}

	protected HtmlOutputText getText4() {
		if (text4 == null) {
			text4 = (HtmlOutputText) findComponentInRoot("text4");
		}
		return text4;
	}

	protected HtmlPanelGrid getGridOnEZPayGrid() {
		if (gridOnEZPayGrid == null) {
			gridOnEZPayGrid = (HtmlPanelGrid) findComponentInRoot("gridOnEZPayGrid");
		}
		return gridOnEZPayGrid;
	}

	protected HtmlPanelGrid getGridNotOnEZPayGrid() {
		if (gridNotOnEZPayGrid == null) {
			gridNotOnEZPayGrid = (HtmlPanelGrid) findComponentInRoot("gridNotOnEZPayGrid");
		}
		return gridNotOnEZPayGrid;
	}

	protected HtmlOutputText getTextCOPPAText() {
		if (textCOPPAText == null) {
			textCOPPAText = (HtmlOutputText) findComponentInRoot("textCOPPAText");
		}
		return textCOPPAText;
	}

	protected HtmlPanelGrid getCOPPAGrid() {
		if (COPPAGrid == null) {
			COPPAGrid = (HtmlPanelGrid) findComponentInRoot("COPPAGrid");
		}
		return COPPAGrid;
	}

	protected HtmlGraphicImageEx getImageEx3() {
		if (imageEx3 == null) {
			imageEx3 = (HtmlGraphicImageEx) findComponentInRoot("imageEx3");
		}
		return imageEx3;
	}

	protected HtmlPanelGrid getPanelGridFormSubmissionGrid() {
		if (panelGridFormSubmissionGrid == null) {
			panelGridFormSubmissionGrid = (HtmlPanelGrid) findComponentInRoot("panelGridFormSubmissionGrid");
		}
		return panelGridFormSubmissionGrid;
	}

	protected HtmlCommandExButton getButtonPlaceOrder() {
		if (buttonPlaceOrder == null) {
			buttonPlaceOrder = (HtmlCommandExButton) findComponentInRoot("buttonPlaceOrder");
		}
		return buttonPlaceOrder;
	}

	protected HtmlJspPanel getJspPanelGeoTrustPanel() {
		if (jspPanelGeoTrustPanel == null) {
			jspPanelGeoTrustPanel = (HtmlJspPanel) findComponentInRoot("jspPanelGeoTrustPanel");
		}
		return jspPanelGeoTrustPanel;
	}

	protected HtmlGraphicImageEx getImageEx33333() {
		if (imageEx33333 == null) {
			imageEx33333 = (HtmlGraphicImageEx) findComponentInRoot("imageEx33333");
		}
		return imageEx33333;
	}

	protected HtmlJspPanel getPleaseWaitPanel() {
		if (pleaseWaitPanel == null) {
			pleaseWaitPanel = (HtmlJspPanel) findComponentInRoot("pleaseWaitPanel");
		}
		return pleaseWaitPanel;
	}

	protected HtmlPanelGrid getGridRenewalDisclaimerGrid() {
		if (gridRenewalDisclaimerGrid == null) {
			gridRenewalDisclaimerGrid = (HtmlPanelGrid) findComponentInRoot("gridRenewalDisclaimerGrid");
		}
		return gridRenewalDisclaimerGrid;
	}

	protected HtmlInputHidden getRenewWithCardOnFailAvailable() {
		if (renewWithCardOnFailAvailable == null) {
			renewWithCardOnFailAvailable = (HtmlInputHidden) findComponentInRoot("renewWithCardOnFailAvailable");
		}
		return renewWithCardOnFailAvailable;
	}

	protected HtmlPanelFormBox getFormBoxPaymentSelection() {
		if (formBoxPaymentSelection == null) {
			formBoxPaymentSelection = (HtmlPanelFormBox) findComponentInRoot("formBoxPaymentSelection");
		}
		return formBoxPaymentSelection;
	}

	public String submitRenewalAction() {

		// This is java code that runs when this action method is invoked
		String responseString = "failure";
		SubscriptionRenewalHandler rh = this.getRenewalHandler();
		CustomerHandler ch = this.getCustomerHandler();
		rh.setCurrentCustomer(ch);
		SubscriberAccountHandler sah = ch.getCurrentAccount();

		ShoppingCartHandler sch = this.getShoppingCartHandler();
		ShoppingCartIntf cart = sch.getCart();

		// check if refresh
		if (sch.getLastOrder() != null) {
			// if an order placed during this session
			try {
				for (OrderItemIntf item : sch.getLastOrder().getOrderedItems()) {
					if (item instanceof RenewalOrderItemIntf) {
						RenewalOrderItemIntf rItem = (RenewalOrderItemIntf) item;
						if (rItem.getAccount() != null) {
							// duplicate form submitted
							if (rItem.getAccount().getAccountNumber().equalsIgnoreCase(sah.getAccount().getAccountNumber())) {
								FacesContext context = FacesContext.getCurrentInstance();
								FacesMessage facesMsg = new FacesMessage(
										FacesMessage.SEVERITY_INFO,
										"Duplicate form submission detected. A Pay Bill transaction for this account has recently been placed. ",
										null);
								context.addMessage(null, facesMsg);
								responseString = "failure";
								return responseString;
								// responseString = "success";
								// return responseString;
							}
						}
					}
				}
			} catch (Exception e) {
			}
		}

		// multi threaded hell
		boolean changeCheckoutStatusInFinallyBlock = true;
		try {

			// HttpSession session = (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);

			if (sch.isCheckoutStarted() || cart.isCheckoutInProcess()) {
				changeCheckoutStatusInFinallyBlock = false; // don't update the status...the original thread will do it.
				return "checkoutInProgress";
			} else {
				sch.setCheckoutStarted(true);
			}

			cart.setBillingContact(null);
			cart.setDeliveryContact(null);
			cart.setCheckOutErrorMessage(null);
			cart.setPaymentMethod(null);
			cart.clearItems();

			OfferIntf offer = sah.getRenewalOffer();

			SubscriptionTermsIntf selectedTerm = null;

			if (rh.getSelectedTerm() == null || offer == null) {
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Please choose a subscription Term.", null);

				context.addMessage(null, facesMsg);
				responseString = "failure";
				return responseString;
			}

			selectedTerm = offer.getTerm(rh.getSelectedTerm());

			if (selectedTerm == null) { // should never fall in here but check none the less
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
						"Unexpected Error...Unable to process order. Can't locate selected term.", null);

				context.addMessage(null, facesMsg);
				responseString = "failure";
				return responseString;
			}

			SubscriptionProductIntf product = SubscriptionProductBO.getSubscriptionProduct(sah.getAccount().getPubCode());

			product.applyOffer(offer);

			product.applyTerm(selectedTerm);

			RenewalOrderItemBO item = new RenewalOrderItemBO();

			ContactIntf deliveryContact = sah.getAccount().getDeliveryContact();

			item.setOneTimeBill(false);
			item.setChoosingEZPay(!rh.getIsChoseBillMeForFutureRenewal());

			if (rh.getIsChoseBillMeForFutureRenewal() && (selectedTerm.requiresEZPAY() || offer.isForceEZPay())) {
				rh.setSelectedRenewalOption(SubscriptionOrderItemIntf.EZPAY);
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage facesMsg = new FacesMessage(
						FacesMessage.SEVERITY_INFO,
						"The selected subscription rate requires that you sign up for the EZ-PAY renewal option. Bill Me, is not available for EZ-PAY required term lengths.",
						null);

				context.addMessage(null, facesMsg);
				responseString = "failure";
				return responseString;
			}

			item.setAccount(sah.getAccount());
			item.setDeliveryZip(deliveryContact.getUIAddress().getZip());
			item.setProduct(product);
			item.setOffer(offer);

			item.setQuantity(sah.getAccount().getNumberOfPapers());
			item.setSelectedTerm(selectedTerm);

			item.setGiftItem(!sah.getAccount().isBillingSameAsDelivery());

			item.setDeliveryEmailAddress(deliveryContact.getEmailAddress());
			item.setKeyCode(sah.getAccount().getKeyCode());

			// try {
			// String renewalTrackingCode = (String)session.getAttribute(UTCommon.SESSION_RENEWAL_TRACKING_CODE);
			// if (renewalTrackingCode != null) {
			// item.setRenewalTrackingCode(renewalTrackingCode);
			// }
			// }
			// catch (Exception e) {
			// // simply ignore any problems with renewal tracking codes
			// }

			item.setExistingDeliveryMethod(sah.getAccount().getDeliveryMethod());

			CreditCardPaymentMethodBO payment = new CreditCardPaymentMethodBO();

			ContactIntf billingContact = sah.getAccount().getBillingContact();

			// process payment instrument new or existing card
			if ("new_cc_info".equalsIgnoreCase(rh.getSelectedPaymentOption())) {
				// new card
				if (rh.getCreditCardNumber() == null || rh.getCreditCardNumber().equals("")) {
					FacesContext context = FacesContext.getCurrentInstance();
					FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
							"Please enter a Valid Credit Card Number.", null);
					context.addMessage(null, facesMsg);
					responseString = "failure";
					return responseString;
				} else {
					try {
						payment.setCardNumber(rh.getCreditCardNumber());
					} catch (UsatException e) {
						FacesContext context = FacesContext.getCurrentInstance();
						FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
								"The credit card number you entered is not recognized. Please check the number and try again.",
								null);

						context.addMessage(null, facesMsg);
						responseString = "failure";
						return responseString;
					}
				}
				if (sah.getAccount().isBillingSameAsDelivery()) {
					payment.setNameOnCard(deliveryContact.getFirstName() + " " + deliveryContact.getLastName());
				} else {
					payment.setNameOnCard(billingContact.getFirstName() + " " + billingContact.getLastName());
				}

				// Check credit card CVV number
				if (rh.getCreditCardVerificationNumber() == null || rh.getCreditCardVerificationNumber().trim().equals("")) {

					PromotionIntf cvvRequired = offer.getPromotionSet().getCVVRequiredOverride();

					// Add check if key code allows skipping of CVV -
					// basically if the promotion is non null then CVV is optional, if null, it's required.
					if (cvvRequired == null) {

						FacesContext context = FacesContext.getCurrentInstance();
						FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
								"Please enter a valid Credit Card Verification Number.", null);

						context.addMessage(null, facesMsg);
						responseString = "failure";
						return responseString;
					}
				} else {
					payment.setCVV(rh.getCreditCardVerificationNumber());
				}

				// Check credit card expiration
				String expirationYear = rh.getCreditCardExpirationYear();
				String expirationMonth = rh.getCreditCardExpirationMonth();
				if (expirationMonth == null || expirationMonth.equalsIgnoreCase("-1") || expirationYear == null
						|| expirationYear.equalsIgnoreCase("-1")) {
					FacesContext context = FacesContext.getCurrentInstance();
					FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
							"Please select a valid Credit Card Expiration Date.", null);

					context.addMessage(null, facesMsg);
					responseString = "failure";
					return responseString;
				} else {
					payment.setExpirationMonth(expirationMonth);
					payment.setExpirationYear(expirationYear);
				}

			} else { // else use credit card on file
				try {
					// Add iSeries credit number retrieval code here
					CreditCardInfoBO creditCardInfoBO = new CreditCardInfoBO();
					String accountNum = String.format("%7s", sah.getAccount().getAccountNumber());
					creditCardInfoBO = creditCardInfoBO.determineCreditCardInfo(sah.getAccount().getPubCode(), accountNum, "", "",
							"");
					String creditNumber = creditCardInfoBO.getCreditCardNum();

					if (sah.getAccount().isBillingSameAsDelivery()) {
						payment.setNameOnCard(deliveryContact.getFirstName() + " " + deliveryContact.getLastName());
					} else {
						payment.setNameOnCard(billingContact.getFirstName() + " " + billingContact.getLastName());
					}

					payment.setCardNumber(creditNumber);
					payment.setExpirationMonth(rh.getExistingCreditCardExpirationMonth());
					payment.setExpirationYear(rh.getExistingCreditCardExpirationYear());

				} catch (Exception e) {
					throw new Exception(
							"Failed to process existing credit card. Please try using the New Card option. Error Detail: "
									+ e.getMessage());
				}
			}

			String clientIP = null;
			try {
				HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
				clientIP = req.getRemoteAddr();
			} catch (Exception e) {
				// ignore
				clientIP = "";
			}

			// make sure the customer object is pointing to the correct account.
			ch.getCustomer().setCurrentAccount(sah.getAccount().getAccountNumber());
			cart.setOwningCustomer(ch.getCustomer());
			cart.addItem(item);
			cart.setPaymentMethod(payment);
			cart.setClientIPAddress(clientIP);

			CheckOutService checkout = new CheckOutService();

			OrderIntf order = checkout.checkOutShoppingCart(cart, UsaTodayConstants.BATCH_PROCESS_CC_NETWORK_FAILURES);

			responseString = "success";

			sch.setLastOrder(order);

			rh.resetForNewOrder();

			try {

				CustomerServiceEmails.sendRenewalConfirmationEmail(ch.getCustomer(), sah.getAccount(), order);

			} catch (UsatException usex) {
				System.out.println("Failed to send renewal confirmation email for customer: ");
				usex.printStackTrace();
			}

			try {
				// If XI subscription status, then write a temporary record to allow access to eEdition
				ExpiredSubscriptionRenewedAccessBO esraBO = new ExpiredSubscriptionRenewedAccessBO();
				esraBO.setAccountNum(sah.getAccount().getAccountNumber());
				esraBO.setEmail(deliveryContact.getEmailAddress());
				esraBO.setFirmName(deliveryContact.getFirmName());
				esraBO.setFirstName(deliveryContact.getFirstName());
				esraBO.setLastName(deliveryContact.getLastName());
				esraBO.setPublication(sah.getAccount().getPubCode());
				esraBO.setOrderID(0);
				// If renewing expired subscription, check and write a renewed access record
				if (sah.getAccount().getTransRecType() != null
						&& !sah.getAccount().getTransRecType().substring(0, 1).equalsIgnoreCase("P")) {
					// If renewed access record does not already exist, write one
					if (!esraBO.select(esraBO)) {
						esraBO.save(esraBO);
					}
					// If other subscription status, remove any previously written expired subscription renewed access record
				} else {
					esraBO.delete(esraBO);
				}
			} catch (Exception e) {
				e.printStackTrace();
				// don't fail because of ExpiredAccountUpdates.
			}

		} catch (UsatException ue) {
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Check out failed: " + ue.getMessage(), null);

			context.addMessage(null, facesMsg);
			responseString = "failure";
		} catch (Exception e) {
			e.printStackTrace();
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "An unexpected error occurred during checkout: "
					+ e.getMessage(), null);

			context.addMessage(null, facesMsg);
			responseString = "failure";
		} finally {
			if (changeCheckoutStatusInFinallyBlock) {
				// clear the flag that we are in this method
				sch.setCheckoutStarted(false);
			}
		}

		return responseString;
	}

	/**
	 * @managed-bean true
	 */
	protected ShoppingCartHandler getShoppingCartHandler() {
		if (shoppingCartHandler == null) {
			shoppingCartHandler = (ShoppingCartHandler) getManagedBean("shoppingCartHandler");
		}
		return shoppingCartHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setShoppingCartHandler(ShoppingCartHandler shoppingCartHandler) {
		this.shoppingCartHandler = shoppingCartHandler;
	}

	protected HtmlInputHidden getShowOverlayPopUp() {
		if (showOverlayPopUp == null) {
			showOverlayPopUp = (HtmlInputHidden) findComponentInRoot("showOverlayPopUp");
		}
		return showOverlayPopUp;
	}

	protected HtmlOutputFormat getFormat1() {
		if (format1 == null) {
			format1 = (HtmlOutputFormat) findComponentInRoot("format1");
		}
		return format1;
	}

	protected UIParameter getParam1() {
		if (param1 == null) {
			param1 = (UIParameter) findComponentInRoot("param1");
		}
		return param1;
	}

	protected HtmlPanelGrid getGridEZPAYOptionsGrid() {
		if (gridEZPAYOptionsGrid == null) {
			gridEZPAYOptionsGrid = (HtmlPanelGrid) findComponentInRoot("gridEZPAYOptionsGrid");
		}
		return gridEZPAYOptionsGrid;
	}

	protected HtmlOutputText getText10() {
		if (text10 == null) {
			text10 = (HtmlOutputText) findComponentInRoot("text10");
		}
		return text10;
	}

	protected HtmlSelectOneRadio getRadioRenewalOptions() {
		if (radioRenewalOptions == null) {
			radioRenewalOptions = (HtmlSelectOneRadio) findComponentInRoot("radioRenewalOptions");
		}
		return radioRenewalOptions;
	}

	protected UISelectItem getSelectItem15() {
		if (selectItem15 == null) {
			selectItem15 = (UISelectItem) findComponentInRoot("selectItem15");
		}
		return selectItem15;
	}

	protected UISelectItem getSelectItem17() {
		if (selectItem17 == null) {
			selectItem17 = (UISelectItem) findComponentInRoot("selectItem17");
		}
		return selectItem17;
	}

	protected HtmlPanelGrid getGridEZPAYRequiredGrid() {
		if (gridEZPAYRequiredGrid == null) {
			gridEZPAYRequiredGrid = (HtmlPanelGrid) findComponentInRoot("gridEZPAYRequiredGrid");
		}
		return gridEZPAYRequiredGrid;
	}

	protected HtmlOutputText getTextForcedEzPayInfoText() {
		if (textForcedEzPayInfoText == null) {
			textForcedEzPayInfoText = (HtmlOutputText) findComponentInRoot("textForcedEzPayInfoText");
		}
		return textForcedEzPayInfoText;
	}

	protected HtmlOutputFormat getTextEZPayHelpText() {
		if (textEZPayHelpText == null) {
			textEZPayHelpText = (HtmlOutputFormat) findComponentInRoot("textEZPayHelpText");
		}
		return textEZPayHelpText;
	}

	protected HtmlJspPanel getJspPanel1() {
		if (jspPanel1 == null) {
			jspPanel1 = (HtmlJspPanel) findComponentInRoot("jspPanel1");
		}
		return jspPanel1;
	}

	protected HtmlOutputText getTextPageJS2() {
		if (textPageJS2 == null) {
			textPageJS2 = (HtmlOutputText) findComponentInRoot("textPageJS2");
		}
		return textPageJS2;
	}

	protected HtmlOutputText getTextPageJS3() {
		if (textPageJS3 == null) {
			textPageJS3 = (HtmlOutputText) findComponentInRoot("textPageJS3");
		}
		return textPageJS3;
	}

	protected HtmlPanelGrid getGridTermsFormBoxBottomFacetGrid() {
		if (gridTermsFormBoxBottomFacetGrid == null) {
			gridTermsFormBoxBottomFacetGrid = (HtmlPanelGrid) findComponentInRoot("gridTermsFormBoxBottomFacetGrid");
		}
		return gridTermsFormBoxBottomFacetGrid;
	}

	protected HtmlOutputText getTextEZPayExplanationMessage() {
		if (textEZPayExplanationMessage == null) {
			textEZPayExplanationMessage = (HtmlOutputText) findComponentInRoot("textEZPayExplanationMessage");
		}
		return textEZPayExplanationMessage;
	}

	protected HtmlOutputText getTextEZPayLearnMore() {
		if (textEZPayLearnMore == null) {
			textEZPayLearnMore = (HtmlOutputText) findComponentInRoot("textEZPayLearnMore");
		}
		return textEZPayLearnMore;
	}

	protected HtmlOutputText getTextEZPayDisclaimer() {
		if (textEZPayDisclaimer == null) {
			textEZPayDisclaimer = (HtmlOutputText) findComponentInRoot("textEZPayDisclaimer");
		}
		return textEZPayDisclaimer;
	}

}