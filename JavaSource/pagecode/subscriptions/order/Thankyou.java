/**
 * 
 */
package pagecode.subscriptions.order;

import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlPanelGroup;

import pagecode.PageCodeBase;

import com.ibm.faces.component.html.HtmlGraphicImageEx;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.ibm.faces.component.html.HtmlPanelLayout;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.esub.handlers.ShoppingCartHandler;
import com.usatoday.esub.handlers.SubscriptionOfferHandler;
import com.usatoday.esub.ncs.handlers.CustomerHandler;
import com.usatoday.esub.trials.handlers.TrialCustomerHandler;

/**
 * @author swong
 * 
 */
public class Thankyou extends PageCodeBase {

	protected HtmlScriptCollector scriptCollector1;
	protected HtmlForm formMainForm;
	protected HtmlOutputText textMainTableHeader;
	protected HtmlPanelGrid gridThankYouInformation;
	protected HtmlJspPanel jspPanelEEThankYouTextPanel;
	protected HtmlOutputText textThankYouText2;
	protected HtmlOutputText textReadNowTextLink;
	protected HtmlOutputText textThankYouBullet2a;
	protected HtmlOutputText textCustServiceLink;
	protected HtmlOutputText textThankYouText3;
	protected HtmlOutputText textThankYouText4;
	protected HtmlOutputLinkEx linkExPrintPageLink;
	protected HtmlOutputText textPrintPageLinkText;
	protected HtmlPanelGrid gridAccountInformation;
	protected HtmlPanelGroup groupAccountInfoHeaderGroup;
	protected HtmlPanelLayout layoutOrderInfo1;
	protected HtmlOutputText textOrderInformationHeaderlabel;
	protected HtmlPanelGroup groupDeliveryInfoGroup;
	protected HtmlPanelLayout layoutOrderInfo2;
	protected HtmlOutputText textOrderInformationDeliveryHeaderlabel;
	protected HtmlOutputLinkEx linkExReadNowTextLink;
	protected HtmlOutputLinkEx linkExCustServiceLink;
	protected HtmlOutputText textThankYouBullet2b;
	protected HtmlGraphicImageEx imageExFillerImage;
	protected HtmlPanelGrid gridPrintButtonGrid;
	protected HtmlJspPanel jspPanelOrderDataPanel;
	protected HtmlOutputText textDateLabel;
	protected HtmlOutputText textDateOfToday;
	protected HtmlOutputText textSubStartDateLabel;
	protected HtmlOutputText textStartDateOfSub;
	protected HtmlOutputText textProductLabel;
	protected HtmlOutputText textProdName;
	protected HtmlOutputText textTermLabel;
	protected HtmlOutputText textSelectedTerm;
	protected HtmlPanelGrid gridAccountInformationDelivery;
	protected HtmlOutputText textDeliveryMethodLabel;
	protected HtmlOutputText textDeliveryMethodDes;
	protected HtmlOutputText textDeliveryNameLabel;
	protected HtmlOutputText textDeliveryName;
	protected HtmlOutputText textDeliveryAddr1Label;
	protected HtmlOutputText textDeliveryAddr1;
	protected HtmlOutputText textDeliveryAptSuiteLabel;
	protected HtmlOutputText textDeliveryAptSuite;
	protected HtmlOutputText textDeliveryAddr2Label;
	protected HtmlOutputText textDeliveryAddr2;
	protected HtmlOutputText textDeliveryAddrStateZipLabel;
	protected HtmlOutputText textDeliveryStateZip;
	protected ShoppingCartHandler shoppingCartHandler;
	protected TrialCustomerHandler trialCustomerHandler;
	protected CustomerHandler customerHandler;
	protected HtmlJspPanel jspPanelThankYouTextPanel;
	protected HtmlOutputText textThankYouTextL1;
	protected HtmlOutputText textEZPayLabel;
	protected HtmlOutputText textUnitPriceLabel;
	protected HtmlOutputText textUnitPrice;
	protected SubscriptionOfferHandler currentOfferHandler;
	protected HtmlOutputText RightColHTMLSpot1Text;
	protected HtmlMessages messages1;
	protected HtmlPanelGroup groupPaymentInfoGroup;
	protected HtmlPanelLayout layoutOrderInfo3;
	protected HtmlOutputText textOrderInformationPaymentHeaderlabel;
	protected HtmlOutputText textSubTotalAmountLabel;
	protected HtmlOutputText textSubTotalChargeAmount;
	protected HtmlOutputText textTaxAmountLabel;
	protected HtmlOutputText textChargeTaxAmount;
	protected HtmlOutputText textChargeAmountLabel;
	protected HtmlOutputText textChargeAmount;
	protected HtmlOutputText textPaymentMethodLabel;
	protected HtmlOutputText textPaymentMethod;
	protected HtmlOutputText textPaymentCardNumberLabel;
	protected HtmlOutputText textPaymentCardNumber;
	protected HtmlPanelGroup groupPaymentInfoBillMeGroup;
	protected HtmlPanelLayout layoutOrderInfo4;
	protected HtmlOutputText textOrderInformationPaymentBillMeHeaderlabel;
	protected HtmlOutputText textSubTotalAmountLabelBillMe;
	protected HtmlPanelGrid gridAccountInformationPaymentBillMe;
	protected HtmlPanelGrid billMeOrderDetails;
	protected HtmlOutputText textSubTotalChargeAmountBillMe;
	protected HtmlOutputText textTaxAmountLabelBillMe;
	protected HtmlOutputText textChargeTaxAmountBillMe;
	protected HtmlOutputText textChargeAmountLabelBillMe;
	protected HtmlOutputText textChargeAmountBillMe;
	protected HtmlOutputText textBeInfoText;
	protected HtmlPanelGroup groupBillingHeaderGroup;
	protected HtmlPanelLayout layoutOrderInfo5;
	protected HtmlOutputText textOrderInformationBillingHeaderlabel;
	protected HtmlPanelGrid gridAccountInformationBilling;
	protected HtmlOutputText textBillingEmailLabel;
	protected HtmlOutputText textBillingEmail;
	protected HtmlOutputText textBillingNameLabel;
	protected HtmlOutputText textBillingName;
	protected HtmlOutputText textBillingCompanyLabel;
	protected HtmlOutputText textBillingCompany;
	protected HtmlOutputText textBillingAptSuiteLabel;
	protected HtmlOutputText textBillingAptSuite;
	protected HtmlOutputText textBillingAddr2Label;
	protected HtmlOutputText textBillingAddr2;
	protected HtmlOutputText textBillingAddrStateZipLabel;
	protected HtmlOutputText textBillingStateZip;
	protected HtmlPanelGrid gridAccountInformationPayment;
	protected HtmlOutputText textDeliveryCompanyNameLabel;
	protected HtmlOutputText textDeliveryCompanyName;
	protected HtmlOutputText textDeliveryPhoneLabel;
	protected HtmlOutputText textDeliveryPhone;
	protected HtmlOutputText textBillingPhoneLabel;
	protected HtmlOutputText textBillingPhone;
	protected HtmlPanelGrid gridAccountEZPayInformation;
	protected HtmlPanelGrid gridRightPanelGrid;
	protected HtmlJspPanel jspPanelRightPanelTop;
	protected HtmlOutputLinkEx linkExReadNowLink;
	protected HtmlGraphicImageEx imageExReadNowButtonImage;
	protected HtmlPanelGrid gridRightColCustomSpot1;
	protected HtmlGraphicImageEx imageExRightColImageEZPay;
	protected HtmlPanelGrid rightColumnCustomPanelGrid;
	protected HtmlGraphicImageEx imageExRightColImage1NoLink;
	protected HtmlPanelGrid gridRightColCustomSpot3;
	protected HtmlPanelGrid gridRightColEZPAY;
	protected HtmlOutputText textDeliveryWorkPhoneLabel;
	protected HtmlOutputText textDeliveryWorkPhone;
	protected HtmlMessages jspPanelEE;
	protected HtmlScriptCollector scriptCollectorTemplateLogoutCollector;
	protected HtmlForm formLogoutTemplateAction;
	protected HtmlOutputLinkEx linkExLogOutLinkTemplate1;
	protected HtmlOutputText textLogoutLinkTemplateLabel1;
	protected HtmlScriptCollector scriptCollectorTemplateCollectorLeftNav;
	protected HtmlPanelGrid gridNavigationTopGrid;
	protected HtmlPanelGrid gridLeftNavGroup1Grid;
	protected HtmlOutputLinkEx linkExNavGroup1SubscriberServices;
	protected HtmlOutputText textNavGroup1LinkLabel1;
	protected HtmlOutputText textNavGroup1LinkLabelDotCom;
	protected HtmlOutputText textNavGroup1LinkLabelFAQ;
	protected HtmlOutputLinkEx linkExNavArea2Link0;
	protected HtmlOutputText textNavArea2Link0;
	protected HtmlOutputText textNavArea2Link1;
	protected HtmlOutputText textNavArea2Link2;
	protected HtmlOutputText textNavArea2Link3;
	protected HtmlOutputText textNavArea2Link4;
	protected HtmlOutputText textNavArea2Link5;
	protected HtmlScriptCollector scriptCollectorBottomNavCollector;
	protected HtmlJspPanel jspPanelBottomNavCustomPanel;
	protected HtmlOutputLinkEx linkExTopNavLogoLink;
	protected HtmlGraphicImageEx imageExPartnerLogo;
	protected HtmlPanelGrid gridNavigationOuterGrid;
	protected HtmlOutputLinkEx linkExNavGroup2DotCom;
	protected HtmlOutputLinkEx linkExNavGroupFAQ;
	protected HtmlPanelGrid gridLeftNavGroup2Grid;
	protected HtmlOutputLinkEx linkExNavArea2Link1;
	protected HtmlOutputLinkEx linkExNavArea2Link2;
	protected HtmlOutputLinkEx linkExNavArea2Link3;
	protected HtmlOutputLinkEx linkExNavArea2Link4;
	protected HtmlOutputLinkEx linkExNavArea2Link5;
	protected HtmlPanelGrid gridNavigationBottomGrid;
	protected HtmlGraphicImageEx imageExPartnerLogoNoLink;
	protected HtmlOutputText textEZPayStatus;
	protected HtmlOutputText textDeliveryEmailLabel;
	protected HtmlOutputText textDeliveryEmail;
	protected HtmlOutputText textBillingAddr1Label;
	protected HtmlOutputText textBillingAddr1;
	protected HtmlGraphicImageEx imageExRightColImage2;
	protected HtmlOutputLinkEx linkExRightColImageSpot2Link;
	protected HtmlOutputLinkEx linkExRightColImageSpot1Link;
	protected HtmlGraphicImageEx imageExRightColImage1;
	protected HtmlOutputLinkEx linkExLeftColImageSpot1Link;
	protected HtmlGraphicImageEx imageExLeftColImage1;
	protected HtmlPanelGrid leftColImageSpot1;
	protected HtmlGraphicImageEx imageExLeftColImage1NoLink;
	protected HtmlJspPanel jspPanelThankYouGiftPanel;
	protected HtmlOutputText textThankYouGift;
	protected HtmlOutputLinkEx linkExGiftLink;
	protected HtmlOutputText textGiftLink;
	protected HtmlPanelGrid gridRightColCustomSpot2;
	protected HtmlOutputText textThankYouCommJunc1;
	protected HtmlOutputText textThankYouJS2;
	protected HtmlOutputText textThankYouJS3;
	protected HtmlOutputText textThankYouJS4;
	protected HtmlOutputText textThankYouJS5;
	protected HtmlOutputText textThankYouJS6;
	protected HtmlOutputText textThankYouJS7;
	protected HtmlOutputText textThankYouJS8;
	protected HtmlOutputText textThankYouJS9;
	protected HtmlPanelGrid gridPremiumPromoGrid;
	protected HtmlOutputText textPremiumPromotionLabel;
	protected HtmlOutputText textPremiumPromotionDescriptionText;
	protected HtmlGraphicImageEx imageExRightColImage2NoLink;
	protected HtmlOutputText RightColHTMLSpot2Text;
	protected HtmlPanelGrid gridRightColCustomSpot4;
	protected HtmlOutputLinkEx linkExOpenDesktopVersion;
	protected HtmlOutputText text1111;
	protected HtmlOutputText text2;
	protected HtmlOutputLinkEx linkExOpenMobileVersion;
	protected HtmlOutputText text1;
	protected HtmlOutputText textThankYouJS1;
	protected HtmlOutputText textThankYouJS12;
	protected HtmlOutputText textThankYouJS10;
	protected HtmlOutputText textEZPayInfoText;
	protected HtmlOutputText textThankYouJS13;
	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlForm getFormMainForm() {
		if (formMainForm == null) {
			formMainForm = (HtmlForm) findComponentInRoot("formMainForm");
		}
		return formMainForm;
	}

	protected HtmlOutputText getTextMainTableHeader() {
		if (textMainTableHeader == null) {
			textMainTableHeader = (HtmlOutputText) findComponentInRoot("textMainTableHeader");
		}
		return textMainTableHeader;
	}

	protected HtmlPanelGrid getGridThankYouInformation() {
		if (gridThankYouInformation == null) {
			gridThankYouInformation = (HtmlPanelGrid) findComponentInRoot("gridThankYouInformation");
		}
		return gridThankYouInformation;
	}

	protected HtmlJspPanel getJspPanelEEThankYouTextPanel() {
		if (jspPanelEEThankYouTextPanel == null) {
			jspPanelEEThankYouTextPanel = (HtmlJspPanel) findComponentInRoot("jspPanelEEThankYouTextPanel");
		}
		return jspPanelEEThankYouTextPanel;
	}

	protected HtmlOutputText getTextThankYouText2() {
		if (textThankYouText2 == null) {
			textThankYouText2 = (HtmlOutputText) findComponentInRoot("textThankYouText2");
		}
		return textThankYouText2;
	}

	protected HtmlOutputText getTextReadNowTextLink() {
		if (textReadNowTextLink == null) {
			textReadNowTextLink = (HtmlOutputText) findComponentInRoot("textReadNowTextLink");
		}
		return textReadNowTextLink;
	}

	protected HtmlOutputText getTextThankYouBullet2a() {
		if (textThankYouBullet2a == null) {
			textThankYouBullet2a = (HtmlOutputText) findComponentInRoot("textThankYouBullet2a");
		}
		return textThankYouBullet2a;
	}

	protected HtmlOutputText getTextCustServiceLink() {
		if (textCustServiceLink == null) {
			textCustServiceLink = (HtmlOutputText) findComponentInRoot("textCustServiceLink");
		}
		return textCustServiceLink;
	}

	protected HtmlOutputText getTextThankYouText3() {
		if (textThankYouText3 == null) {
			textThankYouText3 = (HtmlOutputText) findComponentInRoot("textThankYouText3");
		}
		return textThankYouText3;
	}

	protected HtmlOutputText getTextThankYouText4() {
		if (textThankYouText4 == null) {
			textThankYouText4 = (HtmlOutputText) findComponentInRoot("textThankYouText4");
		}
		return textThankYouText4;
	}

	protected HtmlOutputLinkEx getLinkExPrintPageLink() {
		if (linkExPrintPageLink == null) {
			linkExPrintPageLink = (HtmlOutputLinkEx) findComponentInRoot("linkExPrintPageLink");
		}
		return linkExPrintPageLink;
	}

	protected HtmlOutputText getTextPrintPageLinkText() {
		if (textPrintPageLinkText == null) {
			textPrintPageLinkText = (HtmlOutputText) findComponentInRoot("textPrintPageLinkText");
		}
		return textPrintPageLinkText;
	}

	protected HtmlPanelGrid getGridAccountInformation() {
		if (gridAccountInformation == null) {
			gridAccountInformation = (HtmlPanelGrid) findComponentInRoot("gridAccountInformation");
		}
		return gridAccountInformation;
	}

	protected HtmlPanelGroup getGroupAccountInfoHeaderGroup() {
		if (groupAccountInfoHeaderGroup == null) {
			groupAccountInfoHeaderGroup = (HtmlPanelGroup) findComponentInRoot("groupAccountInfoHeaderGroup");
		}
		return groupAccountInfoHeaderGroup;
	}

	protected HtmlPanelLayout getLayoutOrderInfo1() {
		if (layoutOrderInfo1 == null) {
			layoutOrderInfo1 = (HtmlPanelLayout) findComponentInRoot("layoutOrderInfo1");
		}
		return layoutOrderInfo1;
	}

	protected HtmlOutputText getTextOrderInformationHeaderlabel() {
		if (textOrderInformationHeaderlabel == null) {
			textOrderInformationHeaderlabel = (HtmlOutputText) findComponentInRoot("textOrderInformationHeaderlabel");
		}
		return textOrderInformationHeaderlabel;
	}

	protected HtmlPanelGroup getGroupDeliveryInfoGroup() {
		if (groupDeliveryInfoGroup == null) {
			groupDeliveryInfoGroup = (HtmlPanelGroup) findComponentInRoot("groupDeliveryInfoGroup");
		}
		return groupDeliveryInfoGroup;
	}

	protected HtmlPanelLayout getLayoutOrderInfo2() {
		if (layoutOrderInfo2 == null) {
			layoutOrderInfo2 = (HtmlPanelLayout) findComponentInRoot("layoutOrderInfo2");
		}
		return layoutOrderInfo2;
	}

	protected HtmlOutputText getTextOrderInformationDeliveryHeaderlabel() {
		if (textOrderInformationDeliveryHeaderlabel == null) {
			textOrderInformationDeliveryHeaderlabel = (HtmlOutputText) findComponentInRoot("textOrderInformationDeliveryHeaderlabel");
		}
		return textOrderInformationDeliveryHeaderlabel;
	}

	protected HtmlOutputLinkEx getLinkExReadNowTextLink() {
		if (linkExReadNowTextLink == null) {
			linkExReadNowTextLink = (HtmlOutputLinkEx) findComponentInRoot("linkExReadNowTextLink");
		}
		return linkExReadNowTextLink;
	}

	protected HtmlOutputLinkEx getLinkExCustServiceLink() {
		if (linkExCustServiceLink == null) {
			linkExCustServiceLink = (HtmlOutputLinkEx) findComponentInRoot("linkExCustServiceLink");
		}
		return linkExCustServiceLink;
	}

	protected HtmlOutputText getTextThankYouBullet2b() {
		if (textThankYouBullet2b == null) {
			textThankYouBullet2b = (HtmlOutputText) findComponentInRoot("textThankYouBullet2b");
		}
		return textThankYouBullet2b;
	}

	protected HtmlGraphicImageEx getImageExFillerImage() {
		if (imageExFillerImage == null) {
			imageExFillerImage = (HtmlGraphicImageEx) findComponentInRoot("imageExFillerImage");
		}
		return imageExFillerImage;
	}

	protected HtmlPanelGrid getGridPrintButtonGrid() {
		if (gridPrintButtonGrid == null) {
			gridPrintButtonGrid = (HtmlPanelGrid) findComponentInRoot("gridPrintButtonGrid");
		}
		return gridPrintButtonGrid;
	}

	protected HtmlJspPanel getJspPanelOrderDataPanel() {
		if (jspPanelOrderDataPanel == null) {
			jspPanelOrderDataPanel = (HtmlJspPanel) findComponentInRoot("jspPanelOrderDataPanel");
		}
		return jspPanelOrderDataPanel;
	}

	protected HtmlOutputText getTextDateLabel() {
		if (textDateLabel == null) {
			textDateLabel = (HtmlOutputText) findComponentInRoot("textDateLabel");
		}
		return textDateLabel;
	}

	protected HtmlOutputText getTextDateOfToday() {
		if (textDateOfToday == null) {
			textDateOfToday = (HtmlOutputText) findComponentInRoot("textDateOfToday");
		}
		return textDateOfToday;
	}

	protected HtmlOutputText getTextSubStartDateLabel() {
		if (textSubStartDateLabel == null) {
			textSubStartDateLabel = (HtmlOutputText) findComponentInRoot("textSubStartDateLabel");
		}
		return textSubStartDateLabel;
	}

	protected HtmlOutputText getTextStartDateOfSub() {
		if (textStartDateOfSub == null) {
			textStartDateOfSub = (HtmlOutputText) findComponentInRoot("textStartDateOfSub");
		}
		return textStartDateOfSub;
	}

	protected HtmlOutputText getTextProductLabel() {
		if (textProductLabel == null) {
			textProductLabel = (HtmlOutputText) findComponentInRoot("textProductLabel");
		}
		return textProductLabel;
	}

	protected HtmlOutputText getTextProdName() {
		if (textProdName == null) {
			textProdName = (HtmlOutputText) findComponentInRoot("textProdName");
		}
		return textProdName;
	}

	protected HtmlOutputText getTextTermLabel() {
		if (textTermLabel == null) {
			textTermLabel = (HtmlOutputText) findComponentInRoot("textTermLabel");
		}
		return textTermLabel;
	}

	protected HtmlOutputText getTextSelectedTerm() {
		if (textSelectedTerm == null) {
			textSelectedTerm = (HtmlOutputText) findComponentInRoot("textSelectedTerm");
		}
		return textSelectedTerm;
	}

	protected HtmlPanelGrid getGridAccountInformationDelivery() {
		if (gridAccountInformationDelivery == null) {
			gridAccountInformationDelivery = (HtmlPanelGrid) findComponentInRoot("gridAccountInformationDelivery");
		}
		return gridAccountInformationDelivery;
	}

	protected HtmlOutputText getTextDeliveryMethodLabel() {
		if (textDeliveryMethodLabel == null) {
			textDeliveryMethodLabel = (HtmlOutputText) findComponentInRoot("textDeliveryMethodLabel");
		}
		return textDeliveryMethodLabel;
	}

	protected HtmlOutputText getTextDeliveryMethodDes() {
		if (textDeliveryMethodDes == null) {
			textDeliveryMethodDes = (HtmlOutputText) findComponentInRoot("textDeliveryMethodDes");
		}
		return textDeliveryMethodDes;
	}

	protected HtmlOutputText getTextDeliveryNameLabel() {
		if (textDeliveryNameLabel == null) {
			textDeliveryNameLabel = (HtmlOutputText) findComponentInRoot("textDeliveryNameLabel");
		}
		return textDeliveryNameLabel;
	}

	protected HtmlOutputText getTextDeliveryName() {
		if (textDeliveryName == null) {
			textDeliveryName = (HtmlOutputText) findComponentInRoot("textDeliveryName");
		}
		return textDeliveryName;
	}

	protected HtmlOutputText getTextDeliveryAddr1Label() {
		if (textDeliveryAddr1Label == null) {
			textDeliveryAddr1Label = (HtmlOutputText) findComponentInRoot("textDeliveryAddr1Label");
		}
		return textDeliveryAddr1Label;
	}

	protected HtmlOutputText getTextDeliveryAddr1() {
		if (textDeliveryAddr1 == null) {
			textDeliveryAddr1 = (HtmlOutputText) findComponentInRoot("textDeliveryAddr1");
		}
		return textDeliveryAddr1;
	}

	protected HtmlOutputText getTextDeliveryAptSuiteLabel() {
		if (textDeliveryAptSuiteLabel == null) {
			textDeliveryAptSuiteLabel = (HtmlOutputText) findComponentInRoot("textDeliveryAptSuiteLabel");
		}
		return textDeliveryAptSuiteLabel;
	}

	protected HtmlOutputText getTextDeliveryAptSuite() {
		if (textDeliveryAptSuite == null) {
			textDeliveryAptSuite = (HtmlOutputText) findComponentInRoot("textDeliveryAptSuite");
		}
		return textDeliveryAptSuite;
	}

	protected HtmlOutputText getTextDeliveryAddr2Label() {
		if (textDeliveryAddr2Label == null) {
			textDeliveryAddr2Label = (HtmlOutputText) findComponentInRoot("textDeliveryAddr2Label");
		}
		return textDeliveryAddr2Label;
	}

	protected HtmlOutputText getTextDeliveryAddr2() {
		if (textDeliveryAddr2 == null) {
			textDeliveryAddr2 = (HtmlOutputText) findComponentInRoot("textDeliveryAddr2");
		}
		return textDeliveryAddr2;
	}

	protected HtmlOutputText getTextDeliveryAddrStateZipLabel() {
		if (textDeliveryAddrStateZipLabel == null) {
			textDeliveryAddrStateZipLabel = (HtmlOutputText) findComponentInRoot("textDeliveryAddrStateZipLabel");
		}
		return textDeliveryAddrStateZipLabel;
	}

	protected HtmlOutputText getTextDeliveryStateZip() {
		if (textDeliveryStateZip == null) {
			textDeliveryStateZip = (HtmlOutputText) findComponentInRoot("textDeliveryStateZip");
		}
		return textDeliveryStateZip;
	}

	/**
	 * @managed-bean true
	 */
	protected ShoppingCartHandler getShoppingCartHandler() {
		if (shoppingCartHandler == null) {
			shoppingCartHandler = (ShoppingCartHandler) getManagedBean("shoppingCartHandler");
		}
		return shoppingCartHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setShoppingCartHandler(ShoppingCartHandler shoppingCartHandler) {
		this.shoppingCartHandler = shoppingCartHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected TrialCustomerHandler getTrialCustomerHandler() {
		if (trialCustomerHandler == null) {
			trialCustomerHandler = (TrialCustomerHandler) getManagedBean("trialCustomerHandler");
		}
		return trialCustomerHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setTrialCustomerHandler(TrialCustomerHandler trialCustomerHandler) {
		this.trialCustomerHandler = trialCustomerHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected CustomerHandler getCustomerHandler() {
		if (customerHandler == null) {
			customerHandler = (CustomerHandler) getManagedBean("customerHandler");
		}
		return customerHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setCustomerHandler(CustomerHandler customerHandler) {
		this.customerHandler = customerHandler;
	}

	protected HtmlJspPanel getJspPanelThankYouTextPanel() {
		if (jspPanelThankYouTextPanel == null) {
			jspPanelThankYouTextPanel = (HtmlJspPanel) findComponentInRoot("jspPanelThankYouTextPanel");
		}
		return jspPanelThankYouTextPanel;
	}

	protected HtmlOutputText getTextThankYouTextL1() {
		if (textThankYouTextL1 == null) {
			textThankYouTextL1 = (HtmlOutputText) findComponentInRoot("textThankYouTextL1");
		}
		return textThankYouTextL1;
	}

	protected HtmlOutputText getTextEZPayLabel() {
		if (textEZPayLabel == null) {
			textEZPayLabel = (HtmlOutputText) findComponentInRoot("textEZPayLabel");
		}
		return textEZPayLabel;
	}

	protected HtmlOutputText getTextUnitPriceLabel() {
		if (textUnitPriceLabel == null) {
			textUnitPriceLabel = (HtmlOutputText) findComponentInRoot("textUnitPriceLabel");
		}
		return textUnitPriceLabel;
	}

	protected HtmlOutputText getTextUnitPrice() {
		if (textUnitPrice == null) {
			textUnitPrice = (HtmlOutputText) findComponentInRoot("textUnitPrice");
		}
		return textUnitPrice;
	}

	/**
	 * @managed-bean true
	 */
	protected SubscriptionOfferHandler getCurrentOfferHandler() {
		if (currentOfferHandler == null) {
			currentOfferHandler = (SubscriptionOfferHandler) getManagedBean("currentOfferHandler");
		}
		return currentOfferHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setCurrentOfferHandler(SubscriptionOfferHandler currentOfferHandler) {
		this.currentOfferHandler = currentOfferHandler;
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

	protected HtmlPanelGroup getGroupPaymentInfoGroup() {
		if (groupPaymentInfoGroup == null) {
			groupPaymentInfoGroup = (HtmlPanelGroup) findComponentInRoot("groupPaymentInfoGroup");
		}
		return groupPaymentInfoGroup;
	}

	protected HtmlPanelLayout getLayoutOrderInfo3() {
		if (layoutOrderInfo3 == null) {
			layoutOrderInfo3 = (HtmlPanelLayout) findComponentInRoot("layoutOrderInfo3");
		}
		return layoutOrderInfo3;
	}

	protected HtmlOutputText getTextOrderInformationPaymentHeaderlabel() {
		if (textOrderInformationPaymentHeaderlabel == null) {
			textOrderInformationPaymentHeaderlabel = (HtmlOutputText) findComponentInRoot("textOrderInformationPaymentHeaderlabel");
		}
		return textOrderInformationPaymentHeaderlabel;
	}

	protected HtmlOutputText getTextSubTotalAmountLabel() {
		if (textSubTotalAmountLabel == null) {
			textSubTotalAmountLabel = (HtmlOutputText) findComponentInRoot("textSubTotalAmountLabel");
		}
		return textSubTotalAmountLabel;
	}

	protected HtmlOutputText getTextSubTotalChargeAmount() {
		if (textSubTotalChargeAmount == null) {
			textSubTotalChargeAmount = (HtmlOutputText) findComponentInRoot("textSubTotalChargeAmount");
		}
		return textSubTotalChargeAmount;
	}

	protected HtmlOutputText getTextTaxAmountLabel() {
		if (textTaxAmountLabel == null) {
			textTaxAmountLabel = (HtmlOutputText) findComponentInRoot("textTaxAmountLabel");
		}
		return textTaxAmountLabel;
	}

	protected HtmlOutputText getTextChargeTaxAmount() {
		if (textChargeTaxAmount == null) {
			textChargeTaxAmount = (HtmlOutputText) findComponentInRoot("textChargeTaxAmount");
		}
		return textChargeTaxAmount;
	}

	protected HtmlOutputText getTextChargeAmountLabel() {
		if (textChargeAmountLabel == null) {
			textChargeAmountLabel = (HtmlOutputText) findComponentInRoot("textChargeAmountLabel");
		}
		return textChargeAmountLabel;
	}

	protected HtmlOutputText getTextChargeAmount() {
		if (textChargeAmount == null) {
			textChargeAmount = (HtmlOutputText) findComponentInRoot("textChargeAmount");
		}
		return textChargeAmount;
	}

	protected HtmlOutputText getTextPaymentMethodLabel() {
		if (textPaymentMethodLabel == null) {
			textPaymentMethodLabel = (HtmlOutputText) findComponentInRoot("textPaymentMethodLabel");
		}
		return textPaymentMethodLabel;
	}

	protected HtmlOutputText getTextPaymentMethod() {
		if (textPaymentMethod == null) {
			textPaymentMethod = (HtmlOutputText) findComponentInRoot("textPaymentMethod");
		}
		return textPaymentMethod;
	}

	protected HtmlOutputText getTextPaymentCardNumberLabel() {
		if (textPaymentCardNumberLabel == null) {
			textPaymentCardNumberLabel = (HtmlOutputText) findComponentInRoot("textPaymentCardNumberLabel");
		}
		return textPaymentCardNumberLabel;
	}

	protected HtmlOutputText getTextPaymentCardNumber() {
		if (textPaymentCardNumber == null) {
			textPaymentCardNumber = (HtmlOutputText) findComponentInRoot("textPaymentCardNumber");
		}
		return textPaymentCardNumber;
	}

	protected HtmlPanelGroup getGroupPaymentInfoBillMeGroup() {
		if (groupPaymentInfoBillMeGroup == null) {
			groupPaymentInfoBillMeGroup = (HtmlPanelGroup) findComponentInRoot("groupPaymentInfoBillMeGroup");
		}
		return groupPaymentInfoBillMeGroup;
	}

	protected HtmlPanelLayout getLayoutOrderInfo4() {
		if (layoutOrderInfo4 == null) {
			layoutOrderInfo4 = (HtmlPanelLayout) findComponentInRoot("layoutOrderInfo4");
		}
		return layoutOrderInfo4;
	}

	protected HtmlOutputText getTextOrderInformationPaymentBillMeHeaderlabel() {
		if (textOrderInformationPaymentBillMeHeaderlabel == null) {
			textOrderInformationPaymentBillMeHeaderlabel = (HtmlOutputText) findComponentInRoot("textOrderInformationPaymentBillMeHeaderlabel");
		}
		return textOrderInformationPaymentBillMeHeaderlabel;
	}

	protected HtmlOutputText getTextSubTotalAmountLabelBillMe() {
		if (textSubTotalAmountLabelBillMe == null) {
			textSubTotalAmountLabelBillMe = (HtmlOutputText) findComponentInRoot("textSubTotalAmountLabelBillMe");
		}
		return textSubTotalAmountLabelBillMe;
	}

	protected HtmlPanelGrid getGridAccountInformationPaymentBillMe() {
		if (gridAccountInformationPaymentBillMe == null) {
			gridAccountInformationPaymentBillMe = (HtmlPanelGrid) findComponentInRoot("gridAccountInformationPaymentBillMe");
		}
		return gridAccountInformationPaymentBillMe;
	}

	protected HtmlPanelGrid getBillMeOrderDetails() {
		if (billMeOrderDetails == null) {
			billMeOrderDetails = (HtmlPanelGrid) findComponentInRoot("billMeOrderDetails");
		}
		return billMeOrderDetails;
	}

	protected HtmlOutputText getTextSubTotalChargeAmountBillMe() {
		if (textSubTotalChargeAmountBillMe == null) {
			textSubTotalChargeAmountBillMe = (HtmlOutputText) findComponentInRoot("textSubTotalChargeAmountBillMe");
		}
		return textSubTotalChargeAmountBillMe;
	}

	protected HtmlOutputText getTextTaxAmountLabelBillMe() {
		if (textTaxAmountLabelBillMe == null) {
			textTaxAmountLabelBillMe = (HtmlOutputText) findComponentInRoot("textTaxAmountLabelBillMe");
		}
		return textTaxAmountLabelBillMe;
	}

	protected HtmlOutputText getTextChargeTaxAmountBillMe() {
		if (textChargeTaxAmountBillMe == null) {
			textChargeTaxAmountBillMe = (HtmlOutputText) findComponentInRoot("textChargeTaxAmountBillMe");
		}
		return textChargeTaxAmountBillMe;
	}

	protected HtmlOutputText getTextChargeAmountLabelBillMe() {
		if (textChargeAmountLabelBillMe == null) {
			textChargeAmountLabelBillMe = (HtmlOutputText) findComponentInRoot("textChargeAmountLabelBillMe");
		}
		return textChargeAmountLabelBillMe;
	}

	protected HtmlOutputText getTextChargeAmountBillMe() {
		if (textChargeAmountBillMe == null) {
			textChargeAmountBillMe = (HtmlOutputText) findComponentInRoot("textChargeAmountBillMe");
		}
		return textChargeAmountBillMe;
	}

	protected HtmlOutputText getTextBeInfoText() {
		if (textBeInfoText == null) {
			textBeInfoText = (HtmlOutputText) findComponentInRoot("textBeInfoText");
		}
		return textBeInfoText;
	}

	protected HtmlPanelGroup getGroupBillingHeaderGroup() {
		if (groupBillingHeaderGroup == null) {
			groupBillingHeaderGroup = (HtmlPanelGroup) findComponentInRoot("groupBillingHeaderGroup");
		}
		return groupBillingHeaderGroup;
	}

	protected HtmlPanelLayout getLayoutOrderInfo5() {
		if (layoutOrderInfo5 == null) {
			layoutOrderInfo5 = (HtmlPanelLayout) findComponentInRoot("layoutOrderInfo5");
		}
		return layoutOrderInfo5;
	}

	protected HtmlOutputText getTextOrderInformationBillingHeaderlabel() {
		if (textOrderInformationBillingHeaderlabel == null) {
			textOrderInformationBillingHeaderlabel = (HtmlOutputText) findComponentInRoot("textOrderInformationBillingHeaderlabel");
		}
		return textOrderInformationBillingHeaderlabel;
	}

	protected HtmlPanelGrid getGridAccountInformationBilling() {
		if (gridAccountInformationBilling == null) {
			gridAccountInformationBilling = (HtmlPanelGrid) findComponentInRoot("gridAccountInformationBilling");
		}
		return gridAccountInformationBilling;
	}

	protected HtmlOutputText getTextBillingEmailLabel() {
		if (textBillingEmailLabel == null) {
			textBillingEmailLabel = (HtmlOutputText) findComponentInRoot("textBillingEmailLabel");
		}
		return textBillingEmailLabel;
	}

	protected HtmlOutputText getTextBillingEmail() {
		if (textBillingEmail == null) {
			textBillingEmail = (HtmlOutputText) findComponentInRoot("textBillingEmail");
		}
		return textBillingEmail;
	}

	protected HtmlOutputText getTextBillingNameLabel() {
		if (textBillingNameLabel == null) {
			textBillingNameLabel = (HtmlOutputText) findComponentInRoot("textBillingNameLabel");
		}
		return textBillingNameLabel;
	}

	protected HtmlOutputText getTextBillingName() {
		if (textBillingName == null) {
			textBillingName = (HtmlOutputText) findComponentInRoot("textBillingName");
		}
		return textBillingName;
	}

	protected HtmlOutputText getTextBillingCompanyLabel() {
		if (textBillingCompanyLabel == null) {
			textBillingCompanyLabel = (HtmlOutputText) findComponentInRoot("textBillingCompanyLabel");
		}
		return textBillingCompanyLabel;
	}

	protected HtmlOutputText getTextBillingCompany() {
		if (textBillingCompany == null) {
			textBillingCompany = (HtmlOutputText) findComponentInRoot("textBillingCompany");
		}
		return textBillingCompany;
	}

	protected HtmlOutputText getTextBillingAptSuiteLabel() {
		if (textBillingAptSuiteLabel == null) {
			textBillingAptSuiteLabel = (HtmlOutputText) findComponentInRoot("textBillingAptSuiteLabel");
		}
		return textBillingAptSuiteLabel;
	}

	protected HtmlOutputText getTextBillingAptSuite() {
		if (textBillingAptSuite == null) {
			textBillingAptSuite = (HtmlOutputText) findComponentInRoot("textBillingAptSuite");
		}
		return textBillingAptSuite;
	}

	protected HtmlOutputText getTextBillingAddr2Label() {
		if (textBillingAddr2Label == null) {
			textBillingAddr2Label = (HtmlOutputText) findComponentInRoot("textBillingAddr2Label");
		}
		return textBillingAddr2Label;
	}

	protected HtmlOutputText getTextBillingAddr2() {
		if (textBillingAddr2 == null) {
			textBillingAddr2 = (HtmlOutputText) findComponentInRoot("textBillingAddr2");
		}
		return textBillingAddr2;
	}

	protected HtmlOutputText getTextBillingAddrStateZipLabel() {
		if (textBillingAddrStateZipLabel == null) {
			textBillingAddrStateZipLabel = (HtmlOutputText) findComponentInRoot("textBillingAddrStateZipLabel");
		}
		return textBillingAddrStateZipLabel;
	}

	protected HtmlOutputText getTextBillingStateZip() {
		if (textBillingStateZip == null) {
			textBillingStateZip = (HtmlOutputText) findComponentInRoot("textBillingStateZip");
		}
		return textBillingStateZip;
	}

	protected HtmlPanelGrid getGridAccountInformationPayment() {
		if (gridAccountInformationPayment == null) {
			gridAccountInformationPayment = (HtmlPanelGrid) findComponentInRoot("gridAccountInformationPayment");
		}
		return gridAccountInformationPayment;
	}

	protected HtmlOutputText getTextDeliveryCompanyNameLabel() {
		if (textDeliveryCompanyNameLabel == null) {
			textDeliveryCompanyNameLabel = (HtmlOutputText) findComponentInRoot("textDeliveryCompanyNameLabel");
		}
		return textDeliveryCompanyNameLabel;
	}

	protected HtmlOutputText getTextDeliveryCompanyName() {
		if (textDeliveryCompanyName == null) {
			textDeliveryCompanyName = (HtmlOutputText) findComponentInRoot("textDeliveryCompanyName");
		}
		return textDeliveryCompanyName;
	}

	protected HtmlOutputText getTextDeliveryPhoneLabel() {
		if (textDeliveryPhoneLabel == null) {
			textDeliveryPhoneLabel = (HtmlOutputText) findComponentInRoot("textDeliveryPhoneLabel");
		}
		return textDeliveryPhoneLabel;
	}

	protected HtmlOutputText getTextDeliveryPhone() {
		if (textDeliveryPhone == null) {
			textDeliveryPhone = (HtmlOutputText) findComponentInRoot("textDeliveryPhone");
		}
		return textDeliveryPhone;
	}

	protected HtmlOutputText getTextBillingPhoneLabel() {
		if (textBillingPhoneLabel == null) {
			textBillingPhoneLabel = (HtmlOutputText) findComponentInRoot("textBillingPhoneLabel");
		}
		return textBillingPhoneLabel;
	}

	protected HtmlOutputText getTextBillingPhone() {
		if (textBillingPhone == null) {
			textBillingPhone = (HtmlOutputText) findComponentInRoot("textBillingPhone");
		}
		return textBillingPhone;
	}

	protected HtmlPanelGrid getGridAccountEZPayInformation() {
		if (gridAccountEZPayInformation == null) {
			gridAccountEZPayInformation = (HtmlPanelGrid) findComponentInRoot("gridAccountEZPayInformation");
		}
		return gridAccountEZPayInformation;
	}

	protected HtmlPanelGrid getGridRightPanelGrid() {
		if (gridRightPanelGrid == null) {
			gridRightPanelGrid = (HtmlPanelGrid) findComponentInRoot("gridRightPanelGrid");
		}
		return gridRightPanelGrid;
	}

	protected HtmlJspPanel getJspPanelRightPanelTop() {
		if (jspPanelRightPanelTop == null) {
			jspPanelRightPanelTop = (HtmlJspPanel) findComponentInRoot("jspPanelRightPanelTop");
		}
		return jspPanelRightPanelTop;
	}

	protected HtmlOutputLinkEx getLinkExReadNowLink() {
		if (linkExReadNowLink == null) {
			linkExReadNowLink = (HtmlOutputLinkEx) findComponentInRoot("linkExReadNowLink");
		}
		return linkExReadNowLink;
	}

	protected HtmlGraphicImageEx getImageExReadNowButtonImage() {
		if (imageExReadNowButtonImage == null) {
			imageExReadNowButtonImage = (HtmlGraphicImageEx) findComponentInRoot("imageExReadNowButtonImage");
		}
		return imageExReadNowButtonImage;
	}

	protected HtmlPanelGrid getGridRightColCustomSpot1() {
		if (gridRightColCustomSpot1 == null) {
			gridRightColCustomSpot1 = (HtmlPanelGrid) findComponentInRoot("gridRightColCustomSpot1");
		}
		return gridRightColCustomSpot1;
	}

	protected HtmlGraphicImageEx getImageExRightColImageEZPay() {
		if (imageExRightColImageEZPay == null) {
			imageExRightColImageEZPay = (HtmlGraphicImageEx) findComponentInRoot("imageExRightColImageEZPay");
		}
		return imageExRightColImageEZPay;
	}

	protected HtmlPanelGrid getRightColumnCustomPanelGrid() {
		if (rightColumnCustomPanelGrid == null) {
			rightColumnCustomPanelGrid = (HtmlPanelGrid) findComponentInRoot("rightColumnCustomPanelGrid");
		}
		return rightColumnCustomPanelGrid;
	}

	protected HtmlGraphicImageEx getImageExRightColImage1NoLink() {
		if (imageExRightColImage1NoLink == null) {
			imageExRightColImage1NoLink = (HtmlGraphicImageEx) findComponentInRoot("imageExRightColImage1NoLink");
		}
		return imageExRightColImage1NoLink;
	}

	protected HtmlPanelGrid getGridRightColCustomSpot3() {
		if (gridRightColCustomSpot3 == null) {
			gridRightColCustomSpot3 = (HtmlPanelGrid) findComponentInRoot("gridRightColCustomSpot3");
		}
		return gridRightColCustomSpot3;
	}

	protected HtmlPanelGrid getGridRightColEZPAY() {
		if (gridRightColEZPAY == null) {
			gridRightColEZPAY = (HtmlPanelGrid) findComponentInRoot("gridRightColEZPAY");
		}
		return gridRightColEZPAY;
	}

	protected HtmlOutputText getTextDeliveryWorkPhoneLabel() {
		if (textDeliveryWorkPhoneLabel == null) {
			textDeliveryWorkPhoneLabel = (HtmlOutputText) findComponentInRoot("textDeliveryWorkPhoneLabel");
		}
		return textDeliveryWorkPhoneLabel;
	}

	protected HtmlOutputText getTextDeliveryWorkPhone() {
		if (textDeliveryWorkPhone == null) {
			textDeliveryWorkPhone = (HtmlOutputText) findComponentInRoot("textDeliveryWorkPhone");
		}
		return textDeliveryWorkPhone;
	}

	protected HtmlMessages getJspPanelEE() {
		if (jspPanelEE == null) {
			jspPanelEE = (HtmlMessages) findComponentInRoot("jspPanelEE");
		}
		return jspPanelEE;
	}

	protected HtmlScriptCollector getScriptCollectorTemplateLogoutCollector() {
		if (scriptCollectorTemplateLogoutCollector == null) {
			scriptCollectorTemplateLogoutCollector = (HtmlScriptCollector) findComponentInRoot("scriptCollectorTemplateLogoutCollector");
		}
		return scriptCollectorTemplateLogoutCollector;
	}

	protected HtmlForm getFormLogoutTemplateAction() {
		if (formLogoutTemplateAction == null) {
			formLogoutTemplateAction = (HtmlForm) findComponentInRoot("formLogoutTemplateAction");
		}
		return formLogoutTemplateAction;
	}

	protected HtmlOutputLinkEx getLinkExLogOutLinkTemplate1() {
		if (linkExLogOutLinkTemplate1 == null) {
			linkExLogOutLinkTemplate1 = (HtmlOutputLinkEx) findComponentInRoot("linkExLogOutLinkTemplate1");
		}
		return linkExLogOutLinkTemplate1;
	}

	protected HtmlOutputText getTextLogoutLinkTemplateLabel1() {
		if (textLogoutLinkTemplateLabel1 == null) {
			textLogoutLinkTemplateLabel1 = (HtmlOutputText) findComponentInRoot("textLogoutLinkTemplateLabel1");
		}
		return textLogoutLinkTemplateLabel1;
	}

	protected HtmlScriptCollector getScriptCollectorTemplateCollectorLeftNav() {
		if (scriptCollectorTemplateCollectorLeftNav == null) {
			scriptCollectorTemplateCollectorLeftNav = (HtmlScriptCollector) findComponentInRoot("scriptCollectorTemplateCollectorLeftNav");
		}
		return scriptCollectorTemplateCollectorLeftNav;
	}

	protected HtmlPanelGrid getGridNavigationTopGrid() {
		if (gridNavigationTopGrid == null) {
			gridNavigationTopGrid = (HtmlPanelGrid) findComponentInRoot("gridNavigationTopGrid");
		}
		return gridNavigationTopGrid;
	}

	protected HtmlPanelGrid getGridLeftNavGroup1Grid() {
		if (gridLeftNavGroup1Grid == null) {
			gridLeftNavGroup1Grid = (HtmlPanelGrid) findComponentInRoot("gridLeftNavGroup1Grid");
		}
		return gridLeftNavGroup1Grid;
	}

	protected HtmlOutputLinkEx getLinkExNavGroup1SubscriberServices() {
		if (linkExNavGroup1SubscriberServices == null) {
			linkExNavGroup1SubscriberServices = (HtmlOutputLinkEx) findComponentInRoot("linkExNavGroup1SubscriberServices");
		}
		return linkExNavGroup1SubscriberServices;
	}

	protected HtmlOutputText getTextNavGroup1LinkLabel1() {
		if (textNavGroup1LinkLabel1 == null) {
			textNavGroup1LinkLabel1 = (HtmlOutputText) findComponentInRoot("textNavGroup1LinkLabel1");
		}
		return textNavGroup1LinkLabel1;
	}

	protected HtmlOutputText getTextNavGroup1LinkLabelDotCom() {
		if (textNavGroup1LinkLabelDotCom == null) {
			textNavGroup1LinkLabelDotCom = (HtmlOutputText) findComponentInRoot("textNavGroup1LinkLabelDotCom");
		}
		return textNavGroup1LinkLabelDotCom;
	}

	protected HtmlOutputText getTextNavGroup1LinkLabelFAQ() {
		if (textNavGroup1LinkLabelFAQ == null) {
			textNavGroup1LinkLabelFAQ = (HtmlOutputText) findComponentInRoot("textNavGroup1LinkLabelFAQ");
		}
		return textNavGroup1LinkLabelFAQ;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link0() {
		if (linkExNavArea2Link0 == null) {
			linkExNavArea2Link0 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link0");
		}
		return linkExNavArea2Link0;
	}

	protected HtmlOutputText getTextNavArea2Link0() {
		if (textNavArea2Link0 == null) {
			textNavArea2Link0 = (HtmlOutputText) findComponentInRoot("textNavArea2Link0");
		}
		return textNavArea2Link0;
	}

	protected HtmlOutputText getTextNavArea2Link1() {
		if (textNavArea2Link1 == null) {
			textNavArea2Link1 = (HtmlOutputText) findComponentInRoot("textNavArea2Link1");
		}
		return textNavArea2Link1;
	}

	protected HtmlOutputText getTextNavArea2Link2() {
		if (textNavArea2Link2 == null) {
			textNavArea2Link2 = (HtmlOutputText) findComponentInRoot("textNavArea2Link2");
		}
		return textNavArea2Link2;
	}

	protected HtmlOutputText getTextNavArea2Link3() {
		if (textNavArea2Link3 == null) {
			textNavArea2Link3 = (HtmlOutputText) findComponentInRoot("textNavArea2Link3");
		}
		return textNavArea2Link3;
	}

	protected HtmlOutputText getTextNavArea2Link4() {
		if (textNavArea2Link4 == null) {
			textNavArea2Link4 = (HtmlOutputText) findComponentInRoot("textNavArea2Link4");
		}
		return textNavArea2Link4;
	}

	protected HtmlOutputText getTextNavArea2Link5() {
		if (textNavArea2Link5 == null) {
			textNavArea2Link5 = (HtmlOutputText) findComponentInRoot("textNavArea2Link5");
		}
		return textNavArea2Link5;
	}

	protected HtmlScriptCollector getScriptCollectorBottomNavCollector() {
		if (scriptCollectorBottomNavCollector == null) {
			scriptCollectorBottomNavCollector = (HtmlScriptCollector) findComponentInRoot("scriptCollectorBottomNavCollector");
		}
		return scriptCollectorBottomNavCollector;
	}

	protected HtmlJspPanel getJspPanelBottomNavCustomPanel() {
		if (jspPanelBottomNavCustomPanel == null) {
			jspPanelBottomNavCustomPanel = (HtmlJspPanel) findComponentInRoot("jspPanelBottomNavCustomPanel");
		}
		return jspPanelBottomNavCustomPanel;
	}

	protected HtmlOutputLinkEx getLinkExTopNavLogoLink() {
		if (linkExTopNavLogoLink == null) {
			linkExTopNavLogoLink = (HtmlOutputLinkEx) findComponentInRoot("linkExTopNavLogoLink");
		}
		return linkExTopNavLogoLink;
	}

	protected HtmlGraphicImageEx getImageExPartnerLogo() {
		if (imageExPartnerLogo == null) {
			imageExPartnerLogo = (HtmlGraphicImageEx) findComponentInRoot("imageExPartnerLogo");
		}
		return imageExPartnerLogo;
	}

	protected HtmlPanelGrid getGridNavigationOuterGrid() {
		if (gridNavigationOuterGrid == null) {
			gridNavigationOuterGrid = (HtmlPanelGrid) findComponentInRoot("gridNavigationOuterGrid");
		}
		return gridNavigationOuterGrid;
	}

	protected HtmlOutputLinkEx getLinkExNavGroup2DotCom() {
		if (linkExNavGroup2DotCom == null) {
			linkExNavGroup2DotCom = (HtmlOutputLinkEx) findComponentInRoot("linkExNavGroup2DotCom");
		}
		return linkExNavGroup2DotCom;
	}

	protected HtmlOutputLinkEx getLinkExNavGroupFAQ() {
		if (linkExNavGroupFAQ == null) {
			linkExNavGroupFAQ = (HtmlOutputLinkEx) findComponentInRoot("linkExNavGroupFAQ");
		}
		return linkExNavGroupFAQ;
	}

	protected HtmlPanelGrid getGridLeftNavGroup2Grid() {
		if (gridLeftNavGroup2Grid == null) {
			gridLeftNavGroup2Grid = (HtmlPanelGrid) findComponentInRoot("gridLeftNavGroup2Grid");
		}
		return gridLeftNavGroup2Grid;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link1() {
		if (linkExNavArea2Link1 == null) {
			linkExNavArea2Link1 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link1");
		}
		return linkExNavArea2Link1;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link2() {
		if (linkExNavArea2Link2 == null) {
			linkExNavArea2Link2 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link2");
		}
		return linkExNavArea2Link2;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link3() {
		if (linkExNavArea2Link3 == null) {
			linkExNavArea2Link3 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link3");
		}
		return linkExNavArea2Link3;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link4() {
		if (linkExNavArea2Link4 == null) {
			linkExNavArea2Link4 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link4");
		}
		return linkExNavArea2Link4;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link5() {
		if (linkExNavArea2Link5 == null) {
			linkExNavArea2Link5 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link5");
		}
		return linkExNavArea2Link5;
	}

	protected HtmlPanelGrid getGridNavigationBottomGrid() {
		if (gridNavigationBottomGrid == null) {
			gridNavigationBottomGrid = (HtmlPanelGrid) findComponentInRoot("gridNavigationBottomGrid");
		}
		return gridNavigationBottomGrid;
	}

	protected HtmlGraphicImageEx getImageExPartnerLogoNoLink() {
		if (imageExPartnerLogoNoLink == null) {
			imageExPartnerLogoNoLink = (HtmlGraphicImageEx) findComponentInRoot("imageExPartnerLogoNoLink");
		}
		return imageExPartnerLogoNoLink;
	}

	protected HtmlOutputText getTextEZPayStatus() {
		if (textEZPayStatus == null) {
			textEZPayStatus = (HtmlOutputText) findComponentInRoot("textEZPayStatus");
		}
		return textEZPayStatus;
	}

	protected HtmlOutputText getTextDeliveryEmailLabel() {
		if (textDeliveryEmailLabel == null) {
			textDeliveryEmailLabel = (HtmlOutputText) findComponentInRoot("textDeliveryEmailLabel");
		}
		return textDeliveryEmailLabel;
	}

	protected HtmlOutputText getTextDeliveryEmail() {
		if (textDeliveryEmail == null) {
			textDeliveryEmail = (HtmlOutputText) findComponentInRoot("textDeliveryEmail");
		}
		return textDeliveryEmail;
	}

	protected HtmlOutputText getTextBillingAddr1Label() {
		if (textBillingAddr1Label == null) {
			textBillingAddr1Label = (HtmlOutputText) findComponentInRoot("textBillingAddr1Label");
		}
		return textBillingAddr1Label;
	}

	protected HtmlOutputText getTextBillingAddr1() {
		if (textBillingAddr1 == null) {
			textBillingAddr1 = (HtmlOutputText) findComponentInRoot("textBillingAddr1");
		}
		return textBillingAddr1;
	}

	protected HtmlGraphicImageEx getImageExRightColImage2() {
		if (imageExRightColImage2 == null) {
			imageExRightColImage2 = (HtmlGraphicImageEx) findComponentInRoot("imageExRightColImage2");
		}
		return imageExRightColImage2;
	}

	protected HtmlOutputLinkEx getLinkExRightColImageSpot2Link() {
		if (linkExRightColImageSpot2Link == null) {
			linkExRightColImageSpot2Link = (HtmlOutputLinkEx) findComponentInRoot("linkExRightColImageSpot2Link");
		}
		return linkExRightColImageSpot2Link;
	}

	protected HtmlOutputLinkEx getLinkExRightColImageSpot1Link() {
		if (linkExRightColImageSpot1Link == null) {
			linkExRightColImageSpot1Link = (HtmlOutputLinkEx) findComponentInRoot("linkExRightColImageSpot1Link");
		}
		return linkExRightColImageSpot1Link;
	}

	protected HtmlGraphicImageEx getImageExRightColImage1() {
		if (imageExRightColImage1 == null) {
			imageExRightColImage1 = (HtmlGraphicImageEx) findComponentInRoot("imageExRightColImage1");
		}
		return imageExRightColImage1;
	}

	protected HtmlOutputLinkEx getLinkExLeftColImageSpot1Link() {
		if (linkExLeftColImageSpot1Link == null) {
			linkExLeftColImageSpot1Link = (HtmlOutputLinkEx) findComponentInRoot("linkExLeftColImageSpot1Link");
		}
		return linkExLeftColImageSpot1Link;
	}

	protected HtmlGraphicImageEx getImageExLeftColImage1() {
		if (imageExLeftColImage1 == null) {
			imageExLeftColImage1 = (HtmlGraphicImageEx) findComponentInRoot("imageExLeftColImage1");
		}
		return imageExLeftColImage1;
	}

	protected HtmlPanelGrid getLeftColImageSpot1() {
		if (leftColImageSpot1 == null) {
			leftColImageSpot1 = (HtmlPanelGrid) findComponentInRoot("leftColImageSpot1");
		}
		return leftColImageSpot1;
	}

	protected HtmlGraphicImageEx getImageExLeftColImage1NoLink() {
		if (imageExLeftColImage1NoLink == null) {
			imageExLeftColImage1NoLink = (HtmlGraphicImageEx) findComponentInRoot("imageExLeftColImage1NoLink");
		}
		return imageExLeftColImage1NoLink;
	}

	protected HtmlJspPanel getJspPanelThankYouGiftPanel() {
		if (jspPanelThankYouGiftPanel == null) {
			jspPanelThankYouGiftPanel = (HtmlJspPanel) findComponentInRoot("jspPanelThankYouGiftPanel");
		}
		return jspPanelThankYouGiftPanel;
	}

	protected HtmlOutputText getTextThankYouGift() {
		if (textThankYouGift == null) {
			textThankYouGift = (HtmlOutputText) findComponentInRoot("textThankYouGift");
		}
		return textThankYouGift;
	}

	protected HtmlOutputLinkEx getLinkExGiftLink() {
		if (linkExGiftLink == null) {
			linkExGiftLink = (HtmlOutputLinkEx) findComponentInRoot("linkExGiftLink");
		}
		return linkExGiftLink;
	}

	protected HtmlOutputText getTextGiftLink() {
		if (textGiftLink == null) {
			textGiftLink = (HtmlOutputText) findComponentInRoot("textGiftLink");
		}
		return textGiftLink;
	}

	protected HtmlPanelGrid getGridRightColCustomSpot2() {
		if (gridRightColCustomSpot2 == null) {
			gridRightColCustomSpot2 = (HtmlPanelGrid) findComponentInRoot("gridRightColCustomSpot2");
		}
		return gridRightColCustomSpot2;
	}

	protected HtmlOutputText getTextThankYouCommJunc1() {
		if (textThankYouCommJunc1 == null) {
			textThankYouCommJunc1 = (HtmlOutputText) findComponentInRoot("textThankYouCommJunc1");
		}
		return textThankYouCommJunc1;
	}

	protected HtmlOutputText getTextThankYouJS2() {
		if (textThankYouJS2 == null) {
			textThankYouJS2 = (HtmlOutputText) findComponentInRoot("textThankYouJS2");
		}
		return textThankYouJS2;
	}

	protected HtmlOutputText getTextThankYouJS3() {
		if (textThankYouJS3 == null) {
			textThankYouJS3 = (HtmlOutputText) findComponentInRoot("textThankYouJS3");
		}
		return textThankYouJS3;
	}

	protected HtmlOutputText getTextThankYouJS4() {
		if (textThankYouJS4 == null) {
			textThankYouJS4 = (HtmlOutputText) findComponentInRoot("textThankYouJS4");
		}
		return textThankYouJS4;
	}

	protected HtmlOutputText getTextThankYouJS5() {
		if (textThankYouJS5 == null) {
			textThankYouJS5 = (HtmlOutputText) findComponentInRoot("textThankYouJS5");
		}
		return textThankYouJS5;
	}

	protected HtmlOutputText getTextThankYouJS6() {
		if (textThankYouJS6 == null) {
			textThankYouJS6 = (HtmlOutputText) findComponentInRoot("textThankYouJS6");
		}
		return textThankYouJS6;
	}

	protected HtmlOutputText getTextThankYouJS7() {
		if (textThankYouJS7 == null) {
			textThankYouJS7 = (HtmlOutputText) findComponentInRoot("textThankYouJS7");
		}
		return textThankYouJS7;
	}

	protected HtmlOutputText getTextThankYouJS8() {
		if (textThankYouJS8 == null) {
			textThankYouJS8 = (HtmlOutputText) findComponentInRoot("textThankYouJS8");
		}
		return textThankYouJS8;
	}

	protected HtmlOutputText getTextThankYouJS9() {
		if (textThankYouJS9 == null) {
			textThankYouJS9 = (HtmlOutputText) findComponentInRoot("textThankYouJS9");
		}
		return textThankYouJS9;
	}

	protected HtmlPanelGrid getGridPremiumPromoGrid() {
		if (gridPremiumPromoGrid == null) {
			gridPremiumPromoGrid = (HtmlPanelGrid) findComponentInRoot("gridPremiumPromoGrid");
		}
		return gridPremiumPromoGrid;
	}

	protected HtmlOutputText getTextPremiumPromotionLabel() {
		if (textPremiumPromotionLabel == null) {
			textPremiumPromotionLabel = (HtmlOutputText) findComponentInRoot("textPremiumPromotionLabel");
		}
		return textPremiumPromotionLabel;
	}

	protected HtmlOutputText getTextPremiumPromotionDescriptionText() {
		if (textPremiumPromotionDescriptionText == null) {
			textPremiumPromotionDescriptionText = (HtmlOutputText) findComponentInRoot("textPremiumPromotionDescriptionText");
		}
		return textPremiumPromotionDescriptionText;
	}

	protected HtmlGraphicImageEx getImageExRightColImage2NoLink() {
		if (imageExRightColImage2NoLink == null) {
			imageExRightColImage2NoLink = (HtmlGraphicImageEx) findComponentInRoot("imageExRightColImage2NoLink");
		}
		return imageExRightColImage2NoLink;
	}

	protected HtmlPanelGrid getGridRightColCustomSpot4() {
		if (gridRightColCustomSpot4 == null) {
			gridRightColCustomSpot4 = (HtmlPanelGrid) findComponentInRoot("gridRightColCustomSpot4");
		}
		return gridRightColCustomSpot4;
	}

	protected HtmlOutputLinkEx getLinkExOpenDesktopVersion() {
		if (linkExOpenDesktopVersion == null) {
			linkExOpenDesktopVersion = (HtmlOutputLinkEx) findComponentInRoot("linkExOpenDesktopVersion");
		}
		return linkExOpenDesktopVersion;
	}

	protected HtmlOutputText getText1111() {
		if (text1111 == null) {
			text1111 = (HtmlOutputText) findComponentInRoot("text1111");
		}
		return text1111;
	}

	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected HtmlOutputLinkEx getLinkExOpenMobileVersion() {
		if (linkExOpenMobileVersion == null) {
			linkExOpenMobileVersion = (HtmlOutputLinkEx) findComponentInRoot("linkExOpenMobileVersion");
		}
		return linkExOpenMobileVersion;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlOutputText getTextThankYouJS1() {
		if (textThankYouJS1 == null) {
			textThankYouJS1 = (HtmlOutputText) findComponentInRoot("textThankYouJS1");
		}
		return textThankYouJS1;
	}

	protected HtmlOutputText getTextThankYouJS12() {
		if (textThankYouJS12 == null) {
			textThankYouJS12 = (HtmlOutputText) findComponentInRoot("textThankYouJS12");
		}
		return textThankYouJS12;
	}

	protected HtmlOutputText getTextThankYouJS10() {
		if (textThankYouJS10 == null) {
			textThankYouJS10 = (HtmlOutputText) findComponentInRoot("textThankYouJS10");
		}
		return textThankYouJS10;
	}

	protected HtmlOutputText getTextEZPayInfoText() {
		if (textEZPayInfoText == null) {
			textEZPayInfoText = (HtmlOutputText) findComponentInRoot("textEZPayInfoText");
		}
		return textEZPayInfoText;
	}

	protected HtmlOutputText getTextThankYouJS13() {
		if (textThankYouJS13 == null) {
			textThankYouJS13 = (HtmlOutputText) findComponentInRoot("textThankYouJS13");
		}
		return textThankYouJS13;
	}

}