/**
 * 
 */
package pagecode.subscriptions.order;

import pagecode.PageCodeBase;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.ibm.faces.component.html.HtmlCommandExButton;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlMessages;
import javax.faces.context.FacesContext;

import com.usatoday.esub.handlers.ShoppingCartHandler;
import com.ibm.faces.component.html.HtmlProgressBar;

/**
 * @author aeast
 * 
 */
public class Order_in_progress extends PageCodeBase {

	protected HtmlScriptCollector scriptCollectorOrderChecker1;
	protected HtmlCommandExButton buttonUpdateStatus;
	protected HtmlForm formCheckStatus;
	protected HtmlMessages messages1;
	protected ShoppingCartHandler shoppingCartHandler;
	protected HtmlProgressBar bar1;

	protected HtmlScriptCollector getScriptCollectorOrderChecker1() {
		if (scriptCollectorOrderChecker1 == null) {
			scriptCollectorOrderChecker1 = (HtmlScriptCollector) findComponentInRoot("scriptCollectorOrderChecker1");
		}
		return scriptCollectorOrderChecker1;
	}

	protected HtmlCommandExButton getButtonUpdateStatus() {
		if (buttonUpdateStatus == null) {
			buttonUpdateStatus = (HtmlCommandExButton) findComponentInRoot("buttonUpdateStatus");
		}
		return buttonUpdateStatus;
	}

	protected HtmlForm getFormCheckStatus() {
		if (formCheckStatus == null) {
			formCheckStatus = (HtmlForm) findComponentInRoot("formCheckStatus");
		}
		return formCheckStatus;
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

	public String doButtonUpdateStatusAction() {
		// Type Java code that runs when the component is clicked

		ShoppingCartHandler sch = this.getShoppingCartHandler();

		if (sch.isCheckoutStarted() || sch.getCart().isCheckoutInProcess()) {
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"Your order is still processing payment and address verification..please wait.", null);

			context.addMessage(null, facesMsg);
			return "processing";
		}

		if (sch.getLastOrder() != null) {
			return "orderProcessed";
		} else {
			try {
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
						"Your order did not complete. Please try again or call our National Customer Service for assistance. Detail:"
								+ sch.getCart().getCheckOutErrorMessage(), null);

				context.addMessage(null, facesMsg);
				return "redo";
			} catch (Exception e) {
				System.out.println("Failed to determine order status." + e.getMessage());
				e.printStackTrace();
				return "redo";
			}
		}

	}

	/**
	 * @managed-bean true
	 */
	protected ShoppingCartHandler getShoppingCartHandler() {
		if (shoppingCartHandler == null) {
			shoppingCartHandler = (ShoppingCartHandler) getManagedBean("shoppingCartHandler");
		}
		return shoppingCartHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setShoppingCartHandler(ShoppingCartHandler shoppingCartHandler) {
		this.shoppingCartHandler = shoppingCartHandler;
	}

	public void onPagePost(FacesContext facescontext) {
		// Type Java code to handle page post event here

		// void <method>(FacesContext facescontext)

	}

	protected HtmlProgressBar getBar1() {
		if (bar1 == null) {
			bar1 = (HtmlProgressBar) findComponentInRoot("bar1");
		}
		return bar1;
	}

}