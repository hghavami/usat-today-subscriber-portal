/**
 * 
 */
package pagecode.subscriptions.order;

import java.util.Collection;
import java.util.Hashtable;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.UIParameter;
import javax.faces.component.UISelectItem;
import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputFormat;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.component.html.HtmlSelectOneRadio;
import javax.faces.context.FacesContext;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.servlet.http.HttpServletRequest;

import org.joda.time.DateTime;

import pagecode.PageCodeBase;

import com.ibm.faces.component.html.HtmlAjaxRefreshSubmit;
import com.ibm.faces.component.html.HtmlBehavior;
import com.ibm.faces.component.html.HtmlBehaviorKeyPress;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlFormItem;
import com.ibm.faces.component.html.HtmlGraphicImageEx;
import com.ibm.faces.component.html.HtmlInputHelperAssist;
import com.ibm.faces.component.html.HtmlInputHelperDatePicker;
import com.ibm.faces.component.html.HtmlInputHelperSetFocus;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.ibm.faces.component.html.HtmlPanelDialog;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.component.html.HtmlPanelLayout;
import com.ibm.faces.component.html.HtmlPanelSection;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.UsatException;
import com.usatoday.business.interfaces.UIAddressIntf;
import com.usatoday.business.interfaces.customer.CustomerIntf;
import com.usatoday.business.interfaces.customer.EmailRecordIntf;
import com.usatoday.business.interfaces.customer.TrialInstanceIntf;
import com.usatoday.business.interfaces.products.SubscriptionOfferIntf;
import com.usatoday.business.interfaces.products.SubscriptionProductIntf;
import com.usatoday.business.interfaces.products.SubscriptionTermsIntf;
import com.usatoday.business.interfaces.products.promotions.PromotionIntf;
import com.usatoday.business.interfaces.shopping.OrderIntf;
import com.usatoday.business.interfaces.shopping.ShoppingCartIntf;
import com.usatoday.business.interfaces.shopping.SubscriptionOrderItemIntf;
import com.usatoday.business.interfaces.shopping.payment.PaymentMethodIntf;
import com.usatoday.businessObjects.customer.ContactBO;
import com.usatoday.businessObjects.customer.CustomerBO;
import com.usatoday.businessObjects.customer.CustomerInterimInfoBO;
import com.usatoday.businessObjects.customer.DeliveryMethodCode1ValidationBO;
import com.usatoday.businessObjects.customer.EmailRecordBO;
import com.usatoday.businessObjects.customer.UIAddressBO;
import com.usatoday.businessObjects.products.SubscriptionOfferManager;
import com.usatoday.businessObjects.products.SubscriptionProductBO;
import com.usatoday.businessObjects.products.promotions.PromotionSet;
import com.usatoday.businessObjects.shopping.CheckOutService;
import com.usatoday.businessObjects.shopping.SubscriptionOrderItemBO;
import com.usatoday.businessObjects.shopping.payment.CreditCardPaymentMethodBO;
import com.usatoday.businessObjects.shopping.payment.InvoicePaymentMethodBO;
import com.usatoday.businessObjects.util.AS400CurrentStatus;
import com.usatoday.businessObjects.util.KeyCodeParser;
import com.usatoday.esub.common.UTCommon;
import com.usatoday.esub.handlers.InterimCustomerHandler;
import com.usatoday.esub.handlers.NewSubscriptionOrderHandler;
import com.usatoday.esub.handlers.PremiumPromotionHandler;
import com.usatoday.esub.handlers.ShoppingCartHandler;
import com.usatoday.esub.handlers.SubscriptionOfferHandler;
import com.usatoday.esub.ncs.handlers.CustomerHandler;
import com.usatoday.esub.trials.handlers.PartnerHandler;
import com.usatoday.esub.trials.handlers.TrialCustomerHandler;
import com.usatoday.util.constants.UsaTodayConstants;

/**
 * @author aeast
 * 
 */
public class Checkout extends PageCodeBase {

	protected PartnerHandler trialPartnerHandler;
	protected TrialCustomerHandler trialCustomerHandler;
	protected ShoppingCartHandler shoppingCartHandler;
	protected SubscriptionOfferHandler currentOfferHandler;
	protected CustomerHandler customerHandler;
	protected HtmlAjaxRefreshSubmit ajaxRefreshSubmitStartDate;
	protected HtmlPanelFormBox formBoxBillingAddress;
	protected HtmlScriptCollector scriptCollectorTemplateLogoutCollector;
	protected HtmlForm formLogoutTemplateAction;
	protected HtmlOutputLinkEx linkExLogOutLinkTemplate1;
	protected HtmlOutputText textLogoutLinkTemplateLabel1;
	protected HtmlScriptCollector scriptCollectorTemplateCollectorLeftNav;
	protected HtmlPanelGrid gridNavigationTopGrid;
	protected HtmlPanelGrid gridLeftNavGroup1Grid;
	protected HtmlOutputLinkEx linkExNavGroup2International;
	protected HtmlOutputText textNavGroup1LinkLabelInternational;
	protected HtmlOutputText textNavGroup1LinkLabelKindle;
	protected HtmlOutputText textNavGroup1LinkLabel1;
	protected HtmlOutputText textNavGroup1LinkLabelDotCom;
	protected HtmlOutputText textNavGroup1LinkLabelFAQ;
	protected HtmlOutputLinkEx linkExNavArea2Link0;
	protected HtmlOutputText textNavArea2Link0;
	protected HtmlOutputText textNavArea2Link1;
	protected HtmlOutputText textNavArea2Link2;
	protected HtmlOutputText textNavArea2Link3;
	protected HtmlOutputText textNavArea2Link4;
	protected HtmlOutputText textNavArea2Link5;
	protected HtmlPanelGrid gridNavigationOuterGrid;
	protected HtmlOutputLinkEx linkExNavGroup2Kindle;
	protected HtmlOutputLinkEx linkExNavGroup1SubscriberServices;
	protected HtmlOutputLinkEx linkExNavGroup2DotCom;
	protected HtmlOutputLinkEx linkExNavGroupFAQ;
	protected HtmlPanelGrid gridLeftNavGroup2Grid;
	protected HtmlOutputLinkEx linkExNavArea2Link1;
	protected HtmlOutputLinkEx linkExNavArea2Link2;
	protected HtmlOutputLinkEx linkExNavArea2Link3;
	protected HtmlOutputLinkEx linkExNavArea2Link4;
	protected HtmlOutputLinkEx linkExNavArea2Link5;
	protected HtmlPanelGrid gridNavigationBottomGrid;
	protected HtmlOutputText RightColHTMLSpot1Text;
	protected HtmlOutputText RightColHTMLSpot2Text;
	protected InterimCustomerHandler interimCustomerHandler;
	protected HtmlOutputText mainHeader;
	protected PremiumPromotionHandler premiumPromotionHandler;
	protected HtmlOutputText textDelMethodText;
	protected HtmlInputText textDeliveryFName;
	protected NewSubscriptionOrderHandler newSubscriptionOrderHandler;
	protected HtmlOutputFormat format1;
	protected HtmlScriptCollector scriptCollectorBottomNavCollector;
	protected HtmlForm formBottomNavForm;
	protected HtmlPanelGrid gridLeftColHTMLSpot1Grid;
	protected HtmlOutputText leftColHTMLSpot1Text;
	protected HtmlOutputLinkEx linkExTopNavLogoLink;
	protected HtmlGraphicImageEx imageExPartnerLogo;
	protected HtmlOutputLinkEx linkExLeftColImageSpot1Link;
	protected HtmlGraphicImageEx imageExLeftColImage1;
	protected HtmlOutputLinkEx linkExVideoLink_LeftCol;
	protected HtmlGraphicImageEx imageExVideoSweepsGraphic_LeftCol;
	protected HtmlOutputLinkEx linkExLeftColImageSpot2Link;
	protected HtmlGraphicImageEx imageExLeftColImage2;
	protected HtmlJspPanel jspPanelBottomNavCustomPanel;
	protected HtmlGraphicImageEx imageExPartnerLogoNoLink;
	protected HtmlPanelGrid leftColImageSpot1;
	protected HtmlGraphicImageEx imageExLeftColImage1NoLink;
	protected HtmlPanelGrid gridVideoGridLeftCol;
	protected HtmlOutputText textEEVideoTextLeftCol;
	protected HtmlPanelGrid leftColImageSpot2;
	protected HtmlGraphicImageEx imageExLeftColImage2NoLink;
	protected HtmlInputHelperAssist assist1;
	protected UISelectItems selectItems2;
	protected HtmlInputHelperAssist assist4;
	protected HtmlInputHelperAssist assist5;
	protected HtmlInputHelperAssist assist44;
	protected HtmlInputHelperAssist assist45;
	protected HtmlInputHelperAssist assist2;
	protected HtmlInputHelperAssist assist3;
	protected UISelectItem selectItem1;
	protected UISelectItem selectItem16;
	protected UISelectItems selectItems1;
	protected UISelectItem selectItem17;
	protected UISelectItem selectItem19;
	protected HtmlInputHelperAssist assist7;
	protected HtmlInputHelperDatePicker datePicker1;
	protected UISelectItem selectItem2;
	protected HtmlInputHelperAssist assist6;
	protected UISelectItem selectItem3;
	protected UISelectItem selectItem4;
	protected UISelectItem selectItem5;
	protected UISelectItem selectItem6;
	protected UISelectItem selectItem7;
	protected UISelectItem selectItem8;
	protected UISelectItem selectItem9;
	protected UISelectItem selectItem10;
	protected UISelectItem selectItem11;
	protected UISelectItem selectItem12;
	protected UISelectItem selectItem13;
	protected UISelectItem selectItem14;
	protected UISelectItem selectItem18;
	protected UISelectItem selectItem20;
	protected UISelectItem selectItem15;
	protected HtmlScriptCollector scriptCollectorMainOrderEntryCollector;
	protected HtmlForm formOrderEntryForm;
	protected HtmlOutputText textMainTableHeader;
	protected HtmlJspPanel jspPanelFormPanel;
	protected HtmlOutputText textPromoText1;
	protected HtmlOutputText textDelvMethodText;
	protected HtmlPanelLayout layoutPageLayout;
	protected HtmlPanelGroup groupMainBodyPanel;
	protected HtmlPanelGrid gridTermsGrid;
	protected HtmlPanelGroup group10;
	protected HtmlOutputText text7;
	protected HtmlFormItem formItemSubscriptionTerms;
	protected HtmlSelectOneRadio radioTermsSelection;
	protected HtmlPanelGrid gridTermsFormBoxBottomFacetGrid;
	protected HtmlPanelGroup group4;
	protected HtmlOutputText textEZPayExplanationRateCustomText;
	protected HtmlPanelSection sectionNewOfferCode1;
	protected HtmlJspPanel jspPanel4;
	protected HtmlOutputText textOfferOverrridePanelHeaderClosedText;
	protected HtmlJspPanel jspPanel3;
	protected HtmlOutputText text2;
	protected HtmlPanelFormBox formBoxOfferCodeEntry1;
	protected HtmlFormItem formItem1;
	protected HtmlInputText textNewOfferCode;
	protected UIParameter custOfferCodeParm;
	protected HtmlPanelGrid gridNewOfferBottomGrid1;
	protected HtmlCommandLink linkRevertOfferLink;
	protected HtmlOutputText textRevertOfferLinkText;
	protected HtmlPanelGroup groupPromoTermsHeaderGroup8;
	protected HtmlOutputText textPrmotionsTermLabel;
	protected HtmlOutputText textPromoDescriptionLabel;
	protected HtmlOutputText text6;
	protected HtmlPanelFormBox formBoxQuantityInformation;
	protected HtmlFormItem formItemQuantityInformation;
	protected HtmlSelectOneMenu menuOrderQuantity;
	protected HtmlPanelFormBox formBoxPartnerInformation;
	protected HtmlFormItem formItemPartnerClubNumber;
	protected HtmlInputText textClubNumber;
	protected HtmlPanelFormBox formBoxStartDateInformation;
	protected HtmlFormItem formItemStartDateFormItem;
	protected HtmlInputText textStartDateInput;
	protected HtmlPanelFormBox formBoxDeliveryInformation;
	protected HtmlFormItem formItemDeliveryFName;
	protected HtmlInputText textDeliveryFirstName;
	protected HtmlInputText textDeliveryLName;
	protected HtmlInputText textDeliveryCompanyName;
	protected HtmlInputText textDeliveryAddress1Text;
	protected HtmlInputText textDeliveryAddress2;
	protected HtmlInputText textDeliveryCity;
	protected HtmlSelectOneMenu menuDeliveryState;
	protected HtmlInputText textDeliveryPhoneAreaCode;
	protected HtmlInputText textDeliveryWorkPhoneAreaCode;
	protected HtmlInputText textEmailAddressRecipient;
	protected HtmlInputText textEmailAddressConfirmRecipient;
	protected HtmlPanelGrid gridDeliveryMethodInfoGrid;
	protected HtmlOutputLinkEx linkExDelMethodCheckHelpLink;
	protected HtmlOutputText textDelMethodHelpText;
	protected HtmlPanelGrid gridDeliveryInformationPanelFooterGrid;
	protected HtmlPanelGrid gridDelMethodResultsGrid;
	protected HtmlOutputText textDeliveryMethod;
	protected HtmlPanelFormBox formBoxBillDifferentFromDelSelectionFormBox;
	protected HtmlFormItem formItemBillDifferentFromDelSelector;
	protected HtmlSelectBooleanCheckbox checkboxIsBillDifferentFromDelSelector;
	protected HtmlBehavior behavior1;
	protected HtmlFormItem formItemBillingFirstName;
	protected HtmlInputText textBillingFirstName;
	protected HtmlInputText textBillingLastName;
	protected HtmlInputText textBillingCompanyName;
	protected HtmlInputText textBillingAddress1;
	protected HtmlInputText textBillingAddress2;
	protected HtmlInputText textBillingCity;
	protected HtmlSelectOneMenu menuBillingState;
	protected HtmlInputText textBillingPhoneAreaCode;
	protected HtmlSelectBooleanCheckbox giftSubscriptionCheckbox;
	protected HtmlInputText textPurchaserEmailAddress;
	protected HtmlPanelFormBox formBoxPaymentHeaderFormBox;
	protected HtmlSelectOneRadio radioPaymentMethod;
	protected HtmlOutputText textForceBillMeText;
	protected HtmlFormItem formItemCreditCardNumber;
	protected HtmlInputText textCreditCardNumber;
	protected HtmlInputText textCVV;
	protected HtmlSelectOneMenu menuCCExpireMonth;
	protected HtmlPanelGrid panelGridCreditCardImageGrid;
	protected HtmlJspPanel jspPanelCreditCardImages;
	protected HtmlGraphicImageEx imageExAmEx1;
	protected HtmlSelectOneRadio radioRenewalOptions;
	protected HtmlSelectOneRadio radioRenewalOptionsGift;
	protected HtmlOutputFormat requiresEZPAYText;
	protected UIParameter param5;
	protected HtmlOutputText textDisclaimerText;
	protected HtmlOutputText textOfferDisclaimerText;
	protected HtmlOutputText textCOPPAText;
	protected HtmlGraphicImageEx imageEx3;
	protected HtmlPanelGrid gridEZPayFinePrintGrid;
	protected HtmlOutputText formatEZPAYFinePrint;
	protected HtmlMessages messagesAllMesssages;
	protected HtmlPanelGrid gridCustServicePhone;
	protected HtmlOutputText custServicePhoneText;
	protected HtmlOutputLinkEx linkExRightColImageSpot1Link;
	protected HtmlGraphicImageEx imageExRightColImage1;
	protected HtmlOutputLinkEx linkExVideoLink_B;
	protected HtmlGraphicImageEx imageExVideoSweepsGraphic_B;
	protected HtmlOutputLinkEx linkExRightColSpot2Link;
	protected HtmlGraphicImageEx imageExRightColImage2;
	protected HtmlOutputLinkEx linkExRightColSpot3Link;
	protected HtmlGraphicImageEx imageExRightColImage3;
	protected HtmlPanelGrid gridEZPayLearnMoreGrid2;
	protected HtmlOutputFormat formatEZPayHelpText;
	protected UIParameter param8;
	protected HtmlCommandExButton button2;
	protected HtmlBehavior behaviorEZPay4;
	protected HtmlPanelGrid gridStartDateLearnMoreGrid2;
	protected HtmlOutputFormat formatGiftFutureStartDateText;
	protected UIParameter param1;
	protected HtmlCommandExButton buttonStartDate2;
	protected HtmlBehavior behaviorStartDate4;
	protected HtmlPanelGrid grid2;
	protected HtmlOutputText text3;
	protected HtmlCommandExButton button5;
	protected HtmlBehavior behavior5;
	protected HtmlPanelGrid gridClubLearnMore;
	protected HtmlOutputText textClubLearnMore;
	protected HtmlCommandExButton button1;
	protected HtmlBehavior behavior2;
	protected HtmlPanelGrid gridCVVLearn;
	protected HtmlPanelGroup group7;
	protected HtmlPanelGroup group6;
	protected HtmlOutputText text4;
	protected HtmlCommandExButton button4;
	protected HtmlBehavior behavior4;
	protected HtmlPanelGroup groupDelMethod1;
	protected HtmlPanelGrid gridDelMethod2;
	protected HtmlOutputText textDelMethodDetailHelp;
	protected HtmlBehavior behaviorDelMethod4;
	protected HtmlPanelGroup groupAddlAddrHelpGroup1;
	protected HtmlPanelGrid grid1;
	protected HtmlOutputText textAdditionalAddrLearnMoreInfoText;
	protected HtmlBehavior behavior3;
	protected HtmlPanelGroup groupSpecialOfferGroupBox;
	protected HtmlPanelGrid gridSpecialOfferLayoutGrid;
	protected HtmlOutputLinkEx linkExCloseSpecialOffer;
	protected HtmlOutputText textCloseSpecialOfferText;
	protected HtmlGraphicImageEx tooimageExSpecialOfferImage;
	protected HtmlOutputText textScriptInsert1;
	protected HtmlPanelGrid gridMainBodyGrid;
	protected HtmlPanelFormBox formBoxTermsFormBox;
	protected HtmlOutputText textEZPayExplanationMessage;
	protected HtmlOutputText textEZPayLearnMore;
	protected HtmlPanelGrid gridOffOverrideGrid;
	protected HtmlCommandExButton buttonProcessOfferCode;
	protected HtmlPanelGrid gridPremiumPromotionGrid;
	protected HtmlPanelGrid gridPremiumPromotionTermsGrid;
	protected HtmlOutputText textPromoDescription;
	protected HtmlOutputText textSelectedTermLabel;
	protected HtmlOutputText textSelectedTerm;
	protected HtmlOutputText text5;
	protected HtmlOutputLinkEx linkExPromoPage;
	protected HtmlPanelGrid gridQuantityInformation;
	protected HtmlOutputText textNumCopiesLearnMoreLink;
	protected HtmlPanelGrid gridPartnerInformation;
	protected HtmlOutputText textProgInfoLearnMoreLink;
	protected HtmlPanelGrid gridStartDateInformation;
	protected HtmlOutputText textStartDateLearnMore;
	protected HtmlPanelGrid gridDeliveryInformation;
	protected HtmlFormItem formItemDeliveryLastName;
	protected HtmlFormItem formItemDeliveryCompanyName;
	protected HtmlFormItem formItemDeliveryAddress1;
	protected HtmlInputText textDeliveryAptSuite;
	protected HtmlFormItem formItemDeliveryAddress2;
	protected HtmlOutputText textAddrLearnMore;
	protected HtmlFormItem formItemDeliveryCity;
	protected HtmlFormItem formItemDeliveryState;
	protected HtmlInputText textDeliveryZip;
	protected HtmlFormItem formItemDeliveryPhone;
	protected HtmlInputText textDeliveryPhoneExchange;
	protected HtmlInputText textDeliveryPhoneExtension;
	protected HtmlFormItem formItemDeliveryWorkPhone;
	protected HtmlInputText textDeliveryWorkPhoneExchange;
	protected HtmlInputText textDeliveryWorkPhoneExtension;
	protected HtmlFormItem formItemEmailAddress;
	protected HtmlFormItem formItemEmailAddressConfirm;
	protected HtmlPanelGrid gridDetermineDeliveryOuterGrid;
	protected HtmlOutputText textDeliveryMethodTextDeterminedValue;
	protected HtmlCommandExButton buttonGetDeliveryMethodButton;
	protected HtmlPanelGrid panelGridBillingDifferentThanDelGrid;
	protected HtmlPanelGrid gridBillingAddress;
	protected HtmlFormItem formItemBillingLastName;
	protected HtmlFormItem formItemBillingCompanyName;
	protected HtmlFormItem formItemBillingAddress1;
	protected HtmlInputText textBillingAptSuite;
	protected HtmlFormItem formItemBillingAddress2;
	protected HtmlFormItem formItemBillingCity;
	protected HtmlFormItem formItemBillingState;
	protected HtmlInputText textBillingZipCode;
	protected HtmlFormItem formItemBillingTelephone;
	protected HtmlInputText textBillingPhoneExchange;
	protected HtmlInputText textBillingPhoneExtension;
	protected HtmlFormItem giftSubscriptionFormItem;
	protected HtmlFormItem formItemPayersEmailAddress;
	protected HtmlPanelGrid gridPaymentInformationGridLabelGrid;
	protected HtmlFormItem formItemBillMeFormItem;
	protected HtmlPanelGrid gridForceBillMeInfoGrid;
	protected HtmlPanelGrid gridPaymentInformationGrid;
	protected HtmlPanelFormBox formBoxPaymentInfo;
	protected HtmlFormItem formItemCreditCardCVV;
	protected HtmlOutputText textCVVLearnMore;
	protected HtmlFormItem formItemCCExpirationDate;
	protected HtmlSelectOneMenu menuCCExpireYear;
	protected HtmlGraphicImageEx imageExDiscover1;
	protected HtmlGraphicImageEx imageExMasterCard1;
	protected HtmlGraphicImageEx imageExVisaLogo1;
	protected HtmlPanelGrid gridEZPAYOptionsGrid;
	protected HtmlPanelGrid gridEZPAYOptionsGridGift;
	protected HtmlPanelGrid gridEZPAYRequiredGrid;
	protected HtmlPanelGrid gridTrialDisclaimerGrid;
	protected HtmlPanelGrid gridOfferDisclaimerGrid;
	protected HtmlPanelGrid COPPAGrid;
	protected HtmlPanelGrid panelGridFormSubmissionGrid;
	protected HtmlCommandExButton buttonPlaceOrder;
	protected HtmlJspPanel jspPanelGeoTrustPanel;
	protected HtmlGraphicImageEx imageEx33333;
	protected HtmlJspPanel pleaseWaitPanel;
	protected HtmlPanelGrid gridRequiredEZPayFinePrintGrid;
	protected HtmlPanelGrid rightColImageSpot1;
	protected HtmlGraphicImageEx imageExRightColImage1NoLink;
	protected HtmlPanelGrid gridRightColHTMLSpot1Grid;
	protected HtmlPanelGrid gridVideoGrid;
	protected HtmlOutputText textEEVideoText;
	protected HtmlPanelGrid rightColImageSpot2;
	protected HtmlGraphicImageEx imageExRightColImage2NoLink;
	protected HtmlPanelGrid gridRightColHTMLSpot2Grid;
	protected HtmlPanelGrid rightColImageSpot3;
	protected HtmlGraphicImageEx imageExRightColImage3NoLink;
	protected HtmlPanelGrid gridGeoTrust;
	protected HtmlPanelDialog dialogEZPayLearnMore1;
	protected HtmlPanelGroup group1;
	protected HtmlPanelDialog dialogStartDateLearnMore1;
	protected UIParameter param2;
	protected UIParameter param3;
	protected HtmlPanelGroup groupStartDate1;
	protected HtmlPanelDialog dialogQty;
	protected HtmlPanelGroup group2;
	protected HtmlPanelDialog dialogClubNum;
	protected HtmlPanelGroup group3;
	protected HtmlPanelDialog dialogCVV;
	protected HtmlPanelGroup group5;
	protected HtmlPanelDialog dialogDelMethodDialog;
	protected HtmlCommandExButton buttonDelMethod;
	protected HtmlPanelDialog dialogAdditionalAddrHelp;
	protected HtmlCommandExButton button3;
	protected HtmlInputHelperSetFocus setFocus1;
	protected HtmlInputHidden startDate;
	protected HtmlJspPanel jspPanelPopOverlayPanel;
	protected HtmlOutputLinkEx linkExSpecialOfferLink;
	protected HtmlInputHidden showOverlayPopUp;
	protected HtmlInputHidden isForceBillMe;
	protected HtmlInputHidden paymentMethodHidden;
	protected HtmlBehaviorKeyPress behaviorKeyPress1;
	protected HtmlInputHidden numberEZPayFreeWeeks;

	/**
	 * @managed-bean true
	 */
	protected PartnerHandler getTrialPartnerHandler() {
		if (trialPartnerHandler == null) {
			trialPartnerHandler = (PartnerHandler) getManagedBean("trialPartnerHandler");
		}
		return trialPartnerHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setTrialPartnerHandler(PartnerHandler trialPartnerHandler) {
		this.trialPartnerHandler = trialPartnerHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected TrialCustomerHandler getTrialCustomerHandler() {
		if (trialCustomerHandler == null) {
			trialCustomerHandler = (TrialCustomerHandler) getManagedBean("trialCustomerHandler");
		}
		return trialCustomerHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setTrialCustomerHandler(TrialCustomerHandler trialCustomerHandler) {
		this.trialCustomerHandler = trialCustomerHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected ShoppingCartHandler getShoppingCartHandler() {
		if (shoppingCartHandler == null) {
			shoppingCartHandler = (ShoppingCartHandler) getManagedBean("shoppingCartHandler");
		}
		return shoppingCartHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setShoppingCartHandler(ShoppingCartHandler shoppingCartHandler) {
		this.shoppingCartHandler = shoppingCartHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected SubscriptionOfferHandler getCurrentOfferHandler() {
		if (currentOfferHandler == null) {
			currentOfferHandler = (SubscriptionOfferHandler) getManagedBean("currentOfferHandler");
		}
		return currentOfferHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setCurrentOfferHandler(SubscriptionOfferHandler currentOfferHandler) {
		this.currentOfferHandler = currentOfferHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected CustomerHandler getCustomerHandler() {
		if (customerHandler == null) {
			customerHandler = (CustomerHandler) getManagedBean("customerHandler");
		}
		return customerHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setCustomerHandler(CustomerHandler customerHandler) {
		this.customerHandler = customerHandler;
	}

	public void onPageLoadBegin(FacesContext facescontext) {
		SubscriptionOfferHandler offer = this.getCurrentOfferHandler();

		HttpServletRequest req = (HttpServletRequest) this.getFacesContext().getExternalContext().getRequest();

		SubscriptionOfferIntf currentOffer = UTCommon.getCurrentOfferVersion2(req);

		offer.setCurrentOffer(currentOffer);
		NewSubscriptionOrderHandler newOrder = this.getNewSubscriptionOrderHandler();

		try {
			if (newOrder.getStartDate() == null) {
				newOrder.setStartDate(currentOffer.getSubscriptionProduct().getEarliestPossibleStartDate().toDate());
			}
		} catch (Exception e) {
			; // ignore
		}

		// HDCONS - 41 - remove MPF processing
		// ///////////////////
		// MPF processing ///
		// ///////////////////
		// following is backward compatibility with all the old MPF pages that
		// are not JSF
		// the MPF processor put the promotion into the MPFV2 attribute. We are
		// simply moving it into JSF managed bean here
		// PremiumPromotionHandler promoHandler =
		// this.getPremiumPromotionHandler();
		// com.usatoday.businessObjects.products.promotions.premiums.PremiumPromotion
		// promotion =
		// (com.usatoday.businessObjects.products.promotions.premiums.PremiumPromotion)req.getSession().getAttribute("MPFV2");
		// req.getSession().removeAttribute("MPFV2");
		// if (promotion != null) {
		// promoHandler.setPromotion(promotion);
		// if (promotion.isHasClubNumber()) {
		// newSubscriptionOrderHandler.setClubNumber(promotion.getClubNumber());
		// }
		// }
		// com.usatoday.esub.subscriptions.MPFOnePageHelperBean mpfHelper =
		// (com.usatoday.esub.subscriptions.MPFOnePageHelperBean)req.getSession().getAttribute("MPFOnePageHelper");
		// req.getSession().removeAttribute("MPFOnePageHelper");
		// if (mpfHelper != null) {
		// promoHandler.setMpfHelper(mpfHelper);
		// SubscriptionTermsIntf tempTerm = null;
		// if (currentOffer != null) {
		// tempTerm = currentOffer.findTermUsingTermLength(mpfHelper.getTerm());
		// if (tempTerm != null) {
		// promoHandler.setTermAsString(tempTerm.getTermAsString());
		// newOrder.setSelectedTerm(tempTerm.getTermAsString());
		// }
		// }
		// if (mpfHelper.getGiftSubscription()) {
		// newOrder.setGiftSubscription(true);
		// }
		// }

		try {
			if (currentOffer != null) {
				// If there is rate code in the URL and if it exists in the offer, use it and remove others
				if (req.getParameter("ratecode") != null && !req.getParameter("ratecode").trim().equals("")) {
					String rateCode = req.getParameter("ratecode");
					if (rateCode != null) {
						rateCode = rateCode.trim().toUpperCase();
						try {
							for (SubscriptionTermsIntf termsBean : currentOffer.getTerms()) {
								if (termsBean.getPiaRateCode() != null && !termsBean.getPiaRateCode().trim().equals("")
										&& termsBean.getPiaRateCode().trim().equals(rateCode)) {
									this.getNewSubscriptionOrderHandler().setSelectedTerm(termsBean.getTermAsString());
									newOrder.setSelectedTerm(termsBean.getTermAsString());
									break;
								}
							}
						} catch (Exception e) {
						}
					}
				} else {
					SubscriptionTermsIntf selTerm = currentOffer.getTerm(newOrder.getSelectedTerm());
					if (selTerm == null) {
						for (SubscriptionTermsIntf term : currentOffer.getTerms()) {
							this.getNewSubscriptionOrderHandler().setSelectedTerm(term.getTermAsString());
							this.getRadioTermsSelection().resetValue();
							break;
						}
					}
					if (currentOffer.isForceBillMe()) {
						newOrder.setPaymentMethod("B");
					}
					this.getIsForceBillMe().resetValue();
					this.getNumberEZPayFreeWeeks().resetValue();
				}
			}
		} catch (Exception e) {
			; // ignore
		}

		// void <method>(FacesContext facescontext)
	}

	public void handleTextEmailAddressRecipientValidate(FacesContext facescontext, UIComponent component, Object object)
			throws javax.faces.validator.ValidatorException {

		String email = (String) object;

		if (email.indexOf('@') == -1) {
			((UIInput) component).setValid(false);

			FacesMessage message = new FacesMessage("Invalid Email Address");
			facescontext.addMessage(component.getClientId(facescontext), message);
		}

		try {
			boolean valid = ContactBO.validateContactEmailAddress(email);
			if (!valid) {
				((UIInput) component).setValid(false);

				FacesMessage message = new FacesMessage("Invalid Email Address");
				facescontext.addMessage(component.getClientId(facescontext), message);
			}
		} catch (Exception e) {
			((UIInput) component).setValid(false);

			FacesMessage message = new FacesMessage("Invalid Email Address" + e.getMessage());
			facescontext.addMessage(component.getClientId(facescontext), message);
		}
		String emailDomain = email.substring(email.indexOf('@') + 1);
		try {
			System.out.println(emailDomain + " has " + doLookup(emailDomain) + " mail servers");
		} catch (Exception e) {
			System.out.println(emailDomain + " : " + e.getMessage());
			((UIInput) component).setValid(false);
			FacesMessage message = new FacesMessage("Invalid Email Address");
			facescontext.addMessage(component.getClientId(facescontext), message);
		}
	}

	static int doLookup(String hostName) throws NamingException {
		Hashtable<String, String> env = new Hashtable<String, String>();
		env.put("java.naming.provider.url", "dns:");
		env.put("java.naming.factory.initial", "com.sun.jndi.dns.DnsContextFactory");
		DirContext ictx = new InitialDirContext(env);
		Attributes attrs = ictx.getAttributes(hostName, new String[] { "MX" });
		Attribute attr = attrs.get("MX");
		if (attr == null)
			return (0);
		return (attr.size());
	}

	/**
	 * Where all the magic happens
	 * 
	 * @return
	 */
	public String doButtonPlaceOrderAction() {
		String responseString = "success";
		NewSubscriptionOrderHandler newSub = this.getNewSubscriptionOrderHandler();

		ShoppingCartHandler sch = this.getShoppingCartHandler();
		ShoppingCartIntf cart = sch.getCart();
		// multi threaded hell
		boolean changeCheckoutStatusInFinallyBlock = true;
		try {
			// if a checkout is in progress send to new error page
			if (sch.isCheckoutStarted() || cart.isCheckoutInProcess()) {
				changeCheckoutStatusInFinallyBlock = false; // don't update the
															// status...the
															// original thread
															// will do it.
				return "checkoutInProgress";
			} else {
				sch.setCheckoutStarted(true);
			}

			cart.setBillingContact(null);
			cart.setDeliveryContact(null);
			cart.setCheckOutErrorMessage(null);
			cart.setPaymentMethod(null);
			cart.clearItems();

			SubscriptionOfferIntf offer = this.getCurrentOfferHandler().getCurrentOffer();

			// HDCONS - 41
			// PremiumPromotionHandler promotionHandler =
			// this.getPremiumPromotionHandler();

			// sometimes mpf doesn't carry forward so update it
			// if (newSub.getSelectedTerm() == null) {
			// try {
			// newSub.setSelectedTerm(promotionHandler.getTermAsString());
			// if (newSub.getClubNumber() == null &&
			// promotionHandler.getPromotion().isHasClubNumber()) {
			// newSub.setClubNumber(promotionHandler.getPromotion().getClubNumber());
			// }
			// }
			// catch (Exception e) {
			// // ignore
			// }
			// }
			if (newSub.getSelectedTerm() == null || offer == null || offer.getTerm(newSub.getSelectedTerm()) == null) {
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Please choose a subscription Term.", null);

				context.addMessage(null, facesMsg);
				responseString = "failure";
				return responseString;
			}

			try {
				HttpServletRequest request = (HttpServletRequest) this.getFacesContext().getExternalContext().getRequest();

				if (!newSub.isValidDeliveryEmailAndDeliveryConfirmationEmail()) {
					FacesContext context = FacesContext.getCurrentInstance();
					FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
							"Email Address does not match confirmation email address.", null);

					context.addMessage(null, facesMsg);
					responseString = "failure";
					return responseString;
				}

				// /// Delivery Contact ///////////
				ContactBO deliveryContact = new ContactBO();

				StringBuilder phone = new StringBuilder(newSub.getDeliveryPhoneAreaCode().trim());
				phone.append(newSub.getDeliveryPhoneExchange().trim());
				phone.append(newSub.getDeliveryPhoneExtension().trim());

				deliveryContact.setHomePhone(phone.toString());
				if (newSub.getDeliveryWorkPhoneAreaCode() != null && newSub.getDeliveryWorkPhoneAreaCode().trim().length() == 3) {
					phone = new StringBuilder(newSub.getDeliveryWorkPhoneAreaCode());
					phone.append(newSub.getDeliveryWorkPhoneExchange());
					phone.append(newSub.getDeliveryWorkPhoneExtension());
					if (phone.length() == 10) {
						deliveryContact.setBusinessPhone(phone.toString());
					}
				}

				deliveryContact.setEmailAddress(newSub.getDeliveryEmailAddress());
				deliveryContact.setFirstName(newSub.getDeliveryFirstName().trim());
				deliveryContact.setLastName(newSub.getDeliveryLastName().trim());
				if (newSub.getDeliveryCompanyName() != null) {
					deliveryContact.setFirmName(newSub.getDeliveryCompanyName().trim());
				}

				if (deliveryContact.getFirstName().length() == 0 || deliveryContact.getLastName().length() == 0) {
					FacesContext context = FacesContext.getCurrentInstance();
					FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
							"Delivery First and Last Name are required.", null);
					context.addMessage(null, facesMsg);
					responseString = "failure";
					return responseString;
				}

				UIAddressBO dAddress = new UIAddressBO();
				dAddress.setAddress1(newSub.getDeliveryAddress1().trim());
				dAddress.setAddress2(newSub.getDeliveryAddress2().trim());
				dAddress.setAptSuite(newSub.getDeliveryAptSuite().trim());
				dAddress.setCity(newSub.getDeliveryCity().trim());
				// Check billing state
				if (newSub.getDeliveryState() == null || newSub.getDeliveryState().trim().equals("")
						|| newSub.getDeliveryState().trim().equals("NONE")) {
					FacesContext context = FacesContext.getCurrentInstance();
					FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Delivery State is Required", null);
					context.addMessage(null, facesMsg);
					responseString = "failure";
					return responseString;
				} else {
					dAddress.setState(newSub.getDeliveryState());
				}

				dAddress.setZip(newSub.getDeliveryZipCode().trim());

				deliveryContact.setUIAddress(dAddress);

				OrderIntf lastOrder = sch.getLastOrder();
				if (lastOrder != null) {
					// possible refresh of page.
					if (lastOrder.getDeliveryContact().getUIAddress().equals(dAddress)) {
						FacesContext context = FacesContext.getCurrentInstance();
						FacesMessage facesMsg = new FacesMessage(
								FacesMessage.SEVERITY_INFO,
								"Duplicate form submission detected. An order with this delivery address has already been placed. ",
								null);

						context.addMessage(null, facesMsg);
						responseString = "duplicate";
						return responseString;
					}
				}

				if (!deliveryContact.validateContact()) {
					FacesContext context = FacesContext.getCurrentInstance();
					FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
							"We are unable to validate the delivery address. Please verify the address you entered.", null);

					context.addMessage(null, facesMsg);
					responseString = "failure";
					return responseString;
				} else {
					// update the bean with the code1 address
					try {
						UIAddressIntf correctedAddress = deliveryContact.getPersistentAddress().convertToUIAddress();

						if (correctedAddress.getAddress1().length() > 0) {
							// Check for disallowed addresses
							if (offer.getPubCode().equals("UT") && correctedAddress.getState().equals("HI")) {
								FacesContext context = FacesContext.getCurrentInstance();
								FacesMessage facesMsg = new FacesMessage(
										FacesMessage.SEVERITY_INFO,
										"Thank you for your interest in subscribing to USA TODAY! All subscriptions to USA TODAY delivered in the state of Hawaii are handled by our partner, the Honolulu Star-Advertiser. "
												+ "For information about receiving delivery of USA TODAY in Hawaii, including pricing (which may vary from what is shown here), "
												+ "please contact the Honolulu Star-Advertiser's Customer Service Department by calling 808-538-NEWS (6397) from 5:30am to 5:00 pm Monday-Friday and 6:30am to 10:00 am on weekends (all times HST).",
										null);

								context.addMessage(null, facesMsg);
								newSub.setDeliveryState(correctedAddress.getState());
								responseString = "failure";
								return responseString;
							}

							newSub.setDeliveryAddress1(correctedAddress.getAddress1());
							newSub.setDeliveryAddress2(correctedAddress.getAddress2());
							newSub.setDeliveryAptSuite(correctedAddress.getAptSuite());
							newSub.setDeliveryCity(correctedAddress.getCity());
							newSub.setDeliveryZipCode(correctedAddress.getZip());
						}
					} catch (Exception e) {
						// ignore it
						if (UsaTodayConstants.VALIDATE_POSTAL_ADDRESSES) {
							System.out
									.println("JSF One Page OrderEntry::Exception setting up corrected address: " + e.getMessage());
						}
					}
				}

				cart.setDeliveryContact(deliveryContact);

				// ///// BILLING Contact /////////////
				ContactBO billingContact = null;
				if (newSub.isBillingDifferentThanDelivery()) {
					billingContact = new ContactBO();

					// Check for billing email address
					if (newSub.getBillingEmailAddress() == null || newSub.getBillingEmailAddress().trim().equals("")) {
						FacesContext context = FacesContext.getCurrentInstance();
						FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Billing Email Address is Required",
								null);
						context.addMessage(null, facesMsg);
						responseString = "failure";
						return responseString;
					} else {
						billingContact.setEmailAddress(newSub.getBillingEmailAddress());
					}
					// Check for billing first name
					if (newSub.getBillingFirstName() == null || newSub.getBillingFirstName().trim().equals("")) {
						FacesContext context = FacesContext.getCurrentInstance();
						FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Billing First Name is Required", null);
						context.addMessage(null, facesMsg);
						responseString = "failure";
						return responseString;
					} else {
						billingContact.setFirstName(newSub.getBillingFirstName());
					}
					// Check for billing last name
					if (newSub.getBillingLastName() == null || newSub.getBillingLastName().trim().equals("")) {
						FacesContext context = FacesContext.getCurrentInstance();
						FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Billing Last Name is Required", null);
						context.addMessage(null, facesMsg);
						responseString = "failure";
						return responseString;
					} else {
						billingContact.setLastName(newSub.getBillingLastName());
					}

					if (newSub.getBillingCompanyName() != null) {
						billingContact.setFirmName(newSub.getBillingCompanyName());
					}

					UIAddressBO bAddress = new UIAddressBO();
					// Check billing address line
					if (newSub.getBillingAddress1() == null || newSub.getBillingAddress1().trim().equals("")) {
						FacesContext context = FacesContext.getCurrentInstance();
						FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Billing Address is Required", null);
						context.addMessage(null, facesMsg);
						responseString = "failure";
						return responseString;
					} else {
						bAddress.setAddress1(newSub.getBillingAddress1());
					}
					// Check billing city
					bAddress.setAddress2(newSub.getBillingAddress2());
					bAddress.setAptSuite(newSub.getBillingAptSuite());
					if (newSub.getBillingCity() == null || newSub.getBillingCity().trim().equals("")) {
						FacesContext context = FacesContext.getCurrentInstance();
						FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Billing City is Required", null);
						context.addMessage(null, facesMsg);
						responseString = "failure";
						return responseString;
					} else {
						bAddress.setCity(newSub.getBillingCity());
					}
					// Check billing state
					if (newSub.getBillingState() == null || newSub.getBillingState().trim().equals("")
							|| newSub.getBillingState().trim().equals("NONE")) {
						FacesContext context = FacesContext.getCurrentInstance();
						FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Billing State is Required", null);
						context.addMessage(null, facesMsg);
						responseString = "failure";
						return responseString;
					} else {
						bAddress.setState(newSub.getBillingState());
					}
					// Check billing zip
					if (newSub.getBillingZipCode() == null || newSub.getBillingZipCode().trim().equals("")) {
						FacesContext context = FacesContext.getCurrentInstance();
						FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Billing Zip Code is Required", null);
						context.addMessage(null, facesMsg);
						responseString = "failure";
						return responseString;
					} else {
						bAddress.setZip(newSub.getBillingZipCode());
					}
					// Check billing phone number. Not required, but must enter
					// complete phone number
					if (!newSub.getBillingPhoneAreaCode().trim().equals("") || !newSub.getBillingPhoneExchange().trim().equals("")
							|| !newSub.getBillingPhoneExtension().trim().equals("")) {
						if (newSub.getBillingPhoneAreaCode().trim().equals("")
								|| newSub.getBillingPhoneExchange().trim().equals("")
								|| newSub.getBillingPhoneExtension().trim().equals("")) {
							FacesContext context = FacesContext.getCurrentInstance();
							FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
									"Enter Complete Billing Phone Number.", null);

							context.addMessage(null, facesMsg);
							responseString = "failure";
							return responseString;
						} else {
							phone = new StringBuilder(newSub.getBillingPhoneAreaCode());
							phone.append(newSub.getBillingPhoneExchange());
							phone.append(newSub.getBillingPhoneExtension());
							billingContact.setHomePhone(phone.toString());
						}
					}

					billingContact.setUIAddress(bAddress);

					if (!billingContact.validateContact()) {
						FacesContext context = FacesContext.getCurrentInstance();
						FacesMessage facesMsg = new FacesMessage(
								FacesMessage.SEVERITY_INFO,
								"We are unable to validate the billing address. Please verify that the information entered is correct.",
								null);

						context.addMessage(null, facesMsg);
						responseString = "failure";
						return responseString;
					} else {
						// update the bean with the code1 address
						try {
							UIAddressIntf correctedAddress = billingContact.getPersistentAddress().convertToUIAddress();

							if (correctedAddress.getAddress1().length() > 0) {
								newSub.setBillingAddress1(correctedAddress.getAddress1());
								newSub.setBillingAddress2(correctedAddress.getAddress2());
								newSub.setBillingAptSuite(correctedAddress.getAptSuite());
								newSub.setBillingCity(correctedAddress.getCity());
								newSub.setBillingZipCode(correctedAddress.getZip());
							}
						} catch (Exception e) {
							// ignore it
							if (UsaTodayConstants.VALIDATE_POSTAL_ADDRESSES) {
								System.out.println("JSF One Page OrderEntry::Exception setting up corrected billing address: "
										+ e.getMessage());
							}
						}
					}
					cart.setBillingContact(billingContact);
				}

				SubscriptionProductIntf product = SubscriptionProductBO.getSubscriptionProduct(offer.getPubCode());

				product.applyOffer(offer);
				SubscriptionTermsIntf selectedTerm = offer.getTerm(newSub.getSelectedTerm());

				product.applyTerm(selectedTerm);

				SubscriptionOrderItemBO item = new SubscriptionOrderItemBO();

				item.setDeliveryZip(deliveryContact.getUiAddress().getZip());
				item.setProduct(product);

				item.setQuantity(newSub.getQuantity());

				item.setGiftItem(false);
				/*
				 * if (newSub.isBillingDifferentThanDelivery()) { item.setGiftItem(true); }
				 */
				if (newSub.isGiftSubscription() && newSub.isBillingDifferentThanDelivery()) {
					item.setGiftItem(true);
				}
				item.setOneTimeBill(false);
				item.setChoosingEZPay(false);

				if (newSub.getIsChoseBillMe() && (selectedTerm.requiresEZPAY() || offer.isForceEZPay())) {
					// change user selection to Credit Card payment
					newSub.setPaymentMethod("CC");
					FacesContext context = FacesContext.getCurrentInstance();
					FacesMessage facesMsg = new FacesMessage(
							FacesMessage.SEVERITY_INFO,
							"The selected subscription term requires that you sign up for the EZ-PAY renewal option. Bill Me, is not available for EZ-PAY required term lengths.",
							null);

					context.addMessage(null, facesMsg);
					responseString = "failure";
					return responseString;

				}
				if (newSub.getIsChoseBillMe()) {
					item.setChoosingEZPay(false);
				} else {
					// credit card ... paypal in future

					// process renewal method for non bill me's
					if (!newSub.isBillingDifferentThanDelivery() || !newSub.isGiftSubscription()) { // New
																									// subscription
																									// renewal
																									// method
						if (newSub.getRenewalMethod().equalsIgnoreCase(SubscriptionOrderItemIntf.EZPAY)) {
							item.setChoosingEZPay(true);
						} else {
							// EZ-Pay may be required
							if (selectedTerm.requiresEZPAY() || offer.isForceEZPay()) {
								FacesContext context = FacesContext.getCurrentInstance();
								FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
										"The selected subscription term requires that you sign up for the EZ-PAY renewal option.",
										null);

								context.addMessage(null, facesMsg);
								responseString = "failure";
								return responseString;
							}
						}
					} else { // Gift transaction renewal method
						if (newSub.getRenewalMethodGift().equalsIgnoreCase(SubscriptionOrderItemIntf.EZPAY)) {
							item.setChoosingEZPay(true);
						} else {
							// EZ-Pay may be required
							if (selectedTerm.requiresEZPAY() || offer.isForceEZPay()) {
								FacesContext context = FacesContext.getCurrentInstance();
								FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
										"The selected subscription term requires that you sign up for the EZ-PAY renewal option.",
										null);

								context.addMessage(null, facesMsg);
								responseString = "failure";
								return responseString;
							}
						}
						if (newSub.getRenewalMethodGift().equalsIgnoreCase(SubscriptionOrderItemIntf.ONE_TIME_BILL)) {
							item.setOneTimeBill(true);
						}
					}
				}

				item.setDeliveryEmailAddress(deliveryContact.getEmailAddress());
				item.setKeyCode(offer.getKeyCode());

				String startDate = new DateTime().toString("yyyyMMdd");
				if (newSub.getStartDate() != null) {
					DateTime requestedStart = new DateTime(newSub.getStartDate());

					// if requested date is in future use it, otherwise use
					// today
					if (requestedStart.isAfterNow()) {
						startDate = requestedStart.toString("yyyyMMdd");
					}

				}

				item.setStartDate(startDate);
				item.setSelectedTerm(selectedTerm);
				item.setOffer(offer);

				// HDCONS - 41
				// if (promotionHandler != null &&
				// promotionHandler.getPromotion() != null) {
				// item.setPromotionalItems(promotionHandler.getPromotion());
				// }

				// Gannett Unit checks only apply to print products
				// HDCONS - 44 removed GU / ezpay handling

				// default delivery method to Carrier
				item.setDeliveryMethod("C");
				newSub.setDeliveryMethodText("Morning Delivery");

				// Delivery Method checks
				if (product.isElectronicDelivery()) {
					// for electronic just set to m
					item.setDeliveryMethod("M");
					item.setDeliveryMethodCheck(false);
					newSub.setDeliveryMethodText("Electronic Delivery");
				} else {
					item.setDeliveryMethodCheck(false);

					if (newSub.isGannettUnit()) {
						// always set Gannett Units to C
						item.setDeliveryMethod("C");
						newSub.setDeliveryMethodText("Morning Delivery");
						item.setDeliveryMethodCheck(com.usatoday.util.constants.UsaTodayConstants.DELIVERY_NOTIFY);
					} else {
						DeliveryMethodCode1ValidationBO deliveryMethodBO = new DeliveryMethodCode1ValidationBO();
						// Get delivery Method, if active
						try {
							item.setDeliveryMethodCheck(com.usatoday.util.constants.UsaTodayConstants.DELIVERY_NOTIFY);
							PromotionSet currentOfferPromotionSet = currentOfferHandler.getCurrentOffer().getPromotionSet();

							if (currentOfferPromotionSet != null && currentOfferPromotionSet.getDeliveryNotification() != null
									&& currentOfferPromotionSet.getDeliveryNotification().getFulfillText() != null
									&& !currentOfferPromotionSet.getDeliveryNotification().getFulfillText().trim().equals("")) {
								if (currentOfferPromotionSet.getDeliveryNotification().getFulfillText().equals("ON")) {
									item.setDeliveryMethodCheck(true);
								} else {
									item.setDeliveryMethodCheck(false);
								}
							}

							String deliveryMethod = "";

							if (item.isDeliveryMethodCheck()) {

								// if (AS400CurrentStatus.getJdbcActive()) {
								deliveryMethod = deliveryMethodBO.determineDeliveryMethod(offer.getPubCode(),
										deliveryContact.getUiAddress());
								if (deliveryMethod != null) {
									item.setDeliveryMethod(deliveryMethod);
									if (deliveryMethod.equalsIgnoreCase("M")) {
										newSub.setDeliveryMethodText("Mail Delivery");
									} else if (deliveryMethod.equalsIgnoreCase("C")) {
										newSub.setDeliveryMethodText("Morning Delivery");
									}
								}
								/*
								 * } else { item.setDeliveryMethod("C"); newSub.setDeliveryMethodText ("Morning Prefered"); }
								 */} else {
								newSub.setDeliveryMethodText(null);
							}
						} catch (Exception e) {
							System.out.println("JSF One Page OrderEntry:: Could not determine delivery method, because: "
									+ e.getMessage());
						}
					} // end if not a Gannett Unit
				} // end else print product

				if (newSub.getClubNumber() != null && newSub.getClubNumber().trim().length() > 0) {
					item.setClubNumber(newSub.getClubNumber().trim());
				}

				// check if paying by credit card or bill me
				PaymentMethodIntf payment = null;
				if (newSub.getIsChoseBillMe()) {
					payment = new InvoicePaymentMethodBO();
				} else {
					// Setup Payment information
					CreditCardPaymentMethodBO ccpayment = new CreditCardPaymentMethodBO();
					// Check credit card number
					if (newSub.getCreditCardNumber() == null || newSub.getCreditCardNumber().equals("")) {
						FacesContext context = FacesContext.getCurrentInstance();
						FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
								"Please select a Valid Credit Card Number.", null);
						context.addMessage(null, facesMsg);
						responseString = "failure";
						return responseString;
					} else {
						try {
							ccpayment.setCardNumber(newSub.getCreditCardNumber());

						} catch (UsatException e) {
							FacesContext context = FacesContext.getCurrentInstance();
							FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
									"The credit card number you entered is not recognized. Please check the number and try again.",
									null);

							context.addMessage(null, facesMsg);
							responseString = "failure";
							return responseString;
						}
					}
					if (newSub.isBillingDifferentThanDelivery()) {
						ccpayment.setNameOnCard(billingContact.getFirstName() + " " + billingContact.getLastName());
					} else {
						ccpayment.setNameOnCard(deliveryContact.getFirstName() + " " + deliveryContact.getLastName());
					}

					// Check credit card CVV number
					if (newSub.getCreditCardCVVNumber() == null || newSub.getCreditCardCVVNumber().trim().equals("")) {

						PromotionIntf cvvRequired = offer.getPromotionSet().getCVVRequiredOverride();

						// Add check if keycode allows skipping of CVV -
						// basically if the promo is non null then CVV is
						// optional, if null, it's required.
						if (cvvRequired == null) {

							FacesContext context = FacesContext.getCurrentInstance();
							FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
									"Please enter a valid Credit Card Verification Number.", null);

							context.addMessage(null, facesMsg);
							responseString = "failure";
							return responseString;
						}
					} else {
						ccpayment.setCVV(newSub.getCreditCardCVVNumber());
					}

					// Check credit card expiration
					String expirationYear = newSub.getCreditCardExpirationYear();
					String expirationMonth = newSub.getCreditCardExpirationMonth();
					if (expirationMonth == null || expirationMonth.equalsIgnoreCase("-1") || expirationYear == null
							|| expirationYear.equalsIgnoreCase("-1")) {
						FacesContext context = FacesContext.getCurrentInstance();
						FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
								"Please select a valid Credit Card Expiration Date.", null);

						context.addMessage(null, facesMsg);
						responseString = "failure";
						return responseString;
					} else {
						ccpayment.setExpirationMonth(expirationMonth);
						ccpayment.setExpirationYear(expirationYear);
					}

					payment = ccpayment;
				}
				// add the item now that all validations are complete.
				cart.addItem(item);

				cart.setPaymentMethod(payment);

				// CJ Tracking boolean (used to determine if image pixel should
				// be displayed. in this case yes if 0.0 charge
				sch.setIsZeroDollarAmount(false);
				sch.setIsNotZeroDollar(true);
				if (cart.getTotal() == 0.0) {
					sch.setIsZeroDollarAmount(true);
					sch.setIsNotZeroDollar(false);
				}

				CheckOutService checkout = new CheckOutService();

				TrialInstanceIntf tCust = null;
				if (this.getTrialCustomerHandler().getTrialCustomer() != null) {
					tCust = this.getTrialCustomerHandler().getTrialCustomer().getCurrentInstance();
				}
				// Check for existing accounts for this email.
				String password = null;
				boolean sameEmails = true;
				if (newSub.isBillingDifferentThanDelivery() && billingContact.getEmailAddress() != null
						&& billingContact.getEmailAddress().length() > 0
						&& !deliveryContact.getEmailAddress().equalsIgnoreCase(billingContact.getEmailAddress())) {
					sameEmails = false;
				}
				String sourceEmail = null;
				// Determine if any accounts exist, if so, use that password.
				// otherwise create a new one.
				try {
					sourceEmail = deliveryContact.getEmailAddress();
					if (!sameEmails) {
						sourceEmail = billingContact.getEmailAddress();
					}
					Collection<EmailRecordIntf> existingEmails = EmailRecordBO.getEmailRecordsForEmailAddress(sourceEmail);
					if (existingEmails.size() > 0) {

						password = "Already On File";						

/*						boolean useExistingPassword = true;
						try {
							if (tCust != null && tCust.getContactInformation().getPassword() != null
									&& tCust.getContactInformation().getPassword().trim().length() > 0) {
								password = tCust.getContactInformation().getPassword().trim();
								useExistingPassword = false;
							}
						} catch (Exception ignoree) {
							useExistingPassword = true;
							password = null;
						}

						if (useExistingPassword) {
							for (EmailRecordIntf anEmail : existingEmails) {
								if (anEmail.getPassword() != null && anEmail.getPassword().trim().length() > 0) {
									password = anEmail.getPassword();
									break;
								}
							}
						}

						// no password set up on existing accounts - update them
						if (password == null) {
							password = CustomerBO.generateNewPassword();
							for (EmailRecordIntf anEmail : existingEmails) {
								anEmail.setPassword(password);
								try {
									EmailRecordBO eBO = (EmailRecordBO) anEmail;
									eBO.save();
								} catch (Exception ue) {
									; // ignore if update fails
								}
							}
						} else {
							// update them with new / current password
							for (EmailRecordIntf anEmail : existingEmails) {
								anEmail.setPassword(password);
								try {
									EmailRecordBO eBO = (EmailRecordBO) anEmail;
									eBO.save();
								} catch (Exception ue) {
									; // ignore if update fails
								}
							}
						}
*/					} else {
						// check if user entered a password (trial subscription)
						try {
							if (tCust != null && tCust.getContactInformation().getPassword() != null
									&& tCust.getContactInformation().getPassword().trim().length() > 0) {
								password = tCust.getContactInformation().getPassword().trim();
							} else {
								password = CustomerBO.generateNewPassword();
							}
						} catch (Exception ingnoreE) {
							password = CustomerBO.generateNewPassword();
						}
					}
				} catch (Exception bummer) {
					password = CustomerBO.generateNewPassword();
				}

				// set passwords based on order email scenario
				if (sameEmails) {
					if (billingContact != null) {
						billingContact.setPassword(password);
					}
					deliveryContact.setPassword(password);
				} else {
					billingContact.setPassword(password);
					deliveryContact.setPassword(CustomerBO.generateNewPassword());
				}
				// Done checking for existing passwords

				cart.setClientIPAddress(request.getRemoteAddr());

				OrderIntf order = checkout.checkOutShoppingCart(cart, UsaTodayConstants.BATCH_PROCESS_CC_NETWORK_FAILURES);

				sch.setLastOrder(order);

				try {
					if (this.getInterimCustomerHandler().getInterimCust() != null) {

						this.getInterimCustomerHandler().getInterimCust().delete(this.getInterimCustomerHandler().getInterimCust());
						this.getInterimCustomerHandler().setInterimCust(null);

					}
				} catch (Exception e) {
					; // ignore for now
				}
				// clear order entry handler
				newSub.resetForNewOrder();

				// clear MPF
				// promotionHandler.reset();

				// for backward compatibility with the Omniture tagging.
				request.getSession().setAttribute(UTCommon.SESSION_FIELD, order);

				String trialEmail = null;
				if (tCust != null) {

					// update trial customer record.
					try {
						trialEmail = tCust.getContactInformation().getEmailAddress();

						if (trialEmail.equalsIgnoreCase(order.getDeliveryContact().getEmailAddress())
								|| trialEmail.equalsIgnoreCase(order.getBillingContact().getEmailAddress())) {
							tCust.setIsSubscribed(true);
							tCust.save();
						}

					} catch (Exception e) {
						System.out.println("OrderEntry (Electronic): Failed to update trial subscriber record for email: "
								+ trialEmail);
						e.printStackTrace();
					}

					// clear out the trial customer information from the
					// session.
					this.getTrialCustomerHandler().setTrialCustomer(null);
				}

				try {

					order.sendSubscriptionConfirmationEmail();

				} catch (UsatException usex) {
					; // ignore for now
				}

				try {

					// update session with customer information
					CustomerIntf customer = CustomerBO.loginCustomerByEmailAddress(sourceEmail);
					CustomerHandler ch = this.getCustomerHandler();
					ch.setCustomer(customer);
					ch.setAuthenticated(true);
					ch.setCookieAuthenticated(true);

				} catch (UsatException usex) {
					System.out.println("OrderEntry: Failed to login customer with email address and create Customer object: "
							+ trialEmail);
				}

			} catch (UsatException ue) {
				if (cart.getCheckOutErrorMessage() != null && !cart.getCheckOutErrorMessage().trim().equals("")) {
					FacesContext context = FacesContext.getCurrentInstance();
					FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, cart.getCheckOutErrorMessage(), null);

					context.addMessage(null, facesMsg);
				} else {
					FacesContext context = FacesContext.getCurrentInstance();
					FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, ue.getMessage(), null);

					context.addMessage(null, facesMsg);
				}
				// cart.clearItems();

				responseString = "failure";
			} catch (Exception e) {
				// if (cart != null) {
				// cart.clearItems();
				// }
				// Report the error using the appropriate name and ID.
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
						"An unexpected error occurred during checkout: " + e.getMessage(), null);

				context.addMessage(null, facesMsg);
				responseString = "failure";

			}
		} // end outter most try
		catch (Exception e) {
			e.printStackTrace();
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "An unexpected error occurred during checkout: "
					+ e.getMessage(), null);

			context.addMessage(null, facesMsg);
			responseString = "failure";
		} finally {
			if (changeCheckoutStatusInFinallyBlock) {
				// clear the flag that we are in this method
				sch.setCheckoutStarted(false);
			}
		}
		return responseString;
	}

	public void onPageLoadEnd(FacesContext facescontext) {

		// void <method>(FacesContext facescontext)
	}

	protected HtmlScriptCollector getScriptCollectorTemplateLogoutCollector() {
		if (scriptCollectorTemplateLogoutCollector == null) {
			scriptCollectorTemplateLogoutCollector = (HtmlScriptCollector) findComponentInRoot("scriptCollectorTemplateLogoutCollector");
		}
		return scriptCollectorTemplateLogoutCollector;
	}

	protected HtmlForm getFormLogoutTemplateAction() {
		if (formLogoutTemplateAction == null) {
			formLogoutTemplateAction = (HtmlForm) findComponentInRoot("formLogoutTemplateAction");
		}
		return formLogoutTemplateAction;
	}

	protected HtmlOutputLinkEx getLinkExLogOutLinkTemplate1() {
		if (linkExLogOutLinkTemplate1 == null) {
			linkExLogOutLinkTemplate1 = (HtmlOutputLinkEx) findComponentInRoot("linkExLogOutLinkTemplate1");
		}
		return linkExLogOutLinkTemplate1;
	}

	protected HtmlOutputText getTextLogoutLinkTemplateLabel1() {
		if (textLogoutLinkTemplateLabel1 == null) {
			textLogoutLinkTemplateLabel1 = (HtmlOutputText) findComponentInRoot("textLogoutLinkTemplateLabel1");
		}
		return textLogoutLinkTemplateLabel1;
	}

	protected HtmlScriptCollector getScriptCollectorTemplateCollectorLeftNav() {
		if (scriptCollectorTemplateCollectorLeftNav == null) {
			scriptCollectorTemplateCollectorLeftNav = (HtmlScriptCollector) findComponentInRoot("scriptCollectorTemplateCollectorLeftNav");
		}
		return scriptCollectorTemplateCollectorLeftNav;
	}

	protected HtmlPanelGrid getGridNavigationTopGrid() {
		if (gridNavigationTopGrid == null) {
			gridNavigationTopGrid = (HtmlPanelGrid) findComponentInRoot("gridNavigationTopGrid");
		}
		return gridNavigationTopGrid;
	}

	protected HtmlPanelGrid getGridLeftNavGroup1Grid() {
		if (gridLeftNavGroup1Grid == null) {
			gridLeftNavGroup1Grid = (HtmlPanelGrid) findComponentInRoot("gridLeftNavGroup1Grid");
		}
		return gridLeftNavGroup1Grid;
	}

	protected HtmlOutputLinkEx getLinkExNavGroup2International() {
		if (linkExNavGroup2International == null) {
			linkExNavGroup2International = (HtmlOutputLinkEx) findComponentInRoot("linkExNavGroup2International");
		}
		return linkExNavGroup2International;
	}

	protected HtmlOutputText getTextNavGroup1LinkLabelInternational() {
		if (textNavGroup1LinkLabelInternational == null) {
			textNavGroup1LinkLabelInternational = (HtmlOutputText) findComponentInRoot("textNavGroup1LinkLabelInternational");
		}
		return textNavGroup1LinkLabelInternational;
	}

	protected HtmlOutputText getTextNavGroup1LinkLabelKindle() {
		if (textNavGroup1LinkLabelKindle == null) {
			textNavGroup1LinkLabelKindle = (HtmlOutputText) findComponentInRoot("textNavGroup1LinkLabelKindle");
		}
		return textNavGroup1LinkLabelKindle;
	}

	protected HtmlOutputText getTextNavGroup1LinkLabel1() {
		if (textNavGroup1LinkLabel1 == null) {
			textNavGroup1LinkLabel1 = (HtmlOutputText) findComponentInRoot("textNavGroup1LinkLabel1");
		}
		return textNavGroup1LinkLabel1;
	}

	protected HtmlOutputText getTextNavGroup1LinkLabelDotCom() {
		if (textNavGroup1LinkLabelDotCom == null) {
			textNavGroup1LinkLabelDotCom = (HtmlOutputText) findComponentInRoot("textNavGroup1LinkLabelDotCom");
		}
		return textNavGroup1LinkLabelDotCom;
	}

	protected HtmlOutputText getTextNavGroup1LinkLabelFAQ() {
		if (textNavGroup1LinkLabelFAQ == null) {
			textNavGroup1LinkLabelFAQ = (HtmlOutputText) findComponentInRoot("textNavGroup1LinkLabelFAQ");
		}
		return textNavGroup1LinkLabelFAQ;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link0() {
		if (linkExNavArea2Link0 == null) {
			linkExNavArea2Link0 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link0");
		}
		return linkExNavArea2Link0;
	}

	protected HtmlOutputText getTextNavArea2Link0() {
		if (textNavArea2Link0 == null) {
			textNavArea2Link0 = (HtmlOutputText) findComponentInRoot("textNavArea2Link0");
		}
		return textNavArea2Link0;
	}

	protected HtmlOutputText getTextNavArea2Link1() {
		if (textNavArea2Link1 == null) {
			textNavArea2Link1 = (HtmlOutputText) findComponentInRoot("textNavArea2Link1");
		}
		return textNavArea2Link1;
	}

	protected HtmlOutputText getTextNavArea2Link2() {
		if (textNavArea2Link2 == null) {
			textNavArea2Link2 = (HtmlOutputText) findComponentInRoot("textNavArea2Link2");
		}
		return textNavArea2Link2;
	}

	protected HtmlOutputText getTextNavArea2Link3() {
		if (textNavArea2Link3 == null) {
			textNavArea2Link3 = (HtmlOutputText) findComponentInRoot("textNavArea2Link3");
		}
		return textNavArea2Link3;
	}

	protected HtmlOutputText getTextNavArea2Link4() {
		if (textNavArea2Link4 == null) {
			textNavArea2Link4 = (HtmlOutputText) findComponentInRoot("textNavArea2Link4");
		}
		return textNavArea2Link4;
	}

	protected HtmlOutputText getTextNavArea2Link5() {
		if (textNavArea2Link5 == null) {
			textNavArea2Link5 = (HtmlOutputText) findComponentInRoot("textNavArea2Link5");
		}
		return textNavArea2Link5;
	}

	protected HtmlPanelGrid getGridNavigationOuterGrid() {
		if (gridNavigationOuterGrid == null) {
			gridNavigationOuterGrid = (HtmlPanelGrid) findComponentInRoot("gridNavigationOuterGrid");
		}
		return gridNavigationOuterGrid;
	}

	protected HtmlOutputLinkEx getLinkExNavGroup2Kindle() {
		if (linkExNavGroup2Kindle == null) {
			linkExNavGroup2Kindle = (HtmlOutputLinkEx) findComponentInRoot("linkExNavGroup2Kindle");
		}
		return linkExNavGroup2Kindle;
	}

	protected HtmlOutputLinkEx getLinkExNavGroup1SubscriberServices() {
		if (linkExNavGroup1SubscriberServices == null) {
			linkExNavGroup1SubscriberServices = (HtmlOutputLinkEx) findComponentInRoot("linkExNavGroup1SubscriberServices");
		}
		return linkExNavGroup1SubscriberServices;
	}

	protected HtmlOutputLinkEx getLinkExNavGroup2DotCom() {
		if (linkExNavGroup2DotCom == null) {
			linkExNavGroup2DotCom = (HtmlOutputLinkEx) findComponentInRoot("linkExNavGroup2DotCom");
		}
		return linkExNavGroup2DotCom;
	}

	protected HtmlOutputLinkEx getLinkExNavGroupFAQ() {
		if (linkExNavGroupFAQ == null) {
			linkExNavGroupFAQ = (HtmlOutputLinkEx) findComponentInRoot("linkExNavGroupFAQ");
		}
		return linkExNavGroupFAQ;
	}

	protected HtmlPanelGrid getGridLeftNavGroup2Grid() {
		if (gridLeftNavGroup2Grid == null) {
			gridLeftNavGroup2Grid = (HtmlPanelGrid) findComponentInRoot("gridLeftNavGroup2Grid");
		}
		return gridLeftNavGroup2Grid;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link1() {
		if (linkExNavArea2Link1 == null) {
			linkExNavArea2Link1 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link1");
		}
		return linkExNavArea2Link1;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link2() {
		if (linkExNavArea2Link2 == null) {
			linkExNavArea2Link2 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link2");
		}
		return linkExNavArea2Link2;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link3() {
		if (linkExNavArea2Link3 == null) {
			linkExNavArea2Link3 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link3");
		}
		return linkExNavArea2Link3;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link4() {
		if (linkExNavArea2Link4 == null) {
			linkExNavArea2Link4 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link4");
		}
		return linkExNavArea2Link4;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link5() {
		if (linkExNavArea2Link5 == null) {
			linkExNavArea2Link5 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link5");
		}
		return linkExNavArea2Link5;
	}

	protected HtmlPanelGrid getGridNavigationBottomGrid() {
		if (gridNavigationBottomGrid == null) {
			gridNavigationBottomGrid = (HtmlPanelGrid) findComponentInRoot("gridNavigationBottomGrid");
		}
		return gridNavigationBottomGrid;
	}

	public String doButtonGetDeliveryMethodButtonAction() {
		// Type Java code that runs when the component is clicked

		String responseString = "failure";

		SubscriptionOfferHandler currentOfferHandler = this.getCurrentOfferHandler();
		SubscriptionOfferIntf offer = currentOfferHandler.getCurrentOffer();

		NewSubscriptionOrderHandler newSub = this.getNewSubscriptionOrderHandler();
		// preserve selected term
		try {
			String term = this.getRadioTermsSelection().getSubmittedValue().toString();
			newSub.setSelectedTerm(term);
		} catch (Exception e) {
			// mpf's don't have radio buttons so ignore the exception.
		}

		DeliveryMethodCode1ValidationBO deliveryMethodBO = new DeliveryMethodCode1ValidationBO();
		// Get delivery Method, if active
		try {

			// String delvFName =
			// getTextDeliveryFName().getSubmittedValue().toString();;

			String address1 = getTextDeliveryAddress1Text().getSubmittedValue().toString();

			String aptSte = getTextDeliveryAptSuite().getSubmittedValue().toString();
			String city = getTextDeliveryCity().getSubmittedValue().toString();
			String state = getMenuDeliveryState().getSubmittedValue().toString();
			String zip = getTextDeliveryZip().getSubmittedValue().toString();

			// First validate state
			if (state.equals("HI")) {
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage facesMsg = new FacesMessage(
						FacesMessage.SEVERITY_INFO,
						"Thank you for your interest in subscribing to USA TODAY! All subscriptions to USA TODAY delivered in the state of Hawaii are handled by our partner, the Honolulu Star-Advertiser. "
								+ "For information about receiving delivery of USA TODAY in Hawaii, including pricing (which may vary from what is shown here), "
								+ "please contact the Honolulu Star-Advertiser's Customer Service Department by calling 808-538-NEWS (6397) from 5:30am to 5:00 pm Monday-Friday and 6:30am to 10:00 am on weekends (all times HST).",
						null);

				context.addMessage(null, facesMsg);
				responseString = "failure";
				return responseString;
			}

			// /// Delivery Contact ///////////
			ContactBO deliveryContact = new ContactBO();

			UIAddressBO dAddress = new UIAddressBO();
			dAddress.setAddress1(address1);
			// dAddress.setAddress2();
			dAddress.setAptSuite(aptSte);
			dAddress.setCity(city);
			dAddress.setState(state);
			dAddress.setZip(zip);

			deliveryContact.setUIAddress(dAddress);

			if (!deliveryContact.validateContact()) {
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage facesMsg = new FacesMessage(
						FacesMessage.SEVERITY_INFO,
						"We are unable to validate the delivery address. Please verify the address you entered. We cannot determine delivery method without a valid address. For additional assistance processing your order, please contact National Customer Service at 1-800-872-0001.",
						null);

				context.addMessage(null, facesMsg);
				responseString = "failure";
				return responseString;
			} else {
				// update the bean with the code1 address
				try {
					UIAddressIntf correctedAddress = deliveryContact.getPersistentAddress().convertToUIAddress();

					if (correctedAddress.getAddress1().length() > 0) {
						// Check for disallowed addresses
						if (correctedAddress.getState().equals("HI")) {
							FacesContext context = FacesContext.getCurrentInstance();
							FacesMessage facesMsg = new FacesMessage(
									FacesMessage.SEVERITY_INFO,
									"Thank you for your interest in subscribing to USA TODAY! All subscriptions to USA TODAY delivered in the state of Hawaii are handled by our partner, the Honolulu Star-Advertiser. "
											+ "For information about receiving delivery of USA TODAY in Hawaii, including pricing (which may vary from what is shown here), "
											+ "please contact the Honolulu Star-Advertiser's Customer Service Department by calling 808-538-NEWS (6397) from 5:30am to 5:00 pm Monday-Friday and 6:30am to 10:00 am on weekends (all times HST).",
									null);

							context.addMessage(null, facesMsg);
							newSub.setDeliveryState(correctedAddress.getState());
							responseString = "failure";
							return responseString;
						}

						newSub.setDeliveryAddress1(correctedAddress.getAddress1());
						getTextDeliveryAddress1Text().resetValue();
						newSub.setDeliveryAddress2(correctedAddress.getAddress2());
						newSub.setDeliveryAptSuite(correctedAddress.getAptSuite());
						getTextDeliveryAptSuite().resetValue();
						newSub.setDeliveryCity(correctedAddress.getCity());
						getTextDeliveryCity().resetValue();
						newSub.setDeliveryZipCode(correctedAddress.getZip());
						getTextDeliveryZip().resetValue();
						newSub.setDeliveryState(correctedAddress.getState());
						getMenuDeliveryState().resetValue();
					}
				} catch (Exception e) {
					// ignore it
					System.out.println("JSF One Page OrderEntry::Exception setting up corrected address. ignoring.: "
							+ e.getMessage());
				}
			}

			if (deliveryContact.getPersistentAddress().isGUIAddress()) {
				newSub.setDeliveryMethodText("Morning delivery via carrier");
			} else {
				try {

					if (AS400CurrentStatus.getJdbcActive()) {
						deliveryMethodBO.determineDeliveryMethod(offer.getPubCode(), newSub.getDeliveryAddress1(),
								newSub.getDeliveryAptSuite(), newSub.getDeliveryCity(), newSub.getDeliveryState(),
								newSub.getDeliveryZipCode());
						newSub.setDeliveryMethodText(deliveryMethodBO.getDeliveryMethodDisplayString());
						newSub.setDeliveryMethodErrorText(deliveryMethodBO.getDeliveryMethodDisplayErrorString());
					} else {
						newSub.setDeliveryMethodText("We are unable to determine your delivery method at this time. We will send you your delivery method along with other account information at a later date.");
						newSub.setDeliveryMethodErrorText("Delivery Method System Unavailable");
					}
				} catch (Exception e) {
					// iSeries not available for determining delivery method.
					newSub.setDeliveryMethodText("We are unable to determine your delivery method at this time. We will send you your delivery method along with other account information at a later date.");
					newSub.setDeliveryMethodErrorText("Delivery Method System Unavailable");
				} catch (Throwable te) {
					newSub.setDeliveryMethodText("We are unable to determine your delivery method at this time. We will send you your delivery method along with other account information at a later date.");
					newSub.setDeliveryMethodErrorText("Delivery Method System Unavailable");
				}
			}

			// capture Interim customer data
			try {
				InterimCustomerHandler interimCustHandler = this.getInterimCustomerHandler();
				CustomerInterimInfoBO interimCust = interimCustomerHandler.getInterimCust();

				if (interimCust == null) {
					interimCust = new CustomerInterimInfoBO();
					interimCustHandler.setInterimCust(interimCust);
				}

				KeyCodeParser parser = new KeyCodeParser(offer.getKeyCode());
				interimCust.setPublication(offer.getPubCode());
				interimCust.setContestCode(parser.getContestCode());
				interimCust.setPromoCode(parser.getPromoCode());
				interimCust.setSrcOrdCode(parser.getSourceCode());

				String tempStr = this.getTextDeliveryFName().getSubmittedValue().toString();
				interimCust.setFirstName(tempStr);

				tempStr = this.getTextDeliveryLName().getSubmittedValue().toString();
				interimCust.setLastName(tempStr);

				tempStr = this.getTextEmailAddressRecipient().getSubmittedValue().toString();
				interimCust.setEmail(tempStr);

				tempStr = this.getTextDeliveryCompanyName().getSubmittedValue().toString();
				interimCust.setFirmName(tempStr);

				tempStr = this.getTextDeliveryPhoneAreaCode().getSubmittedValue().toString()
						+ this.getTextDeliveryPhoneExchange().getSubmittedValue().toString()
						+ this.getTextDeliveryPhoneExtension().getSubmittedValue().toString();
				interimCust.setHomePhone(tempStr);

				interimCust.setAddress1(newSub.getDeliveryAddress1());
				interimCust.setCity(newSub.getDeliveryCity());
				interimCust.setState(newSub.getDeliveryState());
				interimCust.setZip(newSub.getDeliveryZipCode());
				interimCust.setDelvMethod(deliveryMethodBO.getDeliveryMethodDisplayString());

				interimCust.save(interimCust);

			} catch (Exception e) {
				; // ignore issues here
			}

			responseString = "delMethodSuccess";
		} catch (UsatException e) {
			newSub.setDeliveryMethodText("Enter your full delivery address to determine delivery method.");
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
					"Unable to determine delivery method. Please verify your full delivery address. " + e.getMessage(), null);

			context.addMessage(null, facesMsg);
			responseString = "failure";
		} catch (Exception e) {
			newSub.setDeliveryMethodText("Delivery Method detection is currently unavailable.");
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"Delivery Method detection is currently unavailable. " + e.getMessage(), null);

			context.addMessage(null, facesMsg);
			responseString = "failure";
		}
		// return "delMethodSuccess";
		// return "failure";
		return responseString;
	}

	public String doButtonProcessOfferCodeAction() {
		// Type Java code that runs when the component is clicked

		// return "newOffer";

		Object submittedVal = this.getTextNewOfferCode().getSubmittedValue();
		try {
			String offerCode = String.valueOf(submittedVal);

			if (offerCode == null || offerCode.trim().length() == 0) {
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "No offer Code Provided.", null);

				context.addMessage(null, facesMsg);
				return "failure";
			} else {
				offerCode = offerCode.trim();
			}

			SubscriptionOfferHandler offerHandler = this.getCurrentOfferHandler();

			// first check via keycode since it may be cached
			SubscriptionOfferIntf newOffer = null;
			try {
				newOffer = SubscriptionOfferManager.getInstance().getOffer(offerCode, offerHandler.getCurrentOffer().getPubCode());
			} catch (Exception e) {
				// ignore...previous method throws exception if not found.
				newOffer = null;
			}

			/*
			 * Operator number does not exist in Genesys, therefore, the following no longer is needed if (newOffer == null) { //
			 * check by offer code newOffer = SubscriptionOfferManager.getInstance().getOfferForOfferCode
			 * (offerHandler.getCurrentOffer().getPubCode(), offerCode); }
			 */
			if (newOffer != null) {
				offerHandler.setCurrentOffer(newOffer);
				HttpServletRequest req = (HttpServletRequest) this.getFacesContext().getExternalContext().getRequest();

				this.getNewSubscriptionOrderHandler().setSelectedTerm(null);

				// updaet session offer
				UTCommon.overrideCurrentOffer(req, newOffer);

				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Offer Code " + offerCode + " applied.", null);

				context.addMessage(null, facesMsg);

			} else {
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
						"No offer for that offer code was found. The offer may have expired.", null);

				context.addMessage(null, facesMsg);
				return "failure";
			}

		} catch (Exception e) {
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
					"No offer for that offer code was found. The offer may have expired.", null);

			context.addMessage(null, facesMsg);
			return "failure";
		}

		// do not returh success
		return "newOffer";
	}

	public String doLinkRevertOfferLinkAction() {
		// Type Java code that runs when the component is clicked

		HttpServletRequest req = (HttpServletRequest) this.getFacesContext().getExternalContext().getRequest();

		SubscriptionOfferHandler offerHandler = this.getCurrentOfferHandler();

		SubscriptionOfferIntf currentOffer = offerHandler.getPreviousOffer();

		offerHandler.setCurrentOffer(currentOffer);
		offerHandler.setPreviousOffer(null);

		this.getNewSubscriptionOrderHandler().setCustomerEnteredOfferCode(null);
		this.getSectionNewOfferCode1().setInitClosed(true);

		this.getNewSubscriptionOrderHandler().setSelectedTerm(null);

		try {
			UTCommon.overrideCurrentOffer(req, currentOffer);
		} catch (Exception e) {
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Error Reverting Offer.", null);

			context.addMessage(null, facesMsg);
			return "failure";
		}

		return "";
	}

	/**
	 * @managed-bean true
	 */
	protected InterimCustomerHandler getInterimCustomerHandler() {
		if (interimCustomerHandler == null) {
			interimCustomerHandler = (InterimCustomerHandler) getManagedBean("interimCustomerHandler");
		}
		return interimCustomerHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setInterimCustomerHandler(InterimCustomerHandler interimCustomerHandler) {
		this.interimCustomerHandler = interimCustomerHandler;
	}

	protected HtmlOutputText getMainHeader() {
		if (mainHeader == null) {
			mainHeader = (HtmlOutputText) findComponentInRoot("mainHeader");
		}
		return mainHeader;
	}

	/**
	 * @managed-bean true
	 */
	protected PremiumPromotionHandler getPremiumPromotionHandler() {
		if (premiumPromotionHandler == null) {
			premiumPromotionHandler = (PremiumPromotionHandler) getManagedBean("premiumPromotionHandler");
		}
		return premiumPromotionHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setPremiumPromotionHandler(PremiumPromotionHandler premiumPromotionHandler) {
		this.premiumPromotionHandler = premiumPromotionHandler;
	}

	protected HtmlOutputText getTextDelMethodText() {
		if (textDelMethodText == null) {
			textDelMethodText = (HtmlOutputText) findComponentInRoot("textDelMethodText");
		}
		return textDelMethodText;
	}

	protected HtmlInputText getTextDeliveryFName() {
		if (textDeliveryFName == null) {
			textDeliveryFName = (HtmlInputText) findComponentInRoot("textDeliveryFirstName");
		}
		return textDeliveryFName;
	}

	/**
	 * @managed-bean true
	 */
	protected NewSubscriptionOrderHandler getNewSubscriptionOrderHandler() {
		if (newSubscriptionOrderHandler == null) {
			newSubscriptionOrderHandler = (NewSubscriptionOrderHandler) getManagedBean("newSubscriptionOrderHandler");
		}
		return newSubscriptionOrderHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setNewSubscriptionOrderHandler(NewSubscriptionOrderHandler newSubscriptionOrderHandler) {
		this.newSubscriptionOrderHandler = newSubscriptionOrderHandler;
	}

	protected HtmlOutputFormat getFormat1() {
		if (format1 == null) {
			format1 = (HtmlOutputFormat) findComponentInRoot("format1");
		}
		return format1;
	}

	protected HtmlScriptCollector getScriptCollectorBottomNavCollector() {
		if (scriptCollectorBottomNavCollector == null) {
			scriptCollectorBottomNavCollector = (HtmlScriptCollector) findComponentInRoot("scriptCollectorBottomNavCollector");
		}
		return scriptCollectorBottomNavCollector;
	}

	protected HtmlForm getFormBottomNavForm() {
		if (formBottomNavForm == null) {
			formBottomNavForm = (HtmlForm) findComponentInRoot("formBottomNavForm");
		}
		return formBottomNavForm;
	}

	protected HtmlPanelGrid getGridLeftColHTMLSpot1Grid() {
		if (gridLeftColHTMLSpot1Grid == null) {
			gridLeftColHTMLSpot1Grid = (HtmlPanelGrid) findComponentInRoot("gridLeftColHTMLSpot1Grid");
		}
		return gridLeftColHTMLSpot1Grid;
	}

	protected HtmlOutputText getLeftColHTMLSpot1Text() {
		if (leftColHTMLSpot1Text == null) {
			leftColHTMLSpot1Text = (HtmlOutputText) findComponentInRoot("leftColHTMLSpot1Text");
		}
		return leftColHTMLSpot1Text;
	}

	protected HtmlOutputLinkEx getLinkExTopNavLogoLink() {
		if (linkExTopNavLogoLink == null) {
			linkExTopNavLogoLink = (HtmlOutputLinkEx) findComponentInRoot("linkExTopNavLogoLink");
		}
		return linkExTopNavLogoLink;
	}

	protected HtmlGraphicImageEx getImageExPartnerLogo() {
		if (imageExPartnerLogo == null) {
			imageExPartnerLogo = (HtmlGraphicImageEx) findComponentInRoot("imageExPartnerLogo");
		}
		return imageExPartnerLogo;
	}

	protected HtmlOutputLinkEx getLinkExLeftColImageSpot1Link() {
		if (linkExLeftColImageSpot1Link == null) {
			linkExLeftColImageSpot1Link = (HtmlOutputLinkEx) findComponentInRoot("linkExLeftColImageSpot1Link");
		}
		return linkExLeftColImageSpot1Link;
	}

	protected HtmlGraphicImageEx getImageExLeftColImage1() {
		if (imageExLeftColImage1 == null) {
			imageExLeftColImage1 = (HtmlGraphicImageEx) findComponentInRoot("imageExLeftColImage1");
		}
		return imageExLeftColImage1;
	}

	protected HtmlOutputLinkEx getLinkExVideoLink_LeftCol() {
		if (linkExVideoLink_LeftCol == null) {
			linkExVideoLink_LeftCol = (HtmlOutputLinkEx) findComponentInRoot("linkExVideoLink_LeftCol");
		}
		return linkExVideoLink_LeftCol;
	}

	protected HtmlGraphicImageEx getImageExVideoSweepsGraphic_LeftCol() {
		if (imageExVideoSweepsGraphic_LeftCol == null) {
			imageExVideoSweepsGraphic_LeftCol = (HtmlGraphicImageEx) findComponentInRoot("imageExVideoSweepsGraphic_LeftCol");
		}
		return imageExVideoSweepsGraphic_LeftCol;
	}

	protected HtmlOutputLinkEx getLinkExLeftColImageSpot2Link() {
		if (linkExLeftColImageSpot2Link == null) {
			linkExLeftColImageSpot2Link = (HtmlOutputLinkEx) findComponentInRoot("linkExLeftColImageSpot2Link");
		}
		return linkExLeftColImageSpot2Link;
	}

	protected HtmlGraphicImageEx getImageExLeftColImage2() {
		if (imageExLeftColImage2 == null) {
			imageExLeftColImage2 = (HtmlGraphicImageEx) findComponentInRoot("imageExLeftColImage2");
		}
		return imageExLeftColImage2;
	}

	protected HtmlJspPanel getJspPanelBottomNavCustomPanel() {
		if (jspPanelBottomNavCustomPanel == null) {
			jspPanelBottomNavCustomPanel = (HtmlJspPanel) findComponentInRoot("jspPanelBottomNavCustomPanel");
		}
		return jspPanelBottomNavCustomPanel;
	}

	protected HtmlGraphicImageEx getImageExPartnerLogoNoLink() {
		if (imageExPartnerLogoNoLink == null) {
			imageExPartnerLogoNoLink = (HtmlGraphicImageEx) findComponentInRoot("imageExPartnerLogoNoLink");
		}
		return imageExPartnerLogoNoLink;
	}

	protected HtmlPanelGrid getLeftColImageSpot1() {
		if (leftColImageSpot1 == null) {
			leftColImageSpot1 = (HtmlPanelGrid) findComponentInRoot("leftColImageSpot1");
		}
		return leftColImageSpot1;
	}

	protected HtmlGraphicImageEx getImageExLeftColImage1NoLink() {
		if (imageExLeftColImage1NoLink == null) {
			imageExLeftColImage1NoLink = (HtmlGraphicImageEx) findComponentInRoot("imageExLeftColImage1NoLink");
		}
		return imageExLeftColImage1NoLink;
	}

	protected HtmlPanelGrid getGridVideoGridLeftCol() {
		if (gridVideoGridLeftCol == null) {
			gridVideoGridLeftCol = (HtmlPanelGrid) findComponentInRoot("gridVideoGridLeftCol");
		}
		return gridVideoGridLeftCol;
	}

	protected HtmlOutputText getTextEEVideoTextLeftCol() {
		if (textEEVideoTextLeftCol == null) {
			textEEVideoTextLeftCol = (HtmlOutputText) findComponentInRoot("textEEVideoTextLeftCol");
		}
		return textEEVideoTextLeftCol;
	}

	protected HtmlPanelGrid getLeftColImageSpot2() {
		if (leftColImageSpot2 == null) {
			leftColImageSpot2 = (HtmlPanelGrid) findComponentInRoot("leftColImageSpot2");
		}
		return leftColImageSpot2;
	}

	protected HtmlGraphicImageEx getImageExLeftColImage2NoLink() {
		if (imageExLeftColImage2NoLink == null) {
			imageExLeftColImage2NoLink = (HtmlGraphicImageEx) findComponentInRoot("imageExLeftColImage2NoLink");
		}
		return imageExLeftColImage2NoLink;
	}

	protected HtmlInputHelperAssist getAssist1() {
		if (assist1 == null) {
			assist1 = (HtmlInputHelperAssist) findComponentInRoot("assist1");
		}
		return assist1;
	}

	protected UISelectItems getSelectItems2() {
		if (selectItems2 == null) {
			selectItems2 = (UISelectItems) findComponentInRoot("selectItems2");
		}
		return selectItems2;
	}

	protected HtmlInputHelperAssist getAssist4() {
		if (assist4 == null) {
			assist4 = (HtmlInputHelperAssist) findComponentInRoot("assist4");
		}
		return assist4;
	}

	protected HtmlInputHelperAssist getAssist5() {
		if (assist5 == null) {
			assist5 = (HtmlInputHelperAssist) findComponentInRoot("assist5");
		}
		return assist5;
	}

	protected HtmlInputHelperAssist getAssist44() {
		if (assist44 == null) {
			assist44 = (HtmlInputHelperAssist) findComponentInRoot("assist44");
		}
		return assist44;
	}

	protected HtmlInputHelperAssist getAssist45() {
		if (assist45 == null) {
			assist45 = (HtmlInputHelperAssist) findComponentInRoot("assist45");
		}
		return assist45;
	}

	protected HtmlInputHelperAssist getAssist2() {
		if (assist2 == null) {
			assist2 = (HtmlInputHelperAssist) findComponentInRoot("assist2");
		}
		return assist2;
	}

	protected HtmlInputHelperAssist getAssist3() {
		if (assist3 == null) {
			assist3 = (HtmlInputHelperAssist) findComponentInRoot("assist3");
		}
		return assist3;
	}

	protected UISelectItem getSelectItem1() {
		if (selectItem1 == null) {
			selectItem1 = (UISelectItem) findComponentInRoot("selectItem1");
		}
		return selectItem1;
	}

	protected UISelectItem getSelectItem16() {
		if (selectItem16 == null) {
			selectItem16 = (UISelectItem) findComponentInRoot("selectItem16");
		}
		return selectItem16;
	}

	protected UISelectItems getSelectItems1() {
		if (selectItems1 == null) {
			selectItems1 = (UISelectItems) findComponentInRoot("selectItems1");
		}
		return selectItems1;
	}

	protected UISelectItem getSelectItem17() {
		if (selectItem17 == null) {
			selectItem17 = (UISelectItem) findComponentInRoot("selectItem17");
		}
		return selectItem17;
	}

	protected UISelectItem getSelectItem19() {
		if (selectItem19 == null) {
			selectItem19 = (UISelectItem) findComponentInRoot("selectItem19");
		}
		return selectItem19;
	}

	protected HtmlInputHelperAssist getAssist7() {
		if (assist7 == null) {
			assist7 = (HtmlInputHelperAssist) findComponentInRoot("assist7");
		}
		return assist7;
	}

	protected HtmlInputHelperDatePicker getDatePicker1() {
		if (datePicker1 == null) {
			datePicker1 = (HtmlInputHelperDatePicker) findComponentInRoot("datePicker1");
		}
		return datePicker1;
	}

	protected UISelectItem getSelectItem2() {
		if (selectItem2 == null) {
			selectItem2 = (UISelectItem) findComponentInRoot("selectItem2");
		}
		return selectItem2;
	}

	protected HtmlInputHelperAssist getAssist6() {
		if (assist6 == null) {
			assist6 = (HtmlInputHelperAssist) findComponentInRoot("assist6");
		}
		return assist6;
	}

	protected UISelectItem getSelectItem3() {
		if (selectItem3 == null) {
			selectItem3 = (UISelectItem) findComponentInRoot("selectItem3");
		}
		return selectItem3;
	}

	protected UISelectItem getSelectItem4() {
		if (selectItem4 == null) {
			selectItem4 = (UISelectItem) findComponentInRoot("selectItem4");
		}
		return selectItem4;
	}

	protected UISelectItem getSelectItem5() {
		if (selectItem5 == null) {
			selectItem5 = (UISelectItem) findComponentInRoot("selectItem5");
		}
		return selectItem5;
	}

	protected UISelectItem getSelectItem6() {
		if (selectItem6 == null) {
			selectItem6 = (UISelectItem) findComponentInRoot("selectItem6");
		}
		return selectItem6;
	}

	protected UISelectItem getSelectItem7() {
		if (selectItem7 == null) {
			selectItem7 = (UISelectItem) findComponentInRoot("selectItem7");
		}
		return selectItem7;
	}

	protected UISelectItem getSelectItem8() {
		if (selectItem8 == null) {
			selectItem8 = (UISelectItem) findComponentInRoot("selectItem8");
		}
		return selectItem8;
	}

	protected UISelectItem getSelectItem9() {
		if (selectItem9 == null) {
			selectItem9 = (UISelectItem) findComponentInRoot("selectItem9");
		}
		return selectItem9;
	}

	protected UISelectItem getSelectItem10() {
		if (selectItem10 == null) {
			selectItem10 = (UISelectItem) findComponentInRoot("selectItem10");
		}
		return selectItem10;
	}

	protected UISelectItem getSelectItem11() {
		if (selectItem11 == null) {
			selectItem11 = (UISelectItem) findComponentInRoot("selectItem11");
		}
		return selectItem11;
	}

	protected UISelectItem getSelectItem12() {
		if (selectItem12 == null) {
			selectItem12 = (UISelectItem) findComponentInRoot("selectItem12");
		}
		return selectItem12;
	}

	protected UISelectItem getSelectItem13() {
		if (selectItem13 == null) {
			selectItem13 = (UISelectItem) findComponentInRoot("selectItem13");
		}
		return selectItem13;
	}

	protected UISelectItem getSelectItem14() {
		if (selectItem14 == null) {
			selectItem14 = (UISelectItem) findComponentInRoot("selectItem14");
		}
		return selectItem14;
	}

	protected UISelectItem getSelectItem18() {
		if (selectItem18 == null) {
			selectItem18 = (UISelectItem) findComponentInRoot("selectItem18");
		}
		return selectItem18;
	}

	protected UISelectItem getSelectItem20() {
		if (selectItem20 == null) {
			selectItem20 = (UISelectItem) findComponentInRoot("selectItem20");
		}
		return selectItem20;
	}

	protected UISelectItem getSelectItem15() {
		if (selectItem15 == null) {
			selectItem15 = (UISelectItem) findComponentInRoot("selectItem15");
		}
		return selectItem15;
	}

	protected HtmlScriptCollector getScriptCollectorMainOrderEntryCollector() {
		if (scriptCollectorMainOrderEntryCollector == null) {
			scriptCollectorMainOrderEntryCollector = (HtmlScriptCollector) findComponentInRoot("scriptCollectorMainOrderEntryCollector");
		}
		return scriptCollectorMainOrderEntryCollector;
	}

	protected HtmlForm getFormOrderEntryForm() {
		if (formOrderEntryForm == null) {
			formOrderEntryForm = (HtmlForm) findComponentInRoot("formOrderEntryForm");
		}
		return formOrderEntryForm;
	}

	protected HtmlOutputText getTextMainTableHeader() {
		if (textMainTableHeader == null) {
			textMainTableHeader = (HtmlOutputText) findComponentInRoot("textMainTableHeader");
		}
		return textMainTableHeader;
	}

	protected HtmlJspPanel getJspPanelFormPanel() {
		if (jspPanelFormPanel == null) {
			jspPanelFormPanel = (HtmlJspPanel) findComponentInRoot("jspPanelFormPanel");
		}
		return jspPanelFormPanel;
	}

	protected HtmlOutputText getTextPromoText1() {
		if (textPromoText1 == null) {
			textPromoText1 = (HtmlOutputText) findComponentInRoot("textPromoText1");
		}
		return textPromoText1;
	}

	protected HtmlOutputText getTextDelvMethodText() {
		if (textDelvMethodText == null) {
			textDelvMethodText = (HtmlOutputText) findComponentInRoot("textDelvMethodText");
		}
		return textDelvMethodText;
	}

	protected HtmlPanelLayout getLayoutPageLayout() {
		if (layoutPageLayout == null) {
			layoutPageLayout = (HtmlPanelLayout) findComponentInRoot("layoutPageLayout");
		}
		return layoutPageLayout;
	}

	protected HtmlPanelGroup getGroupMainBodyPanel() {
		if (groupMainBodyPanel == null) {
			groupMainBodyPanel = (HtmlPanelGroup) findComponentInRoot("groupMainBodyPanel");
		}
		return groupMainBodyPanel;
	}

	protected HtmlPanelGrid getGridTermsGrid() {
		if (gridTermsGrid == null) {
			gridTermsGrid = (HtmlPanelGrid) findComponentInRoot("gridTermsGrid");
		}
		return gridTermsGrid;
	}

	protected HtmlPanelGroup getGroup10() {
		if (group10 == null) {
			group10 = (HtmlPanelGroup) findComponentInRoot("group10");
		}
		return group10;
	}

	protected HtmlOutputText getText7() {
		if (text7 == null) {
			text7 = (HtmlOutputText) findComponentInRoot("text7");
		}
		return text7;
	}

	protected HtmlFormItem getFormItemSubscriptionTerms() {
		if (formItemSubscriptionTerms == null) {
			formItemSubscriptionTerms = (HtmlFormItem) findComponentInRoot("formItemSubscriptionTerms");
		}
		return formItemSubscriptionTerms;
	}

	protected HtmlSelectOneRadio getRadioTermsSelection() {
		if (radioTermsSelection == null) {
			radioTermsSelection = (HtmlSelectOneRadio) findComponentInRoot("radioTermsSelection");
		}
		return radioTermsSelection;
	}

	protected HtmlPanelGrid getGridTermsFormBoxBottomFacetGrid() {
		if (gridTermsFormBoxBottomFacetGrid == null) {
			gridTermsFormBoxBottomFacetGrid = (HtmlPanelGrid) findComponentInRoot("gridTermsFormBoxBottomFacetGrid");
		}
		return gridTermsFormBoxBottomFacetGrid;
	}

	protected HtmlPanelGroup getGroup4() {
		if (group4 == null) {
			group4 = (HtmlPanelGroup) findComponentInRoot("group4");
		}
		return group4;
	}

	protected HtmlOutputText getTextEZPayExplanationRateCustomText() {
		if (textEZPayExplanationRateCustomText == null) {
			textEZPayExplanationRateCustomText = (HtmlOutputText) findComponentInRoot("textEZPayExplanationRateCustomText");
		}
		return textEZPayExplanationRateCustomText;
	}

	protected HtmlPanelSection getSectionNewOfferCode1() {
		if (sectionNewOfferCode1 == null) {
			sectionNewOfferCode1 = (HtmlPanelSection) findComponentInRoot("sectionNewOfferCode1");
		}
		return sectionNewOfferCode1;
	}

	protected HtmlJspPanel getJspPanel4() {
		if (jspPanel4 == null) {
			jspPanel4 = (HtmlJspPanel) findComponentInRoot("jspPanel4");
		}
		return jspPanel4;
	}

	protected HtmlOutputText getTextOfferOverrridePanelHeaderClosedText() {
		if (textOfferOverrridePanelHeaderClosedText == null) {
			textOfferOverrridePanelHeaderClosedText = (HtmlOutputText) findComponentInRoot("textOfferOverrridePanelHeaderClosedText");
		}
		return textOfferOverrridePanelHeaderClosedText;
	}

	protected HtmlJspPanel getJspPanel3() {
		if (jspPanel3 == null) {
			jspPanel3 = (HtmlJspPanel) findComponentInRoot("jspPanel3");
		}
		return jspPanel3;
	}

	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected HtmlPanelFormBox getFormBoxOfferCodeEntry1() {
		if (formBoxOfferCodeEntry1 == null) {
			formBoxOfferCodeEntry1 = (HtmlPanelFormBox) findComponentInRoot("formBoxOfferCodeEntry1");
		}
		return formBoxOfferCodeEntry1;
	}

	protected HtmlFormItem getFormItem1() {
		if (formItem1 == null) {
			formItem1 = (HtmlFormItem) findComponentInRoot("formItem1");
		}
		return formItem1;
	}

	protected HtmlInputText getTextNewOfferCode() {
		if (textNewOfferCode == null) {
			textNewOfferCode = (HtmlInputText) findComponentInRoot("textNewOfferCode");
		}
		return textNewOfferCode;
	}

	protected UIParameter getCustOfferCodeParm() {
		if (custOfferCodeParm == null) {
			custOfferCodeParm = (UIParameter) findComponentInRoot("custOfferCodeParm");
		}
		return custOfferCodeParm;
	}

	protected HtmlPanelGrid getGridNewOfferBottomGrid1() {
		if (gridNewOfferBottomGrid1 == null) {
			gridNewOfferBottomGrid1 = (HtmlPanelGrid) findComponentInRoot("gridNewOfferBottomGrid1");
		}
		return gridNewOfferBottomGrid1;
	}

	protected HtmlCommandLink getLinkRevertOfferLink() {
		if (linkRevertOfferLink == null) {
			linkRevertOfferLink = (HtmlCommandLink) findComponentInRoot("linkRevertOfferLink");
		}
		return linkRevertOfferLink;
	}

	protected HtmlOutputText getTextRevertOfferLinkText() {
		if (textRevertOfferLinkText == null) {
			textRevertOfferLinkText = (HtmlOutputText) findComponentInRoot("textRevertOfferLinkText");
		}
		return textRevertOfferLinkText;
	}

	protected HtmlPanelGroup getGroupPromoTermsHeaderGroup8() {
		if (groupPromoTermsHeaderGroup8 == null) {
			groupPromoTermsHeaderGroup8 = (HtmlPanelGroup) findComponentInRoot("groupPromoTermsHeaderGroup8");
		}
		return groupPromoTermsHeaderGroup8;
	}

	protected HtmlOutputText getTextPrmotionsTermLabel() {
		if (textPrmotionsTermLabel == null) {
			textPrmotionsTermLabel = (HtmlOutputText) findComponentInRoot("textPrmotionsTermLabel");
		}
		return textPrmotionsTermLabel;
	}

	protected HtmlOutputText getTextPromoDescriptionLabel() {
		if (textPromoDescriptionLabel == null) {
			textPromoDescriptionLabel = (HtmlOutputText) findComponentInRoot("textPromoDescriptionLabel");
		}
		return textPromoDescriptionLabel;
	}

	protected HtmlOutputText getText6() {
		if (text6 == null) {
			text6 = (HtmlOutputText) findComponentInRoot("text6");
		}
		return text6;
	}

	protected HtmlPanelFormBox getFormBoxQuantityInformation() {
		if (formBoxQuantityInformation == null) {
			formBoxQuantityInformation = (HtmlPanelFormBox) findComponentInRoot("formBoxQuantityInformation");
		}
		return formBoxQuantityInformation;
	}

	protected HtmlFormItem getFormItemQuantityInformation() {
		if (formItemQuantityInformation == null) {
			formItemQuantityInformation = (HtmlFormItem) findComponentInRoot("formItemQuantityInformation");
		}
		return formItemQuantityInformation;
	}

	protected HtmlSelectOneMenu getMenuOrderQuantity() {
		if (menuOrderQuantity == null) {
			menuOrderQuantity = (HtmlSelectOneMenu) findComponentInRoot("menuOrderQuantity");
		}
		return menuOrderQuantity;
	}

	protected HtmlPanelFormBox getFormBoxPartnerInformation() {
		if (formBoxPartnerInformation == null) {
			formBoxPartnerInformation = (HtmlPanelFormBox) findComponentInRoot("formBoxPartnerInformation");
		}
		return formBoxPartnerInformation;
	}

	protected HtmlFormItem getFormItemPartnerClubNumber() {
		if (formItemPartnerClubNumber == null) {
			formItemPartnerClubNumber = (HtmlFormItem) findComponentInRoot("formItemPartnerClubNumber");
		}
		return formItemPartnerClubNumber;
	}

	protected HtmlInputText getTextClubNumber() {
		if (textClubNumber == null) {
			textClubNumber = (HtmlInputText) findComponentInRoot("textClubNumber");
		}
		return textClubNumber;
	}

	protected HtmlPanelFormBox getFormBoxStartDateInformation() {
		if (formBoxStartDateInformation == null) {
			formBoxStartDateInformation = (HtmlPanelFormBox) findComponentInRoot("formBoxStartDateInformation");
		}
		return formBoxStartDateInformation;
	}

	protected HtmlFormItem getFormItemStartDateFormItem() {
		if (formItemStartDateFormItem == null) {
			formItemStartDateFormItem = (HtmlFormItem) findComponentInRoot("formItemStartDateFormItem");
		}
		return formItemStartDateFormItem;
	}

	protected HtmlInputText getTextStartDateInput() {
		if (textStartDateInput == null) {
			textStartDateInput = (HtmlInputText) findComponentInRoot("textStartDateInput");
		}
		return textStartDateInput;
	}

	protected HtmlPanelFormBox getFormBoxDeliveryInformation() {
		if (formBoxDeliveryInformation == null) {
			formBoxDeliveryInformation = (HtmlPanelFormBox) findComponentInRoot("formBoxDeliveryInformation");
		}
		return formBoxDeliveryInformation;
	}

	protected HtmlFormItem getFormItemDeliveryFName() {
		if (formItemDeliveryFName == null) {
			formItemDeliveryFName = (HtmlFormItem) findComponentInRoot("formItemDeliveryFName");
		}
		return formItemDeliveryFName;
	}

	protected HtmlInputText getTextDeliveryFirstName() {
		if (textDeliveryFirstName == null) {
			textDeliveryFirstName = (HtmlInputText) findComponentInRoot("textDeliveryFirstName");
		}
		return textDeliveryFirstName;
	}

	protected HtmlInputText getTextDeliveryLName() {
		if (textDeliveryLName == null) {
			textDeliveryLName = (HtmlInputText) findComponentInRoot("textDeliveryLName");
		}
		return textDeliveryLName;
	}

	protected HtmlInputText getTextDeliveryCompanyName() {
		if (textDeliveryCompanyName == null) {
			textDeliveryCompanyName = (HtmlInputText) findComponentInRoot("textDeliveryCompanyName");
		}
		return textDeliveryCompanyName;
	}

	protected HtmlInputText getTextDeliveryAddress1Text() {
		if (textDeliveryAddress1Text == null) {
			textDeliveryAddress1Text = (HtmlInputText) findComponentInRoot("textDeliveryAddress1Text");
		}
		return textDeliveryAddress1Text;
	}

	protected HtmlInputText getTextDeliveryAddress2() {
		if (textDeliveryAddress2 == null) {
			textDeliveryAddress2 = (HtmlInputText) findComponentInRoot("textDeliveryAddress2");
		}
		return textDeliveryAddress2;
	}

	protected HtmlInputText getTextDeliveryCity() {
		if (textDeliveryCity == null) {
			textDeliveryCity = (HtmlInputText) findComponentInRoot("textDeliveryCity");
		}
		return textDeliveryCity;
	}

	protected HtmlSelectOneMenu getMenuDeliveryState() {
		if (menuDeliveryState == null) {
			menuDeliveryState = (HtmlSelectOneMenu) findComponentInRoot("menuDeliveryState");
		}
		return menuDeliveryState;
	}

	protected HtmlInputText getTextDeliveryPhoneAreaCode() {
		if (textDeliveryPhoneAreaCode == null) {
			textDeliveryPhoneAreaCode = (HtmlInputText) findComponentInRoot("textDeliveryPhoneAreaCode");
		}
		return textDeliveryPhoneAreaCode;
	}

	protected HtmlInputText getTextDeliveryWorkPhoneAreaCode() {
		if (textDeliveryWorkPhoneAreaCode == null) {
			textDeliveryWorkPhoneAreaCode = (HtmlInputText) findComponentInRoot("textDeliveryWorkPhoneAreaCode");
		}
		return textDeliveryWorkPhoneAreaCode;
	}

	protected HtmlInputText getTextEmailAddressRecipient() {
		if (textEmailAddressRecipient == null) {
			textEmailAddressRecipient = (HtmlInputText) findComponentInRoot("textEmailAddressRecipient");
		}
		return textEmailAddressRecipient;
	}

	protected HtmlInputText getTextEmailAddressConfirmRecipient() {
		if (textEmailAddressConfirmRecipient == null) {
			textEmailAddressConfirmRecipient = (HtmlInputText) findComponentInRoot("textEmailAddressConfirmRecipient");
		}
		return textEmailAddressConfirmRecipient;
	}

	protected HtmlPanelGrid getGridDeliveryMethodInfoGrid() {
		if (gridDeliveryMethodInfoGrid == null) {
			gridDeliveryMethodInfoGrid = (HtmlPanelGrid) findComponentInRoot("gridDeliveryMethodInfoGrid");
		}
		return gridDeliveryMethodInfoGrid;
	}

	protected HtmlOutputLinkEx getLinkExDelMethodCheckHelpLink() {
		if (linkExDelMethodCheckHelpLink == null) {
			linkExDelMethodCheckHelpLink = (HtmlOutputLinkEx) findComponentInRoot("linkExDelMethodCheckHelpLink");
		}
		return linkExDelMethodCheckHelpLink;
	}

	protected HtmlOutputText getTextDelMethodHelpText() {
		if (textDelMethodHelpText == null) {
			textDelMethodHelpText = (HtmlOutputText) findComponentInRoot("textDelMethodHelpText");
		}
		return textDelMethodHelpText;
	}

	protected HtmlPanelGrid getGridDeliveryInformationPanelFooterGrid() {
		if (gridDeliveryInformationPanelFooterGrid == null) {
			gridDeliveryInformationPanelFooterGrid = (HtmlPanelGrid) findComponentInRoot("gridDeliveryInformationPanelFooterGrid");
		}
		return gridDeliveryInformationPanelFooterGrid;
	}

	protected HtmlPanelGrid getGridDelMethodResultsGrid() {
		if (gridDelMethodResultsGrid == null) {
			gridDelMethodResultsGrid = (HtmlPanelGrid) findComponentInRoot("gridDelMethodResultsGrid");
		}
		return gridDelMethodResultsGrid;
	}

	protected HtmlOutputText getTextDeliveryMethod() {
		if (textDeliveryMethod == null) {
			textDeliveryMethod = (HtmlOutputText) findComponentInRoot("textDeliveryMethod");
		}
		return textDeliveryMethod;
	}

	protected HtmlPanelFormBox getFormBoxBillDifferentFromDelSelectionFormBox() {
		if (formBoxBillDifferentFromDelSelectionFormBox == null) {
			formBoxBillDifferentFromDelSelectionFormBox = (HtmlPanelFormBox) findComponentInRoot("formBoxBillDifferentFromDelSelectionFormBox");
		}
		return formBoxBillDifferentFromDelSelectionFormBox;
	}

	protected HtmlFormItem getFormItemBillDifferentFromDelSelector() {
		if (formItemBillDifferentFromDelSelector == null) {
			formItemBillDifferentFromDelSelector = (HtmlFormItem) findComponentInRoot("formItemBillDifferentFromDelSelector");
		}
		return formItemBillDifferentFromDelSelector;
	}

	protected HtmlSelectBooleanCheckbox getCheckboxIsBillDifferentFromDelSelector() {
		if (checkboxIsBillDifferentFromDelSelector == null) {
			checkboxIsBillDifferentFromDelSelector = (HtmlSelectBooleanCheckbox) findComponentInRoot("checkboxIsBillDifferentFromDelSelector");
		}
		return checkboxIsBillDifferentFromDelSelector;
	}

	protected HtmlBehavior getBehavior1() {
		if (behavior1 == null) {
			behavior1 = (HtmlBehavior) findComponentInRoot("behavior1");
		}
		return behavior1;
	}

	protected HtmlFormItem getFormItemBillingFirstName() {
		if (formItemBillingFirstName == null) {
			formItemBillingFirstName = (HtmlFormItem) findComponentInRoot("formItemBillingFirstName");
		}
		return formItemBillingFirstName;
	}

	protected HtmlInputText getTextBillingFirstName() {
		if (textBillingFirstName == null) {
			textBillingFirstName = (HtmlInputText) findComponentInRoot("textBillingFirstName");
		}
		return textBillingFirstName;
	}

	protected HtmlInputText getTextBillingLastName() {
		if (textBillingLastName == null) {
			textBillingLastName = (HtmlInputText) findComponentInRoot("textBillingLastName");
		}
		return textBillingLastName;
	}

	protected HtmlInputText getTextBillingCompanyName() {
		if (textBillingCompanyName == null) {
			textBillingCompanyName = (HtmlInputText) findComponentInRoot("textBillingCompanyName");
		}
		return textBillingCompanyName;
	}

	protected HtmlInputText getTextBillingAddress1() {
		if (textBillingAddress1 == null) {
			textBillingAddress1 = (HtmlInputText) findComponentInRoot("textBillingAddress1");
		}
		return textBillingAddress1;
	}

	protected HtmlInputText getTextBillingAddress2() {
		if (textBillingAddress2 == null) {
			textBillingAddress2 = (HtmlInputText) findComponentInRoot("textBillingAddress2");
		}
		return textBillingAddress2;
	}

	protected HtmlInputText getTextBillingCity() {
		if (textBillingCity == null) {
			textBillingCity = (HtmlInputText) findComponentInRoot("textBillingCity");
		}
		return textBillingCity;
	}

	protected HtmlSelectOneMenu getMenuBillingState() {
		if (menuBillingState == null) {
			menuBillingState = (HtmlSelectOneMenu) findComponentInRoot("menuBillingState");
		}
		return menuBillingState;
	}

	protected HtmlInputText getTextBillingPhoneAreaCode() {
		if (textBillingPhoneAreaCode == null) {
			textBillingPhoneAreaCode = (HtmlInputText) findComponentInRoot("textBillingPhoneAreaCode");
		}
		return textBillingPhoneAreaCode;
	}

	protected HtmlSelectBooleanCheckbox getGiftSubscriptionCheckbox() {
		if (giftSubscriptionCheckbox == null) {
			giftSubscriptionCheckbox = (HtmlSelectBooleanCheckbox) findComponentInRoot("giftSubscriptionCheckbox");
		}
		return giftSubscriptionCheckbox;
	}

	protected HtmlInputText getTextPurchaserEmailAddress() {
		if (textPurchaserEmailAddress == null) {
			textPurchaserEmailAddress = (HtmlInputText) findComponentInRoot("textPurchaserEmailAddress");
		}
		return textPurchaserEmailAddress;
	}

	protected HtmlPanelFormBox getFormBoxPaymentHeaderFormBox() {
		if (formBoxPaymentHeaderFormBox == null) {
			formBoxPaymentHeaderFormBox = (HtmlPanelFormBox) findComponentInRoot("formBoxPaymentHeaderFormBox");
		}
		return formBoxPaymentHeaderFormBox;
	}

	protected HtmlSelectOneRadio getRadioPaymentMethod() {
		if (radioPaymentMethod == null) {
			radioPaymentMethod = (HtmlSelectOneRadio) findComponentInRoot("radioPaymentMethod");
		}
		return radioPaymentMethod;
	}

	protected HtmlOutputText getTextForceBillMeText() {
		if (textForceBillMeText == null) {
			textForceBillMeText = (HtmlOutputText) findComponentInRoot("textForceBillMeText");
		}
		return textForceBillMeText;
	}

	protected HtmlFormItem getFormItemCreditCardNumber() {
		if (formItemCreditCardNumber == null) {
			formItemCreditCardNumber = (HtmlFormItem) findComponentInRoot("formItemCreditCardNumber");
		}
		return formItemCreditCardNumber;
	}

	protected HtmlInputText getTextCreditCardNumber() {
		if (textCreditCardNumber == null) {
			textCreditCardNumber = (HtmlInputText) findComponentInRoot("textCreditCardNumber");
		}
		return textCreditCardNumber;
	}

	protected HtmlInputText getTextCVV() {
		if (textCVV == null) {
			textCVV = (HtmlInputText) findComponentInRoot("textCVV");
		}
		return textCVV;
	}

	protected HtmlSelectOneMenu getMenuCCExpireMonth() {
		if (menuCCExpireMonth == null) {
			menuCCExpireMonth = (HtmlSelectOneMenu) findComponentInRoot("menuCCExpireMonth");
		}
		return menuCCExpireMonth;
	}

	protected HtmlPanelGrid getPanelGridCreditCardImageGrid() {
		if (panelGridCreditCardImageGrid == null) {
			panelGridCreditCardImageGrid = (HtmlPanelGrid) findComponentInRoot("panelGridCreditCardImageGrid");
		}
		return panelGridCreditCardImageGrid;
	}

	protected HtmlJspPanel getJspPanelCreditCardImages() {
		if (jspPanelCreditCardImages == null) {
			jspPanelCreditCardImages = (HtmlJspPanel) findComponentInRoot("jspPanelCreditCardImages");
		}
		return jspPanelCreditCardImages;
	}

	protected HtmlGraphicImageEx getImageExAmEx1() {
		if (imageExAmEx1 == null) {
			imageExAmEx1 = (HtmlGraphicImageEx) findComponentInRoot("imageExAmEx1");
		}
		return imageExAmEx1;
	}

	protected HtmlSelectOneRadio getRadioRenewalOptions() {
		if (radioRenewalOptions == null) {
			radioRenewalOptions = (HtmlSelectOneRadio) findComponentInRoot("radioRenewalOptions");
		}
		return radioRenewalOptions;
	}

	protected HtmlSelectOneRadio getRadioRenewalOptionsGift() {
		if (radioRenewalOptionsGift == null) {
			radioRenewalOptionsGift = (HtmlSelectOneRadio) findComponentInRoot("radioRenewalOptionsGift");
		}
		return radioRenewalOptionsGift;
	}

	protected HtmlOutputFormat getRequiresEZPAYText() {
		if (requiresEZPAYText == null) {
			requiresEZPAYText = (HtmlOutputFormat) findComponentInRoot("requiresEZPAYText");
		}
		return requiresEZPAYText;
	}

	protected UIParameter getParam5() {
		if (param5 == null) {
			param5 = (UIParameter) findComponentInRoot("param5");
		}
		return param5;
	}

	protected HtmlOutputText getTextDisclaimerText() {
		if (textDisclaimerText == null) {
			textDisclaimerText = (HtmlOutputText) findComponentInRoot("textDisclaimerText");
		}
		return textDisclaimerText;
	}

	protected HtmlOutputText getTextOfferDisclaimerText() {
		if (textOfferDisclaimerText == null) {
			textOfferDisclaimerText = (HtmlOutputText) findComponentInRoot("textOfferDisclaimerText");
		}
		return textOfferDisclaimerText;
	}

	protected HtmlOutputText getTextCOPPAText() {
		if (textCOPPAText == null) {
			textCOPPAText = (HtmlOutputText) findComponentInRoot("textCOPPAText");
		}
		return textCOPPAText;
	}

	protected HtmlGraphicImageEx getImageEx3() {
		if (imageEx3 == null) {
			imageEx3 = (HtmlGraphicImageEx) findComponentInRoot("imageEx3");
		}
		return imageEx3;
	}

	protected HtmlPanelGrid getGridEZPayFinePrintGrid() {
		if (gridEZPayFinePrintGrid == null) {
			gridEZPayFinePrintGrid = (HtmlPanelGrid) findComponentInRoot("gridEZPayFinePrintGrid");
		}
		return gridEZPayFinePrintGrid;
	}

	protected HtmlOutputText getFormatEZPAYFinePrint() {
		if (formatEZPAYFinePrint == null) {
			formatEZPAYFinePrint = (HtmlOutputText) findComponentInRoot("formatEZPAYFinePrint");
		}
		return formatEZPAYFinePrint;
	}

	protected HtmlMessages getMessagesAllMesssages() {
		if (messagesAllMesssages == null) {
			messagesAllMesssages = (HtmlMessages) findComponentInRoot("messagesAllMesssages");
		}
		return messagesAllMesssages;
	}

	protected HtmlPanelGrid getGridCustServicePhone() {
		if (gridCustServicePhone == null) {
			gridCustServicePhone = (HtmlPanelGrid) findComponentInRoot("gridCustServicePhone");
		}
		return gridCustServicePhone;
	}

	protected HtmlOutputText getCustServicePhoneText() {
		if (custServicePhoneText == null) {
			custServicePhoneText = (HtmlOutputText) findComponentInRoot("custServicePhoneText");
		}
		return custServicePhoneText;
	}

	protected HtmlOutputLinkEx getLinkExRightColImageSpot1Link() {
		if (linkExRightColImageSpot1Link == null) {
			linkExRightColImageSpot1Link = (HtmlOutputLinkEx) findComponentInRoot("linkExRightColImageSpot1Link");
		}
		return linkExRightColImageSpot1Link;
	}

	protected HtmlGraphicImageEx getImageExRightColImage1() {
		if (imageExRightColImage1 == null) {
			imageExRightColImage1 = (HtmlGraphicImageEx) findComponentInRoot("imageExRightColImage1");
		}
		return imageExRightColImage1;
	}

	protected HtmlOutputLinkEx getLinkExVideoLink_B() {
		if (linkExVideoLink_B == null) {
			linkExVideoLink_B = (HtmlOutputLinkEx) findComponentInRoot("linkExVideoLink_B");
		}
		return linkExVideoLink_B;
	}

	protected HtmlGraphicImageEx getImageExVideoSweepsGraphic_B() {
		if (imageExVideoSweepsGraphic_B == null) {
			imageExVideoSweepsGraphic_B = (HtmlGraphicImageEx) findComponentInRoot("imageExVideoSweepsGraphic_B");
		}
		return imageExVideoSweepsGraphic_B;
	}

	protected HtmlOutputLinkEx getLinkExRightColSpot2Link() {
		if (linkExRightColSpot2Link == null) {
			linkExRightColSpot2Link = (HtmlOutputLinkEx) findComponentInRoot("linkExRightColSpot2Link");
		}
		return linkExRightColSpot2Link;
	}

	protected HtmlGraphicImageEx getImageExRightColImage2() {
		if (imageExRightColImage2 == null) {
			imageExRightColImage2 = (HtmlGraphicImageEx) findComponentInRoot("imageExRightColImage2");
		}
		return imageExRightColImage2;
	}

	protected HtmlOutputLinkEx getLinkExRightColSpot3Link() {
		if (linkExRightColSpot3Link == null) {
			linkExRightColSpot3Link = (HtmlOutputLinkEx) findComponentInRoot("linkExRightColSpot3Link");
		}
		return linkExRightColSpot3Link;
	}

	protected HtmlGraphicImageEx getImageExRightColImage3() {
		if (imageExRightColImage3 == null) {
			imageExRightColImage3 = (HtmlGraphicImageEx) findComponentInRoot("imageExRightColImage3");
		}
		return imageExRightColImage3;
	}

	protected HtmlPanelGrid getGridEZPayLearnMoreGrid2() {
		if (gridEZPayLearnMoreGrid2 == null) {
			gridEZPayLearnMoreGrid2 = (HtmlPanelGrid) findComponentInRoot("gridEZPayLearnMoreGrid2");
		}
		return gridEZPayLearnMoreGrid2;
	}

	protected HtmlOutputFormat getFormatEZPayHelpText() {
		if (formatEZPayHelpText == null) {
			formatEZPayHelpText = (HtmlOutputFormat) findComponentInRoot("formatEZPayHelpText");
		}
		return formatEZPayHelpText;
	}

	protected UIParameter getParam8() {
		if (param8 == null) {
			param8 = (UIParameter) findComponentInRoot("param8");
		}
		return param8;
	}

	protected HtmlCommandExButton getButton2() {
		if (button2 == null) {
			button2 = (HtmlCommandExButton) findComponentInRoot("button2");
		}
		return button2;
	}

	protected HtmlBehavior getBehaviorEZPay4() {
		if (behaviorEZPay4 == null) {
			behaviorEZPay4 = (HtmlBehavior) findComponentInRoot("behaviorEZPay4");
		}
		return behaviorEZPay4;
	}

	protected HtmlPanelGrid getGridStartDateLearnMoreGrid2() {
		if (gridStartDateLearnMoreGrid2 == null) {
			gridStartDateLearnMoreGrid2 = (HtmlPanelGrid) findComponentInRoot("gridStartDateLearnMoreGrid2");
		}
		return gridStartDateLearnMoreGrid2;
	}

	protected HtmlOutputFormat getFormatGiftFutureStartDateText() {
		if (formatGiftFutureStartDateText == null) {
			formatGiftFutureStartDateText = (HtmlOutputFormat) findComponentInRoot("formatGiftFutureStartDateText");
		}
		return formatGiftFutureStartDateText;
	}

	protected UIParameter getParam1() {
		if (param1 == null) {
			param1 = (UIParameter) findComponentInRoot("param1");
		}
		return param1;
	}

	protected HtmlCommandExButton getButtonStartDate2() {
		if (buttonStartDate2 == null) {
			buttonStartDate2 = (HtmlCommandExButton) findComponentInRoot("buttonStartDate2");
		}
		return buttonStartDate2;
	}

	protected HtmlBehavior getBehaviorStartDate4() {
		if (behaviorStartDate4 == null) {
			behaviorStartDate4 = (HtmlBehavior) findComponentInRoot("behaviorStartDate4");
		}
		return behaviorStartDate4;
	}

	protected HtmlPanelGrid getGrid2() {
		if (grid2 == null) {
			grid2 = (HtmlPanelGrid) findComponentInRoot("grid2");
		}
		return grid2;
	}

	protected HtmlOutputText getText3() {
		if (text3 == null) {
			text3 = (HtmlOutputText) findComponentInRoot("text3");
		}
		return text3;
	}

	protected HtmlCommandExButton getButton5() {
		if (button5 == null) {
			button5 = (HtmlCommandExButton) findComponentInRoot("button5");
		}
		return button5;
	}

	protected HtmlBehavior getBehavior5() {
		if (behavior5 == null) {
			behavior5 = (HtmlBehavior) findComponentInRoot("behavior5");
		}
		return behavior5;
	}

	protected HtmlPanelGrid getGridClubLearnMore() {
		if (gridClubLearnMore == null) {
			gridClubLearnMore = (HtmlPanelGrid) findComponentInRoot("gridClubLearnMore");
		}
		return gridClubLearnMore;
	}

	protected HtmlOutputText getTextClubLearnMore() {
		if (textClubLearnMore == null) {
			textClubLearnMore = (HtmlOutputText) findComponentInRoot("textClubLearnMore");
		}
		return textClubLearnMore;
	}

	protected HtmlCommandExButton getButton1() {
		if (button1 == null) {
			button1 = (HtmlCommandExButton) findComponentInRoot("button1");
		}
		return button1;
	}

	protected HtmlBehavior getBehavior2() {
		if (behavior2 == null) {
			behavior2 = (HtmlBehavior) findComponentInRoot("behavior2");
		}
		return behavior2;
	}

	protected HtmlPanelGrid getGridCVVLearn() {
		if (gridCVVLearn == null) {
			gridCVVLearn = (HtmlPanelGrid) findComponentInRoot("gridCVVLearn");
		}
		return gridCVVLearn;
	}

	protected HtmlPanelGroup getGroup7() {
		if (group7 == null) {
			group7 = (HtmlPanelGroup) findComponentInRoot("group7");
		}
		return group7;
	}

	protected HtmlPanelGroup getGroup6() {
		if (group6 == null) {
			group6 = (HtmlPanelGroup) findComponentInRoot("group6");
		}
		return group6;
	}

	protected HtmlOutputText getText4() {
		if (text4 == null) {
			text4 = (HtmlOutputText) findComponentInRoot("text4");
		}
		return text4;
	}

	protected HtmlCommandExButton getButton4() {
		if (button4 == null) {
			button4 = (HtmlCommandExButton) findComponentInRoot("button4");
		}
		return button4;
	}

	protected HtmlBehavior getBehavior4() {
		if (behavior4 == null) {
			behavior4 = (HtmlBehavior) findComponentInRoot("behavior4");
		}
		return behavior4;
	}

	protected HtmlPanelGroup getGroupDelMethod1() {
		if (groupDelMethod1 == null) {
			groupDelMethod1 = (HtmlPanelGroup) findComponentInRoot("groupDelMethod1");
		}
		return groupDelMethod1;
	}

	protected HtmlPanelGrid getGridDelMethod2() {
		if (gridDelMethod2 == null) {
			gridDelMethod2 = (HtmlPanelGrid) findComponentInRoot("gridDelMethod2");
		}
		return gridDelMethod2;
	}

	protected HtmlOutputText getTextDelMethodDetailHelp() {
		if (textDelMethodDetailHelp == null) {
			textDelMethodDetailHelp = (HtmlOutputText) findComponentInRoot("textDelMethodDetailHelp");
		}
		return textDelMethodDetailHelp;
	}

	protected HtmlBehavior getBehaviorDelMethod4() {
		if (behaviorDelMethod4 == null) {
			behaviorDelMethod4 = (HtmlBehavior) findComponentInRoot("behaviorDelMethod4");
		}
		return behaviorDelMethod4;
	}

	protected HtmlPanelGroup getGroupAddlAddrHelpGroup1() {
		if (groupAddlAddrHelpGroup1 == null) {
			groupAddlAddrHelpGroup1 = (HtmlPanelGroup) findComponentInRoot("groupAddlAddrHelpGroup1");
		}
		return groupAddlAddrHelpGroup1;
	}

	protected HtmlPanelGrid getGrid1() {
		if (grid1 == null) {
			grid1 = (HtmlPanelGrid) findComponentInRoot("grid1");
		}
		return grid1;
	}

	protected HtmlOutputText getTextAdditionalAddrLearnMoreInfoText() {
		if (textAdditionalAddrLearnMoreInfoText == null) {
			textAdditionalAddrLearnMoreInfoText = (HtmlOutputText) findComponentInRoot("textAdditionalAddrLearnMoreInfoText");
		}
		return textAdditionalAddrLearnMoreInfoText;
	}

	protected HtmlBehavior getBehavior3() {
		if (behavior3 == null) {
			behavior3 = (HtmlBehavior) findComponentInRoot("behavior3");
		}
		return behavior3;
	}

	protected HtmlPanelGroup getGroupSpecialOfferGroupBox() {
		if (groupSpecialOfferGroupBox == null) {
			groupSpecialOfferGroupBox = (HtmlPanelGroup) findComponentInRoot("groupSpecialOfferGroupBox");
		}
		return groupSpecialOfferGroupBox;
	}

	protected HtmlPanelGrid getGridSpecialOfferLayoutGrid() {
		if (gridSpecialOfferLayoutGrid == null) {
			gridSpecialOfferLayoutGrid = (HtmlPanelGrid) findComponentInRoot("gridSpecialOfferLayoutGrid");
		}
		return gridSpecialOfferLayoutGrid;
	}

	protected HtmlOutputLinkEx getLinkExCloseSpecialOffer() {
		if (linkExCloseSpecialOffer == null) {
			linkExCloseSpecialOffer = (HtmlOutputLinkEx) findComponentInRoot("linkExCloseSpecialOffer");
		}
		return linkExCloseSpecialOffer;
	}

	protected HtmlOutputText getTextCloseSpecialOfferText() {
		if (textCloseSpecialOfferText == null) {
			textCloseSpecialOfferText = (HtmlOutputText) findComponentInRoot("textCloseSpecialOfferText");
		}
		return textCloseSpecialOfferText;
	}

	protected HtmlGraphicImageEx getTooimageExSpecialOfferImage() {
		if (tooimageExSpecialOfferImage == null) {
			tooimageExSpecialOfferImage = (HtmlGraphicImageEx) findComponentInRoot("tooimageExSpecialOfferImage");
		}
		return tooimageExSpecialOfferImage;
	}

	protected HtmlOutputText getTextScriptInsert1() {
		if (textScriptInsert1 == null) {
			textScriptInsert1 = (HtmlOutputText) findComponentInRoot("textScriptInsert1");
		}
		return textScriptInsert1;
	}

	protected HtmlPanelGrid getGridMainBodyGrid() {
		if (gridMainBodyGrid == null) {
			gridMainBodyGrid = (HtmlPanelGrid) findComponentInRoot("gridMainBodyGrid");
		}
		return gridMainBodyGrid;
	}

	protected HtmlPanelFormBox getFormBoxTermsFormBox() {
		if (formBoxTermsFormBox == null) {
			formBoxTermsFormBox = (HtmlPanelFormBox) findComponentInRoot("formBoxTermsFormBox");
		}
		return formBoxTermsFormBox;
	}

	protected HtmlOutputText getTextEZPayExplanationMessage() {
		if (textEZPayExplanationMessage == null) {
			textEZPayExplanationMessage = (HtmlOutputText) findComponentInRoot("textEZPayExplanationMessage");
		}
		return textEZPayExplanationMessage;
	}

	protected HtmlOutputText getTextEZPayLearnMore() {
		if (textEZPayLearnMore == null) {
			textEZPayLearnMore = (HtmlOutputText) findComponentInRoot("textEZPayLearnMore");
		}
		return textEZPayLearnMore;
	}

	protected HtmlPanelGrid getGridOffOverrideGrid() {
		if (gridOffOverrideGrid == null) {
			gridOffOverrideGrid = (HtmlPanelGrid) findComponentInRoot("gridOffOverrideGrid");
		}
		return gridOffOverrideGrid;
	}

	protected HtmlCommandExButton getButtonProcessOfferCode() {
		if (buttonProcessOfferCode == null) {
			buttonProcessOfferCode = (HtmlCommandExButton) findComponentInRoot("buttonProcessOfferCode");
		}
		return buttonProcessOfferCode;
	}

	protected HtmlPanelGrid getGridPremiumPromotionGrid() {
		if (gridPremiumPromotionGrid == null) {
			gridPremiumPromotionGrid = (HtmlPanelGrid) findComponentInRoot("gridPremiumPromotionGrid");
		}
		return gridPremiumPromotionGrid;
	}

	protected HtmlPanelGrid getGridPremiumPromotionTermsGrid() {
		if (gridPremiumPromotionTermsGrid == null) {
			gridPremiumPromotionTermsGrid = (HtmlPanelGrid) findComponentInRoot("gridPremiumPromotionTermsGrid");
		}
		return gridPremiumPromotionTermsGrid;
	}

	protected HtmlOutputText getTextPromoDescription() {
		if (textPromoDescription == null) {
			textPromoDescription = (HtmlOutputText) findComponentInRoot("textPromoDescription");
		}
		return textPromoDescription;
	}

	protected HtmlOutputText getTextSelectedTermLabel() {
		if (textSelectedTermLabel == null) {
			textSelectedTermLabel = (HtmlOutputText) findComponentInRoot("textSelectedTermLabel");
		}
		return textSelectedTermLabel;
	}

	protected HtmlOutputText getTextSelectedTerm() {
		if (textSelectedTerm == null) {
			textSelectedTerm = (HtmlOutputText) findComponentInRoot("textSelectedTerm");
		}
		return textSelectedTerm;
	}

	protected HtmlOutputText getText5() {
		if (text5 == null) {
			text5 = (HtmlOutputText) findComponentInRoot("text5");
		}
		return text5;
	}

	protected HtmlOutputLinkEx getLinkExPromoPage() {
		if (linkExPromoPage == null) {
			linkExPromoPage = (HtmlOutputLinkEx) findComponentInRoot("linkExPromoPage");
		}
		return linkExPromoPage;
	}

	protected HtmlPanelGrid getGridQuantityInformation() {
		if (gridQuantityInformation == null) {
			gridQuantityInformation = (HtmlPanelGrid) findComponentInRoot("gridQuantityInformation");
		}
		return gridQuantityInformation;
	}

	protected HtmlOutputText getTextNumCopiesLearnMoreLink() {
		if (textNumCopiesLearnMoreLink == null) {
			textNumCopiesLearnMoreLink = (HtmlOutputText) findComponentInRoot("textNumCopiesLearnMoreLink");
		}
		return textNumCopiesLearnMoreLink;
	}

	protected HtmlPanelGrid getGridPartnerInformation() {
		if (gridPartnerInformation == null) {
			gridPartnerInformation = (HtmlPanelGrid) findComponentInRoot("gridPartnerInformation");
		}
		return gridPartnerInformation;
	}

	protected HtmlOutputText getTextProgInfoLearnMoreLink() {
		if (textProgInfoLearnMoreLink == null) {
			textProgInfoLearnMoreLink = (HtmlOutputText) findComponentInRoot("textProgInfoLearnMoreLink");
		}
		return textProgInfoLearnMoreLink;
	}

	protected HtmlPanelGrid getGridStartDateInformation() {
		if (gridStartDateInformation == null) {
			gridStartDateInformation = (HtmlPanelGrid) findComponentInRoot("gridStartDateInformation");
		}
		return gridStartDateInformation;
	}

	protected HtmlOutputText getTextStartDateLearnMore() {
		if (textStartDateLearnMore == null) {
			textStartDateLearnMore = (HtmlOutputText) findComponentInRoot("textStartDateLearnMore");
		}
		return textStartDateLearnMore;
	}

	protected HtmlPanelGrid getGridDeliveryInformation() {
		if (gridDeliveryInformation == null) {
			gridDeliveryInformation = (HtmlPanelGrid) findComponentInRoot("gridDeliveryInformation");
		}
		return gridDeliveryInformation;
	}

	protected HtmlFormItem getFormItemDeliveryLastName() {
		if (formItemDeliveryLastName == null) {
			formItemDeliveryLastName = (HtmlFormItem) findComponentInRoot("formItemDeliveryLastName");
		}
		return formItemDeliveryLastName;
	}

	protected HtmlFormItem getFormItemDeliveryCompanyName() {
		if (formItemDeliveryCompanyName == null) {
			formItemDeliveryCompanyName = (HtmlFormItem) findComponentInRoot("formItemDeliveryCompanyName");
		}
		return formItemDeliveryCompanyName;
	}

	protected HtmlFormItem getFormItemDeliveryAddress1() {
		if (formItemDeliveryAddress1 == null) {
			formItemDeliveryAddress1 = (HtmlFormItem) findComponentInRoot("formItemDeliveryAddress1");
		}
		return formItemDeliveryAddress1;
	}

	protected HtmlInputText getTextDeliveryAptSuite() {
		if (textDeliveryAptSuite == null) {
			textDeliveryAptSuite = (HtmlInputText) findComponentInRoot("textDeliveryAptSuite");
		}
		return textDeliveryAptSuite;
	}

	protected HtmlFormItem getFormItemDeliveryAddress2() {
		if (formItemDeliveryAddress2 == null) {
			formItemDeliveryAddress2 = (HtmlFormItem) findComponentInRoot("formItemDeliveryAddress2");
		}
		return formItemDeliveryAddress2;
	}

	protected HtmlOutputText getTextAddrLearnMore() {
		if (textAddrLearnMore == null) {
			textAddrLearnMore = (HtmlOutputText) findComponentInRoot("textAddrLearnMore");
		}
		return textAddrLearnMore;
	}

	protected HtmlFormItem getFormItemDeliveryCity() {
		if (formItemDeliveryCity == null) {
			formItemDeliveryCity = (HtmlFormItem) findComponentInRoot("formItemDeliveryCity");
		}
		return formItemDeliveryCity;
	}

	protected HtmlFormItem getFormItemDeliveryState() {
		if (formItemDeliveryState == null) {
			formItemDeliveryState = (HtmlFormItem) findComponentInRoot("formItemDeliveryState");
		}
		return formItemDeliveryState;
	}

	protected HtmlInputText getTextDeliveryZip() {
		if (textDeliveryZip == null) {
			textDeliveryZip = (HtmlInputText) findComponentInRoot("textDeliveryZip");
		}
		return textDeliveryZip;
	}

	protected HtmlFormItem getFormItemDeliveryPhone() {
		if (formItemDeliveryPhone == null) {
			formItemDeliveryPhone = (HtmlFormItem) findComponentInRoot("formItemDeliveryPhone");
		}
		return formItemDeliveryPhone;
	}

	protected HtmlInputText getTextDeliveryPhoneExchange() {
		if (textDeliveryPhoneExchange == null) {
			textDeliveryPhoneExchange = (HtmlInputText) findComponentInRoot("textDeliveryPhoneExchange");
		}
		return textDeliveryPhoneExchange;
	}

	protected HtmlInputText getTextDeliveryPhoneExtension() {
		if (textDeliveryPhoneExtension == null) {
			textDeliveryPhoneExtension = (HtmlInputText) findComponentInRoot("textDeliveryPhoneExtension");
		}
		return textDeliveryPhoneExtension;
	}

	protected HtmlFormItem getFormItemDeliveryWorkPhone() {
		if (formItemDeliveryWorkPhone == null) {
			formItemDeliveryWorkPhone = (HtmlFormItem) findComponentInRoot("formItemDeliveryWorkPhone");
		}
		return formItemDeliveryWorkPhone;
	}

	protected HtmlInputText getTextDeliveryWorkPhoneExchange() {
		if (textDeliveryWorkPhoneExchange == null) {
			textDeliveryWorkPhoneExchange = (HtmlInputText) findComponentInRoot("textDeliveryWorkPhoneExchange");
		}
		return textDeliveryWorkPhoneExchange;
	}

	protected HtmlInputText getTextDeliveryWorkPhoneExtension() {
		if (textDeliveryWorkPhoneExtension == null) {
			textDeliveryWorkPhoneExtension = (HtmlInputText) findComponentInRoot("textDeliveryWorkPhoneExtension");
		}
		return textDeliveryWorkPhoneExtension;
	}

	protected HtmlFormItem getFormItemEmailAddress() {
		if (formItemEmailAddress == null) {
			formItemEmailAddress = (HtmlFormItem) findComponentInRoot("formItemEmailAddress");
		}
		return formItemEmailAddress;
	}

	protected HtmlFormItem getFormItemEmailAddressConfirm() {
		if (formItemEmailAddressConfirm == null) {
			formItemEmailAddressConfirm = (HtmlFormItem) findComponentInRoot("formItemEmailAddressConfirm");
		}
		return formItemEmailAddressConfirm;
	}

	protected HtmlPanelGrid getGridDetermineDeliveryOuterGrid() {
		if (gridDetermineDeliveryOuterGrid == null) {
			gridDetermineDeliveryOuterGrid = (HtmlPanelGrid) findComponentInRoot("gridDetermineDeliveryOuterGrid");
		}
		return gridDetermineDeliveryOuterGrid;
	}

	protected HtmlOutputText getTextDeliveryMethodTextDeterminedValue() {
		if (textDeliveryMethodTextDeterminedValue == null) {
			textDeliveryMethodTextDeterminedValue = (HtmlOutputText) findComponentInRoot("textDeliveryMethodTextDeterminedValue");
		}
		return textDeliveryMethodTextDeterminedValue;
	}

	protected HtmlCommandExButton getButtonGetDeliveryMethodButton() {
		if (buttonGetDeliveryMethodButton == null) {
			buttonGetDeliveryMethodButton = (HtmlCommandExButton) findComponentInRoot("buttonGetDeliveryMethodButton");
		}
		return buttonGetDeliveryMethodButton;
	}

	protected HtmlPanelGrid getPanelGridBillingDifferentThanDelGrid() {
		if (panelGridBillingDifferentThanDelGrid == null) {
			panelGridBillingDifferentThanDelGrid = (HtmlPanelGrid) findComponentInRoot("panelGridBillingDifferentThanDelGrid");
		}
		return panelGridBillingDifferentThanDelGrid;
	}

	protected HtmlPanelGrid getGridBillingAddress() {
		if (gridBillingAddress == null) {
			gridBillingAddress = (HtmlPanelGrid) findComponentInRoot("gridBillingAddress");
		}
		return gridBillingAddress;
	}

	protected HtmlFormItem getFormItemBillingLastName() {
		if (formItemBillingLastName == null) {
			formItemBillingLastName = (HtmlFormItem) findComponentInRoot("formItemBillingLastName");
		}
		return formItemBillingLastName;
	}

	protected HtmlFormItem getFormItemBillingCompanyName() {
		if (formItemBillingCompanyName == null) {
			formItemBillingCompanyName = (HtmlFormItem) findComponentInRoot("formItemBillingCompanyName");
		}
		return formItemBillingCompanyName;
	}

	protected HtmlFormItem getFormItemBillingAddress1() {
		if (formItemBillingAddress1 == null) {
			formItemBillingAddress1 = (HtmlFormItem) findComponentInRoot("formItemBillingAddress1");
		}
		return formItemBillingAddress1;
	}

	protected HtmlInputText getTextBillingAptSuite() {
		if (textBillingAptSuite == null) {
			textBillingAptSuite = (HtmlInputText) findComponentInRoot("textBillingAptSuite");
		}
		return textBillingAptSuite;
	}

	protected HtmlFormItem getFormItemBillingAddress2() {
		if (formItemBillingAddress2 == null) {
			formItemBillingAddress2 = (HtmlFormItem) findComponentInRoot("formItemBillingAddress2");
		}
		return formItemBillingAddress2;
	}

	protected HtmlFormItem getFormItemBillingCity() {
		if (formItemBillingCity == null) {
			formItemBillingCity = (HtmlFormItem) findComponentInRoot("formItemBillingCity");
		}
		return formItemBillingCity;
	}

	protected HtmlFormItem getFormItemBillingState() {
		if (formItemBillingState == null) {
			formItemBillingState = (HtmlFormItem) findComponentInRoot("formItemBillingState");
		}
		return formItemBillingState;
	}

	protected HtmlInputText getTextBillingZipCode() {
		if (textBillingZipCode == null) {
			textBillingZipCode = (HtmlInputText) findComponentInRoot("textBillingZipCode");
		}
		return textBillingZipCode;
	}

	protected HtmlFormItem getFormItemBillingTelephone() {
		if (formItemBillingTelephone == null) {
			formItemBillingTelephone = (HtmlFormItem) findComponentInRoot("formItemBillingTelephone");
		}
		return formItemBillingTelephone;
	}

	protected HtmlInputText getTextBillingPhoneExchange() {
		if (textBillingPhoneExchange == null) {
			textBillingPhoneExchange = (HtmlInputText) findComponentInRoot("textBillingPhoneExchange");
		}
		return textBillingPhoneExchange;
	}

	protected HtmlInputText getTextBillingPhoneExtension() {
		if (textBillingPhoneExtension == null) {
			textBillingPhoneExtension = (HtmlInputText) findComponentInRoot("textBillingPhoneExtension");
		}
		return textBillingPhoneExtension;
	}

	protected HtmlFormItem getGiftSubscriptionFormItem() {
		if (giftSubscriptionFormItem == null) {
			giftSubscriptionFormItem = (HtmlFormItem) findComponentInRoot("giftSubscriptionFormItem");
		}
		return giftSubscriptionFormItem;
	}

	protected HtmlFormItem getFormItemPayersEmailAddress() {
		if (formItemPayersEmailAddress == null) {
			formItemPayersEmailAddress = (HtmlFormItem) findComponentInRoot("formItemPayersEmailAddress");
		}
		return formItemPayersEmailAddress;
	}

	protected HtmlPanelGrid getGridPaymentInformationGridLabelGrid() {
		if (gridPaymentInformationGridLabelGrid == null) {
			gridPaymentInformationGridLabelGrid = (HtmlPanelGrid) findComponentInRoot("gridPaymentInformationGridLabelGrid");
		}
		return gridPaymentInformationGridLabelGrid;
	}

	protected HtmlFormItem getFormItemBillMeFormItem() {
		if (formItemBillMeFormItem == null) {
			formItemBillMeFormItem = (HtmlFormItem) findComponentInRoot("formItemBillMeFormItem");
		}
		return formItemBillMeFormItem;
	}

	protected HtmlPanelGrid getGridForceBillMeInfoGrid() {
		if (gridForceBillMeInfoGrid == null) {
			gridForceBillMeInfoGrid = (HtmlPanelGrid) findComponentInRoot("gridForceBillMeInfoGrid");
		}
		return gridForceBillMeInfoGrid;
	}

	protected HtmlPanelGrid getGridPaymentInformationGrid() {
		if (gridPaymentInformationGrid == null) {
			gridPaymentInformationGrid = (HtmlPanelGrid) findComponentInRoot("gridPaymentInformationGrid");
		}
		return gridPaymentInformationGrid;
	}

	protected HtmlPanelFormBox getFormBoxPaymentInfo() {
		if (formBoxPaymentInfo == null) {
			formBoxPaymentInfo = (HtmlPanelFormBox) findComponentInRoot("formBoxPaymentInfo");
		}
		return formBoxPaymentInfo;
	}

	protected HtmlFormItem getFormItemCreditCardCVV() {
		if (formItemCreditCardCVV == null) {
			formItemCreditCardCVV = (HtmlFormItem) findComponentInRoot("formItemCreditCardCVV");
		}
		return formItemCreditCardCVV;
	}

	protected HtmlOutputText getTextCVVLearnMore() {
		if (textCVVLearnMore == null) {
			textCVVLearnMore = (HtmlOutputText) findComponentInRoot("textCVVLearnMore");
		}
		return textCVVLearnMore;
	}

	protected HtmlFormItem getFormItemCCExpirationDate() {
		if (formItemCCExpirationDate == null) {
			formItemCCExpirationDate = (HtmlFormItem) findComponentInRoot("formItemCCExpirationDate");
		}
		return formItemCCExpirationDate;
	}

	protected HtmlSelectOneMenu getMenuCCExpireYear() {
		if (menuCCExpireYear == null) {
			menuCCExpireYear = (HtmlSelectOneMenu) findComponentInRoot("menuCCExpireYear");
		}
		return menuCCExpireYear;
	}

	protected HtmlGraphicImageEx getImageExDiscover1() {
		if (imageExDiscover1 == null) {
			imageExDiscover1 = (HtmlGraphicImageEx) findComponentInRoot("imageExDiscover1");
		}
		return imageExDiscover1;
	}

	protected HtmlGraphicImageEx getImageExMasterCard1() {
		if (imageExMasterCard1 == null) {
			imageExMasterCard1 = (HtmlGraphicImageEx) findComponentInRoot("imageExMasterCard1");
		}
		return imageExMasterCard1;
	}

	protected HtmlGraphicImageEx getImageExVisaLogo1() {
		if (imageExVisaLogo1 == null) {
			imageExVisaLogo1 = (HtmlGraphicImageEx) findComponentInRoot("imageExVisaLogo1");
		}
		return imageExVisaLogo1;
	}

	protected HtmlPanelGrid getGridEZPAYOptionsGrid() {
		if (gridEZPAYOptionsGrid == null) {
			gridEZPAYOptionsGrid = (HtmlPanelGrid) findComponentInRoot("gridEZPAYOptionsGrid");
		}
		return gridEZPAYOptionsGrid;
	}

	protected HtmlPanelGrid getGridEZPAYOptionsGridGift() {
		if (gridEZPAYOptionsGridGift == null) {
			gridEZPAYOptionsGridGift = (HtmlPanelGrid) findComponentInRoot("gridEZPAYOptionsGridGift");
		}
		return gridEZPAYOptionsGridGift;
	}

	protected HtmlPanelGrid getGridEZPAYRequiredGrid() {
		if (gridEZPAYRequiredGrid == null) {
			gridEZPAYRequiredGrid = (HtmlPanelGrid) findComponentInRoot("gridEZPAYRequiredGrid");
		}
		return gridEZPAYRequiredGrid;
	}

	protected HtmlPanelGrid getGridTrialDisclaimerGrid() {
		if (gridTrialDisclaimerGrid == null) {
			gridTrialDisclaimerGrid = (HtmlPanelGrid) findComponentInRoot("gridTrialDisclaimerGrid");
		}
		return gridTrialDisclaimerGrid;
	}

	protected HtmlPanelGrid getGridOfferDisclaimerGrid() {
		if (gridOfferDisclaimerGrid == null) {
			gridOfferDisclaimerGrid = (HtmlPanelGrid) findComponentInRoot("gridOfferDisclaimerGrid");
		}
		return gridOfferDisclaimerGrid;
	}

	protected HtmlPanelGrid getCOPPAGrid() {
		if (COPPAGrid == null) {
			COPPAGrid = (HtmlPanelGrid) findComponentInRoot("COPPAGrid");
		}
		return COPPAGrid;
	}

	protected HtmlPanelGrid getPanelGridFormSubmissionGrid() {
		if (panelGridFormSubmissionGrid == null) {
			panelGridFormSubmissionGrid = (HtmlPanelGrid) findComponentInRoot("panelGridFormSubmissionGrid");
		}
		return panelGridFormSubmissionGrid;
	}

	protected HtmlCommandExButton getButtonPlaceOrder() {
		if (buttonPlaceOrder == null) {
			buttonPlaceOrder = (HtmlCommandExButton) findComponentInRoot("buttonPlaceOrder");
		}
		return buttonPlaceOrder;
	}

	protected HtmlJspPanel getJspPanelGeoTrustPanel() {
		if (jspPanelGeoTrustPanel == null) {
			jspPanelGeoTrustPanel = (HtmlJspPanel) findComponentInRoot("jspPanelGeoTrustPanel");
		}
		return jspPanelGeoTrustPanel;
	}

	protected HtmlGraphicImageEx getImageEx33333() {
		if (imageEx33333 == null) {
			imageEx33333 = (HtmlGraphicImageEx) findComponentInRoot("imageEx33333");
		}
		return imageEx33333;
	}

	protected HtmlJspPanel getPleaseWaitPanel() {
		if (pleaseWaitPanel == null) {
			pleaseWaitPanel = (HtmlJspPanel) findComponentInRoot("pleaseWaitPanel");
		}
		return pleaseWaitPanel;
	}

	protected HtmlPanelGrid getGridRequiredEZPayFinePrintGrid() {
		if (gridRequiredEZPayFinePrintGrid == null) {
			gridRequiredEZPayFinePrintGrid = (HtmlPanelGrid) findComponentInRoot("gridRequiredEZPayFinePrintGrid");
		}
		return gridRequiredEZPayFinePrintGrid;
	}

	protected HtmlPanelGrid getRightColImageSpot1() {
		if (rightColImageSpot1 == null) {
			rightColImageSpot1 = (HtmlPanelGrid) findComponentInRoot("rightColImageSpot1");
		}
		return rightColImageSpot1;
	}

	protected HtmlGraphicImageEx getImageExRightColImage1NoLink() {
		if (imageExRightColImage1NoLink == null) {
			imageExRightColImage1NoLink = (HtmlGraphicImageEx) findComponentInRoot("imageExRightColImage1NoLink");
		}
		return imageExRightColImage1NoLink;
	}

	protected HtmlPanelGrid getGridRightColHTMLSpot1Grid() {
		if (gridRightColHTMLSpot1Grid == null) {
			gridRightColHTMLSpot1Grid = (HtmlPanelGrid) findComponentInRoot("gridRightColHTMLSpot1Grid");
		}
		return gridRightColHTMLSpot1Grid;
	}

	protected HtmlPanelGrid getGridVideoGrid() {
		if (gridVideoGrid == null) {
			gridVideoGrid = (HtmlPanelGrid) findComponentInRoot("gridVideoGrid");
		}
		return gridVideoGrid;
	}

	protected HtmlOutputText getTextEEVideoText() {
		if (textEEVideoText == null) {
			textEEVideoText = (HtmlOutputText) findComponentInRoot("textEEVideoText");
		}
		return textEEVideoText;
	}

	protected HtmlPanelGrid getRightColImageSpot2() {
		if (rightColImageSpot2 == null) {
			rightColImageSpot2 = (HtmlPanelGrid) findComponentInRoot("rightColImageSpot2");
		}
		return rightColImageSpot2;
	}

	protected HtmlGraphicImageEx getImageExRightColImage2NoLink() {
		if (imageExRightColImage2NoLink == null) {
			imageExRightColImage2NoLink = (HtmlGraphicImageEx) findComponentInRoot("imageExRightColImage2NoLink");
		}
		return imageExRightColImage2NoLink;
	}

	protected HtmlPanelGrid getGridRightColHTMLSpot2Grid() {
		if (gridRightColHTMLSpot2Grid == null) {
			gridRightColHTMLSpot2Grid = (HtmlPanelGrid) findComponentInRoot("gridRightColHTMLSpot2Grid");
		}
		return gridRightColHTMLSpot2Grid;
	}

	protected HtmlPanelGrid getRightColImageSpot3() {
		if (rightColImageSpot3 == null) {
			rightColImageSpot3 = (HtmlPanelGrid) findComponentInRoot("rightColImageSpot3");
		}
		return rightColImageSpot3;
	}

	protected HtmlGraphicImageEx getImageExRightColImage3NoLink() {
		if (imageExRightColImage3NoLink == null) {
			imageExRightColImage3NoLink = (HtmlGraphicImageEx) findComponentInRoot("imageExRightColImage3NoLink");
		}
		return imageExRightColImage3NoLink;
	}

	protected HtmlPanelGrid getGridGeoTrust() {
		if (gridGeoTrust == null) {
			gridGeoTrust = (HtmlPanelGrid) findComponentInRoot("gridGeoTrust");
		}
		return gridGeoTrust;
	}

	protected HtmlPanelDialog getDialogEZPayLearnMore1() {
		if (dialogEZPayLearnMore1 == null) {
			dialogEZPayLearnMore1 = (HtmlPanelDialog) findComponentInRoot("dialogEZPayLearnMore1");
		}
		return dialogEZPayLearnMore1;
	}

	protected HtmlPanelGroup getGroup1() {
		if (group1 == null) {
			group1 = (HtmlPanelGroup) findComponentInRoot("group1");
		}
		return group1;
	}

	protected HtmlPanelDialog getDialogStartDateLearnMore1() {
		if (dialogStartDateLearnMore1 == null) {
			dialogStartDateLearnMore1 = (HtmlPanelDialog) findComponentInRoot("dialogStartDateLearnMore1");
		}
		return dialogStartDateLearnMore1;
	}

	protected UIParameter getParam2() {
		if (param2 == null) {
			param2 = (UIParameter) findComponentInRoot("param2");
		}
		return param2;
	}

	protected UIParameter getParam3() {
		if (param3 == null) {
			param3 = (UIParameter) findComponentInRoot("param3");
		}
		return param3;
	}

	protected HtmlPanelGroup getGroupStartDate1() {
		if (groupStartDate1 == null) {
			groupStartDate1 = (HtmlPanelGroup) findComponentInRoot("groupStartDate1");
		}
		return groupStartDate1;
	}

	protected HtmlPanelDialog getDialogQty() {
		if (dialogQty == null) {
			dialogQty = (HtmlPanelDialog) findComponentInRoot("dialogQty");
		}
		return dialogQty;
	}

	protected HtmlPanelGroup getGroup2() {
		if (group2 == null) {
			group2 = (HtmlPanelGroup) findComponentInRoot("group2");
		}
		return group2;
	}

	protected HtmlPanelDialog getDialogClubNum() {
		if (dialogClubNum == null) {
			dialogClubNum = (HtmlPanelDialog) findComponentInRoot("dialogClubNum");
		}
		return dialogClubNum;
	}

	protected HtmlPanelGroup getGroup3() {
		if (group3 == null) {
			group3 = (HtmlPanelGroup) findComponentInRoot("group3");
		}
		return group3;
	}

	protected HtmlPanelDialog getDialogCVV() {
		if (dialogCVV == null) {
			dialogCVV = (HtmlPanelDialog) findComponentInRoot("dialogCVV");
		}
		return dialogCVV;
	}

	protected HtmlPanelGroup getGroup5() {
		if (group5 == null) {
			group5 = (HtmlPanelGroup) findComponentInRoot("group5");
		}
		return group5;
	}

	protected HtmlPanelDialog getDialogDelMethodDialog() {
		if (dialogDelMethodDialog == null) {
			dialogDelMethodDialog = (HtmlPanelDialog) findComponentInRoot("dialogDelMethodDialog");
		}
		return dialogDelMethodDialog;
	}

	protected HtmlCommandExButton getButtonDelMethod() {
		if (buttonDelMethod == null) {
			buttonDelMethod = (HtmlCommandExButton) findComponentInRoot("buttonDelMethod");
		}
		return buttonDelMethod;
	}

	protected HtmlPanelDialog getDialogAdditionalAddrHelp() {
		if (dialogAdditionalAddrHelp == null) {
			dialogAdditionalAddrHelp = (HtmlPanelDialog) findComponentInRoot("dialogAdditionalAddrHelp");
		}
		return dialogAdditionalAddrHelp;
	}

	protected HtmlCommandExButton getButton3() {
		if (button3 == null) {
			button3 = (HtmlCommandExButton) findComponentInRoot("button3");
		}
		return button3;
	}

	protected HtmlInputHelperSetFocus getSetFocus1() {
		if (setFocus1 == null) {
			setFocus1 = (HtmlInputHelperSetFocus) findComponentInRoot("setFocus1");
		}
		return setFocus1;
	}

	protected HtmlInputHidden getStartDate() {
		if (startDate == null) {
			startDate = (HtmlInputHidden) findComponentInRoot("startDate");
		}
		return startDate;
	}

	protected HtmlJspPanel getJspPanelPopOverlayPanel() {
		if (jspPanelPopOverlayPanel == null) {
			jspPanelPopOverlayPanel = (HtmlJspPanel) findComponentInRoot("jspPanelPopOverlayPanel");
		}
		return jspPanelPopOverlayPanel;
	}

	protected HtmlOutputLinkEx getLinkExSpecialOfferLink() {
		if (linkExSpecialOfferLink == null) {
			linkExSpecialOfferLink = (HtmlOutputLinkEx) findComponentInRoot("linkExSpecialOfferLink");
		}
		return linkExSpecialOfferLink;
	}

	protected HtmlInputHidden getShowOverlayPopUp() {
		if (showOverlayPopUp == null) {
			showOverlayPopUp = (HtmlInputHidden) findComponentInRoot("showOverlayPopUp");
		}
		return showOverlayPopUp;
	}

	protected HtmlInputHidden getIsForceBillMe() {
		if (isForceBillMe == null) {
			isForceBillMe = (HtmlInputHidden) findComponentInRoot("isForceBillMe");
		}
		return isForceBillMe;
	}

	protected HtmlInputHidden getPaymentMethodHidden() {
		if (paymentMethodHidden == null) {
			paymentMethodHidden = (HtmlInputHidden) findComponentInRoot("paymentMethodHidden");
		}
		return paymentMethodHidden;
	}

	protected HtmlBehaviorKeyPress getBehaviorKeyPress1() {
		if (behaviorKeyPress1 == null) {
			behaviorKeyPress1 = (HtmlBehaviorKeyPress) findComponentInRoot("behaviorKeyPress1");
		}
		return behaviorKeyPress1;
	}

	protected HtmlInputHidden getNumberEZPayFreeWeeks() {
		if (numberEZPayFreeWeeks == null) {
			numberEZPayFreeWeeks = (HtmlInputHidden) findComponentInRoot("numberEZPayFreeWeeks");
		}
		return numberEZPayFreeWeeks;
	}
}