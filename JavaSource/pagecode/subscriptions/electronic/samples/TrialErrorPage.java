/**
 * 
 */
package pagecode.subscriptions.electronic.samples;

import pagecode.PageCodeBase;
import com.ibm.faces.component.html.HtmlScriptCollector;
import javax.faces.component.html.HtmlOutputText;

/**
 * @author aeast
 * 
 */
public class TrialErrorPage extends PageCodeBase {

	protected HtmlScriptCollector scriptCollector1;
	protected HtmlOutputText textMainTableHeader;
	protected HtmlOutputText textInformation;

	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlOutputText getTextMainTableHeader() {
		if (textMainTableHeader == null) {
			textMainTableHeader = (HtmlOutputText) findComponentInRoot("textMainTableHeader");
		}
		return textMainTableHeader;
	}

	protected HtmlOutputText getTextInformation() {
		if (textInformation == null) {
			textInformation = (HtmlOutputText) findComponentInRoot("textInformation");
		}
		return textInformation;
	}

}