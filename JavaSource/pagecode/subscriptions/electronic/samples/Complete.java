/**
 * 
 */
package pagecode.subscriptions.electronic.samples;

import pagecode.PageCodeBase;
import com.ibm.faces.component.html.HtmlScriptCollector;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlPanelGrid;
import com.ibm.faces.component.html.HtmlJspPanel;
import javax.faces.component.html.HtmlOutputText;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import javax.faces.component.html.HtmlPanelGroup;
import com.ibm.faces.component.html.HtmlPanelLayout;
import com.ibm.faces.component.html.HtmlGraphicImageEx;
import com.usatoday.esub.trials.handlers.TrialCustomerHandler;
import com.usatoday.esub.trials.handlers.PartnerHandler;

/**
 * @author aeast
 * 
 */
public class Complete extends PageCodeBase {

	protected HtmlScriptCollector scriptCollectorSampleComplete;
	protected HtmlForm formSampleComplete;
	protected HtmlOutputText textMainTableHeader;
	protected HtmlPanelGrid gridThankYouInformation;
	protected HtmlJspPanel jspPanelThankYouTextPanel;
	protected HtmlOutputText textThankYouText2;
	protected HtmlOutputText textReadNowTextLink;
	protected HtmlOutputText textThankYouBullet2a;
	protected HtmlOutputText textCustServiceLink;
	protected HtmlOutputText textThankYouText3;
	protected HtmlOutputText textThankYouText4;
	protected HtmlOutputLinkEx linkExPrintPageLink;
	protected HtmlOutputText textPrintPageLinkText;
	protected HtmlPanelGrid gridAccountInformation;
	protected HtmlPanelGroup groupAccountInfoHeaderGroup;
	protected HtmlPanelLayout layoutOrderInfo1;
	protected HtmlOutputText textOrderInformationHeaderlabel;
	protected HtmlPanelGroup groupDeliveryInfoGroup;
	protected HtmlPanelLayout layoutOrderInfo2;
	protected HtmlOutputText textOrderInformationDeliveryHeaderlabel;
	protected HtmlPanelGrid gridRightPanelGrid;
	protected HtmlJspPanel jspPanelRightPanelTop;
	protected HtmlOutputLinkEx linkExReadNowLink;
	protected HtmlGraphicImageEx imageExReadNowButtonImage;
	protected HtmlOutputLinkEx linkExReadNowTextLink;
	protected HtmlOutputLinkEx linkExCustServiceLink;
	protected HtmlOutputText textThankYouBullet2b;
	protected HtmlGraphicImageEx imageExFillerImage;
	protected HtmlPanelGrid gridPrintButtonGrid;
	protected HtmlJspPanel jspPanelOrderDataPanel;
	protected HtmlOutputText textDateLabel;
	protected HtmlOutputText textDateOfToday;
	protected HtmlOutputText textProductLabel;
	protected HtmlOutputText textProdName;
	protected HtmlOutputText textTermLabel;
	protected HtmlOutputText textSelectedTerm;
	protected HtmlPanelGrid gridAccountInformationDelivery;
	protected HtmlOutputText textDeliveryEmailLabel;
	protected HtmlOutputText textDeliveryEmail;
	protected HtmlOutputText textDeliveryNameLabel;
	protected HtmlOutputText textDeliveryName;
	protected HtmlOutputText textDeliveryAddr1Label;
	protected HtmlOutputText textDeliveryAddr1;
	protected HtmlOutputText textDeliveryAptSuiteLabel;
	protected HtmlOutputText textDeliveryAptSuite;
	protected HtmlOutputText textDeliveryAddr2Label;
	protected HtmlOutputText textDeliveryAddr2;
	protected HtmlOutputText textDeliveryAddrStateZipLabel;
	protected HtmlOutputText textDeliveryStateZip;
	protected TrialCustomerHandler trialCustomerHandler;
	protected HtmlJspPanel jspPanelPartnerTextPanel;
	protected HtmlOutputText textCustomPartnerText;
	protected PartnerHandler trialPartnerHandler;
	protected HtmlScriptCollector scriptCollectorTopNavCollector;
	protected HtmlJspPanel jspPanelTopNavCustomPanel;
	protected HtmlOutputLinkEx linkExTopNavLogoLink;
	protected HtmlGraphicImageEx imageExPartnerLogo;
	protected HtmlGraphicImageEx imageExPartnerLogoNoLink;
	protected HtmlOutputLinkEx linkExOpenDesktopVersion;
	protected HtmlOutputText text1111;
	protected HtmlOutputText text2;
	protected HtmlOutputLinkEx linkExOpenMobileVersion;
	protected HtmlOutputText text1;

	protected HtmlScriptCollector getScriptCollectorSampleComplete() {
		if (scriptCollectorSampleComplete == null) {
			scriptCollectorSampleComplete = (HtmlScriptCollector) findComponentInRoot("scriptCollectorSampleComplete");
		}
		return scriptCollectorSampleComplete;
	}

	protected HtmlForm getFormSampleComplete() {
		if (formSampleComplete == null) {
			formSampleComplete = (HtmlForm) findComponentInRoot("formSampleComplete");
		}
		return formSampleComplete;
	}

	protected HtmlOutputText getTextMainTableHeader() {
		if (textMainTableHeader == null) {
			textMainTableHeader = (HtmlOutputText) findComponentInRoot("textMainTableHeader");
		}
		return textMainTableHeader;
	}

	protected HtmlPanelGrid getGridThankYouInformation() {
		if (gridThankYouInformation == null) {
			gridThankYouInformation = (HtmlPanelGrid) findComponentInRoot("gridThankYouInformation");
		}
		return gridThankYouInformation;
	}

	protected HtmlJspPanel getJspPanelThankYouTextPanel() {
		if (jspPanelThankYouTextPanel == null) {
			jspPanelThankYouTextPanel = (HtmlJspPanel) findComponentInRoot("jspPanelThankYouTextPanel");
		}
		return jspPanelThankYouTextPanel;
	}

	protected HtmlOutputText getTextThankYouText2() {
		if (textThankYouText2 == null) {
			textThankYouText2 = (HtmlOutputText) findComponentInRoot("textThankYouText2");
		}
		return textThankYouText2;
	}

	protected HtmlOutputText getTextReadNowTextLink() {
		if (textReadNowTextLink == null) {
			textReadNowTextLink = (HtmlOutputText) findComponentInRoot("textReadNowTextLink");
		}
		return textReadNowTextLink;
	}

	protected HtmlOutputText getTextThankYouBullet2a() {
		if (textThankYouBullet2a == null) {
			textThankYouBullet2a = (HtmlOutputText) findComponentInRoot("textThankYouBullet2a");
		}
		return textThankYouBullet2a;
	}

	protected HtmlOutputText getTextCustServiceLink() {
		if (textCustServiceLink == null) {
			textCustServiceLink = (HtmlOutputText) findComponentInRoot("textCustServiceLink");
		}
		return textCustServiceLink;
	}

	protected HtmlOutputText getTextThankYouText3() {
		if (textThankYouText3 == null) {
			textThankYouText3 = (HtmlOutputText) findComponentInRoot("textThankYouText3");
		}
		return textThankYouText3;
	}

	protected HtmlOutputText getTextThankYouText4() {
		if (textThankYouText4 == null) {
			textThankYouText4 = (HtmlOutputText) findComponentInRoot("textThankYouText4");
		}
		return textThankYouText4;
	}

	protected HtmlOutputLinkEx getLinkExPrintPageLink() {
		if (linkExPrintPageLink == null) {
			linkExPrintPageLink = (HtmlOutputLinkEx) findComponentInRoot("linkExPrintPageLink");
		}
		return linkExPrintPageLink;
	}

	protected HtmlOutputText getTextPrintPageLinkText() {
		if (textPrintPageLinkText == null) {
			textPrintPageLinkText = (HtmlOutputText) findComponentInRoot("textPrintPageLinkText");
		}
		return textPrintPageLinkText;
	}

	protected HtmlPanelGrid getGridAccountInformation() {
		if (gridAccountInformation == null) {
			gridAccountInformation = (HtmlPanelGrid) findComponentInRoot("gridAccountInformation");
		}
		return gridAccountInformation;
	}

	protected HtmlPanelGroup getGroupAccountInfoHeaderGroup() {
		if (groupAccountInfoHeaderGroup == null) {
			groupAccountInfoHeaderGroup = (HtmlPanelGroup) findComponentInRoot("groupAccountInfoHeaderGroup");
		}
		return groupAccountInfoHeaderGroup;
	}

	protected HtmlPanelLayout getLayoutOrderInfo1() {
		if (layoutOrderInfo1 == null) {
			layoutOrderInfo1 = (HtmlPanelLayout) findComponentInRoot("layoutOrderInfo1");
		}
		return layoutOrderInfo1;
	}

	protected HtmlOutputText getTextOrderInformationHeaderlabel() {
		if (textOrderInformationHeaderlabel == null) {
			textOrderInformationHeaderlabel = (HtmlOutputText) findComponentInRoot("textOrderInformationHeaderlabel");
		}
		return textOrderInformationHeaderlabel;
	}

	protected HtmlPanelGroup getGroupDeliveryInfoGroup() {
		if (groupDeliveryInfoGroup == null) {
			groupDeliveryInfoGroup = (HtmlPanelGroup) findComponentInRoot("groupDeliveryInfoGroup");
		}
		return groupDeliveryInfoGroup;
	}

	protected HtmlPanelLayout getLayoutOrderInfo2() {
		if (layoutOrderInfo2 == null) {
			layoutOrderInfo2 = (HtmlPanelLayout) findComponentInRoot("layoutOrderInfo2");
		}
		return layoutOrderInfo2;
	}

	protected HtmlOutputText getTextOrderInformationDeliveryHeaderlabel() {
		if (textOrderInformationDeliveryHeaderlabel == null) {
			textOrderInformationDeliveryHeaderlabel = (HtmlOutputText) findComponentInRoot("textOrderInformationDeliveryHeaderlabel");
		}
		return textOrderInformationDeliveryHeaderlabel;
	}

	protected HtmlPanelGrid getGridRightPanelGrid() {
		if (gridRightPanelGrid == null) {
			gridRightPanelGrid = (HtmlPanelGrid) findComponentInRoot("gridRightPanelGrid");
		}
		return gridRightPanelGrid;
	}

	protected HtmlJspPanel getJspPanelRightPanelTop() {
		if (jspPanelRightPanelTop == null) {
			jspPanelRightPanelTop = (HtmlJspPanel) findComponentInRoot("jspPanelRightPanelTop");
		}
		return jspPanelRightPanelTop;
	}

	protected HtmlOutputLinkEx getLinkExReadNowLink() {
		if (linkExReadNowLink == null) {
			linkExReadNowLink = (HtmlOutputLinkEx) findComponentInRoot("linkExReadNowLink");
		}
		return linkExReadNowLink;
	}

	protected HtmlGraphicImageEx getImageExReadNowButtonImage() {
		if (imageExReadNowButtonImage == null) {
			imageExReadNowButtonImage = (HtmlGraphicImageEx) findComponentInRoot("imageExReadNowButtonImage");
		}
		return imageExReadNowButtonImage;
	}

	protected HtmlOutputLinkEx getLinkExReadNowTextLink() {
		if (linkExReadNowTextLink == null) {
			linkExReadNowTextLink = (HtmlOutputLinkEx) findComponentInRoot("linkExReadNowTextLink");
		}
		return linkExReadNowTextLink;
	}

	protected HtmlOutputLinkEx getLinkExCustServiceLink() {
		if (linkExCustServiceLink == null) {
			linkExCustServiceLink = (HtmlOutputLinkEx) findComponentInRoot("linkExCustServiceLink");
		}
		return linkExCustServiceLink;
	}

	protected HtmlOutputText getTextThankYouBullet2b() {
		if (textThankYouBullet2b == null) {
			textThankYouBullet2b = (HtmlOutputText) findComponentInRoot("textThankYouBullet2b");
		}
		return textThankYouBullet2b;
	}

	protected HtmlGraphicImageEx getImageExFillerImage() {
		if (imageExFillerImage == null) {
			imageExFillerImage = (HtmlGraphicImageEx) findComponentInRoot("imageExFillerImage");
		}
		return imageExFillerImage;
	}

	protected HtmlPanelGrid getGridPrintButtonGrid() {
		if (gridPrintButtonGrid == null) {
			gridPrintButtonGrid = (HtmlPanelGrid) findComponentInRoot("gridPrintButtonGrid");
		}
		return gridPrintButtonGrid;
	}

	protected HtmlJspPanel getJspPanelOrderDataPanel() {
		if (jspPanelOrderDataPanel == null) {
			jspPanelOrderDataPanel = (HtmlJspPanel) findComponentInRoot("jspPanelOrderDataPanel");
		}
		return jspPanelOrderDataPanel;
	}

	protected HtmlOutputText getTextDateLabel() {
		if (textDateLabel == null) {
			textDateLabel = (HtmlOutputText) findComponentInRoot("textDateLabel");
		}
		return textDateLabel;
	}

	protected HtmlOutputText getTextDateOfToday() {
		if (textDateOfToday == null) {
			textDateOfToday = (HtmlOutputText) findComponentInRoot("textDateOfToday");
		}
		return textDateOfToday;
	}

	protected HtmlOutputText getTextProductLabel() {
		if (textProductLabel == null) {
			textProductLabel = (HtmlOutputText) findComponentInRoot("textProductLabel");
		}
		return textProductLabel;
	}

	protected HtmlOutputText getTextProdName() {
		if (textProdName == null) {
			textProdName = (HtmlOutputText) findComponentInRoot("textProdName");
		}
		return textProdName;
	}

	protected HtmlOutputText getTextTermLabel() {
		if (textTermLabel == null) {
			textTermLabel = (HtmlOutputText) findComponentInRoot("textTermLabel");
		}
		return textTermLabel;
	}

	protected HtmlOutputText getTextSelectedTerm() {
		if (textSelectedTerm == null) {
			textSelectedTerm = (HtmlOutputText) findComponentInRoot("textSelectedTerm");
		}
		return textSelectedTerm;
	}

	protected HtmlPanelGrid getGridAccountInformationDelivery() {
		if (gridAccountInformationDelivery == null) {
			gridAccountInformationDelivery = (HtmlPanelGrid) findComponentInRoot("gridAccountInformationDelivery");
		}
		return gridAccountInformationDelivery;
	}

	protected HtmlOutputText getTextDeliveryEmailLabel() {
		if (textDeliveryEmailLabel == null) {
			textDeliveryEmailLabel = (HtmlOutputText) findComponentInRoot("textDeliveryEmailLabel");
		}
		return textDeliveryEmailLabel;
	}

	protected HtmlOutputText getTextDeliveryEmail() {
		if (textDeliveryEmail == null) {
			textDeliveryEmail = (HtmlOutputText) findComponentInRoot("textDeliveryEmail");
		}
		return textDeliveryEmail;
	}

	protected HtmlOutputText getTextDeliveryNameLabel() {
		if (textDeliveryNameLabel == null) {
			textDeliveryNameLabel = (HtmlOutputText) findComponentInRoot("textDeliveryNameLabel");
		}
		return textDeliveryNameLabel;
	}

	protected HtmlOutputText getTextDeliveryName() {
		if (textDeliveryName == null) {
			textDeliveryName = (HtmlOutputText) findComponentInRoot("textDeliveryName");
		}
		return textDeliveryName;
	}

	protected HtmlOutputText getTextDeliveryAddr1Label() {
		if (textDeliveryAddr1Label == null) {
			textDeliveryAddr1Label = (HtmlOutputText) findComponentInRoot("textDeliveryAddr1Label");
		}
		return textDeliveryAddr1Label;
	}

	protected HtmlOutputText getTextDeliveryAddr1() {
		if (textDeliveryAddr1 == null) {
			textDeliveryAddr1 = (HtmlOutputText) findComponentInRoot("textDeliveryAddr1");
		}
		return textDeliveryAddr1;
	}

	protected HtmlOutputText getTextDeliveryAptSuiteLabel() {
		if (textDeliveryAptSuiteLabel == null) {
			textDeliveryAptSuiteLabel = (HtmlOutputText) findComponentInRoot("textDeliveryAptSuiteLabel");
		}
		return textDeliveryAptSuiteLabel;
	}

	protected HtmlOutputText getTextDeliveryAptSuite() {
		if (textDeliveryAptSuite == null) {
			textDeliveryAptSuite = (HtmlOutputText) findComponentInRoot("textDeliveryAptSuite");
		}
		return textDeliveryAptSuite;
	}

	protected HtmlOutputText getTextDeliveryAddr2Label() {
		if (textDeliveryAddr2Label == null) {
			textDeliveryAddr2Label = (HtmlOutputText) findComponentInRoot("textDeliveryAddr2Label");
		}
		return textDeliveryAddr2Label;
	}

	protected HtmlOutputText getTextDeliveryAddr2() {
		if (textDeliveryAddr2 == null) {
			textDeliveryAddr2 = (HtmlOutputText) findComponentInRoot("textDeliveryAddr2");
		}
		return textDeliveryAddr2;
	}

	protected HtmlOutputText getTextDeliveryAddrStateZipLabel() {
		if (textDeliveryAddrStateZipLabel == null) {
			textDeliveryAddrStateZipLabel = (HtmlOutputText) findComponentInRoot("textDeliveryAddrStateZipLabel");
		}
		return textDeliveryAddrStateZipLabel;
	}

	protected HtmlOutputText getTextDeliveryStateZip() {
		if (textDeliveryStateZip == null) {
			textDeliveryStateZip = (HtmlOutputText) findComponentInRoot("textDeliveryStateZip");
		}
		return textDeliveryStateZip;
	}

	/**
	 * @managed-bean true
	 */
	protected TrialCustomerHandler getTrialCustomerHandler() {
		if (trialCustomerHandler == null) {
			trialCustomerHandler = (TrialCustomerHandler) getManagedBean("trialCustomerHandler");
		}
		return trialCustomerHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setTrialCustomerHandler(TrialCustomerHandler trialCustomerHandler) {
		this.trialCustomerHandler = trialCustomerHandler;
	}

	protected HtmlJspPanel getJspPanelPartnerTextPanel() {
		if (jspPanelPartnerTextPanel == null) {
			jspPanelPartnerTextPanel = (HtmlJspPanel) findComponentInRoot("jspPanelPartnerTextPanel");
		}
		return jspPanelPartnerTextPanel;
	}

	protected HtmlOutputText getTextCustomPartnerText() {
		if (textCustomPartnerText == null) {
			textCustomPartnerText = (HtmlOutputText) findComponentInRoot("textCustomPartnerText");
		}
		return textCustomPartnerText;
	}

	/**
	 * @managed-bean true
	 */
	protected PartnerHandler getTrialPartnerHandler() {
		if (trialPartnerHandler == null) {
			trialPartnerHandler = (PartnerHandler) getManagedBean("trialPartnerHandler");
		}
		return trialPartnerHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setTrialPartnerHandler(PartnerHandler trialPartnerHandler) {
		this.trialPartnerHandler = trialPartnerHandler;
	}

	protected HtmlScriptCollector getScriptCollectorTopNavCollector() {
		if (scriptCollectorTopNavCollector == null) {
			scriptCollectorTopNavCollector = (HtmlScriptCollector) findComponentInRoot("scriptCollectorTopNavCollector");
		}
		return scriptCollectorTopNavCollector;
	}

	protected HtmlJspPanel getJspPanelTopNavCustomPanel() {
		if (jspPanelTopNavCustomPanel == null) {
			jspPanelTopNavCustomPanel = (HtmlJspPanel) findComponentInRoot("jspPanelTopNavCustomPanel");
		}
		return jspPanelTopNavCustomPanel;
	}

	protected HtmlOutputLinkEx getLinkExTopNavLogoLink() {
		if (linkExTopNavLogoLink == null) {
			linkExTopNavLogoLink = (HtmlOutputLinkEx) findComponentInRoot("linkExTopNavLogoLink");
		}
		return linkExTopNavLogoLink;
	}

	protected HtmlGraphicImageEx getImageExPartnerLogo() {
		if (imageExPartnerLogo == null) {
			imageExPartnerLogo = (HtmlGraphicImageEx) findComponentInRoot("imageExPartnerLogo");
		}
		return imageExPartnerLogo;
	}

	protected HtmlGraphicImageEx getImageExPartnerLogoNoLink() {
		if (imageExPartnerLogoNoLink == null) {
			imageExPartnerLogoNoLink = (HtmlGraphicImageEx) findComponentInRoot("imageExPartnerLogoNoLink");
		}
		return imageExPartnerLogoNoLink;
	}

	protected HtmlOutputLinkEx getLinkExOpenDesktopVersion() {
		if (linkExOpenDesktopVersion == null) {
			linkExOpenDesktopVersion = (HtmlOutputLinkEx) findComponentInRoot("linkExOpenDesktopVersion");
		}
		return linkExOpenDesktopVersion;
	}

	protected HtmlOutputText getText1111() {
		if (text1111 == null) {
			text1111 = (HtmlOutputText) findComponentInRoot("text1111");
		}
		return text1111;
	}

	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected HtmlOutputLinkEx getLinkExOpenMobileVersion() {
		if (linkExOpenMobileVersion == null) {
			linkExOpenMobileVersion = (HtmlOutputLinkEx) findComponentInRoot("linkExOpenMobileVersion");
		}
		return linkExOpenMobileVersion;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

}