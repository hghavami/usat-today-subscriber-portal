/**
 * 
 */
package pagecode.subscriptions.electronic.samples;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import pagecode.PageCodeBase;

import com.ibm.faces.component.html.HtmlGraphicImageEx;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.UsatException;
import com.usatoday.business.interfaces.customer.TrialCustomerIntf;
import com.usatoday.business.interfaces.customer.TrialInstanceIntf;
import com.usatoday.business.interfaces.partners.TrialPartnerIntf;
import com.usatoday.business.interfaces.products.SubscriptionOfferIntf;
import com.usatoday.businessObjects.customer.TrialCustomerBO;
import com.usatoday.businessObjects.partners.TrialPartnerBO;
import com.usatoday.esub.common.UTCommon;
import com.usatoday.esub.handlers.NavigationLinksHandler;
import com.usatoday.esub.handlers.NewSubscriptionOrderHandler;
import com.usatoday.esub.handlers.SubscriptionOfferHandler;
import com.usatoday.esub.trials.handlers.NewTrialOrderHandler;
import com.usatoday.esub.trials.handlers.PartnerHandler;
import com.usatoday.esub.trials.handlers.TrialCustomerHandler;
import com.usatoday.util.constants.UsaTodayConstants;
import com.ibm.faces.component.html.HtmlInputHelperAssist;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlPanelGroup;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.component.html.HtmlFormItem;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.component.html.HtmlInputSecret;
import javax.faces.component.html.HtmlMessages;
import com.ibm.faces.component.html.HtmlPanelLayout;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlInputHelperSetFocus;

/**
 * @author aeast
 * 
 */
public class Index extends PageCodeBase {

	protected HtmlInputHidden Id1;
	protected NewSubscriptionOrderHandler newSubscriptionOrderHandler;
	protected HtmlInputHidden Id2;
	protected NewTrialOrderHandler newSampleOrderHandler;
	protected PartnerHandler trialPartnerHandler;
	protected TrialCustomerHandler trialCustomerHandler;
	protected NavigationLinksHandler eeditionTemplateNavHandler;
	protected HtmlScriptCollector scriptCollector1;
	protected HtmlScriptCollector scriptCollectorTopNavCollector;
	protected HtmlPanelGrid gridLeftNavGroup1Grid;
	protected HtmlOutputLinkEx linkExNavGroup2International;
	protected HtmlOutputText textNavGroup1LinkLabelInternational;
	protected HtmlOutputText textNavGroup1LinkLabelKindle;
	protected HtmlOutputText textNavGroup1LinkLabel1;
	protected HtmlOutputText textNavGroup1LinkLabelDotCom;
	protected HtmlOutputText textNavGroup1LinkLabelFAQ;
	protected HtmlOutputLinkEx linkExNavArea2Link1;
	protected HtmlOutputText textNavArea2Link1;
	protected HtmlOutputText textNavArea2Link2;
	protected HtmlOutputText textNavArea2Link3;
	protected HtmlOutputText textNavArea2Link4;
	protected HtmlPanelGrid gridNavigationOuterGrid;
	protected HtmlOutputLinkEx linkExNavGroup2Kindle;
	protected HtmlOutputLinkEx linkExNavGroup1SubscriberServices;
	protected HtmlOutputLinkEx linkExNavGroup2DotCom;
	protected HtmlOutputLinkEx linkExNavGroupFAQ;
	protected HtmlPanelGrid gridLeftNavGroup2Grid;
	protected HtmlOutputLinkEx linkExNavArea2Link2;
	protected HtmlOutputLinkEx linkExNavArea2Link3;
	protected HtmlOutputLinkEx linkExNavArea2Link4;
	protected HtmlPanelGrid gridNavigationBottomGrid;
	protected HtmlJspPanel jspPanelNavBottomEditionTrialLogin;
	protected HtmlPanelGrid gridLogoGridNav;
	protected HtmlGraphicImageEx imageExPartnerLogo;
	protected HtmlGraphicImageEx imageExPartnerLogoNoLink;
	protected HtmlJspPanel jspPanelLinkableLogoPanel;
	protected HtmlScriptCollector scriptCollectorTemplateCollectorLeftNav;
	protected HtmlPanelGrid gridNavigationTopGrid;
	protected HtmlOutputLinkEx linkExNavArea2Link0;
	protected HtmlOutputText textNavArea2Link0;
	protected HtmlOutputText textNavArea2Link5;
	protected HtmlOutputLinkEx linkExNavArea2Link5;
	protected SubscriptionOfferHandler currentOfferHandler;
	protected HtmlInputHelperAssist assist4;
	protected HtmlInputHelperAssist assist5;
	protected HtmlScriptCollector scriptCollectorSampleForm;
	protected HtmlForm formSampleForm;
	protected HtmlPanelGrid gridMainBodyGrid;
	protected HtmlJspPanel jspPanelHeaderPanel;
	protected HtmlOutputText textMainTableHeader;
	protected HtmlPanelGrid gridPartnerNotValid;
	protected HtmlOutputText textInformation1;
	protected HtmlPanelGroup groupMainBodyPanel;
	protected HtmlPanelGrid gridPartnerOfferDescGridv2;
	protected HtmlOutputText textOfferDescription;
	protected HtmlPanelFormBox formBoxPartnerFormBox;
	protected HtmlFormItem formItemPartnerItem1;
	protected HtmlInputText textPartnerName;
	protected HtmlInputText textHiltonHonorsNumber;
	protected HtmlPanelFormBox formBoxDeliveryInformation;
	protected HtmlFormItem formItemDeliveryFirstName;
	protected HtmlInputText textDeliveryFirstName;
	protected HtmlInputText textDeliveryAddress1;
	protected HtmlInputText textDeliveryAddress2;
	protected HtmlInputText textDeliveryCity;
	protected HtmlSelectOneMenu menuDeliveryState;
	protected HtmlInputText textDeliveryPhoneAreaCode;
	protected HtmlInputText textEmailAddressRecipient;
	protected HtmlInputText textEmailAddressConfirmRecipient;
	protected HtmlInputSecret secretUserPassword;
	protected HtmlInputSecret secretPasswordConfirmation;
	protected HtmlPanelGrid gridInnerDisclaimerGrid;
	protected HtmlOutputText textDisclaimerText;
	protected HtmlPanelGrid gridRightSideGrid;
	protected HtmlPanelGrid gridRightPanel1;
	protected HtmlOutputLinkEx linkExOpenReaderSample;
	protected HtmlOutputText textSampleText1Text;
	protected HtmlOutputLinkEx linkExVideoLink_B;
	protected HtmlGraphicImageEx imageExVideoSweepsGraphic_B;
	protected HtmlOutputText textSampleWeekendText1Text;
	protected HtmlMessages messagesAllMesssages;
	protected HtmlPanelGroup groupSpecialOfferGroupBox;
	protected HtmlPanelGrid gridSpecialOfferLayoutGrid;
	protected HtmlOutputLinkEx linkExCloseSpecialOffer;
	protected HtmlOutputText textCloseSpecialOfferText;
	protected HtmlGraphicImageEx imageExSpecialOfferImage;
	protected HtmlJspPanel jspPanelFormPanel;
	protected HtmlOutputText textInformation2;
	protected HtmlPanelLayout layoutPageLayout;
	protected HtmlPanelGrid gridPartnerGrid;
	protected HtmlFormItem formItemClubNumber;
	protected HtmlPanelGrid gridDeliveryInformation;
	protected HtmlInputText textDeliveryLastName;
	protected HtmlFormItem formItemDeliveryAddress1;
	protected HtmlInputText textDeliveryAptSuite;
	protected HtmlFormItem formItemDeliveryAddress2;
	protected HtmlFormItem formItemDeliveryCity;
	protected HtmlFormItem formItemDeliveryState;
	protected HtmlInputText textDeliveryZip;
	protected HtmlFormItem formItemDeliveryPhone;
	protected HtmlInputText textDeliveryPhoneExchange;
	protected HtmlInputText textDeliveryPhoneExtension;
	protected HtmlFormItem formItemEmailAddress;
	protected HtmlFormItem formItemEmailAddressConfirm;
	protected HtmlFormItem formItem2;
	protected HtmlFormItem formItemConfirmPassword;
	protected HtmlOutputText text1;
	protected HtmlPanelGrid panelGridFormSubmissionGrid;
	protected HtmlOutputText textCOPPAText;
	protected HtmlCommandExButton buttonPlaceOrder;
	protected HtmlPanelGrid gridPartnerDisclaimerText;
	protected HtmlJspPanel jspPanelTopSampleTextPanel;
	protected HtmlPanelGrid gridVideoGrid;
	protected HtmlOutputText textEEVideoText;
	protected HtmlOutputLinkEx linkExWeekendSample;
	protected HtmlJspPanel jspPanelTopWeekendSampleTextPanel;
	protected HtmlInputHidden partnerID;
	protected HtmlInputHelperSetFocus setFocus1;
	protected HtmlInputHidden csrID;
	protected HtmlJspPanel jspPanelPopOverlayPanel;
	protected HtmlOutputLinkEx linkExSpecialOfferLink;
	protected HtmlInputHidden showOverlayPopUp;

	/**
	 * @managed-bean true
	 */
	protected NewSubscriptionOrderHandler getNewSubscriptionOrderHandler() {
		if (newSubscriptionOrderHandler == null) {
			newSubscriptionOrderHandler = (NewSubscriptionOrderHandler) getManagedBean("newSubscriptionOrderHandler");
		}
		return newSubscriptionOrderHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setNewSubscriptionOrderHandler(NewSubscriptionOrderHandler newSubscriptionOrderHandler) {
		this.newSubscriptionOrderHandler = newSubscriptionOrderHandler;
	}

	public String doButtonPlaceOrderAction() {
		// Type Java code that runs when the component is clicked

		FacesContext facesContext = FacesContext.getCurrentInstance();

		TrialInstanceIntf newTrialInstance = null;

		NewTrialOrderHandler trialData = this.getNewSampleOrderHandler();

		TrialCustomerHandler tch = this.getTrialCustomerHandler();

		if (tch != null && tch.getTrialCustomer() != null && tch.getTrialCustomer().getCurrentInstance().getID() >= 0) {
			try {
				if (!tch.getTrialCustomer().getCurrentInstance().getContactInformation().getEmailAddress()
						.equalsIgnoreCase(trialData.getDeliveryEmailAddress().trim())) {
					// reset tch
					tch.setTrialCustomer(null);
				} else {
					throw new Exception("");
				}
			} catch (Exception exp1) {
				// trial already saved just forward to complete page
				FacesMessage facesMsg = new FacesMessage(
						FacesMessage.SEVERITY_INFO,
						"Duplicate Form Submission detected. If this was not a duplicate submission, please close the browser and then try again.",
						null);

				facesContext.addMessage(null, facesMsg);
				return "success";
			}
		}

		// The partner should be set up prior to reaching this page but may not be.
		// Custom landing pages wil use a hard coded partner id value.
		TrialPartnerIntf partner = this.getTrialPartnerHandler().getPartner();

		if (partner == null) {
			try {
				partner = TrialPartnerBO.fetchPartner(UsaTodayConstants.DEFAULT_TRIAL_PID, true);
			} catch (UsatException ue) {
				// handle this exception
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Trial Program Not Found. pid:"
						+ this.getTrialPartnerHandler().getPartnerID(), null);

				facesContext.addMessage(null, facesMsg);
				return "failure";
			}
		}

		// save the customer
		boolean saved = false;

		// validate form fields
		if (!trialData.getIsEmailAndConfirmEmailSame()) {
			FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
					"The email address and confirmation email address do not match.", null);

			facesContext.addMessage(null, facesMsg);
			return "failure";
		}

		if (!trialData.getIsEmailValid()) {
			FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "The email address is not valid.", null);

			facesContext.addMessage(null, facesMsg);
			return "failure";
		}

		if (!trialData.getIsPasswordAndConfirmPasswordSame()) {
			FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
					"The password and confirmation password do not match.", null);

			facesContext.addMessage(null, facesMsg);
			return "failure";
		}

		try {
			newTrialInstance = trialData.generatedNewTrialCustomerFromFields(partner);

			// validate instance.
			boolean isValid = TrialCustomerBO.validateNewTrialInstance(newTrialInstance);

			if (isValid) {
				String cIP = "";
				cIP = ((HttpServletRequest) facesContext.getExternalContext().getRequest()).getRemoteAddr();
				newTrialInstance.setClientIP(cIP);
				newTrialInstance.save();
			}

			saved = true;

		} catch (Exception e) {
			e.printStackTrace();
			FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Failed to save new trial record: "
					+ e.getMessage(), null);

			facesContext.addMessage(null, facesMsg);
			return "failure";
		}
		if (!saved) {
			return "failure";
		} else {
			// set up customer and send confirmation email
			try {
				TrialCustomerIntf tCust = TrialCustomerBO.getTrialCustomerByEmail(newTrialInstance.getContactInformation()
						.getEmailAddress());

				if (!tCust.getCurrentInstance().getPartner().getPartnerID()
						.equalsIgnoreCase(newTrialInstance.getPartner().getPartnerID())) {
					tCust.setCurrentInstance(newTrialInstance.getPartner().getPartnerID());
				}
				tch.setTrialCustomer(tCust);

				// Invoke Method to send confirmation email
				boolean emailSent = newTrialInstance.sendConfirmationEmail();
				if (!emailSent) {
					newTrialInstance.setConfirmationEmailSent("N");
				}

				// if a multiple trial users
				if (tCust.getAllTrialInstances().size() > 1) {
					// update passwords to most recent
					tCust.setPassword(newTrialInstance.getContactInformation().getPassword());
					tCust.save();
				}
			} catch (Exception eee) {
				eee.printStackTrace(); // don't go to failure page because of this.
			}
		}
		return "success";
	}

	/**
	 * @managed-bean true
	 */
	protected NewTrialOrderHandler getNewSampleOrderHandler() {
		if (newSampleOrderHandler == null) {
			newSampleOrderHandler = (NewTrialOrderHandler) getManagedBean("newSampleOrderHandler");
		}
		return newSampleOrderHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setNewSampleOrderHandler(NewTrialOrderHandler newSampleOrderHandler) {
		this.newSampleOrderHandler = newSampleOrderHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected PartnerHandler getTrialPartnerHandler() {
		if (trialPartnerHandler == null) {
			trialPartnerHandler = (PartnerHandler) getManagedBean("trialPartnerHandler");
		}
		return trialPartnerHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setTrialPartnerHandler(PartnerHandler trialPartnerHandler) {
		this.trialPartnerHandler = trialPartnerHandler;
	}

	public void onPageLoadBegin(FacesContext facescontext) {
		// Type Java code to handle page load begin event here

		// remove any current trial customer from session
		TrialCustomerHandler tch = this.getTrialCustomerHandler();
		if (tch != null && tch.getTrialCustomer() != null) {
			tch.setTrialCustomer(null);
			tch.setEarlySubscribeKeycode(null);
		}

		// void <method>(FacesContext facescontext)
		if (this.getTrialPartnerHandler().getPartner() == null) {
			// setup default trial offer if one exists
			try {
				TrialPartnerIntf p = TrialPartnerBO.fetchPartner(UsaTodayConstants.DEFAULT_TRIAL_PID, true);
				this.getTrialPartnerHandler().setPartner(p);
			} catch (Exception e) {
				// e.printStackTrace();
			}
		}
	}

	/**
	 * @managed-bean true
	 */
	protected TrialCustomerHandler getTrialCustomerHandler() {
		if (trialCustomerHandler == null) {
			trialCustomerHandler = (TrialCustomerHandler) getManagedBean("trialCustomerHandler");
		}
		return trialCustomerHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setTrialCustomerHandler(TrialCustomerHandler trialCustomerHandler) {
		this.trialCustomerHandler = trialCustomerHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected NavigationLinksHandler getEeditionTemplateNavHandler() {
		if (eeditionTemplateNavHandler == null) {
			eeditionTemplateNavHandler = (NavigationLinksHandler) getManagedBean("eeditionTemplateNavHandler");
		}
		return eeditionTemplateNavHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setEeditionTemplateNavHandler(NavigationLinksHandler eeditionTemplateNavHandler) {
		this.eeditionTemplateNavHandler = eeditionTemplateNavHandler;
	}

	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlScriptCollector getScriptCollectorTopNavCollector() {
		if (scriptCollectorTopNavCollector == null) {
			scriptCollectorTopNavCollector = (HtmlScriptCollector) findComponentInRoot("scriptCollectorTopNavCollector");
		}
		return scriptCollectorTopNavCollector;
	}

	protected HtmlPanelGrid getGridLeftNavGroup1Grid() {
		if (gridLeftNavGroup1Grid == null) {
			gridLeftNavGroup1Grid = (HtmlPanelGrid) findComponentInRoot("gridLeftNavGroup1Grid");
		}
		return gridLeftNavGroup1Grid;
	}

	protected HtmlOutputLinkEx getLinkExNavGroup2International() {
		if (linkExNavGroup2International == null) {
			linkExNavGroup2International = (HtmlOutputLinkEx) findComponentInRoot("linkExNavGroup2International");
		}
		return linkExNavGroup2International;
	}

	protected HtmlOutputText getTextNavGroup1LinkLabelInternational() {
		if (textNavGroup1LinkLabelInternational == null) {
			textNavGroup1LinkLabelInternational = (HtmlOutputText) findComponentInRoot("textNavGroup1LinkLabelInternational");
		}
		return textNavGroup1LinkLabelInternational;
	}

	protected HtmlOutputText getTextNavGroup1LinkLabelKindle() {
		if (textNavGroup1LinkLabelKindle == null) {
			textNavGroup1LinkLabelKindle = (HtmlOutputText) findComponentInRoot("textNavGroup1LinkLabelKindle");
		}
		return textNavGroup1LinkLabelKindle;
	}

	protected HtmlOutputText getTextNavGroup1LinkLabel1() {
		if (textNavGroup1LinkLabel1 == null) {
			textNavGroup1LinkLabel1 = (HtmlOutputText) findComponentInRoot("textNavGroup1LinkLabel1");
		}
		return textNavGroup1LinkLabel1;
	}

	protected HtmlOutputText getTextNavGroup1LinkLabelDotCom() {
		if (textNavGroup1LinkLabelDotCom == null) {
			textNavGroup1LinkLabelDotCom = (HtmlOutputText) findComponentInRoot("textNavGroup1LinkLabelDotCom");
		}
		return textNavGroup1LinkLabelDotCom;
	}

	protected HtmlOutputText getTextNavGroup1LinkLabelFAQ() {
		if (textNavGroup1LinkLabelFAQ == null) {
			textNavGroup1LinkLabelFAQ = (HtmlOutputText) findComponentInRoot("textNavGroup1LinkLabelFAQ");
		}
		return textNavGroup1LinkLabelFAQ;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link1() {
		if (linkExNavArea2Link1 == null) {
			linkExNavArea2Link1 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link1");
		}
		return linkExNavArea2Link1;
	}

	protected HtmlOutputText getTextNavArea2Link1() {
		if (textNavArea2Link1 == null) {
			textNavArea2Link1 = (HtmlOutputText) findComponentInRoot("textNavArea2Link1");
		}
		return textNavArea2Link1;
	}

	protected HtmlOutputText getTextNavArea2Link2() {
		if (textNavArea2Link2 == null) {
			textNavArea2Link2 = (HtmlOutputText) findComponentInRoot("textNavArea2Link2");
		}
		return textNavArea2Link2;
	}

	protected HtmlOutputText getTextNavArea2Link3() {
		if (textNavArea2Link3 == null) {
			textNavArea2Link3 = (HtmlOutputText) findComponentInRoot("textNavArea2Link3");
		}
		return textNavArea2Link3;
	}

	protected HtmlOutputText getTextNavArea2Link4() {
		if (textNavArea2Link4 == null) {
			textNavArea2Link4 = (HtmlOutputText) findComponentInRoot("textNavArea2Link4");
		}
		return textNavArea2Link4;
	}

	protected HtmlPanelGrid getGridNavigationOuterGrid() {
		if (gridNavigationOuterGrid == null) {
			gridNavigationOuterGrid = (HtmlPanelGrid) findComponentInRoot("gridNavigationOuterGrid");
		}
		return gridNavigationOuterGrid;
	}

	protected HtmlOutputLinkEx getLinkExNavGroup2Kindle() {
		if (linkExNavGroup2Kindle == null) {
			linkExNavGroup2Kindle = (HtmlOutputLinkEx) findComponentInRoot("linkExNavGroup2Kindle");
		}
		return linkExNavGroup2Kindle;
	}

	protected HtmlOutputLinkEx getLinkExNavGroup1SubscriberServices() {
		if (linkExNavGroup1SubscriberServices == null) {
			linkExNavGroup1SubscriberServices = (HtmlOutputLinkEx) findComponentInRoot("linkExNavGroup1SubscriberServices");
		}
		return linkExNavGroup1SubscriberServices;
	}

	protected HtmlOutputLinkEx getLinkExNavGroup2DotCom() {
		if (linkExNavGroup2DotCom == null) {
			linkExNavGroup2DotCom = (HtmlOutputLinkEx) findComponentInRoot("linkExNavGroup2DotCom");
		}
		return linkExNavGroup2DotCom;
	}

	protected HtmlOutputLinkEx getLinkExNavGroupFAQ() {
		if (linkExNavGroupFAQ == null) {
			linkExNavGroupFAQ = (HtmlOutputLinkEx) findComponentInRoot("linkExNavGroupFAQ");
		}
		return linkExNavGroupFAQ;
	}

	protected HtmlPanelGrid getGridLeftNavGroup2Grid() {
		if (gridLeftNavGroup2Grid == null) {
			gridLeftNavGroup2Grid = (HtmlPanelGrid) findComponentInRoot("gridLeftNavGroup2Grid");
		}
		return gridLeftNavGroup2Grid;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link2() {
		if (linkExNavArea2Link2 == null) {
			linkExNavArea2Link2 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link2");
		}
		return linkExNavArea2Link2;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link3() {
		if (linkExNavArea2Link3 == null) {
			linkExNavArea2Link3 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link3");
		}
		return linkExNavArea2Link3;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link4() {
		if (linkExNavArea2Link4 == null) {
			linkExNavArea2Link4 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link4");
		}
		return linkExNavArea2Link4;
	}

	protected HtmlPanelGrid getGridNavigationBottomGrid() {
		if (gridNavigationBottomGrid == null) {
			gridNavigationBottomGrid = (HtmlPanelGrid) findComponentInRoot("gridNavigationBottomGrid");
		}
		return gridNavigationBottomGrid;
	}

	protected HtmlJspPanel getJspPanelNavBottomEditionTrialLogin() {
		if (jspPanelNavBottomEditionTrialLogin == null) {
			jspPanelNavBottomEditionTrialLogin = (HtmlJspPanel) findComponentInRoot("jspPanelNavBottomEditionTrialLogin");
		}
		return jspPanelNavBottomEditionTrialLogin;
	}

	protected HtmlPanelGrid getGridLogoGridNav() {
		if (gridLogoGridNav == null) {
			gridLogoGridNav = (HtmlPanelGrid) findComponentInRoot("gridLogoGridNav");
		}
		return gridLogoGridNav;
	}

	protected HtmlGraphicImageEx getImageExPartnerLogo() {
		if (imageExPartnerLogo == null) {
			imageExPartnerLogo = (HtmlGraphicImageEx) findComponentInRoot("imageExPartnerLogo");
		}
		return imageExPartnerLogo;
	}

	protected HtmlGraphicImageEx getImageExPartnerLogoNoLink() {
		if (imageExPartnerLogoNoLink == null) {
			imageExPartnerLogoNoLink = (HtmlGraphicImageEx) findComponentInRoot("imageExPartnerLogoNoLink");
		}
		return imageExPartnerLogoNoLink;
	}

	protected HtmlJspPanel getJspPanelLinkableLogoPanel() {
		if (jspPanelLinkableLogoPanel == null) {
			jspPanelLinkableLogoPanel = (HtmlJspPanel) findComponentInRoot("jspPanelLinkableLogoPanel");
		}
		return jspPanelLinkableLogoPanel;
	}

	protected HtmlScriptCollector getScriptCollectorTemplateCollectorLeftNav() {
		if (scriptCollectorTemplateCollectorLeftNav == null) {
			scriptCollectorTemplateCollectorLeftNav = (HtmlScriptCollector) findComponentInRoot("scriptCollectorTemplateCollectorLeftNav");
		}
		return scriptCollectorTemplateCollectorLeftNav;
	}

	protected HtmlPanelGrid getGridNavigationTopGrid() {
		if (gridNavigationTopGrid == null) {
			gridNavigationTopGrid = (HtmlPanelGrid) findComponentInRoot("gridNavigationTopGrid");
		}
		return gridNavigationTopGrid;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link0() {
		if (linkExNavArea2Link0 == null) {
			linkExNavArea2Link0 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link0");
		}
		return linkExNavArea2Link0;
	}

	protected HtmlOutputText getTextNavArea2Link0() {
		if (textNavArea2Link0 == null) {
			textNavArea2Link0 = (HtmlOutputText) findComponentInRoot("textNavArea2Link0");
		}
		return textNavArea2Link0;
	}

	protected HtmlOutputText getTextNavArea2Link5() {
		if (textNavArea2Link5 == null) {
			textNavArea2Link5 = (HtmlOutputText) findComponentInRoot("textNavArea2Link5");
		}
		return textNavArea2Link5;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link5() {
		if (linkExNavArea2Link5 == null) {
			linkExNavArea2Link5 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link5");
		}
		return linkExNavArea2Link5;
	}

	public void onPageLoadBegin1(FacesContext facescontext) {
		// Type Java code to handle page load begin event here

		SubscriptionOfferHandler offer = this.getCurrentOfferHandler();

		HttpServletRequest req = (HttpServletRequest) this.getFacesContext().getExternalContext().getRequest();

		// note, this means this page will always be e-only...if we modify the way we track the current
		// offer this page would support orders for any publication.
		SubscriptionOfferIntf defaultOffer = (SubscriptionOfferIntf) UTCommon.getCurrentOffer(req);

		offer.setCurrentOffer(defaultOffer);

	}

	/**
	 * @managed-bean true
	 */
	protected SubscriptionOfferHandler getCurrentOfferHandler() {
		if (currentOfferHandler == null) {
			currentOfferHandler = (SubscriptionOfferHandler) getManagedBean("currentOfferHandler");
		}
		return currentOfferHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setCurrentOfferHandler(SubscriptionOfferHandler currentOfferHandler) {
		this.currentOfferHandler = currentOfferHandler;
	}

	protected HtmlInputHelperAssist getAssist4() {
		if (assist4 == null) {
			assist4 = (HtmlInputHelperAssist) findComponentInRoot("assist4");
		}
		return assist4;
	}

	protected HtmlInputHelperAssist getAssist5() {
		if (assist5 == null) {
			assist5 = (HtmlInputHelperAssist) findComponentInRoot("assist5");
		}
		return assist5;
	}

	protected HtmlScriptCollector getScriptCollectorSampleForm() {
		if (scriptCollectorSampleForm == null) {
			scriptCollectorSampleForm = (HtmlScriptCollector) findComponentInRoot("scriptCollectorSampleForm");
		}
		return scriptCollectorSampleForm;
	}

	protected HtmlForm getFormSampleForm() {
		if (formSampleForm == null) {
			formSampleForm = (HtmlForm) findComponentInRoot("formSampleForm");
		}
		return formSampleForm;
	}

	protected HtmlPanelGrid getGridMainBodyGrid() {
		if (gridMainBodyGrid == null) {
			gridMainBodyGrid = (HtmlPanelGrid) findComponentInRoot("gridMainBodyGrid");
		}
		return gridMainBodyGrid;
	}

	protected HtmlJspPanel getJspPanelHeaderPanel() {
		if (jspPanelHeaderPanel == null) {
			jspPanelHeaderPanel = (HtmlJspPanel) findComponentInRoot("jspPanelHeaderPanel");
		}
		return jspPanelHeaderPanel;
	}

	protected HtmlOutputText getTextMainTableHeader() {
		if (textMainTableHeader == null) {
			textMainTableHeader = (HtmlOutputText) findComponentInRoot("textMainTableHeader");
		}
		return textMainTableHeader;
	}

	protected HtmlPanelGrid getGridPartnerNotValid() {
		if (gridPartnerNotValid == null) {
			gridPartnerNotValid = (HtmlPanelGrid) findComponentInRoot("gridPartnerNotValid");
		}
		return gridPartnerNotValid;
	}

	protected HtmlOutputText getTextInformation1() {
		if (textInformation1 == null) {
			textInformation1 = (HtmlOutputText) findComponentInRoot("textInformation1");
		}
		return textInformation1;
	}

	protected HtmlPanelGroup getGroupMainBodyPanel() {
		if (groupMainBodyPanel == null) {
			groupMainBodyPanel = (HtmlPanelGroup) findComponentInRoot("groupMainBodyPanel");
		}
		return groupMainBodyPanel;
	}

	protected HtmlPanelGrid getGridPartnerOfferDescGridv2() {
		if (gridPartnerOfferDescGridv2 == null) {
			gridPartnerOfferDescGridv2 = (HtmlPanelGrid) findComponentInRoot("gridPartnerOfferDescGridv2");
		}
		return gridPartnerOfferDescGridv2;
	}

	protected HtmlOutputText getTextOfferDescription() {
		if (textOfferDescription == null) {
			textOfferDescription = (HtmlOutputText) findComponentInRoot("textOfferDescription");
		}
		return textOfferDescription;
	}

	protected HtmlPanelFormBox getFormBoxPartnerFormBox() {
		if (formBoxPartnerFormBox == null) {
			formBoxPartnerFormBox = (HtmlPanelFormBox) findComponentInRoot("formBoxPartnerFormBox");
		}
		return formBoxPartnerFormBox;
	}

	protected HtmlFormItem getFormItemPartnerItem1() {
		if (formItemPartnerItem1 == null) {
			formItemPartnerItem1 = (HtmlFormItem) findComponentInRoot("formItemPartnerItem1");
		}
		return formItemPartnerItem1;
	}

	protected HtmlInputText getTextPartnerName() {
		if (textPartnerName == null) {
			textPartnerName = (HtmlInputText) findComponentInRoot("textPartnerName");
		}
		return textPartnerName;
	}

	protected HtmlInputText getTextHiltonHonorsNumber() {
		if (textHiltonHonorsNumber == null) {
			textHiltonHonorsNumber = (HtmlInputText) findComponentInRoot("textHiltonHonorsNumber");
		}
		return textHiltonHonorsNumber;
	}

	protected HtmlPanelFormBox getFormBoxDeliveryInformation() {
		if (formBoxDeliveryInformation == null) {
			formBoxDeliveryInformation = (HtmlPanelFormBox) findComponentInRoot("formBoxDeliveryInformation");
		}
		return formBoxDeliveryInformation;
	}

	protected HtmlFormItem getFormItemDeliveryFirstName() {
		if (formItemDeliveryFirstName == null) {
			formItemDeliveryFirstName = (HtmlFormItem) findComponentInRoot("formItemDeliveryFirstName");
		}
		return formItemDeliveryFirstName;
	}

	protected HtmlInputText getTextDeliveryFirstName() {
		if (textDeliveryFirstName == null) {
			textDeliveryFirstName = (HtmlInputText) findComponentInRoot("textDeliveryFirstName");
		}
		return textDeliveryFirstName;
	}

	protected HtmlInputText getTextDeliveryAddress1() {
		if (textDeliveryAddress1 == null) {
			textDeliveryAddress1 = (HtmlInputText) findComponentInRoot("textDeliveryAddress1");
		}
		return textDeliveryAddress1;
	}

	protected HtmlInputText getTextDeliveryAddress2() {
		if (textDeliveryAddress2 == null) {
			textDeliveryAddress2 = (HtmlInputText) findComponentInRoot("textDeliveryAddress2");
		}
		return textDeliveryAddress2;
	}

	protected HtmlInputText getTextDeliveryCity() {
		if (textDeliveryCity == null) {
			textDeliveryCity = (HtmlInputText) findComponentInRoot("textDeliveryCity");
		}
		return textDeliveryCity;
	}

	protected HtmlSelectOneMenu getMenuDeliveryState() {
		if (menuDeliveryState == null) {
			menuDeliveryState = (HtmlSelectOneMenu) findComponentInRoot("menuDeliveryState");
		}
		return menuDeliveryState;
	}

	protected HtmlInputText getTextDeliveryPhoneAreaCode() {
		if (textDeliveryPhoneAreaCode == null) {
			textDeliveryPhoneAreaCode = (HtmlInputText) findComponentInRoot("textDeliveryPhoneAreaCode");
		}
		return textDeliveryPhoneAreaCode;
	}

	protected HtmlInputText getTextEmailAddressRecipient() {
		if (textEmailAddressRecipient == null) {
			textEmailAddressRecipient = (HtmlInputText) findComponentInRoot("textEmailAddressRecipient");
		}
		return textEmailAddressRecipient;
	}

	protected HtmlInputText getTextEmailAddressConfirmRecipient() {
		if (textEmailAddressConfirmRecipient == null) {
			textEmailAddressConfirmRecipient = (HtmlInputText) findComponentInRoot("textEmailAddressConfirmRecipient");
		}
		return textEmailAddressConfirmRecipient;
	}

	protected HtmlInputSecret getSecretUserPassword() {
		if (secretUserPassword == null) {
			secretUserPassword = (HtmlInputSecret) findComponentInRoot("secretUserPassword");
		}
		return secretUserPassword;
	}

	protected HtmlInputSecret getSecretPasswordConfirmation() {
		if (secretPasswordConfirmation == null) {
			secretPasswordConfirmation = (HtmlInputSecret) findComponentInRoot("secretPasswordConfirmation");
		}
		return secretPasswordConfirmation;
	}

	protected HtmlPanelGrid getGridInnerDisclaimerGrid() {
		if (gridInnerDisclaimerGrid == null) {
			gridInnerDisclaimerGrid = (HtmlPanelGrid) findComponentInRoot("gridInnerDisclaimerGrid");
		}
		return gridInnerDisclaimerGrid;
	}

	protected HtmlOutputText getTextDisclaimerText() {
		if (textDisclaimerText == null) {
			textDisclaimerText = (HtmlOutputText) findComponentInRoot("textDisclaimerText");
		}
		return textDisclaimerText;
	}

	protected HtmlPanelGrid getGridRightSideGrid() {
		if (gridRightSideGrid == null) {
			gridRightSideGrid = (HtmlPanelGrid) findComponentInRoot("gridRightSideGrid");
		}
		return gridRightSideGrid;
	}

	protected HtmlPanelGrid getGridRightPanel1() {
		if (gridRightPanel1 == null) {
			gridRightPanel1 = (HtmlPanelGrid) findComponentInRoot("gridRightPanel1");
		}
		return gridRightPanel1;
	}

	protected HtmlOutputLinkEx getLinkExOpenReaderSample() {
		if (linkExOpenReaderSample == null) {
			linkExOpenReaderSample = (HtmlOutputLinkEx) findComponentInRoot("linkExOpenReaderSample");
		}
		return linkExOpenReaderSample;
	}

	protected HtmlOutputText getTextSampleText1Text() {
		if (textSampleText1Text == null) {
			textSampleText1Text = (HtmlOutputText) findComponentInRoot("textSampleText1Text");
		}
		return textSampleText1Text;
	}

	protected HtmlOutputLinkEx getLinkExVideoLink_B() {
		if (linkExVideoLink_B == null) {
			linkExVideoLink_B = (HtmlOutputLinkEx) findComponentInRoot("linkExVideoLink_B");
		}
		return linkExVideoLink_B;
	}

	protected HtmlGraphicImageEx getImageExVideoSweepsGraphic_B() {
		if (imageExVideoSweepsGraphic_B == null) {
			imageExVideoSweepsGraphic_B = (HtmlGraphicImageEx) findComponentInRoot("imageExVideoSweepsGraphic_B");
		}
		return imageExVideoSweepsGraphic_B;
	}

	protected HtmlOutputText getTextSampleWeekendText1Text() {
		if (textSampleWeekendText1Text == null) {
			textSampleWeekendText1Text = (HtmlOutputText) findComponentInRoot("textSampleWeekendText1Text");
		}
		return textSampleWeekendText1Text;
	}

	protected HtmlMessages getMessagesAllMesssages() {
		if (messagesAllMesssages == null) {
			messagesAllMesssages = (HtmlMessages) findComponentInRoot("messagesAllMesssages");
		}
		return messagesAllMesssages;
	}

	protected HtmlPanelGroup getGroupSpecialOfferGroupBox() {
		if (groupSpecialOfferGroupBox == null) {
			groupSpecialOfferGroupBox = (HtmlPanelGroup) findComponentInRoot("groupSpecialOfferGroupBox");
		}
		return groupSpecialOfferGroupBox;
	}

	protected HtmlPanelGrid getGridSpecialOfferLayoutGrid() {
		if (gridSpecialOfferLayoutGrid == null) {
			gridSpecialOfferLayoutGrid = (HtmlPanelGrid) findComponentInRoot("gridSpecialOfferLayoutGrid");
		}
		return gridSpecialOfferLayoutGrid;
	}

	protected HtmlOutputLinkEx getLinkExCloseSpecialOffer() {
		if (linkExCloseSpecialOffer == null) {
			linkExCloseSpecialOffer = (HtmlOutputLinkEx) findComponentInRoot("linkExCloseSpecialOffer");
		}
		return linkExCloseSpecialOffer;
	}

	protected HtmlOutputText getTextCloseSpecialOfferText() {
		if (textCloseSpecialOfferText == null) {
			textCloseSpecialOfferText = (HtmlOutputText) findComponentInRoot("textCloseSpecialOfferText");
		}
		return textCloseSpecialOfferText;
	}

	protected HtmlGraphicImageEx getImageExSpecialOfferImage() {
		if (imageExSpecialOfferImage == null) {
			imageExSpecialOfferImage = (HtmlGraphicImageEx) findComponentInRoot("imageExSpecialOfferImage");
		}
		return imageExSpecialOfferImage;
	}

	protected HtmlJspPanel getJspPanelFormPanel() {
		if (jspPanelFormPanel == null) {
			jspPanelFormPanel = (HtmlJspPanel) findComponentInRoot("jspPanelFormPanel");
		}
		return jspPanelFormPanel;
	}

	protected HtmlOutputText getTextInformation2() {
		if (textInformation2 == null) {
			textInformation2 = (HtmlOutputText) findComponentInRoot("textInformation2");
		}
		return textInformation2;
	}

	protected HtmlPanelLayout getLayoutPageLayout() {
		if (layoutPageLayout == null) {
			layoutPageLayout = (HtmlPanelLayout) findComponentInRoot("layoutPageLayout");
		}
		return layoutPageLayout;
	}

	protected HtmlPanelGrid getGridPartnerGrid() {
		if (gridPartnerGrid == null) {
			gridPartnerGrid = (HtmlPanelGrid) findComponentInRoot("gridPartnerGrid");
		}
		return gridPartnerGrid;
	}

	protected HtmlFormItem getFormItemClubNumber() {
		if (formItemClubNumber == null) {
			formItemClubNumber = (HtmlFormItem) findComponentInRoot("formItemClubNumber");
		}
		return formItemClubNumber;
	}

	protected HtmlPanelGrid getGridDeliveryInformation() {
		if (gridDeliveryInformation == null) {
			gridDeliveryInformation = (HtmlPanelGrid) findComponentInRoot("gridDeliveryInformation");
		}
		return gridDeliveryInformation;
	}

	protected HtmlInputText getTextDeliveryLastName() {
		if (textDeliveryLastName == null) {
			textDeliveryLastName = (HtmlInputText) findComponentInRoot("textDeliveryLastName");
		}
		return textDeliveryLastName;
	}

	protected HtmlFormItem getFormItemDeliveryAddress1() {
		if (formItemDeliveryAddress1 == null) {
			formItemDeliveryAddress1 = (HtmlFormItem) findComponentInRoot("formItemDeliveryAddress1");
		}
		return formItemDeliveryAddress1;
	}

	protected HtmlInputText getTextDeliveryAptSuite() {
		if (textDeliveryAptSuite == null) {
			textDeliveryAptSuite = (HtmlInputText) findComponentInRoot("textDeliveryAptSuite");
		}
		return textDeliveryAptSuite;
	}

	protected HtmlFormItem getFormItemDeliveryAddress2() {
		if (formItemDeliveryAddress2 == null) {
			formItemDeliveryAddress2 = (HtmlFormItem) findComponentInRoot("formItemDeliveryAddress2");
		}
		return formItemDeliveryAddress2;
	}

	protected HtmlFormItem getFormItemDeliveryCity() {
		if (formItemDeliveryCity == null) {
			formItemDeliveryCity = (HtmlFormItem) findComponentInRoot("formItemDeliveryCity");
		}
		return formItemDeliveryCity;
	}

	protected HtmlFormItem getFormItemDeliveryState() {
		if (formItemDeliveryState == null) {
			formItemDeliveryState = (HtmlFormItem) findComponentInRoot("formItemDeliveryState");
		}
		return formItemDeliveryState;
	}

	protected HtmlInputText getTextDeliveryZip() {
		if (textDeliveryZip == null) {
			textDeliveryZip = (HtmlInputText) findComponentInRoot("textDeliveryZip");
		}
		return textDeliveryZip;
	}

	protected HtmlFormItem getFormItemDeliveryPhone() {
		if (formItemDeliveryPhone == null) {
			formItemDeliveryPhone = (HtmlFormItem) findComponentInRoot("formItemDeliveryPhone");
		}
		return formItemDeliveryPhone;
	}

	protected HtmlInputText getTextDeliveryPhoneExchange() {
		if (textDeliveryPhoneExchange == null) {
			textDeliveryPhoneExchange = (HtmlInputText) findComponentInRoot("textDeliveryPhoneExchange");
		}
		return textDeliveryPhoneExchange;
	}

	protected HtmlInputText getTextDeliveryPhoneExtension() {
		if (textDeliveryPhoneExtension == null) {
			textDeliveryPhoneExtension = (HtmlInputText) findComponentInRoot("textDeliveryPhoneExtension");
		}
		return textDeliveryPhoneExtension;
	}

	protected HtmlFormItem getFormItemEmailAddress() {
		if (formItemEmailAddress == null) {
			formItemEmailAddress = (HtmlFormItem) findComponentInRoot("formItemEmailAddress");
		}
		return formItemEmailAddress;
	}

	protected HtmlFormItem getFormItemEmailAddressConfirm() {
		if (formItemEmailAddressConfirm == null) {
			formItemEmailAddressConfirm = (HtmlFormItem) findComponentInRoot("formItemEmailAddressConfirm");
		}
		return formItemEmailAddressConfirm;
	}

	protected HtmlFormItem getFormItem2() {
		if (formItem2 == null) {
			formItem2 = (HtmlFormItem) findComponentInRoot("formItem2");
		}
		return formItem2;
	}

	protected HtmlFormItem getFormItemConfirmPassword() {
		if (formItemConfirmPassword == null) {
			formItemConfirmPassword = (HtmlFormItem) findComponentInRoot("formItemConfirmPassword");
		}
		return formItemConfirmPassword;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlPanelGrid getPanelGridFormSubmissionGrid() {
		if (panelGridFormSubmissionGrid == null) {
			panelGridFormSubmissionGrid = (HtmlPanelGrid) findComponentInRoot("panelGridFormSubmissionGrid");
		}
		return panelGridFormSubmissionGrid;
	}

	protected HtmlOutputText getTextCOPPAText() {
		if (textCOPPAText == null) {
			textCOPPAText = (HtmlOutputText) findComponentInRoot("textCOPPAText");
		}
		return textCOPPAText;
	}

	protected HtmlCommandExButton getButtonPlaceOrder() {
		if (buttonPlaceOrder == null) {
			buttonPlaceOrder = (HtmlCommandExButton) findComponentInRoot("buttonPlaceOrder");
		}
		return buttonPlaceOrder;
	}

	protected HtmlPanelGrid getGridPartnerDisclaimerText() {
		if (gridPartnerDisclaimerText == null) {
			gridPartnerDisclaimerText = (HtmlPanelGrid) findComponentInRoot("gridPartnerDisclaimerText");
		}
		return gridPartnerDisclaimerText;
	}

	protected HtmlJspPanel getJspPanelTopSampleTextPanel() {
		if (jspPanelTopSampleTextPanel == null) {
			jspPanelTopSampleTextPanel = (HtmlJspPanel) findComponentInRoot("jspPanelTopSampleTextPanel");
		}
		return jspPanelTopSampleTextPanel;
	}

	protected HtmlPanelGrid getGridVideoGrid() {
		if (gridVideoGrid == null) {
			gridVideoGrid = (HtmlPanelGrid) findComponentInRoot("gridVideoGrid");
		}
		return gridVideoGrid;
	}

	protected HtmlOutputText getTextEEVideoText() {
		if (textEEVideoText == null) {
			textEEVideoText = (HtmlOutputText) findComponentInRoot("textEEVideoText");
		}
		return textEEVideoText;
	}

	protected HtmlOutputLinkEx getLinkExWeekendSample() {
		if (linkExWeekendSample == null) {
			linkExWeekendSample = (HtmlOutputLinkEx) findComponentInRoot("linkExWeekendSample");
		}
		return linkExWeekendSample;
	}

	protected HtmlJspPanel getJspPanelTopWeekendSampleTextPanel() {
		if (jspPanelTopWeekendSampleTextPanel == null) {
			jspPanelTopWeekendSampleTextPanel = (HtmlJspPanel) findComponentInRoot("jspPanelTopWeekendSampleTextPanel");
		}
		return jspPanelTopWeekendSampleTextPanel;
	}

	protected HtmlInputHidden getPartnerID() {
		if (partnerID == null) {
			partnerID = (HtmlInputHidden) findComponentInRoot("partnerID");
		}
		return partnerID;
	}

	protected HtmlInputHelperSetFocus getSetFocus1() {
		if (setFocus1 == null) {
			setFocus1 = (HtmlInputHelperSetFocus) findComponentInRoot("setFocus1");
		}
		return setFocus1;
	}

	protected HtmlInputHidden getCsrID() {
		if (csrID == null) {
			csrID = (HtmlInputHidden) findComponentInRoot("csrID");
		}
		return csrID;
	}

	protected HtmlJspPanel getJspPanelPopOverlayPanel() {
		if (jspPanelPopOverlayPanel == null) {
			jspPanelPopOverlayPanel = (HtmlJspPanel) findComponentInRoot("jspPanelPopOverlayPanel");
		}
		return jspPanelPopOverlayPanel;
	}

	protected HtmlOutputLinkEx getLinkExSpecialOfferLink() {
		if (linkExSpecialOfferLink == null) {
			linkExSpecialOfferLink = (HtmlOutputLinkEx) findComponentInRoot("linkExSpecialOfferLink");
		}
		return linkExSpecialOfferLink;
	}

	protected HtmlInputHidden getShowOverlayPopUp() {
		if (showOverlayPopUp == null) {
			showOverlayPopUp = (HtmlInputHidden) findComponentInRoot("showOverlayPopUp");
		}
		return showOverlayPopUp;
	}

}