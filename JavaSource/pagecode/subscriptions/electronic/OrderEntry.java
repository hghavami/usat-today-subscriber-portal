/**
 * 
 */
package pagecode.subscriptions.electronic;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.component.html.HtmlSelectOneRadio;
import javax.faces.context.FacesContext;

import pagecode.PageCodeBase;

import com.ibm.faces.component.html.HtmlGraphicImageEx;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.esub.handlers.NavigationLinksHandler;
import com.usatoday.esub.handlers.NewSubscriptionOrderHandler;
import com.usatoday.esub.handlers.ShoppingCartHandler;
import com.usatoday.esub.handlers.SubscriptionOfferHandler;
import com.usatoday.esub.ncs.handlers.CustomerHandler;
import com.usatoday.esub.trials.handlers.TrialCustomerHandler;

/**
 * @author aeast
 * 
 */
public class OrderEntry extends PageCodeBase {

	protected HtmlScriptCollector scriptCollectorMainOrderEntryCollector;
	protected HtmlForm formOrderEntryForm;
	protected HtmlPanelGrid gridDeliveryInfoGrid;
	protected HtmlPanelGrid grid1;
	protected HtmlJspPanel jspPanelleftSidePanel;
	protected HtmlPanelGrid gridLeftPanelMainGrid;
	protected HtmlPanelGrid gridInnerLeftGrid;
	protected HtmlOutputText textFAQLabel;
	protected HtmlOutputLinkEx linkEx1;
	protected HtmlGraphicImageEx imageExLeftSidePlaceHolder;
	protected SubscriptionOfferHandler currentOfferHandler;
	protected NewSubscriptionOrderHandler newSubscriptionOrderHandler;
	protected ShoppingCartHandler shoppingCartHandler;
	protected CustomerHandler customerHandler;
	protected HtmlOutputText textIntEditionHolder;
	protected HtmlPanelGrid gridNavigationOuterGrid;
	protected HtmlPanelGrid gridLeftNavGroup1Grid;
	protected HtmlOutputLinkEx linkExNavGroup1;
	protected HtmlOutputText textNavGroup1LinkLabel1;
	protected HtmlOutputText textNavGroup1LinkLabel2;
	protected HtmlOutputText textNavGroup1LinkLabel3;
	protected HtmlOutputText textNavGroup1LinkLabel4;
	protected HtmlPanelGroup groupLeftNavGroup1Header;
	protected HtmlOutputText textGroup1HeaderText;
	protected HtmlOutputLinkEx linkExNavGroup2;
	protected HtmlOutputLinkEx linkExNavGroup3;
	protected HtmlOutputLinkEx linkExNavGroup4;
	protected HtmlPanelGrid gridLeftNavGroup2Grid;
	protected HtmlScriptCollector scriptCollectorTemplateCollectorLeftNav;
	protected HtmlPanelGrid gridMainBodyGrid;
	protected HtmlJspPanel jspPanelHeaderPanel;
	protected HtmlJspPanel jspPanelFormPanel;
	protected HtmlOutputText textMainTableHeader;
	protected NavigationLinksHandler eeditionTemplateNavHandler;
	protected HtmlOutputLinkEx linkExNavGroup1SubscriberServices;
	protected HtmlOutputText textNavGroup1LinkLabelInternational;
	protected HtmlOutputLinkEx linkExNavArea2Link1;
	protected HtmlOutputText textNavArea2Link1;
	protected HtmlOutputText textNavArea2Link2;
	protected HtmlOutputText textNavArea2Link3;
	protected HtmlOutputText textNavArea2Link4;
	protected HtmlOutputLinkEx linkExNavGroup2International;
	protected HtmlOutputLinkEx linkExNavArea2Link2;
	protected HtmlOutputLinkEx linkExNavArea2Link3;
	protected HtmlOutputLinkEx linkExNavArea2Link4;
	protected HtmlSelectOneRadio radio1;
	protected HtmlOutputText textNavGroup1LinkLabelKindle;
	protected HtmlOutputText textNavGroup1LinkLabelDotCom;
	protected HtmlOutputLinkEx linkExNavGroup2Kindle;
	protected HtmlOutputLinkEx linkExNavGroup2DotCom;
	protected HtmlOutputText textNavGroup1LinkLabelFAQ;
	protected HtmlOutputLinkEx linkExNavGroupFAQ;
	protected HtmlPanelFormBox formBox1;
	protected TrialCustomerHandler trialCustomerHandler;
	protected HtmlScriptCollector scriptCollectorTopNavCollector;
	protected HtmlJspPanel jspPanelTopNavCustomPanel;
	protected HtmlPanelGrid gridNavigationTopGrid;
	protected HtmlOutputText textNavArea2Link5;
	protected HtmlOutputLinkEx linkExNavArea2Link5;
	protected HtmlPanelGrid gridNavigationBottomGrid;
	protected HtmlOutputLinkEx linkExNavArea2Link0;
	protected HtmlOutputText textNavArea2Link0;
	protected HtmlScriptCollector scriptCollectorTemplateLogoutCollector;
	protected HtmlForm formLogoutTemplateAction;
	protected HtmlCommandLink linkTemplateDoLogout;
	protected HtmlOutputText textTemplateLogoutTextLabel;
	protected HtmlGraphicImageEx imageExLinkServices2Link;
	protected HtmlScriptCollector scriptCollector1;
	protected HtmlOutputLinkEx linkExLogOutLinkTemplate1;
	protected HtmlOutputText textLogoutLinkTemplateLabel1;
	protected HtmlOutputText text2;
	protected HtmlOutputLinkEx linkExpageMoved1;

	protected HtmlScriptCollector getScriptCollectorMainOrderEntryCollector() {
		if (scriptCollectorMainOrderEntryCollector == null) {
			scriptCollectorMainOrderEntryCollector = (HtmlScriptCollector) findComponentInRoot("scriptCollectorMainOrderEntryCollector");
		}
		return scriptCollectorMainOrderEntryCollector;
	}

	protected HtmlForm getFormOrderEntryForm() {
		if (formOrderEntryForm == null) {
			formOrderEntryForm = (HtmlForm) findComponentInRoot("formOrderEntryForm");
		}
		return formOrderEntryForm;
	}

	protected HtmlPanelGrid getGridDeliveryInfoGrid() {
		if (gridDeliveryInfoGrid == null) {
			gridDeliveryInfoGrid = (HtmlPanelGrid) findComponentInRoot("gridDeliveryInfoGrid");
		}
		return gridDeliveryInfoGrid;
	}

	protected HtmlPanelGrid getGrid1() {
		if (grid1 == null) {
			grid1 = (HtmlPanelGrid) findComponentInRoot("grid1");
		}
		return grid1;
	}

	protected HtmlJspPanel getJspPanelleftSidePanel() {
		if (jspPanelleftSidePanel == null) {
			jspPanelleftSidePanel = (HtmlJspPanel) findComponentInRoot("jspPanelleftSidePanel");
		}
		return jspPanelleftSidePanel;
	}

	protected HtmlPanelGrid getGridLeftPanelMainGrid() {
		if (gridLeftPanelMainGrid == null) {
			gridLeftPanelMainGrid = (HtmlPanelGrid) findComponentInRoot("gridLeftPanelMainGrid");
		}
		return gridLeftPanelMainGrid;
	}

	protected HtmlPanelGrid getGridInnerLeftGrid() {
		if (gridInnerLeftGrid == null) {
			gridInnerLeftGrid = (HtmlPanelGrid) findComponentInRoot("gridInnerLeftGrid");
		}
		return gridInnerLeftGrid;
	}

	protected HtmlOutputText getTextFAQLabel() {
		if (textFAQLabel == null) {
			textFAQLabel = (HtmlOutputText) findComponentInRoot("textFAQLabel");
		}
		return textFAQLabel;
	}

	protected HtmlOutputLinkEx getLinkEx1() {
		if (linkEx1 == null) {
			linkEx1 = (HtmlOutputLinkEx) findComponentInRoot("linkEx1");
		}
		return linkEx1;
	}

	protected HtmlGraphicImageEx getImageExLeftSidePlaceHolder() {
		if (imageExLeftSidePlaceHolder == null) {
			imageExLeftSidePlaceHolder = (HtmlGraphicImageEx) findComponentInRoot("imageExLeftSidePlaceHolder");
		}
		return imageExLeftSidePlaceHolder;
	}

	/**
	 * @managed-bean true
	 */
	protected SubscriptionOfferHandler getCurrentOfferHandler() {
		if (currentOfferHandler == null) {
			currentOfferHandler = (SubscriptionOfferHandler) getManagedBean("currentOfferHandler");
		}
		return currentOfferHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setCurrentOfferHandler(SubscriptionOfferHandler currentOfferHandler) {
		this.currentOfferHandler = currentOfferHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected NewSubscriptionOrderHandler getNewSubscriptionOrderHandler() {
		if (newSubscriptionOrderHandler == null) {
			newSubscriptionOrderHandler = (NewSubscriptionOrderHandler) getManagedBean("newSubscriptionOrderHandler");
		}
		return newSubscriptionOrderHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setNewSubscriptionOrderHandler(NewSubscriptionOrderHandler newSubscriptionOrderHandler) {
		this.newSubscriptionOrderHandler = newSubscriptionOrderHandler;
	}

	public void onPageLoadBegin(FacesContext facescontext) {

	}

	/**
	 * @managed-bean true
	 */
	protected ShoppingCartHandler getShoppingCartHandler() {
		if (shoppingCartHandler == null) {
			shoppingCartHandler = (ShoppingCartHandler) getManagedBean("shoppingCartHandler");
		}
		return shoppingCartHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setShoppingCartHandler(ShoppingCartHandler shoppingCartHandler) {
		this.shoppingCartHandler = shoppingCartHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected CustomerHandler getCustomerHandler() {
		if (customerHandler == null) {
			customerHandler = (CustomerHandler) getManagedBean("customerHandler");
		}
		return customerHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setCustomerHandler(CustomerHandler customerHandler) {
		this.customerHandler = customerHandler;
	}

	protected HtmlOutputText getTextIntEditionHolder() {
		if (textIntEditionHolder == null) {
			textIntEditionHolder = (HtmlOutputText) findComponentInRoot("textIntEditionHolder");
		}
		return textIntEditionHolder;
	}

	protected HtmlPanelGrid getGridNavigationOuterGrid() {
		if (gridNavigationOuterGrid == null) {
			gridNavigationOuterGrid = (HtmlPanelGrid) findComponentInRoot("gridNavigationOuterGrid");
		}
		return gridNavigationOuterGrid;
	}

	protected HtmlPanelGrid getGridLeftNavGroup1Grid() {
		if (gridLeftNavGroup1Grid == null) {
			gridLeftNavGroup1Grid = (HtmlPanelGrid) findComponentInRoot("gridLeftNavGroup1Grid");
		}
		return gridLeftNavGroup1Grid;
	}

	protected HtmlOutputLinkEx getLinkExNavGroup1() {
		if (linkExNavGroup1 == null) {
			linkExNavGroup1 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavGroup1");
		}
		return linkExNavGroup1;
	}

	protected HtmlOutputText getTextNavGroup1LinkLabel1() {
		if (textNavGroup1LinkLabel1 == null) {
			textNavGroup1LinkLabel1 = (HtmlOutputText) findComponentInRoot("textNavGroup1LinkLabel1");
		}
		return textNavGroup1LinkLabel1;
	}

	protected HtmlOutputText getTextNavGroup1LinkLabel2() {
		if (textNavGroup1LinkLabel2 == null) {
			textNavGroup1LinkLabel2 = (HtmlOutputText) findComponentInRoot("textNavGroup1LinkLabel2");
		}
		return textNavGroup1LinkLabel2;
	}

	protected HtmlOutputText getTextNavGroup1LinkLabel3() {
		if (textNavGroup1LinkLabel3 == null) {
			textNavGroup1LinkLabel3 = (HtmlOutputText) findComponentInRoot("textNavGroup1LinkLabel3");
		}
		return textNavGroup1LinkLabel3;
	}

	protected HtmlOutputText getTextNavGroup1LinkLabel4() {
		if (textNavGroup1LinkLabel4 == null) {
			textNavGroup1LinkLabel4 = (HtmlOutputText) findComponentInRoot("textNavGroup1LinkLabel4");
		}
		return textNavGroup1LinkLabel4;
	}

	protected HtmlPanelGroup getGroupLeftNavGroup1Header() {
		if (groupLeftNavGroup1Header == null) {
			groupLeftNavGroup1Header = (HtmlPanelGroup) findComponentInRoot("groupLeftNavGroup1Header");
		}
		return groupLeftNavGroup1Header;
	}

	protected HtmlOutputText getTextGroup1HeaderText() {
		if (textGroup1HeaderText == null) {
			textGroup1HeaderText = (HtmlOutputText) findComponentInRoot("textGroup1HeaderText");
		}
		return textGroup1HeaderText;
	}

	protected HtmlOutputLinkEx getLinkExNavGroup2() {
		if (linkExNavGroup2 == null) {
			linkExNavGroup2 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavGroup2");
		}
		return linkExNavGroup2;
	}

	protected HtmlOutputLinkEx getLinkExNavGroup3() {
		if (linkExNavGroup3 == null) {
			linkExNavGroup3 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavGroup3");
		}
		return linkExNavGroup3;
	}

	protected HtmlOutputLinkEx getLinkExNavGroup4() {
		if (linkExNavGroup4 == null) {
			linkExNavGroup4 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavGroup4");
		}
		return linkExNavGroup4;
	}

	protected HtmlPanelGrid getGridLeftNavGroup2Grid() {
		if (gridLeftNavGroup2Grid == null) {
			gridLeftNavGroup2Grid = (HtmlPanelGrid) findComponentInRoot("gridLeftNavGroup2Grid");
		}
		return gridLeftNavGroup2Grid;
	}

	protected HtmlScriptCollector getScriptCollectorTemplateCollectorLeftNav() {
		if (scriptCollectorTemplateCollectorLeftNav == null) {
			scriptCollectorTemplateCollectorLeftNav = (HtmlScriptCollector) findComponentInRoot("scriptCollectorTemplateCollectorLeftNav");
		}
		return scriptCollectorTemplateCollectorLeftNav;
	}

	protected HtmlPanelGrid getGridMainBodyGrid() {
		if (gridMainBodyGrid == null) {
			gridMainBodyGrid = (HtmlPanelGrid) findComponentInRoot("gridMainBodyGrid");
		}
		return gridMainBodyGrid;
	}

	protected HtmlJspPanel getJspPanelHeaderPanel() {
		if (jspPanelHeaderPanel == null) {
			jspPanelHeaderPanel = (HtmlJspPanel) findComponentInRoot("jspPanelHeaderPanel");
		}
		return jspPanelHeaderPanel;
	}

	protected HtmlJspPanel getJspPanelFormPanel() {
		if (jspPanelFormPanel == null) {
			jspPanelFormPanel = (HtmlJspPanel) findComponentInRoot("jspPanelFormPanel");
		}
		return jspPanelFormPanel;
	}

	protected HtmlOutputText getTextMainTableHeader() {
		if (textMainTableHeader == null) {
			textMainTableHeader = (HtmlOutputText) findComponentInRoot("textMainTableHeader");
		}
		return textMainTableHeader;
	}

	/**
	 * @managed-bean true
	 */
	protected NavigationLinksHandler getEeditionTemplateNavHandler() {
		if (eeditionTemplateNavHandler == null) {
			eeditionTemplateNavHandler = (NavigationLinksHandler) getManagedBean("eeditionTemplateNavHandler");
		}
		return eeditionTemplateNavHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setEeditionTemplateNavHandler(NavigationLinksHandler eeditionTemplateNavHandler) {
		this.eeditionTemplateNavHandler = eeditionTemplateNavHandler;
	}

	protected HtmlOutputLinkEx getLinkExNavGroup1SubscriberServices() {
		if (linkExNavGroup1SubscriberServices == null) {
			linkExNavGroup1SubscriberServices = (HtmlOutputLinkEx) findComponentInRoot("linkExNavGroup1SubscriberServices");
		}
		return linkExNavGroup1SubscriberServices;
	}

	protected HtmlOutputText getTextNavGroup1LinkLabelInternational() {
		if (textNavGroup1LinkLabelInternational == null) {
			textNavGroup1LinkLabelInternational = (HtmlOutputText) findComponentInRoot("textNavGroup1LinkLabelInternational");
		}
		return textNavGroup1LinkLabelInternational;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link1() {
		if (linkExNavArea2Link1 == null) {
			linkExNavArea2Link1 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link1");
		}
		return linkExNavArea2Link1;
	}

	protected HtmlOutputText getTextNavArea2Link1() {
		if (textNavArea2Link1 == null) {
			textNavArea2Link1 = (HtmlOutputText) findComponentInRoot("textNavArea2Link1");
		}
		return textNavArea2Link1;
	}

	protected HtmlOutputText getTextNavArea2Link2() {
		if (textNavArea2Link2 == null) {
			textNavArea2Link2 = (HtmlOutputText) findComponentInRoot("textNavArea2Link2");
		}
		return textNavArea2Link2;
	}

	protected HtmlOutputText getTextNavArea2Link3() {
		if (textNavArea2Link3 == null) {
			textNavArea2Link3 = (HtmlOutputText) findComponentInRoot("textNavArea2Link3");
		}
		return textNavArea2Link3;
	}

	protected HtmlOutputText getTextNavArea2Link4() {
		if (textNavArea2Link4 == null) {
			textNavArea2Link4 = (HtmlOutputText) findComponentInRoot("textNavArea2Link4");
		}
		return textNavArea2Link4;
	}

	protected HtmlOutputLinkEx getLinkExNavGroup2International() {
		if (linkExNavGroup2International == null) {
			linkExNavGroup2International = (HtmlOutputLinkEx) findComponentInRoot("linkExNavGroup2International");
		}
		return linkExNavGroup2International;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link2() {
		if (linkExNavArea2Link2 == null) {
			linkExNavArea2Link2 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link2");
		}
		return linkExNavArea2Link2;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link3() {
		if (linkExNavArea2Link3 == null) {
			linkExNavArea2Link3 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link3");
		}
		return linkExNavArea2Link3;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link4() {
		if (linkExNavArea2Link4 == null) {
			linkExNavArea2Link4 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link4");
		}
		return linkExNavArea2Link4;
	}

	protected HtmlSelectOneRadio getRadio1() {
		if (radio1 == null) {
			radio1 = (HtmlSelectOneRadio) findComponentInRoot("radio1");
		}
		return radio1;
	}

	public void handleTextEmailAddressRecipientValidate(FacesContext facescontext, UIComponent component, Object object)
			throws javax.faces.validator.ValidatorException {
		// Type Java code to handle validate here
		String email = (String) object;

		if (email.indexOf('@') == -1) {
			((UIInput) component).setValid(false);

			FacesMessage message = new FacesMessage("Invalid Email Address");
			facescontext.addMessage(component.getClientId(facescontext), message);
		}
		// void validate(FacesContext facescontext, UIComponent component, Object object) throws ValidatorException
	}

	protected HtmlOutputText getTextNavGroup1LinkLabelKindle() {
		if (textNavGroup1LinkLabelKindle == null) {
			textNavGroup1LinkLabelKindle = (HtmlOutputText) findComponentInRoot("textNavGroup1LinkLabelKindle");
		}
		return textNavGroup1LinkLabelKindle;
	}

	protected HtmlOutputText getTextNavGroup1LinkLabelDotCom() {
		if (textNavGroup1LinkLabelDotCom == null) {
			textNavGroup1LinkLabelDotCom = (HtmlOutputText) findComponentInRoot("textNavGroup1LinkLabelDotCom");
		}
		return textNavGroup1LinkLabelDotCom;
	}

	protected HtmlOutputLinkEx getLinkExNavGroup2Kindle() {
		if (linkExNavGroup2Kindle == null) {
			linkExNavGroup2Kindle = (HtmlOutputLinkEx) findComponentInRoot("linkExNavGroup2Kindle");
		}
		return linkExNavGroup2Kindle;
	}

	protected HtmlOutputLinkEx getLinkExNavGroup2DotCom() {
		if (linkExNavGroup2DotCom == null) {
			linkExNavGroup2DotCom = (HtmlOutputLinkEx) findComponentInRoot("linkExNavGroup2DotCom");
		}
		return linkExNavGroup2DotCom;
	}

	protected HtmlOutputText getTextNavGroup1LinkLabelFAQ() {
		if (textNavGroup1LinkLabelFAQ == null) {
			textNavGroup1LinkLabelFAQ = (HtmlOutputText) findComponentInRoot("textNavGroup1LinkLabelFAQ");
		}
		return textNavGroup1LinkLabelFAQ;
	}

	protected HtmlOutputLinkEx getLinkExNavGroupFAQ() {
		if (linkExNavGroupFAQ == null) {
			linkExNavGroupFAQ = (HtmlOutputLinkEx) findComponentInRoot("linkExNavGroupFAQ");
		}
		return linkExNavGroupFAQ;
	}

	protected HtmlPanelFormBox getFormBox1() {
		if (formBox1 == null) {
			formBox1 = (HtmlPanelFormBox) findComponentInRoot("formBox1");
		}
		return formBox1;
	}

	/**
	 * @managed-bean true
	 */
	protected TrialCustomerHandler getTrialCustomerHandler() {
		if (trialCustomerHandler == null) {
			trialCustomerHandler = (TrialCustomerHandler) getManagedBean("trialCustomerHandler");
		}
		return trialCustomerHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setTrialCustomerHandler(TrialCustomerHandler trialCustomerHandler) {
		this.trialCustomerHandler = trialCustomerHandler;
	}

	protected HtmlScriptCollector getScriptCollectorTopNavCollector() {
		if (scriptCollectorTopNavCollector == null) {
			scriptCollectorTopNavCollector = (HtmlScriptCollector) findComponentInRoot("scriptCollectorTopNavCollector");
		}
		return scriptCollectorTopNavCollector;
	}

	protected HtmlJspPanel getJspPanelTopNavCustomPanel() {
		if (jspPanelTopNavCustomPanel == null) {
			jspPanelTopNavCustomPanel = (HtmlJspPanel) findComponentInRoot("jspPanelTopNavCustomPanel");
		}
		return jspPanelTopNavCustomPanel;
	}

	protected HtmlPanelGrid getGridNavigationTopGrid() {
		if (gridNavigationTopGrid == null) {
			gridNavigationTopGrid = (HtmlPanelGrid) findComponentInRoot("gridNavigationTopGrid");
		}
		return gridNavigationTopGrid;
	}

	protected HtmlOutputText getTextNavArea2Link5() {
		if (textNavArea2Link5 == null) {
			textNavArea2Link5 = (HtmlOutputText) findComponentInRoot("textNavArea2Link5");
		}
		return textNavArea2Link5;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link5() {
		if (linkExNavArea2Link5 == null) {
			linkExNavArea2Link5 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link5");
		}
		return linkExNavArea2Link5;
	}

	protected HtmlPanelGrid getGridNavigationBottomGrid() {
		if (gridNavigationBottomGrid == null) {
			gridNavigationBottomGrid = (HtmlPanelGrid) findComponentInRoot("gridNavigationBottomGrid");
		}
		return gridNavigationBottomGrid;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link0() {
		if (linkExNavArea2Link0 == null) {
			linkExNavArea2Link0 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link0");
		}
		return linkExNavArea2Link0;
	}

	protected HtmlOutputText getTextNavArea2Link0() {
		if (textNavArea2Link0 == null) {
			textNavArea2Link0 = (HtmlOutputText) findComponentInRoot("textNavArea2Link0");
		}
		return textNavArea2Link0;
	}

	protected HtmlScriptCollector getScriptCollectorTemplateLogoutCollector() {
		if (scriptCollectorTemplateLogoutCollector == null) {
			scriptCollectorTemplateLogoutCollector = (HtmlScriptCollector) findComponentInRoot("scriptCollectorTemplateLogoutCollector");
		}
		return scriptCollectorTemplateLogoutCollector;
	}

	protected HtmlForm getFormLogoutTemplateAction() {
		if (formLogoutTemplateAction == null) {
			formLogoutTemplateAction = (HtmlForm) findComponentInRoot("formLogoutTemplateAction");
		}
		return formLogoutTemplateAction;
	}

	protected HtmlCommandLink getLinkTemplateDoLogout() {
		if (linkTemplateDoLogout == null) {
			linkTemplateDoLogout = (HtmlCommandLink) findComponentInRoot("linkTemplateDoLogout");
		}
		return linkTemplateDoLogout;
	}

	protected HtmlOutputText getTextTemplateLogoutTextLabel() {
		if (textTemplateLogoutTextLabel == null) {
			textTemplateLogoutTextLabel = (HtmlOutputText) findComponentInRoot("textTemplateLogoutTextLabel");
		}
		return textTemplateLogoutTextLabel;
	}

	protected HtmlGraphicImageEx getImageExLinkServices2Link() {
		if (imageExLinkServices2Link == null) {
			imageExLinkServices2Link = (HtmlGraphicImageEx) findComponentInRoot("imageExLinkServices2Link");
		}
		return imageExLinkServices2Link;
	}

	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlOutputLinkEx getLinkExLogOutLinkTemplate1() {
		if (linkExLogOutLinkTemplate1 == null) {
			linkExLogOutLinkTemplate1 = (HtmlOutputLinkEx) findComponentInRoot("linkExLogOutLinkTemplate1");
		}
		return linkExLogOutLinkTemplate1;
	}

	protected HtmlOutputText getTextLogoutLinkTemplateLabel1() {
		if (textLogoutLinkTemplateLabel1 == null) {
			textLogoutLinkTemplateLabel1 = (HtmlOutputText) findComponentInRoot("textLogoutLinkTemplateLabel1");
		}
		return textLogoutLinkTemplateLabel1;
	}

	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected HtmlOutputLinkEx getLinkExpageMoved1() {
		if (linkExpageMoved1 == null) {
			linkExpageMoved1 = (HtmlOutputLinkEx) findComponentInRoot("linkExpageMoved1");
		}
		return linkExpageMoved1;
	}

}