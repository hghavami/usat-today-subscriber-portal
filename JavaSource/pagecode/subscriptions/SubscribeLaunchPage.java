/**
 * 
 */
package pagecode.subscriptions;

import pagecode.PageCodeBase;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.business.interfaces.products.SubscriptionOfferIntf;
import com.usatoday.esub.common.UTCommon;
import com.usatoday.esub.handlers.SubscriptionOfferHandler;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.usatoday.esub.handlers.NavigationLinksHandler;
import com.ibm.faces.component.html.HtmlGraphicImageEx;
import javax.faces.component.html.HtmlForm;

/**
 * @author aeast
 * 
 */
public class SubscribeLaunchPage extends PageCodeBase {

	protected SubscriptionOfferHandler currentOfferHandler;
	protected HtmlScriptCollector scriptCollectorTemplateCollectorLeftNav;
	protected HtmlPanelGrid gridNavigationOuterGrid;
	protected HtmlPanelGrid gridLeftNavGroup1Grid;
	protected HtmlOutputLinkEx linkExNavGroup1SubscriberServices;
	protected HtmlOutputText textNavGroup1LinkLabel1;
	protected HtmlOutputText textNavGroup1LinkLabelInternational;
	protected HtmlOutputLinkEx linkExNavGroup2International;
	protected HtmlPanelGrid gridLeftNavGroup2Grid;
	protected HtmlJspPanel jspPanelLeftSideEEdition;
	protected NavigationLinksHandler eeditionTemplateNavHandler;
	protected HtmlOutputLinkEx linkExNavArea2Link1;
	protected HtmlOutputText textNavArea2Link1;
	protected HtmlOutputText textNavArea2Link2;
	protected HtmlOutputText textNavArea2Link3;
	protected HtmlOutputText textNavArea2Link4;
	protected HtmlOutputLinkEx linkExNavArea2Link2;
	protected HtmlOutputLinkEx linkExNavArea2Link3;
	protected HtmlOutputLinkEx linkExNavArea2Link4;
	protected HtmlPanelGrid gridVideoGrid_B;
	protected HtmlOutputLinkEx linkExVideoLink_B;
	protected HtmlGraphicImageEx imageExVideoSweepsGraphic_B;
	protected HtmlScriptCollector scriptCollector1;
	protected HtmlOutputText textEEVideoText;
	protected HtmlScriptCollector scriptCollectorMainForm;
	protected HtmlPanelGrid gridMainBodyGrid;
	protected HtmlJspPanel jspPanelHeaderPanel;
	protected HtmlOutputText textCustomPromoText;
	protected HtmlPanelGrid gridLeftSidePrintdition;
	protected HtmlJspPanel jjspPanelRightSideEEdition1;
	protected HtmlOutputLinkEx linkExSubscribeToPrint;
	protected HtmlPanelGrid gridRightSideEEdition;
	protected HtmlJspPanel jspPanelRightSideEEdition1a;
	protected HtmlOutputLinkEx linkExLearnMoreAboutELink;
	protected HtmlOutputText textLearnMoreAboutELabel;
	protected HtmlPanelGrid gridEESubscribeButtonsGrid;
	protected HtmlOutputLinkEx linkExSubscribeToEE;
	protected HtmlPanelGrid gridUTSubscribeButtonsGrid;
	protected HtmlJspPanel jjspPanelLeftSidePrintEdition2;
	protected HtmlJspPanel jjspPanelLeftSidePrintEdition3;
	protected HtmlJspPanel jspPanelLeftSideEEdition3;
	protected HtmlJspPanel jjspPanelLeftSidePrintEdition4;
	protected HtmlJspPanel jspPanelRightSideEEdition1b;
	protected HtmlJspPanel jspPanelLeftSideEEdition1;
	protected HtmlJspPanel jspPanelLeftSideEEdition2;
	protected HtmlScriptCollector scriptCollectorTemplateLogoutCollector;
	protected HtmlForm formLogoutTemplateAction;
	protected HtmlOutputLinkEx linkExLogOutLinkTemplate1;
	protected HtmlOutputText textLogoutLinkTemplateLabel1;
	protected HtmlPanelGrid gridNavigationTopGrid;
	protected HtmlOutputText textNavGroup1LinkLabelKindle;
	protected HtmlOutputText textNavGroup1LinkLabelDotCom;
	protected HtmlOutputText textNavGroup1LinkLabelFAQ;
	protected HtmlOutputLinkEx linkExNavArea2Link0;
	protected HtmlOutputText textNavArea2Link0;
	protected HtmlOutputText textNavArea2Link5;
	protected HtmlOutputLinkEx linkExNavGroup2Kindle;
	protected HtmlOutputLinkEx linkExNavGroup2DotCom;
	protected HtmlOutputLinkEx linkExNavGroupFAQ;
	protected HtmlOutputLinkEx linkExNavArea2Link5;
	protected HtmlPanelGrid gridNavigationBottomGrid;
	protected HtmlOutputText textNavOptionalLinkLabelSingleCopy;

	/**
	 * @managed-bean true
	 */
	protected SubscriptionOfferHandler getCurrentOfferHandler() {
		if (currentOfferHandler == null) {
			currentOfferHandler = (SubscriptionOfferHandler) getManagedBean("currentOfferHandler");
		}
		return currentOfferHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setCurrentOfferHandler(SubscriptionOfferHandler currentOfferHandler) {
		this.currentOfferHandler = currentOfferHandler;
	}

	public void onPageLoadBegin(FacesContext facescontext) {
		this.getEeditionTemplateNavHandler().setShowNavHeaderArea1(true);
		this.getEeditionTemplateNavHandler().setShowNavHeaderArea2(false);

		SubscriptionOfferHandler offer = this.getCurrentOfferHandler();

		HttpServletRequest req = (HttpServletRequest) this.getFacesContext().getExternalContext().getRequest();

		// note, this means this page will always be e-only...if we modify the way we track the current
		// offer this page would support orders for any publication.
		SubscriptionOfferIntf defaultOffer = (SubscriptionOfferIntf) UTCommon.getCurrentOffer(req);

		offer.setCurrentOffer(defaultOffer);

	}

	protected HtmlScriptCollector getScriptCollectorTemplateCollectorLeftNav() {
		if (scriptCollectorTemplateCollectorLeftNav == null) {
			scriptCollectorTemplateCollectorLeftNav = (HtmlScriptCollector) findComponentInRoot("scriptCollectorTemplateCollectorLeftNav");
		}
		return scriptCollectorTemplateCollectorLeftNav;
	}

	protected HtmlPanelGrid getGridNavigationOuterGrid() {
		if (gridNavigationOuterGrid == null) {
			gridNavigationOuterGrid = (HtmlPanelGrid) findComponentInRoot("gridNavigationOuterGrid");
		}
		return gridNavigationOuterGrid;
	}

	protected HtmlPanelGrid getGridLeftNavGroup1Grid() {
		if (gridLeftNavGroup1Grid == null) {
			gridLeftNavGroup1Grid = (HtmlPanelGrid) findComponentInRoot("gridLeftNavGroup1Grid");
		}
		return gridLeftNavGroup1Grid;
	}

	protected HtmlOutputLinkEx getLinkExNavGroup1SubscriberServices() {
		if (linkExNavGroup1SubscriberServices == null) {
			linkExNavGroup1SubscriberServices = (HtmlOutputLinkEx) findComponentInRoot("linkExNavGroup1SubscriberServices");
		}
		return linkExNavGroup1SubscriberServices;
	}

	protected HtmlOutputText getTextNavGroup1LinkLabel1() {
		if (textNavGroup1LinkLabel1 == null) {
			textNavGroup1LinkLabel1 = (HtmlOutputText) findComponentInRoot("textNavGroup1LinkLabel1");
		}
		return textNavGroup1LinkLabel1;
	}

	protected HtmlOutputText getTextNavGroup1LinkLabelInternational() {
		if (textNavGroup1LinkLabelInternational == null) {
			textNavGroup1LinkLabelInternational = (HtmlOutputText) findComponentInRoot("textNavGroup1LinkLabelInternational");
		}
		return textNavGroup1LinkLabelInternational;
	}

	protected HtmlOutputLinkEx getLinkExNavGroup2International() {
		if (linkExNavGroup2International == null) {
			linkExNavGroup2International = (HtmlOutputLinkEx) findComponentInRoot("linkExNavGroup2International");
		}
		return linkExNavGroup2International;
	}

	protected HtmlPanelGrid getGridLeftNavGroup2Grid() {
		if (gridLeftNavGroup2Grid == null) {
			gridLeftNavGroup2Grid = (HtmlPanelGrid) findComponentInRoot("gridLeftNavGroup2Grid");
		}
		return gridLeftNavGroup2Grid;
	}

	protected HtmlJspPanel getJspPanelLeftSideEEdition() {
		if (jspPanelLeftSideEEdition == null) {
			jspPanelLeftSideEEdition = (HtmlJspPanel) findComponentInRoot("jspPanelLeftSideEEdition");
		}
		return jspPanelLeftSideEEdition;
	}

	/**
	 * @managed-bean true
	 */
	protected NavigationLinksHandler getEeditionTemplateNavHandler() {
		if (eeditionTemplateNavHandler == null) {
			eeditionTemplateNavHandler = (NavigationLinksHandler) getManagedBean("eeditionTemplateNavHandler");
		}
		return eeditionTemplateNavHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setEeditionTemplateNavHandler(NavigationLinksHandler eeditionTemplateNavHandler) {
		this.eeditionTemplateNavHandler = eeditionTemplateNavHandler;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link1() {
		if (linkExNavArea2Link1 == null) {
			linkExNavArea2Link1 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link1");
		}
		return linkExNavArea2Link1;
	}

	protected HtmlOutputText getTextNavArea2Link1() {
		if (textNavArea2Link1 == null) {
			textNavArea2Link1 = (HtmlOutputText) findComponentInRoot("textNavArea2Link1");
		}
		return textNavArea2Link1;
	}

	protected HtmlOutputText getTextNavArea2Link2() {
		if (textNavArea2Link2 == null) {
			textNavArea2Link2 = (HtmlOutputText) findComponentInRoot("textNavArea2Link2");
		}
		return textNavArea2Link2;
	}

	protected HtmlOutputText getTextNavArea2Link3() {
		if (textNavArea2Link3 == null) {
			textNavArea2Link3 = (HtmlOutputText) findComponentInRoot("textNavArea2Link3");
		}
		return textNavArea2Link3;
	}

	protected HtmlOutputText getTextNavArea2Link4() {
		if (textNavArea2Link4 == null) {
			textNavArea2Link4 = (HtmlOutputText) findComponentInRoot("textNavArea2Link4");
		}
		return textNavArea2Link4;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link2() {
		if (linkExNavArea2Link2 == null) {
			linkExNavArea2Link2 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link2");
		}
		return linkExNavArea2Link2;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link3() {
		if (linkExNavArea2Link3 == null) {
			linkExNavArea2Link3 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link3");
		}
		return linkExNavArea2Link3;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link4() {
		if (linkExNavArea2Link4 == null) {
			linkExNavArea2Link4 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link4");
		}
		return linkExNavArea2Link4;
	}

	protected HtmlPanelGrid getGridVideoGrid_B() {
		if (gridVideoGrid_B == null) {
			gridVideoGrid_B = (HtmlPanelGrid) findComponentInRoot("gridVideoGrid_B");
		}
		return gridVideoGrid_B;
	}

	protected HtmlOutputLinkEx getLinkExVideoLink_B() {
		if (linkExVideoLink_B == null) {
			linkExVideoLink_B = (HtmlOutputLinkEx) findComponentInRoot("linkExVideoLink_B");
		}
		return linkExVideoLink_B;
	}

	protected HtmlGraphicImageEx getImageExVideoSweepsGraphic_B() {
		if (imageExVideoSweepsGraphic_B == null) {
			imageExVideoSweepsGraphic_B = (HtmlGraphicImageEx) findComponentInRoot("imageExVideoSweepsGraphic_B");
		}
		return imageExVideoSweepsGraphic_B;
	}

	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlOutputText getTextEEVideoText() {
		if (textEEVideoText == null) {
			textEEVideoText = (HtmlOutputText) findComponentInRoot("textEEVideoText");
		}
		return textEEVideoText;
	}

	protected HtmlScriptCollector getScriptCollectorMainForm() {
		if (scriptCollectorMainForm == null) {
			scriptCollectorMainForm = (HtmlScriptCollector) findComponentInRoot("scriptCollectorMainForm");
		}
		return scriptCollectorMainForm;
	}

	protected HtmlPanelGrid getGridMainBodyGrid() {
		if (gridMainBodyGrid == null) {
			gridMainBodyGrid = (HtmlPanelGrid) findComponentInRoot("gridMainBodyGrid");
		}
		return gridMainBodyGrid;
	}

	protected HtmlJspPanel getJspPanelHeaderPanel() {
		if (jspPanelHeaderPanel == null) {
			jspPanelHeaderPanel = (HtmlJspPanel) findComponentInRoot("jspPanelHeaderPanel");
		}
		return jspPanelHeaderPanel;
	}

	protected HtmlOutputText getTextCustomPromoText() {
		if (textCustomPromoText == null) {
			textCustomPromoText = (HtmlOutputText) findComponentInRoot("textCustomPromoText");
		}
		return textCustomPromoText;
	}

	protected HtmlPanelGrid getGridLeftSidePrintdition() {
		if (gridLeftSidePrintdition == null) {
			gridLeftSidePrintdition = (HtmlPanelGrid) findComponentInRoot("gridLeftSidePrintdition");
		}
		return gridLeftSidePrintdition;
	}

	protected HtmlJspPanel getJjspPanelRightSideEEdition1() {
		if (jjspPanelRightSideEEdition1 == null) {
			jjspPanelRightSideEEdition1 = (HtmlJspPanel) findComponentInRoot("jjspPanelRightSideEEdition1");
		}
		return jjspPanelRightSideEEdition1;
	}

	protected HtmlOutputLinkEx getLinkExSubscribeToPrint() {
		if (linkExSubscribeToPrint == null) {
			linkExSubscribeToPrint = (HtmlOutputLinkEx) findComponentInRoot("linkExSubscribeToPrint");
		}
		return linkExSubscribeToPrint;
	}

	protected HtmlPanelGrid getGridRightSideEEdition() {
		if (gridRightSideEEdition == null) {
			gridRightSideEEdition = (HtmlPanelGrid) findComponentInRoot("gridRightSideEEdition");
		}
		return gridRightSideEEdition;
	}

	protected HtmlJspPanel getJspPanelRightSideEEdition1a() {
		if (jspPanelRightSideEEdition1a == null) {
			jspPanelRightSideEEdition1a = (HtmlJspPanel) findComponentInRoot("jspPanelRightSideEEdition1a");
		}
		return jspPanelRightSideEEdition1a;
	}

	protected HtmlOutputLinkEx getLinkExLearnMoreAboutELink() {
		if (linkExLearnMoreAboutELink == null) {
			linkExLearnMoreAboutELink = (HtmlOutputLinkEx) findComponentInRoot("linkExLearnMoreAboutELink");
		}
		return linkExLearnMoreAboutELink;
	}

	protected HtmlOutputText getTextLearnMoreAboutELabel() {
		if (textLearnMoreAboutELabel == null) {
			textLearnMoreAboutELabel = (HtmlOutputText) findComponentInRoot("textLearnMoreAboutELabel");
		}
		return textLearnMoreAboutELabel;
	}

	protected HtmlPanelGrid getGridEESubscribeButtonsGrid() {
		if (gridEESubscribeButtonsGrid == null) {
			gridEESubscribeButtonsGrid = (HtmlPanelGrid) findComponentInRoot("gridEESubscribeButtonsGrid");
		}
		return gridEESubscribeButtonsGrid;
	}

	protected HtmlOutputLinkEx getLinkExSubscribeToEE() {
		if (linkExSubscribeToEE == null) {
			linkExSubscribeToEE = (HtmlOutputLinkEx) findComponentInRoot("linkExSubscribeToEE");
		}
		return linkExSubscribeToEE;
	}

	protected HtmlPanelGrid getGridUTSubscribeButtonsGrid() {
		if (gridUTSubscribeButtonsGrid == null) {
			gridUTSubscribeButtonsGrid = (HtmlPanelGrid) findComponentInRoot("gridUTSubscribeButtonsGrid");
		}
		return gridUTSubscribeButtonsGrid;
	}

	protected HtmlJspPanel getJjspPanelLeftSidePrintEdition2() {
		if (jjspPanelLeftSidePrintEdition2 == null) {
			jjspPanelLeftSidePrintEdition2 = (HtmlJspPanel) findComponentInRoot("jjspPanelLeftSidePrintEdition2");
		}
		return jjspPanelLeftSidePrintEdition2;
	}

	protected HtmlJspPanel getJjspPanelLeftSidePrintEdition3() {
		if (jjspPanelLeftSidePrintEdition3 == null) {
			jjspPanelLeftSidePrintEdition3 = (HtmlJspPanel) findComponentInRoot("jjspPanelLeftSidePrintEdition3");
		}
		return jjspPanelLeftSidePrintEdition3;
	}

	protected HtmlJspPanel getJspPanelLeftSideEEdition3() {
		if (jspPanelLeftSideEEdition3 == null) {
			jspPanelLeftSideEEdition3 = (HtmlJspPanel) findComponentInRoot("jspPanelLeftSideEEdition3");
		}
		return jspPanelLeftSideEEdition3;
	}

	protected HtmlJspPanel getJjspPanelLeftSidePrintEdition4() {
		if (jjspPanelLeftSidePrintEdition4 == null) {
			jjspPanelLeftSidePrintEdition4 = (HtmlJspPanel) findComponentInRoot("jjspPanelLeftSidePrintEdition4");
		}
		return jjspPanelLeftSidePrintEdition4;
	}

	protected HtmlJspPanel getJspPanelRightSideEEdition1b() {
		if (jspPanelRightSideEEdition1b == null) {
			jspPanelRightSideEEdition1b = (HtmlJspPanel) findComponentInRoot("jspPanelRightSideEEdition1b");
		}
		return jspPanelRightSideEEdition1b;
	}

	protected HtmlJspPanel getJspPanelLeftSideEEdition1() {
		if (jspPanelLeftSideEEdition1 == null) {
			jspPanelLeftSideEEdition1 = (HtmlJspPanel) findComponentInRoot("jspPanelLeftSideEEdition1");
		}
		return jspPanelLeftSideEEdition1;
	}

	protected HtmlJspPanel getJspPanelLeftSideEEdition2() {
		if (jspPanelLeftSideEEdition2 == null) {
			jspPanelLeftSideEEdition2 = (HtmlJspPanel) findComponentInRoot("jspPanelLeftSideEEdition2");
		}
		return jspPanelLeftSideEEdition2;
	}

	protected HtmlScriptCollector getScriptCollectorTemplateLogoutCollector() {
		if (scriptCollectorTemplateLogoutCollector == null) {
			scriptCollectorTemplateLogoutCollector = (HtmlScriptCollector) findComponentInRoot("scriptCollectorTemplateLogoutCollector");
		}
		return scriptCollectorTemplateLogoutCollector;
	}

	protected HtmlForm getFormLogoutTemplateAction() {
		if (formLogoutTemplateAction == null) {
			formLogoutTemplateAction = (HtmlForm) findComponentInRoot("formLogoutTemplateAction");
		}
		return formLogoutTemplateAction;
	}

	protected HtmlOutputLinkEx getLinkExLogOutLinkTemplate1() {
		if (linkExLogOutLinkTemplate1 == null) {
			linkExLogOutLinkTemplate1 = (HtmlOutputLinkEx) findComponentInRoot("linkExLogOutLinkTemplate1");
		}
		return linkExLogOutLinkTemplate1;
	}

	protected HtmlOutputText getTextLogoutLinkTemplateLabel1() {
		if (textLogoutLinkTemplateLabel1 == null) {
			textLogoutLinkTemplateLabel1 = (HtmlOutputText) findComponentInRoot("textLogoutLinkTemplateLabel1");
		}
		return textLogoutLinkTemplateLabel1;
	}

	protected HtmlPanelGrid getGridNavigationTopGrid() {
		if (gridNavigationTopGrid == null) {
			gridNavigationTopGrid = (HtmlPanelGrid) findComponentInRoot("gridNavigationTopGrid");
		}
		return gridNavigationTopGrid;
	}

	protected HtmlOutputText getTextNavGroup1LinkLabelKindle() {
		if (textNavGroup1LinkLabelKindle == null) {
			textNavGroup1LinkLabelKindle = (HtmlOutputText) findComponentInRoot("textNavGroup1LinkLabelKindle");
		}
		return textNavGroup1LinkLabelKindle;
	}

	protected HtmlOutputText getTextNavGroup1LinkLabelDotCom() {
		if (textNavGroup1LinkLabelDotCom == null) {
			textNavGroup1LinkLabelDotCom = (HtmlOutputText) findComponentInRoot("textNavGroup1LinkLabelDotCom");
		}
		return textNavGroup1LinkLabelDotCom;
	}

	protected HtmlOutputText getTextNavGroup1LinkLabelFAQ() {
		if (textNavGroup1LinkLabelFAQ == null) {
			textNavGroup1LinkLabelFAQ = (HtmlOutputText) findComponentInRoot("textNavGroup1LinkLabelFAQ");
		}
		return textNavGroup1LinkLabelFAQ;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link0() {
		if (linkExNavArea2Link0 == null) {
			linkExNavArea2Link0 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link0");
		}
		return linkExNavArea2Link0;
	}

	protected HtmlOutputText getTextNavArea2Link0() {
		if (textNavArea2Link0 == null) {
			textNavArea2Link0 = (HtmlOutputText) findComponentInRoot("textNavArea2Link0");
		}
		return textNavArea2Link0;
	}

	protected HtmlOutputText getTextNavArea2Link5() {
		if (textNavArea2Link5 == null) {
			textNavArea2Link5 = (HtmlOutputText) findComponentInRoot("textNavArea2Link5");
		}
		return textNavArea2Link5;
	}

	protected HtmlOutputLinkEx getLinkExNavGroup2Kindle() {
		if (linkExNavGroup2Kindle == null) {
			linkExNavGroup2Kindle = (HtmlOutputLinkEx) findComponentInRoot("linkExNavGroup2Kindle");
		}
		return linkExNavGroup2Kindle;
	}

	protected HtmlOutputLinkEx getLinkExNavGroup2DotCom() {
		if (linkExNavGroup2DotCom == null) {
			linkExNavGroup2DotCom = (HtmlOutputLinkEx) findComponentInRoot("linkExNavGroup2DotCom");
		}
		return linkExNavGroup2DotCom;
	}

	protected HtmlOutputLinkEx getLinkExNavGroupFAQ() {
		if (linkExNavGroupFAQ == null) {
			linkExNavGroupFAQ = (HtmlOutputLinkEx) findComponentInRoot("linkExNavGroupFAQ");
		}
		return linkExNavGroupFAQ;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link5() {
		if (linkExNavArea2Link5 == null) {
			linkExNavArea2Link5 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link5");
		}
		return linkExNavArea2Link5;
	}

	protected HtmlPanelGrid getGridNavigationBottomGrid() {
		if (gridNavigationBottomGrid == null) {
			gridNavigationBottomGrid = (HtmlPanelGrid) findComponentInRoot("gridNavigationBottomGrid");
		}
		return gridNavigationBottomGrid;
	}

	protected HtmlOutputText getTextNavOptionalLinkLabelSingleCopy() {
		if (textNavOptionalLinkLabelSingleCopy == null) {
			textNavOptionalLinkLabelSingleCopy = (HtmlOutputText) findComponentInRoot("textNavOptionalLinkLabelSingleCopy");
		}
		return textNavOptionalLinkLabelSingleCopy;
	}

}