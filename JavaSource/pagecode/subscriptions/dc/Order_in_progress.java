/**
 * 
 */
package pagecode.subscriptions.dc;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import pagecode.PageCodeBase;

import com.usatoday.esub.handlers.ShoppingCartHandler;

/**
 * @author aeast
 * 
 */
public class Order_in_progress extends PageCodeBase {

	protected ShoppingCartHandler shoppingCartHandler;

	public String doButtonUpdateStatusAction() {
		ShoppingCartHandler sch = this.getShoppingCartHandler();

		if (sch.isCheckoutStarted() || sch.getCart().isCheckoutInProcess()) {
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"Your order is still processing payment and address verification..please wait.", null);

			context.addMessage(null, facesMsg);
			return "processing";
		}

		if (sch.getLastOrder() != null) {
			return "orderProcessed";
		} else {
			try {
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
						"Your order did not complete. Please try again or call our National Customer Service for assistance. Detail:"
								+ sch.getCart().getCheckOutErrorMessage(), null);

				context.addMessage(null, facesMsg);
				return "redo";
			} catch (Exception e) {
				System.out.println("Failed to determine order status." + e.getMessage());
				e.printStackTrace();
				return "redo";
			}
		}
	}

	/**
	 * @managed-bean true
	 */
	protected ShoppingCartHandler getShoppingCartHandler() {
		if (shoppingCartHandler == null) {
			shoppingCartHandler = (ShoppingCartHandler) getManagedBean("shoppingCartHandler");
		}
		return shoppingCartHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setShoppingCartHandler(ShoppingCartHandler shoppingCartHandler) {
		this.shoppingCartHandler = shoppingCartHandler;
	}

}