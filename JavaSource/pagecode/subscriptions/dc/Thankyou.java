/**
 * 
 */
package pagecode.subscriptions.dc;

import pagecode.PageCodeBase;
import javax.faces.component.html.HtmlOutputText;
import com.ibm.faces.component.html.HtmlScriptCollector;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlMessages;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import javax.faces.component.html.HtmlPanelGroup;
import com.ibm.faces.component.html.HtmlPanelLayout;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlGraphicImageEx;

/**
 * @author hghavami
 * 
 */
public class Thankyou extends PageCodeBase {

	protected HtmlOutputText RightColHTMLSpot2Text;
	protected HtmlOutputText RightColHTMLSpot1Text;
	protected HtmlScriptCollector scriptCollector1;
	protected HtmlForm formMainForm;
	protected HtmlOutputText textMainTableHeader;
	protected HtmlPanelGrid gridThankYouInformation;
	protected HtmlMessages messages1;
	protected HtmlOutputText textThankYouText2;
	protected HtmlOutputText textReadNowTextLink;
	protected HtmlOutputText textThankYouBullet2a;
	protected HtmlOutputText textCustServiceLink;
	protected HtmlOutputText textThankYouText3;
	protected HtmlOutputText textThankYouText4;
	protected HtmlOutputText textThankYouTextL1;
	protected HtmlOutputText textThankYouGift;
	protected HtmlOutputText textGiftLink;
	protected HtmlOutputLinkEx linkExPrintPageLink;
	protected HtmlOutputText textPrintPageLinkText;
	protected HtmlPanelGrid gridAccountInformation;
	protected HtmlPanelGroup groupAccountInfoHeaderGroup;
	protected HtmlPanelLayout layoutOrderInfo1;
	protected HtmlOutputText textOrderInformationHeaderlabel;
	protected HtmlOutputText textPremiumPromotionLabel;
	protected HtmlPanelGroup groupDeliveryInfoGroup;
	protected HtmlPanelLayout layoutOrderInfo2;
	protected HtmlOutputText textOrderInformationDeliveryHeaderlabel;
	protected HtmlPanelGroup groupPaymentInfoGroup;
	protected HtmlPanelLayout layoutOrderInfo3;
	protected HtmlOutputText textOrderInformationPaymentHeaderlabel;
	protected HtmlPanelGroup groupPaymentInfoBillMeGroup;
	protected HtmlPanelLayout layoutOrderInfo4;
	protected HtmlOutputText textOrderInformationPaymentBillMeHeaderlabel;
	protected HtmlOutputText textSubTotalAmountLabelBillMe;
	protected HtmlPanelGroup groupBillingHeaderGroup;
	protected HtmlPanelLayout layoutOrderInfo5;
	protected HtmlOutputText textOrderInformationBillingHeaderlabel;
	protected HtmlOutputText textEZPayInfoText;
	protected HtmlPanelGrid gridRightPanelGrid;
	protected HtmlOutputText textThankYouCommJunc1;
	protected HtmlJspPanel jspPanelEEThankYouTextPanel;
	protected HtmlOutputLinkEx linkExReadNowTextLink;
	protected HtmlOutputLinkEx linkExCustServiceLink;
	protected HtmlOutputText textThankYouBullet2b;
	protected HtmlJspPanel jspPanelThankYouTextPanel;
	protected HtmlJspPanel jspPanelThankYouGiftPanel;
	protected HtmlOutputLinkEx linkExGiftLink;
	protected HtmlGraphicImageEx imageExFillerImage;
	protected HtmlPanelGrid gridPrintButtonGrid;
	protected HtmlJspPanel jspPanelOrderDataPanel;
	protected HtmlOutputText textDateLabel;
	protected HtmlOutputText textDateOfToday;
	protected HtmlOutputText textSubStartDateLabel;
	protected HtmlOutputText textStartDateOfSub;
	protected HtmlOutputText textProductLabel;
	protected HtmlOutputText textProdName;
	protected HtmlOutputText textTermLabel;
	protected HtmlOutputText textSelectedTerm;
	protected HtmlOutputText textUnitPriceLabel;
	protected HtmlOutputText textUnitPrice;
	protected HtmlOutputText textEZPayLabel;
	protected HtmlOutputText textEZPayStatus;
	protected HtmlPanelGrid gridPremiumPromoGrid;
	protected HtmlOutputText textPremiumPromotionDescriptionText;
	protected HtmlPanelGrid gridAccountInformationDelivery;
	protected HtmlOutputText textDeliveryMethodLabel;
	protected HtmlOutputText textDeliveryMethodDes;
	protected HtmlOutputText textDeliveryEmailLabel;
	protected HtmlOutputText textDeliveryEmail;
	protected HtmlOutputText textDeliveryNameLabel;
	protected HtmlOutputText textDeliveryName;
	protected HtmlOutputText textDeliveryCompanyNameLabel;
	protected HtmlOutputText textDeliveryCompanyName;
	protected HtmlOutputText textDeliveryAddr1Label;
	protected HtmlOutputText textDeliveryAddr1;
	protected HtmlOutputText textDeliveryAptSuiteLabel;
	protected HtmlOutputText textDeliveryAptSuite;
	protected HtmlOutputText textDeliveryAddr2Label;
	protected HtmlOutputText textDeliveryAddr2;
	protected HtmlOutputText textDeliveryAddrStateZipLabel;
	protected HtmlOutputText textDeliveryStateZip;
	protected HtmlOutputText textDeliveryPhoneLabel;
	protected HtmlOutputText textDeliveryPhone;
	protected HtmlOutputText textDeliveryWorkPhoneLabel;
	protected HtmlOutputText textDeliveryWorkPhone;
	protected HtmlPanelGrid gridAccountInformationPayment;
	protected HtmlOutputText textSubTotalAmountLabel;
	protected HtmlOutputText textSubTotalChargeAmount;
	protected HtmlOutputText textTaxAmountLabel;
	protected HtmlOutputText textChargeTaxAmount;
	protected HtmlOutputText textChargeAmountLabel;
	protected HtmlOutputText textChargeAmount;
	protected HtmlOutputText textPaymentMethodLabel;
	protected HtmlOutputText textPaymentMethod;
	protected HtmlOutputText textPaymentCardNumberLabel;
	protected HtmlOutputText textPaymentCardNumber;
	protected HtmlPanelGrid gridAccountInformationPaymentBillMe;
	protected HtmlPanelGrid billMeOrderDetails;
	protected HtmlOutputText textSubTotalChargeAmountBillMe;
	protected HtmlOutputText textTaxAmountLabelBillMe;
	protected HtmlOutputText textChargeTaxAmountBillMe;
	protected HtmlOutputText textChargeAmountLabelBillMe;
	protected HtmlOutputText textChargeAmountBillMe;
	protected HtmlOutputText textBeInfoText;
	protected HtmlPanelGrid gridAccountInformationBilling;
	protected HtmlOutputText textBillingEmailLabel;
	protected HtmlOutputText textBillingEmail;
	protected HtmlOutputText textBillingNameLabel;
	protected HtmlOutputText textBillingName;
	protected HtmlOutputText textBillingCompanyLabel;
	protected HtmlOutputText textBillingCompany;
	protected HtmlOutputText textBillingAddr1Label;
	protected HtmlOutputText textBillingAddr1;
	protected HtmlOutputText textBillingAptSuiteLabel;
	protected HtmlOutputText textBillingAptSuite;
	protected HtmlOutputText textBillingAddr2Label;
	protected HtmlOutputText textBillingAddr2;
	protected HtmlOutputText textBillingAddrStateZipLabel;
	protected HtmlOutputText textBillingStateZip;
	protected HtmlOutputText textBillingPhoneLabel;
	protected HtmlOutputText textBillingPhone;
	protected HtmlPanelGrid gridAccountEZPayInformation;
	protected HtmlOutputText textThankYouJS1;
	protected HtmlOutputText textThankYouJS2;
	protected HtmlOutputText textThankYouJS3;
	protected HtmlOutputText textThankYouJS4;
	protected HtmlOutputText textThankYouJS5;
	protected HtmlOutputText textThankYouJS6;
	protected HtmlOutputText textThankYouJS7;
	protected HtmlOutputText textThankYouJS8;
	protected HtmlOutputText textThankYouJS9;
	protected HtmlJspPanel jspPanelRightPanelTop;
	protected HtmlOutputLinkEx linkExReadNowLink;
	protected HtmlGraphicImageEx imageExReadNowButtonImage;
	protected HtmlOutputLinkEx linkExOpenDesktopVersion;
	protected HtmlOutputText text1111;
	protected HtmlOutputText text2;
	protected HtmlOutputLinkEx linkExOpenMobileVersion;
	protected HtmlOutputText text1;
	protected HtmlPanelGrid gridEEProductGrid;
	protected HtmlOutputText textThankYouJS10;
	protected HtmlOutputText textThankYouJS12;
	protected HtmlOutputText textThankYouJS13;
	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlForm getFormMainForm() {
		if (formMainForm == null) {
			formMainForm = (HtmlForm) findComponentInRoot("formMainForm");
		}
		return formMainForm;
	}

	protected HtmlOutputText getTextMainTableHeader() {
		if (textMainTableHeader == null) {
			textMainTableHeader = (HtmlOutputText) findComponentInRoot("textMainTableHeader");
		}
		return textMainTableHeader;
	}

	protected HtmlPanelGrid getGridThankYouInformation() {
		if (gridThankYouInformation == null) {
			gridThankYouInformation = (HtmlPanelGrid) findComponentInRoot("gridThankYouInformation");
		}
		return gridThankYouInformation;
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

	protected HtmlOutputText getTextThankYouText2() {
		if (textThankYouText2 == null) {
			textThankYouText2 = (HtmlOutputText) findComponentInRoot("textThankYouText2");
		}
		return textThankYouText2;
	}

	protected HtmlOutputText getTextReadNowTextLink() {
		if (textReadNowTextLink == null) {
			textReadNowTextLink = (HtmlOutputText) findComponentInRoot("textReadNowTextLink");
		}
		return textReadNowTextLink;
	}

	protected HtmlOutputText getTextThankYouBullet2a() {
		if (textThankYouBullet2a == null) {
			textThankYouBullet2a = (HtmlOutputText) findComponentInRoot("textThankYouBullet2a");
		}
		return textThankYouBullet2a;
	}

	protected HtmlOutputText getTextCustServiceLink() {
		if (textCustServiceLink == null) {
			textCustServiceLink = (HtmlOutputText) findComponentInRoot("textCustServiceLink");
		}
		return textCustServiceLink;
	}

	protected HtmlOutputText getTextThankYouText3() {
		if (textThankYouText3 == null) {
			textThankYouText3 = (HtmlOutputText) findComponentInRoot("textThankYouText3");
		}
		return textThankYouText3;
	}

	protected HtmlOutputText getTextThankYouText4() {
		if (textThankYouText4 == null) {
			textThankYouText4 = (HtmlOutputText) findComponentInRoot("textThankYouText4");
		}
		return textThankYouText4;
	}

	protected HtmlOutputText getTextThankYouTextL1() {
		if (textThankYouTextL1 == null) {
			textThankYouTextL1 = (HtmlOutputText) findComponentInRoot("textThankYouTextL1");
		}
		return textThankYouTextL1;
	}

	protected HtmlOutputText getTextThankYouGift() {
		if (textThankYouGift == null) {
			textThankYouGift = (HtmlOutputText) findComponentInRoot("textThankYouGift");
		}
		return textThankYouGift;
	}

	protected HtmlOutputText getTextGiftLink() {
		if (textGiftLink == null) {
			textGiftLink = (HtmlOutputText) findComponentInRoot("textGiftLink");
		}
		return textGiftLink;
	}

	protected HtmlOutputLinkEx getLinkExPrintPageLink() {
		if (linkExPrintPageLink == null) {
			linkExPrintPageLink = (HtmlOutputLinkEx) findComponentInRoot("linkExPrintPageLink");
		}
		return linkExPrintPageLink;
	}

	protected HtmlOutputText getTextPrintPageLinkText() {
		if (textPrintPageLinkText == null) {
			textPrintPageLinkText = (HtmlOutputText) findComponentInRoot("textPrintPageLinkText");
		}
		return textPrintPageLinkText;
	}

	protected HtmlPanelGrid getGridAccountInformation() {
		if (gridAccountInformation == null) {
			gridAccountInformation = (HtmlPanelGrid) findComponentInRoot("gridAccountInformation");
		}
		return gridAccountInformation;
	}

	protected HtmlPanelGroup getGroupAccountInfoHeaderGroup() {
		if (groupAccountInfoHeaderGroup == null) {
			groupAccountInfoHeaderGroup = (HtmlPanelGroup) findComponentInRoot("groupAccountInfoHeaderGroup");
		}
		return groupAccountInfoHeaderGroup;
	}

	protected HtmlPanelLayout getLayoutOrderInfo1() {
		if (layoutOrderInfo1 == null) {
			layoutOrderInfo1 = (HtmlPanelLayout) findComponentInRoot("layoutOrderInfo1");
		}
		return layoutOrderInfo1;
	}

	protected HtmlOutputText getTextOrderInformationHeaderlabel() {
		if (textOrderInformationHeaderlabel == null) {
			textOrderInformationHeaderlabel = (HtmlOutputText) findComponentInRoot("textOrderInformationHeaderlabel");
		}
		return textOrderInformationHeaderlabel;
	}

	protected HtmlOutputText getTextPremiumPromotionLabel() {
		if (textPremiumPromotionLabel == null) {
			textPremiumPromotionLabel = (HtmlOutputText) findComponentInRoot("textPremiumPromotionLabel");
		}
		return textPremiumPromotionLabel;
	}

	protected HtmlPanelGroup getGroupDeliveryInfoGroup() {
		if (groupDeliveryInfoGroup == null) {
			groupDeliveryInfoGroup = (HtmlPanelGroup) findComponentInRoot("groupDeliveryInfoGroup");
		}
		return groupDeliveryInfoGroup;
	}

	protected HtmlPanelLayout getLayoutOrderInfo2() {
		if (layoutOrderInfo2 == null) {
			layoutOrderInfo2 = (HtmlPanelLayout) findComponentInRoot("layoutOrderInfo2");
		}
		return layoutOrderInfo2;
	}

	protected HtmlOutputText getTextOrderInformationDeliveryHeaderlabel() {
		if (textOrderInformationDeliveryHeaderlabel == null) {
			textOrderInformationDeliveryHeaderlabel = (HtmlOutputText) findComponentInRoot("textOrderInformationDeliveryHeaderlabel");
		}
		return textOrderInformationDeliveryHeaderlabel;
	}

	protected HtmlPanelGroup getGroupPaymentInfoGroup() {
		if (groupPaymentInfoGroup == null) {
			groupPaymentInfoGroup = (HtmlPanelGroup) findComponentInRoot("groupPaymentInfoGroup");
		}
		return groupPaymentInfoGroup;
	}

	protected HtmlPanelLayout getLayoutOrderInfo3() {
		if (layoutOrderInfo3 == null) {
			layoutOrderInfo3 = (HtmlPanelLayout) findComponentInRoot("layoutOrderInfo3");
		}
		return layoutOrderInfo3;
	}

	protected HtmlOutputText getTextOrderInformationPaymentHeaderlabel() {
		if (textOrderInformationPaymentHeaderlabel == null) {
			textOrderInformationPaymentHeaderlabel = (HtmlOutputText) findComponentInRoot("textOrderInformationPaymentHeaderlabel");
		}
		return textOrderInformationPaymentHeaderlabel;
	}

	protected HtmlPanelGroup getGroupPaymentInfoBillMeGroup() {
		if (groupPaymentInfoBillMeGroup == null) {
			groupPaymentInfoBillMeGroup = (HtmlPanelGroup) findComponentInRoot("groupPaymentInfoBillMeGroup");
		}
		return groupPaymentInfoBillMeGroup;
	}

	protected HtmlPanelLayout getLayoutOrderInfo4() {
		if (layoutOrderInfo4 == null) {
			layoutOrderInfo4 = (HtmlPanelLayout) findComponentInRoot("layoutOrderInfo4");
		}
		return layoutOrderInfo4;
	}

	protected HtmlOutputText getTextOrderInformationPaymentBillMeHeaderlabel() {
		if (textOrderInformationPaymentBillMeHeaderlabel == null) {
			textOrderInformationPaymentBillMeHeaderlabel = (HtmlOutputText) findComponentInRoot("textOrderInformationPaymentBillMeHeaderlabel");
		}
		return textOrderInformationPaymentBillMeHeaderlabel;
	}

	protected HtmlOutputText getTextSubTotalAmountLabelBillMe() {
		if (textSubTotalAmountLabelBillMe == null) {
			textSubTotalAmountLabelBillMe = (HtmlOutputText) findComponentInRoot("textSubTotalAmountLabelBillMe");
		}
		return textSubTotalAmountLabelBillMe;
	}

	protected HtmlPanelGroup getGroupBillingHeaderGroup() {
		if (groupBillingHeaderGroup == null) {
			groupBillingHeaderGroup = (HtmlPanelGroup) findComponentInRoot("groupBillingHeaderGroup");
		}
		return groupBillingHeaderGroup;
	}

	protected HtmlPanelLayout getLayoutOrderInfo5() {
		if (layoutOrderInfo5 == null) {
			layoutOrderInfo5 = (HtmlPanelLayout) findComponentInRoot("layoutOrderInfo5");
		}
		return layoutOrderInfo5;
	}

	protected HtmlOutputText getTextOrderInformationBillingHeaderlabel() {
		if (textOrderInformationBillingHeaderlabel == null) {
			textOrderInformationBillingHeaderlabel = (HtmlOutputText) findComponentInRoot("textOrderInformationBillingHeaderlabel");
		}
		return textOrderInformationBillingHeaderlabel;
	}

	protected HtmlOutputText getTextEZPayInfoText() {
		if (textEZPayInfoText == null) {
			textEZPayInfoText = (HtmlOutputText) findComponentInRoot("textEZPayInfoText");
		}
		return textEZPayInfoText;
	}

	protected HtmlPanelGrid getGridRightPanelGrid() {
		if (gridRightPanelGrid == null) {
			gridRightPanelGrid = (HtmlPanelGrid) findComponentInRoot("gridRightPanelGrid");
		}
		return gridRightPanelGrid;
	}

	protected HtmlOutputText getTextThankYouCommJunc1() {
		if (textThankYouCommJunc1 == null) {
			textThankYouCommJunc1 = (HtmlOutputText) findComponentInRoot("textThankYouCommJunc1");
		}
		return textThankYouCommJunc1;
	}

	protected HtmlJspPanel getJspPanelEEThankYouTextPanel() {
		if (jspPanelEEThankYouTextPanel == null) {
			jspPanelEEThankYouTextPanel = (HtmlJspPanel) findComponentInRoot("jspPanelEEThankYouTextPanel");
		}
		return jspPanelEEThankYouTextPanel;
	}

	protected HtmlOutputLinkEx getLinkExReadNowTextLink() {
		if (linkExReadNowTextLink == null) {
			linkExReadNowTextLink = (HtmlOutputLinkEx) findComponentInRoot("linkExReadNowTextLink");
		}
		return linkExReadNowTextLink;
	}

	protected HtmlOutputLinkEx getLinkExCustServiceLink() {
		if (linkExCustServiceLink == null) {
			linkExCustServiceLink = (HtmlOutputLinkEx) findComponentInRoot("linkExCustServiceLink");
		}
		return linkExCustServiceLink;
	}

	protected HtmlOutputText getTextThankYouBullet2b() {
		if (textThankYouBullet2b == null) {
			textThankYouBullet2b = (HtmlOutputText) findComponentInRoot("textThankYouBullet2b");
		}
		return textThankYouBullet2b;
	}

	protected HtmlJspPanel getJspPanelThankYouTextPanel() {
		if (jspPanelThankYouTextPanel == null) {
			jspPanelThankYouTextPanel = (HtmlJspPanel) findComponentInRoot("jspPanelThankYouTextPanel");
		}
		return jspPanelThankYouTextPanel;
	}

	protected HtmlJspPanel getJspPanelThankYouGiftPanel() {
		if (jspPanelThankYouGiftPanel == null) {
			jspPanelThankYouGiftPanel = (HtmlJspPanel) findComponentInRoot("jspPanelThankYouGiftPanel");
		}
		return jspPanelThankYouGiftPanel;
	}

	protected HtmlOutputLinkEx getLinkExGiftLink() {
		if (linkExGiftLink == null) {
			linkExGiftLink = (HtmlOutputLinkEx) findComponentInRoot("linkExGiftLink");
		}
		return linkExGiftLink;
	}

	protected HtmlGraphicImageEx getImageExFillerImage() {
		if (imageExFillerImage == null) {
			imageExFillerImage = (HtmlGraphicImageEx) findComponentInRoot("imageExFillerImage");
		}
		return imageExFillerImage;
	}

	protected HtmlPanelGrid getGridPrintButtonGrid() {
		if (gridPrintButtonGrid == null) {
			gridPrintButtonGrid = (HtmlPanelGrid) findComponentInRoot("gridPrintButtonGrid");
		}
		return gridPrintButtonGrid;
	}

	protected HtmlJspPanel getJspPanelOrderDataPanel() {
		if (jspPanelOrderDataPanel == null) {
			jspPanelOrderDataPanel = (HtmlJspPanel) findComponentInRoot("jspPanelOrderDataPanel");
		}
		return jspPanelOrderDataPanel;
	}

	protected HtmlOutputText getTextDateLabel() {
		if (textDateLabel == null) {
			textDateLabel = (HtmlOutputText) findComponentInRoot("textDateLabel");
		}
		return textDateLabel;
	}

	protected HtmlOutputText getTextDateOfToday() {
		if (textDateOfToday == null) {
			textDateOfToday = (HtmlOutputText) findComponentInRoot("textDateOfToday");
		}
		return textDateOfToday;
	}

	protected HtmlOutputText getTextSubStartDateLabel() {
		if (textSubStartDateLabel == null) {
			textSubStartDateLabel = (HtmlOutputText) findComponentInRoot("textSubStartDateLabel");
		}
		return textSubStartDateLabel;
	}

	protected HtmlOutputText getTextStartDateOfSub() {
		if (textStartDateOfSub == null) {
			textStartDateOfSub = (HtmlOutputText) findComponentInRoot("textStartDateOfSub");
		}
		return textStartDateOfSub;
	}

	protected HtmlOutputText getTextProductLabel() {
		if (textProductLabel == null) {
			textProductLabel = (HtmlOutputText) findComponentInRoot("textProductLabel");
		}
		return textProductLabel;
	}

	protected HtmlOutputText getTextProdName() {
		if (textProdName == null) {
			textProdName = (HtmlOutputText) findComponentInRoot("textProdName");
		}
		return textProdName;
	}

	protected HtmlOutputText getTextTermLabel() {
		if (textTermLabel == null) {
			textTermLabel = (HtmlOutputText) findComponentInRoot("textTermLabel");
		}
		return textTermLabel;
	}

	protected HtmlOutputText getTextSelectedTerm() {
		if (textSelectedTerm == null) {
			textSelectedTerm = (HtmlOutputText) findComponentInRoot("textSelectedTerm");
		}
		return textSelectedTerm;
	}

	protected HtmlOutputText getTextUnitPriceLabel() {
		if (textUnitPriceLabel == null) {
			textUnitPriceLabel = (HtmlOutputText) findComponentInRoot("textUnitPriceLabel");
		}
		return textUnitPriceLabel;
	}

	protected HtmlOutputText getTextUnitPrice() {
		if (textUnitPrice == null) {
			textUnitPrice = (HtmlOutputText) findComponentInRoot("textUnitPrice");
		}
		return textUnitPrice;
	}

	protected HtmlOutputText getTextEZPayLabel() {
		if (textEZPayLabel == null) {
			textEZPayLabel = (HtmlOutputText) findComponentInRoot("textEZPayLabel");
		}
		return textEZPayLabel;
	}

	protected HtmlOutputText getTextEZPayStatus() {
		if (textEZPayStatus == null) {
			textEZPayStatus = (HtmlOutputText) findComponentInRoot("textEZPayStatus");
		}
		return textEZPayStatus;
	}

	protected HtmlPanelGrid getGridPremiumPromoGrid() {
		if (gridPremiumPromoGrid == null) {
			gridPremiumPromoGrid = (HtmlPanelGrid) findComponentInRoot("gridPremiumPromoGrid");
		}
		return gridPremiumPromoGrid;
	}

	protected HtmlOutputText getTextPremiumPromotionDescriptionText() {
		if (textPremiumPromotionDescriptionText == null) {
			textPremiumPromotionDescriptionText = (HtmlOutputText) findComponentInRoot("textPremiumPromotionDescriptionText");
		}
		return textPremiumPromotionDescriptionText;
	}

	protected HtmlPanelGrid getGridAccountInformationDelivery() {
		if (gridAccountInformationDelivery == null) {
			gridAccountInformationDelivery = (HtmlPanelGrid) findComponentInRoot("gridAccountInformationDelivery");
		}
		return gridAccountInformationDelivery;
	}

	protected HtmlOutputText getTextDeliveryMethodLabel() {
		if (textDeliveryMethodLabel == null) {
			textDeliveryMethodLabel = (HtmlOutputText) findComponentInRoot("textDeliveryMethodLabel");
		}
		return textDeliveryMethodLabel;
	}

	protected HtmlOutputText getTextDeliveryMethodDes() {
		if (textDeliveryMethodDes == null) {
			textDeliveryMethodDes = (HtmlOutputText) findComponentInRoot("textDeliveryMethodDes");
		}
		return textDeliveryMethodDes;
	}

	protected HtmlOutputText getTextDeliveryEmailLabel() {
		if (textDeliveryEmailLabel == null) {
			textDeliveryEmailLabel = (HtmlOutputText) findComponentInRoot("textDeliveryEmailLabel");
		}
		return textDeliveryEmailLabel;
	}

	protected HtmlOutputText getTextDeliveryEmail() {
		if (textDeliveryEmail == null) {
			textDeliveryEmail = (HtmlOutputText) findComponentInRoot("textDeliveryEmail");
		}
		return textDeliveryEmail;
	}

	protected HtmlOutputText getTextDeliveryNameLabel() {
		if (textDeliveryNameLabel == null) {
			textDeliveryNameLabel = (HtmlOutputText) findComponentInRoot("textDeliveryNameLabel");
		}
		return textDeliveryNameLabel;
	}

	protected HtmlOutputText getTextDeliveryName() {
		if (textDeliveryName == null) {
			textDeliveryName = (HtmlOutputText) findComponentInRoot("textDeliveryName");
		}
		return textDeliveryName;
	}

	protected HtmlOutputText getTextDeliveryCompanyNameLabel() {
		if (textDeliveryCompanyNameLabel == null) {
			textDeliveryCompanyNameLabel = (HtmlOutputText) findComponentInRoot("textDeliveryCompanyNameLabel");
		}
		return textDeliveryCompanyNameLabel;
	}

	protected HtmlOutputText getTextDeliveryCompanyName() {
		if (textDeliveryCompanyName == null) {
			textDeliveryCompanyName = (HtmlOutputText) findComponentInRoot("textDeliveryCompanyName");
		}
		return textDeliveryCompanyName;
	}

	protected HtmlOutputText getTextDeliveryAddr1Label() {
		if (textDeliveryAddr1Label == null) {
			textDeliveryAddr1Label = (HtmlOutputText) findComponentInRoot("textDeliveryAddr1Label");
		}
		return textDeliveryAddr1Label;
	}

	protected HtmlOutputText getTextDeliveryAddr1() {
		if (textDeliveryAddr1 == null) {
			textDeliveryAddr1 = (HtmlOutputText) findComponentInRoot("textDeliveryAddr1");
		}
		return textDeliveryAddr1;
	}

	protected HtmlOutputText getTextDeliveryAptSuiteLabel() {
		if (textDeliveryAptSuiteLabel == null) {
			textDeliveryAptSuiteLabel = (HtmlOutputText) findComponentInRoot("textDeliveryAptSuiteLabel");
		}
		return textDeliveryAptSuiteLabel;
	}

	protected HtmlOutputText getTextDeliveryAptSuite() {
		if (textDeliveryAptSuite == null) {
			textDeliveryAptSuite = (HtmlOutputText) findComponentInRoot("textDeliveryAptSuite");
		}
		return textDeliveryAptSuite;
	}

	protected HtmlOutputText getTextDeliveryAddr2Label() {
		if (textDeliveryAddr2Label == null) {
			textDeliveryAddr2Label = (HtmlOutputText) findComponentInRoot("textDeliveryAddr2Label");
		}
		return textDeliveryAddr2Label;
	}

	protected HtmlOutputText getTextDeliveryAddr2() {
		if (textDeliveryAddr2 == null) {
			textDeliveryAddr2 = (HtmlOutputText) findComponentInRoot("textDeliveryAddr2");
		}
		return textDeliveryAddr2;
	}

	protected HtmlOutputText getTextDeliveryAddrStateZipLabel() {
		if (textDeliveryAddrStateZipLabel == null) {
			textDeliveryAddrStateZipLabel = (HtmlOutputText) findComponentInRoot("textDeliveryAddrStateZipLabel");
		}
		return textDeliveryAddrStateZipLabel;
	}

	protected HtmlOutputText getTextDeliveryStateZip() {
		if (textDeliveryStateZip == null) {
			textDeliveryStateZip = (HtmlOutputText) findComponentInRoot("textDeliveryStateZip");
		}
		return textDeliveryStateZip;
	}

	protected HtmlOutputText getTextDeliveryPhoneLabel() {
		if (textDeliveryPhoneLabel == null) {
			textDeliveryPhoneLabel = (HtmlOutputText) findComponentInRoot("textDeliveryPhoneLabel");
		}
		return textDeliveryPhoneLabel;
	}

	protected HtmlOutputText getTextDeliveryPhone() {
		if (textDeliveryPhone == null) {
			textDeliveryPhone = (HtmlOutputText) findComponentInRoot("textDeliveryPhone");
		}
		return textDeliveryPhone;
	}

	protected HtmlOutputText getTextDeliveryWorkPhoneLabel() {
		if (textDeliveryWorkPhoneLabel == null) {
			textDeliveryWorkPhoneLabel = (HtmlOutputText) findComponentInRoot("textDeliveryWorkPhoneLabel");
		}
		return textDeliveryWorkPhoneLabel;
	}

	protected HtmlOutputText getTextDeliveryWorkPhone() {
		if (textDeliveryWorkPhone == null) {
			textDeliveryWorkPhone = (HtmlOutputText) findComponentInRoot("textDeliveryWorkPhone");
		}
		return textDeliveryWorkPhone;
	}

	protected HtmlPanelGrid getGridAccountInformationPayment() {
		if (gridAccountInformationPayment == null) {
			gridAccountInformationPayment = (HtmlPanelGrid) findComponentInRoot("gridAccountInformationPayment");
		}
		return gridAccountInformationPayment;
	}

	protected HtmlOutputText getTextSubTotalAmountLabel() {
		if (textSubTotalAmountLabel == null) {
			textSubTotalAmountLabel = (HtmlOutputText) findComponentInRoot("textSubTotalAmountLabel");
		}
		return textSubTotalAmountLabel;
	}

	protected HtmlOutputText getTextSubTotalChargeAmount() {
		if (textSubTotalChargeAmount == null) {
			textSubTotalChargeAmount = (HtmlOutputText) findComponentInRoot("textSubTotalChargeAmount");
		}
		return textSubTotalChargeAmount;
	}

	protected HtmlOutputText getTextTaxAmountLabel() {
		if (textTaxAmountLabel == null) {
			textTaxAmountLabel = (HtmlOutputText) findComponentInRoot("textTaxAmountLabel");
		}
		return textTaxAmountLabel;
	}

	protected HtmlOutputText getTextChargeTaxAmount() {
		if (textChargeTaxAmount == null) {
			textChargeTaxAmount = (HtmlOutputText) findComponentInRoot("textChargeTaxAmount");
		}
		return textChargeTaxAmount;
	}

	protected HtmlOutputText getTextChargeAmountLabel() {
		if (textChargeAmountLabel == null) {
			textChargeAmountLabel = (HtmlOutputText) findComponentInRoot("textChargeAmountLabel");
		}
		return textChargeAmountLabel;
	}

	protected HtmlOutputText getTextChargeAmount() {
		if (textChargeAmount == null) {
			textChargeAmount = (HtmlOutputText) findComponentInRoot("textChargeAmount");
		}
		return textChargeAmount;
	}

	protected HtmlOutputText getTextPaymentMethodLabel() {
		if (textPaymentMethodLabel == null) {
			textPaymentMethodLabel = (HtmlOutputText) findComponentInRoot("textPaymentMethodLabel");
		}
		return textPaymentMethodLabel;
	}

	protected HtmlOutputText getTextPaymentMethod() {
		if (textPaymentMethod == null) {
			textPaymentMethod = (HtmlOutputText) findComponentInRoot("textPaymentMethod");
		}
		return textPaymentMethod;
	}

	protected HtmlOutputText getTextPaymentCardNumberLabel() {
		if (textPaymentCardNumberLabel == null) {
			textPaymentCardNumberLabel = (HtmlOutputText) findComponentInRoot("textPaymentCardNumberLabel");
		}
		return textPaymentCardNumberLabel;
	}

	protected HtmlOutputText getTextPaymentCardNumber() {
		if (textPaymentCardNumber == null) {
			textPaymentCardNumber = (HtmlOutputText) findComponentInRoot("textPaymentCardNumber");
		}
		return textPaymentCardNumber;
	}

	protected HtmlPanelGrid getGridAccountInformationPaymentBillMe() {
		if (gridAccountInformationPaymentBillMe == null) {
			gridAccountInformationPaymentBillMe = (HtmlPanelGrid) findComponentInRoot("gridAccountInformationPaymentBillMe");
		}
		return gridAccountInformationPaymentBillMe;
	}

	protected HtmlPanelGrid getBillMeOrderDetails() {
		if (billMeOrderDetails == null) {
			billMeOrderDetails = (HtmlPanelGrid) findComponentInRoot("billMeOrderDetails");
		}
		return billMeOrderDetails;
	}

	protected HtmlOutputText getTextSubTotalChargeAmountBillMe() {
		if (textSubTotalChargeAmountBillMe == null) {
			textSubTotalChargeAmountBillMe = (HtmlOutputText) findComponentInRoot("textSubTotalChargeAmountBillMe");
		}
		return textSubTotalChargeAmountBillMe;
	}

	protected HtmlOutputText getTextTaxAmountLabelBillMe() {
		if (textTaxAmountLabelBillMe == null) {
			textTaxAmountLabelBillMe = (HtmlOutputText) findComponentInRoot("textTaxAmountLabelBillMe");
		}
		return textTaxAmountLabelBillMe;
	}

	protected HtmlOutputText getTextChargeTaxAmountBillMe() {
		if (textChargeTaxAmountBillMe == null) {
			textChargeTaxAmountBillMe = (HtmlOutputText) findComponentInRoot("textChargeTaxAmountBillMe");
		}
		return textChargeTaxAmountBillMe;
	}

	protected HtmlOutputText getTextChargeAmountLabelBillMe() {
		if (textChargeAmountLabelBillMe == null) {
			textChargeAmountLabelBillMe = (HtmlOutputText) findComponentInRoot("textChargeAmountLabelBillMe");
		}
		return textChargeAmountLabelBillMe;
	}

	protected HtmlOutputText getTextChargeAmountBillMe() {
		if (textChargeAmountBillMe == null) {
			textChargeAmountBillMe = (HtmlOutputText) findComponentInRoot("textChargeAmountBillMe");
		}
		return textChargeAmountBillMe;
	}

	protected HtmlOutputText getTextBeInfoText() {
		if (textBeInfoText == null) {
			textBeInfoText = (HtmlOutputText) findComponentInRoot("textBeInfoText");
		}
		return textBeInfoText;
	}

	protected HtmlPanelGrid getGridAccountInformationBilling() {
		if (gridAccountInformationBilling == null) {
			gridAccountInformationBilling = (HtmlPanelGrid) findComponentInRoot("gridAccountInformationBilling");
		}
		return gridAccountInformationBilling;
	}

	protected HtmlOutputText getTextBillingEmailLabel() {
		if (textBillingEmailLabel == null) {
			textBillingEmailLabel = (HtmlOutputText) findComponentInRoot("textBillingEmailLabel");
		}
		return textBillingEmailLabel;
	}

	protected HtmlOutputText getTextBillingEmail() {
		if (textBillingEmail == null) {
			textBillingEmail = (HtmlOutputText) findComponentInRoot("textBillingEmail");
		}
		return textBillingEmail;
	}

	protected HtmlOutputText getTextBillingNameLabel() {
		if (textBillingNameLabel == null) {
			textBillingNameLabel = (HtmlOutputText) findComponentInRoot("textBillingNameLabel");
		}
		return textBillingNameLabel;
	}

	protected HtmlOutputText getTextBillingName() {
		if (textBillingName == null) {
			textBillingName = (HtmlOutputText) findComponentInRoot("textBillingName");
		}
		return textBillingName;
	}

	protected HtmlOutputText getTextBillingCompanyLabel() {
		if (textBillingCompanyLabel == null) {
			textBillingCompanyLabel = (HtmlOutputText) findComponentInRoot("textBillingCompanyLabel");
		}
		return textBillingCompanyLabel;
	}

	protected HtmlOutputText getTextBillingCompany() {
		if (textBillingCompany == null) {
			textBillingCompany = (HtmlOutputText) findComponentInRoot("textBillingCompany");
		}
		return textBillingCompany;
	}

	protected HtmlOutputText getTextBillingAddr1Label() {
		if (textBillingAddr1Label == null) {
			textBillingAddr1Label = (HtmlOutputText) findComponentInRoot("textBillingAddr1Label");
		}
		return textBillingAddr1Label;
	}

	protected HtmlOutputText getTextBillingAddr1() {
		if (textBillingAddr1 == null) {
			textBillingAddr1 = (HtmlOutputText) findComponentInRoot("textBillingAddr1");
		}
		return textBillingAddr1;
	}

	protected HtmlOutputText getTextBillingAptSuiteLabel() {
		if (textBillingAptSuiteLabel == null) {
			textBillingAptSuiteLabel = (HtmlOutputText) findComponentInRoot("textBillingAptSuiteLabel");
		}
		return textBillingAptSuiteLabel;
	}

	protected HtmlOutputText getTextBillingAptSuite() {
		if (textBillingAptSuite == null) {
			textBillingAptSuite = (HtmlOutputText) findComponentInRoot("textBillingAptSuite");
		}
		return textBillingAptSuite;
	}

	protected HtmlOutputText getTextBillingAddr2Label() {
		if (textBillingAddr2Label == null) {
			textBillingAddr2Label = (HtmlOutputText) findComponentInRoot("textBillingAddr2Label");
		}
		return textBillingAddr2Label;
	}

	protected HtmlOutputText getTextBillingAddr2() {
		if (textBillingAddr2 == null) {
			textBillingAddr2 = (HtmlOutputText) findComponentInRoot("textBillingAddr2");
		}
		return textBillingAddr2;
	}

	protected HtmlOutputText getTextBillingAddrStateZipLabel() {
		if (textBillingAddrStateZipLabel == null) {
			textBillingAddrStateZipLabel = (HtmlOutputText) findComponentInRoot("textBillingAddrStateZipLabel");
		}
		return textBillingAddrStateZipLabel;
	}

	protected HtmlOutputText getTextBillingStateZip() {
		if (textBillingStateZip == null) {
			textBillingStateZip = (HtmlOutputText) findComponentInRoot("textBillingStateZip");
		}
		return textBillingStateZip;
	}

	protected HtmlOutputText getTextBillingPhoneLabel() {
		if (textBillingPhoneLabel == null) {
			textBillingPhoneLabel = (HtmlOutputText) findComponentInRoot("textBillingPhoneLabel");
		}
		return textBillingPhoneLabel;
	}

	protected HtmlOutputText getTextBillingPhone() {
		if (textBillingPhone == null) {
			textBillingPhone = (HtmlOutputText) findComponentInRoot("textBillingPhone");
		}
		return textBillingPhone;
	}

	protected HtmlPanelGrid getGridAccountEZPayInformation() {
		if (gridAccountEZPayInformation == null) {
			gridAccountEZPayInformation = (HtmlPanelGrid) findComponentInRoot("gridAccountEZPayInformation");
		}
		return gridAccountEZPayInformation;
	}

	protected HtmlOutputText getTextThankYouJS1() {
		if (textThankYouJS1 == null) {
			textThankYouJS1 = (HtmlOutputText) findComponentInRoot("textThankYouJS1");
		}
		return textThankYouJS1;
	}

	protected HtmlOutputText getTextThankYouJS2() {
		if (textThankYouJS2 == null) {
			textThankYouJS2 = (HtmlOutputText) findComponentInRoot("textThankYouJS2");
		}
		return textThankYouJS2;
	}

	protected HtmlOutputText getTextThankYouJS3() {
		if (textThankYouJS3 == null) {
			textThankYouJS3 = (HtmlOutputText) findComponentInRoot("textThankYouJS3");
		}
		return textThankYouJS3;
	}

	protected HtmlOutputText getTextThankYouJS4() {
		if (textThankYouJS4 == null) {
			textThankYouJS4 = (HtmlOutputText) findComponentInRoot("textThankYouJS4");
		}
		return textThankYouJS4;
	}

	protected HtmlOutputText getTextThankYouJS5() {
		if (textThankYouJS5 == null) {
			textThankYouJS5 = (HtmlOutputText) findComponentInRoot("textThankYouJS5");
		}
		return textThankYouJS5;
	}

	protected HtmlOutputText getTextThankYouJS6() {
		if (textThankYouJS6 == null) {
			textThankYouJS6 = (HtmlOutputText) findComponentInRoot("textThankYouJS6");
		}
		return textThankYouJS6;
	}

	protected HtmlOutputText getTextThankYouJS7() {
		if (textThankYouJS7 == null) {
			textThankYouJS7 = (HtmlOutputText) findComponentInRoot("textThankYouJS7");
		}
		return textThankYouJS7;
	}

	protected HtmlOutputText getTextThankYouJS8() {
		if (textThankYouJS8 == null) {
			textThankYouJS8 = (HtmlOutputText) findComponentInRoot("textThankYouJS8");
		}
		return textThankYouJS8;
	}

	protected HtmlOutputText getTextThankYouJS9() {
		if (textThankYouJS9 == null) {
			textThankYouJS9 = (HtmlOutputText) findComponentInRoot("textThankYouJS9");
		}
		return textThankYouJS9;
	}

	protected HtmlJspPanel getJspPanelRightPanelTop() {
		if (jspPanelRightPanelTop == null) {
			jspPanelRightPanelTop = (HtmlJspPanel) findComponentInRoot("jspPanelRightPanelTop");
		}
		return jspPanelRightPanelTop;
	}

	protected HtmlOutputLinkEx getLinkExReadNowLink() {
		if (linkExReadNowLink == null) {
			linkExReadNowLink = (HtmlOutputLinkEx) findComponentInRoot("linkExReadNowLink");
		}
		return linkExReadNowLink;
	}

	protected HtmlGraphicImageEx getImageExReadNowButtonImage() {
		if (imageExReadNowButtonImage == null) {
			imageExReadNowButtonImage = (HtmlGraphicImageEx) findComponentInRoot("imageExReadNowButtonImage");
		}
		return imageExReadNowButtonImage;
	}

	protected HtmlOutputLinkEx getLinkExOpenDesktopVersion() {
		if (linkExOpenDesktopVersion == null) {
			linkExOpenDesktopVersion = (HtmlOutputLinkEx) findComponentInRoot("linkExOpenDesktopVersion");
		}
		return linkExOpenDesktopVersion;
	}

	protected HtmlOutputText getText1111() {
		if (text1111 == null) {
			text1111 = (HtmlOutputText) findComponentInRoot("text1111");
		}
		return text1111;
	}

	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected HtmlOutputLinkEx getLinkExOpenMobileVersion() {
		if (linkExOpenMobileVersion == null) {
			linkExOpenMobileVersion = (HtmlOutputLinkEx) findComponentInRoot("linkExOpenMobileVersion");
		}
		return linkExOpenMobileVersion;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlPanelGrid getGridEEProductGrid() {
		if (gridEEProductGrid == null) {
			gridEEProductGrid = (HtmlPanelGrid) findComponentInRoot("gridEEProductGrid");
		}
		return gridEEProductGrid;
	}

	protected HtmlOutputText getTextThankYouJS10() {
		if (textThankYouJS10 == null) {
			textThankYouJS10 = (HtmlOutputText) findComponentInRoot("textThankYouJS10");
		}
		return textThankYouJS10;
	}

	protected HtmlOutputText getTextThankYouJS12() {
		if (textThankYouJS12 == null) {
			textThankYouJS12 = (HtmlOutputText) findComponentInRoot("textThankYouJS12");
		}
		return textThankYouJS12;
	}

	protected HtmlOutputText getTextThankYouJS13() {
		if (textThankYouJS13 == null) {
			textThankYouJS13 = (HtmlOutputText) findComponentInRoot("textThankYouJS13");
		}
		return textThankYouJS13;
	}

}