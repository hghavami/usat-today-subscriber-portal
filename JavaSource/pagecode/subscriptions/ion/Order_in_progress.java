/**
 * 
 */
package pagecode.subscriptions.ion;

import pagecode.PageCodeBase;
import com.ibm.faces.component.html.HtmlScriptCollector;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlMessages;
import com.ibm.faces.component.html.HtmlProgressBar;
import com.ibm.faces.component.html.HtmlCommandExButton;

/**
 * @author hghavami
 *
 */
public class Order_in_progress extends PageCodeBase {

	protected HtmlScriptCollector scriptCollectorOrderChecker1;
	protected HtmlForm formCheckStatus;
	protected HtmlMessages messages1;
	protected HtmlProgressBar bar1;
	protected HtmlCommandExButton buttonUpdateStatus;

	protected HtmlScriptCollector getScriptCollectorOrderChecker1() {
		if (scriptCollectorOrderChecker1 == null) {
			scriptCollectorOrderChecker1 = (HtmlScriptCollector) findComponentInRoot("scriptCollectorOrderChecker1");
		}
		return scriptCollectorOrderChecker1;
	}

	protected HtmlForm getFormCheckStatus() {
		if (formCheckStatus == null) {
			formCheckStatus = (HtmlForm) findComponentInRoot("formCheckStatus");
		}
		return formCheckStatus;
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

	protected HtmlProgressBar getBar1() {
		if (bar1 == null) {
			bar1 = (HtmlProgressBar) findComponentInRoot("bar1");
		}
		return bar1;
	}

	protected HtmlCommandExButton getButtonUpdateStatus() {
		if (buttonUpdateStatus == null) {
			buttonUpdateStatus = (HtmlCommandExButton) findComponentInRoot("buttonUpdateStatus");
		}
		return buttonUpdateStatus;
	}

}