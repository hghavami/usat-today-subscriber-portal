/**
 * 
 */
package pagecode.pitt;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.joda.time.DateTime;
import org.joda.time.Days;

import pagecode.PageCodeBase;

import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlFormItem;
import com.ibm.faces.component.html.HtmlGraphicImageEx;
import com.ibm.faces.component.html.HtmlInputHelperSetFocus;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.component.html.HtmlPanelLayout;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.UsatException;
import com.usatoday.business.interfaces.customer.TrialCustomerIntf;
import com.usatoday.business.interfaces.customer.TrialInstanceIntf;
import com.usatoday.business.interfaces.partners.TrialPartnerIntf;
import com.usatoday.businessObjects.customer.TrialCustomerBO;
import com.usatoday.businessObjects.partners.TrialPartnerBO;
import com.usatoday.esub.trials.handlers.NewTrialOrderHandler;
import com.usatoday.esub.trials.handlers.PartnerHandler;
import com.usatoday.esub.trials.handlers.TrialCustomerHandler;

/**
 * @author aeast
 * 
 */
public class Pitt extends PageCodeBase {

	protected HtmlScriptCollector scriptCollectorTopNavCollector;
	protected HtmlJspPanel jspPanelNavBottomEditionTrialLogin;
	protected HtmlJspPanel jspPanelLinkableLogoPanel;
	protected HtmlPanelGrid gridLogoGridNav;
	protected HtmlGraphicImageEx imageExPartnerLogoNoLink;
	protected HtmlScriptCollector scriptCollectorSampleForm;
	protected HtmlForm formSampleForm;
	protected HtmlPanelGrid gridMainBodyGrid;
	protected HtmlPanelGrid gridPartnerNotValid;
	protected HtmlOutputText textInformation1;
	protected HtmlPanelGroup groupMainBodyPanel;
	protected HtmlPanelGrid gridPartnerOfferDescGridv2;
	protected HtmlOutputText textOfferDescription;
	protected HtmlPanelFormBox formBoxPartnerFormBox;
	protected HtmlFormItem formItemPartnerItem1;
	protected HtmlInputText textPartnerName;
	protected HtmlPanelFormBox formBoxDeliveryInformation;
	protected HtmlFormItem formItemDeliveryFirstName;
	protected HtmlInputText textDeliveryFirstName;
	protected HtmlInputText textEmailAddressRecipient;
	protected HtmlInputText textEmailAddressConfirmRecipient;
	protected HtmlPanelGrid gridInnerDisclaimerGrid;
	protected HtmlOutputText textDisclaimerText;
	protected HtmlPanelGrid gridRightSideGrid;
	protected HtmlPanelGrid gridRightPanel1;
	protected HtmlOutputLinkEx linkExOpenReaderSample;
	protected HtmlOutputText textSampleText1Text;
	protected HtmlOutputLinkEx linkExVideoLink_B;
	protected HtmlGraphicImageEx imageExVideoSweepsGraphic_B;
	protected HtmlMessages messagesAllMesssages;
	protected HtmlJspPanel jspPanelFormPanel;
	protected HtmlOutputText textInformation2;
	protected HtmlPanelLayout layoutPageLayout;
	protected HtmlPanelGrid gridPartnerGrid;
	protected HtmlPanelGrid gridDeliveryInformation;
	protected HtmlInputText textDeliveryLastName;
	protected HtmlFormItem formItemEmailAddress;
	protected HtmlFormItem formItemEmailAddressConfirm;
	protected HtmlPanelGrid panelGridFormSubmissionGrid;
	protected HtmlOutputText textCOPPAText;
	protected HtmlCommandExButton buttonPlaceOrder;
	protected HtmlPanelGrid gridPartnerDisclaimerText;
	protected HtmlJspPanel jspPanelTopSampleTextPanel;
	protected HtmlPanelGrid gridVideoGrid;
	protected HtmlOutputText textEEVideoText;
	protected TrialCustomerHandler trialCustomerHandler;
	protected NewTrialOrderHandler newSampleOrderHandler;
	protected PartnerHandler trialPartnerHandler;
	protected HtmlInputHidden partnerID;
	protected HtmlInputHelperSetFocus setFocus1;

	protected HtmlScriptCollector getScriptCollectorTopNavCollector() {
		if (scriptCollectorTopNavCollector == null) {
			scriptCollectorTopNavCollector = (HtmlScriptCollector) findComponentInRoot("scriptCollectorTopNavCollector");
		}
		return scriptCollectorTopNavCollector;
	}

	protected HtmlJspPanel getJspPanelNavBottomEditionTrialLogin() {
		if (jspPanelNavBottomEditionTrialLogin == null) {
			jspPanelNavBottomEditionTrialLogin = (HtmlJspPanel) findComponentInRoot("jspPanelNavBottomEditionTrialLogin");
		}
		return jspPanelNavBottomEditionTrialLogin;
	}

	protected HtmlJspPanel getJspPanelLinkableLogoPanel() {
		if (jspPanelLinkableLogoPanel == null) {
			jspPanelLinkableLogoPanel = (HtmlJspPanel) findComponentInRoot("jspPanelLinkableLogoPanel");
		}
		return jspPanelLinkableLogoPanel;
	}

	protected HtmlPanelGrid getGridLogoGridNav() {
		if (gridLogoGridNav == null) {
			gridLogoGridNav = (HtmlPanelGrid) findComponentInRoot("gridLogoGridNav");
		}
		return gridLogoGridNav;
	}

	protected HtmlGraphicImageEx getImageExPartnerLogoNoLink() {
		if (imageExPartnerLogoNoLink == null) {
			imageExPartnerLogoNoLink = (HtmlGraphicImageEx) findComponentInRoot("imageExPartnerLogoNoLink");
		}
		return imageExPartnerLogoNoLink;
	}

	protected HtmlScriptCollector getScriptCollectorSampleForm() {
		if (scriptCollectorSampleForm == null) {
			scriptCollectorSampleForm = (HtmlScriptCollector) findComponentInRoot("scriptCollectorSampleForm");
		}
		return scriptCollectorSampleForm;
	}

	protected HtmlForm getFormSampleForm() {
		if (formSampleForm == null) {
			formSampleForm = (HtmlForm) findComponentInRoot("formSampleForm");
		}
		return formSampleForm;
	}

	protected HtmlPanelGrid getGridMainBodyGrid() {
		if (gridMainBodyGrid == null) {
			gridMainBodyGrid = (HtmlPanelGrid) findComponentInRoot("gridMainBodyGrid");
		}
		return gridMainBodyGrid;
	}

	protected HtmlPanelGrid getGridPartnerNotValid() {
		if (gridPartnerNotValid == null) {
			gridPartnerNotValid = (HtmlPanelGrid) findComponentInRoot("gridPartnerNotValid");
		}
		return gridPartnerNotValid;
	}

	protected HtmlOutputText getTextInformation1() {
		if (textInformation1 == null) {
			textInformation1 = (HtmlOutputText) findComponentInRoot("textInformation1");
		}
		return textInformation1;
	}

	protected HtmlPanelGroup getGroupMainBodyPanel() {
		if (groupMainBodyPanel == null) {
			groupMainBodyPanel = (HtmlPanelGroup) findComponentInRoot("groupMainBodyPanel");
		}
		return groupMainBodyPanel;
	}

	protected HtmlPanelGrid getGridPartnerOfferDescGridv2() {
		if (gridPartnerOfferDescGridv2 == null) {
			gridPartnerOfferDescGridv2 = (HtmlPanelGrid) findComponentInRoot("gridPartnerOfferDescGridv2");
		}
		return gridPartnerOfferDescGridv2;
	}

	protected HtmlOutputText getTextOfferDescription() {
		if (textOfferDescription == null) {
			textOfferDescription = (HtmlOutputText) findComponentInRoot("textOfferDescription");
		}
		return textOfferDescription;
	}

	protected HtmlPanelFormBox getFormBoxPartnerFormBox() {
		if (formBoxPartnerFormBox == null) {
			formBoxPartnerFormBox = (HtmlPanelFormBox) findComponentInRoot("formBoxPartnerFormBox");
		}
		return formBoxPartnerFormBox;
	}

	protected HtmlFormItem getFormItemPartnerItem1() {
		if (formItemPartnerItem1 == null) {
			formItemPartnerItem1 = (HtmlFormItem) findComponentInRoot("formItemPartnerItem1");
		}
		return formItemPartnerItem1;
	}

	protected HtmlInputText getTextPartnerName() {
		if (textPartnerName == null) {
			textPartnerName = (HtmlInputText) findComponentInRoot("textPartnerName");
		}
		return textPartnerName;
	}

	protected HtmlPanelFormBox getFormBoxDeliveryInformation() {
		if (formBoxDeliveryInformation == null) {
			formBoxDeliveryInformation = (HtmlPanelFormBox) findComponentInRoot("formBoxDeliveryInformation");
		}
		return formBoxDeliveryInformation;
	}

	protected HtmlFormItem getFormItemDeliveryFirstName() {
		if (formItemDeliveryFirstName == null) {
			formItemDeliveryFirstName = (HtmlFormItem) findComponentInRoot("formItemDeliveryFirstName");
		}
		return formItemDeliveryFirstName;
	}

	protected HtmlInputText getTextDeliveryFirstName() {
		if (textDeliveryFirstName == null) {
			textDeliveryFirstName = (HtmlInputText) findComponentInRoot("textDeliveryFirstName");
		}
		return textDeliveryFirstName;
	}

	protected HtmlInputText getTextEmailAddressRecipient() {
		if (textEmailAddressRecipient == null) {
			textEmailAddressRecipient = (HtmlInputText) findComponentInRoot("textEmailAddressRecipient");
		}
		return textEmailAddressRecipient;
	}

	protected HtmlInputText getTextEmailAddressConfirmRecipient() {
		if (textEmailAddressConfirmRecipient == null) {
			textEmailAddressConfirmRecipient = (HtmlInputText) findComponentInRoot("textEmailAddressConfirmRecipient");
		}
		return textEmailAddressConfirmRecipient;
	}

	protected HtmlPanelGrid getGridInnerDisclaimerGrid() {
		if (gridInnerDisclaimerGrid == null) {
			gridInnerDisclaimerGrid = (HtmlPanelGrid) findComponentInRoot("gridInnerDisclaimerGrid");
		}
		return gridInnerDisclaimerGrid;
	}

	protected HtmlOutputText getTextDisclaimerText() {
		if (textDisclaimerText == null) {
			textDisclaimerText = (HtmlOutputText) findComponentInRoot("textDisclaimerText");
		}
		return textDisclaimerText;
	}

	protected HtmlPanelGrid getGridRightSideGrid() {
		if (gridRightSideGrid == null) {
			gridRightSideGrid = (HtmlPanelGrid) findComponentInRoot("gridRightSideGrid");
		}
		return gridRightSideGrid;
	}

	protected HtmlPanelGrid getGridRightPanel1() {
		if (gridRightPanel1 == null) {
			gridRightPanel1 = (HtmlPanelGrid) findComponentInRoot("gridRightPanel1");
		}
		return gridRightPanel1;
	}

	protected HtmlOutputLinkEx getLinkExOpenReaderSample() {
		if (linkExOpenReaderSample == null) {
			linkExOpenReaderSample = (HtmlOutputLinkEx) findComponentInRoot("linkExOpenReaderSample");
		}
		return linkExOpenReaderSample;
	}

	protected HtmlOutputText getTextSampleText1Text() {
		if (textSampleText1Text == null) {
			textSampleText1Text = (HtmlOutputText) findComponentInRoot("textSampleText1Text");
		}
		return textSampleText1Text;
	}

	protected HtmlOutputLinkEx getLinkExVideoLink_B() {
		if (linkExVideoLink_B == null) {
			linkExVideoLink_B = (HtmlOutputLinkEx) findComponentInRoot("linkExVideoLink_B");
		}
		return linkExVideoLink_B;
	}

	protected HtmlGraphicImageEx getImageExVideoSweepsGraphic_B() {
		if (imageExVideoSweepsGraphic_B == null) {
			imageExVideoSweepsGraphic_B = (HtmlGraphicImageEx) findComponentInRoot("imageExVideoSweepsGraphic_B");
		}
		return imageExVideoSweepsGraphic_B;
	}

	protected HtmlMessages getMessagesAllMesssages() {
		if (messagesAllMesssages == null) {
			messagesAllMesssages = (HtmlMessages) findComponentInRoot("messagesAllMesssages");
		}
		return messagesAllMesssages;
	}

	protected HtmlJspPanel getJspPanelFormPanel() {
		if (jspPanelFormPanel == null) {
			jspPanelFormPanel = (HtmlJspPanel) findComponentInRoot("jspPanelFormPanel");
		}
		return jspPanelFormPanel;
	}

	protected HtmlOutputText getTextInformation2() {
		if (textInformation2 == null) {
			textInformation2 = (HtmlOutputText) findComponentInRoot("textInformation2");
		}
		return textInformation2;
	}

	protected HtmlPanelLayout getLayoutPageLayout() {
		if (layoutPageLayout == null) {
			layoutPageLayout = (HtmlPanelLayout) findComponentInRoot("layoutPageLayout");
		}
		return layoutPageLayout;
	}

	protected HtmlPanelGrid getGridPartnerGrid() {
		if (gridPartnerGrid == null) {
			gridPartnerGrid = (HtmlPanelGrid) findComponentInRoot("gridPartnerGrid");
		}
		return gridPartnerGrid;
	}

	protected HtmlPanelGrid getGridDeliveryInformation() {
		if (gridDeliveryInformation == null) {
			gridDeliveryInformation = (HtmlPanelGrid) findComponentInRoot("gridDeliveryInformation");
		}
		return gridDeliveryInformation;
	}

	protected HtmlInputText getTextDeliveryLastName() {
		if (textDeliveryLastName == null) {
			textDeliveryLastName = (HtmlInputText) findComponentInRoot("textDeliveryLastName");
		}
		return textDeliveryLastName;
	}

	protected HtmlFormItem getFormItemEmailAddress() {
		if (formItemEmailAddress == null) {
			formItemEmailAddress = (HtmlFormItem) findComponentInRoot("formItemEmailAddress");
		}
		return formItemEmailAddress;
	}

	protected HtmlFormItem getFormItemEmailAddressConfirm() {
		if (formItemEmailAddressConfirm == null) {
			formItemEmailAddressConfirm = (HtmlFormItem) findComponentInRoot("formItemEmailAddressConfirm");
		}
		return formItemEmailAddressConfirm;
	}

	protected HtmlPanelGrid getPanelGridFormSubmissionGrid() {
		if (panelGridFormSubmissionGrid == null) {
			panelGridFormSubmissionGrid = (HtmlPanelGrid) findComponentInRoot("panelGridFormSubmissionGrid");
		}
		return panelGridFormSubmissionGrid;
	}

	protected HtmlOutputText getTextCOPPAText() {
		if (textCOPPAText == null) {
			textCOPPAText = (HtmlOutputText) findComponentInRoot("textCOPPAText");
		}
		return textCOPPAText;
	}

	protected HtmlCommandExButton getButtonPlaceOrder() {
		if (buttonPlaceOrder == null) {
			buttonPlaceOrder = (HtmlCommandExButton) findComponentInRoot("buttonPlaceOrder");
		}
		return buttonPlaceOrder;
	}

	protected HtmlPanelGrid getGridPartnerDisclaimerText() {
		if (gridPartnerDisclaimerText == null) {
			gridPartnerDisclaimerText = (HtmlPanelGrid) findComponentInRoot("gridPartnerDisclaimerText");
		}
		return gridPartnerDisclaimerText;
	}

	protected HtmlJspPanel getJspPanelTopSampleTextPanel() {
		if (jspPanelTopSampleTextPanel == null) {
			jspPanelTopSampleTextPanel = (HtmlJspPanel) findComponentInRoot("jspPanelTopSampleTextPanel");
		}
		return jspPanelTopSampleTextPanel;
	}

	protected HtmlPanelGrid getGridVideoGrid() {
		if (gridVideoGrid == null) {
			gridVideoGrid = (HtmlPanelGrid) findComponentInRoot("gridVideoGrid");
		}
		return gridVideoGrid;
	}

	protected HtmlOutputText getTextEEVideoText() {
		if (textEEVideoText == null) {
			textEEVideoText = (HtmlOutputText) findComponentInRoot("textEEVideoText");
		}
		return textEEVideoText;
	}

	public void onPageLoadBegin(FacesContext facescontext) {
		// Type Java code to handle page load begin event here

		// void <method>(FacesContext facescontext)
		TrialCustomerHandler tch = this.getTrialCustomerHandler();
		if (tch != null && tch.getTrialCustomer() != null) {
			tch.setTrialCustomer(null);
			tch.setEarlySubscribeKeycode(null);
		}

	}

	/**
	 * @managed-bean true
	 */
	protected TrialCustomerHandler getTrialCustomerHandler() {
		if (trialCustomerHandler == null) {
			trialCustomerHandler = (TrialCustomerHandler) getManagedBean("trialCustomerHandler");
		}
		return trialCustomerHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setTrialCustomerHandler(TrialCustomerHandler trialCustomerHandler) {
		this.trialCustomerHandler = trialCustomerHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected NewTrialOrderHandler getNewSampleOrderHandler() {
		if (newSampleOrderHandler == null) {
			newSampleOrderHandler = (NewTrialOrderHandler) getManagedBean("newSampleOrderHandler");
		}
		return newSampleOrderHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setNewSampleOrderHandler(NewTrialOrderHandler newSampleOrderHandler) {
		this.newSampleOrderHandler = newSampleOrderHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected PartnerHandler getTrialPartnerHandler() {
		if (trialPartnerHandler == null) {
			trialPartnerHandler = (PartnerHandler) getManagedBean("trialPartnerHandler");
		}
		return trialPartnerHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setTrialPartnerHandler(PartnerHandler trialPartnerHandler) {
		this.trialPartnerHandler = trialPartnerHandler;
	}

	public String doButtonPlaceOrderAction() {
		// Type Java code that runs when the component is clicked
		FacesContext facesContext = FacesContext.getCurrentInstance();

		TrialInstanceIntf newTrialInstance = null;

		NewTrialOrderHandler trialData = this.getNewSampleOrderHandler();

		TrialCustomerHandler tch = this.getTrialCustomerHandler();

		if (tch != null && tch.getTrialCustomer() != null && tch.getTrialCustomer().getCurrentInstance().getID() >= 0) {
			try {
				if (!tch.getTrialCustomer().getCurrentInstance().getContactInformation().getEmailAddress()
						.equalsIgnoreCase(trialData.getDeliveryEmailAddress().trim())) {
					// reset tch
					tch.setTrialCustomer(null);
				} else {
					throw new Exception("");
				}
			} catch (Exception exp1) {
				// trial already saved just forward to complete page
				FacesMessage facesMsg = new FacesMessage(
						FacesMessage.SEVERITY_INFO,
						"Duplicate Form Submission detected. If this was not a duplicate submission, please close the browser and then try again.",
						null);

				facesContext.addMessage(null, facesMsg);
				return "success";
			}
		}

		// The partner should be set up prior to reaching this page but may not be.
		// Custom landing pages will use a hard coded partner id value.
		TrialPartnerIntf partner = this.getTrialPartnerHandler().getPartner();

		String pID = null;
		if (partner == null) {
			try {
				pID = this.getPartnerID().getSubmittedValue().toString();

				partner = TrialPartnerBO.fetchPartner(pID, true);
			} catch (UsatException ue) {
				// handle this exception
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Trial Program Not Found. pid: " + pID, null);

				facesContext.addMessage(null, facesMsg);
				return "failure";
			}
		}

		// save the customer
		boolean saved = false;

		// validate form fields
		if (!trialData.getIsEmailAndConfirmEmailSame()) {
			FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
					"The email address and confirmation email address do not match.", null);

			facesContext.addMessage(null, facesMsg);
			return "failure";
		}

		try {

			if (!trialData.getIsEmailValid()) {
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "The email address is not valid.", null);

				facesContext.addMessage(null, facesMsg);
				return "failure";
			}

			String emailTemp = trialData.getDeliveryEmailAddress().toLowerCase();
			if (!(emailTemp.endsWith("pitt.edu") || emailTemp.endsWith("@usatoday.com"))) {
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
						"The email address must be a valid University of Pittsburgh email address ending in 'pitt.edu'.", null);

				facesContext.addMessage(null, facesMsg);
				return "failure";
			}
		} catch (Exception e) {
			FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "The email address is not valid.", null);

			facesContext.addMessage(null, facesMsg);
			return "failure";
		}

		try {
			newTrialInstance = trialData.generatedNewTrialCustomerFromFields(partner);

			// override subtype manually
			newTrialInstance.setTrialSubType("college");

			// For Univ of Pitt, decrement trial duration based on sign up date.
			DateTime start = partner.getStartDate();
			DateTime today = new DateTime();

			Days d = Days.daysBetween(start, today);
			int days = d.getDays();

			DateTime endDate = newTrialInstance.getEndDate();
			if (days > 0) {
				endDate = endDate.minusDays(days);
			}

			newTrialInstance.setEndDate(endDate);

			// validate instance.
			boolean isValid = TrialCustomerBO.validateNewTrialInstance(newTrialInstance);

			if (isValid) {
				String cIP = "";
				cIP = ((HttpServletRequest) facesContext.getExternalContext().getRequest()).getRemoteAddr();
				newTrialInstance.setClientIP(cIP);
				newTrialInstance.save();
			}

			saved = true;

		} catch (Exception e) {
			e.printStackTrace();
			FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Failed to save new trial record: "
					+ e.getMessage(), null);

			facesContext.addMessage(null, facesMsg);
			return "failure";
		}
		if (!saved) {
			return "failure";
		} else {
			// set up customer and send confirmation email
			try {
				TrialCustomerIntf tCust = TrialCustomerBO.getTrialCustomerByEmail(newTrialInstance.getContactInformation()
						.getEmailAddress());

				if (!tCust.getCurrentInstance().getPartner().getPartnerID()
						.equalsIgnoreCase(newTrialInstance.getPartner().getPartnerID())) {
					tCust.setCurrentInstance(newTrialInstance.getPartner().getPartnerID());
				}
				tch.setTrialCustomer(tCust);

				// Invoke Method to send confirmation email
				boolean emailSent = newTrialInstance.sendConfirmationEmail();
				if (!emailSent) {
					newTrialInstance.setConfirmationEmailSent("N");
				}

				// if a multiple trial users
				if (tCust.getAllTrialInstances().size() > 1) {
					// update passwords to most recent
					tCust.setPassword(newTrialInstance.getContactInformation().getPassword());
					tCust.save();
				}
			} catch (Exception eee) {
				eee.printStackTrace(); // don't go to failure page because of this.
			}
		}
		return "success";
	}

	protected HtmlInputHidden getPartnerID() {
		if (partnerID == null) {
			partnerID = (HtmlInputHidden) findComponentInRoot("partnerID");
		}
		return partnerID;
	}

	protected HtmlInputHelperSetFocus getSetFocus1() {
		if (setFocus1 == null) {
			setFocus1 = (HtmlInputHelperSetFocus) findComponentInRoot("setFocus1");
		}
		return setFocus1;
	}

}