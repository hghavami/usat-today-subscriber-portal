/**
 * 
 */
package pagecode.idpassword;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;

import pagecode.PageCodeBase;

import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlFormItem;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.business.interfaces.customer.TrialCustomerIntf;
import com.usatoday.businessObjects.customer.TrialCustomerBO;
import com.usatoday.esub.ncs.handlers.LoginPageFormHandler;
import com.usatoday.serviceHandler.trials.TrialCustomerForgotPassword;

/**
 * @author aeast
 * 
 */
public class TrialForgotPassword extends PageCodeBase {

	protected HtmlScriptCollector scriptCollectorForgotPassword;
	protected HtmlForm formForgotPassword;
	protected HtmlPanelGrid gridMainBodyGrid;
	protected HtmlJspPanel jspPanelHeaderPanel;
	protected HtmlFormItem formItemTrialEmailAddress;
	protected HtmlInputText textEmail;
	protected HtmlJspPanel jspPanelFooterPanel;
	protected HtmlMessages messagesLoginErrors;
	protected HtmlPanelFormBox formBoxFogotPasswordFormBox;
	protected LoginPageFormHandler loginFormHandler;
	protected HtmlCommandExButton buttonForgotPassword;
	protected HtmlScriptCollector scriptCollectorTemplateCollectorLeftNav;
	protected HtmlPanelGrid gridNavigationOuterGrid;
	protected HtmlPanelGrid gridLeftNavGroup1Grid;
	protected HtmlOutputLinkEx linkExNavGroup2International;
	protected HtmlOutputText textNavGroup1LinkLabelInternational;
	protected HtmlOutputText textNavGroup1LinkLabelKindle;
	protected HtmlOutputText textNavGroup1LinkLabel1;
	protected HtmlOutputText textNavGroup1LinkLabelDotCom;
	protected HtmlOutputText textNavGroup1LinkLabelFAQ;
	protected HtmlOutputLinkEx linkExNavArea2Link1;
	protected HtmlOutputText textNavArea2Link1;
	protected HtmlOutputText textNavArea2Link2;
	protected HtmlOutputText textNavArea2Link3;
	protected HtmlOutputText textNavArea2Link4;
	protected HtmlOutputLinkEx linkExNavGroup2Kindle;
	protected HtmlOutputLinkEx linkExNavGroup1SubscriberServices;
	protected HtmlOutputLinkEx linkExNavGroup2DotCom;
	protected HtmlOutputLinkEx linkExNavGroupFAQ;
	protected HtmlPanelGrid gridLeftNavGroup2Grid;
	protected HtmlOutputLinkEx linkExNavArea2Link2;
	protected HtmlOutputLinkEx linkExNavArea2Link3;
	protected HtmlOutputLinkEx linkExNavArea2Link4;
	protected HtmlOutputText textBackToLoginlabel;
	protected HtmlOutputLinkEx linkEx1;
	protected HtmlOutputLinkEx linkExBackToLogin;
	protected HtmlOutputText textMainTableHeader;

	protected HtmlScriptCollector getScriptCollectorForgotPassword() {
		if (scriptCollectorForgotPassword == null) {
			scriptCollectorForgotPassword = (HtmlScriptCollector) findComponentInRoot("scriptCollectorForgotPassword");
		}
		return scriptCollectorForgotPassword;
	}

	protected HtmlForm getFormForgotPassword() {
		if (formForgotPassword == null) {
			formForgotPassword = (HtmlForm) findComponentInRoot("formForgotPassword");
		}
		return formForgotPassword;
	}

	protected HtmlPanelGrid getGridMainBodyGrid() {
		if (gridMainBodyGrid == null) {
			gridMainBodyGrid = (HtmlPanelGrid) findComponentInRoot("gridMainBodyGrid");
		}
		return gridMainBodyGrid;
	}

	protected HtmlJspPanel getJspPanelHeaderPanel() {
		if (jspPanelHeaderPanel == null) {
			jspPanelHeaderPanel = (HtmlJspPanel) findComponentInRoot("jspPanelHeaderPanel");
		}
		return jspPanelHeaderPanel;
	}

	protected HtmlFormItem getFormItemTrialEmailAddress() {
		if (formItemTrialEmailAddress == null) {
			formItemTrialEmailAddress = (HtmlFormItem) findComponentInRoot("formItemTrialEmailAddress");
		}
		return formItemTrialEmailAddress;
	}

	protected HtmlInputText getTextEmail() {
		if (textEmail == null) {
			textEmail = (HtmlInputText) findComponentInRoot("textEmail");
		}
		return textEmail;
	}

	protected HtmlJspPanel getJspPanelFooterPanel() {
		if (jspPanelFooterPanel == null) {
			jspPanelFooterPanel = (HtmlJspPanel) findComponentInRoot("jspPanelFooterPanel");
		}
		return jspPanelFooterPanel;
	}

	protected HtmlMessages getMessagesLoginErrors() {
		if (messagesLoginErrors == null) {
			messagesLoginErrors = (HtmlMessages) findComponentInRoot("messagesLoginErrors");
		}
		return messagesLoginErrors;
	}

	protected HtmlPanelFormBox getFormBoxFogotPasswordFormBox() {
		if (formBoxFogotPasswordFormBox == null) {
			formBoxFogotPasswordFormBox = (HtmlPanelFormBox) findComponentInRoot("formBoxFogotPasswordFormBox");
		}
		return formBoxFogotPasswordFormBox;
	}

	/**
	 * @managed-bean true
	 */
	protected LoginPageFormHandler getLoginFormHandler() {
		if (loginFormHandler == null) {
			loginFormHandler = (LoginPageFormHandler) getManagedBean("loginFormHandler");
		}
		return loginFormHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setLoginFormHandler(LoginPageFormHandler loginFormHandler) {
		this.loginFormHandler = loginFormHandler;
	}

	protected HtmlCommandExButton getButtonForgotPassword() {
		if (buttonForgotPassword == null) {
			buttonForgotPassword = (HtmlCommandExButton) findComponentInRoot("buttonForgotPassword");
		}
		return buttonForgotPassword;
	}

	public String doButtonForgotPasswordAction() {
		// Type Java code that runs when the component is clicked

		FacesContext facesContext = FacesContext.getCurrentInstance();

		String email = this.getLoginFormHandler().getEmailAddress();

		if (email != null) {
			try {
				email = email.trim();

				TrialCustomerIntf cust = TrialCustomerBO.getTrialCustomerByEmail(email);

				if (cust != null) {

					TrialCustomerForgotPassword.sendPassword(cust);

					FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
							"You will receive an e-mail with the requested information.", null);

					facesContext.addMessage(null, facesMsg);
				} else {
					FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
							"No email found in our trial subscription system for email address: '" + email + "'", null);

					facesContext.addMessage(null, facesMsg);
				}
			} catch (Exception e) {
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, e.getMessage(), null);

				facesContext.addMessage(null, facesMsg);
			}
		} else {
			FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
					"Please enter your email address and click the Request Password botton. Your password will be emailed to you.",
					null);

			facesContext.addMessage(null, facesMsg);
		}

		return "success";
	}

	protected HtmlScriptCollector getScriptCollectorTemplateCollectorLeftNav() {
		if (scriptCollectorTemplateCollectorLeftNav == null) {
			scriptCollectorTemplateCollectorLeftNav = (HtmlScriptCollector) findComponentInRoot("scriptCollectorTemplateCollectorLeftNav");
		}
		return scriptCollectorTemplateCollectorLeftNav;
	}

	protected HtmlPanelGrid getGridNavigationOuterGrid() {
		if (gridNavigationOuterGrid == null) {
			gridNavigationOuterGrid = (HtmlPanelGrid) findComponentInRoot("gridNavigationOuterGrid");
		}
		return gridNavigationOuterGrid;
	}

	protected HtmlPanelGrid getGridLeftNavGroup1Grid() {
		if (gridLeftNavGroup1Grid == null) {
			gridLeftNavGroup1Grid = (HtmlPanelGrid) findComponentInRoot("gridLeftNavGroup1Grid");
		}
		return gridLeftNavGroup1Grid;
	}

	protected HtmlOutputLinkEx getLinkExNavGroup2International() {
		if (linkExNavGroup2International == null) {
			linkExNavGroup2International = (HtmlOutputLinkEx) findComponentInRoot("linkExNavGroup2International");
		}
		return linkExNavGroup2International;
	}

	protected HtmlOutputText getTextNavGroup1LinkLabelInternational() {
		if (textNavGroup1LinkLabelInternational == null) {
			textNavGroup1LinkLabelInternational = (HtmlOutputText) findComponentInRoot("textNavGroup1LinkLabelInternational");
		}
		return textNavGroup1LinkLabelInternational;
	}

	protected HtmlOutputText getTextNavGroup1LinkLabelKindle() {
		if (textNavGroup1LinkLabelKindle == null) {
			textNavGroup1LinkLabelKindle = (HtmlOutputText) findComponentInRoot("textNavGroup1LinkLabelKindle");
		}
		return textNavGroup1LinkLabelKindle;
	}

	protected HtmlOutputText getTextNavGroup1LinkLabel1() {
		if (textNavGroup1LinkLabel1 == null) {
			textNavGroup1LinkLabel1 = (HtmlOutputText) findComponentInRoot("textNavGroup1LinkLabel1");
		}
		return textNavGroup1LinkLabel1;
	}

	protected HtmlOutputText getTextNavGroup1LinkLabelDotCom() {
		if (textNavGroup1LinkLabelDotCom == null) {
			textNavGroup1LinkLabelDotCom = (HtmlOutputText) findComponentInRoot("textNavGroup1LinkLabelDotCom");
		}
		return textNavGroup1LinkLabelDotCom;
	}

	protected HtmlOutputText getTextNavGroup1LinkLabelFAQ() {
		if (textNavGroup1LinkLabelFAQ == null) {
			textNavGroup1LinkLabelFAQ = (HtmlOutputText) findComponentInRoot("textNavGroup1LinkLabelFAQ");
		}
		return textNavGroup1LinkLabelFAQ;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link1() {
		if (linkExNavArea2Link1 == null) {
			linkExNavArea2Link1 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link1");
		}
		return linkExNavArea2Link1;
	}

	protected HtmlOutputText getTextNavArea2Link1() {
		if (textNavArea2Link1 == null) {
			textNavArea2Link1 = (HtmlOutputText) findComponentInRoot("textNavArea2Link1");
		}
		return textNavArea2Link1;
	}

	protected HtmlOutputText getTextNavArea2Link2() {
		if (textNavArea2Link2 == null) {
			textNavArea2Link2 = (HtmlOutputText) findComponentInRoot("textNavArea2Link2");
		}
		return textNavArea2Link2;
	}

	protected HtmlOutputText getTextNavArea2Link3() {
		if (textNavArea2Link3 == null) {
			textNavArea2Link3 = (HtmlOutputText) findComponentInRoot("textNavArea2Link3");
		}
		return textNavArea2Link3;
	}

	protected HtmlOutputText getTextNavArea2Link4() {
		if (textNavArea2Link4 == null) {
			textNavArea2Link4 = (HtmlOutputText) findComponentInRoot("textNavArea2Link4");
		}
		return textNavArea2Link4;
	}

	protected HtmlOutputLinkEx getLinkExNavGroup2Kindle() {
		if (linkExNavGroup2Kindle == null) {
			linkExNavGroup2Kindle = (HtmlOutputLinkEx) findComponentInRoot("linkExNavGroup2Kindle");
		}
		return linkExNavGroup2Kindle;
	}

	protected HtmlOutputLinkEx getLinkExNavGroup1SubscriberServices() {
		if (linkExNavGroup1SubscriberServices == null) {
			linkExNavGroup1SubscriberServices = (HtmlOutputLinkEx) findComponentInRoot("linkExNavGroup1SubscriberServices");
		}
		return linkExNavGroup1SubscriberServices;
	}

	protected HtmlOutputLinkEx getLinkExNavGroup2DotCom() {
		if (linkExNavGroup2DotCom == null) {
			linkExNavGroup2DotCom = (HtmlOutputLinkEx) findComponentInRoot("linkExNavGroup2DotCom");
		}
		return linkExNavGroup2DotCom;
	}

	protected HtmlOutputLinkEx getLinkExNavGroupFAQ() {
		if (linkExNavGroupFAQ == null) {
			linkExNavGroupFAQ = (HtmlOutputLinkEx) findComponentInRoot("linkExNavGroupFAQ");
		}
		return linkExNavGroupFAQ;
	}

	protected HtmlPanelGrid getGridLeftNavGroup2Grid() {
		if (gridLeftNavGroup2Grid == null) {
			gridLeftNavGroup2Grid = (HtmlPanelGrid) findComponentInRoot("gridLeftNavGroup2Grid");
		}
		return gridLeftNavGroup2Grid;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link2() {
		if (linkExNavArea2Link2 == null) {
			linkExNavArea2Link2 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link2");
		}
		return linkExNavArea2Link2;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link3() {
		if (linkExNavArea2Link3 == null) {
			linkExNavArea2Link3 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link3");
		}
		return linkExNavArea2Link3;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link4() {
		if (linkExNavArea2Link4 == null) {
			linkExNavArea2Link4 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link4");
		}
		return linkExNavArea2Link4;
	}

	protected HtmlOutputText getText1() {
		if (textBackToLoginlabel == null) {
			textBackToLoginlabel = (HtmlOutputText) findComponentInRoot("textBackToLoginlabel");
		}
		return textBackToLoginlabel;
	}

	protected HtmlOutputLinkEx getLinkEx1() {
		if (linkEx1 == null) {
			linkEx1 = (HtmlOutputLinkEx) findComponentInRoot("linkEx1");
		}
		return linkEx1;
	}

	protected HtmlOutputLinkEx getLinkExBackToLogin() {
		if (linkExBackToLogin == null) {
			linkExBackToLogin = (HtmlOutputLinkEx) findComponentInRoot("linkExBackToLogin");
		}
		return linkExBackToLogin;
	}

	protected HtmlOutputText getTextMainTableHeader() {
		if (textMainTableHeader == null) {
			textMainTableHeader = (HtmlOutputText) findComponentInRoot("textMainTableHeader");
		}
		return textMainTableHeader;
	}

}