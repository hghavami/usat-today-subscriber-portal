/**
 * 
 */
package pagecode.err;

import pagecode.PageCodeBase;
import com.ibm.faces.component.html.HtmlScriptCollector;
import javax.faces.component.html.HtmlOutputText;
import com.ibm.faces.component.html.HtmlOutputLinkEx;

/**
 * @author aeast
 * 
 */
public class Jsfsession_expired extends PageCodeBase {

	protected HtmlScriptCollector scriptCollector1;
	protected HtmlOutputText textSessionExpiredMessage;
	protected HtmlOutputText textSessionExpiredMessage2;
	protected HtmlOutputText text1;
	protected HtmlOutputLinkEx linkEx1;

	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlOutputText getTextSessionExpiredMessage() {
		if (textSessionExpiredMessage == null) {
			textSessionExpiredMessage = (HtmlOutputText) findComponentInRoot("textSessionExpiredMessage");
		}
		return textSessionExpiredMessage;
	}

	protected HtmlOutputText getTextSessionExpiredMessage2() {
		if (textSessionExpiredMessage2 == null) {
			textSessionExpiredMessage2 = (HtmlOutputText) findComponentInRoot("textSessionExpiredMessage2");
		}
		return textSessionExpiredMessage2;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlOutputLinkEx getLinkEx1() {
		if (linkEx1 == null) {
			linkEx1 = (HtmlOutputLinkEx) findComponentInRoot("linkEx1");
		}
		return linkEx1;
	}

}