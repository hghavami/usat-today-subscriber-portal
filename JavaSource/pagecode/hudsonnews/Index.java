/**
 * 
 */
package pagecode.hudsonnews;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTime;

import pagecode.PageCodeBase;

import com.gannett.usat.olive.partners.OlivePartner;
import com.gannett.usat.olive.partners.OlivePartnerAccessCode;
import com.gannett.usat.olive.partners.controller.OlivePartnerAccessCodeManager;
import com.gannett.usat.olive.partners.controller.OlivePartnerManager;
import com.gannett.usat.sp.partners.UsatEnewspaperPartnerAccessLog;
import com.gannett.usat.sp.partners.controller.UsatEnewspaperPartnerAccessLogManager;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.business.interfaces.products.ProductIntf;
import com.usatoday.businessObjects.products.ProductBO;
import com.usatoday.businessObjects.products.utils.ElectronicEditionFulfillmentHelper;
import com.usatoday.esub.cookies.AuthenticationCookieProcessor;
import com.usatoday.esub.trials.handlers.HudsonNewsHandler;
import com.usatoday.util.MobileDeviceDetectionUtil;
import com.usatoday.util.constants.UsaTodayConstants;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.component.html.HtmlInputText;
import com.ibm.faces.component.html.HtmlFormItem;
import javax.faces.component.html.HtmlMessages;
import com.ibm.faces.component.html.HtmlGraphicImageEx;

/**
 * @author hghavami
 * 
 */
public class Index extends PageCodeBase {

	protected HtmlScriptCollector scriptCollector1;
	protected HtmlPanelGrid grid1;
	protected HtmlOutputText HeaderText;
	protected HtmlOutputText HeaderDescription;
	protected HtmlPanelFormBox formBox1;
	protected HtmlForm form1;
	protected HudsonNewsHandler hudsonNewsHandler;
	protected HtmlCommandExButton authAccessCodeSubmitButton;
	protected HtmlPanelGroup group1;
	protected HtmlInputText accesscode;
	protected HtmlFormItem accessCodeFormItem;
	protected HtmlMessages messages1;
	protected HtmlOutputText text1;
	protected HtmlGraphicImageEx imageEx1;
	protected HtmlOutputText text3;
	protected HtmlOutputText text4;

	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlPanelGrid getGrid1() {
		if (grid1 == null) {
			grid1 = (HtmlPanelGrid) findComponentInRoot("grid1");
		}
		return grid1;
	}

	protected HtmlPanelFormBox getFormBox1() {
		if (formBox1 == null) {
			formBox1 = (HtmlPanelFormBox) findComponentInRoot("formBox1");
		}
		return formBox1;
	}

	protected HtmlForm getForm1() {
		if (form1 == null) {
			form1 = (HtmlForm) findComponentInRoot("form1");
		}
		return form1;
	}

	/**
	 * @managed-bean true
	 */
	protected HudsonNewsHandler getHudsonNewsHandler() {
		if (hudsonNewsHandler == null) {
			hudsonNewsHandler = (HudsonNewsHandler) getManagedBean("hudsonNewsHandler");
		}
		return hudsonNewsHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setHudsonNewsHandler(HudsonNewsHandler hudsonNewsHandler) {
		this.hudsonNewsHandler = hudsonNewsHandler;
	}

	protected HtmlCommandExButton getAuthAccessCodeSubmitButton() {
		if (authAccessCodeSubmitButton == null) {
			authAccessCodeSubmitButton = (HtmlCommandExButton) findComponentInRoot("authAccessCodeSubmitButton");
		}
		return authAccessCodeSubmitButton;
	}

	public void onPageLoadBegin(FacesContext facescontext) {
	}

	public String doAuthAccessCodeSubmitButtonAction() {
		// This is java code that runs when this action method is invoked

		String status = "success";
		EntityManagerFactory emf = null;
		try {
			HttpServletRequest request = (HttpServletRequest) this.getFacesContext().getExternalContext().getRequest();
			HttpServletResponse response = (HttpServletResponse) this.getFacesContext().getExternalContext().getResponse();
			UsatEnewspaperPartnerAccessLog log = new UsatEnewspaperPartnerAccessLog();
			// If you create an EntityManagerFactory you must call the dispose
			// method when you are done using it.
			emf = Persistence.createEntityManagerFactory("USATDomainModel");
			HudsonNewsHandler hnh = this.getHudsonNewsHandler();
			String accessCode = hnh.getAccesscode();
			String partnerID = UsaTodayConstants.HUDSON_NEWS_OLIVE_PARTNER_ID;
			String propertyID = "NONE";
			String eEditionLink = "";
			boolean accessAllowed = false;
			DateTime now = new DateTime();
			OlivePartnerAccessCodeManager partnerAccessCodeManager = new OlivePartnerAccessCodeManager(emf);
			List<OlivePartnerAccessCode> partnerListAccessCode = partnerAccessCodeManager.getAccessCode(accessCode);
			OlivePartnerAccessCode opac = null;
			if (partnerListAccessCode.size() > 0) {
				opac = partnerListAccessCode.iterator().next();
				propertyID = opac.getPropertyId();
				partnerID = opac.getPartnerId();
				// Check for new access
				if (opac.getStartDate() == null || opac.getEndDate() == null) {
					// Check if partner for the access code still is valid
					OlivePartnerManager partnerManager = new OlivePartnerManager(emf);
					List<OlivePartner> partnerList = partnerManager.getOlivePartnerId(partnerID);
					OlivePartner op = null;
					if (partnerList.size() > 0) {
						op = partnerList.iterator().next();
					}
					if (op == null || !op.isCurrentlyActive()) {
						accessAllowed = false;
					} else {
						// New access
						Date cd = new Date();
						opac.setDateTime(new Timestamp(cd.getTime()));
						opac.setStartDate(new Timestamp(cd.getTime()));
						Timestamp ed = new Timestamp(cd.getTime());
						Calendar cal = Calendar.getInstance();
						cal.setTime(opac.getStartDate());
						cal.add(Calendar.DAY_OF_WEEK, UsaTodayConstants.OLIVE_DAILY_PARTNER_ACCESS_CODE_DURATION);
						ed.setTime(cal.getTime().getTime());
						opac.setEndDate(ed);
						partnerAccessCodeManager.updateOlivePartnerAccessCode(opac);
						// Set cookie for returning access
						AuthenticationCookieProcessor cookieProcessor = new AuthenticationCookieProcessor();
						Cookie c = cookieProcessor.createOneDayAccessRememberMeCookie(accessCode);
						((HttpServletResponse) response).addCookie(c);
						accessAllowed = true;
					}
				} else {
					Timestamp sd = opac.getStartDate();
					Timestamp ed = opac.getEndDate();
					Date cd = new Date();
					Timestamp cts = new Timestamp(cd.getTime());
					if (cts.after(sd) && cts.before(ed)) {
						accessAllowed = true;
					}
				}
			}
			if (!accessAllowed) {
				// log.setClientIp(opac.getclientIP);
				log.setClientIp(request.getRemoteAddr().trim());
				log.setClientUserAgent(request.getHeader("user-agent"));
				log.setPartnerExternalKey("NONE");
				log.setPartnerId(partnerID);
				log.setPropertyId(propertyID);
				log.setTimeStamp(new Timestamp(now.getMillis()));
				log.setResponseCode(UsatEnewspaperPartnerAccessLog.FAILED_INVALID);
				log.setRedirectedTo(UsaTodayConstants.EEDITION_OLIVE_PARTNER_ERROR_PAGE);
				log.setAccessCodeUsed(accessCode);
				UsatEnewspaperPartnerAccessLogManager logManager = new UsatEnewspaperPartnerAccessLogManager(emf);
				logManager.createUsatEnewspaperPartnerAccessLog(log);
				System.out.print("Failed to redirect from Hudson News partner to USA TODAY e-Edition, because cookie was invalid.");
				// response.sendRedirect(UsaTodayConstants.EEDITION_OLIVE_PARTNER_ERROR_PAGE);
				FacesContext.getCurrentInstance().addMessage("formAuth:messagesAuth",
						new FacesMessage("Invalid Access Code.  Please retry."));
				status = "failure";
			} else {
				OlivePartnerManager partnerManager = new OlivePartnerManager(emf);
				List<OlivePartner> partnerList = partnerManager.getOlivePartnerId(partnerID);
				OlivePartner op = null;
				if (partnerList.size() > 0) {
					op = partnerList.iterator().next();
				}
				if (op == null || !op.isCurrentlyActive()) {
					FacesContext.getCurrentInstance().addMessage("formAuth:messagesAuth",
							new FacesMessage("Hudson News is no longer a valid partner.  Please contact us below."));
					status = "failure";
				} else {
					ProductIntf product = ProductBO.fetchProductIncludeInactive("EE");
					boolean useAlternate = MobileDeviceDetectionUtil.isMobileDevice(request);
					eEditionLink = ElectronicEditionFulfillmentHelper.buildOliveFulfillmentURL(product, op.getPartnerId().trim(),
							op.getOliveProductUrl().trim(), op.getOliveProductRestriction().trim(), useAlternate);
					System.out.print("Successfully redirected from Olive partner" + op.getPartnerDescription()
							+ " customer to USA TODAY e-Edition.");
					// log.setClientIp(clientIP);
					log.setClientIp(request.getRemoteAddr().trim());
					log.setClientUserAgent(request.getHeader("user-agent"));
					log.setPartnerExternalKey("NONE");
					log.setPartnerId(partnerID);
					log.setPropertyId(opac.getPropertyId());
					log.setTimeStamp(new Timestamp(now.getMillis()));
					log.setResponseCode(UsatEnewspaperPartnerAccessLog.SUCCESS);
					log.setRedirectedTo(eEditionLink);
					log.setAccessCodeUsed(accessCode);
					UsatEnewspaperPartnerAccessLogManager logManager = new UsatEnewspaperPartnerAccessLogManager(emf);
					logManager.createUsatEnewspaperPartnerAccessLog(log);
					response.sendRedirect(eEditionLink);
					FacesContext.getCurrentInstance().responseComplete();
					status = "success";
				}
			}
		} catch (Exception e) {
			System.out.println("Could not process customer entered access code, because: " + e);
			status = "failure";
		}
		// TODO: Return an outcome that corresponds to a navigation rule
		return status;
	}

	protected HtmlPanelGroup getGroup1() {
		if (group1 == null) {
			group1 = (HtmlPanelGroup) findComponentInRoot("group1");
		}
		return group1;
	}

	protected HtmlInputText getAccesscode() {
		if (accesscode == null) {
			accesscode = (HtmlInputText) findComponentInRoot("accesscode");
		}
		return accesscode;
	}

	protected HtmlFormItem getAccessCodeFormItem() {
		if (accessCodeFormItem == null) {
			accessCodeFormItem = (HtmlFormItem) findComponentInRoot("accessCodeFormItem");
		}
		return accessCodeFormItem;
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlGraphicImageEx getImageEx1() {
		if (imageEx1 == null) {
			imageEx1 = (HtmlGraphicImageEx) findComponentInRoot("imageEx1");
		}
		return imageEx1;
	}

	protected HtmlOutputText getText3() {
		if (text3 == null) {
			text3 = (HtmlOutputText) findComponentInRoot("text3");
		}
		return text3;
	}

	protected HtmlOutputText getText4() {
		if (text4 == null) {
			text4 = (HtmlOutputText) findComponentInRoot("text4");
		}
		return text4;
	}

}