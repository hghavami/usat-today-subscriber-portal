/**
 * 
 */
package pagecode.account;

import pagecode.PageCodeBase;
import javax.faces.component.html.HtmlOutputText;
import com.ibm.faces.component.html.HtmlScriptCollector;
import javax.faces.component.html.HtmlPanelGrid;
import com.ibm.faces.component.html.HtmlGraphicImageEx;
import javax.faces.component.html.HtmlForm;
import com.ibm.faces.component.html.HtmlCommandExButton;

/**
 * @author aeast
 * 
 */
public class WebPreferences extends PageCodeBase {

	protected HtmlOutputText mainHeader;
	protected HtmlScriptCollector scriptCollector1;
	protected HtmlPanelGrid gridLeftGrid;
	protected HtmlOutputText text1;
	protected HtmlGraphicImageEx imageEx1;
	protected HtmlPanelGrid grid1;
	protected HtmlOutputText text2;
	protected HtmlForm form1;
	protected HtmlCommandExButton button1;
	protected HtmlCommandExButton button2;
	protected HtmlGraphicImageEx imageEx2;

	protected HtmlOutputText getMainHeader() {
		if (mainHeader == null) {
			mainHeader = (HtmlOutputText) findComponentInRoot("mainHeader");
		}
		return mainHeader;
	}

	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlPanelGrid getGridLeftGrid() {
		if (gridLeftGrid == null) {
			gridLeftGrid = (HtmlPanelGrid) findComponentInRoot("gridLeftGrid");
		}
		return gridLeftGrid;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlGraphicImageEx getImageEx1() {
		if (imageEx1 == null) {
			imageEx1 = (HtmlGraphicImageEx) findComponentInRoot("imageEx1");
		}
		return imageEx1;
	}

	protected HtmlPanelGrid getGrid1() {
		if (grid1 == null) {
			grid1 = (HtmlPanelGrid) findComponentInRoot("grid1");
		}
		return grid1;
	}

	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected HtmlForm getForm1() {
		if (form1 == null) {
			form1 = (HtmlForm) findComponentInRoot("form1");
		}
		return form1;
	}

	protected HtmlCommandExButton getButton1() {
		if (button1 == null) {
			button1 = (HtmlCommandExButton) findComponentInRoot("button1");
		}
		return button1;
	}

	protected HtmlCommandExButton getButton2() {
		if (button2 == null) {
			button2 = (HtmlCommandExButton) findComponentInRoot("button2");
		}
		return button2;
	}

	protected HtmlGraphicImageEx getImageEx2() {
		if (imageEx2 == null) {
			imageEx2 = (HtmlGraphicImageEx) findComponentInRoot("imageEx2");
		}
		return imageEx2;
	}

}