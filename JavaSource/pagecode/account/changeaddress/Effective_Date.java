/**
 * 
 */
package pagecode.account.changeaddress;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlOutputText;

import pagecode.PageCodeBase;

import com.ibm.faces.component.html.HtmlCommandExButton;
import javax.faces.component.html.HtmlForm;
import com.ibm.faces.component.html.HtmlScriptCollector;
import javax.faces.component.html.HtmlPanelGrid;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.component.html.HtmlFormItem;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;

import com.ibm.faces.component.html.HtmlInputHelperDatePicker;
import com.usatoday.esub.ncs.handlers.ChangeDeliveryAddressHandler;

import javax.faces.component.html.HtmlMessages;

/**
 * @author hghavami
 * 
 */
public class Effective_Date extends PageCodeBase {

	protected HtmlOutputText DateText;
	protected HtmlOutputText DateHelpText;
	protected HtmlCommandExButton Continue;
	protected HtmlForm form1;
	protected HtmlScriptCollector scriptCollector1;
	protected HtmlPanelGrid grid1;
	protected HtmlPanelFormBox formBoxChangeDeliveryAddressDate;
	protected HtmlFormItem formItem1;
	protected HtmlInputText textDeliveryAddressDate;
	protected HtmlInputHelperDatePicker datePicker1;
	protected HtmlPanelFormBox formBoxChangeDeliveryAddressStopDate;
	protected HtmlFormItem formItem2;
	protected HtmlInputText textDeliveryAddressStopDate;
	protected HtmlOutputText textChangeDeliveryAddressNote;
	protected HtmlInputHelperDatePicker datePicker2;
	protected ChangeDeliveryAddressHandler changeDeliveryAddressHandler;
	protected HtmlMessages messages1;

	public String doButtonSubmitChangeDeliveryAddressAction() {

		// This is java code that runs when this action method is invoked
		String response = "success";
		ChangeDeliveryAddressHandler cdah = this.getChangeDeliveryAddressHandler();
		// Check dates
		if (!cdah.getChangeDeliveryAddressDate().after(cdah.getChangeDeliveryAddressStopDate())) {
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Second date must be greater than the first.",
					null);

			context.addMessage(null, facesMsg);
			response = "failure";
			return response;
		}

		// TODO: Return an outcome that corresponds to a navigation rule
		return response;
	}

	protected HtmlForm getForm1() {
		if (form1 == null) {
			form1 = (HtmlForm) findComponentInRoot("form1");
		}
		return form1;
	}

	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlPanelGrid getGrid1() {
		if (grid1 == null) {
			grid1 = (HtmlPanelGrid) findComponentInRoot("grid1");
		}
		return grid1;
	}

	protected HtmlPanelFormBox getFormBoxChangeDeliveryAddressDate() {
		if (formBoxChangeDeliveryAddressDate == null) {
			formBoxChangeDeliveryAddressDate = (HtmlPanelFormBox) findComponentInRoot("formBoxChangeDeliveryAddressDate");
		}
		return formBoxChangeDeliveryAddressDate;
	}

	protected HtmlFormItem getFormItem1() {
		if (formItem1 == null) {
			formItem1 = (HtmlFormItem) findComponentInRoot("formItem1");
		}
		return formItem1;
	}

	protected HtmlInputText getTextDeliveryAddressDate() {
		if (textDeliveryAddressDate == null) {
			textDeliveryAddressDate = (HtmlInputText) findComponentInRoot("textDeliveryAddressDate");
		}
		return textDeliveryAddressDate;
	}

	protected HtmlInputHelperDatePicker getDatePicker1() {
		if (datePicker1 == null) {
			datePicker1 = (HtmlInputHelperDatePicker) findComponentInRoot("datePicker1");
		}
		return datePicker1;
	}

	protected HtmlPanelFormBox getFormBoxChangeDeliveryAddressStopDate() {
		if (formBoxChangeDeliveryAddressStopDate == null) {
			formBoxChangeDeliveryAddressStopDate = (HtmlPanelFormBox) findComponentInRoot("formBoxChangeDeliveryAddressStopDate");
		}
		return formBoxChangeDeliveryAddressStopDate;
	}

	protected HtmlFormItem getFormItem2() {
		if (formItem2 == null) {
			formItem2 = (HtmlFormItem) findComponentInRoot("formItem2");
		}
		return formItem2;
	}

	protected HtmlInputText getTextDeliveryAddressStopDate() {
		if (textDeliveryAddressStopDate == null) {
			textDeliveryAddressStopDate = (HtmlInputText) findComponentInRoot("textDeliveryAddressStopDate");
		}
		return textDeliveryAddressStopDate;
	}

	protected HtmlOutputText getTextChangeDeliveryAddressNote() {
		if (textChangeDeliveryAddressNote == null) {
			textChangeDeliveryAddressNote = (HtmlOutputText) findComponentInRoot("textChangeDeliveryAddressNote");
		}
		return textChangeDeliveryAddressNote;
	}

	protected HtmlInputHelperDatePicker getDatePicker2() {
		if (datePicker2 == null) {
			datePicker2 = (HtmlInputHelperDatePicker) findComponentInRoot("datePicker2");
		}
		return datePicker2;
	}

	/**
	 * @managed-bean true
	 */
	protected ChangeDeliveryAddressHandler getChangeDeliveryAddressHandler() {
		if (changeDeliveryAddressHandler == null) {
			changeDeliveryAddressHandler = (ChangeDeliveryAddressHandler) getManagedBean("changeDeliveryAddressHandler");
		}
		return changeDeliveryAddressHandler;
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

}