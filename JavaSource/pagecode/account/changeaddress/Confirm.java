/**
 * 
 */
package pagecode.account.changeaddress;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.joda.time.DateTime;

import pagecode.PageCodeBase;

import com.gannett.usat.iconapi.domainbeans.GenesysBaseAPIResponse;
import com.ibm.faces.component.html.HtmlBehavior;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlFormItem;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlPanelDialog;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.business.interfaces.customer.CustomerIntf;
import com.usatoday.businessObjects.customer.AdditionalDeliveryAddressBO;
import com.usatoday.businessObjects.customer.service.CustomerService;
import com.usatoday.businessObjects.customer.service.CustomerServiceEmails;
import com.usatoday.businessObjects.subscriptionTransactions.GenesysResponse;
import com.usatoday.esub.ncs.handlers.ChangeDeliveryAddressHandler;
import com.usatoday.esub.ncs.handlers.CustomerHandler;

/**
 * @author hghavami
 * 
 */
public class Confirm extends PageCodeBase {

	protected HtmlOutputText AccountNumberLabel;
	protected ChangeDeliveryAddressHandler changeDeliveryAddressHandler;
	protected CustomerHandler customerHandler;
	protected HtmlOutputText AccountNumber;
	protected HtmlOutputText Publication;
	protected HtmlOutputText PublicationLabel;
	protected HtmlOutputText Colon1;
	protected HtmlOutputText Colon2;
	protected HtmlOutputText Colon3;
	protected HtmlOutputText StartDate;
	protected HtmlOutputText StartDateLabel;
	protected HtmlOutputText DeliveryInfoLabel;
	protected HtmlOutputText FullName;
	protected HtmlOutputText StreetAddress;
	protected HtmlOutputText AptSuite;
	protected HtmlPanelGrid grid3;
	protected HtmlPanelGrid grid2;
	protected HtmlOutputText FirmName;
	protected HtmlPanelGrid grid4;
	protected HtmlOutputText AddlAddr1;
	protected HtmlOutputText CityStateZip;
	protected HtmlOutputText HomePhone;
	protected HtmlPanelGrid grid6;
	protected HtmlOutputText BusPhone;
	protected HtmlCommandExButton Submit;
	protected HtmlScriptCollector scriptCollector1;
	protected HtmlForm form1;
	protected HtmlPanelGrid grid5;
	protected HtmlJspPanel jspPanel1;
	protected HtmlFormItem savePreviousAddressformItem;
	protected HtmlSelectBooleanCheckbox savePreviousAddressCheckBox;
	protected HtmlPanelFormBox savePreviousAddressformBox;
	protected HtmlPanelGrid savePreviousAddressGrid;
	protected HtmlOutputText StopDateLabel;
	protected HtmlOutputText Colon25;
	protected HtmlOutputText StopDate;
	protected HtmlOutputText colon4;
	protected HtmlOutputText text5;
	protected HtmlOutputText text6;
	protected HtmlOutputText text1;
	protected HtmlOutputText text2;
	protected HtmlOutputText text3;
	protected HtmlOutputText text4;
	protected HtmlOutputText text7;
	protected HtmlOutputText text8;
	protected HtmlOutputText text9;
	protected HtmlOutputText text10;
	protected HtmlOutputText text11;
	protected HtmlOutputText text12;
	protected HtmlOutputText text13;
	protected HtmlOutputText text14;
	protected HtmlOutputText text18;
	protected HtmlOutputText text19;
	protected HtmlOutputText text20;
	protected HtmlOutputText text21;
	protected HtmlOutputText text22;
	protected HtmlOutputText text23;
	protected HtmlOutputText text15;
	protected HtmlOutputText text16;
	protected HtmlOutputText text17;
	protected HtmlPanelGroup groupErrorContents;
	protected HtmlJspPanel jspPanelErrorContent;
	protected HtmlOutputText textErrorRawResponse;
	protected HtmlBehavior behavior6;
	protected HtmlBehavior behavior5;
	protected HtmlPanelDialog dialogErrorDialog;
	protected HtmlCommandExButton button2;
	protected HtmlCommandExButton button1;
	protected HtmlPanelGroup group1;
	protected HtmlMessages messages1;

	public String doButtonSubmitChangeDeliveryAddressAction() {

		// This is java code that runs when this action method is invoked

		// reset error dialog to not show by default
		this.getDialogErrorDialog().setInitiallyShow(false);

		ChangeDeliveryAddressHandler cdah = this.getChangeDeliveryAddressHandler();
		CustomerIntf customer = this.getCustomerHandler().getCustomer();
		GenesysResponse response = null;

		// If any change delivery information has changed, prepare the transaction to be written
		try {
			if (!cdah.isNoDeliveryInfoChange()) {
				// Check for transfer start date being greater than next delivery date for the transfer stop date
				// Convert from Date to DateTime and vice versa
				DateTime nextPubDateDT = new DateTime(cdah.getChangeDeliveryAddressStopDate());
				Date nextPubDate = customer.getCurrentAccount().getProduct()
						.getFirstValidStartDateAfter(nextPubDateDT.withTime(23, 59, 59, 999)).toDate();
				// If start and stop dates needed to change and change address start date is after the next pub date
				// HDCONS-46

				if (cdah.isTransType3() && cdah.getChangeDeliveryAddressDate().after(nextPubDate)) {
					// HDCONS-46 - Create vacation hold if needed
					// change to GenesysResponse response
					response = CustomerService.suspendSubscription(customer.getCurrentAccount(), new DateTime(cdah
							.getChangeDeliveryAddressStopDate().getTime()), new DateTime(cdah.getChangeDeliveryAddressDate()
							.getTime()), false);

					if (response.isContainsErrors()) {
						Collection<GenesysBaseAPIResponse> responses = response.getResponses();
						for (GenesysBaseAPIResponse res : responses) {
							if (res.redirectResponse()) {
								// System Error exists
								cdah.setRawAPIResponse(res.getRawResponse());
								this.getDialogErrorDialog().setInitiallyShow(true);
								throw new Exception("Error Occurred: Unexpected Response Received. Please call Customer Service.");
							} else {
								// business rule errors exist
								Collection<String> errors = res.getErrorMessages();
								if (errors != null) {
									// StringBuilder errorBuf = new StringBuilder();
									for (String msg : errors) {
										FacesContext context = FacesContext.getCurrentInstance();
										FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, null);

										context.addMessage(null, facesMsg);
									}
									return "failure";
								} else {
									throw new Exception("Failed to create delivery address change transaction: Reason Unknown.");
								}
							}
						}
					}

					// end HDCONS-46
				}
				// Write the transfer record if anything other than first and last name has changed
				if (!cdah.getFirmName().toUpperCase().trim()
						.equals(customer.getCurrentAccount().getDeliveryContact().getFirmName().toUpperCase().trim())
						|| !cdah.getStreetAddress()
								.toUpperCase()
								.trim()
								.equals(customer.getCurrentAccount().getDeliveryContact().getUIAddress().getAddress1()
										.toUpperCase().trim())
						|| !cdah.getAptDeptSuite()
								.toUpperCase()
								.trim()
								.equals(customer.getCurrentAccount().getDeliveryContact().getUIAddress().getAptSuite()
										.toUpperCase().trim())
						|| !cdah.getCity()
								.toUpperCase()
								.trim()
								.equals(customer.getCurrentAccount().getDeliveryContact().getUIAddress().getCity().toUpperCase()
										.trim())
						|| !cdah.getState()
								.toUpperCase()
								.trim()
								.equals(customer.getCurrentAccount().getDeliveryContact().getUIAddress().getState().toUpperCase()
										.trim())
						|| !cdah.getZip().equals(customer.getCurrentAccount().getDeliveryContact().getUIAddress().getZip())
						|| !cdah.getAddlAddr1()
								.toUpperCase()
								.trim()
								.equals(customer.getCurrentAccount().getDeliveryContact().getUIAddress().getAddress2()
										.toUpperCase().trim())
						|| !cdah.getHomePhoneComplete().equals(customer.getCurrentAccount().getDeliveryContact().getHomePhone())
						|| !cdah.getBusPhoneComplete().equals(customer.getCurrentAccount().getDeliveryContact().getBusinessPhone())) {

					response = CustomerService.processDeliveryAddressChange(customer.getCurrentAccount(),
							cdah.getDeliveryContact(), new DateTime(cdah.getChangeDeliveryAddressDate().getTime()));
					// if no errors
					if (!response.isContainsErrors()) {

						// Send a confirmation email message
						try {
							CustomerServiceEmails.generateChangeDeliveryConfirmationEmailBody(customer, cdah.getDeliveryContact(),
									cdah.getDeliveryMethod(), cdah.getChangeDeliveryAddressDate(), cdah.isTransType3());
						} catch (Exception e) {
							System.out.println("Failed to send Address Delivery Change Email, because " + e);
						}

						// If customer has wanted write old address to the additonal_delivery_address table
						try {
							if (cdah.isSavePreviousAddress() && !cdah.isPreviousAddressAlreadyWritten()) {
								AdditionalDeliveryAddressBO.save(customer);
								cdah.setPreviousAddressAlreadyWritten(true);
								ArrayList<SelectItem> al = cdah.getAdditionalDeliveryAddressCollection();
								al.clear();
								cdah.setAdditionalDeliveryAddressCollection(al);
								this.setChangeDeliveryAddressHandler(cdah);
							}
						} catch (Exception e) {
							FacesContext context = FacesContext.getCurrentInstance();
							FacesMessage message = null;
							message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Failed to save previous Delivery Address. "
									+ e.getMessage(), null);
							context.addMessage(null, message);
							// Return outcome that corresponds to a navigation rule
							return "failure";
						}

					} else {
						// we got errors
						Collection<GenesysBaseAPIResponse> responses = response.getResponses();
						for (GenesysBaseAPIResponse res : responses) {
							if (res.redirectResponse()) {
								// System Error exists
								cdah.setRawAPIResponse(res.getRawResponse());
								this.getDialogErrorDialog().setInitiallyShow(true);
								throw new Exception("Error Occurred: Unexpected Response Received. Please call Customer Service.");
							} else {
								// business rule errors exist
								Collection<String> errors = res.getErrorMessages();
								if (errors != null) {
									// StringBuilder errorBuf = new StringBuilder();
									for (String msg : errors) {
										FacesContext context = FacesContext.getCurrentInstance();
										FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, null);

										context.addMessage(null, facesMsg);
									}
									return "failure";
								} else {
									throw new Exception("Failed to create delivery address change transaction: Reason Unknown.");
								}
							}
						}
					}
				}
				// end HDCONS - 46
				// Check for name changes and call the API
				if (!cdah.getFirstName().toUpperCase().trim()
						.equals(customer.getCurrentAccount().getDeliveryContact().getFirstName().toUpperCase().trim())
						|| !cdah.getLastName().toUpperCase().trim()
								.equals(customer.getCurrentAccount().getDeliveryContact().getLastName().toUpperCase().trim())) {
					response = CustomerService.processNameChange(customer.getCurrentAccount().getPubCode(), customer
							.getCurrentAccount().getAccountNumber(), "D", cdah.getFirstName(), cdah.getLastName());
					if (response.isContainsErrors()) {
						Collection<String> errors = response.getErrorMessages();
						if (errors != null) {
							// StringBuilder errorBuf = new StringBuilder();
							for (String msg : errors) {
								FacesContext context = FacesContext.getCurrentInstance();
								FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, null);

								context.addMessage(null, facesMsg);
							}
						} else {
							FacesContext context = FacesContext.getCurrentInstance();
							FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
									"Failed to create delivery address name change transaction: Reason Unknown.", null);

							context.addMessage(null, facesMsg);
						}
						// Return outcome that corresponds to a navigation rule
						return "failure";
					}
				}
			}
		} catch (Exception e) {
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage message = null;
			message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Failed to Process Change Delivery Address Request. "
					+ e.getMessage(), null);
			context.addMessage(null, message);
			// Return outcome that corresponds to a navigation rule
			return "failure";
		}

		return "success";
	}

	public ChangeDeliveryAddressHandler getChangeDeliveryAddressHandler() {
		if (changeDeliveryAddressHandler == null) {
			changeDeliveryAddressHandler = (ChangeDeliveryAddressHandler) getManagedBean("changeDeliveryAddressHandler");
		}
		return changeDeliveryAddressHandler;
	}

	public CustomerHandler getCustomerHandler() {
		if (customerHandler == null) {
			customerHandler = (CustomerHandler) getManagedBean("customerHandler");
		}
		return customerHandler;
	}

	protected HtmlPanelGrid getGrid2() {
		if (grid2 == null) {
			grid2 = (HtmlPanelGrid) findComponentInRoot("grid2");
		}
		return grid2;
	}

	protected HtmlPanelGrid getGrid3() {
		if (grid3 == null) {
			grid3 = (HtmlPanelGrid) findComponentInRoot("grid3");
		}
		return grid3;
	}

	protected HtmlPanelGrid getGrid4() {
		if (grid4 == null) {
			grid4 = (HtmlPanelGrid) findComponentInRoot("grid4");
		}
		return grid4;
	}

	protected HtmlPanelGrid getGrid6() {
		if (grid6 == null) {
			grid6 = (HtmlPanelGrid) findComponentInRoot("grid6");
		}
		return grid6;
	}

	public void setChangeDeliveryAddressHandler(ChangeDeliveryAddressHandler changeDeliveryAddressHandler) {
		this.changeDeliveryAddressHandler = changeDeliveryAddressHandler;
	}

	public void setCustomerHandler(CustomerHandler customerHandler) {
		this.customerHandler = customerHandler;
	}

	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlForm getForm1() {
		if (form1 == null) {
			form1 = (HtmlForm) findComponentInRoot("form1");
		}
		return form1;
	}

	protected HtmlPanelGrid getGrid5() {
		if (grid5 == null) {
			grid5 = (HtmlPanelGrid) findComponentInRoot("grid5");
		}
		return grid5;
	}

	protected HtmlJspPanel getJspPanel1() {
		if (jspPanel1 == null) {
			jspPanel1 = (HtmlJspPanel) findComponentInRoot("jspPanel1");
		}
		return jspPanel1;
	}

	protected HtmlFormItem getSavePreviousAddressformItem() {
		if (savePreviousAddressformItem == null) {
			savePreviousAddressformItem = (HtmlFormItem) findComponentInRoot("savePreviousAddressformItem");
		}
		return savePreviousAddressformItem;
	}

	protected HtmlSelectBooleanCheckbox getSavePreviousAddressCheckBox() {
		if (savePreviousAddressCheckBox == null) {
			savePreviousAddressCheckBox = (HtmlSelectBooleanCheckbox) findComponentInRoot("savePreviousAddressCheckBox");
		}
		return savePreviousAddressCheckBox;
	}

	protected HtmlPanelFormBox getSavePreviousAddressformBox() {
		if (savePreviousAddressformBox == null) {
			savePreviousAddressformBox = (HtmlPanelFormBox) findComponentInRoot("savePreviousAddressformBox");
		}
		return savePreviousAddressformBox;
	}

	protected HtmlPanelGrid getSavePreviousAddressGrid() {
		if (savePreviousAddressGrid == null) {
			savePreviousAddressGrid = (HtmlPanelGrid) findComponentInRoot("savePreviousAddressGrid");
		}
		return savePreviousAddressGrid;
	}

	protected HtmlOutputText getColon4() {
		if (colon4 == null) {
			colon4 = (HtmlOutputText) findComponentInRoot("colon4");
		}
		return colon4;
	}

	protected HtmlOutputText getText5() {
		if (text5 == null) {
			text5 = (HtmlOutputText) findComponentInRoot("text5");
		}
		return text5;
	}

	protected HtmlOutputText getText6() {
		if (text6 == null) {
			text6 = (HtmlOutputText) findComponentInRoot("text6");
		}
		return text6;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected HtmlOutputText getText3() {
		if (text3 == null) {
			text3 = (HtmlOutputText) findComponentInRoot("text3");
		}
		return text3;
	}

	protected HtmlOutputText getText4() {
		if (text4 == null) {
			text4 = (HtmlOutputText) findComponentInRoot("text4");
		}
		return text4;
	}

	protected HtmlOutputText getText7() {
		if (text7 == null) {
			text7 = (HtmlOutputText) findComponentInRoot("text7");
		}
		return text7;
	}

	protected HtmlOutputText getText8() {
		if (text8 == null) {
			text8 = (HtmlOutputText) findComponentInRoot("text8");
		}
		return text8;
	}

	protected HtmlOutputText getText9() {
		if (text9 == null) {
			text9 = (HtmlOutputText) findComponentInRoot("text9");
		}
		return text9;
	}

	protected HtmlOutputText getText10() {
		if (text10 == null) {
			text10 = (HtmlOutputText) findComponentInRoot("text10");
		}
		return text10;
	}

	protected HtmlOutputText getText11() {
		if (text11 == null) {
			text11 = (HtmlOutputText) findComponentInRoot("text11");
		}
		return text11;
	}

	protected HtmlOutputText getText12() {
		if (text12 == null) {
			text12 = (HtmlOutputText) findComponentInRoot("text12");
		}
		return text12;
	}

	protected HtmlOutputText getText13() {
		if (text13 == null) {
			text13 = (HtmlOutputText) findComponentInRoot("text13");
		}
		return text13;
	}

	protected HtmlOutputText getText14() {
		if (text14 == null) {
			text14 = (HtmlOutputText) findComponentInRoot("text14");
		}
		return text14;
	}

	protected HtmlOutputText getText18() {
		if (text18 == null) {
			text18 = (HtmlOutputText) findComponentInRoot("text18");
		}
		return text18;
	}

	protected HtmlOutputText getText19() {
		if (text19 == null) {
			text19 = (HtmlOutputText) findComponentInRoot("text19");
		}
		return text19;
	}

	protected HtmlOutputText getText20() {
		if (text20 == null) {
			text20 = (HtmlOutputText) findComponentInRoot("text20");
		}
		return text20;
	}

	protected HtmlOutputText getText21() {
		if (text21 == null) {
			text21 = (HtmlOutputText) findComponentInRoot("text21");
		}
		return text21;
	}

	protected HtmlOutputText getText22() {
		if (text22 == null) {
			text22 = (HtmlOutputText) findComponentInRoot("text22");
		}
		return text22;
	}

	protected HtmlOutputText getText23() {
		if (text23 == null) {
			text23 = (HtmlOutputText) findComponentInRoot("text23");
		}
		return text23;
	}

	protected HtmlOutputText getText15() {
		if (text15 == null) {
			text15 = (HtmlOutputText) findComponentInRoot("text15");
		}
		return text15;
	}

	protected HtmlOutputText getText16() {
		if (text16 == null) {
			text16 = (HtmlOutputText) findComponentInRoot("text16");
		}
		return text16;
	}

	protected HtmlOutputText getText17() {
		if (text17 == null) {
			text17 = (HtmlOutputText) findComponentInRoot("text17");
		}
		return text17;
	}

	protected HtmlPanelGroup getGroupErrorContents() {
		if (groupErrorContents == null) {
			groupErrorContents = (HtmlPanelGroup) findComponentInRoot("groupErrorContents");
		}
		return groupErrorContents;
	}

	protected HtmlJspPanel getJspPanelErrorContent() {
		if (jspPanelErrorContent == null) {
			jspPanelErrorContent = (HtmlJspPanel) findComponentInRoot("jspPanelErrorContent");
		}
		return jspPanelErrorContent;
	}

	protected HtmlOutputText getTextErrorRawResponse() {
		if (textErrorRawResponse == null) {
			textErrorRawResponse = (HtmlOutputText) findComponentInRoot("textErrorRawResponse");
		}
		return textErrorRawResponse;
	}

	protected HtmlBehavior getBehavior6() {
		if (behavior6 == null) {
			behavior6 = (HtmlBehavior) findComponentInRoot("behavior6");
		}
		return behavior6;
	}

	protected HtmlBehavior getBehavior5() {
		if (behavior5 == null) {
			behavior5 = (HtmlBehavior) findComponentInRoot("behavior5");
		}
		return behavior5;
	}

	protected HtmlPanelDialog getDialogErrorDialog() {
		if (dialogErrorDialog == null) {
			dialogErrorDialog = (HtmlPanelDialog) findComponentInRoot("dialogErrorDialog");
		}
		return dialogErrorDialog;
	}

	protected HtmlCommandExButton getButton2() {
		if (button2 == null) {
			button2 = (HtmlCommandExButton) findComponentInRoot("button2");
		}
		return button2;
	}

	protected HtmlCommandExButton getButton1() {
		if (button1 == null) {
			button1 = (HtmlCommandExButton) findComponentInRoot("button1");
		}
		return button1;
	}

	protected HtmlPanelGroup getGroup1() {
		if (group1 == null) {
			group1 = (HtmlPanelGroup) findComponentInRoot("group1");
		}
		return group1;
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

}