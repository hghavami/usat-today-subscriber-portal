/**
 * 
 */
package pagecode.account.changeaddress;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.component.html.HtmlSelectOneRadio;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import pagecode.PageCodeBase;

import com.ibm.faces.component.html.HtmlBehaviorKeyPress;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlFormItem;
import com.ibm.faces.component.html.HtmlInputHelperAssist;
import com.ibm.faces.component.html.HtmlInputHelperSetFocus;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.component.html.HtmlPanelLayout;
import com.ibm.faces.component.html.HtmlPanelSection;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.business.interfaces.UIAddressIntf;
import com.usatoday.business.interfaces.customer.AdditionalDeliveryAddressIntf;
import com.usatoday.business.interfaces.customer.CustomerIntf;
import com.usatoday.business.interfaces.products.SubscriptionOfferIntf;
import com.usatoday.business.interfaces.products.SubscriptionProductIntf;
import com.usatoday.businessObjects.customer.AdditionalDeliveryAddressBO;
import com.usatoday.businessObjects.customer.ContactBO;
import com.usatoday.businessObjects.customer.DeliveryMethodCode1ValidationBO;
import com.usatoday.businessObjects.customer.PersistentAddressBO;
import com.usatoday.businessObjects.customer.UIAddressBO;
import com.usatoday.businessObjects.products.SubscriptionProductBO;
import com.usatoday.businessObjects.products.promotions.PromotionSet;
import com.usatoday.esub.common.UTCommon;
import com.usatoday.esub.handlers.SubscriptionOfferHandler;
import com.usatoday.esub.ncs.handlers.ChangeDeliveryAddressHandler;
import com.usatoday.esub.ncs.handlers.CustomerHandler;

/**
 * @author hghavami
 * 
 */
public class Changeaddress extends PageCodeBase {

	protected HtmlScriptCollector scriptCollector1;
	protected HtmlForm formChangeDeliveryAddress;
	protected HtmlInputTextarea textarea1;
	protected HtmlInputText AccountNumber;
	protected HtmlInputText FirstName;
	protected HtmlInputText LastName;
	protected HtmlInputText FirmName;
	protected HtmlInputText StreetAddress;
	protected HtmlInputText AptDeptSuite;
	protected HtmlInputText AdditionalAddress1;
	protected HtmlInputText AdditionalAddress2;
	protected HtmlInputText City;
	protected HtmlInputText State;
	protected HtmlInputText Zip;
	protected HtmlInputText HomePhone;
	protected HtmlInputText WorkPhone;
	protected HtmlOutputText AdditionalAddressText1;
	protected HtmlPanelGrid AdditionalAddressGrid1;
	protected HtmlSelectOneRadio AdditionalAddressRadio1;
	protected HtmlOutputText RequiredText1;
	protected HtmlOutputText text1;
	protected HtmlSelectOneRadio radio1;
	protected HtmlPanelGroup AdditionalAddressGroup1;
	protected HtmlJspPanel jspPanelFormFields;
	protected ChangeDeliveryAddressHandler changeDeliveryAddressHandler;
	protected CustomerHandler customerHandler;
	protected HtmlOutputText AccountNumLabel;
	protected HtmlOutputText FirstNameLabel;
	protected HtmlOutputText LastNameLabel;
	protected HtmlOutputText RequiredLabel;
	protected HtmlOutputText CompanyNameLabel;
	protected HtmlOutputText StreetAddressLabel;
	protected HtmlOutputText AptSuiteLabel;
	protected HtmlOutputText AddlAddressLabel;
	protected HtmlOutputText CityLabel;
	protected HtmlOutputText StateLabel;
	protected HtmlOutputText ZipLabel;
	protected HtmlOutputText HomePhoneLabel;
	protected HtmlOutputText WorkPhoneLabel;
	protected HtmlOutputText RequiredLabel1;
	protected HtmlOutputText RequiredLabel2;
	protected HtmlOutputText RequiredLabel3;
	protected HtmlOutputText RequiredLabel4;
	protected HtmlOutputText RequiredLabel5;
	protected HtmlOutputText RequiredLabel6;
	protected HtmlOutputText RequiredLabel7;
	protected HtmlOutputText RequiredLabel8;
	protected HtmlInputHelperAssist assist1;
	protected HtmlCommandExButton Continue;
	protected HtmlOutputText Title;
	protected HtmlJspPanel jspPanel2;
	protected HtmlJspPanel jspPanel1;
	protected HtmlOutputText text3;
	protected HtmlOutputText text2;
	protected HtmlOutputText colonLabel12;
	protected HtmlOutputText colonLabel;
	protected HtmlOutputText colonLabel1;
	protected HtmlOutputText colonLabel3;
	protected HtmlOutputText colonLabel4;
	protected HtmlOutputText colonLabel5;
	protected HtmlOutputText colonLabel6;
	protected HtmlOutputText colonLabel7;
	protected HtmlOutputText colonLabel8;
	protected HtmlOutputText colonLabel9;
	protected HtmlOutputText colonLabel10;
	protected HtmlOutputText colonLabel11;
	protected HtmlPanelSection section1;
	protected HtmlJspPanel jspPanel3;
	protected HtmlPanelGrid grid2;
	protected HtmlPanelGrid grid3;
	protected HtmlPanelGrid grid4;
	protected HtmlPanelGrid grid5;
	protected HtmlPanelGrid grid6;
	protected HtmlPanelGrid grid7;
	protected HtmlPanelGrid grid8;
	protected HtmlPanelGrid grid9;
	protected HtmlPanelGrid grid10;
	protected HtmlPanelGrid grid11;
	protected HtmlPanelGrid grid12;
	protected HtmlPanelGrid grid13;
	protected HtmlPanelGrid grid14;
	protected HtmlFormItem formAccountNumberLabel;
	protected HtmlPanelFormBox formboxDeliveryInformation;
	protected HtmlFormItem formFirstNameLabel;
	protected HtmlFormItem formLastNameLabel;
	protected HtmlFormItem formFirmNameLabel;
	protected HtmlFormItem formStreetAddressLabel;
	protected HtmlFormItem formAptSuiteLabel;
	protected HtmlFormItem formAddlAddrLabel;
	protected HtmlFormItem formCityLabel;
	protected HtmlFormItem formStateLabel;
	protected HtmlFormItem formZipLabel;
	protected HtmlSelectOneMenu menuDeliveryState;
	protected HtmlFormItem formHomePhone;
	protected HtmlFormItem formBusPhone;
	protected HtmlInputText HomePhoneAreaCode;
	protected HtmlInputText HomePhoneExchange;
	protected HtmlInputText HomePhoneExtension;
	protected HtmlInputText BusPhoneExchange;
	protected HtmlInputText BusPhoneExtension;
	protected HtmlMessages messagesAllMesssages;
	protected HtmlPanelLayout layout1;
	protected HtmlInputHelperAssist assist2;
	protected HtmlInputHelperAssist assist3;
	protected HtmlInputHelperAssist assist5;
	protected HtmlCommandExButton delete;
	protected HtmlBehaviorKeyPress behaviorKeyPress1;
	protected HtmlInputHelperSetFocus setFocus1;
	protected HtmlInputHelperAssist assist6;
	protected HtmlInputHelperAssist assist8;
	protected HtmlInputText BusPhoneAreaCode;
	protected HtmlInputHelperAssist assist7;
	protected HtmlPanelGrid grid1;
	protected HtmlInputHelperAssist assist4;
	protected HtmlInputHelperAssist assist9;
	protected HtmlInputHelperAssist assist10;
	protected HtmlInputHelperAssist assist11;

	public String doButtonSubmitChangeDeliveryAddressAction() {

		String response = "success";
		ChangeDeliveryAddressHandler cdah = this.getChangeDeliveryAddressHandler();
		String additionalAddressChoice = cdah.getAdditionalAddressChoices();
		CustomerIntf customer = null;

		try {
			customer = this.getCustomerHandler().getCustomer();
			this.getChangeDeliveryAddressHandler().setCustomer(customer);
		} catch (Exception e) {
			System.out.println("ChangeDeliveryAddress:  Failed to get customer information: " + e.getMessage());
		}
		// Check for selected previous address
		ArrayList<SelectItem> al = cdah.getAdditionalDeliveryAddressCollection();
		Iterator<SelectItem> itr = al.iterator();
		while (itr.hasNext()) {
			SelectItem sl = itr.next();
			if (sl.getValue().equals(additionalAddressChoice)) {
				// Get the corresponding additional delivery address from the Customer object
				HashMap<String, AdditionalDeliveryAddressIntf> additionalDeliveryAddresses = new HashMap<String, AdditionalDeliveryAddressIntf>();
				additionalDeliveryAddresses = customer.getAdditionalDeliveryAddress();
				Iterator<AdditionalDeliveryAddressIntf> aItr = additionalDeliveryAddresses.values().iterator();
				while (aItr.hasNext()) {
					AdditionalDeliveryAddressIntf adai = aItr.next();
					if (adai.getId() == new Integer(cdah.getAdditionalAddressChoices()).intValue()) {
						// Check this for when overwriting firm name with blanks
						if (!adai.getFirmName().trim().equals("") && adai.getFirmName() != null) {
							cdah.setFirmName(adai.getFirmName());
						} else {
							cdah.setFirmName("  ");
						}
						cdah.setStreetAddress(adai.getStreetAddress());
						// Check this for when overwriting additional address with blanks
						if (!adai.getAddlAddr1().trim().equals("") && adai.getAddlAddr1() != null) {
							cdah.setAddlAddr1(adai.getAddlAddr1());
						} else {
							cdah.setAddlAddr1("  ");
						}
						// Check this for when overwriting apt suite with blanks
						if (!adai.getAptDeptSuite().trim().equals("") && adai.getAptDeptSuite() != null) {
							cdah.setAptDeptSuite(adai.getAptDeptSuite());
						} else {
							cdah.setAptDeptSuite("  ");
						}
						cdah.setCity(adai.getCity());
						cdah.setState(adai.getState());
						cdah.setZip(adai.getZip());
						if (!adai.getHomePhone().trim().equals("") && adai.getHomePhone() != null) {
							cdah.setHomePhoneAreaCode(adai.getHomePhone().substring(0, 3));
							cdah.setHomePhoneExchange(adai.getHomePhone().substring(3, 6));
							cdah.setHomePhoneExtension(adai.getHomePhone().substring(6, 10));
							cdah.setHomePhoneComplete(adai.getHomePhone());
						}
						if (!adai.getBusPhone().trim().equals("") && adai.getBusPhone() != null) {
							cdah.setBusPhoneAreaCode(adai.getBusPhone().substring(0, 3));
							cdah.setBusPhoneExchange(adai.getBusPhone().substring(3, 6));
							cdah.setBusPhoneExtension(adai.getBusPhone().substring(6, 10));
							cdah.setBusPhoneComplete(adai.getBusPhone());
						} else {
							// Check this for when overwriting firm name with blanks
							cdah.setBusPhoneAreaCode("  ");
							cdah.setBusPhoneExchange("  ");
							cdah.setBusPhoneExtension("  ");
							cdah.setBusPhoneComplete("  ");
						}
						this.setChangeDeliveryAddressHandler(cdah);
					}
				}
			}
		}

		// /// Delivery Contact ///////////
		ContactBO deliveryContact = new ContactBO();

		// Verify the delivery address
		try {

			UIAddressBO dAddress = new UIAddressBO();
			dAddress.setAddress1(cdah.getStreetAddress().trim());
			dAddress.setAddress2(cdah.getAddlAddr1().trim());
			dAddress.setAptSuite(cdah.getAptDeptSuite().trim());
			dAddress.setCity(cdah.getCity().trim());
			dAddress.setState(cdah.getState());
			dAddress.setZip(cdah.getZip().trim());

			deliveryContact.setUIAddress(dAddress);
			deliveryContact.setFirstName(cdah.getFirstName());
			if (!cdah.getFirmName().trim().equals("")) {
				deliveryContact.setFirmName(cdah.getFirmName());
			} else {
				deliveryContact.setFirmName("  ");
			}
			deliveryContact.setLastName(cdah.getLastName());
			deliveryContact.setHomePhone(cdah.getHomePhoneComplete());
			if (!cdah.getBusPhoneComplete().trim().equals("")) {
				deliveryContact.setBusinessPhone(cdah.getBusPhoneComplete());
			} else {
				deliveryContact.setBusinessPhone("  ");
			}
			if (!deliveryContact.validateContact()) {
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
						"We are unable to validate the delivery address. Please verify the address you entered.", null);

				context.addMessage(null, facesMsg);
				response = "failure";
				return response;
			} else {
				// update the bean with the code1 address
				try {
					UIAddressIntf correctedAddress = deliveryContact.getPersistentAddress().convertToUIAddress();

					if (correctedAddress.getAddress1().length() > 0) {
						cdah.setStreetAddress(correctedAddress.getAddress1());
						if (!correctedAddress.getAddress2().trim().equals("") && correctedAddress.getAddress2() != null) {
							cdah.setAddlAddr1(cdah.getAddlAddr1());
						} else {
							cdah.setAddlAddr1("  ");
						}
						cdah.setAptDeptSuite(correctedAddress.getAptSuite());
						if (!correctedAddress.getAptSuite().trim().equals("") && correctedAddress.getAptSuite() != null) {
							cdah.setAptDeptSuite(cdah.getAptDeptSuite());
						} else {
							cdah.setAptDeptSuite("  ");
						}
						cdah.setCity(correctedAddress.getCity());
						cdah.setZip(correctedAddress.getZip());
						cdah.setDeliveryContact(deliveryContact);
					}
				} catch (Exception e) {
					// ignore it
					System.out.println("JSF Delivery Address Entry::Exception setting up corrected address: " + e.getMessage());
				}
			}
		} catch (Exception e) {
			System.out.println("JSF Delivery Address Entry::Exception setting up corrected address: " + e.getMessage());
		}

		// Delivery Method checks
		try {
			SubscriptionProductIntf product = SubscriptionProductBO.getSubscriptionProduct(customerHandler.getCurrentAccount()
					.getAccount().getPubCode());
			SubscriptionOfferHandler currentOfferHandler = customerHandler.getCurrentAccountPubDefaultOffer();

			if (product.isElectronicDelivery()) {
				// for electronic just set to m
				cdah.setDeliveryMethod("M");
				cdah.setDeliveryMethodCheck(false);
				cdah.setDeliveryMethodText("Electronic Delivery");
				cdah.setDeliveryMethod("M");
			} else {
				cdah.setDeliveryMethodCheck(false);

				if (deliveryContact.getPersistentAddress().isGUIAddress()) {
					// always set Gannett Units to C
					cdah.setDeliveryMethod("C");
					cdah.setDeliveryMethodText("Morning Delivery");
					cdah.setDeliveryMethodCheck(com.usatoday.util.constants.UsaTodayConstants.DELIVERY_NOTIFY);
					cdah.setDeliveryMethod("C");
				} else {
					DeliveryMethodCode1ValidationBO deliveryMethodBO = new DeliveryMethodCode1ValidationBO();
					// Get delivery Method, if active
					try {
						cdah.setDeliveryMethodCheck(com.usatoday.util.constants.UsaTodayConstants.DELIVERY_NOTIFY);
						PromotionSet currentOfferPromotionSet = currentOfferHandler.getCurrentOffer().getPromotionSet();

						if (currentOfferPromotionSet != null && currentOfferPromotionSet.getDeliveryNotification() != null
								&& currentOfferPromotionSet.getDeliveryNotification().getFulfillText() != null
								&& !currentOfferPromotionSet.getDeliveryNotification().getFulfillText().trim().equals("")) {
							if (currentOfferPromotionSet.getDeliveryNotification().getFulfillText().equals("ON")) {
								cdah.setDeliveryMethodCheck(true);
							} else {
								cdah.setDeliveryMethodCheck(false);
							}
						}

						String deliveryMethod = "";
						// Check for iSeries connection
						boolean as400JDBCServiceUp = com.usatoday.businessObjects.util.AS400CurrentStatus.getJdbcActive();
						if (cdah.isDeliveryMethodCheck() && as400JDBCServiceUp) {
							deliveryMethod = deliveryMethodBO.determineDeliveryMethod(customerHandler.getCurrentAccount()
									.getAccount().getPubCode(), deliveryContact.getUiAddress());
							if (deliveryMethod != null) {
								cdah.setDeliveryMethod(deliveryMethod);
								if (deliveryMethod.equalsIgnoreCase("M")) {
									cdah.setDeliveryMethodText("Mail Delivery");
									cdah.setDeliveryMethod(deliveryMethod);
								} else if (deliveryMethod.equalsIgnoreCase("C")) {
									cdah.setDeliveryMethodText("Morning Delivery");
									cdah.setDeliveryMethod(deliveryMethod);
								}
								this.setChangeDeliveryAddressHandler(cdah);
							}
						} else {
							cdah.setDeliveryMethodText("");
						}
					} catch (Exception e) {
						System.out.println("JSF One Page OrderEntry:: Could not determine delivery method, because: "
								+ e.getMessage());
					}
				} // end if not a Gannett Unit
			} // end else print product
		} catch (Exception e) {

		}
		cdah.setNoDeliveryInfoChange(false);
		// Check if start date is needed, if so transaction type will be 03
		if (!cdah.getFirmName().toUpperCase().trim()
				.equals(customer.getCurrentAccount().getDeliveryContact().getFirmName().toUpperCase().trim())
				|| !cdah.getStreetAddress()
						.toUpperCase()
						.trim()
						.equals(customer.getCurrentAccount().getDeliveryContact().getUIAddress().getAddress1().toUpperCase().trim())
				|| !cdah.getAptDeptSuite()
						.toUpperCase()
						.trim()
						.equals(customer.getCurrentAccount().getDeliveryContact().getUIAddress().getAptSuite().toUpperCase().trim())
				|| !cdah.getCity().toUpperCase().trim()
						.equals(customer.getCurrentAccount().getDeliveryContact().getUIAddress().getCity().toUpperCase().trim())
				|| !cdah.getState().toUpperCase().trim()
						.equals(customer.getCurrentAccount().getDeliveryContact().getUIAddress().getState().toUpperCase().trim())
				|| !cdah.getZip().equals(customer.getCurrentAccount().getDeliveryContact().getUIAddress().getZip())) {

			cdah.setTransType3(true);
			// We are not clearing additional address info any longer
/*			cdah.setAddlAddr1("");
			UIAddressBO dAddress = new UIAddressBO();
			dAddress = (UIAddressBO) deliveryContact.getUiAddress();
			dAddress.setAddress2("");
			deliveryContact.setUIAddress(dAddress);
*/			PersistentAddressBO pAI = (PersistentAddressBO) deliveryContact.getPersistentAddress();
			pAI.setAddress2("");
			deliveryContact.setPersistentAddress(pAI);
			cdah.setDeliveryContact(deliveryContact);
			response = "dateCheck";

		} else if (cdah.getFirstName().toUpperCase().trim()
				.equals(customer.getCurrentAccount().getDeliveryContact().getFirstName().toUpperCase().trim())
				&& cdah.getLastName().toUpperCase().trim()
						.equals(customer.getCurrentAccount().getDeliveryContact().getLastName().toUpperCase().trim())
				&& cdah.getFirmName().toUpperCase().trim()
						.equals(customer.getCurrentAccount().getDeliveryContact().getFirmName().toUpperCase().trim())
				&& cdah.getStreetAddress()
						.toUpperCase()
						.trim()
						.equals(customer.getCurrentAccount().getDeliveryContact().getUIAddress().getAddress1().toUpperCase().trim())
				&& cdah.getAptDeptSuite()
						.toUpperCase()
						.trim()
						.equals(customer.getCurrentAccount().getDeliveryContact().getUIAddress().getAptSuite().toUpperCase().trim())
				&& cdah.getCity().toUpperCase().trim()
						.equals(customer.getCurrentAccount().getDeliveryContact().getUIAddress().getCity().toUpperCase().trim())
				&& cdah.getState().toUpperCase().trim()
						.equals(customer.getCurrentAccount().getDeliveryContact().getUIAddress().getState().toUpperCase().trim())
				&& cdah.getZip().equals(customer.getCurrentAccount().getDeliveryContact().getUIAddress().getZip())
				&& cdah.getAddlAddr1()
						.toUpperCase()
						.trim()
						.equals(customer.getCurrentAccount().getDeliveryContact().getUIAddress().getAddress2().toUpperCase().trim())
				&& cdah.getHomePhoneComplete().equals(customer.getCurrentAccount().getDeliveryContact().getHomePhone())
				&& cdah.getBusPhoneComplete().equals(customer.getCurrentAccount().getDeliveryContact().getBusinessPhone())) {

			// No change at all, do not write a transaction
			cdah.setNoDeliveryInfoChange(true);
			cdah.setTransType3(false);
		}

		else {
			// No real address change, transaction type will be 04
			cdah.setTransType3(false);
		}

		this.setChangeDeliveryAddressHandler(cdah);
		return response;
	}

	public String doButtonDeleteDeliveryAddressAction() {
		String response = "failure";
		ChangeDeliveryAddressHandler cdah = this.getChangeDeliveryAddressHandler();
		String additionalAddressChoice = cdah.getAdditionalAddressChoices();
		CustomerIntf customer = null;

		try {
			customer = this.getCustomerHandler().getCustomer();
			this.getChangeDeliveryAddressHandler().setCustomer(customer);
		} catch (Exception e) {
			System.out.println("ChangeDeliveryAddress:  Failed to get customer information: " + e.getMessage());
		}
		// Check for selected previous address and delete it
		ArrayList<SelectItem> al = cdah.getAdditionalDeliveryAddressCollection();
		Iterator<SelectItem> itr = al.iterator();
		while (itr.hasNext()) {
			SelectItem sl = itr.next();
			if (sl.getValue().equals(additionalAddressChoice)) {
				// Get the corresponding additional delivery address from the Customer object
				HashMap<String, AdditionalDeliveryAddressIntf> additionalDeliveryAddresses = new HashMap<String, AdditionalDeliveryAddressIntf>();
				additionalDeliveryAddresses = customer.getAdditionalDeliveryAddress();
				Iterator<AdditionalDeliveryAddressIntf> aItr = additionalDeliveryAddresses.values().iterator();
				while (aItr.hasNext()) {
					AdditionalDeliveryAddressIntf adai = aItr.next();
					if (adai.getId() == new Integer(cdah.getAdditionalAddressChoices()).intValue()) {
						try {
							AdditionalDeliveryAddressBO.delete(adai);
							al.clear();
							cdah.setAdditionalDeliveryAddressCollection(al);
							this.setChangeDeliveryAddressHandler(cdah);
							FacesContext context = FacesContext.getCurrentInstance();
							FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
									"Selected delivery address deleted.", null);
							context.addMessage(null, facesMsg);
						} catch (Exception e) {
							FacesContext context = FacesContext.getCurrentInstance();
							FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
									"We are unable to delete selected delivery address. Please try again.", null);
							context.addMessage(null, facesMsg);
						}
					}
				}
			}
		}
		return response;
	}

	protected HtmlInputHelperAssist getAssist1() {
		if (assist1 == null) {
			assist1 = (HtmlInputHelperAssist) findComponentInRoot("assist1");
		}
		return assist1;
	}

	/**
	 * @managed-bean true
	 */
	protected ChangeDeliveryAddressHandler getChangeDeliveryAddressHandler() {
		if (changeDeliveryAddressHandler == null) {
			changeDeliveryAddressHandler = (ChangeDeliveryAddressHandler) getManagedBean("changeDeliveryAddressHandler");
		}
		return changeDeliveryAddressHandler;
	}

	protected HtmlOutputText getColonLabel() {
		if (colonLabel == null) {
			colonLabel = (HtmlOutputText) findComponentInRoot("colonLabel");
		}
		return colonLabel;
	}

	protected HtmlOutputText getColonLabel1() {
		if (colonLabel1 == null) {
			colonLabel1 = (HtmlOutputText) findComponentInRoot("colonLabel1");
		}
		return colonLabel1;
	}

	protected HtmlOutputText getColonLabel10() {
		if (colonLabel10 == null) {
			colonLabel10 = (HtmlOutputText) findComponentInRoot("colonLabel10");
		}
		return colonLabel10;
	}

	protected HtmlOutputText getColonLabel11() {
		if (colonLabel11 == null) {
			colonLabel11 = (HtmlOutputText) findComponentInRoot("colonLabel11");
		}
		return colonLabel11;
	}

	protected HtmlOutputText getColonLabel12() {
		if (colonLabel12 == null) {
			colonLabel12 = (HtmlOutputText) findComponentInRoot("colonLabel12");
		}
		return colonLabel12;
	}

	protected HtmlOutputText getColonLabel3() {
		if (colonLabel3 == null) {
			colonLabel3 = (HtmlOutputText) findComponentInRoot("colonLabel3");
		}
		return colonLabel3;
	}

	protected HtmlOutputText getColonLabel4() {
		if (colonLabel4 == null) {
			colonLabel4 = (HtmlOutputText) findComponentInRoot("colonLabel4");
		}
		return colonLabel4;
	}

	protected HtmlOutputText getColonLabel5() {
		if (colonLabel5 == null) {
			colonLabel5 = (HtmlOutputText) findComponentInRoot("colonLabel5");
		}
		return colonLabel5;
	}

	protected HtmlOutputText getColonLabel6() {
		if (colonLabel6 == null) {
			colonLabel6 = (HtmlOutputText) findComponentInRoot("colonLabel6");
		}
		return colonLabel6;
	}

	protected HtmlOutputText getColonLabel7() {
		if (colonLabel7 == null) {
			colonLabel7 = (HtmlOutputText) findComponentInRoot("colonLabel7");
		}
		return colonLabel7;
	}

	protected HtmlOutputText getColonLabel8() {
		if (colonLabel8 == null) {
			colonLabel8 = (HtmlOutputText) findComponentInRoot("colonLabel8");
		}
		return colonLabel8;
	}

	protected HtmlOutputText getColonLabel9() {
		if (colonLabel9 == null) {
			colonLabel9 = (HtmlOutputText) findComponentInRoot("colonLabel9");
		}
		return colonLabel9;
	}

	public CustomerHandler getCustomerHandler() {
		if (customerHandler == null) {
			customerHandler = (CustomerHandler) getManagedBean("customerHandler");
		}
		return customerHandler;
	}

	protected HtmlForm getFormChangeDeliveryAddress() {
		if (formChangeDeliveryAddress == null) {
			formChangeDeliveryAddress = (HtmlForm) findComponentInRoot("formChangeDeliveryAddress");
		}
		return formChangeDeliveryAddress;
	}

	protected HtmlFormItem getFormAccountNumberLabel() {
		if (formAccountNumberLabel == null) {
			formAccountNumberLabel = (HtmlFormItem) findComponentInRoot("formAccountNumberLabel");
		}
		return formAccountNumberLabel;
	}

	protected HtmlFormItem getFormAddlAddrLabel() {
		if (formAddlAddrLabel == null) {
			formAddlAddrLabel = (HtmlFormItem) findComponentInRoot("formAddlAddrLabel");
		}
		return formAddlAddrLabel;
	}

	protected HtmlFormItem getFormAptSuiteLabel() {
		if (formAptSuiteLabel == null) {
			formAptSuiteLabel = (HtmlFormItem) findComponentInRoot("formAptSuiteLabel");
		}
		return formAptSuiteLabel;
	}

	protected HtmlPanelFormBox getFormboxDeliveryInformation() {
		if (formboxDeliveryInformation == null) {
			formboxDeliveryInformation = (HtmlPanelFormBox) findComponentInRoot("formboxDeliveryInformation");
		}
		return formboxDeliveryInformation;
	}

	protected HtmlFormItem getFormBusPhone() {
		if (formBusPhone == null) {
			formBusPhone = (HtmlFormItem) findComponentInRoot("formBusPhone");
		}
		return formBusPhone;
	}

	protected HtmlFormItem getFormCityLabel() {
		if (formCityLabel == null) {
			formCityLabel = (HtmlFormItem) findComponentInRoot("formCityLabel");
		}
		return formCityLabel;
	}

	protected HtmlFormItem getFormFirmNameLabel() {
		if (formFirmNameLabel == null) {
			formFirmNameLabel = (HtmlFormItem) findComponentInRoot("formFirmNameLabel");
		}
		return formFirmNameLabel;
	}

	protected HtmlFormItem getFormFirstNameLabel() {
		if (formFirstNameLabel == null) {
			formFirstNameLabel = (HtmlFormItem) findComponentInRoot("formFirstNameLabel");
		}
		return formFirstNameLabel;
	}

	protected HtmlFormItem getFormHomePhone() {
		if (formHomePhone == null) {
			formHomePhone = (HtmlFormItem) findComponentInRoot("formHomePhone");
		}
		return formHomePhone;
	}

	protected HtmlFormItem getFormLastNameLabel() {
		if (formLastNameLabel == null) {
			formLastNameLabel = (HtmlFormItem) findComponentInRoot("formLastNameLabel");
		}
		return formLastNameLabel;
	}

	protected HtmlFormItem getFormStateLabel() {
		if (formStateLabel == null) {
			formStateLabel = (HtmlFormItem) findComponentInRoot("formStateLabel");
		}
		return formStateLabel;
	}

	protected HtmlFormItem getFormStreetAddressLabel() {
		if (formStreetAddressLabel == null) {
			formStreetAddressLabel = (HtmlFormItem) findComponentInRoot("formStreetAddressLabel");
		}
		return formStreetAddressLabel;
	}

	protected HtmlFormItem getFormZipLabel() {
		if (formZipLabel == null) {
			formZipLabel = (HtmlFormItem) findComponentInRoot("formZipLabel");
		}
		return formZipLabel;
	}

	protected HtmlPanelGrid getGrid10() {
		if (grid10 == null) {
			grid10 = (HtmlPanelGrid) findComponentInRoot("grid10");
		}
		return grid10;
	}

	protected HtmlPanelGrid getGrid11() {
		if (grid11 == null) {
			grid11 = (HtmlPanelGrid) findComponentInRoot("grid11");
		}
		return grid11;
	}

	protected HtmlPanelGrid getGrid12() {
		if (grid12 == null) {
			grid12 = (HtmlPanelGrid) findComponentInRoot("grid12");
		}
		return grid12;
	}

	protected HtmlPanelGrid getGrid13() {
		if (grid13 == null) {
			grid13 = (HtmlPanelGrid) findComponentInRoot("grid13");
		}
		return grid13;
	}

	protected HtmlPanelGrid getGrid14() {
		if (grid14 == null) {
			grid14 = (HtmlPanelGrid) findComponentInRoot("grid14");
		}
		return grid14;
	}

	protected HtmlPanelGrid getGrid2() {
		if (grid2 == null) {
			grid2 = (HtmlPanelGrid) findComponentInRoot("grid2");
		}
		return grid2;
	}

	protected HtmlPanelGrid getGrid3() {
		if (grid3 == null) {
			grid3 = (HtmlPanelGrid) findComponentInRoot("grid3");
		}
		return grid3;
	}

	protected HtmlPanelGrid getGrid4() {
		if (grid4 == null) {
			grid4 = (HtmlPanelGrid) findComponentInRoot("grid4");
		}
		return grid4;
	}

	protected HtmlPanelGrid getGrid5() {
		if (grid5 == null) {
			grid5 = (HtmlPanelGrid) findComponentInRoot("grid5");
		}
		return grid5;
	}

	protected HtmlPanelGrid getGrid6() {
		if (grid6 == null) {
			grid6 = (HtmlPanelGrid) findComponentInRoot("grid6");
		}
		return grid6;
	}

	protected HtmlPanelGrid getGrid7() {
		if (grid7 == null) {
			grid7 = (HtmlPanelGrid) findComponentInRoot("grid7");
		}
		return grid7;
	}

	protected HtmlPanelGrid getGrid8() {
		if (grid8 == null) {
			grid8 = (HtmlPanelGrid) findComponentInRoot("grid8");
		}
		return grid8;
	}

	protected HtmlPanelGrid getGrid9() {
		if (grid9 == null) {
			grid9 = (HtmlPanelGrid) findComponentInRoot("grid9");
		}
		return grid9;
	}

	protected HtmlJspPanel getJspPanel1() {
		if (jspPanel1 == null) {
			jspPanel1 = (HtmlJspPanel) findComponentInRoot("jspPanel1");
		}
		return jspPanel1;
	}

	protected HtmlJspPanel getJspPanel2() {
		if (jspPanel2 == null) {
			jspPanel2 = (HtmlJspPanel) findComponentInRoot("jspPanel2");
		}
		return jspPanel2;
	}

	protected HtmlJspPanel getJspPanel3() {
		if (jspPanel3 == null) {
			jspPanel3 = (HtmlJspPanel) findComponentInRoot("jspPanel3");
		}
		return jspPanel3;
	}

	protected HtmlJspPanel getJspPanelFormFields() {
		if (jspPanelFormFields == null) {
			jspPanelFormFields = (HtmlJspPanel) findComponentInRoot("jspPanelFormFields");
		}
		return jspPanelFormFields;
	}

	protected HtmlSelectOneMenu getMenuDeliveryState() {
		if (menuDeliveryState == null) {
			menuDeliveryState = (HtmlSelectOneMenu) findComponentInRoot("menuDeliveryState");
		}
		return menuDeliveryState;
	}

	protected HtmlSelectOneRadio getRadio1() {
		if (radio1 == null) {
			radio1 = (HtmlSelectOneRadio) findComponentInRoot("radio1");
		}
		return radio1;
	}

	protected HtmlOutputText getRequiredLabel8() {
		if (RequiredLabel8 == null) {
			RequiredLabel8 = (HtmlOutputText) findComponentInRoot("RequiredLabel8");
		}
		return RequiredLabel8;
	}

	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlPanelSection getSection1() {
		if (section1 == null) {
			section1 = (HtmlPanelSection) findComponentInRoot("section1");
		}
		return section1;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected HtmlOutputText getText3() {
		if (text3 == null) {
			text3 = (HtmlOutputText) findComponentInRoot("text3");
		}
		return text3;
	}

	protected HtmlInputTextarea getTextarea1() {
		if (textarea1 == null) {
			textarea1 = (HtmlInputTextarea) findComponentInRoot("textarea1");
		}
		return textarea1;
	}

	public void onPageLoadBegin(FacesContext facescontext) {
		// Type Java code to handle page load begin event here

		ChangeDeliveryAddressHandler cdah = this.getChangeDeliveryAddressHandler();
		HttpServletRequest req = (HttpServletRequest) this.getFacesContext().getExternalContext().getRequest();
		SubscriptionOfferIntf currentOffer = UTCommon.getCurrentOfferVersion2(req);

		try {
			CustomerIntf cust = this.getCustomerHandler().getCustomer();
			this.getChangeDeliveryAddressHandler().setCustomer(cust);
		} catch (Exception e) {
			System.out.println("ChangeDeliveryAddress:  Failed to get customer information: " + e.getMessage());
		}
		try {
			if (cdah.getChangeDeliveryAddressDate() == null) {
				cdah.setChangeDeliveryAddressStopDate(currentOffer.getSubscriptionProduct().getEarliestPossibleStartDate().toDate());
				cdah.setChangeDeliveryAddressDate(currentOffer.getSubscriptionProduct()
						.getFirstValidStartDateAfter(currentOffer.getSubscriptionProduct().getEarliestPossibleStartDate()).toDate());
			}
		} catch (Exception e) {
			; // ignore
		}
	}

	public void onPageLoadEnd(FacesContext facescontext) {

		// void <method>(FacesContext facescontext)
	}

	/**
	 * @managed-bean true
	 */
	protected void setChangeDeliveryAddressHandler(ChangeDeliveryAddressHandler changeDeliveryAddressHandler) {
		this.changeDeliveryAddressHandler = changeDeliveryAddressHandler;
	}

	public void setCustomerHandler(CustomerHandler customerHandler) {
		this.customerHandler = customerHandler;
	}

	protected HtmlMessages getMessagesAllMesssages() {
		if (messagesAllMesssages == null) {
			messagesAllMesssages = (HtmlMessages) findComponentInRoot("messagesAllMesssages");
		}
		return messagesAllMesssages;
	}

	protected HtmlPanelLayout getLayout1() {
		if (layout1 == null) {
			layout1 = (HtmlPanelLayout) findComponentInRoot("layout1");
		}
		return layout1;
	}

	protected HtmlInputHelperAssist getAssist2() {
		if (assist2 == null) {
			assist2 = (HtmlInputHelperAssist) findComponentInRoot("assist2");
		}
		return assist2;
	}

	protected HtmlInputHelperAssist getAssist3() {
		if (assist3 == null) {
			assist3 = (HtmlInputHelperAssist) findComponentInRoot("assist3");
		}
		return assist3;
	}

	protected HtmlInputHelperAssist getAssist5() {
		if (assist5 == null) {
			assist5 = (HtmlInputHelperAssist) findComponentInRoot("assist5");
		}
		return assist5;
	}

	protected HtmlCommandExButton getDelete() {
		if (delete == null) {
			delete = (HtmlCommandExButton) findComponentInRoot("delete");
		}
		return delete;
	}

	protected HtmlBehaviorKeyPress getBehaviorKeyPress1() {
		if (behaviorKeyPress1 == null) {
			behaviorKeyPress1 = (HtmlBehaviorKeyPress) findComponentInRoot("behaviorKeyPress1");
		}
		return behaviorKeyPress1;
	}

	protected HtmlInputHelperSetFocus getSetFocus1() {
		if (setFocus1 == null) {
			setFocus1 = (HtmlInputHelperSetFocus) findComponentInRoot("setFocus1");
		}
		return setFocus1;
	}

	protected HtmlInputHelperAssist getAssist6() {
		if (assist6 == null) {
			assist6 = (HtmlInputHelperAssist) findComponentInRoot("assist6");
		}
		return assist6;
	}

	protected HtmlInputHelperAssist getAssist8() {
		if (assist8 == null) {
			assist8 = (HtmlInputHelperAssist) findComponentInRoot("assist8");
		}
		return assist8;
	}

	protected HtmlInputHelperAssist getAssist7() {
		if (assist7 == null) {
			assist7 = (HtmlInputHelperAssist) findComponentInRoot("assist7");
		}
		return assist7;
	}

	protected HtmlPanelGrid getGrid1() {
		if (grid1 == null) {
			grid1 = (HtmlPanelGrid) findComponentInRoot("grid1");
		}
		return grid1;
	}

	protected HtmlInputHelperAssist getAssist4() {
		if (assist4 == null) {
			assist4 = (HtmlInputHelperAssist) findComponentInRoot("assist4");
		}
		return assist4;
	}

	protected HtmlInputHelperAssist getAssist9() {
		if (assist9 == null) {
			assist9 = (HtmlInputHelperAssist) findComponentInRoot("assist9");
		}
		return assist9;
	}

	protected HtmlInputHelperAssist getAssist10() {
		if (assist10 == null) {
			assist10 = (HtmlInputHelperAssist) findComponentInRoot("assist10");
		}
		return assist10;
	}

	protected HtmlInputHelperAssist getAssist11() {
		if (assist11 == null) {
			assist11 = (HtmlInputHelperAssist) findComponentInRoot("assist11");
		}
		return assist11;
	}
}