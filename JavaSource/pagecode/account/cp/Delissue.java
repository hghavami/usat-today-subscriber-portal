/**
 * 
 */
package pagecode.account.cp;

import java.util.Collection;

import javax.faces.application.FacesMessage;
import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;

import pagecode.PageCodeBase;

import com.gannett.usat.iconapi.domainbeans.GenesysBaseAPIResponse;
import com.ibm.faces.component.html.HtmlBehavior;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlFormItem;
import com.ibm.faces.component.html.HtmlInputHelperDatePicker;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlPanelDialog;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.component.html.HtmlPanelSection;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.business.interfaces.customer.SubscriberAccountIntf;
import com.usatoday.businessObjects.customer.service.Complaint;
import com.usatoday.businessObjects.customer.service.CustomerService;
import com.usatoday.businessObjects.customer.service.CustomerServiceEmails;
import com.usatoday.businessObjects.subscriptionTransactions.GenesysResponse;
import com.usatoday.esub.ncs.handlers.ComplaintHandler;
import com.usatoday.esub.ncs.handlers.ComplaintSelectItems;
import com.usatoday.esub.ncs.handlers.CustomerHandler;

/**
 * @author aeast
 * 
 */
public class Delissue extends PageCodeBase {

	protected HtmlScriptCollector scriptCollectorComplaints;
	protected HtmlOutputText mainHeader;
	protected HtmlMessages messages1;
	protected CustomerHandler customerHandler;
	protected ComplaintSelectItems complaintIssuesAppScope;
	protected ComplaintHandler complaintHandler;
	protected HtmlPanelFormBox formBox1;
	protected HtmlFormItem formItemIssueType;
	protected HtmlSelectOneMenu menuIssueType;
	protected UISelectItems selectItems2;
	protected HtmlFormItem IssueDate3;
	protected HtmlFormItem IssueDate2;
	protected HtmlFormItem IssueDate4;
	protected HtmlFormItem IssueDate5;
	protected HtmlForm form1;
	protected HtmlInputText textIssueDate1;
	protected HtmlFormItem formItemIssueDate1;
	protected HtmlInputHelperDatePicker datePicker1;
	protected HtmlBehavior behavior1;
	protected HtmlPanelGrid grid3;
	protected HtmlOutputText textCityStateZip;
	protected HtmlJspPanel jspPanel2;
	protected HtmlOutputText text2;
	protected HtmlJspPanel jspPanel1;
	protected HtmlInputText textIssueDate2;
	protected HtmlPanelSection sectionIssueDate2;
	protected HtmlPanelFormBox formBoxIssueDate2;
	protected HtmlInputHelperDatePicker datePicker2;
	protected HtmlBehavior behavior2;
	protected HtmlJspPanel jspPanel4;
	protected HtmlOutputText text3;
	protected HtmlJspPanel jspPanel3;
	protected HtmlInputText textIssueDate3;
	protected HtmlPanelSection sectionIssueDate3;
	protected HtmlPanelFormBox formBoxIssueDate3;
	protected HtmlInputHelperDatePicker datePicker3;
	protected HtmlBehavior behavior3;
	protected HtmlJspPanel jspPanel44;
	protected HtmlOutputText text33;
	protected HtmlJspPanel jspPanel33;
	protected HtmlInputText textIssueDate4;
	protected HtmlPanelSection sectionIssueDate4;
	protected HtmlPanelFormBox formBoxIssueDate4;
	protected HtmlInputHelperDatePicker datePicker4;
	protected HtmlBehavior behavior4;
	protected HtmlJspPanel jspPanel444;
	protected HtmlOutputText text333;
	protected HtmlJspPanel jspPanel333;
	protected HtmlInputText textIssueDate5;
	protected HtmlPanelSection sectionIssueDate5;
	protected HtmlPanelFormBox formBoxIssueDate5;
	protected HtmlInputHelperDatePicker datePicker5;
	protected HtmlCommandExButton buttonSubmitComplaint;
	protected HtmlPanelGrid grid2;
	protected HtmlPanelGrid gridCustomerInfoBorderGrid;
	protected HtmlPanelGrid gridProductDataGrid;
	protected HtmlPanelGroup group2;
	protected HtmlPanelGrid grid1;
	protected HtmlPanelGroup group3;
	protected HtmlPanelGrid gridDelInfoHeadGrid;
	protected HtmlOutputText textDeliveryInfoHeaderText;
	protected HtmlOutputText textProductNameLabel;
	protected HtmlOutputText textProductName;
	protected HtmlOutputText textAccountNumberLabel;
	protected HtmlOutputText textAccountNumber;
	protected HtmlPanelGrid gridDeliveryInfoGrid;
	protected HtmlOutputText text1;
	protected HtmlOutputText text5;
	protected HtmlOutputText text6;
	protected HtmlOutputText textPhone;
	protected HtmlOutputText textNumbercopies;
	protected HtmlPanelGroup groupErrorContents;
	protected HtmlCommandExButton button2;
	protected HtmlBehavior behavior6;
	protected HtmlBehavior behavior5;
	protected HtmlPanelDialog dialogErrorDialog;
	protected HtmlCommandExButton button1;
	protected HtmlJspPanel jspPanelErrorContent;
	protected HtmlOutputText textAccountInfoTextHeader;
	protected HtmlOutputText textErrorRawResponse;
	protected HtmlPanelGrid grid4;
	protected HtmlOutputText text7;
	protected HtmlPanelGrid grid5;

	protected HtmlScriptCollector getScriptCollectorComplaints() {
		if (scriptCollectorComplaints == null) {
			scriptCollectorComplaints = (HtmlScriptCollector) findComponentInRoot("scriptCollectorComplaints");
		}
		return scriptCollectorComplaints;
	}

	protected HtmlOutputText getMainHeader() {
		if (mainHeader == null) {
			mainHeader = (HtmlOutputText) findComponentInRoot("mainHeader");
		}
		return mainHeader;
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

	/**
	 * @managed-bean true
	 */
	protected CustomerHandler getCustomerHandler() {
		if (customerHandler == null) {
			customerHandler = (CustomerHandler) getManagedBean("customerHandler");
		}
		return customerHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setCustomerHandler(CustomerHandler customerHandler) {
		this.customerHandler = customerHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected ComplaintSelectItems getComplaintIssuesAppScope() {
		if (complaintIssuesAppScope == null) {
			complaintIssuesAppScope = (ComplaintSelectItems) getManagedBean("complaintIssuesAppScope");
		}
		return complaintIssuesAppScope;
	}

	/**
	 * @managed-bean true
	 */
	protected void setComplaintIssuesAppScope(ComplaintSelectItems complaintIssuesAppScope) {
		this.complaintIssuesAppScope = complaintIssuesAppScope;
	}

	/**
	 * @managed-bean true
	 */
	protected ComplaintHandler getComplaintHandler() {
		if (complaintHandler == null) {
			complaintHandler = (ComplaintHandler) getManagedBean("complaintHandler");
		}
		return complaintHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setComplaintHandler(ComplaintHandler complaintHandler) {
		this.complaintHandler = complaintHandler;
	}

	public void onPageLoadBegin(FacesContext facescontext) {
		// Type Java code to handle page load begin event here

		// void <method>(FacesContext facescontext)
		// force initialization if hasn't happened
		this.getComplaintIssuesAppScope();
		ComplaintHandler cplh = this.getComplaintHandler();

		CustomerHandler ch = this.getCustomerHandler();

		if (cplh.getCurrentCustomer() == null) {
			cplh.setCurrentCustomer(ch);
		}

		// If NI trans type, then the account is pending and no rates can be displayed
		if (!cplh.isSubscriptionActive()) {
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
					"Your subscription currently is pending/inactive, please try later or call our customer service.", null);

			context.addMessage(null, facesMsg);
		}
	}

	protected HtmlPanelFormBox getFormBox1() {
		if (formBox1 == null) {
			formBox1 = (HtmlPanelFormBox) findComponentInRoot("formBox1");
		}
		return formBox1;
	}

	protected HtmlFormItem getFormItemIssueType() {
		if (formItemIssueType == null) {
			formItemIssueType = (HtmlFormItem) findComponentInRoot("formItemIssueType");
		}
		return formItemIssueType;
	}

	protected HtmlSelectOneMenu getMenuIssueType() {
		if (menuIssueType == null) {
			menuIssueType = (HtmlSelectOneMenu) findComponentInRoot("menuIssueType");
		}
		return menuIssueType;
	}

	protected UISelectItems getSelectItems2() {
		if (selectItems2 == null) {
			selectItems2 = (UISelectItems) findComponentInRoot("selectItems2");
		}
		return selectItems2;
	}

	protected HtmlForm getForm1() {
		if (form1 == null) {
			form1 = (HtmlForm) findComponentInRoot("form1");
		}
		return form1;
	}

	public String doButtonSubmitComplaintAction() {
		// This is java code that runs when this action method is invoked
		String responseString = "failure";
		ComplaintHandler complaintHandler = this.getComplaintHandler();
		CustomerHandler ch = this.getCustomerHandler();

		Complaint c = null;
		try {
			SubscriberAccountIntf account = ch.getCurrentAccount().getAccount();

			c = Complaint.complaintFactory(account.getPubCode());

			c.setTransactionCode(complaintHandler.getIssueType());

			c.addIssueDate(complaintHandler.getDate1());
			if (complaintHandler.getDate2() != null) {
				c.addIssueDate(complaintHandler.getDate2());
			}
			if (complaintHandler.getDate3() != null) {
				c.addIssueDate(complaintHandler.getDate3());
			}
			if (complaintHandler.getDate4() != null) {
				c.addIssueDate(complaintHandler.getDate4());
			}
			if (complaintHandler.getDate5() != null) {
				c.addIssueDate(complaintHandler.getDate5());
			}

			GenesysResponse response = CustomerService.processComplaint(account, c);

			// if no errors
			if (!response.isContainsErrors()) {
				complaintHandler.resetWebFormValuesOnly();
				complaintHandler.setLastComplaint(c);
				try {
					CustomerServiceEmails.sendComplaintConfirmationEmail(ch.getCustomer(), account, c);
				} catch (Exception e) {
					// ignore email failure;
					System.out.println("Failed to send complaint confirmation email for account: " + account.getAccountNumber()
							+ ". " + e.getMessage());
				}
				responseString = "success";
			} else {
				// we got errors
				Collection<GenesysBaseAPIResponse> responses = response.getResponses();
				for (GenesysBaseAPIResponse res : responses) {
					if (res.redirectResponse()) {
						// System Error exists
						complaintHandler.setRawAPIResponse(res.getRawResponse());
						this.getDialogErrorDialog().setInitiallyShow(true);
						throw new Exception("Error Occurred: Unexpected Response Received. Please call Customer Service.");
					} else {
						// business rule errors exist
						Collection<String> errors = res.getErrorMessages();
						if (errors != null && !errors.isEmpty()) {
							// StringBuilder errorBuf = new StringBuilder();
							for (String msg : errors) {
								FacesContext context = FacesContext.getCurrentInstance();
								FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, null);

								context.addMessage(null, facesMsg);
							}
							throw new Exception();
						} 
//						else {
//							throw new Exception("Failed to create delivery issue record: Reason Unknown.");
//						}
					}
				}
			}

		} catch (Exception e) {
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), null);

			context.addMessage(null, facesMsg);
			responseString = "failure";
		}

		return responseString;
	}

	protected HtmlInputText getTextIssueDate1() {
		if (textIssueDate1 == null) {
			textIssueDate1 = (HtmlInputText) findComponentInRoot("textIssueDate1");
		}
		return textIssueDate1;
	}

	protected HtmlFormItem getFormItemIssueDate1() {
		if (formItemIssueDate1 == null) {
			formItemIssueDate1 = (HtmlFormItem) findComponentInRoot("formItemIssueDate1");
		}
		return formItemIssueDate1;
	}

	protected HtmlInputHelperDatePicker getDatePicker1() {
		if (datePicker1 == null) {
			datePicker1 = (HtmlInputHelperDatePicker) findComponentInRoot("datePicker1");
		}
		return datePicker1;
	}

	protected HtmlBehavior getBehavior1() {
		if (behavior1 == null) {
			behavior1 = (HtmlBehavior) findComponentInRoot("behavior1");
		}
		return behavior1;
	}

	protected HtmlPanelGrid getGrid3() {
		if (grid3 == null) {
			grid3 = (HtmlPanelGrid) findComponentInRoot("grid3");
		}
		return grid3;
	}

	protected HtmlOutputText getTextCityStateZip() {
		if (textCityStateZip == null) {
			textCityStateZip = (HtmlOutputText) findComponentInRoot("textCityStateZip");
		}
		return textCityStateZip;
	}

	protected HtmlJspPanel getJspPanel2() {
		if (jspPanel2 == null) {
			jspPanel2 = (HtmlJspPanel) findComponentInRoot("jspPanel2");
		}
		return jspPanel2;
	}

	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected HtmlJspPanel getJspPanel1() {
		if (jspPanel1 == null) {
			jspPanel1 = (HtmlJspPanel) findComponentInRoot("jspPanel1");
		}
		return jspPanel1;
	}

	protected HtmlInputText getTextIssueDate2() {
		if (textIssueDate2 == null) {
			textIssueDate2 = (HtmlInputText) findComponentInRoot("textIssueDate2");
		}
		return textIssueDate2;
	}

	protected HtmlPanelSection getSectionIssueDate2() {
		if (sectionIssueDate2 == null) {
			sectionIssueDate2 = (HtmlPanelSection) findComponentInRoot("sectionIssueDate2");
		}
		return sectionIssueDate2;
	}

	protected HtmlPanelFormBox getFormBoxIssueDate2() {
		if (formBoxIssueDate2 == null) {
			formBoxIssueDate2 = (HtmlPanelFormBox) findComponentInRoot("formBoxIssueDate2");
		}
		return formBoxIssueDate2;
	}

	protected HtmlInputHelperDatePicker getDatePicker2() {
		if (datePicker2 == null) {
			datePicker2 = (HtmlInputHelperDatePicker) findComponentInRoot("datePicker2");
		}
		return datePicker2;
	}

	protected HtmlBehavior getBehavior2() {
		if (behavior2 == null) {
			behavior2 = (HtmlBehavior) findComponentInRoot("behavior2");
		}
		return behavior2;
	}

	protected HtmlJspPanel getJspPanel4() {
		if (jspPanel4 == null) {
			jspPanel4 = (HtmlJspPanel) findComponentInRoot("jspPanel4");
		}
		return jspPanel4;
	}

	protected HtmlOutputText getText3() {
		if (text3 == null) {
			text3 = (HtmlOutputText) findComponentInRoot("text3");
		}
		return text3;
	}

	protected HtmlJspPanel getJspPanel3() {
		if (jspPanel3 == null) {
			jspPanel3 = (HtmlJspPanel) findComponentInRoot("jspPanel3");
		}
		return jspPanel3;
	}

	protected HtmlInputText getTextIssueDate3() {
		if (textIssueDate3 == null) {
			textIssueDate3 = (HtmlInputText) findComponentInRoot("textIssueDate3");
		}
		return textIssueDate3;
	}

	protected HtmlPanelSection getSectionIssueDate3() {
		if (sectionIssueDate3 == null) {
			sectionIssueDate3 = (HtmlPanelSection) findComponentInRoot("sectionIssueDate3");
		}
		return sectionIssueDate3;
	}

	protected HtmlPanelFormBox getFormBoxIssueDate3() {
		if (formBoxIssueDate3 == null) {
			formBoxIssueDate3 = (HtmlPanelFormBox) findComponentInRoot("formBoxIssueDate3");
		}
		return formBoxIssueDate3;
	}

	protected HtmlInputHelperDatePicker getDatePicker3() {
		if (datePicker3 == null) {
			datePicker3 = (HtmlInputHelperDatePicker) findComponentInRoot("datePicker3");
		}
		return datePicker3;
	}

	protected HtmlBehavior getBehavior3() {
		if (behavior3 == null) {
			behavior3 = (HtmlBehavior) findComponentInRoot("behavior3");
		}
		return behavior3;
	}

	protected HtmlJspPanel getJspPanel44() {
		if (jspPanel44 == null) {
			jspPanel44 = (HtmlJspPanel) findComponentInRoot("jspPanel44");
		}
		return jspPanel44;
	}

	protected HtmlOutputText getText33() {
		if (text33 == null) {
			text33 = (HtmlOutputText) findComponentInRoot("text33");
		}
		return text33;
	}

	protected HtmlJspPanel getJspPanel33() {
		if (jspPanel33 == null) {
			jspPanel33 = (HtmlJspPanel) findComponentInRoot("jspPanel33");
		}
		return jspPanel33;
	}

	protected HtmlInputText getTextIssueDate4() {
		if (textIssueDate4 == null) {
			textIssueDate4 = (HtmlInputText) findComponentInRoot("textIssueDate4");
		}
		return textIssueDate4;
	}

	protected HtmlPanelSection getSectionIssueDate4() {
		if (sectionIssueDate4 == null) {
			sectionIssueDate4 = (HtmlPanelSection) findComponentInRoot("sectionIssueDate4");
		}
		return sectionIssueDate4;
	}

	protected HtmlPanelFormBox getFormBoxIssueDate4() {
		if (formBoxIssueDate4 == null) {
			formBoxIssueDate4 = (HtmlPanelFormBox) findComponentInRoot("formBoxIssueDate4");
		}
		return formBoxIssueDate4;
	}

	protected HtmlInputHelperDatePicker getDatePicker4() {
		if (datePicker4 == null) {
			datePicker4 = (HtmlInputHelperDatePicker) findComponentInRoot("datePicker4");
		}
		return datePicker4;
	}

	protected HtmlBehavior getBehavior4() {
		if (behavior4 == null) {
			behavior4 = (HtmlBehavior) findComponentInRoot("behavior4");
		}
		return behavior4;
	}

	protected HtmlJspPanel getJspPanel444() {
		if (jspPanel444 == null) {
			jspPanel444 = (HtmlJspPanel) findComponentInRoot("jspPanel444");
		}
		return jspPanel444;
	}

	protected HtmlOutputText getText333() {
		if (text333 == null) {
			text333 = (HtmlOutputText) findComponentInRoot("text333");
		}
		return text333;
	}

	protected HtmlJspPanel getJspPanel333() {
		if (jspPanel333 == null) {
			jspPanel333 = (HtmlJspPanel) findComponentInRoot("jspPanel333");
		}
		return jspPanel333;
	}

	protected HtmlInputText getTextIssueDate5() {
		if (textIssueDate5 == null) {
			textIssueDate5 = (HtmlInputText) findComponentInRoot("textIssueDate5");
		}
		return textIssueDate5;
	}

	protected HtmlPanelSection getSectionIssueDate5() {
		if (sectionIssueDate5 == null) {
			sectionIssueDate5 = (HtmlPanelSection) findComponentInRoot("sectionIssueDate5");
		}
		return sectionIssueDate5;
	}

	protected HtmlPanelFormBox getFormBoxIssueDate5() {
		if (formBoxIssueDate5 == null) {
			formBoxIssueDate5 = (HtmlPanelFormBox) findComponentInRoot("formBoxIssueDate5");
		}
		return formBoxIssueDate5;
	}

	protected HtmlInputHelperDatePicker getDatePicker5() {
		if (datePicker5 == null) {
			datePicker5 = (HtmlInputHelperDatePicker) findComponentInRoot("datePicker5");
		}
		return datePicker5;
	}

	protected HtmlCommandExButton getButtonSubmitComplaint() {
		if (buttonSubmitComplaint == null) {
			buttonSubmitComplaint = (HtmlCommandExButton) findComponentInRoot("buttonSubmitComplaint");
		}
		return buttonSubmitComplaint;
	}

	protected HtmlPanelGrid getGrid2() {
		if (grid2 == null) {
			grid2 = (HtmlPanelGrid) findComponentInRoot("grid2");
		}
		return grid2;
	}

	protected HtmlPanelGrid getGridCustomerInfoBorderGrid() {
		if (gridCustomerInfoBorderGrid == null) {
			gridCustomerInfoBorderGrid = (HtmlPanelGrid) findComponentInRoot("gridCustomerInfoBorderGrid");
		}
		return gridCustomerInfoBorderGrid;
	}

	protected HtmlPanelGrid getGridProductDataGrid() {
		if (gridProductDataGrid == null) {
			gridProductDataGrid = (HtmlPanelGrid) findComponentInRoot("gridProductDataGrid");
		}
		return gridProductDataGrid;
	}

	protected HtmlPanelGroup getGroup2() {
		if (group2 == null) {
			group2 = (HtmlPanelGroup) findComponentInRoot("group2");
		}
		return group2;
	}

	protected HtmlPanelGrid getGrid1() {
		if (grid1 == null) {
			grid1 = (HtmlPanelGrid) findComponentInRoot("grid1");
		}
		return grid1;
	}

	protected HtmlPanelGroup getGroup3() {
		if (group3 == null) {
			group3 = (HtmlPanelGroup) findComponentInRoot("group3");
		}
		return group3;
	}

	protected HtmlPanelGrid getGridDelInfoHeadGrid() {
		if (gridDelInfoHeadGrid == null) {
			gridDelInfoHeadGrid = (HtmlPanelGrid) findComponentInRoot("gridDelInfoHeadGrid");
		}
		return gridDelInfoHeadGrid;
	}

	protected HtmlOutputText getTextDeliveryInfoHeaderText() {
		if (textDeliveryInfoHeaderText == null) {
			textDeliveryInfoHeaderText = (HtmlOutputText) findComponentInRoot("textDeliveryInfoHeaderText");
		}
		return textDeliveryInfoHeaderText;
	}

	protected HtmlOutputText getTextProductNameLabel() {
		if (textProductNameLabel == null) {
			textProductNameLabel = (HtmlOutputText) findComponentInRoot("textProductNameLabel");
		}
		return textProductNameLabel;
	}

	protected HtmlOutputText getTextProductName() {
		if (textProductName == null) {
			textProductName = (HtmlOutputText) findComponentInRoot("textProductName");
		}
		return textProductName;
	}

	protected HtmlOutputText getTextAccountNumberLabel() {
		if (textAccountNumberLabel == null) {
			textAccountNumberLabel = (HtmlOutputText) findComponentInRoot("textAccountNumberLabel");
		}
		return textAccountNumberLabel;
	}

	protected HtmlOutputText getTextAccountNumber() {
		if (textAccountNumber == null) {
			textAccountNumber = (HtmlOutputText) findComponentInRoot("textAccountNumber");
		}
		return textAccountNumber;
	}

	protected HtmlPanelGrid getGridDeliveryInfoGrid() {
		if (gridDeliveryInfoGrid == null) {
			gridDeliveryInfoGrid = (HtmlPanelGrid) findComponentInRoot("gridDeliveryInfoGrid");
		}
		return gridDeliveryInfoGrid;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlOutputText getText5() {
		if (text5 == null) {
			text5 = (HtmlOutputText) findComponentInRoot("text5");
		}
		return text5;
	}

	protected HtmlOutputText getText6() {
		if (text6 == null) {
			text6 = (HtmlOutputText) findComponentInRoot("text6");
		}
		return text6;
	}

	protected HtmlOutputText getTextPhone() {
		if (textPhone == null) {
			textPhone = (HtmlOutputText) findComponentInRoot("textPhone");
		}
		return textPhone;
	}

	protected HtmlOutputText getTextNumbercopies() {
		if (textNumbercopies == null) {
			textNumbercopies = (HtmlOutputText) findComponentInRoot("textNumbercopies");
		}
		return textNumbercopies;
	}

	protected HtmlPanelGroup getGroupErrorContents() {
		if (groupErrorContents == null) {
			groupErrorContents = (HtmlPanelGroup) findComponentInRoot("groupErrorContents");
		}
		return groupErrorContents;
	}

	protected HtmlCommandExButton getButton2() {
		if (button2 == null) {
			button2 = (HtmlCommandExButton) findComponentInRoot("button2");
		}
		return button2;
	}

	protected HtmlBehavior getBehavior6() {
		if (behavior6 == null) {
			behavior6 = (HtmlBehavior) findComponentInRoot("behavior6");
		}
		return behavior6;
	}

	protected HtmlBehavior getBehavior5() {
		if (behavior5 == null) {
			behavior5 = (HtmlBehavior) findComponentInRoot("behavior5");
		}
		return behavior5;
	}

	protected HtmlPanelDialog getDialogErrorDialog() {
		if (dialogErrorDialog == null) {
			dialogErrorDialog = (HtmlPanelDialog) findComponentInRoot("dialogErrorDialog");
		}
		return dialogErrorDialog;
	}

	protected HtmlCommandExButton getButton1() {
		if (button1 == null) {
			button1 = (HtmlCommandExButton) findComponentInRoot("button1");
		}
		return button1;
	}

	protected HtmlJspPanel getJspPanelErrorContent() {
		if (jspPanelErrorContent == null) {
			jspPanelErrorContent = (HtmlJspPanel) findComponentInRoot("jspPanelErrorContent");
		}
		return jspPanelErrorContent;
	}

	protected HtmlOutputText getTextAccountInfoTextHeader() {
		if (textAccountInfoTextHeader == null) {
			textAccountInfoTextHeader = (HtmlOutputText) findComponentInRoot("textAccountInfoTextHeader");
		}
		return textAccountInfoTextHeader;
	}

	protected HtmlOutputText getTextErrorRawResponse() {
		if (textErrorRawResponse == null) {
			textErrorRawResponse = (HtmlOutputText) findComponentInRoot("textErrorRawResponse");
		}
		return textErrorRawResponse;
	}

	protected HtmlPanelGrid getGrid4() {
		if (grid4 == null) {
			grid4 = (HtmlPanelGrid) findComponentInRoot("grid4");
		}
		return grid4;
	}

	protected HtmlOutputText getText7() {
		if (text7 == null) {
			text7 = (HtmlOutputText) findComponentInRoot("text7");
		}
		return text7;
	}

	protected HtmlPanelGrid getGrid5() {
		if (grid5 == null) {
			grid5 = (HtmlPanelGrid) findComponentInRoot("grid5");
		}
		return grid5;
	}

}