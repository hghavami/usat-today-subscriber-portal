/**
 * 
 */
package pagecode.account.cp;

import pagecode.PageCodeBase;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.esub.ncs.handlers.ComplaintHandler;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.ibm.faces.component.html.HtmlGraphicImageEx;

/**
 * @author aeast
 * 
 */
public class DelissueComplete extends PageCodeBase {

	protected HtmlOutputText mainHeader;
	protected HtmlPanelGrid mainGrid;
	protected HtmlScriptCollector scriptCollector1;
	protected HtmlOutputText textHeader1;
	protected HtmlOutputText textHeader2;
	protected HtmlPanelGrid gridAccountInformation;
	protected HtmlOutputText textDateLabel;
	protected HtmlOutputText textDateOfToday;
	protected HtmlOutputText textSubStartDateLabel;
	protected HtmlOutputText textStartDateOfSub;
	protected HtmlPanelGrid gridAccountInformationDelivery;
	protected HtmlOutputText textDeliveryNameLabel;
	protected HtmlOutputText textDeliveryName;
	protected HtmlOutputText textDeliveryCompanyNameLabel;
	protected HtmlOutputText textDeliveryCompanyName;
	protected HtmlOutputText textDeliveryAddr1Label;
	protected HtmlOutputText textDeliveryAddr1;
	protected HtmlOutputText textDeliveryAddr2Label;
	protected HtmlOutputText textDeliveryAddr2;
	protected ComplaintHandler complaintHandler;
	protected HtmlOutputText textHeader3;
	protected HtmlOutputLinkEx linkExReadNowLink_2;
	protected HtmlOutputLinkEx linkExReadNowLink;
	protected HtmlGraphicImageEx imageExReadNowButtonImage;
	protected HtmlOutputText textEEFAQtext;
	protected HtmlPanelGrid notExpiredAccountGrid;
	protected HtmlJspPanel eReaderLinkPanel;
	protected HtmlOutputLinkEx linkExEEFAQ1;

	protected HtmlOutputText getMainHeader() {
		if (mainHeader == null) {
			mainHeader = (HtmlOutputText) findComponentInRoot("mainHeader");
		}
		return mainHeader;
	}

	protected HtmlPanelGrid getMainGrid() {
		if (mainGrid == null) {
			mainGrid = (HtmlPanelGrid) findComponentInRoot("mainGrid");
		}
		return mainGrid;
	}

	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlOutputText getTextHeader1() {
		if (textHeader1 == null) {
			textHeader1 = (HtmlOutputText) findComponentInRoot("textHeader1");
		}
		return textHeader1;
	}

	protected HtmlOutputText getTextHeader2() {
		if (textHeader2 == null) {
			textHeader2 = (HtmlOutputText) findComponentInRoot("textHeader2");
		}
		return textHeader2;
	}

	protected HtmlPanelGrid getGridAccountInformation() {
		if (gridAccountInformation == null) {
			gridAccountInformation = (HtmlPanelGrid) findComponentInRoot("gridAccountInformation");
		}
		return gridAccountInformation;
	}

	protected HtmlOutputText getTextDateLabel() {
		if (textDateLabel == null) {
			textDateLabel = (HtmlOutputText) findComponentInRoot("textDateLabel");
		}
		return textDateLabel;
	}

	protected HtmlOutputText getTextDateOfToday() {
		if (textDateOfToday == null) {
			textDateOfToday = (HtmlOutputText) findComponentInRoot("textDateOfToday");
		}
		return textDateOfToday;
	}

	protected HtmlOutputText getTextSubStartDateLabel() {
		if (textSubStartDateLabel == null) {
			textSubStartDateLabel = (HtmlOutputText) findComponentInRoot("textSubStartDateLabel");
		}
		return textSubStartDateLabel;
	}

	protected HtmlOutputText getTextStartDateOfSub() {
		if (textStartDateOfSub == null) {
			textStartDateOfSub = (HtmlOutputText) findComponentInRoot("textStartDateOfSub");
		}
		return textStartDateOfSub;
	}

	protected HtmlPanelGrid getGridAccountInformationDelivery() {
		if (gridAccountInformationDelivery == null) {
			gridAccountInformationDelivery = (HtmlPanelGrid) findComponentInRoot("gridAccountInformationDelivery");
		}
		return gridAccountInformationDelivery;
	}

	protected HtmlOutputText getTextDeliveryNameLabel() {
		if (textDeliveryNameLabel == null) {
			textDeliveryNameLabel = (HtmlOutputText) findComponentInRoot("textDeliveryNameLabel");
		}
		return textDeliveryNameLabel;
	}

	protected HtmlOutputText getTextDeliveryName() {
		if (textDeliveryName == null) {
			textDeliveryName = (HtmlOutputText) findComponentInRoot("textDeliveryName");
		}
		return textDeliveryName;
	}

	protected HtmlOutputText getTextDeliveryCompanyNameLabel() {
		if (textDeliveryCompanyNameLabel == null) {
			textDeliveryCompanyNameLabel = (HtmlOutputText) findComponentInRoot("textDeliveryCompanyNameLabel");
		}
		return textDeliveryCompanyNameLabel;
	}

	protected HtmlOutputText getTextDeliveryCompanyName() {
		if (textDeliveryCompanyName == null) {
			textDeliveryCompanyName = (HtmlOutputText) findComponentInRoot("textDeliveryCompanyName");
		}
		return textDeliveryCompanyName;
	}

	protected HtmlOutputText getTextDeliveryAddr1Label() {
		if (textDeliveryAddr1Label == null) {
			textDeliveryAddr1Label = (HtmlOutputText) findComponentInRoot("textDeliveryAddr1Label");
		}
		return textDeliveryAddr1Label;
	}

	protected HtmlOutputText getTextDeliveryAddr1() {
		if (textDeliveryAddr1 == null) {
			textDeliveryAddr1 = (HtmlOutputText) findComponentInRoot("textDeliveryAddr1");
		}
		return textDeliveryAddr1;
	}

	protected HtmlOutputText getTextDeliveryAddr2Label() {
		if (textDeliveryAddr2Label == null) {
			textDeliveryAddr2Label = (HtmlOutputText) findComponentInRoot("textDeliveryAddr2Label");
		}
		return textDeliveryAddr2Label;
	}

	protected HtmlOutputText getTextDeliveryAddr2() {
		if (textDeliveryAddr2 == null) {
			textDeliveryAddr2 = (HtmlOutputText) findComponentInRoot("textDeliveryAddr2");
		}
		return textDeliveryAddr2;
	}

	/**
	 * @managed-bean true
	 */
	protected ComplaintHandler getComplaintHandler() {
		if (complaintHandler == null) {
			complaintHandler = (ComplaintHandler) getManagedBean("complaintHandler");
		}
		return complaintHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setComplaintHandler(ComplaintHandler complaintHandler) {
		this.complaintHandler = complaintHandler;
	}

	protected HtmlOutputText getTextHeader3() {
		if (textHeader3 == null) {
			textHeader3 = (HtmlOutputText) findComponentInRoot("textHeader3");
		}
		return textHeader3;
	}

	protected HtmlOutputLinkEx getLinkExReadNowLink_2() {
		if (linkExReadNowLink_2 == null) {
			linkExReadNowLink_2 = (HtmlOutputLinkEx) findComponentInRoot("linkExReadNowLink_2");
		}
		return linkExReadNowLink_2;
	}

	protected HtmlOutputLinkEx getLinkExReadNowLink() {
		if (linkExReadNowLink == null) {
			linkExReadNowLink = (HtmlOutputLinkEx) findComponentInRoot("linkExReadNowLink");
		}
		return linkExReadNowLink;
	}

	protected HtmlGraphicImageEx getImageExReadNowButtonImage() {
		if (imageExReadNowButtonImage == null) {
			imageExReadNowButtonImage = (HtmlGraphicImageEx) findComponentInRoot("imageExReadNowButtonImage");
		}
		return imageExReadNowButtonImage;
	}

	protected HtmlOutputText getTextEEFAQtext() {
		if (textEEFAQtext == null) {
			textEEFAQtext = (HtmlOutputText) findComponentInRoot("textEEFAQtext");
		}
		return textEEFAQtext;
	}

	protected HtmlPanelGrid getNotExpiredAccountGrid() {
		if (notExpiredAccountGrid == null) {
			notExpiredAccountGrid = (HtmlPanelGrid) findComponentInRoot("notExpiredAccountGrid");
		}
		return notExpiredAccountGrid;
	}

	protected HtmlJspPanel getEReaderLinkPanel() {
		if (eReaderLinkPanel == null) {
			eReaderLinkPanel = (HtmlJspPanel) findComponentInRoot("eReaderLinkPanel");
		}
		return eReaderLinkPanel;
	}

	protected HtmlOutputLinkEx getLinkExEEFAQ1() {
		if (linkExEEFAQ1 == null) {
			linkExEEFAQ1 = (HtmlOutputLinkEx) findComponentInRoot("linkExEEFAQ1");
		}
		return linkExEEFAQ1;
	}

}