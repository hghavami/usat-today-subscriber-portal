/**
 * 
 */
package pagecode.account.accounthistory;

import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.context.FacesContext;

import pagecode.PageCodeBase;

import com.ibm.faces.component.html.HtmlGraphicImageEx;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.esub.ncs.handlers.CustomerHandler;

/**
 * @author aeast
 * 
 */
public class AccountSummary extends PageCodeBase {

	protected HtmlOutputText mainHeader;
	protected CustomerHandler customerHandler;
	protected HtmlPanelGrid gridAccountInformation;
	protected HtmlOutputText textAccountLabel;
	protected HtmlOutputText textAccountNumber;
	protected HtmlOutputText textNameLabel;
	protected HtmlOutputText text3;
	protected HtmlOutputText textStartDate;
	protected HtmlPanelGroup groupSpecialOfferGroupBox;
	protected HtmlPanelGrid gridSpecialOfferLayoutGrid;
	protected HtmlOutputLinkEx linkExCloseSpecialOffer;
	protected HtmlOutputText textCloseSpecialOfferText;
	protected HtmlGraphicImageEx imageExSpecialOfferImage;
	protected HtmlJspPanel jspPanelPopOverlayPanel;
	protected HtmlOutputLinkEx linkExSpecialOfferLink;
	protected HtmlInputHidden showOverlayPopUp;
	protected HtmlOutputText textAccountProductName;
	protected HtmlOutputText textAccountProdName;
	protected HtmlOutputText textCompanyNameLabel;
	protected HtmlOutputText textCompanyName;
	protected HtmlOutputText textAddress1Label;
	protected HtmlOutputText textStartDateLabel;
	protected HtmlPanelGrid deliverAddrGrid;
	protected HtmlOutputText textAddress2;
	protected HtmlOutputText textAddressCSZ;
	protected HtmlOutputText textAddress1;
	protected HtmlOutputText textPhoneLabel;
	protected HtmlOutputText textPhoneNum;
	protected HtmlOutputLinkEx linkExReadNowLink_2;
	protected HtmlOutputLinkEx linkExReadNowLink;
	protected HtmlGraphicImageEx imageExReadNowButtonImage;
	protected HtmlGraphicImageEx imageExRenewalButton;
	protected HtmlOutputLinkEx linkExRenewalLink;
	protected HtmlPanelGrid gridUTCompanionGrid;
	protected HtmlPanelGrid notExpiredAccountGrid;
	protected HtmlJspPanel eReaderLinkPanel;
	protected HtmlOutputLinkEx linkExEEFAQ1;
	protected HtmlOutputText textEEFAQtext;
	protected HtmlJspPanel RenewRequiredPanel;
	protected HtmlPanelGrid gridBWCompanionGrid;
	protected HtmlPanelGrid notExpiredAccountGrid2;
	protected HtmlOutputLinkEx linkExReadNowLink_22;
	protected HtmlJspPanel eReaderLinkBWPanel;
	protected HtmlOutputLinkEx linkExReadNowBWLink;
	protected HtmlGraphicImageEx imageExReadNowBWButtonImage;
	protected HtmlOutputText textBWInfoText1;
	protected HtmlJspPanel renewBWRequiredPanel;
	protected HtmlPanelGrid gridPromoSpotOutterPanel1;
	protected HtmlPanelGroup groupPromoHeaderGroup1;
	protected HtmlOutputText textPromoHeaderText;
	protected HtmlPanelGrid gridInnerPromoSpots;
	protected HtmlGraphicImageEx imageExLowLeft;
	protected HtmlOutputLinkEx linkExLowLeftPromoImage1;
	protected HtmlGraphicImageEx imageExLowLeftClickable;
	protected HtmlGraphicImageEx imageExLowMiddle;
	protected HtmlOutputLinkEx linkExLowMiddlePromoImage1;
	protected HtmlGraphicImageEx imageExLowMiddleClickable;
	protected HtmlGraphicImageEx imageExLowRight;
	protected HtmlOutputLinkEx linkExLowRightPromoImage1;
	protected HtmlGraphicImageEx imageExLowRightClickable;
	protected HtmlScriptCollector scriptCollector1;
	protected HtmlMessages messages1;
	protected HtmlOutputLinkEx linkExOpenDesktopVersion;
	protected HtmlOutputText text1111;
	protected HtmlOutputText text2;
	protected HtmlOutputLinkEx linkExOpenMobileVersion;
	protected HtmlOutputText text1;
	protected HtmlOutputText textExpirationDate;
	protected HtmlOutputText textEZPayLabel;
	protected HtmlOutputText textUpdateEmailPreferenceLinkText;
	protected HtmlOutputText textOnEZPay;
	protected HtmlOutputText textAPlaceHolder;
	protected HtmlOutputLinkEx linkExUpdateEmailPreferencesLink;
	protected HtmlOutputText textSubscriptionEnd;
	protected HtmlOutputText getMainHeader() {
		if (mainHeader == null) {
			mainHeader = (HtmlOutputText) findComponentInRoot("mainHeader");
		}
		return mainHeader;
	}

	/**
	 * @managed-bean true
	 */
	protected CustomerHandler getCustomerHandler() {
		if (customerHandler == null) {
			customerHandler = (CustomerHandler) getManagedBean("customerHandler");
		}
		return customerHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setCustomerHandler(CustomerHandler customerHandler) {
		this.customerHandler = customerHandler;
	}

	protected HtmlPanelGrid getGridAccountInformation() {
		if (gridAccountInformation == null) {
			gridAccountInformation = (HtmlPanelGrid) findComponentInRoot("gridAccountInformation");
		}
		return gridAccountInformation;
	}

	protected HtmlOutputText getTextAccountLabel() {
		if (textAccountLabel == null) {
			textAccountLabel = (HtmlOutputText) findComponentInRoot("textAccountLabel");
		}
		return textAccountLabel;
	}

	protected HtmlOutputText getTextAccountNumber() {
		if (textAccountNumber == null) {
			textAccountNumber = (HtmlOutputText) findComponentInRoot("textAccountNumber");
		}
		return textAccountNumber;
	}

	protected HtmlOutputText getTextNameLabel() {
		if (textNameLabel == null) {
			textNameLabel = (HtmlOutputText) findComponentInRoot("textNameLabel");
		}
		return textNameLabel;
	}

	protected HtmlOutputText getText3() {
		if (text3 == null) {
			text3 = (HtmlOutputText) findComponentInRoot("text3");
		}
		return text3;
	}

	protected HtmlOutputText getTextStartDate() {
		if (textStartDate == null) {
			textStartDate = (HtmlOutputText) findComponentInRoot("textStartDate");
		}
		return textStartDate;
	}

	protected HtmlPanelGroup getGroupSpecialOfferGroupBox() {
		if (groupSpecialOfferGroupBox == null) {
			groupSpecialOfferGroupBox = (HtmlPanelGroup) findComponentInRoot("groupSpecialOfferGroupBox");
		}
		return groupSpecialOfferGroupBox;
	}

	protected HtmlPanelGrid getGridSpecialOfferLayoutGrid() {
		if (gridSpecialOfferLayoutGrid == null) {
			gridSpecialOfferLayoutGrid = (HtmlPanelGrid) findComponentInRoot("gridSpecialOfferLayoutGrid");
		}
		return gridSpecialOfferLayoutGrid;
	}

	protected HtmlOutputLinkEx getLinkExCloseSpecialOffer() {
		if (linkExCloseSpecialOffer == null) {
			linkExCloseSpecialOffer = (HtmlOutputLinkEx) findComponentInRoot("linkExCloseSpecialOffer");
		}
		return linkExCloseSpecialOffer;
	}

	protected HtmlOutputText getTextCloseSpecialOfferText() {
		if (textCloseSpecialOfferText == null) {
			textCloseSpecialOfferText = (HtmlOutputText) findComponentInRoot("textCloseSpecialOfferText");
		}
		return textCloseSpecialOfferText;
	}

	protected HtmlGraphicImageEx getImageExSpecialOfferImage() {
		if (imageExSpecialOfferImage == null) {
			imageExSpecialOfferImage = (HtmlGraphicImageEx) findComponentInRoot("imageExSpecialOfferImage");
		}
		return imageExSpecialOfferImage;
	}

	protected HtmlJspPanel getJspPanelPopOverlayPanel() {
		if (jspPanelPopOverlayPanel == null) {
			jspPanelPopOverlayPanel = (HtmlJspPanel) findComponentInRoot("jspPanelPopOverlayPanel");
		}
		return jspPanelPopOverlayPanel;
	}

	protected HtmlOutputLinkEx getLinkExSpecialOfferLink() {
		if (linkExSpecialOfferLink == null) {
			linkExSpecialOfferLink = (HtmlOutputLinkEx) findComponentInRoot("linkExSpecialOfferLink");
		}
		return linkExSpecialOfferLink;
	}

	protected HtmlInputHidden getShowOverlayPopUp() {
		if (showOverlayPopUp == null) {
			showOverlayPopUp = (HtmlInputHidden) findComponentInRoot("showOverlayPopUp");
		}
		return showOverlayPopUp;
	}

	protected HtmlOutputText getTextAccountProductName() {
		if (textAccountProductName == null) {
			textAccountProductName = (HtmlOutputText) findComponentInRoot("textAccountProductName");
		}
		return textAccountProductName;
	}

	protected HtmlOutputText getTextAccountProdName() {
		if (textAccountProdName == null) {
			textAccountProdName = (HtmlOutputText) findComponentInRoot("textAccountProdName");
		}
		return textAccountProdName;
	}

	protected HtmlOutputText getTextCompanyNameLabel() {
		if (textCompanyNameLabel == null) {
			textCompanyNameLabel = (HtmlOutputText) findComponentInRoot("textCompanyNameLabel");
		}
		return textCompanyNameLabel;
	}

	protected HtmlOutputText getTextCompanyName() {
		if (textCompanyName == null) {
			textCompanyName = (HtmlOutputText) findComponentInRoot("textCompanyName");
		}
		return textCompanyName;
	}

	protected HtmlOutputText getTextAddress1Label() {
		if (textAddress1Label == null) {
			textAddress1Label = (HtmlOutputText) findComponentInRoot("textAddress1Label");
		}
		return textAddress1Label;
	}

	protected HtmlOutputText getTextStartDateLabel() {
		if (textStartDateLabel == null) {
			textStartDateLabel = (HtmlOutputText) findComponentInRoot("textStartDateLabel");
		}
		return textStartDateLabel;
	}

	protected HtmlPanelGrid getDeliverAddrGrid() {
		if (deliverAddrGrid == null) {
			deliverAddrGrid = (HtmlPanelGrid) findComponentInRoot("deliverAddrGrid");
		}
		return deliverAddrGrid;
	}

	protected HtmlOutputText getTextAddress2() {
		if (textAddress2 == null) {
			textAddress2 = (HtmlOutputText) findComponentInRoot("textAddress2");
		}
		return textAddress2;
	}

	protected HtmlOutputText getTextAddressCSZ() {
		if (textAddressCSZ == null) {
			textAddressCSZ = (HtmlOutputText) findComponentInRoot("textAddressCSZ");
		}
		return textAddressCSZ;
	}

	protected HtmlOutputText getTextAddress1() {
		if (textAddress1 == null) {
			textAddress1 = (HtmlOutputText) findComponentInRoot("textAddress1");
		}
		return textAddress1;
	}

	protected HtmlOutputText getTextPhoneLabel() {
		if (textPhoneLabel == null) {
			textPhoneLabel = (HtmlOutputText) findComponentInRoot("textPhoneLabel");
		}
		return textPhoneLabel;
	}

	protected HtmlOutputText getTextPhoneNum() {
		if (textPhoneNum == null) {
			textPhoneNum = (HtmlOutputText) findComponentInRoot("textPhoneNum");
		}
		return textPhoneNum;
	}

	protected HtmlOutputLinkEx getLinkExReadNowLink_2() {
		if (linkExReadNowLink_2 == null) {
			linkExReadNowLink_2 = (HtmlOutputLinkEx) findComponentInRoot("linkExReadNowLink_2");
		}
		return linkExReadNowLink_2;
	}

	protected HtmlOutputLinkEx getLinkExReadNowLink() {
		if (linkExReadNowLink == null) {
			linkExReadNowLink = (HtmlOutputLinkEx) findComponentInRoot("linkExReadNowLink");
		}
		return linkExReadNowLink;
	}

	protected HtmlGraphicImageEx getImageExReadNowButtonImage() {
		if (imageExReadNowButtonImage == null) {
			imageExReadNowButtonImage = (HtmlGraphicImageEx) findComponentInRoot("imageExReadNowButtonImage");
		}
		return imageExReadNowButtonImage;
	}

	protected HtmlGraphicImageEx getImageExRenewalButton() {
		if (imageExRenewalButton == null) {
			imageExRenewalButton = (HtmlGraphicImageEx) findComponentInRoot("imageExRenewalButton");
		}
		return imageExRenewalButton;
	}

	protected HtmlOutputLinkEx getLinkExRenewalLink() {
		if (linkExRenewalLink == null) {
			linkExRenewalLink = (HtmlOutputLinkEx) findComponentInRoot("linkExRenewalLink");
		}
		return linkExRenewalLink;
	}

	protected HtmlPanelGrid getGridUTCompanionGrid() {
		if (gridUTCompanionGrid == null) {
			gridUTCompanionGrid = (HtmlPanelGrid) findComponentInRoot("gridUTCompanionGrid");
		}
		return gridUTCompanionGrid;
	}

	protected HtmlPanelGrid getNotExpiredAccountGrid() {
		if (notExpiredAccountGrid == null) {
			notExpiredAccountGrid = (HtmlPanelGrid) findComponentInRoot("notExpiredAccountGrid");
		}
		return notExpiredAccountGrid;
	}

	protected HtmlOutputLinkEx getLinkExEEFAQ1() {
		if (linkExEEFAQ1 == null) {
			linkExEEFAQ1 = (HtmlOutputLinkEx) findComponentInRoot("linkExEEFAQ1");
		}
		return linkExEEFAQ1;
	}

	protected HtmlOutputText getTextEEFAQtext() {
		if (textEEFAQtext == null) {
			textEEFAQtext = (HtmlOutputText) findComponentInRoot("textEEFAQtext");
		}
		return textEEFAQtext;
	}

	protected HtmlPanelGrid getGridBWCompanionGrid() {
		if (gridBWCompanionGrid == null) {
			gridBWCompanionGrid = (HtmlPanelGrid) findComponentInRoot("gridBWCompanionGrid");
		}
		return gridBWCompanionGrid;
	}

	protected HtmlPanelGrid getNotExpiredAccountGrid2() {
		if (notExpiredAccountGrid2 == null) {
			notExpiredAccountGrid2 = (HtmlPanelGrid) findComponentInRoot("notExpiredAccountGrid2");
		}
		return notExpiredAccountGrid2;
	}

	protected HtmlOutputLinkEx getLinkExReadNowLink_22() {
		if (linkExReadNowLink_22 == null) {
			linkExReadNowLink_22 = (HtmlOutputLinkEx) findComponentInRoot("linkExReadNowLink_22");
		}
		return linkExReadNowLink_22;
	}

	protected HtmlOutputLinkEx getLinkExReadNowBWLink() {
		if (linkExReadNowBWLink == null) {
			linkExReadNowBWLink = (HtmlOutputLinkEx) findComponentInRoot("linkExReadNowBWLink");
		}
		return linkExReadNowBWLink;
	}

	protected HtmlGraphicImageEx getImageExReadNowBWButtonImage() {
		if (imageExReadNowBWButtonImage == null) {
			imageExReadNowBWButtonImage = (HtmlGraphicImageEx) findComponentInRoot("imageExReadNowBWButtonImage");
		}
		return imageExReadNowBWButtonImage;
	}

	protected HtmlOutputText getTextBWInfoText1() {
		if (textBWInfoText1 == null) {
			textBWInfoText1 = (HtmlOutputText) findComponentInRoot("textBWInfoText1");
		}
		return textBWInfoText1;
	}

	protected HtmlJspPanel getRenewBWRequiredPanel() {
		if (renewBWRequiredPanel == null) {
			renewBWRequiredPanel = (HtmlJspPanel) findComponentInRoot("renewBWRequiredPanel");
		}
		return renewBWRequiredPanel;
	}

	protected HtmlPanelGrid getGridPromoSpotOutterPanel1() {
		if (gridPromoSpotOutterPanel1 == null) {
			gridPromoSpotOutterPanel1 = (HtmlPanelGrid) findComponentInRoot("gridPromoSpotOutterPanel1");
		}
		return gridPromoSpotOutterPanel1;
	}

	protected HtmlPanelGroup getGroupPromoHeaderGroup1() {
		if (groupPromoHeaderGroup1 == null) {
			groupPromoHeaderGroup1 = (HtmlPanelGroup) findComponentInRoot("groupPromoHeaderGroup1");
		}
		return groupPromoHeaderGroup1;
	}

	protected HtmlOutputText getTextPromoHeaderText() {
		if (textPromoHeaderText == null) {
			textPromoHeaderText = (HtmlOutputText) findComponentInRoot("textPromoHeaderText");
		}
		return textPromoHeaderText;
	}

	protected HtmlPanelGrid getGridInnerPromoSpots() {
		if (gridInnerPromoSpots == null) {
			gridInnerPromoSpots = (HtmlPanelGrid) findComponentInRoot("gridInnerPromoSpots");
		}
		return gridInnerPromoSpots;
	}

	protected HtmlGraphicImageEx getImageExLowLeft() {
		if (imageExLowLeft == null) {
			imageExLowLeft = (HtmlGraphicImageEx) findComponentInRoot("imageExLowLeft");
		}
		return imageExLowLeft;
	}

	protected HtmlOutputLinkEx getLinkExLowLeftPromoImage1() {
		if (linkExLowLeftPromoImage1 == null) {
			linkExLowLeftPromoImage1 = (HtmlOutputLinkEx) findComponentInRoot("linkExLowLeftPromoImage1");
		}
		return linkExLowLeftPromoImage1;
	}

	protected HtmlGraphicImageEx getImageExLowLeftClickable() {
		if (imageExLowLeftClickable == null) {
			imageExLowLeftClickable = (HtmlGraphicImageEx) findComponentInRoot("imageExLowLeftClickable");
		}
		return imageExLowLeftClickable;
	}

	protected HtmlGraphicImageEx getImageExLowMiddle() {
		if (imageExLowMiddle == null) {
			imageExLowMiddle = (HtmlGraphicImageEx) findComponentInRoot("imageExLowMiddle");
		}
		return imageExLowMiddle;
	}

	protected HtmlOutputLinkEx getLinkExLowMiddlePromoImage1() {
		if (linkExLowMiddlePromoImage1 == null) {
			linkExLowMiddlePromoImage1 = (HtmlOutputLinkEx) findComponentInRoot("linkExLowMiddlePromoImage1");
		}
		return linkExLowMiddlePromoImage1;
	}

	protected HtmlGraphicImageEx getImageExLowMiddleClickable() {
		if (imageExLowMiddleClickable == null) {
			imageExLowMiddleClickable = (HtmlGraphicImageEx) findComponentInRoot("imageExLowMiddleClickable");
		}
		return imageExLowMiddleClickable;
	}

	protected HtmlGraphicImageEx getImageExLowRight() {
		if (imageExLowRight == null) {
			imageExLowRight = (HtmlGraphicImageEx) findComponentInRoot("imageExLowRight");
		}
		return imageExLowRight;
	}

	protected HtmlOutputLinkEx getLinkExLowRightPromoImage1() {
		if (linkExLowRightPromoImage1 == null) {
			linkExLowRightPromoImage1 = (HtmlOutputLinkEx) findComponentInRoot("linkExLowRightPromoImage1");
		}
		return linkExLowRightPromoImage1;
	}

	protected HtmlGraphicImageEx getImageExLowRightClickable() {
		if (imageExLowRightClickable == null) {
			imageExLowRightClickable = (HtmlGraphicImageEx) findComponentInRoot("imageExLowRightClickable");
		}
		return imageExLowRightClickable;
	}

	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

	public void onPageLoadBegin(FacesContext facescontext) {
		// Type Java code to handle page load begin event here

		// void <method>(FacesContext facescontext)
		CustomerHandler ch = this.getCustomerHandler();

		if (ch.getCustomer().getCurrentAccount() == null) {
			System.out.println("Current account not set in Account summary page.");
		}
	}

	protected HtmlOutputLinkEx getLinkExOpenDesktopVersion() {
		if (linkExOpenDesktopVersion == null) {
			linkExOpenDesktopVersion = (HtmlOutputLinkEx) findComponentInRoot("linkExOpenDesktopVersion");
		}
		return linkExOpenDesktopVersion;
	}

	protected HtmlOutputText getText1111() {
		if (text1111 == null) {
			text1111 = (HtmlOutputText) findComponentInRoot("text1111");
		}
		return text1111;
	}

	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected HtmlOutputLinkEx getLinkExOpenMobileVersion() {
		if (linkExOpenMobileVersion == null) {
			linkExOpenMobileVersion = (HtmlOutputLinkEx) findComponentInRoot("linkExOpenMobileVersion");
		}
		return linkExOpenMobileVersion;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlOutputText getTextExpirationDate() {
		if (textExpirationDate == null) {
			textExpirationDate = (HtmlOutputText) findComponentInRoot("textExpirationDate");
		}
		return textExpirationDate;
	}

	protected HtmlOutputText getTextEZPayLabel() {
		if (textEZPayLabel == null) {
			textEZPayLabel = (HtmlOutputText) findComponentInRoot("textEZPayLabel");
		}
		return textEZPayLabel;
	}

	protected HtmlOutputText getTextUpdateEmailPreferenceLinkText() {
		if (textUpdateEmailPreferenceLinkText == null) {
			textUpdateEmailPreferenceLinkText = (HtmlOutputText) findComponentInRoot("textUpdateEmailPreferenceLinkText");
		}
		return textUpdateEmailPreferenceLinkText;
	}

	protected HtmlOutputText getTextOnEZPay() {
		if (textOnEZPay == null) {
			textOnEZPay = (HtmlOutputText) findComponentInRoot("textOnEZPay");
		}
		return textOnEZPay;
	}

	protected HtmlOutputText getTextAPlaceHolder() {
		if (textAPlaceHolder == null) {
			textAPlaceHolder = (HtmlOutputText) findComponentInRoot("textAPlaceHolder");
		}
		return textAPlaceHolder;
	}

	protected HtmlOutputLinkEx getLinkExUpdateEmailPreferencesLink() {
		if (linkExUpdateEmailPreferencesLink == null) {
			linkExUpdateEmailPreferencesLink = (HtmlOutputLinkEx) findComponentInRoot("linkExUpdateEmailPreferencesLink");
		}
		return linkExUpdateEmailPreferencesLink;
	}

	protected HtmlOutputText getTextSubscriptionEnd() {
		if (textSubscriptionEnd == null) {
			textSubscriptionEnd = (HtmlOutputText) findComponentInRoot("textSubscriptionEnd");
		}
		return textSubscriptionEnd;
	}

}