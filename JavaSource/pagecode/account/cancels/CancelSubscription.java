/**
 * 
 */
package pagecode.account.cancels;

import java.util.ArrayList;
import java.util.Iterator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlSelectOneListbox;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import pagecode.PageCodeBase;

import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.business.interfaces.customer.CustomerIntf;
import com.usatoday.business.interfaces.subscriptionTransactions.ExtranetSubscriberTransactionIntf;
import com.usatoday.businessObjects.customer.CancelSubscriptionBO;
import com.usatoday.businessObjects.subscriptionTransactions.SubscriptionTransactionFactory;
import com.usatoday.esub.ncs.handlers.CancelsHandler;
import com.usatoday.esub.ncs.handlers.CustomerHandler;
import com.usatoday.integration.SubscriberTransactionDAO;
import com.ibm.faces.component.html.HtmlInputHelperAssist;

/**
 * @author hghavami
 * 
 */
public class CancelSubscription extends PageCodeBase {

	protected HtmlScriptCollector scriptCollector1;
	protected HtmlOutputText text1;
	protected HtmlForm form1;
	protected HtmlPanelFormBox formBox1;
	protected HtmlPanelGrid grid1;
	protected HtmlForm formMainForm;
	protected HtmlPanelGrid gridMainBodyGrid;
	protected HtmlJspPanel jspPanelHeaderPanel;
	protected HtmlOutputLinkEx linkExNavGroup2International;
	protected HtmlOutputText textNavGroup1LinkLabelInternational;
	protected HtmlOutputText textNavGroup1LinkLabelKindle;
	protected HtmlOutputLinkEx linkExNavGroup2Kindle;
	protected HtmlOutputText AccountNumber;
	protected CustomerHandler customerHandler;
	protected HtmlOutputText ProductNameLabel;
	protected HtmlOutputText AccountNumberLabel;
	protected HtmlOutputText ProductName;
	protected HtmlSelectOneListbox CancelSubReasonList;
	protected HtmlOutputText CancelSubReasonLabel;
	protected UISelectItems selectItems2;
	protected HtmlOutputText CommentLabel;
	protected HtmlInputTextarea Comment;
	protected CancelsHandler cancelsHandler;
	protected HtmlOutputText CancelSubReason;
	protected HtmlOutputText cancelComment;
	protected HtmlOutputText CancelHeadingText;
	protected HtmlInputText Comment1;
	protected HtmlInputText Comment2;
	protected HtmlInputText Comment3;
	protected HtmlOutputText expireDate2;
	protected HtmlCommandExButton buttonSubmitCancelSubscription;
	protected HtmlOutputText CancelHeadingText1;
	protected HtmlOutputText CancelHeadingText2;
	protected HtmlOutputText expireDate1;
	protected HtmlOutputText companyNameLabel;
	protected HtmlOutputText companyName;
	protected HtmlOutputText subscriberNameLabel;
	protected HtmlOutputText subscriberFirstName;
	protected HtmlOutputText subscriberAddressLabel;
	protected HtmlOutputText address1;
	protected HtmlOutputText address2;
	protected HtmlOutputText city;
	protected HtmlOutputText homePhoneLabel;
	protected HtmlOutputText homePhone;
	protected HtmlOutputText startDateLabel;
	protected HtmlOutputText startDate;
	protected HtmlOutputText expireDateLabel;
	protected HtmlOutputText expireDate;
	protected HtmlOutputText ezPayLabel;
	protected HtmlOutputText ezPay;
	protected HtmlOutputText numCopiesLabel;
	protected HtmlOutputText numCopies;
	protected UISelectItems selectItems1;
	protected HtmlOutputText subscriberLastName;
	protected HtmlOutputText aptSuite;
	protected HtmlOutputText state;
	protected HtmlOutputText zipCode;
	protected HtmlInputHelperAssist assist1;
	protected HtmlInputHelperAssist assist2;
	protected HtmlInputHelperAssist assist3;
	protected HtmlOutputText mainHeader;

	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlForm getForm1() {
		if (form1 == null) {
			form1 = (HtmlForm) findComponentInRoot("form1");
		}
		return form1;
	}

	protected HtmlPanelFormBox getFormBox1() {
		if (formBox1 == null) {
			formBox1 = (HtmlPanelFormBox) findComponentInRoot("formBox1");
		}
		return formBox1;
	}

	protected HtmlPanelGrid getGrid1() {
		if (grid1 == null) {
			grid1 = (HtmlPanelGrid) findComponentInRoot("grid1");
		}
		return grid1;
	}

	protected HtmlForm getFormMainForm() {
		if (formMainForm == null) {
			formMainForm = (HtmlForm) findComponentInRoot("formMainForm");
		}
		return formMainForm;
	}

	protected HtmlPanelGrid getGridMainBodyGrid() {
		if (gridMainBodyGrid == null) {
			gridMainBodyGrid = (HtmlPanelGrid) findComponentInRoot("gridMainBodyGrid");
		}
		return gridMainBodyGrid;
	}

	protected HtmlJspPanel getJspPanelHeaderPanel() {
		if (jspPanelHeaderPanel == null) {
			jspPanelHeaderPanel = (HtmlJspPanel) findComponentInRoot("jspPanelHeaderPanel");
		}
		return jspPanelHeaderPanel;
	}

	protected HtmlOutputLinkEx getLinkExNavGroup2International() {
		if (linkExNavGroup2International == null) {
			linkExNavGroup2International = (HtmlOutputLinkEx) findComponentInRoot("linkExNavGroup2International");
		}
		return linkExNavGroup2International;
	}

	protected HtmlOutputText getTextNavGroup1LinkLabelInternational() {
		if (textNavGroup1LinkLabelInternational == null) {
			textNavGroup1LinkLabelInternational = (HtmlOutputText) findComponentInRoot("textNavGroup1LinkLabelInternational");
		}
		return textNavGroup1LinkLabelInternational;
	}

	protected HtmlOutputText getTextNavGroup1LinkLabelKindle() {
		if (textNavGroup1LinkLabelKindle == null) {
			textNavGroup1LinkLabelKindle = (HtmlOutputText) findComponentInRoot("textNavGroup1LinkLabelKindle");
		}
		return textNavGroup1LinkLabelKindle;
	}

	protected HtmlOutputLinkEx getLinkExNavGroup2Kindle() {
		if (linkExNavGroup2Kindle == null) {
			linkExNavGroup2Kindle = (HtmlOutputLinkEx) findComponentInRoot("linkExNavGroup2Kindle");
		}
		return linkExNavGroup2Kindle;
	}

	/**
	 * @managed-bean true
	 */
	protected CustomerHandler getCustomerHandler() {
		if (customerHandler == null) {
			customerHandler = (CustomerHandler) getManagedBean("customerHandler");
		}
		return customerHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setCustomerHandler(CustomerHandler customerHandler) {
		this.customerHandler = customerHandler;
	}

	protected UISelectItems getSelectItems2() {
		if (selectItems2 == null) {
			selectItems2 = (UISelectItems) findComponentInRoot("selectItems2");
		}
		return selectItems2;
	}

	/**
	 * @managed-bean true
	 */
	protected CancelsHandler getCancelsHandler() {
		if (cancelsHandler == null) {
			cancelsHandler = (CancelsHandler) getManagedBean("cancelsHandler");
		}
		return cancelsHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setCancelsHandler(CancelsHandler cancelsHandler) {
		this.cancelsHandler = cancelsHandler;
	}

	public void onPageLoadBegin(FacesContext facescontext) {
		// Type Java code to handle page load begin event here
		try {
			CustomerIntf cust = this.getCustomerHandler().getCustomer();
			this.getCancelsHandler().setCustomer(cust);
		} catch (Exception e) {
			System.out.println("CancelSubscription:  Failed to get customer information: " + e.getMessage());
		}
		// void <method>(FacesContext facescontext)
	}

	public String doButtonSubmitCancelSubscriptionAction() {
		// Type Java code that runs when the component is clicked
		CancelsHandler ch = this.getCancelsHandler();
		String cancelSubCode = ch.getCancelCode();
		CancelSubscriptionBO cancelSubscriptionBO = new CancelSubscriptionBO();
		ArrayList<SelectItem> al = ch.getCancelSubsReasons();
		Iterator<SelectItem> itr = al.iterator();
		// Get cancelation reason from the select item
		while (itr.hasNext()) {
			SelectItem sl = itr.next();
			if (sl.getValue().equals(cancelSubCode)) {
				ch.setCancelReason(sl.getDescription());
				cancelSubscriptionBO.setCancelSubsReasonCode(cancelSubCode);
				cancelSubscriptionBO.setCancelSubsReasonDesc(sl.getDescription());
			}
		}
		String cancelComment1 = ch.getComment1().trim().toUpperCase();
		String cancelComment2 = ch.getComment2().trim().toUpperCase();
		String cancelComment3 = ch.getComment3().trim().toUpperCase();
		cancelSubscriptionBO.setComment(cancelComment1 + " " + cancelComment2 + " " + cancelComment3);
		// Write to the database
		String transRecType = "05";
		String transType = cancelSubCode.substring(0, 1);
		String transCode = cancelSubCode.substring(1, 3);
		ExtranetSubscriberTransactionIntf trans = null;
		SubscriberTransactionDAO subscriptionDAO = new SubscriberTransactionDAO();
		CustomerIntf customer = this.getCustomerHandler().getCustomer();
		try {
			// Write a cancel transaction
			trans = SubscriptionTransactionFactory.createSubscriptionCancelTransaction(transRecType, transType, transCode, "",
					customer.getCurrentAccount());
			subscriptionDAO.insert(trans);
			// Check if needed write a IFO record for each 38 chars of comment up to 114 chars
			if (cancelComment1 != null && !cancelComment1.equals("")) {
				transRecType = "08";
				transType = "I";
				transCode = "FO";
				trans = SubscriptionTransactionFactory.createCustomerInfoTransaction(transRecType, transType, transCode,
						cancelComment1, customer.getCurrentAccount());
				subscriptionDAO.insert(trans);
			}
			if (cancelComment2 != null && !cancelComment2.equals("")) {
				transRecType = "08";
				transType = "I";
				transCode = "FO";
				trans = SubscriptionTransactionFactory.createCustomerInfoTransaction(transRecType, transType, transCode,
						cancelComment2, customer.getCurrentAccount());
				subscriptionDAO.insert(trans);
			}
			if (cancelComment3 != null && !cancelComment3.equals("")) {
				transRecType = "08";
				transType = "I";
				transCode = "FO";
				trans = SubscriptionTransactionFactory.createCustomerInfoTransaction(transRecType, transType, transCode,
						cancelComment3, customer.getCurrentAccount());
				subscriptionDAO.insert(trans);
			}
			// Send the confirmation email
			try {
				cancelSubscriptionBO.sendSubscriptionConfirmationEmail(customer);
			} catch (Exception eeee) {
			}
		} catch (Exception e) {
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage message = null;
			message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Failed to Process Cancel Subscription Request. "
					+ e.getMessage(), null);
			context.addMessage(null, message);
			// Return outcome that corresponds to a navigation rule
			return "failure";
		}

		// Return outcome that corresponds to a navigation rule
		return "success";
	}

	protected HtmlOutputText getCancelComment() {
		if (cancelComment == null) {
			cancelComment = (HtmlOutputText) findComponentInRoot("cancelComment");
		}
		return cancelComment;
	}

	protected HtmlOutputText getExpireDate2() {
		if (expireDate2 == null) {
			expireDate2 = (HtmlOutputText) findComponentInRoot("expireDate2");
		}
		return expireDate2;
	}

	protected HtmlCommandExButton getButtonSubmitCancelSubscription() {
		if (buttonSubmitCancelSubscription == null) {
			buttonSubmitCancelSubscription = (HtmlCommandExButton) findComponentInRoot("buttonSubmitCancelSubscription");
		}
		return buttonSubmitCancelSubscription;
	}

	protected HtmlOutputText getExpireDate1() {
		if (expireDate1 == null) {
			expireDate1 = (HtmlOutputText) findComponentInRoot("expireDate1");
		}
		return expireDate1;
	}

	protected HtmlOutputText getCompanyNameLabel() {
		if (companyNameLabel == null) {
			companyNameLabel = (HtmlOutputText) findComponentInRoot("companyNameLabel");
		}
		return companyNameLabel;
	}

	protected HtmlOutputText getCompanyName() {
		if (companyName == null) {
			companyName = (HtmlOutputText) findComponentInRoot("companyName");
		}
		return companyName;
	}

	protected HtmlOutputText getSubscriberNameLabel() {
		if (subscriberNameLabel == null) {
			subscriberNameLabel = (HtmlOutputText) findComponentInRoot("subscriberNameLabel");
		}
		return subscriberNameLabel;
	}

	protected HtmlOutputText getSubscriberFirstName() {
		if (subscriberFirstName == null) {
			subscriberFirstName = (HtmlOutputText) findComponentInRoot("subscriberFirstName");
		}
		return subscriberFirstName;
	}

	protected HtmlOutputText getSubscriberAddressLabel() {
		if (subscriberAddressLabel == null) {
			subscriberAddressLabel = (HtmlOutputText) findComponentInRoot("subscriberAddressLabel");
		}
		return subscriberAddressLabel;
	}

	protected HtmlOutputText getAddress1() {
		if (address1 == null) {
			address1 = (HtmlOutputText) findComponentInRoot("address1");
		}
		return address1;
	}

	protected HtmlOutputText getAddress2() {
		if (address2 == null) {
			address2 = (HtmlOutputText) findComponentInRoot("address2");
		}
		return address2;
	}

	protected HtmlOutputText getCity() {
		if (city == null) {
			city = (HtmlOutputText) findComponentInRoot("city");
		}
		return city;
	}

	protected HtmlOutputText getHomePhoneLabel() {
		if (homePhoneLabel == null) {
			homePhoneLabel = (HtmlOutputText) findComponentInRoot("homePhoneLabel");
		}
		return homePhoneLabel;
	}

	protected HtmlOutputText getHomePhone() {
		if (homePhone == null) {
			homePhone = (HtmlOutputText) findComponentInRoot("homePhone");
		}
		return homePhone;
	}

	protected HtmlOutputText getStartDateLabel() {
		if (startDateLabel == null) {
			startDateLabel = (HtmlOutputText) findComponentInRoot("startDateLabel");
		}
		return startDateLabel;
	}

	protected HtmlOutputText getStartDate() {
		if (startDate == null) {
			startDate = (HtmlOutputText) findComponentInRoot("startDate");
		}
		return startDate;
	}

	protected HtmlOutputText getExpireDateLabel() {
		if (expireDateLabel == null) {
			expireDateLabel = (HtmlOutputText) findComponentInRoot("expireDateLabel");
		}
		return expireDateLabel;
	}

	protected HtmlOutputText getExpireDate() {
		if (expireDate == null) {
			expireDate = (HtmlOutputText) findComponentInRoot("expireDate");
		}
		return expireDate;
	}

	protected HtmlOutputText getEzPayLabel() {
		if (ezPayLabel == null) {
			ezPayLabel = (HtmlOutputText) findComponentInRoot("ezPayLabel");
		}
		return ezPayLabel;
	}

	protected HtmlOutputText getEzPay() {
		if (ezPay == null) {
			ezPay = (HtmlOutputText) findComponentInRoot("ezPay");
		}
		return ezPay;
	}

	protected HtmlOutputText getNumCopiesLabel() {
		if (numCopiesLabel == null) {
			numCopiesLabel = (HtmlOutputText) findComponentInRoot("numCopiesLabel");
		}
		return numCopiesLabel;
	}

	protected HtmlOutputText getNumCopies() {
		if (numCopies == null) {
			numCopies = (HtmlOutputText) findComponentInRoot("numCopies");
		}
		return numCopies;
	}

	protected UISelectItems getSelectItems1() {
		if (selectItems1 == null) {
			selectItems1 = (UISelectItems) findComponentInRoot("selectItems1");
		}
		return selectItems1;
	}

	protected HtmlOutputText getSubscriberLastName() {
		if (subscriberLastName == null) {
			subscriberLastName = (HtmlOutputText) findComponentInRoot("subscriberLastName");
		}
		return subscriberLastName;
	}

	protected HtmlOutputText getAptSuite() {
		if (aptSuite == null) {
			aptSuite = (HtmlOutputText) findComponentInRoot("aptSuite");
		}
		return aptSuite;
	}

	protected HtmlOutputText getState() {
		if (state == null) {
			state = (HtmlOutputText) findComponentInRoot("state");
		}
		return state;
	}

	protected HtmlOutputText getZipCode() {
		if (zipCode == null) {
			zipCode = (HtmlOutputText) findComponentInRoot("zipCode");
		}
		return zipCode;
	}

	protected HtmlInputHelperAssist getAssist1() {
		if (assist1 == null) {
			assist1 = (HtmlInputHelperAssist) findComponentInRoot("assist1");
		}
		return assist1;
	}

	protected HtmlInputHelperAssist getAssist2() {
		if (assist2 == null) {
			assist2 = (HtmlInputHelperAssist) findComponentInRoot("assist2");
		}
		return assist2;
	}

	protected HtmlInputHelperAssist getAssist3() {
		if (assist3 == null) {
			assist3 = (HtmlInputHelperAssist) findComponentInRoot("assist3");
		}
		return assist3;
	}

	protected HtmlOutputText getMainHeader() {
		if (mainHeader == null) {
			mainHeader = (HtmlOutputText) findComponentInRoot("mainHeader");
		}
		return mainHeader;
	}

}