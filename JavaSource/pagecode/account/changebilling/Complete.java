/**
 * 
 */
package pagecode.account.changebilling;

import pagecode.PageCodeBase;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.context.FacesContext;

import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.usatoday.esub.ncs.handlers.ChangeBillingAddressHandler;

/**
 * @author hghavami
 * 
 */
public class Complete extends PageCodeBase {

	protected HtmlForm form1;
	protected HtmlPanelGrid grid5;
	protected HtmlOutputText AccountNumberLabel;
	protected HtmlJspPanel jspPanel1;
	protected HtmlOutputText Colon1;
	protected HtmlOutputText AccountNumber;
	protected HtmlOutputText PublicationLabel;
	protected HtmlOutputText Colon2;
	protected HtmlOutputText Publication;
	protected HtmlOutputText StopDateLabel;
	protected HtmlOutputText Colon25;
	protected HtmlOutputText StopDate;
	protected HtmlOutputText StartDateLabel;
	protected HtmlOutputText Colon3;
	protected HtmlOutputText StartDate;
	protected HtmlOutputText DeliveryInfoLabel;
	protected HtmlOutputText colon4;
	protected HtmlOutputText FullName;
	protected HtmlOutputText text5;
	protected HtmlOutputText text6;
	protected HtmlOutputText FirmName;
	protected HtmlOutputText text1;
	protected HtmlOutputText text2;
	protected HtmlOutputText StreetAddress;
	protected HtmlOutputText text3;
	protected HtmlOutputText text4;
	protected HtmlOutputText AptSuite;
	protected HtmlOutputText text7;
	protected HtmlOutputText text8;
	protected HtmlOutputText AddlAddr1;
	protected HtmlOutputText text9;
	protected HtmlOutputText text10;
	protected HtmlOutputText CityStateZip;
	protected HtmlOutputText text11;
	protected HtmlOutputText text12;
	protected HtmlOutputText HomePhone;
	protected HtmlOutputText text18;
	protected HtmlOutputText text19;
	protected HtmlOutputText text20;
	protected HtmlCommandExButton Submit;
	protected HtmlScriptCollector scriptCollector1;
	protected HtmlOutputText text13;
	protected HtmlOutputText text14;
	protected HtmlOutputText creditCardInfo;
	protected HtmlOutputText text15;
	protected HtmlOutputText text16;
	protected HtmlOutputText creditCardExp;
	protected HtmlOutputText BillingInfoLabel;
	protected HtmlPanelGrid grid1;
	protected HtmlPanelFormBox formBox1;
	protected HtmlOutputText BillingnfoLabel;
	protected HtmlOutputText BillingnfoLabel1;
	protected HtmlPanelGrid grid2;
	protected HtmlOutputText SameAsBefore;
	protected HtmlOutputText colon41;
	protected HtmlPanelGrid grid3;
	protected ChangeBillingAddressHandler changeBillingAddressHandler;

	public ChangeBillingAddressHandler getChangeBillingAddressHandler() {
		try {
			changeBillingAddressHandler = (ChangeBillingAddressHandler) getManagedBean("changeBillingAddressHandler");
		} catch (Exception e) {
			System.out.println("Complete page:  Failed to get ChangeBillingAddressHandler: " + e.getMessage());
		}
		return changeBillingAddressHandler;
	}

	protected HtmlOutputText getColon4() {
		if (colon4 == null) {
			colon4 = (HtmlOutputText) findComponentInRoot("colon4");
		}
		return colon4;
	}

	protected HtmlOutputText getColon41() {
		if (colon41 == null) {
			colon41 = (HtmlOutputText) findComponentInRoot("colon41");
		}
		return colon41;
	}

	protected HtmlOutputText getCreditCardExp() {
		if (creditCardExp == null) {
			creditCardExp = (HtmlOutputText) findComponentInRoot("creditCardExp");
		}
		return creditCardExp;
	}

	protected HtmlOutputText getCreditCardInfo() {
		if (creditCardInfo == null) {
			creditCardInfo = (HtmlOutputText) findComponentInRoot("creditCardInfo");
		}
		return creditCardInfo;
	}

	protected HtmlForm getForm1() {
		if (form1 == null) {
			form1 = (HtmlForm) findComponentInRoot("form1");
		}
		return form1;
	}

	protected HtmlPanelFormBox getFormBox1() {
		if (formBox1 == null) {
			formBox1 = (HtmlPanelFormBox) findComponentInRoot("formBox1");
		}
		return formBox1;
	}

	protected HtmlPanelGrid getGrid1() {
		if (grid1 == null) {
			grid1 = (HtmlPanelGrid) findComponentInRoot("grid1");
		}
		return grid1;
	}

	protected HtmlPanelGrid getGrid2() {
		if (grid2 == null) {
			grid2 = (HtmlPanelGrid) findComponentInRoot("grid2");
		}
		return grid2;
	}

	protected HtmlPanelGrid getGrid3() {
		if (grid3 == null) {
			grid3 = (HtmlPanelGrid) findComponentInRoot("grid3");
		}
		return grid3;
	}

	protected HtmlPanelGrid getGrid5() {
		if (grid5 == null) {
			grid5 = (HtmlPanelGrid) findComponentInRoot("grid5");
		}
		return grid5;
	}

	protected HtmlJspPanel getJspPanel1() {
		if (jspPanel1 == null) {
			jspPanel1 = (HtmlJspPanel) findComponentInRoot("jspPanel1");
		}
		return jspPanel1;
	}

	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlOutputText getText10() {
		if (text10 == null) {
			text10 = (HtmlOutputText) findComponentInRoot("text10");
		}
		return text10;
	}

	protected HtmlOutputText getText11() {
		if (text11 == null) {
			text11 = (HtmlOutputText) findComponentInRoot("text11");
		}
		return text11;
	}

	protected HtmlOutputText getText12() {
		if (text12 == null) {
			text12 = (HtmlOutputText) findComponentInRoot("text12");
		}
		return text12;
	}

	protected HtmlOutputText getText13() {
		if (text13 == null) {
			text13 = (HtmlOutputText) findComponentInRoot("text13");
		}
		return text13;
	}

	protected HtmlOutputText getText14() {
		if (text14 == null) {
			text14 = (HtmlOutputText) findComponentInRoot("text14");
		}
		return text14;
	}

	protected HtmlOutputText getText15() {
		if (text15 == null) {
			text15 = (HtmlOutputText) findComponentInRoot("text15");
		}
		return text15;
	}

	protected HtmlOutputText getText16() {
		if (text16 == null) {
			text16 = (HtmlOutputText) findComponentInRoot("text16");
		}
		return text16;
	}

	protected HtmlOutputText getText18() {
		if (text18 == null) {
			text18 = (HtmlOutputText) findComponentInRoot("text18");
		}
		return text18;
	}

	protected HtmlOutputText getText19() {
		if (text19 == null) {
			text19 = (HtmlOutputText) findComponentInRoot("text19");
		}
		return text19;
	}

	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected HtmlOutputText getText20() {
		if (text20 == null) {
			text20 = (HtmlOutputText) findComponentInRoot("text20");
		}
		return text20;
	}

	protected HtmlOutputText getText3() {
		if (text3 == null) {
			text3 = (HtmlOutputText) findComponentInRoot("text3");
		}
		return text3;
	}

	protected HtmlOutputText getText4() {
		if (text4 == null) {
			text4 = (HtmlOutputText) findComponentInRoot("text4");
		}
		return text4;
	}

	protected HtmlOutputText getText5() {
		if (text5 == null) {
			text5 = (HtmlOutputText) findComponentInRoot("text5");
		}
		return text5;
	}

	protected HtmlOutputText getText6() {
		if (text6 == null) {
			text6 = (HtmlOutputText) findComponentInRoot("text6");
		}
		return text6;
	}

	protected HtmlOutputText getText7() {
		if (text7 == null) {
			text7 = (HtmlOutputText) findComponentInRoot("text7");
		}
		return text7;
	}

	protected HtmlOutputText getText8() {
		if (text8 == null) {
			text8 = (HtmlOutputText) findComponentInRoot("text8");
		}
		return text8;
	}

	protected HtmlOutputText getText9() {
		if (text9 == null) {
			text9 = (HtmlOutputText) findComponentInRoot("text9");
		}
		return text9;
	}

	public void onPageLoadEnd(FacesContext facescontext) {
		this.getChangeBillingAddressHandler().resetChangeBilling();
	}

	public void setChangeBillingAddressHandler(ChangeBillingAddressHandler changeBillingAddressHandler) {
		this.changeBillingAddressHandler = changeBillingAddressHandler;
	}

}