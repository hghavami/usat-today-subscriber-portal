/**
 * 
 */
package pagecode.account.changebilling;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UISelectItem;
import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import pagecode.PageCodeBase;

import com.ibm.faces.component.html.HtmlBehavior;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlFormItem;
import com.ibm.faces.component.html.HtmlGraphicImageEx;
import com.ibm.faces.component.html.HtmlInputHelperAssist;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.component.html.HtmlPanelLayout;
import com.ibm.faces.component.html.HtmlPanelSection;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.paytec.webapp.business.Address;
import com.usatoday.business.interfaces.UIAddressIntf;
import com.usatoday.business.interfaces.customer.ContactIntf;
import com.usatoday.business.interfaces.customer.CustomerIntf;
import com.usatoday.businessObjects.customer.ContactBO;
import com.usatoday.businessObjects.customer.UIAddressBO;
import com.usatoday.businessObjects.shopping.payment.CreditCardPaymentMethodBO;
import com.usatoday.businessObjects.shopping.payment.PTIConfig;
import com.usatoday.businessObjects.shopping.payment.PTICreditCardProcessor;
import com.usatoday.businessObjects.util.mail.EmailAlert;
import com.usatoday.esub.handlers.SubscriptionOfferHandler;
import com.usatoday.esub.ncs.handlers.ChangeBillingAddressHandler;
import com.usatoday.esub.ncs.handlers.CustomerHandler;
import com.usatoday.util.constants.UsaTodayConstants;

/**
 * @author hghavami
 * 
 */
public class ChangeBilling extends PageCodeBase {

	protected HtmlScriptCollector scriptCollector1;
	protected CustomerHandler customerHandler;
	protected SubscriptionOfferHandler currentOfferHandler;
	protected HtmlForm formChangeBillingAddress;
	protected HtmlPanelSection CreditCardInfoSection;
	protected HtmlPanelLayout layout1;
	protected HtmlPanelGrid Credit;
	protected HtmlSelectBooleanCheckbox EzPaySignupCheckbox;
	protected ChangeBillingAddressHandler changeBillingAddressHandler;
	protected HtmlOutputText AccountNumber;
	protected HtmlInputText FirstName;
	protected HtmlInputText LastName;
	protected HtmlInputText FirmName;
	protected HtmlInputText StreetAddress;
	protected HtmlInputText AptDeptSuite;
	protected HtmlInputText AdditionalAddress1;
	protected HtmlInputText City;
	protected HtmlInputText Zip;
	protected HtmlInputText HomePhoneAreaCode;
	protected HtmlInputText BusPhoneAreaCode;
	protected HtmlOutputText RequiredText1;
	protected HtmlInputText HomePhoneExchange;
	protected HtmlInputText HomePhoneExtension;
	protected HtmlInputText BusPhoneExchange;
	protected HtmlInputText BusPhoneExtension;
	protected HtmlMessages messagesAllMesssages;
	protected HtmlPanelGroup group1;
	protected HtmlPanelGrid creditCardTextEzPay;
	protected HtmlOutputText ezPaytext;
	protected HtmlPanelFormBox formBox3;
	protected HtmlOutputText nonEzPayText;
	protected HtmlSelectBooleanCheckbox ezPaySignupCheckbox;
	protected HtmlBehavior behavior1;
	protected HtmlPanelGrid creditCardTextNoEzPay;
	protected HtmlFormItem ezPaySignupFormItem;
	protected HtmlOutputText extraLine1;
	protected HtmlFormItem formItemCreditCardNumber;
	protected HtmlInputText textCreditCardNumber;
	protected HtmlSelectOneMenu menuCCExpireMonth;
	protected UISelectItem selectItem16;
	protected UISelectItems selectItems1;
	protected HtmlPanelGrid panelGridCreditCardImageGrid;
	protected HtmlOutputText extraLine3;
	protected HtmlGraphicImageEx imageExAmEx1;
	protected HtmlPanelGrid creditCardInfogrid;
	protected HtmlPanelFormBox formBoxPaymentInfo;
	protected HtmlInputHelperAssist assist6;
	protected HtmlFormItem formItemCCExpirationDate;
	protected UISelectItem selectItem3;
	protected UISelectItem selectItem4;
	protected UISelectItem selectItem5;
	protected UISelectItem selectItem6;
	protected UISelectItem selectItem7;
	protected UISelectItem selectItem8;
	protected UISelectItem selectItem9;
	protected UISelectItem selectItem10;
	protected UISelectItem selectItem11;
	protected UISelectItem selectItem12;
	protected UISelectItem selectItem13;
	protected UISelectItem selectItem14;
	protected HtmlSelectOneMenu menuCCExpireYear;
	protected HtmlJspPanel jspPanelCreditCardImages;
	protected HtmlGraphicImageEx imageExDiscover1;
	protected HtmlGraphicImageEx imageExMasterCard1;
	protected HtmlGraphicImageEx imageExVisaLogo1;
	protected HtmlOutputText creditCardPaymentInfotext;
	protected HtmlPanelGrid grid5;
	protected HtmlOutputText extraLine2;
	protected HtmlOutputText extraLine4;
	protected HtmlSelectOneMenu menuBillingState;
	protected HtmlJspPanel jspPanel3;
	protected HtmlPanelFormBox formboxBillingInformation;
	protected HtmlFormItem formAccountNumberLabel;
	protected HtmlFormItem formFirstNameLabel;
	protected HtmlFormItem formLastNameLabel;
	protected HtmlFormItem formFirmNameLabel;
	protected HtmlFormItem formStreetAddressLabel;
	protected HtmlFormItem formAptSuiteLabel;
	protected HtmlFormItem formAddlAddrLabel;
	protected HtmlFormItem formCityLabel;
	protected HtmlFormItem formStateLabel;
	protected HtmlFormItem formZipLabel;
	protected HtmlFormItem formHomePhone;
	protected HtmlInputHelperAssist assist5;
	protected HtmlInputHelperAssist assist8;
	protected HtmlInputHelperAssist assist9;
	protected HtmlPanelGrid grid14;
	protected HtmlCommandExButton continueButton;
	protected HtmlPanelGrid grid2;
	protected HtmlInputHidden isAccountOnEZPay;
	protected HtmlInputHelperAssist assist1;
	protected HtmlInputHelperAssist assist2;
	protected HtmlInputHelperAssist assist3;
	protected HtmlInputHelperAssist assist4;
	protected HtmlInputHelperAssist assist7;
	protected HtmlInputHelperAssist assist10;
	protected HtmlInputHelperAssist assist11;
	protected HtmlInputHelperAssist assist12;

	private String chargeCard(CreditCardPaymentMethodBO ccPayment, ContactIntf billingContact, CustomerIntf customer) {

		String response = "success";
		PTICreditCardProcessor cardProcessor = null;
		boolean containsUTProduct = false;
		boolean containsSWProduct = false;

		// if not a bill me then process card
		if (UsaTodayConstants.AUTHORIZE_CREDIT_CARDS) {

			Address address = new Address();
			address.setAddressLine1(billingContact.getUIAddress().getAddress1());
			address.setAddressLine2(billingContact.getUIAddress().getAptSuite());
			address.setCity(billingContact.getUIAddress().getCity());
			if (billingContact.getUIAddress().getState().length() == 2) {
				address.setStateAbbrev(billingContact.getUIAddress().getState());
			}
			// USA Country only
			address.setCountryCode("840");
			address.setPostalCode(billingContact.getUIAddress().getZip());

			cardProcessor = new PTICreditCardProcessor();
			cardProcessor.setAddress(address);
			cardProcessor.setPaymentType(ccPayment.getPaymentType());
			cardProcessor.setBillingFirstName(billingContact.getFirstName());
			cardProcessor.setBillingLastName(billingContact.getLastName());
			cardProcessor.setCardHolderName(ccPayment.getNameOnCard());
			cardProcessor.setCardNumber(ccPayment.getCardNumber());
			// cardProcessor.setChargeAmount(0.01);
			cardProcessor.setChargeAmount(1);
			if (billingContact.getEmailAddress() != null && billingContact.getEmailAddress().length() > 0) {
				cardProcessor.setEmailAddress(billingContact.getEmailAddress());
			}
			cardProcessor.setExpMonth(ccPayment.getExpirationMonth());
			cardProcessor.setExpYear(ccPayment.getExpirationYear());
			if (UsaTodayConstants.UT_PUBCODE.equalsIgnoreCase(customer.getCurrentAccount().getPubCode())) {
				containsUTProduct = true;
			} else if (UsaTodayConstants.SW_PUBCODE.equalsIgnoreCase(customer.getCurrentAccount().getPubCode())) {
				containsSWProduct = true;
			}

			try {
				if (containsUTProduct) {
					cardProcessor.setPTIClientID(PTIConfig.getInstance().getUTClientID());
				} else if (containsSWProduct) {
					cardProcessor.setPTIClientID(PTIConfig.getInstance().getSWClientID());
				} else {
					cardProcessor.setPTIClientID(PTIConfig.getInstance().getMerchandiseClientID());
				}
			} catch (Exception e) {
				System.out.println("Change Billing address::bulding cardProcessor - Payment processing not properly configured: "
						+ e.getMessage());
			}

			// charge the card
			try {
				cardProcessor.processCCAuthorizationTransaction();

				boolean cardAuthorized = cardProcessor.getAuthResponse().isApproved();

				if (cardAuthorized) {
					// card charged
					ccPayment.setAuthorizedSuccessfully(true);
					ccPayment.setAuthCode(cardProcessor.getResponse().getAuthCode());

					// done to void 0.01 transaction for 0.0 charges
					boolean voided = false;
					try {
						// No void is necessary for Visa or MC
						if (!ccPayment.getCreditCardTypeCode().equals("V") && !ccPayment.getCreditCardTypeCode().equals("M")) {
							cardProcessor.processCCVoidTransaction();
							voided = cardProcessor.getResponse().isApproved();
						} else {
							voided = true;
						}
					} catch (Exception e) {
						voided = false;
					}

					if (!voided) {
						// send email if charge not voided correctly

						StringBuffer bodyText = new StringBuffer();
						try {
							EmailAlert alert = new EmailAlert();
							alert.setSender("InternetCheckOutService@usatoday.com");
							alert.setSubject("Order Entry Alert - FAILED to Void 0.01 charge");
							bodyText.append("\n \nCredit Card Transaction NOT voided successfully: CCLOG: "
									+ cardProcessor.generateCCLogRecord().toString());
							bodyText.append("\n \nCredit Card Voided: ").append(voided);
							bodyText.append("\n \nBilling Last Name: " + billingContact.getLastName());
							bodyText.append("\nBilling First Name: " + billingContact.getFirstName());
							bodyText.append("\nBilling Home Phone: " + billingContact.getHomePhone());
							bodyText.append("\nBilling Bus Phone: " + billingContact.getBusinessPhone());
							bodyText.append("\nBilling Address: " + billingContact.getUIAddress().toString());
							bodyText.append("\n \nDelivery Last Name: "
									+ customer.getCurrentAccount().getDeliveryContact().getLastName());
							bodyText.append("\nDelivery First Name: "
									+ customer.getCurrentAccount().getDeliveryContact().getFirstName());
							bodyText.append("\nDelivery Home Phone: "
									+ customer.getCurrentAccount().getDeliveryContact().getHomePhone());
							bodyText.append("\nDelivery Bus Phone: "
									+ customer.getCurrentAccount().getDeliveryContact().getBusinessPhone());
							bodyText.append("\nDelivery Email: "
									+ customer.getCurrentAccount().getDeliveryContact().getEmailAddress());
							bodyText.append("\n\n Delivery Address: "
									+ customer.getCurrentAccount().getDeliveryContact().getUIAddress().toString());
							alert.setBodyText(bodyText.toString());
							alert.setReceiverList(UsaTodayConstants.EMAIL_ALERT_RECEIVER_EMAILS);
							alert.sendAlert();
						} catch (Exception exp) {
							System.out.println("Failed to send alert: " + bodyText.toString());
						}
					}
				} else { // card failed to Auth

					// if we are allowing batch processing then simply flag item for batch processing
					if (cardProcessor.isNetworkFailure() && UsaTodayConstants.BATCH_PROCESS_CC_NETWORK_FAILURES) {
						ccPayment.setAuthorizedSuccessfully(false);
						response = "success";
					} else {
						// otherwise throw exception on non network error or no batching
						FacesContext context = FacesContext.getCurrentInstance();
						FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
								"The credit card number you entered can not be validated. Please check the number and try again.",
								null);

						context.addMessage(null, facesMsg);
						response = "failure";
					}
				}
			} catch (Exception e) {
				// e.printStackTrace();
				// check for Authorization and void if necessary
				if (cardProcessor.getAuthResponse().isApproved()) {
					try {
						cardProcessor.processCCVoidTransaction();
					} catch (Exception eeee) {
						System.out.println("Failed to Void. " + eeee.getMessage());
					}
				}
			}
		}
		return response;
	}

	public String doButtonSubmitChangeBillingAddressAction() {

		// This is java code that runs when this action method is invoked

		// TODO: Return an outcome that corresponds to a navigation rule
		// return "failure";
		// return "success";

		String response = "success";
		ChangeBillingAddressHandler cbah = this.getChangeBillingAddressHandler();
		CustomerIntf customer = null;

		try {
			customer = this.getCustomerHandler().getCustomer();
			this.getChangeBillingAddressHandler().setCustomer(customer);
		} catch (Exception e) {
			System.out.println("ChangeBillingAddress:  Failed to get customer information: " + e.getMessage());
		}

		// /// billing Contact ///////////
		ContactBO billingContact = new ContactBO();
		ContactBO deliveryContact = new ContactBO();
		CreditCardPaymentMethodBO ccPayment = new CreditCardPaymentMethodBO();

		boolean errorEncountered = false;
		// Check billing address if there is no credit card number present (EZ-Pay or signing up for EZ-Pay) or any billing address
		// field populated
		if (cbah.getCreditCardNumber() == null || cbah.getCreditCardNumber().trim().equals("")
				|| !cbah.getFirstName().trim().equals("") || !cbah.getLastName().trim().equals("")
				|| !cbah.getFirmName().trim().equals("") || !cbah.getStreetAddress().trim().equals("")
				|| !cbah.getAptDeptSuite().trim().equals("") || !cbah.getAddlAddr1().trim().equals("")
				|| !cbah.getCity().trim().equals("") || !cbah.getState().trim().equals("") || !cbah.getZip().trim().equals("")
				|| !cbah.getHomePhoneComplete().trim().equals("") || !customer.getCurrentAccount().isBillingSameAsDelivery()) {
			if (cbah.getFirstName().trim().equals("")) {
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "First Name is Required.", null);

				context.addMessage(null, facesMsg);
				errorEncountered = true;
			}
			if (cbah.getLastName().trim().equals("")) {
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Last Name is Required.", null);

				context.addMessage(null, facesMsg);
				errorEncountered = true;
			}
			if (cbah.getStreetAddress().trim().equals("")) {
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Street Address is Required.", null);

				context.addMessage(null, facesMsg);
				errorEncountered = true;
			}
			if (cbah.getCity().trim().equals("")) {
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "City is Required.", null);

				context.addMessage(null, facesMsg);
				errorEncountered = true;
			}
			if (cbah.getState().trim().equals("")) {
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "State is Required.", null);

				context.addMessage(null, facesMsg);
				errorEncountered = true;
			}
			if (cbah.getZip().trim().equals("") || cbah.getZip().length() < 5) {
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Zip code is invalid.", null);

				context.addMessage(null, facesMsg);
				errorEncountered = true;
			}
			if (cbah.getHomePhoneAreaCode().trim().equals("") || cbah.getHomePhoneAreaCode().length() < 3
					|| cbah.getHomePhoneExchange().trim().equals("") || cbah.getHomePhoneExchange().length() < 3
					|| cbah.getHomePhoneExtension().trim().equals("") || cbah.getHomePhoneExtension().length() < 4) {
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
						"Phone number should be entered in the format: ### ### ####.", null);

				context.addMessage(null, facesMsg);
				errorEncountered = true;
			}
		}
		if (errorEncountered) {
			response = "failure";
			return response;
		}
		// Verify the billing address, if it is entered
		if (cbah.getZip() != null && !cbah.getZip().trim().equals("")) {
			try {

				UIAddressBO dAddress = new UIAddressBO();
				dAddress.setAddress1(cbah.getStreetAddress().trim());
				dAddress.setAddress2(cbah.getAddlAddr1().trim());
				dAddress.setAptSuite(cbah.getAptDeptSuite().trim());
				dAddress.setCity(cbah.getCity().trim());
				dAddress.setState(cbah.getState());
				dAddress.setZip(cbah.getZip().trim());

				billingContact.setUIAddress(dAddress);
				if (!cbah.getFirstName().trim().equals("")) {
					billingContact.setFirstName(cbah.getFirstName());
				} else {
					billingContact.setFirstName(" ");
				}
				if (!cbah.getFirmName().trim().equals("")) {
					billingContact.setFirmName(cbah.getFirmName());
				} else {
					billingContact.setFirmName(" ");
				}
				if (!cbah.getLastName().trim().equals("")) {
					billingContact.setLastName(cbah.getLastName());
				} else {
					billingContact.setLastName(" ");
				}

				if (!cbah.getHomePhoneComplete().trim().equals("")) {
					billingContact.setHomePhone(cbah.getHomePhoneComplete());
				} else {
					billingContact.setHomePhone(" ");
				}

				if (!billingContact.validateContact()) {
					FacesContext context = FacesContext.getCurrentInstance();
					FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
							"We are unable to validate the billing address. Please verify the address you entered.", null);

					context.addMessage(null, facesMsg);
					response = "failure";
					return response;
				} else {
					// update the bean with the code1 address
					try {
						UIAddressIntf correctedAddress = billingContact.getPersistentAddress().convertToUIAddress();

						if (correctedAddress.getAddress1().length() > 0) {
							cbah.setStreetAddress(correctedAddress.getAddress1());
							if (!correctedAddress.getAddress2().trim().equals("") && correctedAddress.getAddress2() != null) {
								cbah.setAddlAddr1(cbah.getAddlAddr1());
							} else {
								cbah.setAddlAddr1(" ");
							}
							cbah.setAptDeptSuite(correctedAddress.getAptSuite());
							if (!correctedAddress.getAptSuite().trim().equals("") && correctedAddress.getAptSuite() != null) {
								cbah.setAptDeptSuite(cbah.getAptDeptSuite());
							} else {
								cbah.setAptDeptSuite(" ");
							}
							cbah.setCity(correctedAddress.getCity());
							cbah.setZip(correctedAddress.getZip());
							cbah.setBillingContact(billingContact);
						}
					} catch (Exception e) {
						// ignore it
						System.out.println("JSF billing Address Entry::Exception setting up corrected address: " + e.getMessage());
					}
				}
			} catch (Exception e) {
				System.out.println("JSF billing Address Entry::Exception setting up corrected address: " + e.getMessage());
			}
		} else { // User delivery instead of billing contact information
			deliveryContact.setFirstName(customer.getCurrentAccount().getDeliveryContact().getFirstName());
			deliveryContact.setLastName(customer.getCurrentAccount().getDeliveryContact().getLastName());
			deliveryContact.setFirmName(customer.getCurrentAccount().getDeliveryContact().getFirmName());
			deliveryContact.setUIAddress(customer.getCurrentAccount().getDeliveryContact().getUIAddress());
			deliveryContact.setPersistentAddress(customer.getCurrentAccount().getDeliveryContact().getPersistentAddress());
			deliveryContact.setHomePhone(customer.getCurrentAccount().getDeliveryContact().getHomePhone());
			cbah.setDeliveryContact(deliveryContact);
		}
		cbah.setNoBillingInfoChange(false);
		// Check if anything has changed
		if (cbah.getFirstName().toUpperCase().trim()
				.equals(customer.getCurrentAccount().getBillingContact().getFirstName().toUpperCase().trim())
				&& cbah.getLastName().toUpperCase().trim()
						.equals(customer.getCurrentAccount().getBillingContact().getLastName().toUpperCase().trim())
				&& cbah.getFirmName().toUpperCase().trim()
						.equals(customer.getCurrentAccount().getBillingContact().getFirmName().toUpperCase().trim())
				&& cbah.getStreetAddress().toUpperCase().trim()
						.equals(customer.getCurrentAccount().getBillingContact().getUIAddress().getAddress1().toUpperCase().trim())
				&& cbah.getAptDeptSuite().toUpperCase().trim()
						.equals(customer.getCurrentAccount().getBillingContact().getUIAddress().getAptSuite().toUpperCase().trim())
				&& cbah.getCity().toUpperCase().trim()
						.equals(customer.getCurrentAccount().getBillingContact().getUIAddress().getCity().toUpperCase().trim())
				&& cbah.getState().toUpperCase().trim()
						.equals(customer.getCurrentAccount().getBillingContact().getUIAddress().getState().toUpperCase().trim())
				&& cbah.getZip().trim().equals(customer.getCurrentAccount().getBillingContact().getUIAddress().getZip().trim())
				&& cbah.getAddlAddr1().toUpperCase().trim()
						.equals(customer.getCurrentAccount().getBillingContact().getUIAddress().getAddress2().toUpperCase().trim())
				&& cbah.getHomePhoneComplete().trim()
						.equals(customer.getCurrentAccount().getBillingContact().getHomePhone().trim())
				&& cbah.isFuturePaymentNotices() == customer.getCurrentAccount().isOneTimeBill()) {

			// No change at all, do not write a transaction
			cbah.setNoBillingInfoChange(true);
		}

		errorEncountered = false;
		// Validate credit card information for EZPay transactions
		if (cbah.isOnEzpay() || cbah.isSignupForEzpay()) {
			String ccNum = "";
			String ccExpYear = "";
			String ccExpMonth = "";
			String currCCNum = "************" + customer.getCurrentAccount().getCreditCardNum().trim();
//			String currCCType = customer.getCurrentAccount().getCreditCardType();
			String currCCExpYear = "";
			String currCCExpMonth = "";
			try {
				currCCExpYear = customer.getCurrentAccount().getCreditCardExpYear().trim();
				currCCExpMonth = customer.getCurrentAccount().getCreditCardExpMonth().trim();
			} catch (Exception e) {

			}
			if (cbah.getCreditCardNumber() != null) {
				ccNum = cbah.getCreditCardNumber().trim();
			}
			if (cbah.getCreditCardExpirationMonth() != null) {
				ccExpMonth = cbah.getCreditCardExpirationMonth().trim();
			}
			if (cbah.getCreditCardExpirationYear() != null) {
				ccExpYear = cbah.getCreditCardExpirationYear().trim();
			}
			// Check expiration date against today's date
			String monthYear = new SimpleDateFormat("yyyyMM").format(new Date());
			int ccMonthYearInt = 0;
			int monthYearInt = 0;
			try {
				ccMonthYearInt = Integer.parseInt(ccExpYear + ccExpMonth);
				monthYearInt = Integer.parseInt(monthYear);
			} catch (Exception e) {
				errorEncountered = true;
			}
			// If expiration date or address changed, currently full CC number is required
			if ((!ccExpYear.equals(currCCExpYear) || !ccExpMonth.equals(currCCExpMonth))
					&& ccNum.contains("*")) {
				String errorMsg = "";
				if (cbah.getBillingContact() != null && cbah.getBillingContact().getUiAddress() != null && !cbah.getBillingContact().getUiAddress().getZip().trim().equals("")) {
					errorMsg = "For your security, the credit card on-file is protected. Please enter the credit card number associated with this billing address.";
				} else {
					errorMsg = "For your security, the credit card on-file is protected. Please enter the credit card number.";
				}
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, errorMsg, null);
				context.addMessage(null, facesMsg);
				response = "failure";
				return response;
			}
			if (!errorEncountered) {
				if (ccMonthYearInt < monthYearInt) {
					errorEncountered = true;
					// Check for no changes in CC info
				} else if (cbah.isNoBillingInfoChange()) {
					if ((!ccNum.equals(currCCNum) || !ccExpMonth.equals(currCCExpMonth)) || !ccExpYear.equals(currCCExpYear)) {
						cbah.setNoBillingInfoChange(false);
					}
				}
				// No CC validation, if no billing info changes
				if (!cbah.isNoBillingInfoChange()) {
					// Check CC info
					if (ccNum == null || ccNum.equals("") || ccExpYear == null || ccExpYear.equals("") || ccExpMonth == null
							|| ccExpMonth.equals("")) {
						errorEncountered = true;
					} else if (!ccNum.equals("")) {
						if (ccNum.length() < 12) {
							errorEncountered = true;
							// Check for numeric or * (existing) CC info
						} else if (!ccNum.contains("*")) {
							try {
								Double.parseDouble(ccNum);
							} catch (Exception e) {
								errorEncountered = true;
							}
							// Error if CC number contains * and has changed
						} else if (!ccNum.equals(currCCNum)) {
							errorEncountered = true;
						}
					}
					if (!errorEncountered) {
						// New CC number
						if (!ccNum.equals(currCCNum)) {
							// Check credit card type
							try {
								ccPayment.setCardNumber(ccNum);
								ccPayment.setExpirationMonth(ccExpMonth);
								ccPayment.setExpirationYear(ccExpYear);
								ccPayment.setNameOnCard(cbah.getFirstName().trim() + " " + cbah.getLastName().trim());
								if (!errorEncountered) {
									// Check whether to use billing or delivery address to verify CC info
									if (cbah.getZip() != null && !cbah.getZip().trim().equals("")) {
										response = this.chargeCard(ccPayment, billingContact, customer);
									} else {
										response = this.chargeCard(ccPayment, deliveryContact, customer);
									}
								}
							} catch (Exception e) {
								System.out.println("Problem during change billing transaction with checking credit card number "
										+ e.getMessage());
								FacesContext context = FacesContext.getCurrentInstance();
								FacesMessage facesMsg = new FacesMessage(
										FacesMessage.SEVERITY_INFO,
										"The credit card information is not recognized. Please check the information and try again.",
										null);
								context.addMessage(null, facesMsg);
								response = "failure";
								return response;
							}
							// CC number is not changed, but expiration date has and will need to be written
						} else {
							try {
								ccPayment = null;
								// Add iSeries credit number retrieval code here
//								CreditCardInfoBO creditCardInfoBO = new CreditCardInfoBO();
//								String accountNum = String.format("%7s", cbah.getCustomer().getCurrentAccount().getAccountNumber());
//								creditCardInfoBO = creditCardInfoBO.determineCreditCardInfo(cbah.getCustomer().getCurrentAccount()
//										.getPubCode(), accountNum, "", "", "");
//								String creditNumber = creditCardInfoBO.getCreditCardNum();
//								// ccPayment.setCurrCardNumber(currCCNum, currCCType);
//								ccPayment.setCurrCardNumber(creditNumber, currCCType);
//								ccPayment.setExpirationMonth(ccExpMonth);
//								ccPayment.setExpirationYear(ccExpYear);
//								ccPayment.setNameOnCard(cbah.getFirstName().trim() + " " + cbah.getLastName().trim());
//								ccPayment.setAuthorizedSuccessfully(true);
//								ccPayment.setAuthCode("");
							} catch (Exception e) {
								System.out.println("Problem during change billing transaction with checking credit card number "
										+ e.getMessage());
								FacesContext context = FacesContext.getCurrentInstance();
								FacesMessage facesMsg = new FacesMessage(
										FacesMessage.SEVERITY_INFO,
										"The credit card information is not recognized. Please check the information and try again.",
										null);
								context.addMessage(null, facesMsg);
								response = "failure";
								return response;
							}
						}
					}
				}
			}
			if (errorEncountered) {
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage facesMsg = new FacesMessage(
						FacesMessage.SEVERITY_INFO,
						"The credit card information is not recognized. Please check the information and try again.",
						null);
				context.addMessage(null, facesMsg);
				response = "failure";
				return response;
			} else {
				cbah.setBillingPayment(ccPayment);
			}
		}
		this.setChangeBillingAddressHandler(cbah);
		return response;
	}

	protected HtmlInputHelperAssist getAssist5() {
		if (assist5 == null) {
			assist5 = (HtmlInputHelperAssist) findComponentInRoot("assist5");
		}
		return assist5;
	}

	protected HtmlInputHelperAssist getAssist6() {
		if (assist6 == null) {
			assist6 = (HtmlInputHelperAssist) findComponentInRoot("assist6");
		}
		return assist6;
	}

	protected HtmlInputHelperAssist getAssist8() {
		if (assist8 == null) {
			assist8 = (HtmlInputHelperAssist) findComponentInRoot("assist8");
		}
		return assist8;
	}

	protected HtmlInputHelperAssist getAssist9() {
		if (assist9 == null) {
			assist9 = (HtmlInputHelperAssist) findComponentInRoot("assist9");
		}
		return assist9;
	}

	protected HtmlBehavior getBehavior1() {
		if (behavior1 == null) {
			behavior1 = (HtmlBehavior) findComponentInRoot("behavior1");
		}
		return behavior1;
	}

	/**
	 * @managed-bean true
	 */
	protected ChangeBillingAddressHandler getChangeBillingAddressHandler() {
		if (changeBillingAddressHandler == null) {
			changeBillingAddressHandler = (ChangeBillingAddressHandler) getManagedBean("changeBillingAddressHandler");
		}
		return changeBillingAddressHandler;
	}

	protected HtmlCommandExButton getContinueButton() {
		if (continueButton == null) {
			continueButton = (HtmlCommandExButton) findComponentInRoot("continueButton");
		}
		return continueButton;
	}

	protected HtmlPanelGrid getCreditCardInfogrid() {
		if (creditCardInfogrid == null) {
			creditCardInfogrid = (HtmlPanelGrid) findComponentInRoot("creditCardInfogrid");
		}
		return creditCardInfogrid;
	}

	protected HtmlOutputText getCreditCardPaymentInfotext() {
		if (creditCardPaymentInfotext == null) {
			creditCardPaymentInfotext = (HtmlOutputText) findComponentInRoot("creditCardPaymentInfotext");
		}
		return creditCardPaymentInfotext;
	}

	protected HtmlPanelGrid getCreditCardTextEzPay() {
		if (creditCardTextEzPay == null) {
			creditCardTextEzPay = (HtmlPanelGrid) findComponentInRoot("creditCardTextEzPay");
		}
		return creditCardTextEzPay;
	}

	protected HtmlPanelGrid getCreditCardTextNoEzPay() {
		if (creditCardTextNoEzPay == null) {
			creditCardTextNoEzPay = (HtmlPanelGrid) findComponentInRoot("creditCardTextNoEzPay");
		}
		return creditCardTextNoEzPay;
	}

	public CustomerHandler getCustomerHandler() {
		if (customerHandler == null) {
			customerHandler = (CustomerHandler) getManagedBean("customerHandler");
		}
		return customerHandler;
	}

	protected HtmlOutputText getExtraLine1() {
		if (extraLine1 == null) {
			extraLine1 = (HtmlOutputText) findComponentInRoot("extraLine1");
		}
		return extraLine1;
	}

	protected HtmlOutputText getExtraLine2() {
		if (extraLine2 == null) {
			extraLine2 = (HtmlOutputText) findComponentInRoot("extraLine2");
		}
		return extraLine2;
	}

	protected HtmlOutputText getExtraLine3() {
		if (extraLine3 == null) {
			extraLine3 = (HtmlOutputText) findComponentInRoot("extraLine3");
		}
		return extraLine3;
	}

	protected HtmlOutputText getExtraLine4() {
		if (extraLine4 == null) {
			extraLine4 = (HtmlOutputText) findComponentInRoot("extraLine4");
		}
		return extraLine4;
	}

	protected HtmlSelectBooleanCheckbox getEzPaySignupCheckbox() {
		if (ezPaySignupCheckbox == null) {
			ezPaySignupCheckbox = (HtmlSelectBooleanCheckbox) findComponentInRoot("ezPaySignupCheckbox");
		}
		return ezPaySignupCheckbox;
	}

	protected HtmlFormItem getEzPaySignupFormItem() {
		if (ezPaySignupFormItem == null) {
			ezPaySignupFormItem = (HtmlFormItem) findComponentInRoot("ezPaySignupFormItem");
		}
		return ezPaySignupFormItem;
	}

	protected HtmlOutputText getEzPaytext() {
		if (ezPaytext == null) {
			ezPaytext = (HtmlOutputText) findComponentInRoot("ezPaytext");
		}
		return ezPaytext;
	}

	protected HtmlFormItem getFormAccountNumberLabel() {
		if (formAccountNumberLabel == null) {
			formAccountNumberLabel = (HtmlFormItem) findComponentInRoot("formAccountNumberLabel");
		}
		return formAccountNumberLabel;
	}

	protected HtmlFormItem getFormAddlAddrLabel() {
		if (formAddlAddrLabel == null) {
			formAddlAddrLabel = (HtmlFormItem) findComponentInRoot("formAddlAddrLabel");
		}
		return formAddlAddrLabel;
	}

	protected HtmlFormItem getFormAptSuiteLabel() {
		if (formAptSuiteLabel == null) {
			formAptSuiteLabel = (HtmlFormItem) findComponentInRoot("formAptSuiteLabel");
		}
		return formAptSuiteLabel;
	}

	protected HtmlPanelFormBox getFormBox3() {
		if (formBox3 == null) {
			formBox3 = (HtmlPanelFormBox) findComponentInRoot("formBox3");
		}
		return formBox3;
	}

	protected HtmlPanelFormBox getFormboxBillingInformation() {
		if (formboxBillingInformation == null) {
			formboxBillingInformation = (HtmlPanelFormBox) findComponentInRoot("formboxBillingInformation");
		}
		return formboxBillingInformation;
	}

	protected HtmlPanelFormBox getFormBoxPaymentInfo() {
		if (formBoxPaymentInfo == null) {
			formBoxPaymentInfo = (HtmlPanelFormBox) findComponentInRoot("formBoxPaymentInfo");
		}
		return formBoxPaymentInfo;
	}

	protected HtmlForm getFormChangeBillingAddress() {
		if (formChangeBillingAddress == null) {
			formChangeBillingAddress = (HtmlForm) findComponentInRoot("formChangeBillingAddress");
		}
		return formChangeBillingAddress;
	}

	protected HtmlFormItem getFormCityLabel() {
		if (formCityLabel == null) {
			formCityLabel = (HtmlFormItem) findComponentInRoot("formCityLabel");
		}
		return formCityLabel;
	}

	protected HtmlFormItem getFormFirmNameLabel() {
		if (formFirmNameLabel == null) {
			formFirmNameLabel = (HtmlFormItem) findComponentInRoot("formFirmNameLabel");
		}
		return formFirmNameLabel;
	}

	protected HtmlFormItem getFormFirstNameLabel() {
		if (formFirstNameLabel == null) {
			formFirstNameLabel = (HtmlFormItem) findComponentInRoot("formFirstNameLabel");
		}
		return formFirstNameLabel;
	}

	protected HtmlFormItem getFormHomePhone() {
		if (formHomePhone == null) {
			formHomePhone = (HtmlFormItem) findComponentInRoot("formHomePhone");
		}
		return formHomePhone;
	}

	protected HtmlFormItem getFormItemCCExpirationDate() {
		if (formItemCCExpirationDate == null) {
			formItemCCExpirationDate = (HtmlFormItem) findComponentInRoot("formItemCCExpirationDate");
		}
		return formItemCCExpirationDate;
	}

	protected HtmlFormItem getFormItemCreditCardNumber() {
		if (formItemCreditCardNumber == null) {
			formItemCreditCardNumber = (HtmlFormItem) findComponentInRoot("formItemCreditCardNumber");
		}
		return formItemCreditCardNumber;
	}

	protected HtmlFormItem getFormLastNameLabel() {
		if (formLastNameLabel == null) {
			formLastNameLabel = (HtmlFormItem) findComponentInRoot("formLastNameLabel");
		}
		return formLastNameLabel;
	}

	protected HtmlFormItem getFormStateLabel() {
		if (formStateLabel == null) {
			formStateLabel = (HtmlFormItem) findComponentInRoot("formStateLabel");
		}
		return formStateLabel;
	}

	protected HtmlFormItem getFormStreetAddressLabel() {
		if (formStreetAddressLabel == null) {
			formStreetAddressLabel = (HtmlFormItem) findComponentInRoot("formStreetAddressLabel");
		}
		return formStreetAddressLabel;
	}

	protected HtmlFormItem getFormZipLabel() {
		if (formZipLabel == null) {
			formZipLabel = (HtmlFormItem) findComponentInRoot("formZipLabel");
		}
		return formZipLabel;
	}

	protected HtmlPanelGrid getGrid14() {
		if (grid14 == null) {
			grid14 = (HtmlPanelGrid) findComponentInRoot("grid14");
		}
		return grid14;
	}

	protected HtmlPanelGrid getGrid2() {
		if (grid2 == null) {
			grid2 = (HtmlPanelGrid) findComponentInRoot("grid2");
		}
		return grid2;
	}

	protected HtmlPanelGrid getGrid5() {
		if (grid5 == null) {
			grid5 = (HtmlPanelGrid) findComponentInRoot("grid5");
		}
		return grid5;
	}

	protected HtmlPanelGroup getGroup1() {
		if (group1 == null) {
			group1 = (HtmlPanelGroup) findComponentInRoot("group1");
		}
		return group1;
	}

	protected HtmlGraphicImageEx getImageExAmEx1() {
		if (imageExAmEx1 == null) {
			imageExAmEx1 = (HtmlGraphicImageEx) findComponentInRoot("imageExAmEx1");
		}
		return imageExAmEx1;
	}

	protected HtmlGraphicImageEx getImageExDiscover1() {
		if (imageExDiscover1 == null) {
			imageExDiscover1 = (HtmlGraphicImageEx) findComponentInRoot("imageExDiscover1");
		}
		return imageExDiscover1;
	}

	protected HtmlGraphicImageEx getImageExMasterCard1() {
		if (imageExMasterCard1 == null) {
			imageExMasterCard1 = (HtmlGraphicImageEx) findComponentInRoot("imageExMasterCard1");
		}
		return imageExMasterCard1;
	}

	protected HtmlGraphicImageEx getImageExVisaLogo1() {
		if (imageExVisaLogo1 == null) {
			imageExVisaLogo1 = (HtmlGraphicImageEx) findComponentInRoot("imageExVisaLogo1");
		}
		return imageExVisaLogo1;
	}

	protected HtmlInputHidden getIsAccountOnEZPay() {
		if (isAccountOnEZPay == null) {
			isAccountOnEZPay = (HtmlInputHidden) findComponentInRoot("isAccountOnEZPay");
		}
		return isAccountOnEZPay;
	}

	protected HtmlJspPanel getJspPanel3() {
		if (jspPanel3 == null) {
			jspPanel3 = (HtmlJspPanel) findComponentInRoot("jspPanel3");
		}
		return jspPanel3;
	}

	protected HtmlJspPanel getJspPanelCreditCardImages() {
		if (jspPanelCreditCardImages == null) {
			jspPanelCreditCardImages = (HtmlJspPanel) findComponentInRoot("jspPanelCreditCardImages");
		}
		return jspPanelCreditCardImages;
	}

	protected HtmlPanelLayout getLayout1() {
		if (layout1 == null) {
			layout1 = (HtmlPanelLayout) findComponentInRoot("layout1");
		}
		return layout1;
	}

	protected HtmlSelectOneMenu getMenuBillingState() {
		if (menuBillingState == null) {
			menuBillingState = (HtmlSelectOneMenu) findComponentInRoot("menuBillingState");
		}
		return menuBillingState;
	}

	protected HtmlSelectOneMenu getMenuCCExpireMonth() {
		if (menuCCExpireMonth == null) {
			menuCCExpireMonth = (HtmlSelectOneMenu) findComponentInRoot("menuCCExpireMonth");
		}
		return menuCCExpireMonth;
	}

	protected HtmlSelectOneMenu getMenuCCExpireYear() {
		if (menuCCExpireYear == null) {
			menuCCExpireYear = (HtmlSelectOneMenu) findComponentInRoot("menuCCExpireYear");
		}
		return menuCCExpireYear;
	}

	protected HtmlMessages getMessagesAllMesssages() {
		if (messagesAllMesssages == null) {
			messagesAllMesssages = (HtmlMessages) findComponentInRoot("messagesAllMesssages");
		}
		return messagesAllMesssages;
	}

	protected HtmlOutputText getNonEzPayText() {
		if (nonEzPayText == null) {
			nonEzPayText = (HtmlOutputText) findComponentInRoot("nonEzPayText");
		}
		return nonEzPayText;
	}

	protected HtmlPanelGrid getPanelGridCreditCardImageGrid() {
		if (panelGridCreditCardImageGrid == null) {
			panelGridCreditCardImageGrid = (HtmlPanelGrid) findComponentInRoot("panelGridCreditCardImageGrid");
		}
		return panelGridCreditCardImageGrid;
	}

	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected UISelectItem getSelectItem10() {
		if (selectItem10 == null) {
			selectItem10 = (UISelectItem) findComponentInRoot("selectItem10");
		}
		return selectItem10;
	}

	protected UISelectItem getSelectItem11() {
		if (selectItem11 == null) {
			selectItem11 = (UISelectItem) findComponentInRoot("selectItem11");
		}
		return selectItem11;
	}

	protected UISelectItem getSelectItem12() {
		if (selectItem12 == null) {
			selectItem12 = (UISelectItem) findComponentInRoot("selectItem12");
		}
		return selectItem12;
	}

	protected UISelectItem getSelectItem13() {
		if (selectItem13 == null) {
			selectItem13 = (UISelectItem) findComponentInRoot("selectItem13");
		}
		return selectItem13;
	}

	protected UISelectItem getSelectItem14() {
		if (selectItem14 == null) {
			selectItem14 = (UISelectItem) findComponentInRoot("selectItem14");
		}
		return selectItem14;
	}

	protected UISelectItem getSelectItem16() {
		if (selectItem16 == null) {
			selectItem16 = (UISelectItem) findComponentInRoot("selectItem16");
		}
		return selectItem16;
	}

	protected UISelectItem getSelectItem3() {
		if (selectItem3 == null) {
			selectItem3 = (UISelectItem) findComponentInRoot("selectItem3");
		}
		return selectItem3;
	}

	protected UISelectItem getSelectItem4() {
		if (selectItem4 == null) {
			selectItem4 = (UISelectItem) findComponentInRoot("selectItem4");
		}
		return selectItem4;
	}

	protected UISelectItem getSelectItem5() {
		if (selectItem5 == null) {
			selectItem5 = (UISelectItem) findComponentInRoot("selectItem5");
		}
		return selectItem5;
	}

	protected UISelectItem getSelectItem6() {
		if (selectItem6 == null) {
			selectItem6 = (UISelectItem) findComponentInRoot("selectItem6");
		}
		return selectItem6;
	}

	protected UISelectItem getSelectItem7() {
		if (selectItem7 == null) {
			selectItem7 = (UISelectItem) findComponentInRoot("selectItem7");
		}
		return selectItem7;
	}

	protected UISelectItem getSelectItem8() {
		if (selectItem8 == null) {
			selectItem8 = (UISelectItem) findComponentInRoot("selectItem8");
		}
		return selectItem8;
	}

	protected UISelectItem getSelectItem9() {
		if (selectItem9 == null) {
			selectItem9 = (UISelectItem) findComponentInRoot("selectItem9");
		}
		return selectItem9;
	}

	protected UISelectItems getSelectItems1() {
		if (selectItems1 == null) {
			selectItems1 = (UISelectItems) findComponentInRoot("selectItems1");
		}
		return selectItems1;
	}

	protected HtmlInputText getTextCreditCardNumber() {
		if (textCreditCardNumber == null) {
			textCreditCardNumber = (HtmlInputText) findComponentInRoot("textCreditCardNumber");
		}
		return textCreditCardNumber;
	}

	public void onPageLoadBegin(FacesContext facescontext) {

		try {
			CustomerIntf cust = this.getCustomerHandler().getCustomer();
			this.getChangeBillingAddressHandler().setCustomer(cust);
		} catch (Exception e) {
			System.out.println("ChangeBillingAddress:  Failed to get customer information: " + e.getMessage());
		}
	}

	public void onPageLoadEnd(FacesContext facescontext) {

	}

	/**
	 * @managed-bean true
	 */
	protected void setChangeBillingAddressHandler(ChangeBillingAddressHandler changeBillingAddressHandler) {
		this.changeBillingAddressHandler = changeBillingAddressHandler;
	}

	public void validateFirstName(FacesContext facescontext, UIComponent component, Object object) throws ValidatorException {

		if (!this.getCustomerHandler().getCustomer().getCurrentAccount().isOnEZPay() && (FirstName.equals("") || FirstName == null)) {
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "First Name is invalid.", null);
			context.addMessage(null, facesMsg);
		}

	}

	public void validateHomePhone(FacesContext facescontext, UIComponent component, Object object) throws ValidatorException {
		if (textCreditCardNumber.equals("") || !FirstName.equals("") || !LastName.equals("") || !FirmName.equals("")
				|| !StreetAddress.equals("") || !AptDeptSuite.equals("") || !AdditionalAddress1.equals("") || !City.equals("")
				|| !menuBillingState.equals("") || !Zip.equals("") || !HomePhoneAreaCode.equals("")
				|| !HomePhoneExchange.equals("") || !HomePhoneExtension.equals("")) {

			if (HomePhoneAreaCode.equals("") || HomePhoneAreaCode.getSize() < 3 || HomePhoneExchange.equals("")
					|| HomePhoneExchange.getSize() < 3 || HomePhoneExtension.equals("") || HomePhoneExtension.getSize() < 4) {
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
						"Phone number should be entered in the format: ### ### ####.", null);
				context.addMessage(null, facesMsg);
			}
		}
	}

	protected HtmlInputHelperAssist getAssist1() {
		if (assist1 == null) {
			assist1 = (HtmlInputHelperAssist) findComponentInRoot("assist1");
		}
		return assist1;
	}

	protected HtmlInputHelperAssist getAssist2() {
		if (assist2 == null) {
			assist2 = (HtmlInputHelperAssist) findComponentInRoot("assist2");
		}
		return assist2;
	}

	protected HtmlInputHelperAssist getAssist3() {
		if (assist3 == null) {
			assist3 = (HtmlInputHelperAssist) findComponentInRoot("assist3");
		}
		return assist3;
	}

	protected HtmlInputHelperAssist getAssist4() {
		if (assist4 == null) {
			assist4 = (HtmlInputHelperAssist) findComponentInRoot("assist4");
		}
		return assist4;
	}

	protected HtmlInputHelperAssist getAssist7() {
		if (assist7 == null) {
			assist7 = (HtmlInputHelperAssist) findComponentInRoot("assist7");
		}
		return assist7;
	}

	protected HtmlInputHelperAssist getAssist10() {
		if (assist10 == null) {
			assist10 = (HtmlInputHelperAssist) findComponentInRoot("assist10");
		}
		return assist10;
	}

	protected HtmlInputHelperAssist getAssist11() {
		if (assist11 == null) {
			assist11 = (HtmlInputHelperAssist) findComponentInRoot("assist11");
		}
		return assist11;
	}

	protected HtmlInputHelperAssist getAssist12() {
		if (assist12 == null) {
			assist12 = (HtmlInputHelperAssist) findComponentInRoot("assist12");
		}
		return assist12;
	}
}