/**
 * 
 */
package pagecode.account.vacationholds;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.context.FacesContext;

import org.joda.time.DateTime;

import pagecode.PageCodeBase;

import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlFormItem;
import com.ibm.faces.component.html.HtmlInputHelperAssist;
import com.ibm.faces.component.html.HtmlInputHelperDatePicker;
import com.ibm.faces.component.html.HtmlInputHelperSetFocus;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.UsatException;
import com.usatoday.esub.ncs.handlers.CustomerHandler;
import com.usatoday.esub.ncs.handlers.SuspendResumeSubscriptionHandler;

/**
 * @author aeast
 * 
 */
public class Holds extends PageCodeBase {

	protected HtmlOutputText mainHeader;
	protected HtmlScriptCollector scriptCollector1;
	protected HtmlForm form1;
	protected HtmlPanelFormBox formBoxSuspendResumeFormBox;
	protected HtmlFormItem formItemHoldEnd;
	protected HtmlFormItem formItemHoldStart;
	protected HtmlInputText textHoldStart;
	protected HtmlInputText textHoldEnd;
	protected SuspendResumeSubscriptionHandler suspendResumeHandler;
	protected CustomerHandler customerHandler;
	protected HtmlInputHelperAssist assist1;
	protected HtmlMessages messages1;
	protected HtmlOutputText textAlreadyHeldMessage;
	protected HtmlPanelGrid alreadyHeldGrid;
	protected HtmlInputHelperDatePicker datePicker2;
	protected HtmlInputHelperSetFocus setFocus1;
	protected HtmlPanelFormBox formBoxSuspendResumeFormBoxBottom;
	protected HtmlPanelGrid gridFooterSubmitGrid;
	protected HtmlCommandExButton buttonSubmitHold;
	protected HtmlSelectBooleanCheckbox checkboxOpenHold;
	protected HtmlFormItem formItemNoRestart;
	protected HtmlSelectBooleanCheckbox checkboxNIESelection;
	protected HtmlFormItem formItemNIEItem;
	protected HtmlInputHelperDatePicker datePicker1;
	protected HtmlPanelGrid grid1;

	protected HtmlOutputText getMainHeader() {
		if (mainHeader == null) {
			mainHeader = (HtmlOutputText) findComponentInRoot("mainHeader");
		}
		return mainHeader;
	}

	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlForm getForm1() {
		if (form1 == null) {
			form1 = (HtmlForm) findComponentInRoot("form1");
		}
		return form1;
	}

	protected HtmlPanelFormBox getFormBoxSuspendResumeFormBox() {
		if (formBoxSuspendResumeFormBox == null) {
			formBoxSuspendResumeFormBox = (HtmlPanelFormBox) findComponentInRoot("formBoxSuspendResumeFormBox");
		}
		return formBoxSuspendResumeFormBox;
	}

	protected HtmlFormItem getFormItemHoldEnd() {
		if (formItemHoldEnd == null) {
			formItemHoldEnd = (HtmlFormItem) findComponentInRoot("formItemHoldEnd");
		}
		return formItemHoldEnd;
	}

	protected HtmlFormItem getFormItemHoldStart() {
		if (formItemHoldStart == null) {
			formItemHoldStart = (HtmlFormItem) findComponentInRoot("formItemHoldStart");
		}
		return formItemHoldStart;
	}

	protected HtmlInputText getTextHoldStart() {
		if (textHoldStart == null) {
			textHoldStart = (HtmlInputText) findComponentInRoot("textHoldStart");
		}
		return textHoldStart;
	}

	protected HtmlInputText getTextHoldEnd() {
		if (textHoldEnd == null) {
			textHoldEnd = (HtmlInputText) findComponentInRoot("textHoldEnd");
		}
		return textHoldEnd;
	}

	public void onPageLoadBegin(FacesContext facescontext) {
		// Type Java code to handle page load begin event here

		// void <method>(FacesContext facescontext)
		SuspendResumeSubscriptionHandler srh = this.getSuspendResumeHandler();

		CustomerHandler ch = this.getCustomerHandler();
		if (srh.getCurrentCustomer() == null) {
			srh.setCurrentCustomer(ch);
		}

		// If NI trans type, then the account is pending and no rates can be displayed
		if (!srh.isSubscriptionActive()) {
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
					"Your subscription currently is pending/inactive, please try later or call our customer service.", null);

			context.addMessage(null, facesMsg);
		} else {

			if (srh.getHoldStart() == null) {
				// set to earliest possible date

				DateTime initialDate = ch.getCurrentAccount().getAccount().getProduct().getEarliestPossibleHoldStartDate();
				// following work around due to JSF checking down to millisecond.
				initialDate = initialDate.plusSeconds(10);

				srh.setHoldStart(initialDate.toDate());
			}
//			srh.resetMIE();
		}
	}

	/**
	 * @managed-bean true
	 */
	protected SuspendResumeSubscriptionHandler getSuspendResumeHandler() {
		if (suspendResumeHandler == null) {
			suspendResumeHandler = (SuspendResumeSubscriptionHandler) getManagedBean("suspendResumeHandler");
		}
		return suspendResumeHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setSuspendResumeHandler(SuspendResumeSubscriptionHandler suspendResumeHandler) {
		this.suspendResumeHandler = suspendResumeHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected CustomerHandler getCustomerHandler() {
		if (customerHandler == null) {
			customerHandler = (CustomerHandler) getManagedBean("customerHandler");
		}
		return customerHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setCustomerHandler(CustomerHandler customerHandler) {
		this.customerHandler = customerHandler;
	}

	public String doButtonSubmitHoldAction() {

		String responseString = "success";

		CustomerHandler ch = this.getCustomerHandler();
		SuspendResumeSubscriptionHandler srh = this.getSuspendResumeHandler();

		// boolean wasDonating = ch.getCurrentAccount().getAccount().isDonatingToNIE();

		// if initiating a vacation hold
		if (!ch.getCurrentAccount().getAccount().isSubscriptionSuspended()) {
			try {
				if (!srh.isUnsureOfResumeDate()) {
					if (srh.getHoldEnd() == null) {
						throw new UsatException(
								"Please choose a date to restart your subscription. If you are unsure of the restart date, check the box to indicate you would like an open ended hold.");
					} else if (srh.getHoldEnd().equals(srh.getHoldStart())) {
						throw new UsatException("Stop and Restart dates can not be the same.");
					} else if (srh.getHoldEnd().before(srh.getHoldStart())) {
						throw new UsatException("Restart date can not be before Stop date.");
					}
				}

				/*
				 * 
				 * //DateTime holdEndDT = null; DateTime holdStartDT = null; //holdEndDT = new DateTime(srh.getHoldEnd().getTime());
				 * holdStartDT = new DateTime(srh.getHoldStart().getTime()); DateTime holdEndDT = null; GenesysResponse response =
				 * CustomerService.resumeSubscription(ch.getCurrentAccount().getAccount(), holdStartDT,wasDonating ); // if no
				 * errors if (!response.isContainsErrors()) { // complaintHandler.resetWebFormValuesOnly(); //
				 * complaintHandler.setLastComplaint(c);
				 * 
				 * try { // CustomerServiceEmails.sendComplaintConfirmationEmail(ch.getCustomer(), account, c); } catch (Exception
				 * e) { // ignore email failure; //System.out.println("Failed to send complaint confirmation email for account: " +
				 * account.getAccountNumber() + ". " + e.getMessage()); } responseString = "success"; } else { // we got errors
				 * Collection<GenesysBaseAPIResponse> responses = response.getResponses(); for (GenesysBaseAPIResponse res :
				 * responses) { if (res.redirectResponse()) { // System Error exists //
				 * complaintHandler.setRawAPIResponse(res.getRawResponse()); // this.getDialogErrorDialog().setInitiallyShow(true);
				 * throw new Exception("Error Occurred: Unexpected Response Received. Please call Customer Service."); } else { //
				 * business rule errors exist Collection<String> errors = res.getErrorMessages(); if (errors != null) {
				 * //StringBuilder errorBuf = new StringBuilder(); for (String msg : errors){ FacesContext context =
				 * FacesContext.getCurrentInstance(); FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, msg,
				 * null);
				 * 
				 * context.addMessage(null, facesMsg); } throw new Exception(); } else { throw new
				 * Exception("Failed to create delivery issue record: Reason Unknown."); } } } }
				 */
			} catch (Exception e) {
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed to validate subscription hold: "
						+ e.getMessage(), null);

				context.addMessage(null, facesMsg);
				responseString = "failure";
			}

		} else { // resuming an open delivery hold
			try {
				if (srh.getHoldEnd() == null) {
					throw new UsatException("Invalid re-start date (null)");
				}

				/*
				 * ///Start hdcons-47 DateTime holdEndDT = null; DateTime holdStartDT = null; holdEndDT = new
				 * DateTime(srh.getHoldEnd().getTime()); holdStartDT = new DateTime(srh.getHoldStart().getTime()); GenesysResponse
				 * response = CustomerService.suspendSubscription(ch.getCurrentAccount().getAccount(), holdStartDT,
				 * holdEndDT,wasDonating );
				 * 
				 * // if no errors if (!response.isContainsErrors()) { // complaintHandler.resetWebFormValuesOnly(); //
				 * complaintHandler.setLastComplaint(c);
				 * 
				 * try { // CustomerServiceEmails.sendSuspendResumeDeliveryEmail(ch.getCustomer(), startDT, endDT, wasDonating, "");
				 * } catch (Exception e) { // ignore email failure;
				 * //System.out.println("Failed to send vaca confirmation email for account: " + account.getAccountNumber() + ". " +
				 * e.getMessage()); } responseString = "success"; } else { // we got errors Collection<GenesysBaseAPIResponse>
				 * responses = response.getResponses(); for (GenesysBaseAPIResponse res : responses) { if (res.redirectResponse()) {
				 * // System Error exists // complaintHandler.setRawAPIResponse(res.getRawResponse()); //
				 * this.getDialogErrorDialog().setInitiallyShow(true); throw new
				 * Exception("Error Occurred: Unexpected Response Received. Please call Customer Service."); } else { // business
				 * rule errors exist Collection<String> errors = res.getErrorMessages(); if (errors != null) { //StringBuilder
				 * errorBuf = new StringBuilder(); for (String msg : errors){ FacesContext context =
				 * FacesContext.getCurrentInstance(); FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, msg,
				 * null);
				 * 
				 * context.addMessage(null, facesMsg); } throw new Exception(); } else { throw new
				 * Exception("Failed to create vacation hold record: Reason Unknown."); } } } }
				 */

			} catch (Exception e) {
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed to validate subscription re-start: "
						+ e.getMessage(), null);

				context.addMessage(null, facesMsg);
				responseString = "failure";
				srh.setTransactionPlaced(false);
			}

		} // end else

		return responseString;
	}

	protected HtmlInputHelperAssist getAssist1() {
		if (assist1 == null) {
			assist1 = (HtmlInputHelperAssist) findComponentInRoot("assist1");
		}
		return assist1;
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

	protected HtmlOutputText getTextAlreadyHeldMessage() {
		if (textAlreadyHeldMessage == null) {
			textAlreadyHeldMessage = (HtmlOutputText) findComponentInRoot("textAlreadyHeldMessage");
		}
		return textAlreadyHeldMessage;
	}

	protected HtmlPanelGrid getAlreadyHeldGrid() {
		if (alreadyHeldGrid == null) {
			alreadyHeldGrid = (HtmlPanelGrid) findComponentInRoot("alreadyHeldGrid");
		}
		return alreadyHeldGrid;
	}

	protected HtmlInputHelperDatePicker getDatePicker2() {
		if (datePicker2 == null) {
			datePicker2 = (HtmlInputHelperDatePicker) findComponentInRoot("datePicker2");
		}
		return datePicker2;
	}

	protected HtmlInputHelperSetFocus getSetFocus1() {
		if (setFocus1 == null) {
			setFocus1 = (HtmlInputHelperSetFocus) findComponentInRoot("setFocus1");
		}
		return setFocus1;
	}

	protected HtmlPanelFormBox getFormBoxSuspendResumeFormBoxBottom() {
		if (formBoxSuspendResumeFormBoxBottom == null) {
			formBoxSuspendResumeFormBoxBottom = (HtmlPanelFormBox) findComponentInRoot("formBoxSuspendResumeFormBoxBottom");
		}
		return formBoxSuspendResumeFormBoxBottom;
	}

	protected HtmlPanelGrid getGridFooterSubmitGrid() {
		if (gridFooterSubmitGrid == null) {
			gridFooterSubmitGrid = (HtmlPanelGrid) findComponentInRoot("gridFooterSubmitGrid");
		}
		return gridFooterSubmitGrid;
	}

	protected HtmlCommandExButton getButtonSubmitHold() {
		if (buttonSubmitHold == null) {
			buttonSubmitHold = (HtmlCommandExButton) findComponentInRoot("buttonSubmitHold");
		}
		return buttonSubmitHold;
	}

	protected HtmlSelectBooleanCheckbox getCheckboxOpenHold() {
		if (checkboxOpenHold == null) {
			checkboxOpenHold = (HtmlSelectBooleanCheckbox) findComponentInRoot("checkboxOpenHold");
		}
		return checkboxOpenHold;
	}

	protected HtmlFormItem getFormItemNoRestart() {
		if (formItemNoRestart == null) {
			formItemNoRestart = (HtmlFormItem) findComponentInRoot("formItemNoRestart");
		}
		return formItemNoRestart;
	}

	protected HtmlSelectBooleanCheckbox getCheckboxNIESelection() {
		if (checkboxNIESelection == null) {
			checkboxNIESelection = (HtmlSelectBooleanCheckbox) findComponentInRoot("checkboxNIESelection");
		}
		return checkboxNIESelection;
	}

	protected HtmlFormItem getFormItemNIEItem() {
		if (formItemNIEItem == null) {
			formItemNIEItem = (HtmlFormItem) findComponentInRoot("formItemNIEItem");
		}
		return formItemNIEItem;
	}

	protected HtmlInputHelperDatePicker getDatePicker1() {
		if (datePicker1 == null) {
			datePicker1 = (HtmlInputHelperDatePicker) findComponentInRoot("datePicker1");
		}
		return datePicker1;
	}

	protected HtmlPanelGrid getGrid1() {
		if (grid1 == null) {
			grid1 = (HtmlPanelGrid) findComponentInRoot("grid1");
		}
		return grid1;
	}

}