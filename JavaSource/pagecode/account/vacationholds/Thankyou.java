/**
 * 
 */
package pagecode.account.vacationholds;

import pagecode.PageCodeBase;
import javax.faces.component.html.HtmlOutputText;
import com.ibm.faces.component.html.HtmlScriptCollector;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlMessages;
import com.ibm.faces.component.html.HtmlFormItem;
import javax.faces.component.html.HtmlPanelGrid;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import javax.faces.context.FacesContext;
import com.usatoday.esub.ncs.handlers.SuspendResumeSubscriptionHandler;

/**
 * @author aeast
 * 
 */
public class Thankyou extends PageCodeBase {

	protected HtmlOutputText mainHeader;
	protected HtmlScriptCollector scriptCollector1;
	protected HtmlForm form1;
	protected HtmlMessages messages1;
	protected HtmlFormItem formItemStopDate;
	protected HtmlOutputText textStopDate;
	protected HtmlOutputText text2;
	protected HtmlOutputText text3;
	protected HtmlOutputText text4;
	protected HtmlOutputText outputTextDonateStatus;
	protected HtmlPanelGrid gridStartStopConfirm;
	protected HtmlOutputText text1;
	protected HtmlPanelFormBox formBoxDetails;
	protected HtmlFormItem formItemNoStopDate;
	protected HtmlFormItem formItemRestartDate;
	protected HtmlFormItem formItemNoRestartDate;
	protected HtmlFormItem formItemDonating;
	protected SuspendResumeSubscriptionHandler suspendResumeHandler;

	protected HtmlOutputText getMainHeader() {
		if (mainHeader == null) {
			mainHeader = (HtmlOutputText) findComponentInRoot("mainHeader");
		}
		return mainHeader;
	}

	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlForm getForm1() {
		if (form1 == null) {
			form1 = (HtmlForm) findComponentInRoot("form1");
		}
		return form1;
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

	protected HtmlFormItem getFormItemStopDate() {
		if (formItemStopDate == null) {
			formItemStopDate = (HtmlFormItem) findComponentInRoot("formItemStopDate");
		}
		return formItemStopDate;
	}

	protected HtmlOutputText getTextStopDate() {
		if (textStopDate == null) {
			textStopDate = (HtmlOutputText) findComponentInRoot("textStopDate");
		}
		return textStopDate;
	}

	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected HtmlOutputText getText3() {
		if (text3 == null) {
			text3 = (HtmlOutputText) findComponentInRoot("text3");
		}
		return text3;
	}

	protected HtmlOutputText getText4() {
		if (text4 == null) {
			text4 = (HtmlOutputText) findComponentInRoot("text4");
		}
		return text4;
	}

	protected HtmlOutputText getOutputTextDonateStatus() {
		if (outputTextDonateStatus == null) {
			outputTextDonateStatus = (HtmlOutputText) findComponentInRoot("outputTextDonateStatus");
		}
		return outputTextDonateStatus;
	}

	protected HtmlPanelGrid getGridStartStopConfirm() {
		if (gridStartStopConfirm == null) {
			gridStartStopConfirm = (HtmlPanelGrid) findComponentInRoot("gridStartStopConfirm");
		}
		return gridStartStopConfirm;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlPanelFormBox getFormBoxDetails() {
		if (formBoxDetails == null) {
			formBoxDetails = (HtmlPanelFormBox) findComponentInRoot("formBoxDetails");
		}
		return formBoxDetails;
	}

	protected HtmlFormItem getFormItemNoStopDate() {
		if (formItemNoStopDate == null) {
			formItemNoStopDate = (HtmlFormItem) findComponentInRoot("formItemNoStopDate");
		}
		return formItemNoStopDate;
	}

	protected HtmlFormItem getFormItemRestartDate() {
		if (formItemRestartDate == null) {
			formItemRestartDate = (HtmlFormItem) findComponentInRoot("formItemRestartDate");
		}
		return formItemRestartDate;
	}

	protected HtmlFormItem getFormItemNoRestartDate() {
		if (formItemNoRestartDate == null) {
			formItemNoRestartDate = (HtmlFormItem) findComponentInRoot("formItemNoRestartDate");
		}
		return formItemNoRestartDate;
	}

	protected HtmlFormItem getFormItemDonating() {
		if (formItemDonating == null) {
			formItemDonating = (HtmlFormItem) findComponentInRoot("formItemDonating");
		}
		return formItemDonating;
	}

	public void onPageLoadEnd(FacesContext facescontext) {
		// Type Java code to handle page load end event here

		// void <method>(FacesContext facescontext)
		this.getSuspendResumeHandler().resetHandler();

	}

	/**
	 * @managed-bean true
	 */
	protected SuspendResumeSubscriptionHandler getSuspendResumeHandler() {
		if (suspendResumeHandler == null) {
			suspendResumeHandler = (SuspendResumeSubscriptionHandler) getManagedBean("suspendResumeHandler");
		}
		return suspendResumeHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setSuspendResumeHandler(SuspendResumeSubscriptionHandler suspendResumeHandler) {
		this.suspendResumeHandler = suspendResumeHandler;
	}

}