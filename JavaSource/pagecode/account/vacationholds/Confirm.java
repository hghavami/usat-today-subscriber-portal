/**
 * 
 */
package pagecode.account.vacationholds;

import java.util.Collection;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.context.FacesContext;

import org.joda.time.DateTime;

import pagecode.PageCodeBase;

import com.gannett.usat.iconapi.domainbeans.GenesysBaseAPIResponse;
import com.ibm.faces.component.html.HtmlFormItem;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.UsatException;
import com.usatoday.businessObjects.customer.service.CustomerService;
import com.usatoday.businessObjects.customer.service.CustomerServiceEmails;
import com.usatoday.businessObjects.subscriptionTransactions.GenesysResponse;
import com.usatoday.esub.common.UTCommon;
import com.usatoday.esub.ncs.handlers.CustomerHandler;
import com.usatoday.esub.ncs.handlers.SuspendResumeSubscriptionHandler;
import com.usatoday.util.constants.UsaTodayConstants;

import javax.faces.component.html.HtmlMessages;
import com.ibm.faces.component.html.HtmlCommandExButton;

/**
 * @author aeast
 * 
 */
public class Confirm extends PageCodeBase {

	protected HtmlOutputText mainHeader;
	protected HtmlScriptCollector scriptCollector1;
	protected HtmlForm form1;
	protected HtmlPanelGrid gridStartStopConfirm;
	protected SuspendResumeSubscriptionHandler suspendResumeHandler;
	protected HtmlOutputText text1;
	protected HtmlPanelGroup group1;
	protected HtmlPanelFormBox formBoxDetails;
	protected HtmlFormItem formItemStopDate;
	protected HtmlFormItem formItemNoStopDate;
	protected HtmlOutputText textStopDate;
	protected HtmlOutputText text2;
	protected HtmlFormItem formItemRestartDate;
	protected HtmlFormItem formItemNoRestartDate;
	protected HtmlFormItem formItemDonating;
	protected HtmlOutputText text3;
	protected HtmlOutputText text4;
	protected CustomerHandler customerHandler;
	protected HtmlMessages messages1;
	protected HtmlOutputText outputTextDonateStatus;
	protected HtmlCommandExButton buttonSaveTransaction;

	protected HtmlOutputText getMainHeader() {
		if (mainHeader == null) {
			mainHeader = (HtmlOutputText) findComponentInRoot("mainHeader");
		}
		return mainHeader;
	}

	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlForm getForm1() {
		if (form1 == null) {
			form1 = (HtmlForm) findComponentInRoot("form1");
		}
		return form1;
	}

	protected HtmlPanelGrid getGridStartStopConfirm() {
		if (gridStartStopConfirm == null) {
			gridStartStopConfirm = (HtmlPanelGrid) findComponentInRoot("gridStartStopConfirm");
		}
		return gridStartStopConfirm;
	}

	/**
	 * @managed-bean true
	 */
	protected SuspendResumeSubscriptionHandler getSuspendResumeHandler() {
		if (suspendResumeHandler == null) {
			suspendResumeHandler = (SuspendResumeSubscriptionHandler) getManagedBean("suspendResumeHandler");
		}
		return suspendResumeHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setSuspendResumeHandler(SuspendResumeSubscriptionHandler suspendResumeHandler) {
		this.suspendResumeHandler = suspendResumeHandler;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlPanelGroup getGroup1() {
		if (group1 == null) {
			group1 = (HtmlPanelGroup) findComponentInRoot("group1");
		}
		return group1;
	}

	protected HtmlPanelFormBox getFormBoxDetails() {
		if (formBoxDetails == null) {
			formBoxDetails = (HtmlPanelFormBox) findComponentInRoot("formBoxDetails");
		}
		return formBoxDetails;
	}

	protected HtmlFormItem getFormItemStopDate() {
		if (formItemStopDate == null) {
			formItemStopDate = (HtmlFormItem) findComponentInRoot("formItemStopDate");
		}
		return formItemStopDate;
	}

	protected HtmlFormItem getFormItemNoStopDate() {
		if (formItemNoStopDate == null) {
			formItemNoStopDate = (HtmlFormItem) findComponentInRoot("formItemNoStopDate");
		}
		return formItemNoStopDate;
	}

	protected HtmlOutputText getTextStopDate() {
		if (textStopDate == null) {
			textStopDate = (HtmlOutputText) findComponentInRoot("textStopDate");
		}
		return textStopDate;
	}

	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected HtmlFormItem getFormItemRestartDate() {
		if (formItemRestartDate == null) {
			formItemRestartDate = (HtmlFormItem) findComponentInRoot("formItemRestartDate");
		}
		return formItemRestartDate;
	}

	protected HtmlFormItem getFormItemNoRestartDate() {
		if (formItemNoRestartDate == null) {
			formItemNoRestartDate = (HtmlFormItem) findComponentInRoot("formItemNoRestartDate");
		}
		return formItemNoRestartDate;
	}

	protected HtmlFormItem getFormItemDonating() {
		if (formItemDonating == null) {
			formItemDonating = (HtmlFormItem) findComponentInRoot("formItemDonating");
		}
		return formItemDonating;
	}

	protected HtmlOutputText getText3() {
		if (text3 == null) {
			text3 = (HtmlOutputText) findComponentInRoot("text3");
		}
		return text3;
	}

	protected HtmlOutputText getText4() {
		if (text4 == null) {
			text4 = (HtmlOutputText) findComponentInRoot("text4");
		}
		return text4;
	}

	public String doButtonSaveTransactionAction() {
		String responseString = "success";
		// This is java code that runs when this action method is invoked
		CustomerHandler ch = this.getCustomerHandler();
		SuspendResumeSubscriptionHandler srh = this.getSuspendResumeHandler();
		// if initiating a vacation hold

		if (srh.isTransactionPlaced() || srh.getPostedTransactions() != null) {
			if (srh.getPostedTransactions() != null) {
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage facesMsg = new FacesMessage(
						FacesMessage.SEVERITY_INFO,
						"A Suspend/Resume Delivery transaction has already been placed during this session. Please start your transaction over if you would like to place an additional transaction.",
						null);

				context.addMessage(null, facesMsg);
				responseString = "startOver";
				srh.setTransactionPlaced(false);
				srh.getPostedTransactions().clear();
				srh.setPostedTransactions(null);
				return responseString;
			} else {
				// double click submit probably.
				try {
					Thread.currentThread().wait(4000);
				} catch (Exception e) {
					;
				}
				return "success";
			}
		} else {
			srh.setTransactionPlaced(true);
		}

		if (!ch.getCurrentAccount().getAccount().isSubscriptionSuspended()) {
			try {
				DateTime start = new DateTime(srh.getHoldStart().getTime());
				DateTime end = null;
				if (!srh.isUnsureOfResumeDate()) {
					if (srh.getHoldEnd() == null) {
						throw new UsatException(
								"Please choose a date to restart your subscription. If you are unsure of the restart date, check the box to indicate you would like an open ended hold.");
					}
					end = new DateTime(srh.getHoldEnd().getTime());
				}
//				boolean wasDonating = ch.getCurrentAccount().getAccount().isDonatingToNIE();
				boolean wasDonating = srh.isDonateToNIE();
				// Collection<ExtranetSubscriberTransactionIntf> trans =
				// CustomerService.suspendSubscription(ch.getCurrentAccount().getAccount(), start, end, srh.isDonateToNIE());
				GenesysResponse response = CustomerService.suspendSubscription(ch.getCurrentAccount().getAccount(), start, end,
						wasDonating);

				// if no errors
				if (!response.isContainsErrors()) {
					// complaintHandler.resetWebFormValuesOnly();
					// complaintHandler.setLastComplaint(c);

					try {
						// CustomerServiceEmails.sendComplaintConfirmationEmail(ch.getCustomer(), account, c);
					} catch (Exception e) {
						// ignore email failure;
						// System.out.println("Failed to send complaint confirmation email for account: " +
						// account.getAccountNumber() + ". " + e.getMessage());
					}
					responseString = "success";
				} else {
					// we got errors
					Collection<GenesysBaseAPIResponse> responses = response.getResponses();
					for (GenesysBaseAPIResponse res : responses) {
						if (res.redirectResponse()) {
							// System Error exists
							// complaintHandler.setRawAPIResponse(res.getRawResponse());
							// this.getDialogErrorDialog().setInitiallyShow(true);
							throw new Exception("Error Occurred: Unexpected Response Received. Please call Customer Service.");
						} else {
							// business rule errors exist
							Collection<String> errors = res.getErrorMessages();
							if (errors != null) {
								// StringBuilder errorBuf = new StringBuilder();
								for (String msg : errors) {
									FacesContext context = FacesContext.getCurrentInstance();
									FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, null);

									context.addMessage(null, facesMsg);
								}
								throw new Exception();
							} else {
								throw new Exception("Failed to create delivery issue record: Reason Unknown.");
							}
						}
					}
				}

				// srh.setPostedTransactions(trans);

				// send email
				String msg = null;
				try {
					if (UsaTodayConstants.debug) {
						System.out.println("Sending vaca hold confirmation email. 1");
					}
					msg = CustomerServiceEmails.sendSuspendResumeDeliveryEmail(ch.getCustomer(), start, end, srh.isDonateToNIE(),
							UTCommon.GLOBAL_EMAIL_PROMO_TEXT);
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("Email Message Send Failed for vaca hold 1: " + msg);
				}
			} catch (Exception e) {
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed to place subscription hold: "
						+ e.getMessage(), null);

				context.addMessage(null, facesMsg);
				responseString = "failure";
				srh.setTransactionPlaced(false);
			}
		} else { // resuming an open delivery hold
			try {
				DateTime end = null;
				if (srh.getHoldEnd() == null) {
					throw new UsatException("Invalid re-start date (null)");
				}

				end = new DateTime(srh.getHoldEnd().getTime());
//				boolean wasDonating = ch.getCurrentAccount().getAccount().isDonatingToNIE();
				boolean wasDonating = srh.isDonateToNIE();
				// Collection<ExtranetSubscriberTransactionIntf> trans =
				// CustomerService.resumeSubscription(ch.getCurrentAccount().getAccount(), start,end, wasDonating);
				// srh.setPostedTransactions(trans);

				GenesysResponse response = CustomerService
						.resumeSubscription(ch.getCurrentAccount().getAccount(), end, wasDonating);

				// if no errors
				if (!response.isContainsErrors()) {
					// complaintHandler.resetWebFormValuesOnly();
					// complaintHandler.setLastComplaint(c);

					try {
						// CustomerServiceEmails.sendComplaintConfirmationEmail(ch.getCustomer(), account, c);
					} catch (Exception e) {
						// ignore email failure;
						// System.out.println("Failed to send complaint confirmation email for account: " +
						// account.getAccountNumber() + ". " + e.getMessage());
					}
					responseString = "success";
				} else {
					// we got errors
					Collection<GenesysBaseAPIResponse> responses = response.getResponses();
					for (GenesysBaseAPIResponse res : responses) {
						if (res.redirectResponse()) {
							// System Error exists
							// complaintHandler.setRawAPIResponse(res.getRawResponse());
							// this.getDialogErrorDialog().setInitiallyShow(true);
							throw new Exception("Error Occurred: Unexpected Response Received. Please call Customer Service.");
						} else {
							// business rule errors exist
							Collection<String> errors = res.getErrorMessages();
							if (errors != null) {
								// StringBuilder errorBuf = new StringBuilder();
								for (String msg : errors) {
									FacesContext context = FacesContext.getCurrentInstance();
									FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, null);

									context.addMessage(null, facesMsg);
								}
								throw new Exception();
							} else {
								throw new Exception("Failed to create delivery issue record: Reason Unknown.");
							}
						}
					}
				}

				// send email
				String msg = null;
				try {
					if (UsaTodayConstants.debug) {
						System.out.println("Sending vaca hold confirmation email. 2");
					}
					msg = CustomerServiceEmails.sendSuspendResumeDeliveryEmail(ch.getCustomer(), null, end, srh.isDonateToNIE(),
							UTCommon.GLOBAL_EMAIL_PROMO_TEXT);
				} catch (Exception e) {
					e.printStackTrace(); // ignore it
					System.out.println("Email Message Send Failed for vaca hold 2: " + msg);
				}

			} catch (Exception e) {
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed to place subscription re-start: "
						+ e.getMessage(), null);

				context.addMessage(null, facesMsg);
				responseString = "failure";
				srh.setTransactionPlaced(false);
			}

		} // end else

		return responseString;
	}

	/**
	 * @managed-bean true
	 */
	protected CustomerHandler getCustomerHandler() {
		if (customerHandler == null) {
			customerHandler = (CustomerHandler) getManagedBean("customerHandler");
		}
		return customerHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setCustomerHandler(CustomerHandler customerHandler) {
		this.customerHandler = customerHandler;
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

	protected HtmlOutputText getOutputTextDonateStatus() {
		if (outputTextDonateStatus == null) {
			outputTextDonateStatus = (HtmlOutputText) findComponentInRoot("outputTextDonateStatus");
		}
		return outputTextDonateStatus;
	}

	protected HtmlCommandExButton getButtonSaveTransaction() {
		if (buttonSaveTransaction == null) {
			buttonSaveTransaction = (HtmlCommandExButton) findComponentInRoot("buttonSaveTransaction");
		}
		return buttonSaveTransaction;
	}

}