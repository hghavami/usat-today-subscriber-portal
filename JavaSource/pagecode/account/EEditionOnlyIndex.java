/**
 * 
 */
package pagecode.account;

import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;

import pagecode.PageCodeBase;

import com.ibm.faces.component.html.HtmlGraphicImageEx;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.ibm.faces.component.html.HtmlJspPanel;
import javax.faces.context.FacesContext;
import com.usatoday.esub.handlers.NavigationLinksHandler;
import com.usatoday.esub.ncs.handlers.CustomerHandler;

import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.component.html.HtmlInputHidden;

/**
 * @author aeast
 * 
 */
public class EEditionOnlyIndex extends PageCodeBase {

	protected CustomerHandler customerHandler;
	protected HtmlPanelGrid gridAccountInformation;
	protected HtmlOutputText textAccountLabel;
	protected HtmlOutputText textAccountNumber;
	protected HtmlOutputText textNameLabel;
	protected HtmlOutputText text3;
	protected HtmlOutputText textStartDateLabel;
	protected HtmlOutputText textStartDate;
	protected HtmlOutputText textSubscriptionEnd;
	protected HtmlOutputText textExpirationDate;
	protected HtmlOutputText textEZPayLabel;
	protected HtmlOutputText textOnEZPay;
	protected HtmlOutputText textFAQ;
	protected HtmlScriptCollector scriptCollectorMainForm;
	protected HtmlForm form1;
	protected HtmlScriptCollector scriptCollectorTemplateCollectorLeftNav;
	protected HtmlPanelGrid gridNavigationOuterGrid;
	protected HtmlPanelGrid gridLeftNavGroup1Grid;
	protected HtmlOutputLinkEx linkExNavGroup1SubscriberServices;
	protected HtmlOutputText textNavGroup1LinkLabel1;
	protected HtmlOutputText textNavGroup1LinkLabelInternational;
	protected HtmlOutputLinkEx linkExNavGroup2International;
	protected HtmlPanelGrid gridLeftNavGroup2Grid;
	protected HtmlPanelGrid gridMainBodyGrid;
	protected HtmlForm formMainForm;
	protected NavigationLinksHandler eeditionTemplateNavHandler;
	protected HtmlOutputLinkEx linkExNavArea2Link1;
	protected HtmlOutputText textNavArea2Link1;
	protected HtmlOutputText textNavArea2Link2;
	protected HtmlOutputText textNavArea2Link3;
	protected HtmlOutputText textNavArea2Link4;
	protected HtmlOutputLinkEx linkExNavArea2Link2;
	protected HtmlOutputLinkEx linkExNavArea2Link3;
	protected HtmlOutputLinkEx linkExNavArea2Link4;
	protected HtmlOutputLinkEx linkExReadNowLink;
	protected HtmlGraphicImageEx imageExReadNowButtonImage;
	protected HtmlOutputText textAPlaceHolder;
	protected HtmlOutputText textUpdateEmailPreferenceLinkText;
	protected HtmlOutputLinkEx linkExUpdateEmailPreferencesLink;
	protected HtmlOutputLinkEx linkExReadNowLink_2;
	protected HtmlPanelGroup groupSpecialOfferGroupBox;
	protected HtmlPanelGrid gridSpecialOfferLayoutGrid;
	protected HtmlOutputLinkEx linkExCloseSpecialOffer;
	protected HtmlOutputText textCloseSpecialOfferText;
	protected HtmlGraphicImageEx imageExSpecialOfferImage;
	protected HtmlJspPanel jspPanelPopOverlayPanel;
	protected HtmlOutputLinkEx linkExSpecialOfferLink;
	protected HtmlInputHidden showOverlayPopUp;
	protected HtmlOutputLinkEx linkExRenewalLink;
	protected HtmlGraphicImageEx imageExRenewalButton;
	protected HtmlOutputText mainHeader;

	/**
	 * @managed-bean true
	 */
	protected CustomerHandler getCustomerHandler() {
		if (customerHandler == null) {
			customerHandler = (CustomerHandler) getManagedBean("customerHandler");
		}
		return customerHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setCustomerHandler(CustomerHandler customerHandler) {
		this.customerHandler = customerHandler;
	}

	protected HtmlPanelGrid getGridAccountInformation() {
		if (gridAccountInformation == null) {
			gridAccountInformation = (HtmlPanelGrid) findComponentInRoot("gridAccountInformation");
		}
		return gridAccountInformation;
	}

	protected HtmlOutputText getTextAccountLabel() {
		if (textAccountLabel == null) {
			textAccountLabel = (HtmlOutputText) findComponentInRoot("textAccountLabel");
		}
		return textAccountLabel;
	}

	protected HtmlOutputText getTextAccountNumber() {
		if (textAccountNumber == null) {
			textAccountNumber = (HtmlOutputText) findComponentInRoot("textAccountNumber");
		}
		return textAccountNumber;
	}

	protected HtmlOutputText getTextNameLabel() {
		if (textNameLabel == null) {
			textNameLabel = (HtmlOutputText) findComponentInRoot("textNameLabel");
		}
		return textNameLabel;
	}

	protected HtmlOutputText getText3() {
		if (text3 == null) {
			text3 = (HtmlOutputText) findComponentInRoot("text3");
		}
		return text3;
	}

	protected HtmlOutputText getTextStartDateLabel() {
		if (textStartDateLabel == null) {
			textStartDateLabel = (HtmlOutputText) findComponentInRoot("textStartDateLabel");
		}
		return textStartDateLabel;
	}

	protected HtmlOutputText getTextStartDate() {
		if (textStartDate == null) {
			textStartDate = (HtmlOutputText) findComponentInRoot("textStartDate");
		}
		return textStartDate;
	}

	protected HtmlOutputText getTextSubscriptionEnd() {
		if (textSubscriptionEnd == null) {
			textSubscriptionEnd = (HtmlOutputText) findComponentInRoot("textSubscriptionEnd");
		}
		return textSubscriptionEnd;
	}

	protected HtmlOutputText getTextExpirationDate() {
		if (textExpirationDate == null) {
			textExpirationDate = (HtmlOutputText) findComponentInRoot("textExpirationDate");
		}
		return textExpirationDate;
	}

	protected HtmlOutputText getTextEZPayLabel() {
		if (textEZPayLabel == null) {
			textEZPayLabel = (HtmlOutputText) findComponentInRoot("textEZPayLabel");
		}
		return textEZPayLabel;
	}

	protected HtmlOutputText getTextOnEZPay() {
		if (textOnEZPay == null) {
			textOnEZPay = (HtmlOutputText) findComponentInRoot("textOnEZPay");
		}
		return textOnEZPay;
	}

	protected HtmlOutputText getTextFAQ() {
		if (textFAQ == null) {
			textFAQ = (HtmlOutputText) findComponentInRoot("textFAQ");
		}
		return textFAQ;
	}

	protected HtmlScriptCollector getScriptCollectorMainForm() {
		if (scriptCollectorMainForm == null) {
			scriptCollectorMainForm = (HtmlScriptCollector) findComponentInRoot("scriptCollectorMainForm");
		}
		return scriptCollectorMainForm;
	}

	protected HtmlForm getForm1() {
		if (form1 == null) {
			form1 = (HtmlForm) findComponentInRoot("form1");
		}
		return form1;
	}

	protected HtmlScriptCollector getScriptCollectorTemplateCollectorLeftNav() {
		if (scriptCollectorTemplateCollectorLeftNav == null) {
			scriptCollectorTemplateCollectorLeftNav = (HtmlScriptCollector) findComponentInRoot("scriptCollectorTemplateCollectorLeftNav");
		}
		return scriptCollectorTemplateCollectorLeftNav;
	}

	protected HtmlPanelGrid getGridNavigationOuterGrid() {
		if (gridNavigationOuterGrid == null) {
			gridNavigationOuterGrid = (HtmlPanelGrid) findComponentInRoot("gridNavigationOuterGrid");
		}
		return gridNavigationOuterGrid;
	}

	protected HtmlPanelGrid getGridLeftNavGroup1Grid() {
		if (gridLeftNavGroup1Grid == null) {
			gridLeftNavGroup1Grid = (HtmlPanelGrid) findComponentInRoot("gridLeftNavGroup1Grid");
		}
		return gridLeftNavGroup1Grid;
	}

	protected HtmlOutputLinkEx getLinkExNavGroup1SubscriberServices() {
		if (linkExNavGroup1SubscriberServices == null) {
			linkExNavGroup1SubscriberServices = (HtmlOutputLinkEx) findComponentInRoot("linkExNavGroup1SubscriberServices");
		}
		return linkExNavGroup1SubscriberServices;
	}

	protected HtmlOutputText getTextNavGroup1LinkLabel1() {
		if (textNavGroup1LinkLabel1 == null) {
			textNavGroup1LinkLabel1 = (HtmlOutputText) findComponentInRoot("textNavGroup1LinkLabel1");
		}
		return textNavGroup1LinkLabel1;
	}

	protected HtmlOutputText getTextNavGroup1LinkLabelInternational() {
		if (textNavGroup1LinkLabelInternational == null) {
			textNavGroup1LinkLabelInternational = (HtmlOutputText) findComponentInRoot("textNavGroup1LinkLabelInternational");
		}
		return textNavGroup1LinkLabelInternational;
	}

	protected HtmlOutputLinkEx getLinkExNavGroup2International() {
		if (linkExNavGroup2International == null) {
			linkExNavGroup2International = (HtmlOutputLinkEx) findComponentInRoot("linkExNavGroup2International");
		}
		return linkExNavGroup2International;
	}

	protected HtmlPanelGrid getGridLeftNavGroup2Grid() {
		if (gridLeftNavGroup2Grid == null) {
			gridLeftNavGroup2Grid = (HtmlPanelGrid) findComponentInRoot("gridLeftNavGroup2Grid");
		}
		return gridLeftNavGroup2Grid;
	}

	protected HtmlPanelGrid getGridMainBodyGrid() {
		if (gridMainBodyGrid == null) {
			gridMainBodyGrid = (HtmlPanelGrid) findComponentInRoot("gridMainBodyGrid");
		}
		return gridMainBodyGrid;
	}

	protected HtmlForm getFormMainForm() {
		if (formMainForm == null) {
			formMainForm = (HtmlForm) findComponentInRoot("formMainForm");
		}
		return formMainForm;
	}

	public void onPageLoadBegin(FacesContext facescontext) {

		// void <method>(FacesContext facescontext)
		this.getEeditionTemplateNavHandler().setShowNavHeaderArea1(false);
		this.getEeditionTemplateNavHandler().setShowNavHeaderArea2(true);

	}

	/**
	 * @managed-bean true
	 */
	protected NavigationLinksHandler getEeditionTemplateNavHandler() {
		if (eeditionTemplateNavHandler == null) {
			eeditionTemplateNavHandler = (NavigationLinksHandler) getManagedBean("eeditionTemplateNavHandler");
		}
		return eeditionTemplateNavHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setEeditionTemplateNavHandler(NavigationLinksHandler eeditionTemplateNavHandler) {
		this.eeditionTemplateNavHandler = eeditionTemplateNavHandler;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link1() {
		if (linkExNavArea2Link1 == null) {
			linkExNavArea2Link1 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link1");
		}
		return linkExNavArea2Link1;
	}

	protected HtmlOutputText getTextNavArea2Link1() {
		if (textNavArea2Link1 == null) {
			textNavArea2Link1 = (HtmlOutputText) findComponentInRoot("textNavArea2Link1");
		}
		return textNavArea2Link1;
	}

	protected HtmlOutputText getTextNavArea2Link2() {
		if (textNavArea2Link2 == null) {
			textNavArea2Link2 = (HtmlOutputText) findComponentInRoot("textNavArea2Link2");
		}
		return textNavArea2Link2;
	}

	protected HtmlOutputText getTextNavArea2Link3() {
		if (textNavArea2Link3 == null) {
			textNavArea2Link3 = (HtmlOutputText) findComponentInRoot("textNavArea2Link3");
		}
		return textNavArea2Link3;
	}

	protected HtmlOutputText getTextNavArea2Link4() {
		if (textNavArea2Link4 == null) {
			textNavArea2Link4 = (HtmlOutputText) findComponentInRoot("textNavArea2Link4");
		}
		return textNavArea2Link4;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link2() {
		if (linkExNavArea2Link2 == null) {
			linkExNavArea2Link2 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link2");
		}
		return linkExNavArea2Link2;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link3() {
		if (linkExNavArea2Link3 == null) {
			linkExNavArea2Link3 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link3");
		}
		return linkExNavArea2Link3;
	}

	protected HtmlOutputLinkEx getLinkExNavArea2Link4() {
		if (linkExNavArea2Link4 == null) {
			linkExNavArea2Link4 = (HtmlOutputLinkEx) findComponentInRoot("linkExNavArea2Link4");
		}
		return linkExNavArea2Link4;
	}

	protected HtmlOutputLinkEx getLinkExReadNowLink() {
		if (linkExReadNowLink == null) {
			linkExReadNowLink = (HtmlOutputLinkEx) findComponentInRoot("linkExReadNowLink");
		}
		return linkExReadNowLink;
	}

	protected HtmlGraphicImageEx getImageExReadNowButtonImage() {
		if (imageExReadNowButtonImage == null) {
			imageExReadNowButtonImage = (HtmlGraphicImageEx) findComponentInRoot("imageExReadNowButtonImage");
		}
		return imageExReadNowButtonImage;
	}

	protected HtmlOutputText getTextAPlaceHolder() {
		if (textAPlaceHolder == null) {
			textAPlaceHolder = (HtmlOutputText) findComponentInRoot("textAPlaceHolder");
		}
		return textAPlaceHolder;
	}

	protected HtmlOutputText getTextUpdateEmailPreferenceLinkText() {
		if (textUpdateEmailPreferenceLinkText == null) {
			textUpdateEmailPreferenceLinkText = (HtmlOutputText) findComponentInRoot("textUpdateEmailPreferenceLinkText");
		}
		return textUpdateEmailPreferenceLinkText;
	}

	protected HtmlOutputLinkEx getLinkExUpdateEmailPreferencesLink() {
		if (linkExUpdateEmailPreferencesLink == null) {
			linkExUpdateEmailPreferencesLink = (HtmlOutputLinkEx) findComponentInRoot("linkExUpdateEmailPreferencesLink");
		}
		return linkExUpdateEmailPreferencesLink;
	}

	protected HtmlOutputLinkEx getLinkExReadNowLink_2() {
		if (linkExReadNowLink_2 == null) {
			linkExReadNowLink_2 = (HtmlOutputLinkEx) findComponentInRoot("linkExReadNowLink_2");
		}
		return linkExReadNowLink_2;
	}

	protected HtmlPanelGroup getGroupSpecialOfferGroupBox() {
		if (groupSpecialOfferGroupBox == null) {
			groupSpecialOfferGroupBox = (HtmlPanelGroup) findComponentInRoot("groupSpecialOfferGroupBox");
		}
		return groupSpecialOfferGroupBox;
	}

	protected HtmlPanelGrid getGridSpecialOfferLayoutGrid() {
		if (gridSpecialOfferLayoutGrid == null) {
			gridSpecialOfferLayoutGrid = (HtmlPanelGrid) findComponentInRoot("gridSpecialOfferLayoutGrid");
		}
		return gridSpecialOfferLayoutGrid;
	}

	protected HtmlOutputLinkEx getLinkExCloseSpecialOffer() {
		if (linkExCloseSpecialOffer == null) {
			linkExCloseSpecialOffer = (HtmlOutputLinkEx) findComponentInRoot("linkExCloseSpecialOffer");
		}
		return linkExCloseSpecialOffer;
	}

	protected HtmlOutputText getTextCloseSpecialOfferText() {
		if (textCloseSpecialOfferText == null) {
			textCloseSpecialOfferText = (HtmlOutputText) findComponentInRoot("textCloseSpecialOfferText");
		}
		return textCloseSpecialOfferText;
	}

	protected HtmlGraphicImageEx getImageExSpecialOfferImage() {
		if (imageExSpecialOfferImage == null) {
			imageExSpecialOfferImage = (HtmlGraphicImageEx) findComponentInRoot("imageExSpecialOfferImage");
		}
		return imageExSpecialOfferImage;
	}

	protected HtmlJspPanel getJspPanelPopOverlayPanel() {
		if (jspPanelPopOverlayPanel == null) {
			jspPanelPopOverlayPanel = (HtmlJspPanel) findComponentInRoot("jspPanelPopOverlayPanel");
		}
		return jspPanelPopOverlayPanel;
	}

	protected HtmlOutputLinkEx getLinkExSpecialOfferLink() {
		if (linkExSpecialOfferLink == null) {
			linkExSpecialOfferLink = (HtmlOutputLinkEx) findComponentInRoot("linkExSpecialOfferLink");
		}
		return linkExSpecialOfferLink;
	}

	protected HtmlInputHidden getShowOverlayPopUp() {
		if (showOverlayPopUp == null) {
			showOverlayPopUp = (HtmlInputHidden) findComponentInRoot("showOverlayPopUp");
		}
		return showOverlayPopUp;
	}

	protected HtmlOutputLinkEx getLinkExRenewalLink() {
		if (linkExRenewalLink == null) {
			linkExRenewalLink = (HtmlOutputLinkEx) findComponentInRoot("linkExRenewalLink");
		}
		return linkExRenewalLink;
	}

	protected HtmlGraphicImageEx getImageExRenewalButton() {
		if (imageExRenewalButton == null) {
			imageExRenewalButton = (HtmlGraphicImageEx) findComponentInRoot("imageExRenewalButton");
		}
		return imageExRenewalButton;
	}

	protected HtmlOutputText getMainHeader() {
		if (mainHeader == null) {
			mainHeader = (HtmlOutputText) findComponentInRoot("mainHeader");
		}
		return mainHeader;
	}

}