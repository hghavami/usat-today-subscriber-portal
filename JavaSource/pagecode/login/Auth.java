/**
 * 
 */
package pagecode.login;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputSecret;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.component.html.HtmlOutputLink;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.joda.time.DateTime;

import pagecode.PageCodeBase;

import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlInputHelperSetFocus;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.business.interfaces.customer.CustomerIntf;
import com.usatoday.business.interfaces.customer.EmailRecordIntf;
import com.usatoday.business.interfaces.customer.TrialCustomerIntf;
import com.usatoday.business.interfaces.products.SubscriptionProductIntf;
import com.usatoday.businessObjects.customer.CustomerBO;
import com.usatoday.businessObjects.customer.LoginException;
import com.usatoday.businessObjects.customer.TrialCustomerBO;
import com.usatoday.businessObjects.products.SubscriptionProductBO;
import com.usatoday.businessObjects.products.utils.ElectronicEditionFulfillmentHelper;
import com.usatoday.businessObjects.util.UsatDateTimeFormatter;
import com.usatoday.esub.common.UTCommon;
import com.usatoday.esub.cookies.AuthenticationCookieProcessor;
import com.usatoday.esub.electronic.utils.EReaderOpenRecorder;
import com.usatoday.esub.ncs.handlers.CancelsHandler;
import com.usatoday.esub.ncs.handlers.CustomerHandler;
import com.usatoday.esub.ncs.handlers.LoginPageFormHandler;
import com.usatoday.esub.trials.handlers.PartnerHandler;
import com.usatoday.esub.trials.handlers.TrialCustomerHandler;
import com.usatoday.util.MobileDeviceDetectionUtil;
import com.usatoday.util.constants.UsaTodayConstants;

/**
 * @author swong
 * 
 */
public class Auth extends PageCodeBase {

	private static final long serialVersionUID = 5939963829227112950L;
	// public static final String SUCCESS_URL = "SUCCESSURL";
	// public static final String FAILURE_URL = "FAILUREURL";
	// public static final String SELECT_URL = "SELECTURL";
	public static final String LOGIN_ID = "LOGIN_ID";
	public static final String LOGIN_PASSWORD = "LOGIN_PASSWORD";

	protected HtmlJspPanel eReaderLinkPanel;
	protected HtmlJspPanel RenewRequiredPanel;
	protected HtmlJspPanel eReaderLinkBWPanel;
	protected HtmlOutputText mainHeader;
	protected HtmlScriptCollector scriptCollector1;
	protected LoginPageFormHandler loginFormHandler;
	protected HtmlForm formAuth;
	protected HtmlPanelGrid gridEmail;
	protected HtmlPanelGroup group1;
	protected HtmlPanelGrid grid1;
	protected HtmlOutputText loginHeader;
	protected HtmlCommandExButton loginButton;
	protected HtmlOutputText text1;
	protected HtmlOutputLink link1;
	protected HtmlOutputLink linkRememberMe;
	protected HtmlOutputText textRememberMe;
	protected HtmlSelectBooleanCheckbox checkboxRemember;
	protected HtmlPanelGrid grid3;
	protected HtmlSelectBooleanCheckbox checkbox1;
	protected HtmlInputText textEmail;
	protected HtmlOutputLabel label2;
	protected HtmlMessages messagesAuth;
	protected HtmlInputSecret secretPass;
	protected HtmlOutputLabel label3;
	protected HtmlOutputLabel labelPass;
	protected HtmlInputHelperSetFocus setFocus1;
	protected HtmlOutputLabel label1;
	protected HtmlOutputText text5;
	protected HtmlPanelGrid grid2;
	protected HtmlOutputLink linkEELearn;
	protected HtmlOutputLabel label4;
	protected HtmlOutputText text3;
	protected HtmlPanelGrid grid4;
	protected HtmlOutputLink linkOnline;
	protected HtmlOutputLabel label5;
	protected HtmlOutputText text4;
	protected HtmlPanelGrid grid5;
	protected HtmlOutputLink linkContact;

	protected HtmlOutputText getMainHeader() {
		if (mainHeader == null) {
			mainHeader = (HtmlOutputText) findComponentInRoot("mainHeader");
		}
		return mainHeader;
	}

	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	public String doButton1Action() {
		// This is java code that runs when this action method is invoked
		// user not logged in; retrieve the input parameters
		// This is java code that runs when this action method is invoked

		String responseString = "failure";

		try {

			HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
			CustomerIntf customer = null;

			int numberAccounts = 0;

			HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();

			HttpServletResponse res = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();

			// Retrieve Promotional parameters
			// Accounts selected will be the same publication as the promotion link selected SEW

			// user not logged in; retrieve the input parameters
			String id = this.getLoginFormHandler().getEmailAddress();
			String password = this.getLoginFormHandler().getPassword();

			// following bit of code removes white space on either side..for when people cut and paste
			if (id != null) {
				id = id.trim();
				this.getLoginFormHandler().setEmailAddress(id);
			}
			if (password != null) {
				password = password.trim();
				this.getLoginFormHandler().setPassword(password);
			}

			boolean rememberMe = false;

			if (this.getLoginFormHandler().isRememberMe()) {
				rememberMe = true;
			}

			try {
				boolean multipleAccounts = false;

				customer = CustomerBO.loginCustomer(id, password);

				if (customer != null && customer.getHasActiveAccounts()) {

					numberAccounts = customer.getNumberOfAccounts();
					int numberEmailRecords = customer.getEmailRecords().size();

					// Check for multiple accounts
					if (numberAccounts > 1 || numberEmailRecords > 1) {
						multipleAccounts = true;
					}

					session.setAttribute(UTCommon.SESSION_CUSTOMER_INFO, customer);
					CustomerHandler ch = null;
					ch = (CustomerHandler) session.getAttribute("customerHandler");
					if (ch == null) {
						ch = new CustomerHandler();
						session.setAttribute("customerHandler", ch);
					}

					// Clear the cancels handler
					CancelsHandler cnh = (CancelsHandler) session.getAttribute("cancelsHandler");
					if (cnh != null) {
						cnh.setCustomer(customer);
						cnh.setComment1("");
						cnh.setComment2("");
						cnh.setComment3("");
						cnh.setCancelReason("");
						cnh.setCancelCode("");
						cnh.setOnEZPay("");
					}

					javax.servlet.http.Cookie c = null;
					javax.servlet.http.Cookie c1 = null;
					javax.servlet.http.Cookie c2 = null;
					if (rememberMe) {
						AuthenticationCookieProcessor cookieProcessor = new AuthenticationCookieProcessor();
						c = cookieProcessor.createRememberMeCookie(id);
						((HttpServletResponse) res).addCookie(c);
						// Atypon autologin cookie
						cookieProcessor = new AuthenticationCookieProcessor();
						c1 = cookieProcessor.createAtyponAutoLoginCookie(customer.getCurrentEmailRecord().getAutoLogin());
						((HttpServletResponse) res).addCookie(c1);
						// Atypon session key cookie
						cookieProcessor = new AuthenticationCookieProcessor();
						c2 = cookieProcessor.createAtyponSessionKeyCookie(customer.getCurrentEmailRecord().getSessionKey());
						((HttpServletResponse) res).addCookie(c2);
					}

					ch.setRememberMe(rememberMe);
					ch.setUserEnteredPwd(password);
					ch.setUserEnteredUserID(id);
					ch.setCustomer(customer);
					ch.setAuthenticated(true);
					ch.setCookieAuthenticated(false);

					if (multipleAccounts) {
						// String result = null;

						// display the next 'successful account' jsp
						responseString = "multiAccounts";
						return responseString;

						// handle single accounts SEW
					} else {
						// Check for next page being set

						if (customer != null) {
							//
							// make sure pub code is set properly
							SubscriptionProductIntf product = SubscriptionProductBO.getSubscriptionProduct(customer
									.getCurrentEmailRecord().getPubCode());

							session.removeAttribute(UTCommon.SESSION_PUB_NAME);
							session.setAttribute(UTCommon.SESSION_PUB_NAME, product.getBrandingPubCode());

							// APE - Check for eEdition Only Account and override redirect
							// will return true if all eEdition products or a new print account.
							Boolean directToElectronic = (Boolean) req.getSession().getAttribute("eEditionDirectTOPaperFlag");
							req.getSession().removeAttribute("eEditionDirectTOPaperFlag");

							if (directToElectronic != null && (directToElectronic.booleanValue() == true)) {
								// redirect to eEdition automatically

								String fromParam = (String) req.getSession().getAttribute("eEditionRedirectURL");
								req.getSession().removeAttribute("eEditionRedirectURL");

								boolean useAlternate = MobileDeviceDetectionUtil.isMobileDevice(req);

								String eEditionLink = null;

								if (fromParam != null && fromParam.length() > 0) {
									eEditionLink = ElectronicEditionFulfillmentHelper.buildOliveFulfillmentURL(product, customer
											.getCurrentEmailRecord().getEmailAddress(), fromParam, null, useAlternate);
								} else {
									eEditionLink = ElectronicEditionFulfillmentHelper.buildOliveFulfillmentURL(product, customer
											.getCurrentEmailRecord().getEmailAddress(), null, null, useAlternate);
								}

								if (eEditionLink != null && (eEditionLink.length() > 0)) {
									// record open send redirect
									EReaderOpenRecorder.recordEReaderOpen(req);

									// JSF redirect
									FacesContext.getCurrentInstance().getExternalContext().redirect(eEditionLink);

									// old redirect
									// res.sendRedirect(eEditionLink);
									// return
									responseString = "success";
									return responseString;
								} else {
									responseString = "success";
									return responseString;
								}

							} else if (product.isElectronicDelivery()) {
								if (customer.getNumberOfAccounts() == 1) {
									responseString = "success";
									return responseString;
								} else {
									// account not fully set up yet
									responseString = "eEditionNewAccountIndex";
									return responseString;
								}
							} else {
								// print subscription
								if (product.getProductCode().equalsIgnoreCase(UsaTodayConstants.UT_PUBCODE)
										&& customer.getNumberOfAccounts() == 0) {
									responseString = "eEditionNewAccountIndex";
									return responseString;
								} else if (product.getProductCode().equalsIgnoreCase(UsaTodayConstants.BW_PUBCODE)
										&& customer.getNumberOfAccounts() == 0) {
									// SW Print..no eEdition go to failure URL
									// session.removeAttribute("LOGIN_FAILED_MESSAGE");
									// session.setAttribute("LOGIN_FAILED_MESSAGE",
									// "Invalid E-mail / password combination. It can take up to 5 days for your online account access to become available.");
									session.removeAttribute(UTCommon.SESSION_CUSTOMER_INFO);
									ch.setCustomer(null);
									if (c != null) {
										AuthenticationCookieProcessor cookieProcessor = new AuthenticationCookieProcessor();
										c = cookieProcessor.setCookieForDeletion(c);
										// res.addCookie(c);
										((HttpServletResponse) res).addCookie(c);
									}
									if (c1 != null) {
										AuthenticationCookieProcessor cookieProcessor = new AuthenticationCookieProcessor();
										c1 = cookieProcessor.setCookieForDeletion(c1);
										((HttpServletResponse) res).addCookie(c1);
									}
									if (c2 != null) {
										AuthenticationCookieProcessor cookieProcessor = new AuthenticationCookieProcessor();
										c2 = cookieProcessor.setCookieForDeletion(c2);
										((HttpServletResponse) res).addCookie(c2);
									}
									ch.setAuthenticated(false);
									FacesContext
											.getCurrentInstance()
											.addMessage(
													"formAuth:messagesAuth",
													new FacesMessage(
															"Invalid E-mail / password combination. It can take up to 5 days for your online account access to become available."));
									responseString = "failure";
									return responseString;
								}
							}

							// UTCommon.showUrl((HttpServletResponse) res, successURL);
							responseString = "success";
							return responseString;

						}// end custo=null
					}

				} else {
					// throw an exception to get into exception processing .. checking of trial subscriber.
					// this is done because changes to using perm start date will return a valid customer even if there are no
					// active accounts.
					throw new Exception("whatever");
				}

			} catch (Exception e) {

				if (e instanceof IllegalArgumentException) {
					System.out.println("LoginServlet:doPost() - Illegal Argument Exception. id: " + id + "  pwd: " + password);
					if (req != null) {
						// System.out.println("LoginServlet:doPost() -  Page Requested:" + req.getRequestURL() + " SuccessURL: " +
						// successURL + " FailureURL: " + failureURL + " SelectURL: " + selectURL);
						System.out.println("Auth.Faces command button - Login error");
					}
				}

				// if not a valid subscriber, check trial database table
				TrialCustomerIntf trialCustomer = null;
				TrialCustomerHandler tch = null;

				try {
					tch = (TrialCustomerHandler) req.getSession().getAttribute("trialCustomerHandler");
					if (tch == null) {
						tch = new TrialCustomerHandler();
						req.getSession().setAttribute("trialCustomerHandler", tch);
					}

					trialCustomer = TrialCustomerBO.getTrialCustomerByEmailPassword(id, password);

					if (trialCustomer != null) {
						tch.setTrialCustomer(trialCustomer);

						PartnerHandler ph = (PartnerHandler) req.getSession().getAttribute("trialPartnerHandler");
						if (ph == null) {
							ph = new PartnerHandler();
							req.getSession().setAttribute("trialPartnerHandler", ph);
						}
						ph.setPartner(trialCustomer.getCurrentInstance().getPartner());

						if (trialCustomer.getCurrentInstance().getIsTrialPeriodExpired()) {
							// UTCommon.showUrl(res, "/eAccount/trials/trialOver.faces");
							responseString = "trialOver";
							return responseString;
						} else {
							if (rememberMe) {
								AuthenticationCookieProcessor cookieProcessor = new AuthenticationCookieProcessor();
								try {
									javax.servlet.http.Cookie c = cookieProcessor.createRememberMeTrialCookie(id, trialCustomer
											.getCurrentInstance().getPartner().getDurationInDays());
									res.addCookie(c);
								} catch (Exception ce) {
									; // ignore
								}
							}
							// UTCommon.showUrl(res, "/eAccount/trials/trialReaderLaunchPage.faces");
							responseString = "trialReaderLaunchPage";
							return responseString;
						}

					}

				} catch (Exception e2) {
					; // ignore any trial login exceptions
				}

				// no data entered for either field!

				session.removeAttribute("LOGIN_FAILED_MESSAGE");
				StringBuilder loginFailedBuilder = new StringBuilder();
				if (e instanceof LoginException) {
					LoginException le = (LoginException) e;
					loginFailedBuilder.append(le.getErrorMessage());
				}

				if (customer != null && !customer.getHasActiveAccounts()) {
					if (customer.getHasFutureStarts()) {
						// clear the login failed message.
						loginFailedBuilder = new StringBuilder();
						if (customer.getEmailRecords().size() > 1) {
							loginFailedBuilder
									.append("The accounts associated with your email address are not yet active. You have accounts starting on ");

							int count = 0;
							int numberRecords = customer.getEmailRecords().size();
							for (EmailRecordIntf em : customer.getEmailRecords().values()) {
								count++;
								if (em.isStartDateInFuture()) {
									DateTime dt = UsatDateTimeFormatter.convertYYYYMMDDToDateTime(em.getPermStartDate());

									loginFailedBuilder.append(dt.toString("MM/dd/yyyy"));

									if (count < numberRecords) {
										loginFailedBuilder.append(", ");
									}
								}
							}
							loginFailedBuilder
									.append(". You may access this site once a subscription starts. If you need immediate assistance, please contact National Customer Service at 800-872-0001.");
						} else {
							EmailRecordIntf em = customer.getCurrentEmailRecord();
							loginFailedBuilder
									.append("The account associated with your email address is not yet active. Your account is scheduled to begin on ");
							DateTime dt = UsatDateTimeFormatter.convertYYYYMMDDToDateTime(em.getPermStartDate());
							loginFailedBuilder.append(dt.toString("MM/dd/yyyy"));
							loginFailedBuilder
									.append("; you may access this site once your subscription starts. If you need immediate assistance, please contact National Customer Service at 800-872-0001.");
						}
					} else {
						loginFailedBuilder
								.append(" Your email address was found in our database, but their are no active accounts associated with it. You may need to <a href=\"/firsttime/first_time.jsp\">Set Up Online Account Access</a>, or if your account has expired, you may place a new order. For assistance, contact our National Customer Service.");
					}
				}

				session.setAttribute("LOGIN_FAILED_MESSAGE", loginFailedBuilder.toString());

				FacesContext.getCurrentInstance().addMessage("formAuth:messagesAuth",
						new FacesMessage(loginFailedBuilder.toString()));
				responseString = "failure";
				return responseString;

			}

		} catch (Exception e) {

		}

		// Return an outcome that corresponds to a navigation rule
		return responseString;
	}

	/**
	 * @managed-bean true
	 */
	protected LoginPageFormHandler getLoginFormHandler() {
		if (loginFormHandler == null) {
			loginFormHandler = (LoginPageFormHandler) getManagedBean("loginFormHandler");
		}
		return loginFormHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setLoginFormHandler(LoginPageFormHandler loginFormHandler) {
		this.loginFormHandler = loginFormHandler;
	}

	protected HtmlForm getFormAuth() {
		if (formAuth == null) {
			formAuth = (HtmlForm) findComponentInRoot("formAuth");
		}
		return formAuth;
	}

	protected HtmlPanelGrid getGridEmail() {
		if (gridEmail == null) {
			gridEmail = (HtmlPanelGrid) findComponentInRoot("gridEmail");
		}
		return gridEmail;
	}

	protected HtmlPanelGroup getGroup1() {
		if (group1 == null) {
			group1 = (HtmlPanelGroup) findComponentInRoot("group1");
		}
		return group1;
	}

	protected HtmlPanelGrid getGrid1() {
		if (grid1 == null) {
			grid1 = (HtmlPanelGrid) findComponentInRoot("grid1");
		}
		return grid1;
	}

	protected HtmlOutputText getLoginHeader() {
		if (loginHeader == null) {
			loginHeader = (HtmlOutputText) findComponentInRoot("loginHeader");
		}
		return loginHeader;
	}

	protected HtmlCommandExButton getLoginButton() {
		if (loginButton == null) {
			loginButton = (HtmlCommandExButton) findComponentInRoot("loginButton");
		}
		return loginButton;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlOutputLink getLink1() {
		if (link1 == null) {
			link1 = (HtmlOutputLink) findComponentInRoot("link1");
		}
		return link1;
	}

	protected HtmlOutputLink getLinkRememberMe() {
		if (linkRememberMe == null) {
			linkRememberMe = (HtmlOutputLink) findComponentInRoot("linkRememberMe");
		}
		return linkRememberMe;
	}

	protected HtmlOutputText getTextRememberMe() {
		if (textRememberMe == null) {
			textRememberMe = (HtmlOutputText) findComponentInRoot("textRememberMe");
		}
		return textRememberMe;
	}

	protected HtmlSelectBooleanCheckbox getCheckboxRemember() {
		if (checkboxRemember == null) {
			checkboxRemember = (HtmlSelectBooleanCheckbox) findComponentInRoot("checkboxRemember");
		}
		return checkboxRemember;
	}

	protected HtmlPanelGrid getGrid3() {
		if (grid3 == null) {
			grid3 = (HtmlPanelGrid) findComponentInRoot("grid3");
		}
		return grid3;
	}

	protected HtmlSelectBooleanCheckbox getCheckbox1() {
		if (checkbox1 == null) {
			checkbox1 = (HtmlSelectBooleanCheckbox) findComponentInRoot("checkbox1");
		}
		return checkbox1;
	}

	protected HtmlInputText getTextEmail() {
		if (textEmail == null) {
			textEmail = (HtmlInputText) findComponentInRoot("textEmail");
		}
		return textEmail;
	}

	protected HtmlOutputLabel getLabel2() {
		if (label2 == null) {
			label2 = (HtmlOutputLabel) findComponentInRoot("label2");
		}
		return label2;
	}

	protected HtmlMessages getMessagesAuth() {
		if (messagesAuth == null) {
			messagesAuth = (HtmlMessages) findComponentInRoot("messagesAuth");
		}
		return messagesAuth;
	}

	protected HtmlInputSecret getSecretPass() {
		if (secretPass == null) {
			secretPass = (HtmlInputSecret) findComponentInRoot("secretPass");
		}
		return secretPass;
	}

	protected HtmlOutputLabel getLabel3() {
		if (label3 == null) {
			label3 = (HtmlOutputLabel) findComponentInRoot("label3");
		}
		return label3;
	}

	protected HtmlOutputLabel getLabelPass() {
		if (labelPass == null) {
			labelPass = (HtmlOutputLabel) findComponentInRoot("labelPass");
		}
		return labelPass;
	}

	protected HtmlInputHelperSetFocus getSetFocus1() {
		if (setFocus1 == null) {
			setFocus1 = (HtmlInputHelperSetFocus) findComponentInRoot("setFocus1");
		}
		return setFocus1;
	}

	protected HtmlOutputLabel getLabel1() {
		if (label1 == null) {
			label1 = (HtmlOutputLabel) findComponentInRoot("label1");
		}
		return label1;
	}

	protected HtmlOutputText getText5() {
		if (text5 == null) {
			text5 = (HtmlOutputText) findComponentInRoot("text5");
		}
		return text5;
	}

	protected HtmlPanelGrid getGrid2() {
		if (grid2 == null) {
			grid2 = (HtmlPanelGrid) findComponentInRoot("grid2");
		}
		return grid2;
	}

	protected HtmlOutputLink getLinkEELearn() {
		if (linkEELearn == null) {
			linkEELearn = (HtmlOutputLink) findComponentInRoot("linkEELearn");
		}
		return linkEELearn;
	}

	protected HtmlOutputLabel getLabel4() {
		if (label4 == null) {
			label4 = (HtmlOutputLabel) findComponentInRoot("label4");
		}
		return label4;
	}

	protected HtmlOutputText getText3() {
		if (text3 == null) {
			text3 = (HtmlOutputText) findComponentInRoot("text3");
		}
		return text3;
	}

	protected HtmlPanelGrid getGrid4() {
		if (grid4 == null) {
			grid4 = (HtmlPanelGrid) findComponentInRoot("grid4");
		}
		return grid4;
	}

	protected HtmlOutputLink getLinkOnline() {
		if (linkOnline == null) {
			linkOnline = (HtmlOutputLink) findComponentInRoot("linkOnline");
		}
		return linkOnline;
	}

	protected HtmlOutputLabel getLabel5() {
		if (label5 == null) {
			label5 = (HtmlOutputLabel) findComponentInRoot("label5");
		}
		return label5;
	}

	protected HtmlOutputText getText4() {
		if (text4 == null) {
			text4 = (HtmlOutputText) findComponentInRoot("text4");
		}
		return text4;
	}

	protected HtmlPanelGrid getGrid5() {
		if (grid5 == null) {
			grid5 = (HtmlPanelGrid) findComponentInRoot("grid5");
		}
		return grid5;
	}

	protected HtmlOutputLink getLinkContact() {
		if (linkContact == null) {
			linkContact = (HtmlOutputLink) findComponentInRoot("linkContact");
		}
		return linkContact;
	}

}