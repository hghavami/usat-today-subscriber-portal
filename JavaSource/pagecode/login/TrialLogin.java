/**
 * 
 */
package pagecode.login;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputSecret;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.context.FacesContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import pagecode.PageCodeBase;

import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlFormItem;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.business.interfaces.customer.TrialCustomerIntf;
import com.usatoday.businessObjects.customer.LoginException;
import com.usatoday.businessObjects.customer.TrialCustomerBO;
import com.usatoday.esub.cookies.AuthenticationCookieProcessor;
import com.usatoday.esub.ncs.handlers.LoginPageFormHandler;
import com.usatoday.esub.trials.handlers.PartnerHandler;
import com.usatoday.esub.trials.handlers.TrialCustomerHandler;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlMessages;

/**
 * @author aeast
 * 
 */
public class TrialLogin extends PageCodeBase {

	protected HtmlScriptCollector scriptCollector1;
	protected HtmlForm form1;
	protected HtmlPanelFormBox formBoxLoginFormBox;
	protected HtmlFormItem formItemTrialPassword;
	protected HtmlFormItem formItemTrialEmailAddress;
	protected HtmlInputText textEmail;
	protected HtmlInputSecret secretPassword;
	protected HtmlFormItem formItemRememberMe;
	protected HtmlSelectBooleanCheckbox checkboxRememberMe;
	protected LoginPageFormHandler loginFormHandler;
	protected TrialCustomerHandler trialCustomerHandler;
	protected HtmlJspPanel jspPanelFooterPanel;
	protected HtmlCommandExButton buttonLoginButton;
	protected HtmlOutputLinkEx linkEx1;
	protected HtmlOutputText text1;
	protected PartnerHandler trialPartnerHandler;
	protected HtmlOutputText mainHeader;
	protected HtmlPanelGrid gridHeaderGrid1;
	protected HtmlMessages messagesLoginErrors;

	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlForm getForm1() {
		if (form1 == null) {
			form1 = (HtmlForm) findComponentInRoot("form1");
		}
		return form1;
	}

	protected HtmlPanelFormBox getFormBoxLoginFormBox() {
		if (formBoxLoginFormBox == null) {
			formBoxLoginFormBox = (HtmlPanelFormBox) findComponentInRoot("formBoxLoginFormBox");
		}
		return formBoxLoginFormBox;
	}

	protected HtmlFormItem getFormItemTrialPassword() {
		if (formItemTrialPassword == null) {
			formItemTrialPassword = (HtmlFormItem) findComponentInRoot("formItemTrialPassword");
		}
		return formItemTrialPassword;
	}

	protected HtmlFormItem getFormItemTrialEmailAddress() {
		if (formItemTrialEmailAddress == null) {
			formItemTrialEmailAddress = (HtmlFormItem) findComponentInRoot("formItemTrialEmailAddress");
		}
		return formItemTrialEmailAddress;
	}

	protected HtmlInputText getTextEmail() {
		if (textEmail == null) {
			textEmail = (HtmlInputText) findComponentInRoot("textEmail");
		}
		return textEmail;
	}

	protected HtmlInputSecret getSecretPassword() {
		if (secretPassword == null) {
			secretPassword = (HtmlInputSecret) findComponentInRoot("secretPassword");
		}
		return secretPassword;
	}

	public String doButtonLoginButtonAction() {
		// Type Java code that runs when the component is clicked

		String responseString = "failure";

		FacesContext facesContext = FacesContext.getCurrentInstance();

		TrialCustomerHandler tch = this.getTrialCustomerHandler();

		tch.setTrialCustomer(null);

		TrialCustomerIntf trialCustomer = null;
		try {
			trialCustomer = TrialCustomerBO.getTrialCustomerByEmailPassword(this.getLoginFormHandler().getEmailAddress(), this
					.getLoginFormHandler().getPassword());

			if (trialCustomer != null) {
				tch.setTrialCustomer(trialCustomer);

				if (!trialCustomer.getCurrentInstance().getIsTrialPeriodExpired()) {
					if (this.getLoginFormHandler().isRememberMe()) {
						AuthenticationCookieProcessor cp = new AuthenticationCookieProcessor();

						Cookie c = cp.createRememberMeTrialCookie(trialCustomer.getEmailAddress(), trialCustomer
								.getCurrentInstance().getPartner().getDurationInDays());
						((HttpServletResponse) facesContext.getExternalContext().getResponse()).addCookie(c);
					}
					responseString = "success";
				} else {
					// trial expired.
					responseString = "trialOver";
				}

				PartnerHandler ph = this.getTrialPartnerHandler();
				ph.setPartner(trialCustomer.getCurrentInstance().getPartner());
			}
		} catch (LoginException le) {
			FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, le.getErrorMessage(), null);

			facesContext.addMessage(null, facesMsg);
			responseString = "failure";

		} catch (Exception e) {
			FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Unexpected Exception: " + e.getMessage(), null);

			facesContext.addMessage(null, facesMsg);

			responseString = "failure";
		}

		return responseString;
	}

	protected HtmlFormItem getFormItemRememberMe() {
		if (formItemRememberMe == null) {
			formItemRememberMe = (HtmlFormItem) findComponentInRoot("formItemRememberMe");
		}
		return formItemRememberMe;
	}

	protected HtmlSelectBooleanCheckbox getCheckboxRememberMe() {
		if (checkboxRememberMe == null) {
			checkboxRememberMe = (HtmlSelectBooleanCheckbox) findComponentInRoot("checkboxRememberMe");
		}
		return checkboxRememberMe;
	}

	/**
	 * @managed-bean true
	 */
	protected LoginPageFormHandler getLoginFormHandler() {
		if (loginFormHandler == null) {
			loginFormHandler = (LoginPageFormHandler) getManagedBean("loginFormHandler");
		}
		return loginFormHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setLoginFormHandler(LoginPageFormHandler loginFormHandler) {
		this.loginFormHandler = loginFormHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected TrialCustomerHandler getTrialCustomerHandler() {
		if (trialCustomerHandler == null) {
			trialCustomerHandler = (TrialCustomerHandler) getManagedBean("trialCustomerHandler");
		}
		return trialCustomerHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setTrialCustomerHandler(TrialCustomerHandler trialCustomerHandler) {
		this.trialCustomerHandler = trialCustomerHandler;
	}

	protected HtmlJspPanel getJspPanelFooterPanel() {
		if (jspPanelFooterPanel == null) {
			jspPanelFooterPanel = (HtmlJspPanel) findComponentInRoot("jspPanelFooterPanel");
		}
		return jspPanelFooterPanel;
	}

	protected HtmlCommandExButton getButtonLoginButton() {
		if (buttonLoginButton == null) {
			buttonLoginButton = (HtmlCommandExButton) findComponentInRoot("buttonLoginButton");
		}
		return buttonLoginButton;
	}

	protected HtmlOutputLinkEx getLinkEx1() {
		if (linkEx1 == null) {
			linkEx1 = (HtmlOutputLinkEx) findComponentInRoot("linkEx1");
		}
		return linkEx1;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	/**
	 * @managed-bean true
	 */
	protected PartnerHandler getTrialPartnerHandler() {
		if (trialPartnerHandler == null) {
			trialPartnerHandler = (PartnerHandler) getManagedBean("trialPartnerHandler");
		}
		return trialPartnerHandler;
	}

	/**
	 * @managed-bean true
	 */
	protected void setTrialPartnerHandler(PartnerHandler trialPartnerHandler) {
		this.trialPartnerHandler = trialPartnerHandler;
	}

	protected HtmlOutputText getMainHeader() {
		if (mainHeader == null) {
			mainHeader = (HtmlOutputText) findComponentInRoot("mainHeader");
		}
		return mainHeader;
	}

	protected HtmlPanelGrid getGridHeaderGrid1() {
		if (gridHeaderGrid1 == null) {
			gridHeaderGrid1 = (HtmlPanelGrid) findComponentInRoot("gridHeaderGrid1");
		}
		return gridHeaderGrid1;
	}

	protected HtmlMessages getMessagesLoginErrors() {
		if (messagesLoginErrors == null) {
			messagesLoginErrors = (HtmlMessages) findComponentInRoot("messagesLoginErrors");
		}
		return messagesLoginErrors;
	}

}