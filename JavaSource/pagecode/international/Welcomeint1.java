/**
 * 
 */
package pagecode.international;

import pagecode.PageCodeBase;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.ibm.faces.component.html.HtmlJspPanel;

/**
 * @author swong
 * 
 */
public class Welcomeint1 extends PageCodeBase {

	protected HtmlScriptCollector scriptCollector1;
	protected HtmlJspPanel jjspPanelLeftSidePrintEdition3;
	protected HtmlJspPanel jspPanelHeaderPanel;
	protected HtmlJspPanel jjspPanelRightSideEEdition1;
	protected HtmlJspPanel jspPanelRightSideEEdition1a;
	protected HtmlJspPanel jspPanelRightSideEEdition1b;
	protected HtmlJspPanel jspPanelLeftSideEEdition2;

	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlJspPanel getJjspPanelLeftSidePrintEdition3() {
		if (jjspPanelLeftSidePrintEdition3 == null) {
			jjspPanelLeftSidePrintEdition3 = (HtmlJspPanel) findComponentInRoot("jjspPanelLeftSidePrintEdition3");
		}
		return jjspPanelLeftSidePrintEdition3;
	}

	protected HtmlJspPanel getJspPanelHeaderPanel() {
		if (jspPanelHeaderPanel == null) {
			jspPanelHeaderPanel = (HtmlJspPanel) findComponentInRoot("jspPanelHeaderPanel");
		}
		return jspPanelHeaderPanel;
	}

	protected HtmlJspPanel getJjspPanelRightSideEEdition1() {
		if (jjspPanelRightSideEEdition1 == null) {
			jjspPanelRightSideEEdition1 = (HtmlJspPanel) findComponentInRoot("jjspPanelRightSideEEdition1");
		}
		return jjspPanelRightSideEEdition1;
	}

	protected HtmlJspPanel getJspPanelRightSideEEdition1a() {
		if (jspPanelRightSideEEdition1a == null) {
			jspPanelRightSideEEdition1a = (HtmlJspPanel) findComponentInRoot("jspPanelRightSideEEdition1a");
		}
		return jspPanelRightSideEEdition1a;
	}

	protected HtmlJspPanel getJspPanelRightSideEEdition1b() {
		if (jspPanelRightSideEEdition1b == null) {
			jspPanelRightSideEEdition1b = (HtmlJspPanel) findComponentInRoot("jspPanelRightSideEEdition1b");
		}
		return jspPanelRightSideEEdition1b;
	}

	protected HtmlJspPanel getJspPanelLeftSideEEdition2() {
		if (jspPanelLeftSideEEdition2 == null) {
			jspPanelLeftSideEEdition2 = (HtmlJspPanel) findComponentInRoot("jspPanelLeftSideEEdition2");
		}
		return jspPanelLeftSideEEdition2;
	}

}