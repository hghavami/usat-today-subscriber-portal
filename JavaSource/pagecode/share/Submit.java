/**
 * 
 */
package pagecode.share;

import pagecode.PageCodeBase;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlPanelGroup;

/**
 * @author swong
 * 
 */
public class Submit extends PageCodeBase {

	protected HtmlOutputText mainHeader;
	protected HtmlPanelGrid grid1;
	protected HtmlPanelGroup group2;
	protected HtmlPanelGroup group1;
	protected HtmlPanelGrid grid2;
	protected HtmlPanelGroup group4;
	protected HtmlOutputText textReferCopy;
	protected HtmlPanelGroup group3;
	protected HtmlOutputText textMainTableHeader1;

	protected HtmlOutputText getMainHeader() {
		if (mainHeader == null) {
			mainHeader = (HtmlOutputText) findComponentInRoot("mainHeader");
		}
		return mainHeader;
	}

	protected HtmlPanelGrid getGrid1() {
		if (grid1 == null) {
			grid1 = (HtmlPanelGrid) findComponentInRoot("grid1");
		}
		return grid1;
	}

	protected HtmlPanelGroup getGroup2() {
		if (group2 == null) {
			group2 = (HtmlPanelGroup) findComponentInRoot("group2");
		}
		return group2;
	}

	protected HtmlPanelGroup getGroup1() {
		if (group1 == null) {
			group1 = (HtmlPanelGroup) findComponentInRoot("group1");
		}
		return group1;
	}

	protected HtmlPanelGrid getGrid2() {
		if (grid2 == null) {
			grid2 = (HtmlPanelGrid) findComponentInRoot("grid2");
		}
		return grid2;
	}

	protected HtmlPanelGroup getGroup4() {
		if (group4 == null) {
			group4 = (HtmlPanelGroup) findComponentInRoot("group4");
		}
		return group4;
	}

	protected HtmlOutputText getTextReferCopy() {
		if (textReferCopy == null) {
			textReferCopy = (HtmlOutputText) findComponentInRoot("textReferCopy");
		}
		return textReferCopy;
	}

	protected HtmlPanelGroup getGroup3() {
		if (group3 == null) {
			group3 = (HtmlPanelGroup) findComponentInRoot("group3");
		}
		return group3;
	}

	protected HtmlOutputText getTextMainTableHeader1() {
		if (textMainTableHeader1 == null) {
			textMainTableHeader1 = (HtmlOutputText) findComponentInRoot("textMainTableHeader1");
		}
		return textMainTableHeader1;
	}

}